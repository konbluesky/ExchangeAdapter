// KsFtAnalyzing.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <cassert>
#include <fstream>
#include <sstream>

//case 'B':	*psecurityexch = "SHFE";break;
//case 'G':	*psecurityexch = "CFFEX";break;
//case 'C':	*psecurityexch = "ZCE";break;
//case 'D':	*psecurityexch = "DCE";break;

typedef struct ksftquota_pubdata_item_tag
{
	int	contract_id;				//由交易品种和交割期算出来的id,对应：id号;
	int	upd_serial;				//行情更新序号，对应：序号
	int	upd_date;				//行情日期,格式：YYYYMMDD
	int	pre_upd_date;				//行情上次更新日期(保留)
	int	pre_upd_serial;				//上次更新时的序号(保留)
	char	sys_recv_time[9];		//行情服务器收到行情的时间，行情服务器唯一维护，格式：HH:MM:SS

	char	exchCode[6];		//交易所代码
	char	varity_code[32];		//品种代码
	char	deliv_date[9];		//交割期
	char	chgStatus[2];	//对应：状态
	//1-2bit表示：买入;3-4bit表示：卖出;
	//5-6bit表示：最新;7-8bit不用;
	//00->新行情    01->低于以前的行情
	//11->高于以前的行情    00->与以前相平

	double	openPrice;				//开盘价
	double	lastPrice;				//最新价
	double	highestPrice;				//最高价
	double	lowestPrice;				//最低价
	int	doneVolume;				//成交量
	double	chgPrice;				//涨跌
	double	upperLimitPrice;			//涨停板
	double	lowerLimitPrice;			//跌停板
	double	hisHighestPrice;			//历史最高价
	double	hisLowestPrice;				//历史最低价
	int	openInterest;				//净持仓
	double	preSettlePrice;				//昨日结算
	double	preClosePrice;				//昨日收盘
	double	settlePrice;				//今日结算
	double	turnover;				//成交金额
	//20061208新增,qbin
	int	preOpenInterest;			//昨日持仓量
	double	closePrice;				//今日收盘
	double	preDelta;				//昨虚实度
	double	currDelta;				//今虚实度


	double	bidPrice1;				//买入价1
	int	bidVolume1;				//买入量1
	double	bidPrice2;				//买入价2
	int	bidVolume2;				//买入量2
	double	bidPrice3;				//买入价3
	int	bidVolume3;				//买入量3
	double	bidPrice4;				//买入价4
	int	bidVolume4;				//买入量4
	double	bidPrice5;				//买入价5
	int	bidVolume5;				//买入量5

	double	askPrice1;				//卖出价1
	int	askVolume1;				//卖出量1
	double	askPrice2;				//卖出价2
	int	askVolume2;				//卖出量2
	double	askPrice3;				//卖出价3
	int	askVolume3;				//卖出量3
	double	askPrice4;				//卖出价4
	int	askVolume4;				//卖出量4
	double	askPrice5;				//卖出价5
	int	askVolume5;				//卖出量5

	char cmbtype[2];   //‘0’或NULL:普通行情
	//‘1’：组合套利行情
	//20080514
	int derive_bidlot;   //买推导量   组合买入数量
	int derive_asklot;   //卖推导量   组合卖出数量

}KSFT_QUOTA_PUBDATA_ITEM;

int32 GetBuffer(char8 *tmpbuffer,char8 **pindex)
{
	char8 *pnewindex = tmpbuffer;
	bool bflag = true;
	do {
		*pnewindex = **pindex;
		if(',' == *pnewindex)
			*pnewindex = '\0';
		if('\0' == *pnewindex)
			break;
		pnewindex++;
		(*pindex)++;
	} while (bflag);
	(*pindex)++;
	return 0;
}

void GetVar(char8 **pindex,int32 *idata)
{
	char8 tmpbuffer[64] = {'\0'};
	GetBuffer(tmpbuffer,pindex);
	std::stringstream ss;
	ss<<tmpbuffer;
	ss>>*idata;
}

void GetVar(char8 **pindex,double *idata)
{
	char8 tmpbuffer[64] = {'\0'};
	GetBuffer(tmpbuffer,pindex);
	std::stringstream ss;
	ss<<tmpbuffer;
	ss>>*idata;
}

void GetVar(char8 **pindex,char8 *idata)
{
	string8 str;
	char tmpbuffer[64] = {'\0'};
	GetBuffer(tmpbuffer,pindex);
	std::stringstream ss;
	ss<<tmpbuffer;
	ss>>str;
	std::strcpy(idata,str.c_str());
}

static int iseq = 0;

int8 LogFile(KSFT_QUOTA_PUBDATA_ITEM &item,const char8 *pexchcode,
	const char8 *pvaritycode,const char8 *pdelivdate)
{
	int8 flag;
	if(0 != std::strcmp(pexchcode,item.exchCode) || 
		0 != std::strcmp(pvaritycode,item.varity_code) ||
		0 != std::strcmp(pdelivdate,item.deliv_date))
		return -1;

	if(1 == item.upd_serial)
		flag = -2;
	else if(iseq >= item.upd_serial)
		return -1;
	else if(iseq != 0 && 1 != item.upd_serial && iseq + 1 != item.upd_serial)
		flag = 0;
	else
		flag = 1;

	iseq = item.upd_serial;
	return flag;
}

void PrintfLog(KSFT_QUOTA_PUBDATA_ITEM &item,std::FILE *file,int8 flag)
{
	if(0 > flag)
		return;
	if(0 == flag)
		std::fprintf(file,"MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM\n");

	std::fprintf(file,
		"Num:%6d,%8s,%8d,"
		"%10d,%1d,%1d,"
		"%2s,%5s,%4s,"
		"%10.2f,%10.2f,%10.2f,%10.2f,"
		"%6d,%10.2f,%10.2f,%10.2f,%10.2f,%10.2f,"
		"%6d,%10.2f,%10.2f,%10.2f,%20.6f,"
		"%2d,%2.1f,%2.1f,%2.1f,%10.2f,%6d,%10.2f,%6d,"
		"%c,%4d,%4d\n",
		item.upd_serial,item.sys_recv_time,item.upd_date,
		item.contract_id,item.pre_upd_date,item.pre_upd_serial,
		item.exchCode,item.varity_code,item.deliv_date,
		item.openPrice,item.lastPrice,item.highestPrice,item.lowestPrice,
		item.doneVolume,item.chgPrice,item.upperLimitPrice,item.lowerLimitPrice,item.hisHighestPrice,item.hisLowestPrice,
		item.openInterest,item.preSettlePrice,item.preClosePrice,item.settlePrice,item.turnover,
		item.preOpenInterest,item.closePrice,item.preDelta,item.currDelta,item.bidPrice1,item.bidVolume1,item.askPrice1,item.askVolume1,
		('1' == item.cmbtype[0] ? '1':'0'),item.derive_bidlot,item.derive_asklot);
}

void ReadLogFile(string8 logfilename,string8 formatfilename,int32 lines,
	string8 exchcode,string8 varitycode,string8 devidate)
{
	std::FILE *file = std::fopen(logfilename.c_str(),"r");
	if (NULL == file)
		assert(false);
	std::FILE *newfile = std::fopen(formatfilename.c_str(),"a+");
	if (NULL == newfile)
		assert(false);

	char *pindex = NULL;
	char buffer[512];
	KSFT_QUOTA_PUBDATA_ITEM item;
	int32 flag;
	for (int32 index = 0;index < lines;index++)
	{
		std::fgets(buffer,512,file);
		pindex = buffer;

		GetVar(&pindex,&(item.contract_id));
		GetVar(&pindex,&(item.upd_serial));
		GetVar(&pindex,&(item.upd_date));
		GetVar(&pindex,&(item.pre_upd_date));
		GetVar(&pindex,&(item.pre_upd_serial));
		GetVar(&pindex,item.sys_recv_time);
		GetVar(&pindex,item.exchCode);
		GetVar(&pindex,item.varity_code);
		GetVar(&pindex,item.deliv_date);
		GetVar(&pindex,item.chgStatus);
		GetVar(&pindex,&(item.openPrice));
		GetVar(&pindex,&(item.lastPrice));
		GetVar(&pindex,&(item.highestPrice));
		GetVar(&pindex,&(item.lowestPrice));
		GetVar(&pindex,&(item.doneVolume));
		GetVar(&pindex,&(item.chgPrice));
		GetVar(&pindex,&(item.upperLimitPrice));
		GetVar(&pindex,&(item.lowerLimitPrice));
		GetVar(&pindex,&(item.hisHighestPrice));
		GetVar(&pindex,&(item.hisLowestPrice));
		GetVar(&pindex,&(item.openInterest));
		GetVar(&pindex,&(item.preSettlePrice));
		GetVar(&pindex,&(item.preClosePrice));
		GetVar(&pindex,&(item.settlePrice));
		GetVar(&pindex,&(item.turnover));
		GetVar(&pindex,&(item.preOpenInterest));
		GetVar(&pindex,&(item.closePrice));
		GetVar(&pindex,&(item.preDelta));
		GetVar(&pindex,&(item.currDelta));
		GetVar(&pindex,&(item.bidPrice1));
		GetVar(&pindex,&(item.bidVolume1));
		GetVar(&pindex,&(item.askPrice1));
		GetVar(&pindex,&(item.askVolume1));
		GetVar(&pindex,item.cmbtype);
		GetVar(&pindex,&(item.derive_bidlot));
		GetVar(&pindex,&(item.derive_asklot));

		flag = LogFile(item,exchcode.c_str(),varitycode.c_str(),
			devidate.c_str());
		PrintfLog(item,newfile,flag);
	}

	std::fclose(file);
}
int _tmain(int argc, _TCHAR* argv[])
{
	if(7 > argc){
		std::cout<<"缺少参数.格式为:Path <File> <newFile> <lines>"
			<<std::endl<<"		     <exchid> <contractid> <deliverdate>"
			<<std::endl<<"退出请按'q':";
		while('q' != std::getchar());
		return -1;
	} else {
		string8 logfilename = argv[1];
		string8 formatfilename = argv[2];
		int32 lines = std::atoi(argv[3]);
		string8 exchcode = argv[4];
		string8 varitycode = argv[5];
		string8 devidate = argv[6];
		ReadLogFile(logfilename,formatfilename,lines,exchcode,varitycode,
			devidate);
		return 0;
	}
}