#ifndef __CTP_HELPER_H__
#define __CTP_HELPER_H__

#include <iostream>
#include "IOrderService.h"
#include "ITradeAdapter.h"
#include "ThostFtdcUserApiDataType.h"
#include "ThostFtdcUserApiStruct.h"
#include "threadlock.h"
#include "MetaIO.h"
#include "ExchangeAdapterDef.h"
#include "utimeutils.h"

#pragma warning(disable : 4506)

struct CThostFtdcRspInfoField;

namespace tradeadapter
{
	namespace ctp
	{
		std::ostream& operator <<(std::ostream& os, const	CThostFtdcOrderField& data);
		std::ostream& operator <<(std::ostream& os, const	CThostFtdcRspInfoField& data);
		std::ostream& operator <<(std::ostream& os, const	CThostFtdcDepthMarketDataField& data);
		std::ostream& operator <<(std::ostream& os, const	CThostFtdcInvestorPositionField& data);


		enum EventType
		{
			NoDefined,
			TimeOut,
			SysClose,
			OnFrontConnected,
			OnFrontDisconnected,
			OnRspSettlementInfoConfirm,
			OnRspUserLogin,
			OnRspUserLogout,
			OnRspQryDepthMarketData,
			OnRspQryInvestorPosition,
			OnRspQrySettlementInfo,
			OnRspOrderAction,
			OnRspOrderInsert,
			OnRspQryOrder,
			OnRtnOrder,
			OnErrRtnOrderAction,//撤单
			OnRspQryTradingAccount, //查询账户资金

			OnRspQryInvestor, //查询帐户基本信息

		};
		struct SyncData
		{

			EventType responsetype;
			char data[1024 * 4];

			CThostFtdcRspInfoField rspinfo;
			SyncData();

		};







		struct CTPMeta
		{
			bool m_settled;
			long m_requestid;
			long m_orderid;

		public: CTPMeta();

		};


		class CCTPData : public CMetaIO < CTPMeta >
		{
		public:
			void SetOrderStartID(int val);



			int GetRequestID();
			int GetOrderID();
			bool GetSettled();
		private:
			CTPMeta m_data;

			threadlock::multi_threaded_local m_lock;


		};

		class TAIAPI  CTPHelper
		{
		public:

#pragma warning(disable : 4251)
			static std::vector< CThostFtdcInstrumentField> products;

			static inline std::string GetCurrentIFCode();

			static inline PositionInfo ToPostionInfo(const CThostFtdcInvestorPositionField& field);
			static inline TThostFtdcOrderPriceTypeType ToOrderPriceType(EPriceMode mode);
			static inline TThostFtdcDirectionType ToTFtdcDirectionType(EBuySell side);
			static inline TThostFtdcOffsetFlagType ToTThostFtdcOffsetFlagType(EOpenClose offset);
			static inline TThostFtdcHedgeFlagType ToTThostFtdcHedgeFlagType(EHedgeFlag hedgeFlag);
			static inline TThostFtdcTimeConditionType ToTThostFtdcTimeConditionType(EPriceMode mode);


			static inline TThostFtdcHedgeFlagType ToTThostFtdcHedgeFlagType(int i);

			static inline int CTPTime2Int(const char* input)
			{
				int ret = 0;
				ret =
					_char2int(input[0]) * 1000000 + _char2int(input[1]) * 100000 + _char2int(input[3]) * 10000 + _char2int(input[4]) * 1000 + _char2int(input[6]) * 100 + _char2int(input[7]);
				return ret;
			}
			static inline int _char2int(char input)
			{
				return input - '0';
			}

			static inline EPosSide FromTThostFtdcPosiDirection(TThostFtdcPosiDirectionType direction);
			static inline void Assign(char* pbuff, char data);
			static inline void Assign(char* pbuff, const char* data);
			static inline void Assign(char* buff, const std::string& data);
			static inline void Assign(char* buff, const std::wstring& data);
			static inline void Assign(char* buff, int id);
			static inline int intlen(int val);
			static inline void Assign(char* buff, int id, int num, int fillchar);
		};
	};

};
#endif