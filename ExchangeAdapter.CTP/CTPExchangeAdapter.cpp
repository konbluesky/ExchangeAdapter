#include "StdAfx.h"
#include "ucodehelper.h"
#include "ExchangeAdapterDef.h"
#include "ustringutils.h"
#include <assert.h>
#include "CTPExchangeAdapter.h"
#include "udirectory.h"
#include "ThostFtdcTraderApi.h"
#include "ustringutils.h"
#include "CTPMsgHandler.h"
#include "umaths.h"
#include "tinyxml.h"
#include "tconfigsetting.h"
#include "CTPHelper.h"
#include "ustringutils.h"
#include "texceptionex.h"
#include "uconsole.h"
#include "ucomhelper.h"
#include "utimeutils.h"
#pragma warning(disable : 4482)
#pragma warning(disable : 4244)

//注：这里的采用和股票统一的框架，技术上是可取的，但速度上需要进行充分的优化，
//1.下单，不等待同步服务器状态的返回，单子的状态由异步的返回再记录状态
//2.查单、撤单这些需要同步进行，但查单应该充分利用缓存，撤单也类似。
USING_LIBSPACE
namespace tradeadapter
{


	namespace ctp
	{

		//0
		CCTPExchangeAdapter::CCTPExchangeAdapter(void) :m_api(NULL), m_handler(NULL), m_initialized(false)
		{
		}

		//1.
		bool CCTPExchangeAdapter::InitializeByXML(const TCHAR* fpath)
		{
			TConfigSetting cfg;
			bool ret = false;
			ret = cfg.LoadConfig(fpath);
			if (!ret)
			{
				SetLastError(MAKE_ERRORINFO( EErrSystem::EConfigFile, -1, _T("无法找到CTP的配置文件")));
				return false;
			}

			CTPParam param;

			param.addr = U82TS(cfg["configuration"]["saddress"].EleVal().c_str());
			param.port = UMaths::ToInt32(cfg["configuration"]["iport"].EleVal());
			param.BrokerID = U82TS(cfg["configuration"]["brokerid"].EleVal().c_str());
			param.usr = U82TS(cfg["configuration"]["suserid"].EleVal().c_str());
			param.pwd = U82TS(cfg["configuration"]["spassword"].EleVal().c_str());
			param.metafilepath = U82TS(cfg["configuration"]["meta"].EleVal().c_str());
			param.hedge = (EHedgeFlag)UMaths::ToInt32(cfg["configuration"]["CombHedgeFlag"].EleVal());

			ret = Initialize(&param);

			return ret;
		}

		//2.
		bool CCTPExchangeAdapter::Initialize(void* param /*= NULL */)
		{
			_unInitialize();
			assert(param != NULL);

			_CTP_GUARD();
			CTPParam* input = (CTPParam*)param;
			m_param = CTPParamA::FromCTPParam(*input);

			//0.create  setting log 
			UDirectory::CreateDirectory(_T("ctptradelog"));
			std::string logpath = std::string("ctptradelog\\") + m_param.usr + TS2GB(UStrConvUtils::ToString(UTimeUtils::GetNowDate()));

			//1.create api instance & msg handler instance.
			m_api = CThostFtdcTraderApi::CreateFtdcTraderApi(logpath.c_str());
			m_handler = new CCTPMsgHandler(this);
			m_api->RegisterSpi(m_handler);

			//2. TODO : these param should check
			m_api->SubscribePrivateTopic(THOST_TERT_QUICK);
			m_api->SubscribePublicTopic(THOST_TERT_QUICK);


			//3. Start connect & login
			std::string addr = TS2GB(m_param.addr);
			char proctal[256] = { 0 };
			sprintf(proctal, "%s:%d", addr.c_str(), m_param.port);
			m_api->RegisterFront(proctal);
			m_api->Init();

			SyncData data = m_handler->GetSyncReply(EventType::OnFrontConnected);//this is param less callback ,which means no error would occurs except timeout.
			if (data.responsetype != EventType::OnFrontConnected)
			{
				return false;
			}



			CThostFtdcReqUserLoginField field = { 0 };
			const char* date = m_api->GetTradingDay();
			m_param.metafilepath = m_param.metafilepath + date;
			m_ctpinfo.SetMetaPath(m_param.metafilepath.c_str());

			CTPHelper::Assign(field.TradingDay, date);
			CTPHelper::Assign(field.UserProductInfo, "Tigera");

			CTPHelper::Assign(field.BrokerID, m_param.BrokerID);
			CTPHelper::Assign(field.UserID, m_param.usr);
			CTPHelper::Assign(field.Password, m_param.pwd);
			m_api->ReqUserLogin(&field, m_ctpinfo.GetRequestID());

			data = m_handler->GetSyncReply(EventType::OnRspUserLogin);
			if (data.responsetype != EventType::OnRspUserLogin)
			{
				return false;
			}
			if (data.rspinfo.ErrorID != 0)
			{
				SetLastError(MAKE_ERRORINFO( EErrSystem::ETradeAdapter,-1, data.rspinfo.ErrorMsg) );
				return false;
			}

			std::cout << "登录成功" << std::endl;



			if (!m_ctpinfo.GetSettled())
			{
				CThostFtdcSettlementInfoConfirmField field1 = { 0 };
				strcpy_s(field1.ConfirmDate, date);
				CTPHelper::Assign(field1.BrokerID, m_param.BrokerID);
				strcpy_s(field1.InvestorID, m_param.usr.c_str());
				m_api->ReqSettlementInfoConfirm(&field1, m_ctpinfo.GetRequestID());
				m_handler->GetSyncReply(EventType::OnRspSettlementInfoConfirm);
			}
			else
			{
				/*
				CThostFtdcSettlementInfoField field1;
				strcpy_s(field1.BrokerID,m_usrinfo.BrokerID.c_str());
				strcpy_s(field1.,date);
				strcpy_s(field1.InvestorID,m_usrinfo.usr.c_str());
				m_api->ReqQrySettlementInfo(&field1,m_ctpinfo.GetRequestID());
				m_handler->GetReply(EventType::OnRspQrySettlementInfo);
				*/
			}

			m_initialized = true;
			//GetContractInfo();


			return true;


		}

		bool CCTPExchangeAdapter::IsInitialized()
		{
			return m_initialized;
		}




		double ToValidPrice(float price)
		{

			int val = (int)(price * 10 + 0.9);
			int ret1 = val / 10;
			int ret2 = val % 10;
			double ret = ret1 + ret2 / 10.0;
			return ret;
		}

		bool CCTPExchangeAdapter::EntrustOrder(Order order, OrderID* pOutID)
		{
			_CTP_GUARD();
			*pOutID = tradeadapter::InvalidOrderId;

			CThostFtdcInputOrderField field = { 0 };

			CTPHelper::Assign(field.BrokerID, m_param.BrokerID);
			CTPHelper::Assign(field.InvestorID, m_param.usr);
			CTPHelper::Assign(field.InstrumentID, order.ContractCode);
			field.OrderPriceType = (char)CTPHelper::ToOrderPriceType(order.pricemode);
			field.Direction = (char)CTPHelper::ToTFtdcDirectionType(order.OpeDir);
			CTPHelper::Assign(field.CombOffsetFlag, CTPHelper::ToTThostFtdcOffsetFlagType(order.openclose));
			if ((int)order.hedge == 0)
			{
				CTPHelper::Assign(field.CombHedgeFlag, CTPHelper::ToTThostFtdcHedgeFlagType(m_param.hedge));
			}
			else
			{
				CTPHelper::Assign(field.CombHedgeFlag, CTPHelper::ToTThostFtdcHedgeFlagType(order.hedge));
			}

			field.LimitPrice = order.pricemode == EPriceMode::eMarketPrice ? 0 : ToValidPrice(order.Price);
			field.VolumeTotalOriginal = order.Amount;
			field.TimeCondition = (char)CTPHelper::ToTThostFtdcTimeConditionType(order.pricemode);
			//field.BusinessUnit = 0.2;
			field.VolumeCondition = (char)THOST_FTDC_VC_AV;
			field.MinVolume = 1;
			field.ContingentCondition = (char)THOST_FTDC_CC_Immediately;
			strcpy_s(field.UserID, "");
			strcpy_s(field.GTDDate, "");
			strcpy_s(field.BusinessUnit, "0.2");

			field.ForceCloseReason = (char)THOST_FTDC_FCC_NotForceClose;
			field.IsAutoSuspend = 1;//自动挂起标志: 是
			field.UserForceClose = 0;//用户强平标志: 否

			int orderid = m_ctpinfo.GetOrderID();
			int reqid = m_ctpinfo.GetRequestID();
			std::string strorderid = TS2GB(UStrConvUtils::ToString(orderid));
			CTPHelper::Assign(field.OrderRef, orderid);
			//	m_reqid2orderid[reqid] = orderid;

			m_api->ReqOrderInsert(&field, reqid);




			std::vector<EventType> evnts;
			evnts.push_back(EventType::OnRtnOrder);
			evnts.push_back(EventType::OnRspOrderInsert);

			SyncData data = m_handler->GetReplys(evnts);
			if (data.responsetype != EventType::OnRtnOrder)
			{
				return false;
			}

			//
			data = m_handler->GetSyncReply(EventType::OnRtnOrder);

			if (data.responsetype != EventType::OnRtnOrder)
			{
				return false;
			}
			else
			{
				std::string strid = ((CThostFtdcOrderField*)data.data)->OrderSysID;


				int id = UMaths::ToInt32(USOpeUtils::Trim(strid.c_str()).c_str());
				if (id == 0)
					id = -1;

				*pOutID = id;
				return true;
			}
		}

		bool CCTPExchangeAdapter::CancelOrder(OrderID orderid)
		{
			_CTP_GUARD();
			if (orderid == tradeadapter::InvalidOrderId)
			{
				return true;
			}
			CThostFtdcInputOrderActionField field = { 0 };
			CTPHelper::Assign(field.ExchangeID, "SHFE");

			CTPHelper::Assign(field.BrokerID, m_param.BrokerID);
			CTPHelper::Assign(field.InvestorID, m_param.usr);

			CTPHelper::Assign(field.OrderSysID, orderid, 12, 32);

			field.ActionFlag = THOST_FTDC_AF_Delete;

			m_api->ReqOrderAction(&field, m_ctpinfo.GetRequestID());

			return true;
		}

		bool CCTPExchangeAdapter::QueryOrder(OrderID id, int* pCount)
		{
			_CTP_GUARD();

			*pCount = 0;

			if (id == tradeadapter::InvalidOrderId)
			{
				return false;
			}


			CThostFtdcQryOrderField field = { 0 };
			CTPHelper::Assign(field.BrokerID, m_param.BrokerID);
			CTPHelper::Assign(field.ExchangeID, "SHFE");
			CTPHelper::Assign(field.OrderSysID, id, 5, 32);//m_orid2sysid[id]);
			m_api->ReqQryOrder(&field, m_ctpinfo.GetRequestID());
			SyncData data = m_handler->GetSyncReply(EventType::OnRspQryOrder);
			if (data.responsetype != EventType::OnRspQryOrder)
			{
				return false;
			}
			else
			{
				CThostFtdcOrderField* pret = (CThostFtdcOrderField*)data.data;

				*pCount = pret->VolumeTraded;
				return true;
			}


		}

		bool CCTPExchangeAdapter::QueryDealedOrders(std::list<DealedOrder>* pres)
		{
			_CTP_GUARD();

			CThostFtdcQryTradeField  field = { 0 };
			m_api->ReqQryTrade(&field, m_ctpinfo.GetRequestID());

			return true;
		}

		void CCTPExchangeAdapter::UnInitialize()
		{
			_CTP_GUARD();
			_unInitialize();
		}

		bool CCTPExchangeAdapter::QueryAccountInfo(BasicAccountInfo* info)
		{
			_CTP_GUARD();

			_getAccountName(info);

			CThostFtdcQryTradingAccountField req1;
			strcpy_s(req1.BrokerID, m_param.BrokerID.c_str());
			strcpy_s(req1.InvestorID, m_param.usr.c_str());



			m_api->ReqQryTradingAccount(&req1, m_ctpinfo.GetRequestID());
			SyncData  data = m_handler->GetSyncReply(EventType::OnRspQryTradingAccount);



			CThostFtdcTradingAccountField* pdata = (CThostFtdcTradingAccountField*)data.data;
			info->ClientID = GB2TS(pdata->AccountID);
			info->BranchID = GB2TS(pdata->BrokerID);
			info->AssetBalance = pdata->Deposit;
			info->EnableBalance = pdata->Available;
			info->ExProvider = _T("ctp");

			return true;
		}

		bool CCTPExchangeAdapter::QueryPostions(std::list<PositionInfo>* pPoss, const TCHAR* pstock /*= NULL */)
		{
			_CTP_GUARD();
			std::string code = pstock == NULL ? "" : TS2GB(pstock);


			CThostFtdcQryInvestorPositionField  field = { 0 };

			CTPHelper::Assign(field.InstrumentID, code);
			strcpy_s(field.BrokerID, m_param.BrokerID.c_str());
			strcpy_s(field.InvestorID, m_param.usr.c_str());

			m_api->ReqQryInvestorPosition(&field, m_ctpinfo.GetRequestID());
			SyncData data = m_handler->GetSyncReply(EventType::OnRspQryInvestorPosition);
			if (data.responsetype != EventType::OnRspQryInvestorPosition)
			{
				return false;
			}
			if (data.rspinfo.ErrorID != 0)
			{
				return false;
			}

			pPoss->clear();
			std::vector<CThostFtdcInvestorPositionField> * vec = (std::vector<CThostFtdcInvestorPositionField> *)data.data;
			for (std::vector<CThostFtdcInvestorPositionField>::size_type i = 0; i < vec->size(); ++i)
			{
				CThostFtdcInvestorPositionField item = (*vec)[i];
				PositionInfo info = CTPHelper::ToPostionInfo(item);
				pPoss->push_back(info);
			}
			return true;



		}



		

		bool CCTPExchangeAdapter::GetDepMarket(const char* code, CThostFtdcDepthMarketDataField* pdata)
		{

			CThostFtdcQryDepthMarketDataField field = { 0 };
			strcpy_s(field.InstrumentID, code);
			m_api->ReqQryDepthMarketData(&field, m_ctpinfo.GetRequestID());

			SyncData data = m_handler->GetSyncReply(EventType::OnRspQryDepthMarketData);
			if (data.responsetype == EventType::OnRspQryDepthMarketData)
			{
				memcpy(pdata, data.data, sizeof(CThostFtdcDepthMarketDataField));
				std::cout << *pdata << std::endl;
				return true;
			}

			return false;
		}

		void CCTPExchangeAdapter::_unInitialize()
		{
			_CTP_GUARD();

			if (m_api)
			{
				m_api->RegisterSpi(NULL);
				m_api->Release(); //m_api was create by CThostFtdcTraderApi::CreateFtdcTraderApi
				m_api = NULL;
			}
			if (m_handler != NULL)
			{
				delete m_handler;
				m_handler = NULL;
			}
		}

		CCTPData* CCTPExchangeAdapter::GetCTPData()
		{
			return &m_ctpinfo;
		}

		//bool CCTPExchangeAdapter::GetQuote(const TCHAR* id, CThostFtdcDepthMarketDataField* pQuote)
		//{
		//	CThostFtdcDepthMarketDataField field = { 0 };
		//	bool ret = GetDepMarket(UCodeHelper::ToAS(id).c_str(), &field);
		//	if (!ret)
		//		return ret;
		//	else
		//	{
		//		pQuote->Code = UCodeHelper::ToTS(field.InstrumentID);
		//		pQuote->Time = CTPHelper::FromCTPTimeToInt(field.UpdateTime);
		//		pQuote->Open = field.OpenPrice;
		//		pQuote->High = field.HighestPrice;
		//		pQuote->Low = field.LowestPrice;
		//		pQuote->Close = field.LastPrice;
		//		pQuote->Vol = field.Volume;
		//		pQuote->Ask1 = field.AskPrice1;
		//		pQuote->Bid1 = field.BidPrice1;
		//		return true;
		//	}
		//}

		void CCTPExchangeAdapter::GetContractInfo(const TCHAR* code)
		{
			CThostFtdcQryInstrumentField field = { 0 };

			if (code == NULL)
			{
				CTPHelper::Assign(field.InstrumentID, "");
			}
			else
			{
				
				CTPHelper::Assign(field.ProductID , code);  //?
			}
			m_api->ReqQryInstrument(&field, m_ctpinfo.GetRequestID());
		}

		std::string CCTPExchangeAdapter::GetCurrentIF()
		{
			return CTPHelper::GetCurrentIFCode();
		}

		bool CCTPExchangeAdapter::QueryEntrustInfos(std::list<EntrustInfo>* pinfo)
		{

			return false;

		}

		bool CCTPExchangeAdapter::_updateAccountInfo(const CThostFtdcTradingAccountField* pInfo)
		{
			return true;
		}

		

		void CCTPExchangeAdapter::_getAccountName(BasicAccountInfo* info)
		{
			if (m_param.accountname != _T(""))
			{
				info->ClientName = m_param.accountname;
				return;
			}

			CThostFtdcQryInvestorField req = { 0 };

			strcpy_s(req.BrokerID, m_param.BrokerID.c_str());
			strcpy_s(req.InvestorID, m_param.usr.c_str());

			m_api->ReqQryInvestor(&req, m_ctpinfo.GetRequestID());

			SyncData  data1 = m_handler->GetSyncReply(EventType::OnRspQryInvestor);

			CThostFtdcInvestorField* pdata1 = (CThostFtdcInvestorField*)data1.data;
			info->ClientName = TS2GB(pdata1->InvestorName);
			m_param.accountname = info->ClientName;
		}

		tstring CCTPExchangeAdapter::GetName()
		{
			return _T("CTP Future Adapter");
		}

		CCTPExchangeAdapter::~CCTPExchangeAdapter(void)
		{
			UnInitialize();
		}

		bool CCTPExchangeAdapter::_checkfunctionreturn(int ret)
		{
			if (ret == -2)
			{
				SetLastError(MAKE_ERRORINFO ( EErrSystem::EQuoteAdapter, -2, _T("未处理请求超过许可数")) );
				
				return false;
			}
			if (ret == -3)
			{
				SetLastError(MAKE_ERRORINFO ( EErrSystem::EQuoteAdapter, -3, _T("每秒发送请求数超过许可数")) );
				return false;
			}
			return true;
		}


		tradeadapter::ctp::CTPParamA CTPParamA::FromCTPParam(const CTPParam& input)
		{
			CTPParamA ret;
			ret.addr = TS2GB(input.addr);
			ret.port = input.port;
			ret.usr = TS2GB(input.usr);
			ret.pwd = TS2GB(input.pwd);
			ret.BrokerID = TS2GB(input.BrokerID);
			ret.metafilepath = TS2GB(input.metafilepath);
			return ret;
		}

	};
};