#include "StdAfx.h"
#include "ucodehelper.h"
#include "ExchangeAdapterDef.h"
#include "ustringutils.h"
#include <iostream>
#include "CTPHelper.h"
#include "CTPMsgHandler.h"
#include "ustringutils.h"
#include "umaths.h"
#include "ucomhelper.h"
#include "CTPExchangeAdapter.h"
USING_LIBSPACE
#pragma warning(disable : 4482)


void tmplog(char*)
{}
void heartbeatlog(char*, int)
{}

namespace tradeadapter
{
	namespace ctp
	{
		CCTPMsgHandler::CCTPMsgHandler(CCTPExchangeAdapter* parent)
		{
			m_parentAdapter = parent;
		}

		CCTPMsgHandler::~CCTPMsgHandler(void)
		{
		}

		void CCTPMsgHandler::OnFrontConnected()
		{
			tmplog("OnFrontConnected be called, Next Login");
			SyncData data;
			data.responsetype = EventType::OnFrontConnected;
			m_datas.enqueue(data);
		}

		void CCTPMsgHandler::OnFrontDisconnected(int nReason)
		{
			tmplog("OnFrontDisconnected be called");
		}

		void CCTPMsgHandler::OnHeartBeatWarning(int nTimeLapse)
		{
		}

		void CCTPMsgHandler::OnRspAuthenticate(CThostFtdcRspAuthenticateField *pRspAuthenticateField, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{

			tmplog("OnRspAuthenticate be called ");
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO( EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}

		}

		void CCTPMsgHandler::OnRspUserLogin(CThostFtdcRspUserLoginField *pRspUserLogin, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{

			std::cout << "OnRspUserLogin becalled " << std::endl;
			if (pRspInfo == NULL && pRspInfo->ErrorID == 0)
			{
				this->m_parentAdapter->GetCTPData()->SetOrderStartID(UMaths::ToInt32(pRspUserLogin->MaxOrderRef));
			}

			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}



			EnqueueData(EventType::OnRspUserLogin, pRspUserLogin, sizeof(CThostFtdcRspUserLoginField), pRspInfo);
		}

		void CCTPMsgHandler::OnRspUserLogout(CThostFtdcUserLogoutField *pUserLogout, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{
			std::cout << "OnRspUserLogout becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
		}

		void CCTPMsgHandler::OnRspUserPasswordUpdate(CThostFtdcUserPasswordUpdateField *pUserPasswordUpdate, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{
			std::cout << " OnRspUserPasswordUpdate becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
		}

		void CCTPMsgHandler::OnRspTradingAccountPasswordUpdate(CThostFtdcTradingAccountPasswordUpdateField *pTradingAccountPasswordUpdate, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{
			std::cout << "OnRspTradingAccountPasswordUpdate  becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
		}

		void CCTPMsgHandler::OnRspOrderInsert(CThostFtdcInputOrderField *pInputOrder, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{

			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
			
			EnqueueData(EventType::OnRspOrderInsert, pInputOrder, sizeof(CThostFtdcInputOrderField), pRspInfo);
		}

		void CCTPMsgHandler::OnRspParkedOrderInsert(CThostFtdcParkedOrderField *pParkedOrder, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{
			std::cout << "OnRspParkedOrderInsert  becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
		}

		void CCTPMsgHandler::OnRspParkedOrderAction(CThostFtdcParkedOrderActionField *pParkedOrderAction, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{
			std::cout << "OnRspParkedOrderAction  becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
		}

		void CCTPMsgHandler::OnRspOrderAction(CThostFtdcInputOrderActionField *pInputOrderAction, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{
			std::cout << "OnRspOrderAction  becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
			std::cout << *pRspInfo << std::endl;
		}

		void CCTPMsgHandler::OnRspQueryMaxOrderVolume(CThostFtdcQueryMaxOrderVolumeField *pQueryMaxOrderVolume, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{
			std::cout << " OnRspQueryMaxOrderVolume becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
		}

		void CCTPMsgHandler::OnRspSettlementInfoConfirm(CThostFtdcSettlementInfoConfirmField *pSettlementInfoConfirm, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{
			std::cout << "OnRspSettlementInfoConfirm  becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
			EnqueueData(EventType::OnRspSettlementInfoConfirm, pSettlementInfoConfirm, sizeof(CThostFtdcSettlementInfoConfirmField), pRspInfo);
		}

		void CCTPMsgHandler::OnRspRemoveParkedOrder(CThostFtdcRemoveParkedOrderField *pRemoveParkedOrder, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{
			std::cout << " OnRspRemoveParkedOrder becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
		}

		void CCTPMsgHandler::OnRspRemoveParkedOrderAction(CThostFtdcRemoveParkedOrderActionField *pRemoveParkedOrderAction, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{
			std::cout << " OnRspRemoveParkedOrderAction becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
		}

		void CCTPMsgHandler::OnRspQryOrder(CThostFtdcOrderField *pOrder, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{

			std::cout << "OnRspQryOrder  becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
			EnqueueData(EventType::OnRspQryOrder, pOrder, sizeof(CThostFtdcOrderField), pRspInfo);
			//std::cout<<*pOrder<<std::endl;
		}

		void CCTPMsgHandler::OnRspQryTrade(CThostFtdcTradeField *pTrade, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{
			std::cout << "OnRspQryTrade  becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
		}




		void CCTPMsgHandler::OnRspQryInvestorPosition(CThostFtdcInvestorPositionField *pInvestorPosition, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{
			std::cout << " OnRspQryInvestorPosition becalled ��ѯ�ֲ���Ӧb_last" << bIsLast << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				EnqueueData(EventType::OnRspQryInvestorPosition, NULL, sizeof(CThostFtdcInvestorPositionField), pRspInfo);
				return;
			}
			if (m_CThostFtdcInvestorPositionFieldnRequestID != nRequestID)
			{
				m_vec.clear();
			}
			m_CThostFtdcInvestorPositionFieldnRequestID = nRequestID;
			if (pInvestorPosition != NULL)
			{
				m_vec.push_back(*pInvestorPosition);
			}
			if (bIsLast)
			{
				EnqueueData(EventType::OnRspQryInvestorPosition, &m_vec, sizeof(CThostFtdcInvestorPositionField), pRspInfo);
			}
		}

		void CCTPMsgHandler::OnRspQryTradingAccount(CThostFtdcTradingAccountField *pTradingAccount, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{
			std::cout << (pTradingAccount != NULL ? pTradingAccount->CloseProfit : 0) << std::endl;
			EnqueueData(EventType::OnRspQryTradingAccount, pTradingAccount, sizeof(CThostFtdcTradingAccountField), pRspInfo);

			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}

			//this->m_parentAdapter->_SetAccoutInfo(pTradingAccount);
			//pTradingAccount.
		}

		// ...cha xun zhang hu ji ben xin xi
		void CCTPMsgHandler::OnRspQryInvestor(CThostFtdcInvestorField *pInvestor, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{
			std::cout << " OnRspQryInvestor becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
			EnqueueData(EventType::OnRspQryInvestor, pInvestor, sizeof(CThostFtdcInvestorField), pRspInfo);
		}

		void CCTPMsgHandler::OnRspQryTradingCode(CThostFtdcTradingCodeField *pTradingCode, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{
			std::cout << " OnRspQryTradingCode becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
		}

		void CCTPMsgHandler::OnRspQryInstrumentMarginRate(CThostFtdcInstrumentMarginRateField *pInstrumentMarginRate, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{
			std::cout << "OnRspQryInstrumentMarginRate  becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
		}

		void CCTPMsgHandler::OnRspQryInstrumentCommissionRate(CThostFtdcInstrumentCommissionRateField *pInstrumentCommissionRate, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{
			std::cout << "OnRspQryInstrumentCommissionRate  becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
		}

		void CCTPMsgHandler::OnRspQryExchange(CThostFtdcExchangeField *pExchange, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{
			std::cout << " OnRspQryExchange becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
		}


		///comment: if can not find the the product item ,the pInstrument is NULL.
		void CCTPMsgHandler::OnRspQryInstrument(CThostFtdcInstrumentField *pInstrument, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{

			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}

			static int reqid = 0;
			//this is a new request return's data.
			if (reqid != nRequestID)
			{
				CTPHelper::products.clear();
				reqid = nRequestID;
			}
			if(pInstrument == NULL)
			{
				return;
			}
			CTPHelper::products.push_back(*pInstrument);

		}

		void CCTPMsgHandler::OnRspQryDepthMarketData(CThostFtdcDepthMarketDataField *pDepthMarketData, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{
			std::cout << "OnRspQryDepthMarketData  becalled " << std::endl;
			EnqueueData(EventType::OnRspQryDepthMarketData, pDepthMarketData, sizeof(CThostFtdcDepthMarketDataField), pRspInfo);
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}

		}

		void CCTPMsgHandler::OnRspQrySettlementInfo(CThostFtdcSettlementInfoField *pSettlementInfo, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{
			std::cout << " OnRspQrySettlementInfo becalled " << std::endl;
			EnqueueData(EventType::OnRspQrySettlementInfo, pSettlementInfo, sizeof(CThostFtdcSettlementInfoField), pRspInfo);
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}

		}

		void CCTPMsgHandler::OnRspQryTransferBank(CThostFtdcTransferBankField *pTransferBank, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{
			std::cout << "OnRspQryTransferBank  becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
		}

		void CCTPMsgHandler::OnRspQryInvestorPositionDetail(CThostFtdcInvestorPositionDetailField *pInvestorPositionDetail, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{
			std::cout << " OnRspQryInvestorPositionDetail becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
		}

		void CCTPMsgHandler::OnRspQryNotice(CThostFtdcNoticeField *pNotice, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{
			std::cout << "OnRspQryNotice  becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
		}

		void CCTPMsgHandler::OnRspQrySettlementInfoConfirm(CThostFtdcSettlementInfoConfirmField *pSettlementInfoConfirm, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{
			std::cout << "OnRspQrySettlementInfoConfirm  becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
		}

		void CCTPMsgHandler::OnRspQryInvestorPositionCombineDetail(CThostFtdcInvestorPositionCombineDetailField *pInvestorPositionCombineDetail, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{
			std::cout << "OnRspQryInvestorPositionCombineDetail  becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
		}

		void CCTPMsgHandler::OnRspQryCFMMCTradingAccountKey(CThostFtdcCFMMCTradingAccountKeyField *pCFMMCTradingAccountKey, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{
			std::cout << "OnRspQryCFMMCTradingAccountKey  becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
		}

		void CCTPMsgHandler::OnRspQryEWarrantOffset(CThostFtdcEWarrantOffsetField *pEWarrantOffset, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{
			std::cout << " OnRspQryEWarrantOffset becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
		}

		void CCTPMsgHandler::OnRspQryTransferSerial(CThostFtdcTransferSerialField *pTransferSerial, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{
			std::cout << "OnRspQryTransferSerial  becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
		}

		void CCTPMsgHandler::OnRspQryAccountregister(CThostFtdcAccountregisterField *pAccountregister, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{
			std::cout << " OnRspQryAccountregister becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
		}

		void CCTPMsgHandler::OnRspError(CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{
			std::cout << "OnRspError  becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
		}



		void CCTPMsgHandler::OnRtnOrder(CThostFtdcOrderField *pOrder)
		{

			std::cout << "OnRtnOrder  becalled " << std::endl;

			std::cout << *pOrder << std::endl;
			EnqueueData(EventType::OnRtnOrder, pOrder, sizeof(CThostFtdcOrderField), NULL);

		}

		void CCTPMsgHandler::OnRtnTrade(CThostFtdcTradeField *pTrade)
		{
			std::cout << " OnRtnTrade becalled " << std::endl;
		}

		void CCTPMsgHandler::OnErrRtnOrderInsert(CThostFtdcInputOrderField *pInputOrder, CThostFtdcRspInfoField *pRspInfo)
		{
			std::cout << " OnErrRtnOrderInsert becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
		}

		void CCTPMsgHandler::OnErrRtnOrderAction(CThostFtdcOrderActionField *pOrderAction, CThostFtdcRspInfoField *pRspInfo)
		{
			std::cout << "OnErrRtnOrderAction  becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
		}

		void CCTPMsgHandler::OnRtnInstrumentStatus(CThostFtdcInstrumentStatusField *pInstrumentStatus)
		{
			std::cout << " OnRtnInstrumentStatus becalled " << std::endl;
		}

		void CCTPMsgHandler::OnRtnTradingNotice(CThostFtdcTradingNoticeInfoField *pTradingNoticeInfo)
		{
			std::cout << " OnRtnTradingNotice becalled " << std::endl;
		}

		void CCTPMsgHandler::OnRtnErrorConditionalOrder(CThostFtdcErrorConditionalOrderField *pErrorConditionalOrder)
		{
			std::cout << " OnRtnErrorConditionalOrder becalled " << std::endl;
		}

		void CCTPMsgHandler::OnRspQryContractBank(CThostFtdcContractBankField *pContractBank, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{
			std::cout << " OnRspQryContractBank becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
		}

		void CCTPMsgHandler::OnRspQryParkedOrder(CThostFtdcParkedOrderField *pParkedOrder, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{
			std::cout << "OnRspQryParkedOrder  becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
		}

		void CCTPMsgHandler::OnRspQryParkedOrderAction(CThostFtdcParkedOrderActionField *pParkedOrderAction, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{
			std::cout << " OnRspQryParkedOrderAction becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter, pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
		}

		void CCTPMsgHandler::OnRspQryTradingNotice(CThostFtdcTradingNoticeField *pTradingNotice, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{
			std::cout << " OnRspQryTradingNotice becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
		}

		void CCTPMsgHandler::OnRspQryBrokerTradingParams(CThostFtdcBrokerTradingParamsField *pBrokerTradingParams, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{
			std::cout << " OnRspQryBrokerTradingParams becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
		}

		void CCTPMsgHandler::OnRspQryBrokerTradingAlgos(CThostFtdcBrokerTradingAlgosField *pBrokerTradingAlgos, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{
			std::cout << " OnRspQryBrokerTradingAlgos becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
		}

		void CCTPMsgHandler::OnRtnFromBankToFutureByBank(CThostFtdcRspTransferField *pRspTransfer)
		{
			std::cout << " OnRtnFromBankToFutureByBank becalled " << std::endl;
		}

		void CCTPMsgHandler::OnRtnFromFutureToBankByBank(CThostFtdcRspTransferField *pRspTransfer)
		{
			std::cout << "OnRtnFromFutureToBankByBank  becalled " << std::endl;
		}

		void CCTPMsgHandler::OnRtnRepealFromBankToFutureByBank(CThostFtdcRspRepealField *pRspRepeal)
		{
			std::cout << "OnRtnRepealFromBankToFutureByBank  becalled " << std::endl;
		}

		void CCTPMsgHandler::OnRtnRepealFromFutureToBankByBank(CThostFtdcRspRepealField *pRspRepeal)
		{
			std::cout << "OnRtnRepealFromFutureToBankByBank  becalled " << std::endl;
		}

		void CCTPMsgHandler::OnRtnFromBankToFutureByFuture(CThostFtdcRspTransferField *pRspTransfer)
		{
			std::cout << "OnRtnFromBankToFutureByFuture  becalled " << std::endl;
		}

		void CCTPMsgHandler::OnRtnFromFutureToBankByFuture(CThostFtdcRspTransferField *pRspTransfer)
		{
			std::cout << " OnRtnFromFutureToBankByFuture becalled " << std::endl;
		}

		void CCTPMsgHandler::OnRtnRepealFromBankToFutureByFutureManual(CThostFtdcRspRepealField *pRspRepeal)
		{
			std::cout << " OnRtnRepealFromBankToFutureByFutureManual becalled " << std::endl;
		}

		void CCTPMsgHandler::OnRtnRepealFromFutureToBankByFutureManual(CThostFtdcRspRepealField *pRspRepeal)
		{
			std::cout << "OnRtnRepealFromFutureToBankByFutureManual  becalled " << std::endl;
		}

		void CCTPMsgHandler::OnRtnQueryBankBalanceByFuture(CThostFtdcNotifyQueryAccountField *pNotifyQueryAccount)
		{
			std::cout << "OnRtnQueryBankBalanceByFuture  becalled " << std::endl;
		}

		void CCTPMsgHandler::OnErrRtnBankToFutureByFuture(CThostFtdcReqTransferField *pReqTransfer, CThostFtdcRspInfoField *pRspInfo)
		{
			std::cout << " OnErrRtnBankToFutureByFuture becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
		}

		void CCTPMsgHandler::OnErrRtnFutureToBankByFuture(CThostFtdcReqTransferField *pReqTransfer, CThostFtdcRspInfoField *pRspInfo)
		{
			std::cout << "OnErrRtnFutureToBankByFuture  becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
		}

		void CCTPMsgHandler::OnErrRtnRepealBankToFutureByFutureManual(CThostFtdcReqRepealField *pReqRepeal, CThostFtdcRspInfoField *pRspInfo)
		{
			std::cout << "OnErrRtnRepealBankToFutureByFutureManual  becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
		}

		void CCTPMsgHandler::OnErrRtnRepealFutureToBankByFutureManual(CThostFtdcReqRepealField *pReqRepeal, CThostFtdcRspInfoField *pRspInfo)
		{
			std::cout << "OnErrRtnRepealFutureToBankByFutureManual  becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
		}

		void CCTPMsgHandler::OnErrRtnQueryBankBalanceByFuture(CThostFtdcReqQueryAccountField *pReqQueryAccount, CThostFtdcRspInfoField *pRspInfo)
		{
			std::cout << " OnErrRtnQueryBankBalanceByFuture becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
		}

		void CCTPMsgHandler::OnRtnRepealFromBankToFutureByFuture(CThostFtdcRspRepealField *pRspRepeal)
		{
			std::cout << "OnRtnRepealFromBankToFutureByFuture  becalled " << std::endl;
		}

		void CCTPMsgHandler::OnRspFromBankToFutureByFuture(CThostFtdcReqTransferField *pReqTransfer, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{
			std::cout << " OnRspFromBankToFutureByFuture becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
		}

		void CCTPMsgHandler::OnRspFromFutureToBankByFuture(CThostFtdcReqTransferField *pReqTransfer, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{
			std::cout << " OnRspFromFutureToBankByFuture becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
		}

		void CCTPMsgHandler::OnRspQueryBankAccountMoneyByFuture(CThostFtdcReqQueryAccountField *pReqQueryAccount, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
		{
			std::cout << "OnRspQueryBankAccountMoneyByFuture  becalled " << std::endl;
			if (pRspInfo != NULL && pRspInfo->ErrorID != 0)
			{
				m_parentAdapter->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,pRspInfo->ErrorID, pRspInfo->ErrorMsg));
			}
		}

		void CCTPMsgHandler::OnRtnOpenAccountByBank(CThostFtdcOpenAccountField *pOpenAccount)
		{
			std::cout << " OnRtnOpenAccountByBank becalled " << std::endl;
		}

		void CCTPMsgHandler::OnRtnCancelAccountByBank(CThostFtdcCancelAccountField *pCancelAccount)
		{
			std::cout << "OnRtnCancelAccountByBank  becalled " << std::endl;
		}

		void CCTPMsgHandler::OnRtnChangeAccountByBank(CThostFtdcChangeAccountField *pChangeAccount)
		{
			std::cout << "OnRtnChangeAccountByBank  becalled " << std::endl;
		}

		void CCTPMsgHandler::OnRtnRepealFromFutureToBankByFuture(CThostFtdcRspRepealField *pRspRepeal)
		{
			std::cout << "OnRtnRepealFromFutureToBankByFuture  becalled " << std::endl;
		}

		SyncData CCTPMsgHandler::GetSyncReply(EventType eventtype, int timeout)
		{
			std::vector<EventType> types;
			types.push_back(eventtype);
			return GetReplys(types, timeout);

		}



		bool CCTPMsgHandler::EnqueueData(EventType evtype, void* pdata, int sizedata, CThostFtdcRspInfoField* prsp)
		{
			SyncData data;
			data.responsetype = evtype;
			memcpy(data.data, pdata, sizedata);
			if (prsp != NULL)
			{
				data.rspinfo = *prsp;
			}
			else
			{
				data.rspinfo.ErrorID = 0;
				CTPHelper::Assign(data.rspinfo.ErrorMsg, "");
			}

			m_datas.enqueue(data);
			return true;
		}

		bool inrange(EventType type, const std::vector<EventType>& datas)
		{
			return std::find(datas.begin(), datas.end(), type) != datas.end();
		}
		tradeadapter::ctp::SyncData CCTPMsgHandler::GetReplys(std::vector<EventType> types, int timeout /*= 10*1000*/)
		{
			while (true)
			{
				try
				{
					SyncData data = m_datas.dequeue(timeout);
					if (!inrange(data.responsetype, types))
					{
						continue;
					}
					else
					{
						return data;
					}
				}
				catch (TimeOutException&)
				{
					SyncData data;
					data.responsetype = EventType::TimeOut;
					return data;
				}
				catch (CloseException&)
				{
					SyncData data;
					data.responsetype = EventType::SysClose;
					return data;
				}
			}
		}



		SyncData::SyncData()
		{
			this->responsetype = NoDefined;
			memset(data, 0, sizeof(char) * 1024 * 4);
		}
	}
}