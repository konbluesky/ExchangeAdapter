#include "stdafx.h"
#include "ucodehelper.h"
#include "ExchangeAdapterDef.h"
#include "ustringutils.h"
#pragma warning(disable : 4482)
#include "ThostFtdcUserApiStruct.h"
#include "CTPHelper.h"
#include "texceptionex.h"
USING_LIBSPACE
namespace tradeadapter
{


	namespace ctp
	{
		std::ostream& operator<<(std::ostream& os, const CThostFtdcRspInfoField& data)
		{
			os << "错误:" << data.ErrorID << "   " << data.ErrorMsg << std::endl;
			return os;
		}

		std::ostream& operator<<(std::ostream& os, const CThostFtdcDepthMarketDataField& data)
		{

			os << "接收到报价" << std::endl <<
				"OpenPrice		:" << data.OpenPrice << std::endl <<
				"HighestPrice	:" << data.HighestPrice << std::endl <<
				"LowestPrice	:" << data.LowestPrice << std::endl <<
				"ClosePrice		:" << data.ClosePrice << std::endl <<
				"Volume			:" << data.Volume << std::endl <<
				"SettlementPrice:" << data.SettlementPrice << std::endl <<
				"LastPrice		:" << data.LastPrice << std::endl <<
				"UpdateTime		:" << data.UpdateTime << std::endl <<
				"AskPrice1		:" << data.AskPrice1 << std::endl <<
				"AskVolume1		:" << data.AskVolume1 << std::endl <<
				"BidPrice1		:" << data.BidPrice1 << std::endl <<
				"BidVolume1		:" << data.BidVolume1 << std::endl <<
				"AveragePrice	:" << data.AveragePrice << std::endl;
			return os;
		}
		std::ostream& operator<<(std::ostream& os, const CThostFtdcOrderField& data)
		{
			os << "接收到挂单返回:" << std::endl;

			os << data.StatusMsg << std::endl;
			return os;

		}



		TThostFtdcDirectionType CTPHelper::ToTFtdcDirectionType(EBuySell side)
		{
			switch (side)
			{
			case EBuySell::eBuy:
				return THOST_FTDC_D_Buy;
			case EBuySell::eSell:
				return THOST_FTDC_D_Sell;
			default:
				throw TException(_T("错误的类型"));
			}
		}

		TThostFtdcOffsetFlagType CTPHelper::ToTThostFtdcOffsetFlagType(EOpenClose offset)
		{
			switch (offset)
			{
			case EOpenClose::eOpen: return THOST_FTDC_OF_Open;
			case EOpenClose::eClose: return THOST_FTDC_OF_Close;
			case EOpenClose::eCloseToday: return THOST_FTDC_OF_CloseToday;
			case EOpenClose::eCloseYestoday: return THOST_FTDC_OF_CloseYesterday;
			case EOpenClose::eForceClose: return THOST_FTDC_OF_ForceClose;
			default:
				throw TException(_T("错误的类型"));
			}
		}

		TThostFtdcOrderPriceTypeType CTPHelper::ToOrderPriceType(EPriceMode mode)
		{
			switch (mode)
			{
			case EPriceMode::eLimitPrice:
				return THOST_FTDC_OPT_LimitPrice;
			case EPriceMode::eMarketPrice:
				return THOST_FTDC_OPT_AnyPrice;
			default:
				throw TException(_T("错误的类型"));
			}
		}

		TThostFtdcHedgeFlagType CTPHelper::ToTThostFtdcHedgeFlagType(EHedgeFlag hedgeFlag)
		{
			switch (hedgeFlag)
			{
			case EHedgeFlag::eSpeculation: return THOST_FTDC_HF_Speculation;
			case EHedgeFlag::eHedge: return THOST_FTDC_HF_Hedge;
			default:
				throw TException(_T("错误的类型"));
			}
		}

		/*TThostFtdcHedgeFlagType CTPHelper::ToTThostFtdcHedgeFlagType( int i )
		{
		if(i == 1)
		return TThostFtdcHedgeFlagType::TThostFtdcHedgeFlagType_THOST_FTDC_HF_Speculation;
		else if(i == 3)
		return TThostFtdcHedgeFlagType::TThostFtdcHedgeFlagType_THOST_FTDC_HF_Hedge;
		else
		throw TException(_T("错误的类型"));
		}
		}*/

		TThostFtdcTimeConditionType CTPHelper::ToTThostFtdcTimeConditionType(EPriceMode mode)
		{
			switch (mode)
			{
			case EPriceMode::eMarketPrice:
				return THOST_FTDC_TC_IOC;
			default:
				return THOST_FTDC_TC_GFD;
			}
		}

		void CTPHelper::Assign(char* pbuff, char data)
		{
			pbuff[0] = data;
			pbuff[1] = '\0';
		}

		void CTPHelper::Assign(char* pbuff, const char* data)
		{
			strcpy(pbuff, data);
		}

		void CTPHelper::Assign(char* pbuff, const std::string& data)
		{
			Assign(pbuff, data.c_str());
		}

		void CTPHelper::Assign(char* buff, const std::wstring& data)
		{
			std::string val = UCodeHelper::UnicodeToGB2312(data.c_str());
			Assign(buff, val);
		}

		void CTPHelper::Assign(char* buff, int id)
		{
			sprintf(buff, "%d", id);
		}

		void CTPHelper::Assign(char* buff, int id, int num, int fillchar)
		{
			int len = intlen(id);

			for (int i = 0; i < num - len; ++i)
			{
				buff[i] = (char)fillchar;
			}
			Assign(&buff[num - len], id);
		}

		EPosSide CTPHelper::FromTThostFtdcPosiDirection(TThostFtdcPosiDirectionType direction)
		{
			switch (direction)
			{
			case THOST_FTDC_PD_Long:
				return EPosSide::eMore;
			case THOST_FTDC_PD_Short:
				return EPosSide::eShort;
			case THOST_FTDC_PD_Net:
				return EPosSide::eClear;
			default:
				throw TException(_T("不会有这种情况"));
			}
		}

		tradeadapter::PositionInfo CTPHelper::ToPostionInfo(const  CThostFtdcInvestorPositionField& field)
		{
			PositionInfo info;
			info.code = GB2TS(field.InstrumentID);
			info.hand_flag = ETradeUnit::eHand;
			info.datetype = field.PositionDate == (char)'1' ? EPositionDateType::eUseHistory : EPositionDateType::eNoUseHistory;
			info.current_amount = field.Position;
			info.enable_amount = field.Position;
			info.total_amount = field.Position;
			info.total_diaableamount = 0;//field.CloseVolume;
			info.posside = CTPHelper::FromTThostFtdcPosiDirection(field.PosiDirection);

			return info;
		}



		bool CCTPData::GetSettled()
		{
			threadlock::autoguard<threadlock::multi_threaded_local> g(&m_lock);
			Load(&m_data);
			bool settled = m_data.m_settled;
			if (settled)
			{
				return settled;
			}
			else
			{
				m_data.m_settled = true;
				Save(m_data);
				return false;
			}
		}


		int CCTPData::GetOrderID()
		{
			InterlockedIncrement(&m_data.m_orderid);
			return m_data.m_orderid;
		}

		int CCTPData::GetRequestID()
		{
			InterlockedIncrement(&m_data.m_requestid);
			return m_data.m_requestid;
		}

		void CCTPData::SetOrderStartID(int val)
		{
			threadlock::autoguard<threadlock::multi_threaded_local> g(&m_lock);
			m_data.m_orderid = val;
		}


		CTPMeta::CTPMeta()
		{
			m_orderid = UTimeUtils::GetNowTime() * 1000;
			m_settled = false;
			m_requestid = UTimeUtils::GetNowTime() * 1000;
		}

		std::vector<CThostFtdcInstrumentField > CTPHelper::products;



		std::string CTPHelper::GetCurrentIFCode()
		{
			static std::string code;
			if (code != "")
				return code;

			std::string procode;

			for (unsigned int i = 0; i < products.size(); ++i)
			{
				procode = products[i].InstrumentID;
				if (procode.substr(0, 2) == "IF")
				{
					if (code == "" || code > procode)
					{
						code = procode;
					}
				}
			}
			return code;
		}

		int CTPHelper::intlen(int val)
		{
			int len = 0;
			while (val > 0)
			{
				len++;
				val = val / 10;
			}
			return len;
		}

		

		
		

	}
}