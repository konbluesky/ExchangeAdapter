#pragma once
#include <map>
#include "stdafx.h"
#include "IOrderService.h"
#include "ITradeAdapter.h"
#include "threadlock.h"
#include "CTPHelper.h"
#include "IConfigSetting.h"


class CThostFtdcTraderApi;
struct CThostFtdcDepthMarketDataField;

namespace tradeadapter
{

	namespace ctp
	{


		struct CTPParam
		{
			tstring addr;
			int			port;
			tstring usr;
			tstring pwd;
			tstring BrokerID;
			//std::string currentindex;
			tstring metafilepath;


			EHedgeFlag hedge;
		public:
			CTPParam()
			{
				memset(this, 0, sizeof(CTPParam));
			}
			/*static std::pair<bool, CTPParam> LoadFromXML(const TCHAR* pPath)
			{
			LIB_NS::TConfigSetting setting;
			setting.LoadConfig(pPath);
			}*/
		};



		struct CTPParamA
		{
			std::string addr;
			int			port;
			std::string usr;
			std::string pwd;
			std::string BrokerID;
			//std::string currentindex;
			std::string metafilepath;


			std::string FrontID; // this get from login server return.



			std::string SessionID; //this get from login server return.


			int MaxOrderRef;//this get from login server return.


			tstring accountname;


			EHedgeFlag hedge;
		public:
			CTPParamA()
			{
				memset(this, 0, sizeof(CTPParamA));
			}

			static CTPParamA FromCTPParam(const CTPParam& input);
		};


		//ctp的所有操作，首先从缓存获取结果，没有的话，则同步的获取结果。
		//for ctp trade, it's based on async mode but also we need fastest speed in trade.
		struct CTPDataCache
		{
			tstring accontname;   // this value get from login job,
			std::list<DealedOrder> m_dealedorder;

		};
		class CCTPMsgHandler;

		class TAIAPI CCTPExchangeAdapter :public ITradeAdapter
		{
		public:
			CCTPExchangeAdapter(void);

			~CCTPExchangeAdapter(void);

			virtual bool Initialize(void* param = NULL);

			virtual bool IsInitialized();

			virtual bool InitializeByXML(const TCHAR* fpath);

			virtual bool EntrustOrder(Order order, OrderID* pOutID);

			virtual bool CancelOrder(OrderID orderid);

			virtual bool QueryOrder(OrderID id, int* pCount);

			virtual bool QueryDealedOrders(std::list<DealedOrder>* pres);

			virtual void UnInitialize();

			virtual bool QueryAccountInfo(BasicAccountInfo* info);

			virtual bool QueryPostions(std::list<PositionInfo>* pPoss, const TCHAR* pstock = NULL);

			

			CCTPData* GetCTPData();


			std::string GetCurrentIF();

			


			void GetContractInfo(const TCHAR* code = NULL);

			bool _updateAccountInfo(const CThostFtdcTradingAccountField* pInfo);

			bool _checkfunctionreturn(int ret);

#pragma warning(disable : 4251)


		private:
			void _getAccountName(BasicAccountInfo* info);
			bool GetDepMarket(const char* code, CThostFtdcDepthMarketDataField* pdata);
			void _unInitialize();

			virtual bool QueryEntrustInfos(std::list<EntrustInfo>* pinfo);

			virtual tstring GetName();



#pragma warning(disable : 4251)
			threadlock::multi_threaded_local m_lock;
#define  _CTP_GUARD() threadlock::autoguard<threadlock::multi_threaded_local> guard(&m_lock)
			CThostFtdcTraderApi* m_api;

			CCTPMsgHandler * m_handler;


			CTPParamA m_param;

			CCTPData m_ctpinfo;
			bool m_initialized;

			

			CTPDataCache m_ctpcache;


		};

	};
};