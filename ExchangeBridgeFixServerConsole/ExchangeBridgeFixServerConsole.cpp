#include "stdafx.h"
#include "ProfileTest.h"
#include "GlobalTest.h"
#define TIXML_USE_STL
#include "tinyxml.h"

int _tmain(int /*argc*/, tchar* /*argv[]*/)
{
	const tchar *pstarttype = "starttype";
	int8 choice = 'q';

	TiXmlDocument doc("Common.xml");
	if (!doc.LoadFile())
		return -1;

	/* 获取启动的类型 */
	TiXmlElement *pRootElement = NULL,*pTmpElement = NULL;
	pRootElement = doc.RootElement();
	pTmpElement = pRootElement->FirstChildElement(pstarttype);
	if (NULL != pTmpElement){
		tstring strtype = pTmpElement->GetText();
		if ("trader" == strtype){
			std::cout<<"Config starting type:trader"<<std::endl
				<<"Exit:press 'q':"<<std::endl;
			choice = '1';
		} else if("market" == strtype){
			std::cout<<"Config starting type:market"<<std::endl
				<<"Exit:press 'q':"<<std::endl;
			choice = '2';
		} else {
			std::cout<<"Please give type:"<<std::endl<<
				"[1]:GloTrader Trader"<<std::endl<<
				"[2]:GloTrader Market"<<std::endl<<
				"[3]:function test(Not supported temporarily)"<<std::endl<<
				"[4]:Performance test(Not supported temporarily)"<<std::endl<<
				"[q]:Exit test"<<std::endl<<
				":";
			std::cin>>choice;
		}
	}	
	
	/* 根据配置启动功能 */
	if('1' == choice) {
		std::cout<<"Start trading"<<std::endl;
		return TestGlobal();
	} else if('2' == choice){
		std::cout<<"Start market data"<<std::endl;
		return TestGlobalMarket();
	} else if('3' == choice) {
		std::cout<<"Not supported temporarily"<<std::endl;
		tchar itmp;
		std::cin>>itmp;
		return -1;
	} else if('4' == choice) {
		std::cout<<"Start test performance"<<std::endl;
		return TestProfile();	
	} else if('q' == choice) {
		std::cout<<"Exit"<<std::endl;
		return 0;
	}
}