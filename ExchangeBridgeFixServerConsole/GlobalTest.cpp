#include "stdafx.h"
#include "GlobalTest.h"
#include <cassert>

int32 TestGlobal()
{
	RMMS_NS::RMMSTraderRouter gate;
	
	try	{
		/* 初始化参数 */
		if(0 > gate.Init())
			return -1;

		/* 启动服务器 */
		if(0 > gate.Start())
			return -1;

		std::cout<<"Start server success..."<<std::endl;
		std::cout<<"Press 'q' for exit."<<std::endl;
		while('q' != std::getchar());

		gate.Stop();
		gate.Uninit();
	}catch(...){
		std::printf("!!!!Exception");
		assert(0);
	}

	return 0;
}

int32 TestGlobalMarket(void)
{
	RMMS_NS::RMMSMarketRouter gate;

	/* 初始化参数 */
	if(0 > gate.Init())
		return -1;

	/* 启动服务器 */
	if(0 > gate.Start())
		return -1;

	std::cout<<"Start server success..."<<std::endl;
	std::cout<<"Press 'q' for exit."<<std::endl;
	while('q' != std::getchar());

	gate.Stop();
	gate.Uninit();

	return 0;
}
