// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#define RMMS_EXCHANGE_ADAPTER_USE_DLL

#include "targetver.h"

#include "RMMSMarketRouter.h"
#include "RMMSTraderRouter.h"
#include "DateTime.h"
