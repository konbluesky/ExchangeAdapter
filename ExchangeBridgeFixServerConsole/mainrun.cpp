#include "stdafx.h"
#define TIXML_USE_STL
#include "tinyxml.h"
#include "Directory.h"
#include "udll.h"
#include "ChainMarketDataProcessor.h"
#include "FixApplication.h"
#include "FIXMarketDataProcessor.h"
#include "ConsolePrintMarketDataProcessor.h"

#pragma warning(disable:4189)

FixApplication* papp;
typedef void* CreateMarketDataAdapterFunc();
void StartAdapters(std::vector<std::string>& adapters)
{
	IMarketDataProcessor& processor = 	FIXMarketDataProcessor::Instance();

	((ChainMarketDataProcessor*)&ChainMarketDataProcessor::Instance())->AddProcessor(&ConsolePrintMarketDataProcessor::Instance());
	((ChainMarketDataProcessor*)&ChainMarketDataProcessor::Instance())->AddProcessor(&processor);
	((ConsolePrintMarketDataProcessor*)&ConsolePrintMarketDataProcessor::Instance())->SetSimpleInfo(true);

	// TODO: Add your control notification handler code here


	for (std::vector<std::string>::iterator it = adapters.begin(); it != adapters.end(); ++it)
	{

		std::string dllname = *it + ".dll";
		std::string confname= *it + ".market.xml";

		LIB_NS::UDLLUtils::HLIB hlib =  LIB_NS::UDLLUtils::DllOpenLibrary(dllname.c_str());
		if (hlib == NULL)
		{
			::MessageBox(NULL,"not loaded the dll------","",MB_OK);

		}
		CreateMarketDataAdapterFunc* pfunc = (CreateMarketDataAdapterFunc*)LIB_NS::UDLLUtils::DllGetFunc(hlib,"CreateMarketDataProvider");
		if (pfunc != NULL)
		{
			IMarketDataProvider* pmaketprovider = (IMarketDataProvider*) pfunc();
			pmaketprovider->SetMarketDataProcessor(&ChainMarketDataProcessor::Instance());
			if(!	pmaketprovider->InitializeByXML(confname.c_str()))
			{
				::MessageBox(NULL,"InitializeByXML failed","",MB_OK);
			}
		}
		else
		{
			::MessageBox(NULL,"not loaded the dll","",MB_OK);
		}
	}
}


void StartFix()
{
	// TODO: Add your control notification handler code here
	LIB_NS::UDirectory::SetAppDirectory();
	papp = new FixApplication;
	papp->Initialize("RmmDomesticPrice_AcceptorSession.txt");
	papp->Start();
	papp->m_process =(FIXMarketDataProcessor *)&	FIXMarketDataProcessor::Instance();
}

void Split(std::vector<tstring>* pdest, const tstring& data, const tstring& delimiter )
{
	int begin = 0;			
	int index = data.find_first_of (delimiter);
	while (index != tstring::npos)
	{
		tstring tmp = data.substr (begin,index-begin);
		pdest->push_back (tmp);
		begin = index+delimiter.size();
		index = data.find_first_of (delimiter,begin);
	}
	if (!data.empty ())
	{
		tstring tmp = data.substr (begin);
		pdest->push_back (tmp);
	}
}

std::pair<bool,std::vector<std::string>> GetMarketProviderFromConf(const char* fname)
{
	std::vector<std::string> ret;
	TiXmlDocument doc(fname);
	bool loadOkay = doc.LoadFile();
	if (!loadOkay)
	{
		return std::make_pair(false,ret);
	}
	TiXmlNode* pNode = doc.FirstChild("configure");


	TiXmlNode* ele;

	ele= pNode->FirstChild("marketproviders");
	std::string stringdata =  ele->FirstChild()->Value();

	Split(&ret,stringdata,",");

	return std::make_pair(true,ret);

}


void RunServer()
{
	LIB_NS::UDirectory::SetAppDirectory();
	StartFix();
	std::pair<bool,std::vector<std::string>> info = GetMarketProviderFromConf("ExchangeBridgeFixServerConsole.conf.xml");

	FIXMarketDataProcessor* pProcessor = 	(FIXMarketDataProcessor*)(&FIXMarketDataProcessor::Instance());
	/*while(!pProcessor->IsSessionAvailable())
	{
		::Sleep(10*1000);		
	}*/
	if (info.first)
	{
		StartAdapters(info.second);
	}
	else
	{
		printf("Load config file failed");
	}
	
}
