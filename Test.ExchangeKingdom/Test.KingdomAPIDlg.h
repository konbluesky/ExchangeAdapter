// Test.KingdomAPIDlg.h : header file
//

#pragma once


// CTestKingdomAPIDlg dialog
class CTestKingdomAPIDlg : public CDialog
{
// Construction
public:
	CTestKingdomAPIDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_TESTKINGDOMAPI_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedInilize();
	afx_msg void OnBnClickedLogin();
	afx_msg void OnBnClickedButton7();
	afx_msg void OnBnClickedButton8();
	afx_msg void OnBnClickedQueryServerStatus();
	afx_msg void OnBnClickedPutOrder();
	afx_msg void OnBnClickedButton4();
	afx_msg void OnBnClickedButton10();
};
