// Test.KingdomAPIDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ustringutils.h"
#include "Test.KingdomAPI.h"
#include "Test.KingdomAPIDlg.h"
#include <iostream>



#include "..\ExchangeAdapter.Kingdom\KingdomTradeAdapter.h"
using namespace std;
using namespace tradeadapter::kingdom;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CKingdomTradeAdapter adp;
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CTestKingdomAPIDlg dialog




CTestKingdomAPIDlg::CTestKingdomAPIDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTestKingdomAPIDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CTestKingdomAPIDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CTestKingdomAPIDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDOK, &CTestKingdomAPIDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON1, &CTestKingdomAPIDlg::OnBnClickedInilize)
	ON_BN_CLICKED(IDC_BUTTON2, &CTestKingdomAPIDlg::OnBnClickedLogin)
	ON_BN_CLICKED(IDC_BUTTON7, &CTestKingdomAPIDlg::OnBnClickedButton7)
	ON_BN_CLICKED(IDC_BUTTON8, &CTestKingdomAPIDlg::OnBnClickedButton8)
	ON_BN_CLICKED(IDC_BUTTON9, &CTestKingdomAPIDlg::OnBnClickedQueryServerStatus)
	ON_BN_CLICKED(IDC_BUTTON3, &CTestKingdomAPIDlg::OnBnClickedPutOrder)
	ON_BN_CLICKED(IDC_BUTTON4, &CTestKingdomAPIDlg::OnBnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON10, &CTestKingdomAPIDlg::OnBnClickedButton10)
END_MESSAGE_MAP()


// CTestKingdomAPIDlg message handlers

BOOL CTestKingdomAPIDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	OnBnClickedInilize();
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CTestKingdomAPIDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CTestKingdomAPIDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CTestKingdomAPIDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CTestKingdomAPIDlg::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	AfxMessageBox(FunctionID::KDF_ENTRUSTORDER);
	OnOK();
}

void CTestKingdomAPIDlg::OnBnClickedInilize()
{
	// TODO: Add your control notification handler code here
	
	BOOL ret = adp.InitializeByXML("ExchangeAdapter.Kingdom.xml");

	cout<<" adp.Initialize(NULL); ret = : " <<ret<<std::endl;//()
	//printf("%d",ret);

	ret = adp._Connect("10.1.77.233",
		"10.1.77.233",
		21000,
		"KCXP00",
		"888888",
		"req_test1",
		"ans_test1"
		);
	cout<<"adp.Connect( ret = "  <<ret<<std::endl;//()
	adp.SetLogAble(TRUE);

}

void CTestKingdomAPIDlg::OnBnClickedLogin()
{
	CString pwd,usr;
	GetDlgItemText(IDC_EDIT_PWD,pwd);
	GetDlgItemText(IDC_EDIT_USR,usr);

	// TODO: Add your control notification handler code here
	BOOL ret = adp._Login('Z',"11700001042","740121");
	cout<<"ret = adp.Login();" << ret<<std::endl;//()

}

void CTestKingdomAPIDlg::OnBnClickedButton7()
{
	// TODO: Add your control notification handler code here
	adp.SetLogAble(TRUE);
}

void CTestKingdomAPIDlg::OnBnClickedButton8()
{
	// TODO: Add your control notification handler code here
//	std::list<CKingdomTradeAdapter::StockInfo> infos = adp.GetStockinfo(MarketDef::ALL,NULL);
}

void CTestKingdomAPIDlg::OnBnClickedQueryServerStatus()
{
	// TODO: Add your control notification handler code here
	adp.QueryServerStauts(20130110);
}

void CTestKingdomAPIDlg::OnBnClickedPutOrder()
{
	// TODO: Add your control notification handler code here 11700001042

	/*
	(MarketDef::MarketType market,//交易市场	market	char(1)
	char* secuid,//股东代码	secuid	char(10) 0152807367
	int fundid,//资金账户	fundid	int  可以不送，没有的话使用股东缺省账户
	char* stkcode,//证券代码	stkcode	char(8)
	char* bsflag,//买卖类别	bsflag	char(2)B:买入S:卖出
	double price,//价格	price	numeric(9,3)
	int qty,//数量	qty	int
	int ordergroup = -1)//委托批号	ordergroup	int 客户端制定，如果不想用的话，输入-1
	*/

//	std::string market = adp.GetStockinfo(MarketDef::ALL,"000100").front().market.c_str();
	/*adp.PlaceOrder(market.c_str()[0],
		adp.m_param.secuid.c_str(),
		adp.m_param.fundid.c_str(),
		"000100",
		"B",
		6.01,
		100,
		-1);*/
}

void CTestKingdomAPIDlg::OnBnClickedButton4()
{
	// TODO: Add your control notification handler code here
//	adp.QueryAllPosition();
}

void CTestKingdomAPIDlg::OnBnClickedButton10()
{
	// TODO: Add your control notification handler code here
	//adp.QueryAccountMoney();
}
