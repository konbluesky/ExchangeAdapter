
// Test.ExchangeAdapter.HST2Dlg.cpp : 实现文件
//

#include "stdafx.h"
#include "ustringutils.h"
#include "umaths.h"
#include "Test.ExchangeAdapter.HST2.h"
#include "Test.ExchangeAdapter.HST2Dlg.h"
#include "HST2TradeAdapter.h"
using namespace tradeadapter;
using namespace tradeadapter::hst2;
USING_LIBSPACE
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CHST2TradeAdapter instance;
// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CTestExchangeAdapterHST2Dlg 对话框




CTestExchangeAdapterHST2Dlg::CTestExchangeAdapterHST2Dlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTestExchangeAdapterHST2Dlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CTestExchangeAdapterHST2Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CTestExchangeAdapterHST2Dlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON_INITIALZIE, &CTestExchangeAdapterHST2Dlg::OnBnClickedButtonInitialzie)
	ON_BN_CLICKED(IDC_BUTTON_INITIALZIE2, &CTestExchangeAdapterHST2Dlg::OnBnClickedButtonInitialzie2)
	ON_BN_CLICKED(IDC_BUTTON_INITIALZIE3, &CTestExchangeAdapterHST2Dlg::OnBnClickedButtonInitialzie3)
END_MESSAGE_MAP()


// CTestExchangeAdapterHST2Dlg 消息处理程序

BOOL CTestExchangeAdapterHST2Dlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CTestExchangeAdapterHST2Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CTestExchangeAdapterHST2Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CTestExchangeAdapterHST2Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CTestExchangeAdapterHST2Dlg::OnBnClickedButtonInitialzie()
{
	// TODO: Add your control notification handler code here
	HST2IniParam param;
	param.addrandport = _T("10.10.10.37:9003");
	param.licensefile = _T("license.dat");
	param.fund_account = _T("850004");
	param.pref = LanguagePref::Chinese;
	param.operator_no = _T("6281");
	param.password = _T("6281");

	instance.Initialize(&param);

}


void CTestExchangeAdapterHST2Dlg::OnBnClickedButtonInitialzie2()
{
	// TODO: Add your control notification handler code here
	Order order;
	order.OpeDir = BuySell::Buy;
	order.Amount = 900;
	order.Price = 6.55;
	order.ContractCode = _T("600016");
	int orderid = instance.EntrustOrder(order);
	this->GetDlgItem(IDC_EDIT1)->SetWindowText(UStrConvUtils::ToString(orderid).c_str());
}

void CTestExchangeAdapterHST2Dlg::OnBnClickedButtonInitialzie3()
{
	// TODO: Add your control notification handler code here
	CString txt;
	this->GetDlgItem(IDC_EDIT1)->GetWindowText(txt);
	instance.CancelOrder(UMaths::ToInt32( (const TCHAR*)txt));
}
