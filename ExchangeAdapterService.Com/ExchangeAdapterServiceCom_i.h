

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 7.00.0500 */
/* at Tue Oct 15 17:01:50 2013
 */
/* Compiler settings for .\ExchangeAdapterServiceCom.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext, robust
    error checks: stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __ExchangeAdapterServiceCom_i_h__
#define __ExchangeAdapterServiceCom_i_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IOrderService_FWD_DEFINED__
#define __IOrderService_FWD_DEFINED__
typedef interface IOrderService IOrderService;
#endif 	/* __IOrderService_FWD_DEFINED__ */


#ifndef __OrderService_FWD_DEFINED__
#define __OrderService_FWD_DEFINED__

#ifdef __cplusplus
typedef class OrderService OrderService;
#else
typedef struct OrderService OrderService;
#endif /* __cplusplus */

#endif 	/* __OrderService_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __IOrderService_INTERFACE_DEFINED__
#define __IOrderService_INTERFACE_DEFINED__

/* interface IOrderService */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IOrderService;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("419E8F28-D53C-49AD-88E8-643745A03C0A")
    IOrderService : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE InitializeByXML( 
            BSTR path) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE UnInitialize( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IOrderServiceVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IOrderService * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IOrderService * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IOrderService * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IOrderService * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IOrderService * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IOrderService * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IOrderService * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *InitializeByXML )( 
            IOrderService * This,
            BSTR path);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *UnInitialize )( 
            IOrderService * This);
        
        END_INTERFACE
    } IOrderServiceVtbl;

    interface IOrderService
    {
        CONST_VTBL struct IOrderServiceVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IOrderService_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IOrderService_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IOrderService_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IOrderService_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IOrderService_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IOrderService_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IOrderService_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IOrderService_InitializeByXML(This,path)	\
    ( (This)->lpVtbl -> InitializeByXML(This,path) ) 

#define IOrderService_UnInitialize(This)	\
    ( (This)->lpVtbl -> UnInitialize(This) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IOrderService_INTERFACE_DEFINED__ */



#ifndef __ExchangeAdapterServiceComLib_LIBRARY_DEFINED__
#define __ExchangeAdapterServiceComLib_LIBRARY_DEFINED__

/* library ExchangeAdapterServiceComLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_ExchangeAdapterServiceComLib;

EXTERN_C const CLSID CLSID_OrderService;

#ifdef __cplusplus

class DECLSPEC_UUID("0228F819-3807-4D71-9E62-3711115FD5AC")
OrderService;
#endif
#endif /* __ExchangeAdapterServiceComLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


