// dllmain.h : Declaration of module class.

class CExchangeAdapterServiceComModule : public CAtlDllModuleT< CExchangeAdapterServiceComModule >
{
public :
	DECLARE_LIBID(LIBID_ExchangeAdapterServiceComLib)
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_EXCHANGEADAPTERSERVICECOM, "{2853B3CB-B6E6-415C-A392-8CB309B851A2}")
};

extern class CExchangeAdapterServiceComModule _AtlModule;
