// OrderService.h : Declaration of the COrderService

#pragma once
#include "resource.h"       // main symbols

#include "ExchangeAdapterServiceCom_i.h"


#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif

namespace tradeadapter
{
	class IOrderService;
}


// COrderService

class ATL_NO_VTABLE COrderService :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<COrderService, &CLSID_OrderService>,
	public IDispatchImpl<IOrderService, &IID_IOrderService, &LIBID_ExchangeAdapterServiceComLib, /*wMajor =*/ 1, /*wMinor =*/ 0>
{
public:
	COrderService()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_ORDERSERVICE)


BEGIN_COM_MAP(COrderService)
	COM_INTERFACE_ENTRY(IOrderService)
	COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()



	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:

	STDMETHOD(InitializeByXML)(BSTR path);
	STDMETHOD(UnInitialize)(void);

private:
	tradeadapter::IOrderService* m_pInstance;
};

OBJECT_ENTRY_AUTO(__uuidof(OrderService), COrderService)
