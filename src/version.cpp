
#include "stdio.h"

#if defined(WIN32)
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#endif

BOOL APIENTRY DllMain( HANDLE hModule, 
					  DWORD  ul_reason_for_call, 
					  LPVOID lpReserved
					  )
{
	return TRUE;
}

#ifdef __DATE__
#define BUILD_DATE __DATE__
#else
#define BUILD_DATE "xx/xx/xx"
#endif
#ifdef __TIME__
#define BUILD_TIME __TIME__
#else
#define BUILD_TIME "xx:xx:xx"
#endif
namespace 
{
	static const char* GetBuildTime()
	{
		static char buildInfo[256];	
		sprintf_s(buildInfo,256,"#Build: %.20s, %.9s",BUILD_DATE,BUILD_TIME);
		return buildInfo;
	}
}

extern "C"	__declspec(dllexport) 	const char* DllGetVersion()
{
#define DLL_UTILS_VERSION		"1.0.0"
	static char version[256] ={0};
	sprintf_s(version,256,"%s  %s",DLL_UTILS_VERSION,GetBuildTime());
	return version;
}



