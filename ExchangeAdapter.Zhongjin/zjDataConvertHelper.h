 #ifndef __ZJ_DATACONVERT_H__
#define __ZJ_DATACONVERT_H__

#include "ZJLib.h"
#include "ChinaOrderDefinition.h"
#include "ChinaQuoteDefinition.h"
#include "zjTraderAdapter.h"

namespace exchangeadapter_zhongjin
{
	class UZJDataConvertHelper
	{
	public:
		static int TranslateQuoteDataToPublicData(
			const CFfexFtdcDepthMarketDataField *,Quote &,DoM &);
	public:
		static int TranslatePublicDataToOrderData(
			const ZJTraderProvider::InitParam *pInitParam,
			const Order & clientOrder,CFfexFtdcInputOrderField* pApiOrder);

		static int TranslatePublicDataToCancelOrderData(
			const ZJTraderProvider::InitParam *pInitParam,
			const OrderCancelRequest &clientOrder,
			CFfexFtdcOrderActionField* pApiOrder);

		static int TranslatePublicDataToCancelRelaceOrderData(
			const OrderCancelReplaceRequest &clientOrder,
			CFfexFtdcOrderActionField* pApiOrder);

		static int TraslateExecutionReportToPublicData(
			const CFfexFtdcInputOrderField &apiReport,
			ExecutionReport *pClientReport);

		static int TraslateExecutionReportToPublicData(
			const CFfexFtdcInputOrderField &apiReport,
			BusinessReject *pClientReject, CFfexFtdcRspInfoField *pRspInfo,
			int requestID);

		static int TranslateQuoteDataToPublicData(
			const CFfexFtdcOrderActionField &apiReport,
			ExecutionReport* pClientReport);

		static int TraslateExecutionReportToPublicData(
			const CFfexFtdcOrderActionField &apiReport,
			OrderCancelReject *pClientReject, CFfexFtdcRspInfoField *pRspInfo,
			int requestID);

		static int TraslateExecutionReportToPublicData(
			const CFfexFtdcOrderActionField &apiReport,
			OrderCancelReplaceReject* pClientReject,
			CFfexFtdcRspInfoField *pRspInfo,
			int requestID);

		static int TraslateExecutionReportToPublicData(
			const CFfexFtdcOrderField &apiReport,
			ExecutionReport *pClientReport);

		static int TraslateExecutionReportToPublicData(
			const CFfexFtdcTradeField &apiReport,
			FillReport *pClientReport);

		static int TranslatePublicDataToBulletin(
			const CFfexFtdcBulletinField &apiBulletin,
			Bulletin *pClientBulletin);

	private:
		UZJDataConvertHelper(void);
		virtual ~UZJDataConvertHelper(void);
	};
}

#endif