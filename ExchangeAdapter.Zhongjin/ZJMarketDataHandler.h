#ifndef __ZJ_CUSTOMCALLBACK_H__
#define __ZJ_CUSTOMCALLBACK_H__


#include "ZJLib.h"

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4512)
#else
#error "不应该出现,以待其他"
#endif

class IFixMarketProvider;

class CZJMarketDataHandler : public CFfexFtdcMduserSpi
{
public:
	struct InitParam 
	{
		std::string ParticipantID;
		std::string UserID;
		std::string Password;
		std::string Configpath;
	};

	CZJMarketDataHandler(IFixMarketProvider* marketprovider,CFfexFtdcMduserApi *,const InitParam& param);
	~CZJMarketDataHandler(void);

	///当客户端与交易后台建立起通信连接时（还未登录前），该方法被调用。
	virtual void OnFrontConnected();
	
	///当客户端与交易后台通信连接断开时，该方法被调用。当发生这个情况后，API会自动重新连接，客户端可不做处理。
	///@param nReason 错误原因
	///        0x1001 网络读失败
	///        0x1002 网络写失败
	///        0x2001 接收心跳超时
	///        0x2002 发送心跳失败
	///        0x2003 收到错误报文
	virtual void OnFrontDisconnected(int nReason);
		
	///心跳超时警告。当长时间未收到报文时，该方法被调用。
	///@param nTimeLapse 距离上次接收报文的时间
	virtual void OnHeartBeatWarning(int nTimeLapse);
	

	///错误应答
	virtual void OnRspError(CFfexFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) ;

	///用户登录应答
	virtual void OnRspUserLogin(CFfexFtdcRspUserLoginField *pRspUserLogin, CFfexFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast);

	///用户退出应答
	virtual void OnRspUserLogout(CFfexFtdcRspUserLogoutField *pRspUserLogout, CFfexFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast);

	///深度行情通知
	virtual void OnRtnDepthMarketData(CFfexFtdcDepthMarketDataField *pDepthMarketData);

public:
	const InitParam m_param;

private:
	CFfexFtdcMduserApi *m_pUserApi;
	IFixMarketProvider* m_MarketProvider;
};

#ifdef _MSC_VER
#pragma warning(pop)
#else
#error "仅支持VC,以待修改"
#endif

#endif