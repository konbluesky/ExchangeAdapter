#include "zjDataConvertHelper.h"
#include "zjEnumsConvertHelper.h"
#include "ZJPriceHelper.h"
#include <map>
#include <strstream>
#include "StringUtils.h"
#include "debugutils.h"
#include "zjTraderAdapter.h"
#include "LocalCompile.h"

using namespace std;

namespace exchangeadapter_zhongjin
{
	struct PreVolume
	{
		int totalVolume;
		int lastVolume;
	};

	int UZJDataConvertHelper::TranslatePublicDataToOrderData(
		const ZJTraderProvider::InitParam *pInitParam,const Order &clientOrder,
		CFfexFtdcInputOrderField *pApiOrder)
	{
		DBG_ASSERT(NULL != pApiOrder);

		LIB_NS::UString::Copy(pApiOrder->ParticipantID,
			pInitParam->sParticipantID);
		LIB_NS::UString::Copy(pApiOrder->UserID,pInitParam->sUserID);
#if defined(RMMS_TEST_CLIENT)
		LIB_NS::UString::Copy(pApiOrder->ClientID,RMMS_ZJ_CLIENT);
#else
		LIB_NS::UString::Copy(pApiOrder->ClientID,clientOrder.IDClient);
#endif
		LIB_NS::UString::Copy(pApiOrder->OrderLocalID,
			clientOrder.zjIDLocalOrder);
		LIB_NS::UString::Copy(pApiOrder->OrderSysID,"");
		LIB_NS::UString::Copy(pApiOrder->InstrumentID,
			clientOrder.SecurityDesc);
		pApiOrder->VolumeTotalOriginal = clientOrder.Quantity;
		pApiOrder->LimitPrice = clientOrder.LimitPrice;
		pApiOrder->StopPrice = clientOrder.StopPrice;
		UZJEnumsConvert::Convert2ExchOpenClose(clientOrder.OpenClose,
			pApiOrder->CombOffsetFlag);
		UZJEnumsConvert::Convert2ExchOrderType(clientOrder.OrderType,
			pApiOrder->OrderPriceType);
		UZJEnumsConvert::Convert2ExchSide(clientOrder.Side,
			pApiOrder->Direction);
		UZJEnumsConvert::Convert2ExchTimeCondition(clientOrder.OrderType,
			clientOrder.zjpri.TimeCondition, pApiOrder->TimeCondition);

		pApiOrder->VolumeCondition = FFEX_FTDC_VC_AV;
		LIB_NS::UString::Copy(pApiOrder->CombHedgeFlag, "1");
		pApiOrder->ContingentCondition = FFEX_FTDC_CC_Immediately;
		pApiOrder->ForceCloseReason = FFEX_FTDC_FCC_NotForceClose;
		LIB_NS::UString::Copy(pApiOrder->GTDDate, "");
		pApiOrder->IsAutoSuspend = 0;

		return 0;
	}

	int UZJDataConvertHelper::TranslateQuoteDataToPublicData(
		const CFfexFtdcDepthMarketDataField *marketData, Quote &quote,DoM &dom)
	{
		DBG_ASSERT(NULL != marketData);
		static std::map<std::string,PreVolume> preVolume;
		quote.ID = marketData->InstrumentID;
		quote.Last_Price = UZJPriceHelper::Price(marketData->LastPrice);

		quote.Total_Volume = marketData->Volume;
		if (quote.Total_Volume != preVolume[quote.ID].totalVolume){
			quote.Last_Volume = preVolume[quote.ID].lastVolume = 
				quote.Total_Volume - preVolume[quote.ID].totalVolume ;
			preVolume[quote.ID].totalVolume = quote.Total_Volume;
		}else
			quote.Last_Volume = preVolume[quote.ID].lastVolume;

		quote.BidPrice = UZJPriceHelper::Price(marketData->BidPrice1);
		quote.BidQty = marketData->BidVolume1;
		quote.AskPrice = UZJPriceHelper::Price(marketData->AskPrice1);
		quote.AskQty = marketData->AskVolume1;
		quote.LowPrice = UZJPriceHelper::Price(marketData->LowestPrice);
		quote.HighPrice = UZJPriceHelper::Price(marketData->HighestPrice);
		quote.Open_Price = UZJPriceHelper::Price(marketData->OpenPrice);
		quote.SettlementPrice = UZJPriceHelper::Price(
			marketData->PreSettlementPrice);

		int32 year = 0,month = 0,day = 0,hour = 0,minute = 0,second = 0;
		std::sscanf(marketData->TradingDay,"%4d%2d%2d",&year,&month,&day);
		std::sscanf(marketData->UpdateTime,"%2d:%2d:%2d",&hour,&minute,&second);
		quote.RecieveTime.SetYMD(year,month,day);
		quote.RecieveTime.SetHMS(hour,minute,second,marketData->UpdateMillisec);
		dom.RecieveTime = quote.RecieveTime;
		
		// 进行深度数据转换
		dom.CurrentPrice = quote.Last_Price;
		dom.ID = quote.ID;
		std::printf("%s::%f\n",dom.ID.c_str(),dom.CurrentPrice);

		dom.LastQuantity = quote.Last_Volume;
		dom.Open_Price = quote.Open_Price;
		dom.LowPrice = quote.LowPrice;
		dom.HighPrice = quote.HighPrice;
		dom.Total_Volume = quote.Total_Volume;
		dom.SettlementPrice = quote.SettlementPrice;
		dom.AskLevel[0] = UZJPriceHelper::Price(marketData->AskPrice1);
		dom.AskLevel[1] = UZJPriceHelper::Price(marketData->AskPrice2);
		dom.AskLevel[2] = UZJPriceHelper::Price(marketData->AskPrice3);
		dom.AskLevel[3] = UZJPriceHelper::Price(marketData->AskPrice4);
		dom.AskLevel[4] = UZJPriceHelper::Price(marketData->AskPrice5);
		dom.AskLevel[5] = dom.AskLevel[6] = dom.AskLevel[7] = dom.AskLevel[8] = 
			dom.AskLevel[9] = 0.0;
		dom.BidLevel[0] = UZJPriceHelper::Price(marketData->BidPrice1);
		dom.BidLevel[1] = UZJPriceHelper::Price(marketData->BidPrice2);
		dom.BidLevel[2] = UZJPriceHelper::Price(marketData->BidPrice3);
		dom.BidLevel[3] = UZJPriceHelper::Price(marketData->BidPrice4);
		dom.BidLevel[4] = UZJPriceHelper::Price(marketData->BidPrice5);
		dom.BidLevel[5] = dom.BidLevel[6] = dom.BidLevel[7] = dom.BidLevel[8] = 
			dom.BidLevel[9] = 0.0;

		dom.AskQuantities[0] = marketData->AskVolume1;
		dom.AskQuantities[1] = marketData->AskVolume2;
		dom.AskQuantities[2] = marketData->AskVolume3;
		dom.AskQuantities[3] = marketData->AskVolume4;
		dom.AskQuantities[4] = marketData->AskVolume5;
		dom.AskQuantities[5] = dom.AskQuantities[6] = dom.AskQuantities[7] = 
			dom.AskQuantities[8] = dom.AskQuantities[9] = 0.0;

		dom.BidQuantities[0] = marketData->BidVolume1;
		dom.BidQuantities[1] = marketData->BidVolume2;
		dom.BidQuantities[2] = marketData->BidVolume3;
		dom.BidQuantities[3] = marketData->BidVolume4;
		dom.BidQuantities[4] = marketData->BidVolume5;
		dom.BidQuantities[5] = dom.BidQuantities[6] = dom.BidQuantities[7] = 
			dom.BidQuantities[8] = dom.BidQuantities[9] = 0.0;
		// 。。。。。。结束深度数据转换
		dom.SupportLevel = 5;

		return 0;
	}

	//撤单请求转换
	int UZJDataConvertHelper::TranslatePublicDataToCancelOrderData(
		const ZJTraderProvider::InitParam *pInitParam,
		const OrderCancelRequest &clientOrder,
		CFfexFtdcOrderActionField* pApiOrder)
	{
		memset(pApiOrder,0,sizeof(CFfexFtdcOrderActionField));

		DBG_ASSERT(NULL != pApiOrder);

		pApiOrder->ActionFlag = FFEX_FTDC_AF_Delete;
		LIB_NS::UString::Copy(pApiOrder->OrderLocalID,
			clientOrder.zjIDLocalOrder);
		LIB_NS::UString::Copy(pApiOrder->ParticipantID,
			pInitParam->sParticipantID);
		LIB_NS::UString::Copy(pApiOrder->UserID,pInitParam->sUserID);
#if defined(RMMS_TEST_CLIENT)
		LIB_NS::UString::Copy(pApiOrder->ClientID, RMMS_ZJ_CLIENT);
#else
		LIB_NS::UString::Copy(pApiOrder->ClientID, clientOrder.IDClient);
#endif

		LIB_NS::UString::FillChar(pApiOrder->OrderSysID,13,' ');

		LIB_NS::UString::FillEnd(pApiOrder->OrderSysID,
			clientOrder.IDSysOrder.c_str());
		strcpy(pApiOrder->ActionLocalID, "");
		pApiOrder->LimitPrice = 0.0;
		pApiOrder->VolumeChange = 0;
		strcpy(pApiOrder->BusinessUnit, "");

		return 0;
	}

	/* 修改单请求转换 */
	int UZJDataConvertHelper::TranslatePublicDataToCancelRelaceOrderData(
		const OrderCancelReplaceRequest &clientOrder,
		CFfexFtdcOrderActionField* pApiOrder)
	{
		std::memset(pApiOrder,0,sizeof(CFfexFtdcOrderActionField));
		DBG_ASSERT(NULL != pApiOrder);

		pApiOrder->ActionFlag = FFEX_FTDC_AF_Modify;
		LIB_NS::UString::Copy(pApiOrder->OrderLocalID,
			clientOrder.zjIDLocalOrder);
		//LIB_NS::UString::Copy(pApiOrder->ParticipantID,
		//	clientOrder.IDParticipant);
		//LIB_NS::UString::Copy(pApiOrder->UserID,
		//	clientOrder.IDUser);
		LIB_NS::UString::Copy(pApiOrder->ClientID,
			clientOrder.IDClient);

		LIB_NS::UString::FillChar(pApiOrder->OrderSysID,13,' ');
		LIB_NS::UString::FillEnd(pApiOrder->OrderSysID,
			clientOrder.IDSysOrder.c_str());
		strcpy(pApiOrder->ActionLocalID, "");
		pApiOrder->LimitPrice = 0.0;
		pApiOrder->VolumeChange = 0;
		strcpy(pApiOrder->BusinessUnit, "");

		return 0;
	}


	//下单响应转换
	int UZJDataConvertHelper::TraslateExecutionReportToPublicData(
		const CFfexFtdcInputOrderField &apiReport,
		ExecutionReport *pClientReport)
	{
		DBG_ASSERT(NULL != pClientReport);
		/*
#if defined(RMMS_TEST_CLIENT)
		pClientReport->IDClient = RMMS_ZJ_GLOTRADER;
#else
		pClientReport->IDClient = apiReport.ClientID;
#endif
		*/
		sprintf(pClientReport->zjIDLocalOrder,apiReport.OrderLocalID,
			strlen(pClientReport->zjIDLocalOrder));
		pClientReport->IDSysOrder = apiReport.OrderSysID;

		pClientReport->PriceLimit = apiReport.LimitPrice;
		pClientReport->PriceStop = 0;
		pClientReport->QtyOrder = apiReport.VolumeTotalOriginal;

		pClientReport->OrderStatus = OrderStatusFlag::New;
		UZJEnumsConvert::Convert2RmmSide(apiReport.Direction,
			pClientReport->Side);
		pClientReport->ExecType = ExecTypeFlag::ExecType_New;

		pClientReport->SecurityDesc = apiReport.InstrumentID;
		pClientReport->datetime = LIB_NS::DateTime::NowUtc();

		return 0;
	}

	//下单拒绝转换
	int UZJDataConvertHelper::TraslateExecutionReportToPublicData(
		const CFfexFtdcInputOrderField &apiReport,
		BusinessReject *pClientReject, CFfexFtdcRspInfoField *pRspInfo,
		int requestID)
	{
		DBG_ASSERT(NULL != pClientReject);

		sprintf(pClientReject->zjIDLocalOrder,apiReport.OrderLocalID,
			strlen(pClientReject->zjIDLocalOrder));
		pClientReject->LastMsgSeqNumProcessed = requestID;
		pClientReject->RefMsgType = "D";
		pClientReject->BusinessRejectReason = 
			BusinessRejectReasonFlag::BRR_Other;

		strstream ss;
		ss<<pRspInfo->ErrorID;
		ss>>pClientReject->ErrorCode;
		ss.clear();

		pClientReject->ErrorSource = "ZJS";

		return 0;
	}

	int UZJDataConvertHelper::TranslateQuoteDataToPublicData(
		const CFfexFtdcOrderActionField &apiReport,
		ExecutionReport* pClientReport)
	{
		return 0;
	}

	//撤单拒绝转换
	int UZJDataConvertHelper::TraslateExecutionReportToPublicData(
		const CFfexFtdcOrderActionField &apiReport,
		OrderCancelReject *pClientReject,
		CFfexFtdcRspInfoField *pRspInfo,
		int requestID)
	{
		DBG_ASSERT(NULL != pClientReject);

		pClientReject->IDSysOrder = apiReport.OrderSysID;
		pClientReject->LastMsgSeqNumProcessed = requestID;

		strstream ss;
		ss<<pRspInfo->ErrorID;
		ss>>pClientReject->ErrorCode;
		ss.clear();

		pClientReject->ErrorSource = "ZJS";

		return 0;
	}

	//改单拒绝转换
	int UZJDataConvertHelper::TraslateExecutionReportToPublicData(
		const CFfexFtdcOrderActionField &apiReport,
		OrderCancelReplaceReject* pClientReject,
		CFfexFtdcRspInfoField *pRspInfo, int requestID)
	{
		DBG_ASSERT(NULL != pClientReject);

		pClientReject->IDSysOrder = apiReport.OrderSysID;
		pClientReject->LastMsgSeqNumProcessed = requestID;

		strstream ss;
		ss<<pRspInfo->ErrorID;
		ss>>pClientReject->ErrorCode;
		ss.clear();

		pClientReject->ErrorSource = "ZJS";

		return 0;
	}

	//报单状态改变转换
	int UZJDataConvertHelper::TraslateExecutionReportToPublicData(
		const CFfexFtdcOrderField &apiReport, ExecutionReport *pClientReport)
	{
		DBG_ASSERT(NULL != pClientReport);

		if(apiReport.OrderStatus == FFEX_FTDC_OST_AllTraded ||
			apiReport.OrderStatus == FFEX_FTDC_OST_PartTradedQueueing ||
			apiReport.OrderStatus == FFEX_FTDC_OST_PartTradedNotQueueing ||
			apiReport.OrderStatus == FFEX_FTDC_OST_NoTradeQueueing)
			return 0;

		/*
#if defined(RMMS_TEST_CLIENT)
		pClientReport->IDClient = RMMS_ZJ_GLOTRADER;
#else
		pClientReport->IDClient = apiReport.ClientID;
#endif
		*/
		sprintf(pClientReport->zzIDLocalOrder,apiReport.OrderLocalID,
			std::strlen(pClientReport->zjIDLocalOrder));
		pClientReport->IDSysOrder = apiReport.OrderSysID;

		UZJEnumsConvert::Convert2RmmOrderStatus(apiReport.OrderStatus,
			pClientReport->OrderStatus);
		pClientReport->PriceLimit = apiReport.LimitPrice;
		pClientReport->PriceStop = apiReport.StopPrice;
		pClientReport->QtyOrder = apiReport.VolumeTotalOriginal;
		UZJEnumsConvert::Convert2RmmSide(apiReport.Direction,
			pClientReport->Side);
		UZJEnumsConvert::Convert2RmmExecType(pClientReport->OrderStatus,
			pClientReport->ExecType);

		pClientReport->SecurityDesc = apiReport.InstrumentID;

		pClientReport->datetime = LIB_NS::DateTime::NowUtc();

		return 0;
	}

	int UZJDataConvertHelper::TraslateExecutionReportToPublicData(
		const CFfexFtdcTradeField &apiReport, FillReport *pClientReport)
	{
		DBG_ASSERT(NULL != pClientReport);

#if defined(RMMS_TEST_CLIENT)
		pClientReport->IDClient = RMMS_ZJ_GLOTRADER;
#else
		pClientReport->IDClient = apiReport.ClientID;
#endif
		sprintf(pClientReport->zjIDLocalOrder,apiReport.OrderLocalID,
			std::strlen(pClientReport->zjIDLocalOrder));
		pClientReport->IDSysOrder = apiReport.OrderSysID;
		pClientReport->IDExec = apiReport.TradeID;
		pClientReport->LastPx = apiReport.Price;
		pClientReport->QtyLastShares = apiReport.Volume;

		UZJEnumsConvert::Convert2RmmSide(apiReport.Direction,
			pClientReport->Side);
		pClientReport->SecurityDesc = apiReport.InstrumentID;

		int year = (apiReport.TradingDay[0]-'0') * 1000 + 
			(apiReport.TradingDay[1]-'0') * 100 + 
			(apiReport.TradingDay[2]-'0') * 10 + 
			(apiReport.TradingDay[3]-'0');
		int month = (apiReport.TradingDay[4]-'0') * 10 + 
			(apiReport.TradingDay[5]-'0');
		int day = (apiReport.TradingDay[6]-'0') * 10 + 
			(apiReport.TradingDay[7]-'0');
		int hour = (apiReport.TradeTime[0]-'0') * 10 + 
			(apiReport.TradeTime[1]-'0');
		int minute = (apiReport.TradeTime[3]-'0') * 10 + 
			(apiReport.TradeTime[4]-'0');
		int second = (apiReport.TradeTime[6]-'0') * 10 + 
			(apiReport.TradeTime[7]-'0');

		pClientReport->TradeTime.SetYMD(year,month,day);
		pClientReport->TradeTime.SetHMS(hour,minute,second,0);

		return 0;
	}

	//公告转换
	int UZJDataConvertHelper::TranslatePublicDataToBulletin(
		const CFfexFtdcBulletinField &apiBulletin, Bulletin *pClientBulletin)
	{
		pClientBulletin->Headline = apiBulletin.Abstract;
		pClientBulletin->Content = apiBulletin.Content;
		pClientBulletin->URLLink = apiBulletin.URLLink;

		int year = (apiBulletin.TradingDay[0]-'0') * 1000 + 
			(apiBulletin.TradingDay[1]-'0') * 100 + 
			(apiBulletin.TradingDay[2]-'0') * 10 + 
			(apiBulletin.TradingDay[3]-'0');
		int month = (apiBulletin.TradingDay[4]-'0') * 10 + 
			(apiBulletin.TradingDay[5]-'0');
		int day = (apiBulletin.TradingDay[6]-'0') * 10 + 
			(apiBulletin.TradingDay[7]-'0');
		int hour = (apiBulletin.SendTime[0]-'0') * 10 +
			(apiBulletin.SendTime[1]-'0');
		int minute = (apiBulletin.SendTime[3]-'0') * 10 + 
			(apiBulletin.SendTime[4]-'0');
		int second = (apiBulletin.SendTime[6]-'0') * 10 + 
			(apiBulletin.SendTime[7]-'0');

		pClientBulletin->SendTime.SetYMD(year,month,day);
		pClientBulletin->SendTime.SetHMS(hour,minute,second,0);

		return 0;
	}
}