#ifndef ZJ_TRADER_ADAPTER_H
#define ZJ_TRADER_ADAPTER_H

#include "ZJLib.h"
#include "IFixTrader.h"
#include "ChinaOrderDefinition.h"
#include "ZJTraderAdapterHandler.h"
#include "autoptr.h"
#include "ExchangeAdapterBase.h"
#include <vector>

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4251)
#pragma warning(disable:4275)
#else
#error "不应该出现,以待其他"
#endif

struct OrderCancelReplaceRequest;
class CFfexFtdcTraderApi;
class ZJTraderAdapterHandler;
namespace exchangeadapter_zhongjin
{
	class EXAPI  ZJTraderProvider : public IFixTraderProvider
	{
	public:
		struct InitParam
		{
			std::string sFlowPath;
			std::vector<tstring> sAddressIps;
			std::vector<int> iPorts;
			tstring sParticipantID;
			tstring sUserID;
			tstring sPassword;
			int32 iResumeType;

			EXAPI static std::pair<bool,InitParam> LoadConfig(const tstring& fname);	
		};

		ZJTraderProvider(void);
		virtual ~ZJTraderProvider();
		virtual tstring GetName();
		virtual bool InitializeByXML( const tchar* configfile );
		virtual bool Initialize(void* );
		virtual void UnInitialize();
		virtual bool Login(void*);
		virtual bool Logout(void*);
		virtual void RequestFix(FIX::Message &msg);
				
		void ReqeustOrderAdd(const Order& order);
		void RequestOrderCancel(const OrderCancelRequest& order);
		void RequestOrderModify(const OrderCancelReplaceRequest& order);
#if 0
		/************************ following functions implemented by gyliu *****************************/
		virtual void ReqUserPasswordUpdate(CFfexFtdcUserPasswordUpdateField *pUserPasswordUpdate, int nRequestID);
		virtual void ReqExecOrderInsert(CFfexFtdcInputExecOrderField *pInputExecOrder, int nRequestID);
		virtual void ReqExecOrderAction(CFfexFtdcExecOrderActionField *pExecOrderAction, int nRequestID);
		virtual void ReqQryPartAccount(CFfexFtdcQryPartAccountField *pQryPartAccount, int nRequestID);
		virtual void RequestQryOrder(CFfexFtdcQryOrderField *pQryOrder, int nRequestID);
		virtual void ReqQryQuote(CFfexFtdcQryQuoteField *pQryQuote, int nRequestID);
		virtual void ReqQryTrade(CFfexFtdcQryTradeField *pQryTrade, int nRequestID);
		virtual void ReqQryClient(CFfexFtdcQryClientField *pQryClient, int nRequestID);
		virtual void ReqQryPartPosition(CFfexFtdcQryPartPositionField *pQryPartPosition, int nRequestID);
		virtual void ReqQryClientPosition(CFfexFtdcQryClientPositionField *pQryClientPosition, int nRequestID);
		virtual void ReqQryInstrument(CFfexFtdcQryInstrumentField *pQryInstrument, int nRequestID);
		virtual void ReqQryInstrumentStatus(CFfexFtdcQryInstrumentStatusField *pQryInstrumentStatus, int nRequestID);
		virtual void ReqQryMarketData(CFfexFtdcQryMarketDataField *pQryMarketData, int nRequestID);
		virtual void ReqQryBulletin(CFfexFtdcQryBulletinField *pQryBulletin, int nRequestID);
		virtual void ReqQryMBLMarketData(CFfexFtdcQryMBLMarketDataField *pQryMBLMarketData, int nRequestID);
		virtual void ReqQryHedgeVolume(CFfexFtdcQryHedgeVolumeField *pQryHedgeVolume, int nRequestID);
		virtual void ReqAdminOrderInsert(CFfexFtdcInputAdminOrderField *pInputAdminOrder, int nRequuestID);
		virtual void ReqQryCreditLimit(CFfexFtdcQryCreditLimitField *pQryCreaditLimit, int nRequestID);
		virtual void ReqQuoteInsert(CFfexFtdcInputQuoteField *pInputQuote, int nRequestID);
		virtual void ReqQuoteAction(CFfexFtdcQuoteActionField *pQuoteAction, int nRequestID);
#endif

	private:
		CFfexFtdcTraderApi *trader;
		boost::shared_ptr<ZJTraderAdapterHandler> sh;
		InitParam pInitParam;
	};
}

#ifdef _MSC_VER
#pragma warning(pop)
#else
#error "仅支持VC,以待修改"
#endif

#endif