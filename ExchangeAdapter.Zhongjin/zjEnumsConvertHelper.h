#ifndef __ZJENUMCONVERT_H__
#define __ZJENUMCONVERT_H__

#include "ZJLib.h"
#include "ChinaOrderDefinition.h"

namespace exchangeadapter_zhongjin
{
	class UZJEnumsConvert
	{
	public:
		static void Convert2ExchOpenClose(OpenCloseFlag rmmOpenClose, TFfexFtdcCombOffsetFlagType& exchOpenClose);
		static void Convert2ExchTimeCondition(OrderTypeFlag rmmOrderType, TimeConditionFlag rmmTimeCondition, TFfexFtdcTimeConditionType& exchTimeCondition);
		static void Convert2ExchSide(TradeSide rmmTradeSide, TFfexFtdcDirectionType& exchTradSide);
		static void Convert2ExchOrderType(OrderTypeFlag rmmOrderType, TFfexFtdcOrderPriceTypeType& exchOrderType);
		static void Convert2ExchOrderStatus(OrderStatusFlag rmmOrderStatus, TFfexFtdcOrderStatusType& exchOrderStatus);
		static void Convert2RmmOpenClose(const TFfexFtdcCombOffsetFlagType &exchOpenClose, OpenCloseFlag& rmmOpenClose);
		static void Convert2RmmTimeCondition(TFfexFtdcTimeConditionType exchTimeCondition, TimeConditionFlag& rmmTimeCondition);
		static void Convert2RmmSide(TFfexFtdcDirectionType exchSide, TradeSide& rmmTradeSide);
		static void Convert2RmmOrderType(TFfexFtdcOrderPriceTypeType exchOrderType, OrderTypeFlag& rmmOrderType); 
		static void Convert2RmmOrderStatus(TFfexFtdcOrderStatusType exchOrderStatus, OrderStatusFlag& rmmOrderStatus);
		static void Convert2RmmExecType(OrderStatusFlag rmmOrderStauts, ExecTypeFlag& rmmExecType);
	private:
		UZJEnumsConvert();
	};
}
#endif