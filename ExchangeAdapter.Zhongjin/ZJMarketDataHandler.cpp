#include "ZJMarketDataHandler.h"
#include "ChinaQuoteDefinition.h"
#include "zjDataConvertHelper.h"
#include "StringUtils.h"
#include "DebugUtils.h"
#include "ZJMarketDataProvider.h"
#include "FixDataConvertHelper.h"
#include "LocalCompile.h"

CZJMarketDataHandler::CZJMarketDataHandler(IFixMarketProvider* marketprovider,
	CFfexFtdcMduserApi *pUserApi,const InitParam& param)
	:m_MarketProvider(marketprovider),m_pUserApi(pUserApi),m_param(param)
{
}

CZJMarketDataHandler::~CZJMarketDataHandler(void)
{
}

void CZJMarketDataHandler::OnFrontConnected()
{
	m_MarketProvider->PriOnNetConnected();	
}
	
void CZJMarketDataHandler::OnFrontDisconnected(int nReason)
{
	DBG_TRACE(("<OnFrontDisconnected> be called. Reason=[%d]\n", nReason));
	m_MarketProvider->PriOnNetDisconnected();
}
		
void CZJMarketDataHandler::OnHeartBeatWarning(int nTimeLapse)
{
}
	
void CZJMarketDataHandler::OnRspError(CFfexFtdcRspInfoField *pRspInfo,
	int nRequestID, bool bIsLast)
{
}

void CZJMarketDataHandler::OnRspUserLogin(
	CFfexFtdcRspUserLoginField *pRspUserLogin, CFfexFtdcRspInfoField *pRspInfo,
	int nRequestID, bool bIsLast)
{
	m_MarketProvider->PriOnLogin();
}

void CZJMarketDataHandler::OnRspUserLogout(
	CFfexFtdcRspUserLogoutField *pRspUserLogout,
	CFfexFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	m_MarketProvider->PriOnLogout();
}

void CZJMarketDataHandler::OnRtnDepthMarketData(
	CFfexFtdcDepthMarketDataField *pDepthMarketData)
{
	Quote quote;
	DoM dom;
	FIX::Message msg;
	exchangeadapter_zhongjin::UZJDataConvertHelper::
		TranslateQuoteDataToPublicData(pDepthMarketData,quote,dom);
		
	//m_MarketProvider->OnQuoteRecieve(quote);
	//m_MarketProvider->OnDoMRecieve(dom);
	FixDataConvertHelper::InternalToFix(RMMS_NS::RMMS_CN_ZJ,dom,&msg);
	exchangeadapter_zhongjin::CZJMarketDataProvider* provider = dynamic_cast<
		exchangeadapter_zhongjin::CZJMarketDataProvider*>(
		m_MarketProvider);
	provider->OnRspMsg(msg);
}