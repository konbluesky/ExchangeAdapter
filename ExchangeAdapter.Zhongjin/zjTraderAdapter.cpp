#include "ExchangeAdapterConfig.h"
#include "ZJTraderAdapterHandler.h"
#include "zjDataConvertHelper.h"
#include "debugutils.h"
#define TIXML_USE_STL
#include "tinyxml.h"
#include "FixDataConvertHelper.h"

using namespace std;

namespace exchangeadapter_zhongjin
{
	/* 临时测试 */
	double mystatic[100] = {0.0};
	int staticcount = 0;

	ZJTraderProvider::ZJTraderProvider(void)
	{
	}

	ZJTraderProvider::~ZJTraderProvider()
	{
	}

	tstring ZJTraderProvider::GetName()
	{
		return "CFFEXTraderAdapter";
	}

	void ZJTraderProvider::RequestFix(FIX::Message &msg)
	{
		FIX::MsgType type;
		const FIX::Header &header = msg.getHeader();
		header.getField(type);
		tstring strtype = type.getValue();

		if(FIX::MsgType_NewOrderSingle == strtype){
			Order order;
			if(0 > FixDataConvertHelper::FixToInternal(RMMS_NS::RMMS_CN_ZJ,msg,&order))
				return;
			ReqeustOrderAdd(order);
		} else if(FIX::MsgType_OrderCancelRequest == strtype) {
			OrderCancelRequest cancel;
			if(0 > FixDataConvertHelper::FixToInternal(RMMS_NS::RMMS_CN_ZJ,msg,&cancel))
				return;
			RequestOrderCancel(cancel);
		}
	}

	void ZJTraderProvider::ReqeustOrderAdd(const Order& order)
	{
		CFfexFtdcInputOrderField pApiOrder;
		UZJDataConvertHelper::TranslatePublicDataToOrderData(&pInitParam,order,
			&pApiOrder);

		DBG_ASSERT(NULL != trader);
		
		/* 测试项 */
		mystatic[staticcount] = DBG_QueryTimeTickImp();
		staticcount++;
		/* 测试项 */

		int result = trader->ReqOrderInsert(&pApiOrder, order.zjpri.IDRequest);
		
		/* 测试项 */
		if(100 <= staticcount) {
			for(int i = 0;i < 100;i++) {
				std::cout<<"第"<<i<<"次所用时间"<<mystatic[i]<<std::endl;
			}
		}
		/* 测试项 */
		if(result == 0)
			return;

		CFfexFtdcRspInfoField pRspInfo;
		pRspInfo.ErrorID = result;
		std::strcpy(pRspInfo.ErrorMsg,"");

		BusinessReject pClientReport;
		UZJDataConvertHelper::TraslateExecutionReportToPublicData(pApiOrder,
			&pClientReport, &pRspInfo, order.zjpri.IDRequest);
		//OnBussinessReject(pClientReport);
		FIX::Message msg;
		FixDataConvertHelper::InternalToFix(RMMS_NS::RMMS_CN_ZJ,pClientReport,&msg);
		OnResponseMsg(msg);
	}

	void ZJTraderProvider::RequestOrderCancel(const OrderCancelRequest& order)
	{
		CFfexFtdcOrderActionField pApiOrder;
		UZJDataConvertHelper::TranslatePublicDataToCancelOrderData(&pInitParam,
			order, &pApiOrder);

		DBG_ASSERT(NULL != trader);
		int result = trader->ReqOrderAction(&pApiOrder, order.zjpri.IDRequest);
		if(0 == result)
			return;

		CFfexFtdcRspInfoField pRspInfo;
		pRspInfo.ErrorID = result;
		strcpy(pRspInfo.ErrorMsg, "");

		OrderCancelReject pClientReport;
		UZJDataConvertHelper::TraslateExecutionReportToPublicData(pApiOrder,
			&pClientReport, &pRspInfo, order.zjpri.IDRequest);
		//OnOrderCancelReject(pClientReport);
		FIX::Message msg;
		FixDataConvertHelper::InternalToFix(RMMS_NS::RMMS_CN_ZJ,pClientReport,&msg);
		OnResponseMsg(msg);
	}

	void ZJTraderProvider::RequestOrderModify(
		const OrderCancelReplaceRequest& order)
	{
		CFfexFtdcOrderActionField pApiOrder;

		UZJDataConvertHelper::TranslatePublicDataToCancelRelaceOrderData(order,
			&pApiOrder);

		DBG_ASSERT(NULL != trader);
		int result = trader->ReqOrderAction(&pApiOrder, order.IDRequest);
		if(result == 0)
			return;

		CFfexFtdcRspInfoField pRspInfo;
		pRspInfo.ErrorID = result;
		strcpy(pRspInfo.ErrorMsg, "");

		OrderCancelReplaceReject pClientReport;
		UZJDataConvertHelper::TraslateExecutionReportToPublicData(pApiOrder,
			&pClientReport, &pRspInfo, order.IDRequest);
		//OnOrderCancelReplaceReject(pClientReport);
		FIX::Message msg;
		FixDataConvertHelper::InternalToFix(RMMS_NS::RMMS_CN_ZJ,pClientReport,&msg);
		OnResponseMsg(msg);
	}

#if 0
	/************************ following functions implemented by gyliu *****************************/

	// 下单查询
	void ZJTraderProvider::RequestQryOrder(CFfexFtdcQryOrderField *pQryOrder, int nRequestID)
	{
		trader->ReqQryOrder(pQryOrder, nRequestID);
	}

	// 改密码
	void ZJTraderProvider::ReqUserPasswordUpdate(CFfexFtdcUserPasswordUpdateField *pUserPasswordUpdate, int nRequestID)
	{
		trader->ReqUserPasswordUpdate(pUserPasswordUpdate, nRequestID);
	}

	// 宣告录入请求，这个东西似乎只适用于期权，后期可以删掉……
	void ZJTraderProvider::ReqExecOrderInsert(CFfexFtdcInputExecOrderField *pInputExecOrder, int nRequestID)
	{
		trader->ReqExecOrderInsert(pInputExecOrder, nRequestID);
	}

	// 执行宣告操作请求，应该也是只用于期权
	void ZJTraderProvider::ReqExecOrderAction(CFfexFtdcExecOrderActionField *pExecOrderAction, int nRequestID)
	{
		trader->ReqExecOrderAction(pExecOrderAction, nRequestID);
	}

	// 查询会员资金余额
	void ZJTraderProvider::ReqQryPartAccount(CFfexFtdcQryPartAccountField *pQryPartAccount, int nRequestID)
	{
		trader->ReqQryPartAccount(pQryPartAccount, nRequestID);
	}

	// 报价查询请求
	void ZJTraderProvider::ReqQryQuote(CFfexFtdcQryQuoteField *pQryQuote, int nRequestID)
	{
		trader->ReqQryQuote(pQryQuote, nRequestID);
	}

	// 成交单查询请求
	void ZJTraderProvider::ReqQryTrade(CFfexFtdcQryTradeField *pQryTrade, int nRequestID)
	{
		trader->ReqQryTrade(pQryTrade, nRequestID);
	}

	// 会员客户查询请求
	void ZJTraderProvider::ReqQryClient(CFfexFtdcQryClientField *pQryClient, int nRequestID)
	{
		trader->ReqQryClient(pQryClient, nRequestID);
	}

	// 会员持仓查询请求
	void ZJTraderProvider::ReqQryPartPosition(CFfexFtdcQryPartPositionField *pQryPartPosition, int nRequestID)
	{
		trader->ReqQryPartPosition(pQryPartPosition, nRequestID);
	}

	// 客户持仓查询请求
	void ZJTraderProvider::ReqQryClientPosition(CFfexFtdcQryClientPositionField *pQryClientPosition, int nRequestID)
	{
		trader->ReqQryClientPosition(pQryClientPosition, nRequestID);
	}

	// 合约查询请求
	void ZJTraderProvider::ReqQryInstrument(CFfexFtdcQryInstrumentField *pQryInstrument, int nRequestID)
	{
		trader->ReqQryInstrument(pQryInstrument, nRequestID);
	}

	// 合约交易状态查询请求
	void ZJTraderProvider::ReqQryInstrumentStatus(CFfexFtdcQryInstrumentStatusField *pQryInstrumentStatus, int nRequestID)
	{
		trader->ReqQryInstrumentStatus(pQryInstrumentStatus, nRequestID);
	}

	// 客户端发出普通行情查询请求
	void ZJTraderProvider::ReqQryMarketData(CFfexFtdcQryMarketDataField *pQryMarketData, int nRequestID)
	{
		trader->ReqQryMarketData(pQryMarketData, nRequestID);
	}

	// 交易所公告查询请求
	void ZJTraderProvider::ReqQryBulletin(CFfexFtdcQryBulletinField *pQryBulletin, int nRequestID)
	{
		trader->ReqQryBulletin(pQryBulletin, nRequestID);
	}

	// 合约价位查询请求
	void ZJTraderProvider::ReqQryMBLMarketData(CFfexFtdcQryMBLMarketDataField *pQryMBLMarketData, int nRequestID)
	{
		trader->ReqQryMBLMarketData(pQryMBLMarketData, nRequestID);
	}

	// 保值额度查询请求
	void ZJTraderProvider::ReqQryHedgeVolume(CFfexFtdcQryHedgeVolumeField *pQryHedgeVolume, int nRequestID)
	{
		trader->ReqQryHedgeVolume(pQryHedgeVolume, nRequestID);
	}

	// 通过AdminOrder，初始化、调整以及取消信用限额。在初始化以前，不能进行信用额度的调整。允许不取消信用额度而直接重新设置。额度调整的额度分为正负值，正值表示增加额度，负值表示减少额度。
	// 取消信用额度相当于将信用额度清零，取消信用额度总是允许操作的。
	void ZJTraderProvider::ReqAdminOrderInsert(CFfexFtdcInputAdminOrderField *pInputAdminOrder, int nRequuestID)
	{
		trader->ReqAdminOrderInsert(pInputAdminOrder, nRequuestID);
	}

	// 信用额度查询请求
	void ZJTraderProvider::ReqQryCreditLimit(CFfexFtdcQryCreditLimitField *pQryCreaditLimit, int nRequestID)
	{
		trader->ReqQryCreditLimit(pQryCreaditLimit, nRequestID);
	}

	// 客户端发出报价录入请求
	void ZJTraderProvider::ReqQuoteInsert(CFfexFtdcInputQuoteField *pInputQuote, int nRequestID)
	{
		trader->ReqQuoteInsert(pInputQuote, nRequestID);
	}

	// 客户端发出报价操作请求，包括报价的撤销、报价的挂起、报价的激活、报价的修改
	void ZJTraderProvider::ReqQuoteAction(CFfexFtdcQuoteActionField *pQuoteAction, int nRequestID)
	{
		trader->ReqQuoteAction(pQuoteAction, nRequestID);
	}

	/************************ end of gyliu's part ***********************/
#endif

	bool ZJTraderProvider::Initialize( void* pParam)
	{
		pInitParam = *((InitParam*)pParam);
		trader = CFfexFtdcTraderApi::CreateFtdcTraderApi(
			pInitParam.sFlowPath.c_str());

		ZJTraderAdapterHandler *handler = new ZJTraderAdapterHandler(trader,
			this);
		sh = boost::shared_ptr<ZJTraderAdapterHandler>(handler);

		std::strcpy(handler->g_chParticipantID,
			pInitParam.sParticipantID.c_str());
		std::strcpy(handler->g_chUserID,
			pInitParam.sUserID.c_str());
		std::strcpy(handler->g_chPassword,
			pInitParam.sPassword.c_str());

		trader->RegisterSpi(sh.get());
		switch(pInitParam.iResumeType){
		case 0: 
			trader->SubscribePrivateTopic(TERT_RESTART);
			trader->SubscribePublicTopic(TERT_RESTART);
			break;
		case 1: 
			trader->SubscribePrivateTopic(TERT_RESUME);
			trader->SubscribePublicTopic(TERT_RESUME);
			break;
		case 2:
			trader->SubscribePrivateTopic(TERT_QUICK);
			trader->SubscribePublicTopic(TERT_QUICK);
			break;
		}

		for(int i = 0;i < (int)pInitParam.sAddressIps.size();i++){
			std::string straddr = pInitParam.sAddressIps[i];
			int iport = pInitParam.iPorts[i];
			char addressinfo[64];
			std::sprintf(addressinfo,"tcp://%s:%d",straddr.c_str(),iport);
			trader->RegisterFront(addressinfo);
		}

		trader->Init();
		
		return true;
	}

	bool ZJTraderProvider::InitializeByXML( const char* configfile )
	{
		std::pair<bool,ZJTraderProvider::InitParam> param = 
			InitParam::LoadConfig(configfile);
		if (param.first)
			return Initialize(&param.second);
		else
			return false;
	}
		
	std::pair<bool,ZJTraderProvider::InitParam> 
		ZJTraderProvider::InitParam::LoadConfig(const std::string& fname)
	{
		ZJTraderProvider::InitParam param;
		TiXmlDocument doc(fname);
		bool loadOkay = doc.LoadFile();
		if (!loadOkay)
			return std::make_pair(false,param);
		TiXmlNode* pNode = doc.FirstChild("configure");
		TiXmlNode* ele = pNode->FirstChild("sflowpath");
		param.sFlowPath = ele->FirstChild()->Value();
		
		ele = ele->NextSibling();
		TiXmlNode* addressNode;
		while(ele != NULL){
			if(ele->ValueStr() == "saddress"){
				addressNode = ele->FirstChild("saddressip");
				param.sAddressIps.push_back(addressNode->FirstChild()->Value());

				addressNode = ele->FirstChild("iport");
				param.iPorts.push_back(atoi(
					addressNode->FirstChild()->Value()));
			}
			ele = ele->NextSibling();
		}
		
		ele = pNode->FirstChild("sparticipantid");
		param.sParticipantID = ele->FirstChild()->Value();

		ele = pNode->FirstChild("suserid");
		param.sUserID = ele->FirstChild()->Value();

		ele = pNode->FirstChild("spassword");
		param.sPassword = ele->FirstChild()->Value();

		ele = pNode->FirstChild("iresumeType");
		param.iResumeType = atoi(ele->FirstChild()->Value());

		return std::make_pair(true,param);
	}

	void ZJTraderProvider::UnInitialize()
	{		
		trader->Release();
		trader = NULL;
	}

	bool ZJTraderProvider::Login(void* ppara)
	{
		uint32 seqnum = (uint32)(ppara);
		CFfexFtdcReqUserLoginField reqUserLogin;
		std::strcpy(reqUserLogin.ParticipantID,sh->g_chParticipantID);
		std::strcpy(reqUserLogin.UserID, sh->g_chUserID);
		std::strcpy(reqUserLogin.Password,sh->g_chPassword);
		std::strcpy(reqUserLogin.UserProductInfo, "RMMSoft(v1.0.0)");

		if(0 > trader->ReqUserLogin(&reqUserLogin,seqnum))
			return false;
		return true;
	}

	bool ZJTraderProvider::Logout(void* ppara)
	{
		uint32 seqnum = (uint32)(ppara);
		CFfexFtdcReqUserLogoutField reqUserLogout;
		std::strcpy(reqUserLogout.ParticipantID,sh->g_chParticipantID);
		std::strcpy(reqUserLogout.UserID, sh->g_chUserID);

		if(0 > trader->ReqUserLogout(&reqUserLogout,seqnum))
			return false;
		return true;
	}
}