#ifndef __MACROINSTANCEDEF_H__
#define __MACROINSTANCEDEF_H__

#include "ZJMarketDataProvider.h"
#include "zjTraderAdapter.h"

#define __MARKETCLASS exchangeadapter_zhongjin::CZJMarketDataProvider

#define __TRADERCLASS exchangeadapter_zhongjin::ZJTraderProvider

#endif