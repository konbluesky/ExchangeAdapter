#ifndef __ZJ_MARKETADAPTER_H__
#define __ZJ_MARKETADAPTER_H__

#include "ZJLib.h"
#include "IFixMarket.h"
#include <vector>
#include "autoptr.h"
#include "ExchangeAdapterBase.h"

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4251)
#pragma warning(disable:4275)
#else
#error "不应该出现,以待其他"
#endif

class CFfexFtdcMduserApi;
class CZJMarketDataHandler;

namespace exchangeadapter_zhongjin
{
	class EXAPI CZJMarketDataProvider : public IFixMarketProvider
	{
	public:
		struct InitParam
		{
			tstring sFlowPath;
			std::vector<std::string> sAddressIps;
			std::vector<int> iPorts;
			tstring sParticipantID;
			tstring sUserID;
			tstring sPassword;
			int32 iResumeType;
			
			EXAPI static std::pair<bool,CZJMarketDataProvider::InitParam> 
				LoadConfig(const tstring& fname);	
		};
		
	public:
		CZJMarketDataProvider(void);
		virtual ~CZJMarketDataProvider(void);
	public:
		virtual tstring CZJMarketDataProvider::GetName();
		virtual bool Initialize(void*);
		virtual bool InitializeByXML(const char* configfile);
		virtual void UnInitialize();
		virtual bool Login(void*);
		virtual bool Logout(void*);
		virtual bool SupportDoM();
		
		void OnRspMsg(const FIX::Message &msg);

	private:
		CFfexFtdcMduserApi* pUserApi;
		boost::shared_ptr<CZJMarketDataHandler> pPriceHandler;
	};	 
}

#ifdef _MSC_VER
#pragma warning(pop)
#else
#error "仅支持VC,以待修改"
#endif

#endif