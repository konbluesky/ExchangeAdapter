#ifndef __ZJTraderAdapterHandler_H
#define __ZJTraderAdapterHandler_H

#include "ZJLib.h"
#include "Mutex.h"

class IFixTraderProvider;

namespace exchangeadapter_zhongjin
{
	class ZJTraderAdapterHandler : public CFfexFtdcTraderSpi
	{
	public:
		ZJTraderAdapterHandler(CFfexFtdcTraderApi *pUserApi,
			IFixTraderProvider *traderProvider);
		~ZJTraderAdapterHandler();

		virtual void OnFrontConnected();
		virtual void OnFrontDisconnected(int nReason);
		virtual void OnRspUserLogin(CFfexFtdcRspUserLoginField *pRspUserLogin,
			CFfexFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast);
		virtual void OnRspUserLogout(
			CFfexFtdcRspUserLogoutField *pRspUserLogout,
			CFfexFtdcRspInfoField *pRspInfo,
			int nRequestID,bool bIsLast);


		virtual void OnRspOrderInsert(CFfexFtdcInputOrderField *pInputOrder,
			CFfexFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast);
		virtual void OnRspOrderAction(CFfexFtdcOrderActionField *pOrderAction,
			CFfexFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast);
		virtual void OnRtnOrder(CFfexFtdcOrderField *pOrder);
		virtual void OnRtnTrade(CFfexFtdcTradeField *pTrade);
		virtual void OnRspError(CFfexFtdcRspInfoField *pRspInfo, int nRequestID,
			bool bIsLast);
		virtual void OnRtnBulletin(CFfexFtdcBulletinField *pBulletin);

	public:
		TFfexFtdcParticipantIDType g_chParticipantID;
		TFfexFtdcUserIDType g_chUserID;
		TFfexFtdcPasswordType g_chPassword;

	private:
		CFfexFtdcTraderApi *m_pUserApi;
		IFixTraderProvider *m_TraderProvider;
	};
}
#endif