#include "FixDataConvertHelper42.h"
#include "FixFieldTrans.h"
#include "LocalCompile.h"

/***
 * 对于响应单,完全按照FIX协议来实现即可.
 * 对于国内的数据,尽量按照各个交易所的类型来区分.
 */
void FixDataConvertHelper42::InternalToFix_Insert(RMMS_NS::Exchange exch,
	const ExecutionReport &rsporderinsert,FIX42::Message *pmsg)
{
	pmsg->setField(FIX::AvgPx((double)rsporderinsert.PriceLimit));
	/* pmsg->setField(FIX::Account(rsporderinsert.IDClient));*/
	switch(exch){
	case RMMS_NS::RMMS_CN_ZZ:
		pmsg->setField(FIX::ClOrdID(rsporderinsert.zzIDLocalOrder));
		break;
	case RMMS_NS::RMMS_CN_ZJ:
		pmsg->setField(FIX::ClOrdID(rsporderinsert.zjIDLocalOrder));
		break;
	case RMMS_NS::RMMS_CN_DL:
		{
			tchar buff[11] = {0};
			std::sprintf(buff,"%010u",rsporderinsert.dlIDLocalOrder);
			pmsg->setField(FIX::ClOrdID(buff));
		}
		break;
	}
	pmsg->setField(FIX::OrderID(rsporderinsert.IDSysOrder));
	pmsg->setField(FIX::ExecID(" "));
	pmsg->setField(FIX::ExecTransType(FIX::ExecTransType_NEW)); //全部为ExecTransType_NEW
	pmsg->setField(FIX::OrdStatus(FIX::OrdStatus_NEW));
	pmsg->setField(FIX::ExecType(FIX::ExecType_NEW));  //与OrdStats一样
	FixFieldTrans::Side_I2F(rsporderinsert.Side,pmsg);
	pmsg->setField(FIX::OrderQty((double(rsporderinsert.QtyOrder))));
	pmsg->setField(FIX::CumQty((double)0));
	pmsg->setField(FIX::LeavesQty(double(rsporderinsert.QtyOrder)));
	pmsg->setField(FIX::LastShares((double)0));
	tstring symbol;
	SecurityDescToSymbol(rsporderinsert.SecurityDesc, &symbol);
	pmsg->setField(FIX::Symbol(symbol));
	FIX::UtcTimeStamp timestamp(&(rsporderinsert.datetime.GetTmUtc()),
		rsporderinsert.datetime.GetMillisecond());
	pmsg->setField(FIX::TransactTime(timestamp));
	//pmsg->setField(FIX::SecurityDesc(rsporderinsert.SecurityDesc));

	tstring securitydesc;
	FixFieldTrans::SecDesc_I2F(exch,rsporderinsert.SecurityDesc, &symbol,
		&securitydesc);
	pmsg->setField(FIX::SecurityDesc(securitydesc));
}

void FixDataConvertHelper42::InternalToFix_Cancel(RMMS_NS::Exchange exch,
	const ExecutionReport &rspordercancel,FIX42::Message *pmsg)
{
	pmsg->setField(FIX::AvgPx((double)rspordercancel.PriceLimit));
	/* pmsg->setField(FIX::Account(rspordercancel.IDClient));*/
	switch(exch){
	case RMMS_NS::RMMS_CN_ZZ:
		pmsg->setField(FIX::ClOrdID(rspordercancel.zzIDLocalOrder));
		break;
	case RMMS_NS::RMMS_CN_ZJ:
		pmsg->setField(FIX::ClOrdID(rspordercancel.zjIDLocalOrder));
		break;
	case RMMS_NS::RMMS_CN_DL:
		{
			tchar buff[11] = {0};
			std::sprintf(buff,"%010u",rspordercancel.dlIDLocalOrder);
			pmsg->setField(FIX::ClOrdID(buff));
		}
		break;
	}
	pmsg->setField(FIX::OrderID(rspordercancel.IDSysOrder));
	pmsg->setField(FIX::ExecID(" "));
	pmsg->setField(FIX::ExecTransType(FIX::ExecTransType_NEW)); //全部为ExecTransType_NEW
	pmsg->setField(FIX::OrdStatus(FIX::OrdStatus_CANCELED));
	pmsg->setField(FIX::ExecType(FIX::ExecType_CANCELLED));  //与OrdStats一样
	FixFieldTrans::Side_I2F(rspordercancel.Side,pmsg);
	pmsg->setField(FIX::ExecType(FIX::ExecType_CANCELLED));
	pmsg->setField(FIX::OrderQty((double)rspordercancel.QtyOrder));
	pmsg->setField(FIX::CumQty((double)0));
	pmsg->setField(FIX::LeavesQty((double)rspordercancel.QtyOrder));
	pmsg->setField(FIX::LastShares((double)0));
	tstring symbol;
	SecurityDescToSymbol(rspordercancel.SecurityDesc, &symbol);
	pmsg->setField(FIX::Symbol(symbol));
	FIX::UtcTimeStamp timestamp(&(rspordercancel.datetime.GetTmUtc()),
		rspordercancel.datetime.GetMillisecond());
	pmsg->setField(FIX::TransactTime(timestamp));
	//pmsg->setField(FIX::SecurityDesc(rspordercancel.SecurityDesc));
	
	tstring securitydesc;
	FixFieldTrans::SecDesc_I2F(exch,rspordercancel.SecurityDesc, &symbol, &securitydesc);
	pmsg->setField(FIX::SecurityDesc(securitydesc));
}

void FixDataConvertHelper42::InternalToFix(RMMS_NS::Exchange exch,
	const ExecutionReport &exereport,FIX42::Message *pmsg)
{
	FIX::Header &header = pmsg->getHeader();
	header.setField(FIX::MsgType(FIX::MsgType_ExecutionReport));

	switch(exereport.OrderStatus)
	{
	case OrderStatusFlag::New:
		InternalToFix_Insert(exch,exereport,pmsg);
		break;
	case OrderStatusFlag::Canceled:
		InternalToFix_Cancel(exch,exereport,pmsg);
		break;
	}
}

void FixDataConvertHelper42::InternalToFix(RMMS_NS::Exchange exch,
	const FillReport& fillreport,FIX42::Message *pmsg)
{
	FIX::Header &header = pmsg->getHeader();
	header.setField(FIX::MsgType(FIX::MsgType_ExecutionReport));

	pmsg->setField(FIX::LastPx((double)fillreport.LastPx));
	pmsg->setField(FIX::AvgPx(0));
	switch(exch){
	case RMMS_NS::RMMS_CN_ZZ:
		pmsg->setField(FIX::ClOrdID(fillreport.zzIDLocalOrder));
		break;
	case RMMS_NS::RMMS_CN_ZJ:
		pmsg->setField(FIX::ClOrdID(fillreport.zjIDLocalOrder));
	case RMMS_NS::RMMS_CN_DL:
		{
			tchar buff[11] = {0};
			std::sprintf(buff,"%010u",fillreport.dlIDLocalOrder);
			pmsg->setField(FIX::ClOrdID(buff));
		}
		break;
	}
	pmsg->setField(FIX::OrderID(fillreport.IDSysOrder));
	pmsg->setField(FIX::ExecID(fillreport.IDExec));
	pmsg->setField(FIX::ExecTransType(FIX::ExecTransType_NEW)); //全部为ExecTransType_NEW
	//pmsg->setField(FIX::OrdStatus("")); //需要由路由器根据成交数量来判定
	//pmsg->setField(FIX::ExecType("")); //与OrdStatus状态一样，由路由器负责填写
	FixFieldTrans::Side_I2F(fillreport.Side,pmsg);
	pmsg->setField(FIX::Account(fillreport.IDClient));
	pmsg->setField(FIX::LastShares(fillreport.QtyLastShares));
	//pmsg->setField(FIX::OrderQty((double(fillreport.QtyOrder)))); //需要由路由器提供
	//pmsg->setField(FIX::CumQty((double)rsporderinsert.QtyCum)); //需要由路由器提供
	//pmsg->setField(FIX::LeavesQty(double(fillreport.QtyLeaves)));  //需要由路由器提供
	tstring symbol;
	SecurityDescToSymbol(fillreport.SecurityDesc, &symbol);
	pmsg->setField(FIX::Symbol(symbol));
	FIX::UtcTimeStamp timestamp(&(fillreport.TradeTime.GetTmUtc()),
		fillreport.TradeTime.GetMillisecond());
	pmsg->setField(FIX::TransactTime(timestamp));
	//pmsg->setField(FIX::SecurityDesc(fillreport.SecurityDesc));
	
	tstring securitydesc;
	FixFieldTrans::SecDesc_I2F(exch,fillreport.SecurityDesc, &symbol, &securitydesc);
	pmsg->setField(FIX::SecurityDesc(securitydesc));
}

void FixDataConvertHelper42::InternalToFix(RMMS_NS::Exchange exch,
	const OrderCancelReject &cancelreject,FIX42::Message *pmsg)
{
	FIX::Header &header = pmsg->getHeader();
	header.setField(FIX::MsgType(FIX::MsgType_OrderCancelReject));
	switch(exch){
	case RMMS_NS::RMMS_CN_ZZ:
		pmsg->setField(FIX::ClOrdID(cancelreject.zzIDLocalOrder));
		break;
	case RMMS_NS::RMMS_CN_ZJ:
		pmsg->setField(FIX::MsgSeqNum(cancelreject.LastMsgSeqNumProcessed));
		break;
	case RMMS_NS::RMMS_CN_DL:
		{
			tchar buff[11] = {0};
			std::sprintf(buff,"%010u",cancelreject.dlIDLocalOrder);
			pmsg->setField(FIX::ClOrdID(buff));
		}
		break;
	}

	pmsg->setField(FIX::OrderID(cancelreject.IDSysOrder));
	pmsg->setField(FIX::CxlRejResponseTo('1'));
	pmsg->setField(FIX::Text(cancelreject.ErrorSource + cancelreject.ErrorCode));
}

void FixDataConvertHelper42::InternalToFix(RMMS_NS::Exchange exch,
	const OrderCancelReplaceReject &cancelreplacereject,FIX42::Message *pmsg)
{
	switch(exch){
	case RMMS_NS::RMMS_CN_ZZ:
		pmsg->setField(FIX::ClOrdID(cancelreplacereject.zzIDLocalOrder));
		break;
	}
	
	pmsg->setField(FIX::OrderID(cancelreplacereject.IDSysOrder));

	//1 = Order Cancel Request
	//2 = Order Cancel/Replace Request
	pmsg->setField(FIX::CxlRejResponseTo('2'));
	
	pmsg->setField(FIX::Text(cancelreplacereject.ErrorSource + cancelreplacereject.ErrorCode));
}

void FixDataConvertHelper42::InternalToFix(RMMS_NS::Exchange exch,
	const BusinessReject &bizreject,FIX42::Message *pmsg)
{
	FIX::Header &header = pmsg->getHeader();
	header.setField(FIX::MsgType(FIX::MsgType_BusinessMessageReject));
	pmsg->setField(FIX::Text(bizreject.ErrorSource + bizreject.ErrorCode));
	pmsg->setField(FIX::RefMsgType(bizreject.RefMsgType));
	switch(exch){
	case RMMS_NS::RMMS_CN_ZZ:
		pmsg->setField(FIX::ClOrdID(bizreject.zzIDLocalOrder));
		break;
	case RMMS_NS::RMMS_CN_ZJ:
		pmsg->setField(FIX::ClOrdID(bizreject.zjIDLocalOrder));
		pmsg->setField(FIX::RefSeqNum(bizreject.LastMsgSeqNumProcessed));
		break;
	case RMMS_NS::RMMS_CN_DL:
		{
			tchar buff[11] = {0};
			std::sprintf(buff,"%010u",bizreject.dlIDLocalOrder);
			pmsg->setField(FIX::ClOrdID(buff));
		}		
		break;
	}
}

int32 FixDataConvertHelper42::FixToInternal(RMMS_NS::Exchange exch,
	const FIX42::Message &msg,Order *porder)
{
	const FIX::Header &header = msg.getHeader();
	
	/*** 要求必带 ***/
	FIX::Account account;
	msg.getField(account);
	porder->IDClient = account.getValue();

	FIX::ClOrdID clordid;
	msg.getField(clordid);
	switch(exch){
	case RMMS_NS::RMMS_CN_ZJ:
		std::strcpy(porder->zjIDLocalOrder,clordid.getValue().c_str());
		break;
	case RMMS_NS::RMMS_CN_ZZ:
		std::strcpy(porder->zzIDLocalOrder,clordid.getValue().c_str());
		break;
	case RMMS_NS::RMMS_CN_DL:
		porder->dlIDLocalOrder = std::atoi(clordid.getValue().c_str());
		break;
	}

	FIX::OrdType orderType;
	msg.getField(orderType);
	FixFieldTrans::OrderType_F2I(orderType,&(porder->OrderType));

	FIX::Side side;
	msg.getField(side);
	FixFieldTrans::Side_F2I(side,&(porder->Side));

	FIX::SecurityDesc desc;
	msg.getField(desc);
	FixFieldTrans::SecDesc_F2I(exch,desc.getValue(),&porder->SecurityDesc);

	switch(exch){
	case RMMS_NS::RMMS_CN_ZJ:
		FIX::MsgSeqNum seqnum;
		header.getField(seqnum);
		porder->zjpri.IDRequest = seqnum.getValue();
		break;
	}
	

	FIX::TimeInForce timeinforce;
	msg.getField(timeinforce);
	switch(exch)
	{
	case RMMS_NS::RMMS_CN_ZJ:
		FixFieldTrans::TimeInForce_F2I(timeinforce,
			&(porder->zjpri.TimeCondition));
		break;
	case RMMS_NS::RMMS_CN_ZZ:
		FixFieldTrans::TimeInForce_F2I(timeinforce,
			&(porder->zzpri.TimeCondition));
		break;
	default:
		break;
	}
	
	FIX::OpenClose openclose;
	msg.getField(openclose);
	FixFieldTrans::OpenClose_F2I(openclose,&(porder->OpenClose));

	msg.isSetField(FIX::FIELD::OrderQty);
	FIX::OrderQty orderqty;
	msg.getField(orderqty);
	FixFieldTrans::OrderQty_F2I(orderqty,&(porder->Quantity));

	/* 有条件携带 */
	switch(porder->OrderType){
	case Limit:
		{
			FIX::Price price;
			msg.getField(price);
			porder->LimitPrice = price.getValue();
		}
		break;
	}

	return 0;
}

int32 FixDataConvertHelper42::FixToInternal(RMMS_NS::Exchange exch,
	const FIX42::Message &msg,OrderCancelRequest *pcancel)
{
	const FIX::Header &header = msg.getHeader();

	/* 协议必带 */
	FIX::Account account;
	msg.getField(account);
	pcancel->IDClient = account.getValue();

	FIX::ClOrdID ordid;
	msg.getField(ordid);
	switch(exch){
	case RMMS_NS::RMMS_CN_ZJ:
		std::strcpy(pcancel->zjIDLocalOrder,ordid.getValue().c_str());
		break;
	case RMMS_NS::RMMS_CN_ZZ:
		std::strcpy(pcancel->zzIDLocalOrder,ordid.getValue().c_str());
		break;
	case RMMS_NS::RMMS_CN_DL:
		pcancel->dlIDLocalOrder = std::atoi(ordid.getValue().c_str());
		break;
	}

	FIX::OrderID orderid;
	msg.getField(orderid);
	pcancel->IDSysOrder = orderid.getValue();

	// OrigClOrdID国内不需要,FIX对齐

	// Side国内不需要,FIX对齐

	// Symbol国内不需要,FIX对齐

	// TransactTime国内不需要,FIX对齐

	// CorrelationClOrdID国内不需要,FIX对齐

	FIX::SecurityDesc desc;
	msg.getField(desc);
	//pcancel->IDInstrument = desc.getValue();
	FixFieldTrans::SecDesc_F2I(exch,desc.getValue(),&pcancel->IDInstrument);

	/* ZJ必须 */
	FIX::MsgSeqNum seqnum;
	header.getField(seqnum);
	pcancel->zjpri.IDRequest = seqnum.getValue();

	/* 郑州交易所必须 */
#if 0
	if(msg.isSetField(FIX::FIELD::OrdType)){
		FIX::OrdType orderType;
		msg.getField(orderType);
		FixFieldTrans::OrderType_F2I(orderType,&(pcancel->orderType));
	}
#endif
	return 0;
}

void FixDataConvertHelper42::SecurityDescToSymbol(
	const tstring securitydesc, tstring *p_symbol)
{
	tstring symbol = securitydesc.substr(0, securitydesc.size() - 2);
	*p_symbol = symbol;
}