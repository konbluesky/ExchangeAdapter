#include "ZJTraderAdapterHandler.h"
#include "ChinaOrderDefinition.h"
#include "zjDataConvertHelper.h"
#include "debugutils.h"
#include "FixDataConvertHelper.h"
#include "LocalCompile.h"
using namespace std;

namespace exchangeadapter_zhongjin
{
	static const char* OrderSatusString[] =
	{
		"NULSTRING"
		"Working"
		"Completed"
		"Rejected"
		"Cancelled"
		"Held"
		"PartiallyFilled"
		"Replaced"
		"Pending"
		"Expired"
		"CancelPending"
		"ReplacePending"
	};

	ZJTraderAdapterHandler::ZJTraderAdapterHandler(
		CFfexFtdcTraderApi *pUserApi,IFixTraderProvider *traderProvider) 
		: m_pUserApi(pUserApi), m_TraderProvider(traderProvider)
	{}

	ZJTraderAdapterHandler::~ZJTraderAdapterHandler(){}

	void ZJTraderAdapterHandler::OnFrontConnected()
	{
		m_TraderProvider->PriOnNetConnected();
	}

	void ZJTraderAdapterHandler::OnFrontDisconnected(int nReason)
	{
		DBG_TRACE(("<OnFrontDisconnected> be called. Reason=[%d]\n", nReason));
		m_TraderProvider->PriOnNetDisconnected();
	}

	void ZJTraderAdapterHandler::OnRspUserLogin(
		CFfexFtdcRspUserLoginField *pRspUserLogin,
		CFfexFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
	{
		if (0 != pRspInfo->ErrorID)
			DBG_TRACE(("Failed to login, errorcode=%d errormsg=%s "
				"requestid=%d chain=%d", pRspInfo->ErrorID, pRspInfo->ErrorMsg,
				nRequestID, bIsLast));
		else {
			m_TraderProvider->PriOnLogin();
			DBG_TRACE(("�н��½�ɹ�"));
		}
	}

	void ZJTraderAdapterHandler::OnRspUserLogout(
		CFfexFtdcRspUserLogoutField *pRspUserLogout,
		CFfexFtdcRspInfoField *pRspInfo,int nRequestID,bool bIsLast)
	{
		m_TraderProvider->PriOnLogout();		
	}

	void ZJTraderAdapterHandler::OnRspOrderInsert(
		CFfexFtdcInputOrderField *pInputOrder,CFfexFtdcRspInfoField *pRspInfo,
		int nRequestID, bool bIsLast)
	{
		if(0 == pRspInfo->ErrorID){
			ExecutionReport pClientReport;
			UZJDataConvertHelper::TraslateExecutionReportToPublicData(
				*pInputOrder, &pClientReport);
			//m_TraderProvider->OnOrderAddResponse(pClientReport);
			FIX::Message msg;
			FixDataConvertHelper::InternalToFix(RMMS_NS::RMMS_CN_ZJ,pClientReport,
				&msg);
			m_TraderProvider->OnResponseMsg(msg);
		}else{
			BusinessReject pClientReport;
			UZJDataConvertHelper::TraslateExecutionReportToPublicData(
				*pInputOrder, &pClientReport, pRspInfo, nRequestID);
			//m_TraderProvider->OnBussinessReject(pClientReport);
			FIX::Message msg;
			FixDataConvertHelper::InternalToFix(RMMS_NS::RMMS_CN_ZJ,pClientReport,
				&msg);
			m_TraderProvider->OnResponseMsg(msg);
		}
	}

	void ZJTraderAdapterHandler::OnRtnOrder(CFfexFtdcOrderField *pOrder)
	{
		ExecutionReport pClientReport;
		FIX::Message msg;
		std::memset(&pClientReport,0,sizeof(ExecutionReport));
		switch(pOrder->OrderStatus){
		case FFEX_FTDC_OST_AllTraded:
		case FFEX_FTDC_OST_PartTradedQueueing:
		case FFEX_FTDC_OST_PartTradedNotQueueing:
		case FFEX_FTDC_OST_NoTradeNotQueueing:
			break;
		case FFEX_FTDC_OST_Canceled:
			{
				UZJDataConvertHelper::TraslateExecutionReportToPublicData(
				*pOrder, &pClientReport);
			//m_TraderProvider->OnOrderAddResponse(pClientReport);
			FixDataConvertHelper::InternalToFix(RMMS_NS::RMMS_CN_ZJ,pClientReport,
				&msg);
			m_TraderProvider->OnResponseMsg(msg);
			}
			break;
		case FFEX_FTDC_OST_NoTradeQueueing:
			break;
		}
	}

	void ZJTraderAdapterHandler::OnRtnTrade(CFfexFtdcTradeField *pTrade)
	{
		FillReport pClientReport;
		UZJDataConvertHelper::TraslateExecutionReportToPublicData(
			*pTrade, &pClientReport);
		//m_TraderProvider->OnOrderFillResponse(pClientReport);
		FIX::Message msg;
		FixDataConvertHelper::InternalToFix(RMMS_NS::RMMS_CN_ZJ,pClientReport,
			&msg);
		m_TraderProvider->OnResponseMsg(msg);
	}

	void ZJTraderAdapterHandler::OnRspOrderAction(
		CFfexFtdcOrderActionField *pOrderAction,
		CFfexFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
	{
		if(pRspInfo->ErrorID == 0)
			return;
		else if(FFEX_FTDC_AF_Delete == pOrderAction->ActionFlag){
			OrderCancelReject pClientReject;
			UZJDataConvertHelper::TraslateExecutionReportToPublicData(
				*pOrderAction, &pClientReject, pRspInfo, nRequestID);
			//m_TraderProvider->OnOrderCancelReject(pClientReject);
			FIX::Message msg;
			FixDataConvertHelper::InternalToFix(RMMS_NS::RMMS_CN_ZJ,pClientReject,
				&msg);
			m_TraderProvider->OnResponseMsg(msg);
		}else if(FFEX_FTDC_AF_Modify == pOrderAction->ActionFlag){
			OrderCancelReplaceReject pClientReject;
			UZJDataConvertHelper::TraslateExecutionReportToPublicData(
				*pOrderAction, &pClientReject, pRspInfo, nRequestID);
			//m_TraderProvider->OnOrderCancelReplaceReject(pClientReject);
			FIX::Message msg;
			FixDataConvertHelper::InternalToFix(RMMS_NS::RMMS_CN_ZJ,pClientReject,
				&msg);
			m_TraderProvider->OnResponseMsg(msg);
		}
	}

	void ZJTraderAdapterHandler::OnRspError(CFfexFtdcRspInfoField *pRspInfo,
		int nRequestID, bool bIsLast) 
	{
		DBG_ASSERT(false);
	}

	void ZJTraderAdapterHandler::OnRtnBulletin(
		CFfexFtdcBulletinField *pBulletin)
	{
		DBG_TRACE(("<OnRtnBulletin>\n"));
		Bulletin clientBulletin;
		UZJDataConvertHelper::TranslatePublicDataToBulletin(*pBulletin,
			&clientBulletin);
	}
	
}