#include "zjEnumsConvertHelper.h"
#include "StringUtils.h"
#include "LocalCompile.h"

namespace exchangeadapter_zhongjin
{
	

	void UZJEnumsConvert::Convert2ExchOpenClose(OpenCloseFlag rmmOpenClose,TFfexFtdcCombOffsetFlagType& exchOpenClose)
	{
		switch(rmmOpenClose)
		{
		case OpenCloseFlag::Open: 
			{
				LIB_NS::UString::Copy(exchOpenClose, "0");
				break;
			}
		case OpenCloseFlag::Close:
			{
				LIB_NS::UString::Copy(exchOpenClose, "1");
				break;
			}
		default:
			{
				LIB_NS::UString::Copy(exchOpenClose, "0");
				break;
			}
		}
	}

	void UZJEnumsConvert::Convert2ExchOrderType(OrderTypeFlag rmmOrderType, TFfexFtdcOrderPriceTypeType &exchOrderType)
	{
		switch(rmmOrderType)
		{
		case OrderTypeFlag::Market: 
			exchOrderType = FFEX_FTDC_OPT_AnyPrice;
			break;
		case OrderTypeFlag::Limit: 
			exchOrderType = FFEX_FTDC_OPT_LimitPrice;
			break;
		case OrderTypeFlag::Stop:
		case OrderTypeFlag::Stop_Limit:
		default:exchOrderType = FFEX_FTDC_OPT_AnyPrice;
			break;
		}
	}

	void UZJEnumsConvert::Convert2ExchTimeCondition(OrderTypeFlag rmmOrderType, TimeConditionFlag rmmTimeCondition, TFfexFtdcTimeConditionType &exchTimeCondition)
	{
		switch(rmmTimeCondition)
		{
		case TimeConditionFlag::Day: 
			if(rmmOrderType == OrderTypeFlag::Market)
				exchTimeCondition = FFEX_FTDC_TC_IOC;
			else
				exchTimeCondition = FFEX_FTDC_TC_GFD;
			break;
		case TimeConditionFlag::GTC: 
			exchTimeCondition = FFEX_FTDC_TC_GTC;
			break;
		case TimeConditionFlag::IOC:
			exchTimeCondition = FFEX_FTDC_TC_IOC;
			break;
		default:exchTimeCondition = FFEX_FTDC_TC_GFD;
			break;
		}
	}

	void UZJEnumsConvert::Convert2ExchSide(TradeSide rmmTradeSide, TFfexFtdcDirectionType& exchTradSide)
	{
		switch(rmmTradeSide)
		{
		case TradeSide::BUY:
			exchTradSide = FFEX_FTDC_D_Buy;
			break;
		case TradeSide::SELL: 
			exchTradSide = FFEX_FTDC_D_Sell;
			break;
		default: 
			exchTradSide = FFEX_FTDC_D_Buy;
			break;
		}
	}

	void Convert2ExchOrderStatus(OrderStatusFlag rmmOrderStatus, TFfexFtdcOrderStatusType& exchOrderStatus)
	{
		switch(rmmOrderStatus)
		{
		case OrderStatusFlag::New: 
			exchOrderStatus = FFEX_FTDC_OST_NoTradeQueueing;
			break;
		case OrderStatusFlag::Filled: 
			exchOrderStatus = FFEX_FTDC_OST_AllTraded;
			break;
		case OrderStatusFlag::Rejected:
			exchOrderStatus = FFEX_FTDC_OST_NoTradeNotQueueing;
			break;
		case OrderStatusFlag::Canceled:
			exchOrderStatus = FFEX_FTDC_OST_Canceled;
			break;
		//case OrderStatusFlag::Held:
		case OrderStatusFlag::PartiallyFilled:
			exchOrderStatus = FFEX_FTDC_OST_PartTradedNotQueueing;
			break;
		//OrderStatusFlag::Replaced = 7,
		//OrderStatusFlag::Pending = 8,
		//OrderStatusFlag::Expired = 9,
		//OrderStatusFlag::CancelPending = 10,
		//OrderStatusFlag::ReplacePending = 11
		default:
			exchOrderStatus = FFEX_FTDC_OST_NoTradeNotQueueing;
			break;
		}
	}

	void UZJEnumsConvert::Convert2RmmOpenClose(const TFfexFtdcCombOffsetFlagType &exchOpenClose, OpenCloseFlag &rmmOpenClose)
	{
		if(strcmp(exchOpenClose, "0") == 0)
			rmmOpenClose = OpenCloseFlag::Open;
		else if(strcmp(exchOpenClose, "1") == 0)
			rmmOpenClose = OpenCloseFlag::Close;
		else
			rmmOpenClose = OpenCloseFlag::Open;
	}

	void UZJEnumsConvert::Convert2RmmOrderType(TFfexFtdcOrderPriceTypeType exchOrderType, OrderTypeFlag& rmmOrderType)
	{
		switch(exchOrderType)
		{
		case FFEX_FTDC_OPT_AnyPrice: rmmOrderType = OrderTypeFlag::Market;
			break;
		case FFEX_FTDC_OPT_LimitPrice: rmmOrderType = OrderTypeFlag::Limit;
			break;
		default: rmmOrderType = OrderTypeFlag::Market;
			break;
		}
	}

	void UZJEnumsConvert::Convert2RmmSide(TFfexFtdcDirectionType exchSide, TradeSide& rmmTradeSide)
	{
		switch(exchSide)
		{
		case FFEX_FTDC_D_Buy: rmmTradeSide = TradeSide::BUY;
			break;
		case FFEX_FTDC_D_Sell: rmmTradeSide = TradeSide::SELL;
			break;
		default: rmmTradeSide = TradeSide::BUY;
			break;
		}
	}

	void UZJEnumsConvert::Convert2RmmTimeCondition(TFfexFtdcTimeConditionType exchTimeCondition, TimeConditionFlag& rmmTimeCondition)
	{
		switch(exchTimeCondition)
		{
		case FFEX_FTDC_TC_GFD: rmmTimeCondition = TimeConditionFlag::Day;
			break;
		case FFEX_FTDC_TC_GTC: rmmTimeCondition = TimeConditionFlag::GTC;
			break;
		default: rmmTimeCondition = TimeConditionFlag::Day;
			break;
		}
	}

	void UZJEnumsConvert::Convert2RmmOrderStatus(TFfexFtdcOrderStatusType exchOrderStatus, OrderStatusFlag& rmmOrderStatus)
	{
		switch(exchOrderStatus)
		{
		case FFEX_FTDC_OST_AllTraded: rmmOrderStatus = OrderStatusFlag::Filled;
			break;
		case FFEX_FTDC_OST_PartTradedQueueing: rmmOrderStatus = OrderStatusFlag::PartiallyFilled;
			break;
		case FFEX_FTDC_OST_PartTradedNotQueueing: rmmOrderStatus = OrderStatusFlag::PartiallyFilled;
			break;
		case FFEX_FTDC_OST_NoTradeQueueing: rmmOrderStatus = OrderStatusFlag::New;
			break;
		case FFEX_FTDC_OST_NoTradeNotQueueing: rmmOrderStatus = OrderStatusFlag::Rejected;
			break;
		case FFEX_FTDC_OST_Canceled: rmmOrderStatus = OrderStatusFlag::Canceled;
			break;
		default: rmmOrderStatus = OrderStatusFlag::Rejected;
			break;
		}
	}

	void UZJEnumsConvert::Convert2RmmExecType(OrderStatusFlag rmmOrderStauts, ExecTypeFlag& rmmExecType)
	{
		switch(rmmOrderStauts)
		{
		case OrderStatusFlag::New: rmmExecType = ExecTypeFlag::ExecType_New; break;
		case OrderStatusFlag::DoneForDay: rmmExecType = ExecTypeFlag::ExecType_DoneForDay; break;
		case OrderStatusFlag::Canceled: rmmExecType = ExecTypeFlag::ExecType_Canceled; break;
		case OrderStatusFlag::Replaced: rmmExecType = ExecTypeFlag::ExecType_Replaced; break;
		case OrderStatusFlag::PendingCancel: rmmExecType = ExecTypeFlag::ExecType_PendingCancel; break;
		case OrderStatusFlag::Stopped: rmmExecType = ExecTypeFlag::ExecType_Stopped; break;
		case OrderStatusFlag::Rejected: rmmExecType = ExecTypeFlag::ExecType_Rejected; break;
		case OrderStatusFlag::Suspended: rmmExecType = ExecTypeFlag::ExecType_Suspended; break;
		case OrderStatusFlag::PendingNew: rmmExecType = ExecTypeFlag::ExecType_PendingNew; break;
		case OrderStatusFlag::Calculated: rmmExecType = ExecTypeFlag::ExecType_Calculated; break;
		case OrderStatusFlag::Expired: rmmExecType = ExecTypeFlag::ExecType_Expired; break;
		case OrderStatusFlag::PendingReplace: rmmExecType = ExecTypeFlag::ExecType_PendingReplace; break;
		//case OrderStatusFlag::AcceptedForBidding: rmmExecType = ExecTypeFlag::ExecType_Restated; break;
		}
	}
}