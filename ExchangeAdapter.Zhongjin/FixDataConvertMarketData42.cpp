#include "FixDataConvertHelper42.h"
#include "quickfix/fix42/MarketDataSnapshotFullRefresh.h"
#include "stringutils.h"
#include "FixFieldTrans.h"
#include "LocalCompile.h"

void FixDataConvertHelper42::InternalToFix(RMMS_NS::Exchange exch,
	const Quote &quote,FIX42::Message *pmsg)
{

}

static std::string toString(int v)
{
	tchar data[20];
	sprintf(data,"%i",v);
	return data;
}

void FixDataConvertHelper42::InternalToFix(RMMS_NS::Exchange exch,
	const DoM &dom,FIX42::Message *pmsg)
{
	FIX::Header &header = pmsg->getHeader();
	header.setField(FIX::MsgType(FIX::MsgType_MarketDataSnapshotFullRefresh));

	tstring strprofam;
	tstring tdesc;
	FixFieldTrans::SecDesc_I2F(exch,dom.ID,&strprofam,&tdesc);
	pmsg->setField(FIX::FIELD::Symbol,strprofam);
	pmsg->setField(FIX::FIELD::SecurityType,"FUT");
	pmsg->setField(FIX::FIELD::SecurityID,tdesc);
	pmsg->setField(FIX::FIELD::MDReqID,"0");
	pmsg->setField(FIX::FIELD::TotalVolumeTraded,toString(dom.Total_Volume));

	FIX42::MarketDataSnapshotFullRefresh::NoMDEntries group;
	for (int i = 0; i<dom.SupportLevel; ++i)
	{
		group.set(FIX::MDEntryType('1'));
		group.set(FIX::MDEntryPx(dom.AskLevel[i]));
		group.set(FIX::MDEntrySize(dom.AskQuantities[i]));
		group.setField(FIX::MDEntryPositionNo(i+1));
		pmsg->addGroup(group);

		group.set(FIX::MDEntryType('0'));
		group.set(FIX::MDEntryPx(dom.BidLevel[i]));
		group.set(FIX::MDEntrySize(dom.BidQuantities[i]));
		group.setField(FIX::MDEntryPositionNo(i+1));
		pmsg->addGroup(group);
	}

	group.set(FIX::MDEntryType('2'));
	group.set(FIX::MDEntryPx(dom.CurrentPrice));
	group.set(FIX::MDEntrySize(dom.LastQuantity));
	pmsg->addGroup(group);

	group.set(FIX::MDEntryType('4'));
	group.set(FIX::MDEntryPx(dom.Open_Price));
	pmsg->addGroup(group);

	group.set(FIX::MDEntryType('6'));
	group.set(FIX::MDEntryPx(dom.SettlementPrice));
	pmsg->addGroup(group);

	group.set(FIX::MDEntryType('7'));
	group.set(FIX::MDEntryPx(dom.HighPrice));
	pmsg->addGroup(group);

	group.set(FIX::MDEntryType('8'));
	group.set(FIX::MDEntryPx(dom.LowPrice));
	pmsg->addGroup(group);
}