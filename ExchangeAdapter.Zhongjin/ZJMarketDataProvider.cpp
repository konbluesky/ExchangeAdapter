#include "ExchangeAdapterConfig.h"
#include "ZJMarketDataProvider.h"
#include "ZJMarketDataHandler.h"
#define TIXML_USE_STL
#include "tinyxml.h"

namespace exchangeadapter_zhongjin
{
	CZJMarketDataProvider::CZJMarketDataProvider(void)
	{
	}

	CZJMarketDataProvider::~CZJMarketDataProvider(void)
	{
	}

	std::string CZJMarketDataProvider::GetName()
	{
		return "ZhongJingMarketDataProvider";
	}
	
	void CZJMarketDataProvider::OnRspMsg(const FIX::Message &msg)
	{
		OnResponseMsg(msg);
	}

	bool CZJMarketDataProvider::Initialize(void* pParam)
	{
		InitParam *pInitparam = (InitParam*)pParam;
		pUserApi = CFfexFtdcMduserApi::CreateFtdcMduserApi(
			pInitparam->sFlowPath.c_str());
		CZJMarketDataHandler::InitParam handlerparam;
		handlerparam.ParticipantID = pInitparam->sParticipantID;
		handlerparam.UserID = pInitparam->sUserID;
		handlerparam.Password = pInitparam->sPassword;
		pPriceHandler = boost::shared_ptr<CZJMarketDataHandler>(
			new CZJMarketDataHandler(this,pUserApi,handlerparam));
		pUserApi->RegisterSpi(pPriceHandler.get());
		switch(pInitparam->iResumeType){
		case 0: pUserApi-> SubscribeMarketDataTopic (100, TERT_RESTART);break;
		case 1: pUserApi-> SubscribeMarketDataTopic (100, TERT_RESUME);break;
		case 2:
		default:pUserApi-> SubscribeMarketDataTopic (100, TERT_QUICK);break;
		}

		for(int i = 0;i < (int)(pInitparam->sAddressIps.size());i++){
			std::string straddr = pInitparam->sAddressIps[i];
			int iport = pInitparam->iPorts[i];
			char addressinfo[64];
			sprintf(addressinfo,"tcp://%s:%d",straddr.c_str(),iport);
			pUserApi->RegisterFront(addressinfo);
		}

		pUserApi->Init();
		return true;
	}

	void CZJMarketDataProvider::UnInitialize()
	{
		pUserApi->Release();
	}

	bool CZJMarketDataProvider::SupportDoM()
	{
		return true;
	}

	bool CZJMarketDataProvider::InitializeByXML( const char* configfile )
	{
		std::pair<bool,CZJMarketDataProvider::InitParam> param = 
			CZJMarketDataProvider::InitParam::LoadConfig(configfile);
		if (param.first)
			return Initialize(&param.second);
		else
			return false;
	}

	bool CZJMarketDataProvider::Login(void* ppara)
	{
		uint32 seqnum = (uint32)(ppara);
		CFfexFtdcReqUserLoginField reqUserLogin;
		std::strcpy(reqUserLogin.ParticipantID,
			pPriceHandler->m_param.ParticipantID.c_str());
		std::strcpy(reqUserLogin.UserID,
			pPriceHandler->m_param.UserID.c_str());
		std::strcpy(reqUserLogin.Password,
			pPriceHandler->m_param.Password.c_str());
		std::strcpy(reqUserLogin.UserProductInfo, "RMMSoft(v1.0.0)");

		if(0 > pUserApi->ReqUserLogin(&reqUserLogin,seqnum))
			return false;
		return true;
	}

	bool CZJMarketDataProvider::Logout(void* ppara)
	{
		uint32 seqnum = (uint32)(ppara);
		CFfexFtdcReqUserLogoutField reqUserLogout;
		std::strcpy(reqUserLogout.ParticipantID,
			pPriceHandler->m_param.ParticipantID.c_str());
		std::strcpy(reqUserLogout.UserID,pPriceHandler->m_param.UserID.c_str());

		if(0 > pUserApi->ReqUserLogout(&reqUserLogout,seqnum))
			return false;
		return true;
	}

	std::pair<bool,CZJMarketDataProvider::InitParam> 
		CZJMarketDataProvider::InitParam::LoadConfig(const std::string& fname)
	{
		CZJMarketDataProvider::InitParam param;

		TiXmlDocument doc(fname);
		bool loadOkay = doc.LoadFile();
		if (!loadOkay)
			return std::make_pair(false,param);

		TiXmlNode* pNode = doc.FirstChild("configure");
		TiXmlNode* ele = pNode->FirstChild("sflowpath");
		param.sFlowPath = ele->FirstChild()->Value();
		ele = ele->NextSibling();

		TiXmlNode* addressNode;
		while(ele != NULL){
			if(ele->ValueStr() == "saddress"){
				addressNode = ele->FirstChild("saddressip");
				param.sAddressIps.push_back(addressNode->FirstChild()->Value());
				addressNode = ele->FirstChild("iport");
				param.iPorts.push_back(atoi(
					addressNode->FirstChild()->Value()));
			}
			ele = ele->NextSibling();
		}
		ele = pNode->FirstChild("sparticipantid");
		param.sParticipantID = ele->FirstChild()->Value();
		ele = pNode->FirstChild("suserid");
		param.sUserID = ele->FirstChild()->Value();
		ele = pNode->FirstChild("spassword");
		param.sPassword = ele->FirstChild()->Value();
		ele = pNode->FirstChild("iresumeType");
		param.iResumeType = atoi(ele->FirstChild()->Value());

		return std::make_pair(true,param);
	}
}