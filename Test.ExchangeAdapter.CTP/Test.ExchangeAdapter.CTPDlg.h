
// Test.ExchangeAdapter.CTPDlg.h : header file
//

#pragma once
#include "afxcmn.h"
#include "simplelistctrl.h"

// CTestExchangeAdapterCTPDlg dialog
class CTestExchangeAdapterCTPDlg : public CDialog
{
// Construction
public:
	CTestExchangeAdapterCTPDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_TESTEXCHANGEADAPTERCTP_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	CSimpleListCtrl m_QuoteListCtrl;
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton4();
	afx_msg void OnBnClickedOk2();
	afx_msg void OnBnClickedOk3();
	afx_msg void OnBnClickedCancel2();
	afx_msg void OnBnClickedButton5();
};
