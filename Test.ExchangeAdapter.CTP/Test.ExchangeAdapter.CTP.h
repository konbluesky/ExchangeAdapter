
// Test.ExchangeAdapter.CTP.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CTestExchangeAdapterCTPApp:
// See Test.ExchangeAdapter.CTP.cpp for the implementation of this class
//

class CTestExchangeAdapterCTPApp : public CWinAppEx
{
public:
	CTestExchangeAdapterCTPApp();

// Overrides
	public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CTestExchangeAdapterCTPApp theApp;