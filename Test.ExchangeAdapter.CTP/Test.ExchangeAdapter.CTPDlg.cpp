
// Test.ExchangeAdapter.CTPDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ucodehelper.h"
#include "ustringutils.h"
#include "ExchangeAdapterDef.h"
#include "Test.ExchangeAdapter.CTP.h"
#include "Test.ExchangeAdapter.CTPDlg.h"
#include "CTPExchangeAdapter.h"
#include "iorderservice.h"
#include "ustringutils.h"
#include "ThostFtdcUserApiStruct.h"
#include "umaths.h"
#include "CTPHelper.h"
#pragma warning(disable : 4482)
using namespace tradeadapter;

//using namespace ctp;
USING_LIBSPACE
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CTestExchangeAdapterCTPDlg dialog




CTestExchangeAdapterCTPDlg::CTestExchangeAdapterCTPDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTestExchangeAdapterCTPDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CTestExchangeAdapterCTPDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_QUOTE, m_QuoteListCtrl);
}

BEGIN_MESSAGE_MAP(CTestExchangeAdapterCTPDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON1, &CTestExchangeAdapterCTPDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CTestExchangeAdapterCTPDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &CTestExchangeAdapterCTPDlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON4, &CTestExchangeAdapterCTPDlg::OnBnClickedButton4)
	ON_BN_CLICKED(IDOK2, &CTestExchangeAdapterCTPDlg::OnBnClickedOk2)
	ON_BN_CLICKED(IDOK3, &CTestExchangeAdapterCTPDlg::OnBnClickedOk3)
	ON_BN_CLICKED(IDCANCEL2, &CTestExchangeAdapterCTPDlg::OnBnClickedCancel2)
	ON_BN_CLICKED(IDC_BUTTON5, &CTestExchangeAdapterCTPDlg::OnBnClickedButton5)
END_MESSAGE_MAP()


// CTestExchangeAdapterCTPDlg message handlers

BOOL CTestExchangeAdapterCTPDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	m_QuoteListCtrl.SetDefaultStyle();
	CString heads= _T("  ,80;    ,80;");
	m_QuoteListCtrl.SetHeads(heads);

//	m_QuoteListCtrl.SetSortAble();



	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CTestExchangeAdapterCTPDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CTestExchangeAdapterCTPDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CTestExchangeAdapterCTPDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

tradeadapter::ctp::CCTPExchangeAdapter instance;
void CTestExchangeAdapterCTPDlg::OnBnClickedButton1()
{
	// TODO: Add your control notification handler code here
	/*
	tradeadapter::CTPParam param;
	param.addr = "tcp://116.236.213.178";
	param.port = 21205;
	param.BrokerID = "1023";
	param .usr = "00000088";
	param.pwd = "123456";
	instance.Initialize(&param);
	*/
	bool ret = instance.InitializeByXML(_T("ExchangeAdapter.CTP.xml"));
	if (!ret)
	{
	}
	
}

void CTestExchangeAdapterCTPDlg::OnBnClickedButton2()
{
	// TODO: Add your control notification handler code here


//	tradeadapter::Quote quote;
	//((IQuoteService*)&instance )->GetQuote(UCodeHelper::ToTS(instance.GetCurrentIF()).c_str(),&quote);

	/*
	CThostFtdcDepthMarketDataField* pdatea = (CThostFtdcDepthMarketDataField*)buff;
	this->m_QuoteListCtrl.AddLine(_T("��1"),UStrConvUtils::ToString(pdatea->AskPrice1).c_str());
	this->m_QuoteListCtrl.AddLine(_T("��2"),UStrConvUtils::ToString(pdatea->AskPrice2).c_str());
	this->m_QuoteListCtrl.AddLine(_T("��3"),UStrConvUtils::ToString(pdatea->AskPrice3).c_str());
	this->m_QuoteListCtrl.AddLine(_T("��4"),UStrConvUtils::ToString(pdatea->AskPrice4).c_str());
	this->m_QuoteListCtrl.AddLine(_T("��5"),UStrConvUtils::ToString(pdatea->AskPrice5).c_str());
	*/
}

void CTestExchangeAdapterCTPDlg::OnBnClickedButton3()
{
	// TODO: Add your control notification handler code here
	tradeadapter::Order order;
	order.ContractCode = UCodeHelper::ToTS(instance.GetCurrentIF());
	order.Amount =1;
	order.hedge = EHedgeFlag::eSpeculation;
	order.OpeDir = EBuySell::eBuy;
	order.Price =(float) 2570.2;
	order.openclose = EOpenClose::eOpen;
	order.pricemode = EPriceMode::eLimitPrice;
	int orderid;
	instance.EntrustOrder(order,&orderid);
	this->GetDlgItem(IDC_EDIT5)->SetWindowText(UStrConvUtils::ToString(orderid).c_str());
}

void CTestExchangeAdapterCTPDlg::OnBnClickedButton4()
{
	// TODO: Add your control notification handler code here
	std::list<PositionInfo> pos;
	bool ret = instance.QueryPostions(&pos, UCodeHelper::ToTS(instance.GetCurrentIF()).c_str());
	for(std::list<PositionInfo>::iterator it = pos.begin(); it != pos.end(); ++it)
	{
		std::cout << UCodeHelper::ToAS(it->code) << std::endl;
	}
}

void CTestExchangeAdapterCTPDlg::OnBnClickedOk2()
{
	// TODO: Add your control notification handler code here
	instance.GetContractInfo();
}

void CTestExchangeAdapterCTPDlg::OnBnClickedOk3()
{
	// TODO: Add your control notification handler code here
	CEdit* pedit =  ( CEdit* ) this->GetDlgItem(IDC_EDIT4);
	CString data;
	pedit->GetWindowText(data);

		instance.CancelOrder(UMaths::ToInt32( (const TCHAR*) data));
}

void CTestExchangeAdapterCTPDlg::OnBnClickedCancel2()
{
	// TODO: Add your control notification handler code here
	instance.UnInitialize();
}

void CTestExchangeAdapterCTPDlg::OnBnClickedButton5()
{
	// TODO: Add your control notification handler code here
	std::string code = tradeadapter::ctp::CTPHelper::GetCurrentIFCode();
	std::cout<<"=========="<<code<<std::endl;
}
