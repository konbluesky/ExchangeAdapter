
// Test.ExchangeAdapterService.cpp : 定义应用程序的类行为。
//

#include "stdafx.h"
#include "ucodehelper.h"
#include "ustringutils.h"

#include "Test.ExchangeAdapterService.h"
#include "Test.ExchangeAdapterServiceDlg.h"
#include "udirectory.h"
#include "texceptionex.h"
//#include "ApexTestDlg.h"
USING_LIBSPACE

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

USING_LIBSPACE
// CTestExchangeAdapterServiceApp

BEGIN_MESSAGE_MAP(CTestExchangeAdapterServiceApp, CWinAppEx)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()

struct SaveItem
{
	INT CURINDEX;
	TCHAR CODE[10];
	double Price;
	int Amount;
};


// CTestExchangeAdapterServiceApp 构造

CTestExchangeAdapterServiceApp::CTestExchangeAdapterServiceApp()
{
	// TODO: 在此处添加构造代码，
	// 将所有重要的初始化放置在 InitInstance 中
}


// 唯一的一个 CTestExchangeAdapterServiceApp 对象

CTestExchangeAdapterServiceApp theApp;


// CTestExchangeAdapterServiceApp 初始化

BOOL CTestExchangeAdapterServiceApp::InitInstance()
{
	// 如果一个运行在 Windows XP 上的应用程序清单指定要
	// 使用 ComCtl32.dll 版本 6 或更高版本来启用可视化方式，
	//则需要 InitCommonControlsEx()。否则，将无法创建窗口。
	UDirectory::SetAppDirectory();
	UDirectory::ExporerDirectory();
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// 将它设置为包括所有要在应用程序中使用的
	// 公共控件类。
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinAppEx::InitInstance();

	AfxEnableControlContainer();

	// 标准初始化
	// 如果未使用这些功能并希望减小
	// 最终可执行文件的大小，则应移除下列
	// 不需要的特定初始化例程
	// 更改用于存储设置的注册表项
	// TODO: 应适当修改该字符串，
	// 例如修改为公司或组织名
	SetRegistryKey(_T("应用程序向导生成的本地应用程序"));



	CTestExchangeAdapterServiceDlg dlg;
	m_pMainWnd = &dlg;

	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: 在此放置处理何时用
		//  “确定”来关闭对话框的代码
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: 在此放置处理何时用
		//  “取消”来关闭对话框的代码
	}
	/*CApexTestDlg apexdlg;
	apexdlg.DoModal();*/

	// 由于对话框已关闭，所以将返回 FALSE 以便退出应用程序，
	//  而不是启动应用程序的消息泵。
	return FALSE;
}


//#pragma comment(linker,"/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")




int GetInCreasedID(const char* fname = "increaseorderid.data")
{
	static int ret;
	char data[32] = { 0 };

	int sdatalen = 32;
	DWORD adatalen = 0;
	static HANDLE handler = INVALID_HANDLE_VALUE;
	if (handler == INVALID_HANDLE_VALUE)
	{
		handler = ::CreateFileA(fname,
			0,
			FILE_SHARE_READ | FILE_SHARE_DELETE | FILE_SHARE_WRITE, //we should enable file shared read & write
			NULL,
			OPEN_ALWAYS,
			FILE_ATTRIBUTE_NORMAL | FILE_FLAG_NO_BUFFERING, //force write the data imediately
			NULL);

		if (handler == INVALID_HANDLE_VALUE)
		{
			char error[MAX_PATH * 3] = { 0 };
			sprintf(error, "无法打开文件  %s", fname);

			throw TException(GB2TS(error));
			return 0;
		}

		::ReadFile(handler, data, sdatalen, &adatalen, NULL);
		ret = atoi(data);
	}


	{

		sprintf(data, "%d", ret + 1);
		//SeekFile

		::WriteFile(handler, data, strlen(data), &adatalen, NULL);
		ret = ret + 1;
		return ret;
	}


	return ret;

}




#pragma comment(linker,"/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")