//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Test.ExchangeAdapterService.rc
//
#define IDD_TESTEXCHANGEADAPTERSERVICE_DIALOG 102
#define IDR_MAINFRAME                   128
#define IDC_COMBO1                      1000
#define IDC_BUTTON1                     1001
#define IDC_BUTTON_QUERYPOS             1002
#define IDC_LIST_POS                    1003
#define IDC_BUTTON5                     1004
#define IDC_STATIC_DEALEDNUM            1005
#define IDC_LIST_DEALORDER              1006
#define IDC_BUTTON100                   1007
#define IDC_BUTTON9                     1008
#define IDC_EDIT1                       1009
#define IDC_BUTTON_QUERY_DELADEORDER    1010
#define IDC_EDIT_LOG                    1011
#define IDC_BUTTON_ACCOUNTINFO          1012
#define IDC_EDIT_TIMES                  1013
#define IDC_BUTTON2                     1014
#define IDC_BUTTON_UNIITALIZE           1015
#define IDC_LIST_ENTRUST                1016
#define IDC_EDIT_STOCKID                1017
#define IDC_EDIT_PRICE                  1018
#define IDC_EDIT_AMOUNT                 1019
#define IDC_LIST2                       1020
#define IDC_RADIO_CHECKBUY              1021
#define IDC_RADIO2                      1022
#define IDC_RADIO_SELL                  1022
#define IDC_BUTTON_STRESSTEST           1023
#define IDC_EDIT2                       1024
#define IDC_EDIT_STRESSTIMES            1024
#define IDC_RADIO_MORE                  1025
#define IDC_RADIO_CLOSE                 1026
#define IDC_RADIO_SHORT                 1026
#define IDC_BUTTON3                     1027
#define IDC_BUTTON_CANCELORDER          1027

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1028
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
