#pragma once


// CApexTestDlg dialog

class CApexTestDlg : public CDialog
{
	DECLARE_DYNAMIC(CApexTestDlg)

public:
	CApexTestDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CApexTestDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG_APEX_TEST };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedOk2();
	afx_msg void OnBnClickedOk3();
};
