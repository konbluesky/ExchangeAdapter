
// Test.ExchangeAdapterServiceDlg.h : 头文件
//

#pragma once

#include "simplelistctrl.h"
#include "afxwin.h"
// CTestExchangeAdapterServiceDlg 对话框
class CTestExchangeAdapterServiceDlg : public CDialog
{
	// 构造
public:
	CTestExchangeAdapterServiceDlg(CWnd* pParent = NULL);	// 标准构造函数

	// 对话框数据
	enum { IDD = IDD_TESTEXCHANGEADAPTERSERVICE_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持
	CString GetIDText(int id)
	{
		CString tmp;
		GetDlgItem(id)->GetWindowText(tmp);
		return tmp;
	}

	void stresstest();

	int GetORderID();

	bool m_UpdateUI;

	// 实现
protected:
	HICON m_hIcon;
	CSimpleListCtrl m_poslistctrl;
	CSimpleListCtrl m_dealedlistctrl;
	CEdit     m_LogEdit;
	CSimpleListCtrl m_entrustinfoctrl;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedInitialize();
	afx_msg void OnBnClickedEntrustOrder();

	void _doEntrustOrder();

	afx_msg void OnBnClickedQueryDealedNum();
	afx_msg void OnBnClickedCancelOrder();
	afx_msg void OnBnClickedButtonQuerypos();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedButtonQueryDeladeorderS();
	afx_msg void OnBnClickedButtonAccountinfo();
	afx_msg void OnBnClickedQueryEntrust();
	afx_msg void OnBnClickedButtonUniitalize();

	afx_msg void OnBnClickedButtonStresstest();
	afx_msg void OnBnClickedButtonCancelorder();
	afx_msg void OnEnKillfocusEditAmount();
};
