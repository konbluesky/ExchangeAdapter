// ApexTestDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ustringutils.h"
#include "IOrderService.h"
#include "Test.ExchangeAdapterService.h"
#include "ApexTestDlg.h"
#include "ApexHelper.h"

using namespace tradeadapter;
using namespace tradeadapter::apex;
// CApexTestDlg dialog

IMPLEMENT_DYNAMIC(CApexTestDlg, CDialog)

CApexTestDlg::CApexTestDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CApexTestDlg::IDD, pParent)
{

}

CApexTestDlg::~CApexTestDlg()
{
}

void CApexTestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CApexTestDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CApexTestDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDOK2, &CApexTestDlg::OnBnClickedOk2)
	ON_BN_CLICKED(IDOK3, &CApexTestDlg::OnBnClickedOk3)
END_MESSAGE_MAP()


// CApexTestDlg message handlers

void CApexTestDlg::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	//OnOK();
	Order order;
	order.Amount = 100;
	//order.clietID = _T("me");
	static int orderid = 11;
	orderid++;


#pragma warning(disable : 4482)	
	order. ContractCode = _T("50001");
	order. OpeDir = BuySell::Buy;
	order.	Price = 12.3;			//note: we only put fixed price order.
	
	order.		Day = 20130516;
	order.		Time = 101010;			//121120 = 12:11:20	
	//order.ExchangeID = 
	UApexHelper::SaveOrderInfo(orderid,order, "jys","gdh");
}

void CApexTestDlg::OnBnClickedOk2()
{
	// TODO: Add your control notification handler code here
	UApexHelper::InitializeDB("tradelog","","");
}

void CApexTestDlg::OnBnClickedOk3()
{
	// TODO: Add your control notification handler code here
	UApexHelper::LoadTodayEntrustOrdersFromDB();
}
