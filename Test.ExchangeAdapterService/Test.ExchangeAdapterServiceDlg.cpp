
// Test.ExchangeAdapterServiceDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "ucodehelper.h"
#define  USEDEBUG
#include "usystem.h"
#include "usystemutils.h"
#include "ustringutils.h"
#include "umaths.h"
#include "ITradeAdapter.h"
#include "IOrderService.h"
#include "Test.ExchangeAdapterService.h"
#include "Test.ExchangeAdapterServiceDlg.h"
#include "tfilesearcher.h"
#include "tinyxml.h"
#include "tconfigsetting.h"
#include "ucomhelper.h"
#include "texceptionex.h"
#include <iostream>
#include "ubugutils.h"
#include "utimeutils.h"
USING_LIBSPACE

#pragma warning(disable : 4482)


using namespace tradeadapter;
ITradeAdapter* pinstance = NULL;;


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CTestExchangeAdapterServiceDlg::CTestExchangeAdapterServiceDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTestExchangeAdapterServiceDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CTestExchangeAdapterServiceDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_POS, m_poslistctrl);
	DDX_Control(pDX, IDC_LIST_DEALORDER, m_dealedlistctrl);
	DDX_Control(pDX, IDC_EDIT_LOG, m_LogEdit);

	DDX_Control(pDX, IDC_LIST_ENTRUST, m_entrustinfoctrl);
}

BEGIN_MESSAGE_MAP(CTestExchangeAdapterServiceDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON1, &CTestExchangeAdapterServiceDlg::OnBnClickedInitialize)
	ON_BN_CLICKED(IDC_BUTTON5, &CTestExchangeAdapterServiceDlg::OnBnClickedEntrustOrder)
	ON_BN_CLICKED(IDC_BUTTON9, &CTestExchangeAdapterServiceDlg::OnBnClickedQueryDealedNum)
	ON_BN_CLICKED(IDC_BUTTON100, &CTestExchangeAdapterServiceDlg::OnBnClickedCancelOrder)
	ON_BN_CLICKED(IDC_BUTTON_QUERYPOS, &CTestExchangeAdapterServiceDlg::OnBnClickedButtonQuerypos)
	ON_BN_CLICKED(IDOK, &CTestExchangeAdapterServiceDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON_QUERY_DELADEORDER, &CTestExchangeAdapterServiceDlg::OnBnClickedButtonQueryDeladeorderS)
	ON_BN_CLICKED(IDC_BUTTON_ACCOUNTINFO, &CTestExchangeAdapterServiceDlg::OnBnClickedButtonAccountinfo)
	ON_BN_CLICKED(IDC_BUTTON2, &CTestExchangeAdapterServiceDlg::OnBnClickedQueryEntrust)
	ON_BN_CLICKED(IDC_BUTTON_UNIITALIZE, &CTestExchangeAdapterServiceDlg::OnBnClickedButtonUniitalize)
	ON_BN_CLICKED(IDC_BUTTON_STRESSTEST, &CTestExchangeAdapterServiceDlg::OnBnClickedButtonStresstest)
	ON_BN_CLICKED(IDC_BUTTON_CANCELORDER, &CTestExchangeAdapterServiceDlg::OnBnClickedButtonCancelorder)
	ON_EN_KILLFOCUS(IDC_EDIT_AMOUNT, &CTestExchangeAdapterServiceDlg::OnEnKillfocusEditAmount)
END_MESSAGE_MAP()


// CTestExchangeAdapterServiceDlg 消息处理程序

BOOL CTestExchangeAdapterServiceDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	//读取ExchangeAdapterService.xml，初始化adapter选择列表
	CComboBox* pcomb = (CComboBox*)this->GetDlgItem(IDC_COMBO1);
	TConfigSetting cfgExcAdpService;
	cfgExcAdpService.LoadConfig("ExchangeAdapterService.xml");
	TiXmlNode& nodeCfg = cfgExcAdpService["configuration"];
	for (TiXmlNode* nodeChild = nodeCfg.FirstChild(); nodeChild; nodeChild = nodeCfg.IterateChildren(nodeChild))
	{
		if (nodeChild->Type() == TiXmlNode::TINYXML_ELEMENT
			&& nodeChild->FirstChild("dll")
			&& nodeChild->FirstChild("cfg"))
		{
			pcomb->AddString(U82TS(nodeChild->Value()).c_str());
		}
	}

	//读取Test.ExchangeAdapterService.xml，初始化测试股票代码价格数量...
	TConfigSetting cfg;
	cfg.LoadConfig("Test.ExchangeAdapterService.xml");
	int index = UMaths::ToInt32(cfg["configuration"]["curadapterindex"].EleVal().c_str());
	pcomb->SetCurSel(index);
	this->GetDlgItem(IDC_EDIT_STOCKID)->SetWindowText(U82TS(cfg["configuration"]["stock"].EleVal().c_str()).c_str());
	this->GetDlgItem(IDC_EDIT_PRICE)->SetWindowText(U82TS(cfg["configuration"]["price"].EleVal().c_str()).c_str());
	this->GetDlgItem(IDC_EDIT_AMOUNT)->SetWindowText(U82TS(cfg["configuration"]["amount"].EleVal().c_str()).c_str());
	((CButton*)this->GetDlgItem(IDC_RADIO_CHECKBUY))->SetCheck(TRUE);

	m_poslistctrl.SetDefaultStyle();
	m_poslistctrl.SetHeads(_T("股票代码,40; 股票名称,40; 数量,40;价格,40; 可卖数量,40; 成本价, 40; 总市值,40; 浮动盈亏,40; "));
	m_poslistctrl.AdjustColumnWidth();


	m_dealedlistctrl.SetDefaultStyle();
	m_dealedlistctrl.SetHeads(_T("成交编号,40;委托号,40;成交时间,40;股票代码,40; 股票名称,40;买卖,40;委托价格,40;成交数量,40;"));
	m_dealedlistctrl.AdjustColumnWidth();

	GetDlgItem(IDC_EDIT_TIMES)->SetWindowText(_T("1"));

	m_entrustinfoctrl.SetDefaultStyle();
	m_entrustinfoctrl.SetHeads(_T("委托号,40;委托时间,40;股票代码,40; 股票名称,40;买卖,40; 委托数量,40;委托价格,40;成交数量,40;成交金额,40"));
	m_entrustinfoctrl.AdjustColumnWidth();




	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CTestExchangeAdapterServiceDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CTestExchangeAdapterServiceDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

IOrderService* pser = NULL;
void CTestExchangeAdapterServiceDlg::OnBnClickedInitialize()
{
	try
	{

		// TODO: Add your control notification handler code here
		CComboBox* pcomb = (CComboBox*)this->GetDlgItem(IDC_COMBO1);
		CString txt;
		pcomb->GetWindowText(txt);

		if (pser != NULL)
		{
			pser->UnInitialize();
			pser = NULL;
		}


		pser = IOrderService::CreateInstance(txt);
		bool init = pser->Initialize(NULL);		//可能会因为Initialize函数抛出的异常导致下一行代码中pinstance未能初始化
		pinstance = pser->GetAdapter();		//所以其他地方在使用pinstance指针之前最好进行判断

		if (!init &&  pinstance != NULL)
		{
			ErrorInfo error;
			pinstance->GetLastError(&error);
			std::cout << "Error" << error.Code << TS2GB(error.Msg) << std::endl;

		}




		std::cout << "ini" << init << std::endl;

	}
	catch (TException& e)
	{
		tstring msg = GB2TS(e.What());
		AfxMessageBox(msg.c_str());

	}
	catch (std::exception& e)
	{
		tstring msg = GB2TS(e.what());

		AfxMessageBox(msg.c_str());
	}
	catch (CException* e)
	{
		e->ReportError();
		//AfxMessageBox(e->GetErrorMessage());
	}
	catch (...)
	{
		tstring err = _T("Exception in Initialize:\n\r");
		err += USystemUtils::GetErrorMsg();	//这儿的GetErrorMsg有时并没能得到真正的异常点的信息

		AfxMessageBox(err.c_str());
	}


}

void CTestExchangeAdapterServiceDlg::OnBnClickedEntrustOrder()
{
	if (!pinstance)
	{
		AfxMessageBox(_T("Adapter instance pointer not initialized(pinstance==NULL)。"));
		return;
	}

	// TODO: Add your control notification handler code here

	int time = UTimeUtils::GetNowTime();
	CaculateRunTime<1> instance;
	instance.BeginCaculate();
	m_UpdateUI = false;

	//int times = UMaths::ToInt32( (const TCHAR*)txt);




	_doEntrustOrder();

	///////////////////////////////////
	instance.EndCaculate();
	double val = instance.GetCacultePointTimeTick();

	int newtime = UTimeUtils::GetNowTime();
	time = newtime - time;




	//tstring msg2 = tstring(_T("总体时间")) + UStrConvUtils::ToString(time);

	//AfxMessageBox(msg2.c_str());

	//tstring msg1 = tstring(_T("cpu时间")) + UStrConvUtils::ToString(val);
	//AfxMessageBox(msg1.c_str());


}

void CTestExchangeAdapterServiceDlg::OnBnClickedQueryDealedNum()
{
	if (!pinstance)
	{
		AfxMessageBox(_T("Adapter instance pointer not initialized(pinstance==NULL)。"));
		return;
	}

	// TODO: Add your control notification handler code here
	// TODO: Add your control notification handler code here
	std::list<DealedOrder> orders;
	//pinstance->GetDealedOrderInfo(&orders);
	//for(std::list<DealedOrder>::iterator it = orders.begin(); it != orders.end(); ++it)
	//{
	//	std::wcout<<it->business_time << _T("  ") <<it->stock_code << _T(" ") << it->entrust_no<<std::endl;
	//}
	//return;
	//// TODO: Add your control notification handler code here
	int ret = GetORderID();
	int rets;
	bool rt = pinstance->QueryOrder(ret, &rets);
	if (!rt)
	{
		ErrorInfo error;
		pinstance->GetLastError(&error);
		std::cout << "Error" << error.Code << TS2GB(error.Msg) << std::endl;
	}
	std::cout << "==============" << rets << std::endl;

	CString tmp;
	tmp.Format(_T("dealed num: %d"), rets);

	this->GetDlgItem(IDC_STATIC_DEALEDNUM)->SetWindowText(tmp);

}

void CTestExchangeAdapterServiceDlg::OnBnClickedCancelOrder()
{
	if (!pinstance)
	{
		AfxMessageBox(_T("Adapter instance pointer not initialized(pinstance==NULL)。"));
		return;
	}
	// TODO: Add your control notification handler code here

	// TODO: Add your control notification handler code here
	int ret = GetORderID();
	pinstance->CancelOrder(ret);
}

int CTestExchangeAdapterServiceDlg::GetORderID()
{
	CEdit* pEdit = (CEdit*)GetDlgItem(IDC_EDIT1);
	CString txt;
	pEdit->GetWindowText(txt);
	int ret = UMaths::ToInt32(txt);
	return ret;

}

void CTestExchangeAdapterServiceDlg::OnBnClickedButtonQuerypos()
{
	if (!pinstance)
	{
		AfxMessageBox(_T("Adapter instance pointer not initialized(pinstance==NULL)。"));
		return;
	}

	this->m_poslistctrl.DeleteAllItems();
	std::list<PositionInfo> posss;
	bool ret = pinstance->QueryPostions(&posss);

	if (!ret)
	{
		ErrorInfo error;
		pinstance->GetLastError(&error);
		std::cout << "Error" << error.Code << TS2GB(error.Msg) << std::endl;

	}
	if (m_UpdateUI)
	{


		for (std::list<PositionInfo>::iterator ite = posss.begin(); ite != posss.end(); ++ite)
		{
			m_poslistctrl.AddLine(ite->code.c_str(),
				ite->stock_name.c_str(),
				UStrConvUtils::ToString(ite->current_amount).c_str(),
				UStrConvUtils::ToString(ite->last_price).c_str(),
				UStrConvUtils::ToString(ite->enable_amount).c_str(),
				UStrConvUtils::ToString(ite->cost_price).c_str(),
				UStrConvUtils::ToString(ite->market_value).c_str(),
				UStrConvUtils::ToString(ite->income_balance).c_str()
				);
		}

		m_poslistctrl.AdjustColumnWidth();
	}

}

void CTestExchangeAdapterServiceDlg::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	OnOK();
}

void CTestExchangeAdapterServiceDlg::OnBnClickedButtonQueryDeladeorderS()
{
	if (!pinstance)
	{
		AfxMessageBox(_T("Adapter instance pointer not initialized(pinstance==NULL)。"));
		return;
	}

	m_dealedlistctrl.DeleteAllItems();
	std::list<DealedOrder> orders;
	bool ret = pinstance->QueryDealedOrders(&orders);


	if (!ret)
	{
		ErrorInfo error;
		pinstance->GetLastError(&error);
		std::cout << "Error" << error.Code << TS2GB(error.Msg) << std::endl;
		return;

	}

	//std::sort(orders.begin(),orders.end(),)

	orders.sort(DealedOrder::comparebyDealedID);
	if (m_UpdateUI)
	{
		for (std::list<DealedOrder>::iterator it = orders.begin(); it != orders.end(); ++it)
		{
			//m_dealedlistctrl.SetHeads(_T("委托号,40;成交时间,40;股票代码,40; 股票名称,40;买卖,40;委托价格,40;成交数量,40;"));

			m_dealedlistctrl.AddLine(
				UStrConvUtils::ToString(it->serial_no).c_str(),
				UStrConvUtils::ToString(it->entrust_no).c_str(),
				UStrConvUtils::ToString(it->business_time).c_str(),
				it->stock_code.c_str(),
				it->stock_name.c_str(),
				it->entrust_bs == EBuySell::eBuy ? _T("买入") : _T("卖出"),
				UStrConvUtils::ToString(it->business_price).c_str(),
				UStrConvUtils::ToString(it->business_amount).c_str()
				);
		}
		m_dealedlistctrl.AdjustColumnWidth();
	}
	tstring retx = UStrConvUtils::Format(_T("数目%d"), orders.size());
	//AfxMessageBox(retx.c_str());

}

void CTestExchangeAdapterServiceDlg::OnBnClickedButtonAccountinfo()
{
	if (!pinstance)
	{
		AfxMessageBox(_T("Adapter instance pointer not initialized(pinstance==NULL)。"));
		return;
	}

	BasicAccountInfo info;

	bool ret = pinstance->QueryAccountInfo(&info);
	if (!ret)
	{
		ErrorInfo error;
		pinstance->GetLastError(&error);
		std::cout << "Error" << error.Code << TS2GB(error.Msg) << std::endl;


	}


	tstring msg = tstring(_T("ClientID:\t")) + info.ClientID;
	msg += _T("\r\nBranchID:\t") + info.BranchID;
	msg += _T("\r\nFundAccount\t") + info.FundAccount;
	msg += _T("\r\nClientName:\t") + info.ClientName;
	msg += _T("\r\nMoneyType:\t") + info.MoneyType;
	msg += UStrConvUtils::Format(_T("\r\nCurrentBalance:\t%f"), info.CurrentBalance);
	msg += UStrConvUtils::Format(_T("\r\nEnableBalance:\t%f"), info.EnableBalance);
	msg += UStrConvUtils::Format(_T("\r\nFetchCash:\t%f"), info.FetchCash);
	msg += UStrConvUtils::Format(_T("\r\nAssetBalance:\t%f"), info.AssetBalance);
	msg += _T("\r\nSHAccount:\t") + info.SHStockAccount;
	msg += _T("\r\nSZAccount:\t") + info.SZStockAccount;

	//m_LogEdit.SETW
	m_LogEdit.SetWindowText(msg.c_str());



}
//#include "KingdomTradeAdapter.h"
void CTestExchangeAdapterServiceDlg::OnBnClickedQueryEntrust()
{
	if (!pinstance)
	{
		AfxMessageBox(_T("Adapter instance pointer not initialized(pinstance==NULL)。"));
		return;
	}

	m_entrustinfoctrl.DeleteAllItems();

	std::list<EntrustInfo> infos;
	bool ret = pinstance->QueryEntrustInfos(&infos);

	if (!ret)
	{
		ErrorInfo error;
		pinstance->GetLastError(&error);
		std::cout << "Error" << error.Code << TS2GB(error.Msg) << std::endl;

	}

	for (std::list<EntrustInfo>::iterator it = infos.begin(); it != infos.end(); ++it)
	{
		//m_entrustinfoctrl.SetHeads(_T("委托号,40;委托时间,40;股票代码,40; 股票名称,40;买卖,40; 委托数量,40;委托价格,40;成交数量,40;"));
		this->m_entrustinfoctrl.AddLine(

			UStrConvUtils::ToString(it->orderid).c_str(),
			UStrConvUtils::ToString(it->ordertime).c_str(),
			it->stkcode.c_str(),
			it->stkname.c_str(),
			it->Ope == EBuySell::eBuy ? _T("买入") : _T("卖出"),

			UStrConvUtils::ToString(it->orderqty).c_str(),
			UStrConvUtils::ToString(it->orderprice).c_str(),
			UStrConvUtils::ToString(it->matchqty).c_str(),
			UStrConvUtils::ToString(it->matchamt).c_str()
			);
	}

	m_entrustinfoctrl.AdjustColumnWidth();
}

void CTestExchangeAdapterServiceDlg::OnBnClickedButtonUniitalize()
{
	if (!pinstance)
	{
		AfxMessageBox(_T("Adapter instance pointer not initialized(pinstance==NULL)。"));
		return;
	}

	pinstance->UnInitialize();
	pinstance = NULL;
}

void CTestExchangeAdapterServiceDlg::OnBnClickedButtonStresstest()
{
	// TODO: Add your control notification handler code here

	int time = UTimeUtils::GetNowTime();
	CaculateRunTime<1> instance;
	instance.BeginCaculate();
	m_UpdateUI = false;
	CEdit* pedit = (CEdit*)GetDlgItem(IDC_EDIT_STRESSTIMES);
	CString txt;
	pedit->GetWindowText(txt);
	int times = UMaths::ToInt32((const TCHAR*)txt);
	std::cout << "开始进行循环业务压力测试:" << times << "次" << std::endl;
	for (int i = 0; i < times - 1; ++i)
	{
		stresstest();

	}
	m_UpdateUI = true;

	stresstest();

	instance.EndCaculate();
	double val = instance.GetCacultePointTimeTick();

	int newtime = UTimeUtils::GetNowTime();
	time = newtime - time;


	tstring msg1 = tstring(_T("cpu时间")) + UStrConvUtils::ToString(val);
	//AfxMessageBox(msg1.c_str());


	tstring msg2 = tstring(_T("总体时间")) + UStrConvUtils::ToString(time);

	//AfxMessageBox(msg2.c_str());

	OnBnClickedButtonQueryDeladeorderS();
	OnBnClickedButtonQuerypos();



}

void CTestExchangeAdapterServiceDlg::stresstest()
{
	_doEntrustOrder();
	OnBnClickedCancelOrder();
	OnBnClickedQueryDealedNum();
	//OnBnClickedButtonQueryDeladeorderS();
	//OnBnClickedButtonQuerypos();
}


void CTestExchangeAdapterServiceDlg::OnBnClickedButtonCancelorder()
{
	// TODO: Add your control notification handler code here
	int rowcount = m_entrustinfoctrl.GetLinesCount();
	for (int i = 0; i < rowcount; ++i)
	{
		int orderid = UMaths::ToInt32((CONST TCHAR*)	m_entrustinfoctrl.GetItemValue(i, 0));
		pinstance->CancelOrder(orderid);
	}

}

void CTestExchangeAdapterServiceDlg::_doEntrustOrder()
{
	// TODO: Add your control notification handler code here
	CString tmp;
	GetDlgItem(IDC_EDIT_TIMES)->GetWindowText(tmp);
	int times = UMaths::ToInt32((const TCHAR*)tmp);
	for (int i = 0; i < times; ++i)
	{
		Order xx;
		CString tmp;
		//GetDlgItem(IDC_EDIT_STOCKID)->get
		xx.ContractCode = GetIDText(IDC_EDIT_STOCKID);
		xx.ContractCode = UStrConvUtils::ToUpper(xx.ContractCode);
		xx.Price = UMaths::ToDouble(GetIDText(IDC_EDIT_PRICE));
		xx.Amount = UMaths::ToInt32(GetIDText(IDC_EDIT_AMOUNT));
		xx.OpeDir = ((CButton*)GetDlgItem(IDC_RADIO_CHECKBUY))->GetCheck() ? eBuy : eSell;

		xx.openclose = ((CButton*)GetDlgItem(IDC_RADIO_MORE))->GetCheck() ? eOpen : eClose;
		xx.hedge = tradeadapter::EHedgeFlag::eSpeculation;
		int ret;
		pinstance->EntrustOrder(xx, &ret);
		GetDlgItem(IDC_EDIT1)->SetWindowText(UStrConvUtils::ToString(ret).c_str());
		if (ret == -1)
		{
			ErrorInfo error;
			pinstance->GetLastError(&error);
			std::cout << "Error" << error.Code << TS2GB(error.Msg) << std::endl;
		}

	}
}


void CTestExchangeAdapterServiceDlg::OnEnKillfocusEditAmount()
{
	// TODO: Add your control notification handler code here
	CEdit* pedit = (CEdit*)GetDlgItem(IDC_EDIT_STOCKID);
}
