#include "ExchangeAdapterConfig.h"
#include "RMMSExchTraderMsgManager.h"
#include "DebugUtils.h"
#include "sqlite3.h"
#include "ExchangeAdapterCompiler.h"

RMMS_NS_BEGIN

/**
 * RMMSDatabase管理
 */
RMMSDatabase::RMMSDatabase():m_pdb(NULL){}

RMMSDatabase::~RMMSDatabase(){}

int32 RMMSDatabase::Init(const tchar *pdsn,const tchar *ptableformat)
{
	DBG_ASSERT(NULL != ptableformat);
	if(!Open(pdsn))
		return -1;
	
	if(!IsTablePresent() && !CreateTable(ptableformat))
		return -1;
	return 0;
}

void RMMSDatabase::Uninit(void)
{
	Close();
}

bool RMMSDatabase::Open(const tchar *pdsn)
{
	DBG_ASSERT(NULL != pdsn && '\0' != *pdsn);
	if (SQLITE_OK != sqlite3_open(pdsn,&m_pdb))
		return false;
	m_strdsn = pdsn;
	return true;
}

void RMMSDatabase::Close()
{
	DBG_ASSERT(NULL != m_pdb);
	sqlite3_close(m_pdb);
	m_pdb = NULL;
	m_strdsn = "";
}

bool RMMSDatabase::IsTablePresent()
{
	const tchar *psql = "select count(*) from sqlite_master where "
		"type='table' and name='neworder'";
	sqlite3_stmt * stat;
	if(SQLITE_OK != sqlite3_prepare(m_pdb,psql,-1,&stat,NULL))
		return false;
	if(SQLITE_ROW != sqlite3_step(stat))
		return false;
	if(0 == sqlite3_column_int(stat,0))
		return false;
	else
		return true;
}

bool RMMSDatabase::CreateTable(const tchar *tableformat)
{
	tstring tmp = "create table neworder";
	tmp += tableformat;
	return (SQLITE_OK == sqlite3_exec(m_pdb,tmp.c_str(),NULL,NULL,NULL));
}

tstring RMMSDatabase::GetDatabaseName() const
{
	return m_strdsn;
}

bool RMMSDatabase::IsOpen() const 
{
	return NULL != m_pdb;
}

bool RMMSDatabase::ExecuteSQL(const tchar *psql)
{
	tchar *perrmsg;
	if(SQLITE_OK != sqlite3_exec(m_pdb,psql,NULL,NULL,&perrmsg)){
		DBG_TRACE(("%s",perrmsg));
		return false;
	}
	return true;
}

/*****************************************************************************/
/* 缓冲区管理类
/*****************************************************************************/
RMMSBufferManager::RMMSBufferManager(){}
RMMSBufferManager::~RMMSBufferManager()
{
	m_buffmap.clear();
	m_buffqueue.empty();
}

int32 RMMSBufferManager::Insert(const tchar *pubclordid,DataUnit &data)
{
	if(DATA_SIZE_LEN - 20 < m_buffqueue.size())
		Free();
	m_buffmap[pubclordid] =data;
	m_buffqueue.push(pubclordid);
	return 0;
}

void RMMSBufferManager::Delete(const tchar *pubclordid)
{
	if(m_buffmap.end() == m_buffmap.find(pubclordid))
		return;
	m_buffmap.erase(pubclordid);
}

int32 RMMSBufferManager::Get(const tchar *pubclordid,DataUnit *pdata,bool bdel)
{
	if(m_buffmap.end() == m_buffmap.find(pubclordid))
		return -1;
	*pdata = m_buffmap[pubclordid];
	if(true == bdel)
		m_buffmap.erase(pubclordid);
	return 0;
}

void RMMSBufferManager::Free(uint32 freedata)
{
	uint32 min = m_buffqueue.size();
	min = min < freedata ? min:freedata;

	for(unsigned int i = 0;i < min;i++){
		Delete(m_buffqueue.front().c_str());
		m_buffqueue.pop();
	}
}

/**
 * IRMMSExchMsgSimpleManager管理机制
 */
int32 IRMMSExchMsgSimpleManager::CacheMsgInfo(
	const tchar *pagentid,const tchar *ppriclordid,FIX::Message *pmsg,
	RMMSClOrdIdManager *pclordidmanager,RMMSMsgSeqNumManager *pseqnummanager)
{
	DBG_ASSERT(NULL != pmsg);
	DBG_ASSERT(NULL != pagentid);
	DBG_ASSERT(NULL != pclordidmanager);
	DBG_ASSERT(NULL != pseqnummanager);

	/*
	 * 如果该clordid没有记录,说明是新消息,那么需要获取新的统一clordid,
	 * 然后将该消息的映射等全部保存.以便返回时候能够恢复交易所不提供的信息.
	 */
	tstring pubclordid;
	LIB_NS::Locker locker(m_rwmutex);
	if(IsHaveMsgInfo(pagentid,ppriclordid,&pubclordid))
		return -1;

	pubclordid = pclordidmanager->GetNewLocalId(m_exch);
	if(0 > LocalSaveMsgInfo(pagentid,ppriclordid,pubclordid.c_str(),pmsg,
		pseqnummanager))
		return -1;
	
	return 0;
}

bool IRMMSExchMsgSimpleManager::GetMsgInfo(FIX::Message *pmsg,
	tstring *pagentid)
{
	DBG_ASSERT(NULL != pmsg);
	DBG_ASSERT(NULL != pagentid);

	/**
	 * 如果无法恢复,那么pagentid为空,至于此种情况如何处理,由路由器策略决定 
	 */
	LIB_NS::Locker locker(m_rwmutex);
	if(0 > LocalGetMsgInfo(pmsg,pagentid)){
		DBG_TRACE(("!!!!非常严重的错误,无法找到返回路径\n%s-%d\n",
			__FILE__,__LINE__));
		*pagentid = "";
	}

	return true;
}


/*****************************************************************************/
/* 缓冲区管理类
/*****************************************************************************/
RMMSBufferManagerZZ::RMMSBufferManagerZZ(){}
RMMSBufferManagerZZ::~RMMSBufferManagerZZ()
{
	m_buffmap.clear();
	m_buffqueue.empty();
}

int32 RMMSBufferManagerZZ::Insert(const tchar *pubclordid,DataUnit &data)
{
	if(DATA_SIZE_LEN - 20 < m_buffqueue.size())
		Free();
	m_buffmap[pubclordid] =data;
	m_buffqueue.push(pubclordid);
	return 0;
}

void RMMSBufferManagerZZ::Delete(const tchar *pubclordid)
{
	if(m_buffmap.end() == m_buffmap.find(pubclordid))
		return;
	m_buffmap.erase(pubclordid);
}

int32 RMMSBufferManagerZZ::Get(const tchar *pubclordid,DataUnit *pdata,bool bdel)
{
	if(m_buffmap.end() == m_buffmap.find(pubclordid))
		return -1;
	*pdata = m_buffmap[pubclordid];
	if(true == bdel)
		m_buffmap.erase(pubclordid);
	return 0;
}

void RMMSBufferManagerZZ::Free(uint32 freedata)
{
	uint32 min = m_buffqueue.size();
	min = min < freedata ? min:freedata;

	for(unsigned int i = 0;i < min;i++){
		Delete(m_buffqueue.front().c_str());
		m_buffqueue.pop();
	}
}

RMMS_NS_END