#ifndef __RMMS_EXCHANGE_ADAPTER_BASE_H__
#define __RMMS_EXCHANGE_ADAPTER_BASE_H__

/* RMMS通用文件 */
#include "RMMSCommon.h"

/* 接口宏 */
#if defined(WIN32)

#if defined(RMMS_EXCHANGE_ADAPTER_BUILD_DLL)
#define RMMS_EXCHANGE_ADAPTER_API __declspec(dllexport)		
#elif defined(RMMS_EXCHANGE_ADAPTER_USE_DLL)
#define RMMS_EXCHANGE_ADAPTER_API __declspec(dllimport)
#else
#define RMMS_EXCHANGE_ADAPTER_API	
#endif	

#else

#define RMMS_EXCHANGE_ADAPTER_API

#endif

#endif