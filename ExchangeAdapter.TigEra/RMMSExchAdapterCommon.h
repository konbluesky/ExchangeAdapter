#ifndef __RMMS_EXCH_ADAPTER_COMMON_H__
#define __RMMS_EXCH_ADAPTER_COMMON_H__

#include "RMMSExchangeAdapterBase.h"
#include "IFixMarket.h"
#include "IFixTrader.h"
#include "DebugUtils.h"
#include "udll.h"
#include "simsingleton.h"
#include "Mutex.h"

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4275)
#pragma warning(disable:4251)
#pragma warning(disable:4512)
#else
#error "仅支持VC,以待其他"
#endif

RMMS_NS_BEGIN

class RMMSMarketRouter;
class RMMSTraderRouter;

/**
 * 这部分接口暂时要保留,主要目的是业务尚没有弄清楚.后期需要弄清楚
 * 所有的接口之后,再着手去掉该层封装.
 */
class RMMSExchMarketAdapter : public IFixMarketProcessor
{
public:
	RMMSExchMarketAdapter(Exchange exch,IFixMarketProvider* provider);
	virtual ~RMMSExchMarketAdapter();
	void RegisterResponse(RMMSMarketRouter *response,void* priv);
	IFixMarketProvider *GetProvider(void);

	void BroadcastFix(const FIX::Message &msg);
	virtual void OnNetConnected();
	virtual void OnNetDisconnected();
	virtual void OnLogin();
	virtual void OnLogout();

	/* 暂时未完全想清楚 */
	virtual int32 Login();
	virtual void Logout();

public:
	const Exchange m_exch;

private:
	RMMSMarketRouter *m_response;
	IFixMarketProvider *m_pprovider;
	void* m_priv;
};

class RMMSExchTraderAdapter : public IFixTraderProcessor
{
public:
	RMMSExchTraderAdapter(Exchange exch,IFixTraderProvider* provider);
	virtual ~RMMSExchTraderAdapter();

public:
	void RegisterResponse(RMMSTraderRouter *response,void* priv);
	int32 Request(FIX::Message &msg) const;
	IFixTraderProvider *GetProvider(void);
	virtual void ResponseFix(const FIX::Message &msg);
	virtual void OnNetConnected();
	virtual void OnNetDisconnected();
	virtual void OnLogin();
	virtual void OnLogout();

public:
	const Exchange m_exch;

protected:
	RMMSTraderRouter *m_response;
	IFixTraderProvider *m_pprovider;
	void* m_priv;
};

#define GInstanceRef(classname)		\
	simpattern::Singleton_InClass<classname>::Instance()

class RMMSExchDllManager {
public:
	RMMSExchDllManager(Exchange exch):m_exch(exch){}
	virtual ~RMMSExchDllManager(){}

public:
	enum Action {
		BIND,
		UNBIND,
	};

	bool BindDll(const tstring &dllname)
	{
		m_hlib = LIB_NS::UDLLUtils::DllOpenLibrary(dllname.c_str());
		if(NULL == m_hlib)
			return false;
		return true;
	}

	void UnbindDll()
	{
		if(NULL == m_hlib)
			return;
		LIB_NS::UDLLUtils::DllCloseLibrary(m_hlib);
		m_hlib = NULL;
	}

	void* GetDllPFun(const tchar *strFunc)
	{
		return LIB_NS::UDLLUtils::DllGetFunc(m_hlib,strFunc);
	}

	Exchange GetExch() const { return m_exch;}
	bool IsBind() const {return NULL != m_hlib;}

private:
	LIB_NS::UDLLUtils::HLIB m_hlib;
	Exchange m_exch;
};

struct RMMSExchPara {
	InitType type;
	tstring para;
};

class RMMSExchAdapterFactory {
public:
	RMMSExchAdapterFactory(){}
	virtual ~RMMSExchAdapterFactory(){}
public:
	static void* CreateExchAdapter(FuncType func,
		RMMSExchDllManager &exchdll);
	static void DestoryExchAdapter(FuncType func,
		RMMSExchDllManager &exchdll,void** pexchadapter);
	static bool InitExchAdapter(FuncType func,
		void* pexchadapter,RMMSExchPara &para);
	static void UninitExchAdapter(FuncType func,
		void* pexchadapter);
	static bool LoginExchAdapter(FuncType func,
		void* pexchadapter,void *ppara);
	static bool LogoutExchAdapter(FuncType func,
		void* pexchadapter,void *ppara);
};

template<typename TAdapter,FuncType functype>
class RMMSExchManager {
public:
	int32 AddExch(Exchange exch,const tchar *dllname,RMMSExchPara &para)
	{
		/* 上锁 */
		LIB_NS::Locker locker(m_addlock);

		if(m_exchs.end() != m_exchs.find(exch))
			return 1;

		ExchInfo info;
		std::memset(&info,0,sizeof(ExchInfo));

		info.pexchdll = new RMMSExchDllManager(exch);
		if(NULL == info.pexchdll)
			return -2;

		if(!(info.pexchdll->BindDll(dllname))){
			delete info.pexchdll;
			info.pexchdll = NULL;
			return -3;
		}

		info.para = para;
		info.pexchadapter = (TAdapter*)
			RMMSExchAdapterFactory::CreateExchAdapter(functype,*info.pexchdll);
		if(NULL == info.pexchadapter){
			info.pexchdll->UnbindDll();
			delete info.pexchdll;
			info.pexchdll = NULL;
			return -4;
		}

		m_exchs[exch] = info;
		
		return 0;
	}

	void DecExch(Exchange exch)
	{
		if(m_exchs.end() == m_exchs.find(exch))
			return;

		RMMSExchAdapterFactory::DestoryExchAdapter(functype,
			*(m_exchs[exch].pexchdll),(void**)&(m_exchs[exch].pexchadapter));
		m_exchs[exch].pexchdll->UnbindDll();
		delete (m_exchs[exch].pexchdll);
		m_exchs.erase(exch);
	}

	bool InitExch(Exchange exch)
	{
		if(m_exchs.end() == m_exchs.find(exch))
			return false;

		return RMMSExchAdapterFactory::InitExchAdapter(functype,
			(void*)(m_exchs[exch].pexchadapter),m_exchs[exch].para);
	}

	void UninitExch(Exchange exch)
	{
		if(m_exchs.end() == m_exchs.find(exch))
			return;

		RMMSExchAdapterFactory::UninitExchAdapter(functype,
			(void*)(m_exchs[exch].pexchadapter));
	}

	int32 LoginExch(Exchange exch,void *ppara)
	{
		if(false == IsInit(exch))
			return -1;
		if(true == m_exchs[exch].started)
			return 1;

		m_exchs[exch].started = RMMSExchAdapterFactory::LoginExchAdapter(
			functype,m_exchs[exch].pexchadapter,
			ppara);
		if(!m_exchs[exch].started)
			return -1;

		return 0;
	}

	bool LogoutExch(Exchange exch,void *ppara)
	{
		if(false == m_exchs[exch].started)
			return true;

		return RMMSExchAdapterFactory::LogoutExchAdapter(
			functype,m_exchs[exch].pexchadapter,ppara);
	}

	TAdapter *GetExchAdapter(Exchange exch)
	{
		DBG_ASSERT(m_exchs.end() != m_exchs.find(exch));
		return m_exchs[exch].pexchadapter;
	}

	bool IsStart(Exchange exch)
	{
		if(m_exchs.end() == m_exchs.find(exch))
			return false;
		return true;
	}

	bool IsInit(Exchange exch)
	{
		return m_exchs.end() != m_exchs.find(exch);
	}

protected:
	struct ExchInfo {
		TAdapter* pexchadapter;
		RMMSExchDllManager* pexchdll;
		RMMSExchPara para;
		bool started;
		tstring dllname;
	};
	LIB_NS::Mutex m_addlock;
	std::map<Exchange,ExchInfo> m_exchs;
};

typedef RMMSExchManager<RMMSExchMarketAdapter,MARKET> 
	RMMSExchMarketManager;
class RMMSExchTraderManager : public RMMSExchManager<
	RMMSExchTraderAdapter,TRADER>
{
public:
	int32 Request(Exchange exch,FIX::Message &msg)
	{
		if(!IsStart(exch))
			return -1;
		return m_exchs[exch].pexchadapter->Request(msg);
	}
};

RMMS_NS_END

#ifdef _MSC_VER
#pragma warning(pop)
#else
#error "仅支持VC,以待修改"
#endif

#endif