#ifndef __RMMS_GATEWAY_H__
#define __RMMS_GATEWAY_H__

#include "RMMSExchangeAdapterBase.h"
#include "RMMSExchAdapterCommon.h"
#include <map>

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4275)
#pragma warning(disable:4251)
#else
#error "仅支持VC,以待修改"
#endif

RMMS_NS_BEGIN

/**
 * Gateway Para
 */
class RMMSMarketRouter;
class RMMSTraderRouter;

template<typename TResponse>
struct RMMSGatewayPara {
	struct {
		bool bAllowedStart;
		InitType type;
		tstring para;
		tstring dllname;
		TResponse *presponse;
	} exchpara[RMMS_EXCH_SUM];
};

typedef RMMSGatewayPara<RMMSTraderRouter> RMMSTraderGatewayPara;
typedef RMMSGatewayPara<RMMSMarketRouter> RMMSMarketGatewayPara;

/**
 * Gateway Template
 */
template<typename TManager,typename TManagerPara>
class RMMSGateway 
{
public:
	RMMSGateway(){std::memset(&m_para,0,sizeof(m_para));}
	~RMMSGateway(){};

public:
	int32 Init(TManagerPara &para,std::map<Exchange,bool> *pexchs)
	{
		/* 暂时直接复制 */
		m_para = para;

		/* 加载全部允许的交易所,这一步不允许出错 */
		bool bsuccess = true;
		Exchange exch;
		RMMSExchPara exchpara;

		for (uint32 i = 0;i < (Exchange)RMMS_EXCH_SUM;i++) {
			exch = (Exchange)i;
			if(!m_para.exchpara[exch].bAllowedStart)
				continue;

			exchpara.para = m_para.exchpara[exch].para;
			exchpara.type = m_para.exchpara[exch].type;
			if(0 > AddExch(exch,m_para.exchpara[exch].dllname.c_str(),
				exchpara)) {
				bsuccess = false;
				break;
			}
			(*pexchs)[exch] = false;
		}

		if(!bsuccess){
			DecAllExch(pexchs);
			return -1;
		}

		return 0;
	}

	void Uninit(std::map<Exchange,bool> *pexchs) 
	{
		DecAllExch(pexchs);
	}

	int32 LoginExch(Exchange exch,void *ppara,std::map<Exchange,bool> *pexchs)
	{
		DBG_ASSERT((*pexchs).end() != (*pexchs).find(exch));
		if((0 > GInstanceRef(TManager).LoginExch(exch,ppara)))
			return -1;
		(*pexchs)[exch] = true;
		return 0;
	}

	void LogoutExch(Exchange exch,void *ppara,std::map<Exchange,bool> *pexchs)
	{
		DBG_ASSERT((*pexchs).end() != (*pexchs).find(exch));
		GInstanceRef(TManager).LogoutExch(exch,ppara);
		(*pexchs)[exch] = false;
	}

private:
	int32 AddExch(Exchange exch,const tchar *pdllname,RMMSExchPara &exchpara)
	{
		DBG_ASSERT(NULL != m_para.exchpara[exch].presponse);
		if(0 > GInstanceRef(TManager).AddExch(exch,pdllname,exchpara))
			return -1;
		(GInstanceRef(TManager).GetExchAdapter(exch))->RegisterResponse(
			m_para.exchpara[exch].presponse,NULL);
		if(!GInstanceRef(TManager).InitExch(exch)){
			GInstanceRef(TManager).DecExch(exch);
			return -1;
		}
		return 0;
	}

	void DecExch(Exchange exch)
	{
		GInstanceRef(TManager).UninitExch(exch);
		GInstanceRef(TManager).DecExch(exch);
	}

	void DecAllExch(std::map<Exchange,bool> *pexchs)
	{
		std::map<Exchange,bool>::iterator it;
		for(it = (*pexchs).begin();it != (*pexchs).end();it++)
			DecExch(it->first);
	}

private:
	TManagerPara m_para;
};

/**
 * Special Gateway:Market and Trader
 */
typedef RMMSGateway<RMMSExchMarketManager,RMMSMarketGatewayPara> 
	RMMSMarketGateway;

class RMMSTraderGateway : public RMMSGateway<RMMSExchTraderManager,
	RMMSTraderGatewayPara> {
public:
	static int32 Request(Exchange exch,FIX::Message &msg)
	{
		return GInstanceRef(RMMSExchTraderManager).Request(exch,msg);
	}
};

RMMS_NS_END

#ifdef _MSC_VER
#pragma warning(pop)
#else
#error "仅支持VC,以待修改"
#endif

#endif