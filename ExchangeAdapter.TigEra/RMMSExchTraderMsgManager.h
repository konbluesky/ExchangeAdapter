#ifndef __RMMS_EXCH_TRADER_MSG_MANAGER_H__
#define __RMMS_EXCH_TRADER_MSG_MANAGER_H__

#include "RMMSExchangeAdapterBase.h"
#include "RMMSExchMsgCacheManager.h"
#include "Mutex.h"
#include <map>
#include <queue>

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4251)
#pragma warning(disable:4275)
#else
#error "仅支持VC,以待修改"
#endif

struct sqlite3;

RMMS_NS_BEGIN

/**
 * 数据存储管理,按照数据库的标准实现
 */
class RMMSDatabase {
public:
	RMMSDatabase();
	virtual ~RMMSDatabase();
/**
 * 数据连接管理
 */
public:
	int32 Init(const tchar *pdsn,const tchar *ptableformat);
	void Uninit(void);

/**
 * 数据库属性
 */
public:
	tstring GetDatabaseName(void) const;
	bool IsOpen(void) const;

/**
 * 数据库操作
 */
public:
	bool ExecuteSQL(const tchar *psql);

protected:
	bool Open(const tchar *pdsn);
	void Close(void);
	bool IsTablePresent();
	bool CreateTable(const tchar *tableformat);

public:
	sqlite3 *m_pdb;

protected:
	tstring m_strdsn;
};

/**
 * 缓冲区管理
 */
class RMMSBufferManager {
public:
	RMMSBufferManager();
	virtual ~RMMSBufferManager();

public:
	struct DataUnit	{
		std::string agentid;
		std::string priclordid;
		std::string pubclordid;
		std::string priorigclordid;
	};
	int32 Insert(const tchar *pubclordid,DataUnit &data);
	void Delete(const tchar* pubclordid);
	int32 Get(const tchar* pubclordid,DataUnit *pdata,
		bool bdel = false);

private:
#define DATA_SIZE_LEN 1200
#define FREE_DATA_SIZE 200
	void Free(uint32 freedata = FREE_DATA_SIZE);

private:
	std::map<std::string,DataUnit> m_buffmap;
	std::queue<std::string> m_buffqueue;
};

class RMMSBufferManagerZZ {
public:
	RMMSBufferManagerZZ();
	virtual ~RMMSBufferManagerZZ();

public:
	struct DataUnit	{
		std::string agentid;
		std::string priclordid;
		std::string pubclordid;
		std::string priorigclordid;
		/* std::string account; */
	};
	int32 Insert(const tchar *pubclordid,DataUnit &data);
	void Delete(const tchar* pubclordid);
	int32 Get(const tchar* pubclordid,DataUnit *pdata,
		bool bdel = false);

private:
#define DATA_SIZE_LEN 1200
#define FREE_DATA_SIZE 200
	void Free(uint32 freedata = FREE_DATA_SIZE);

private:
	std::map<std::string,DataUnit> m_buffmap;
	std::queue<std::string> m_buffqueue;
};

/**
 * 简单业务业务管理机制
 */
class IRMMSExchMsgSimpleManager : public IRMMSExchMsgManager
{
public:
	IRMMSExchMsgSimpleManager(Exchange exch):
	  IRMMSExchMsgManager(exch){}
	  virtual ~IRMMSExchMsgSimpleManager(){}

public:
	virtual int32 CacheMsgInfo(const tchar *pagentid,const tchar *ppriclordid,
		FIX::Message *pmsg,RMMSClOrdIdManager *pclordidmanager,
		RMMSMsgSeqNumManager *pseqnummanager);
	virtual bool GetMsgInfo(FIX::Message *pmsg,tstring *pagentid);

protected:
	virtual bool IsHaveMsgInfo(const tchar *agentid,const tchar *ppriclordid,
		tstring *ppublocalid) = 0;
	virtual int32 LocalSaveMsgInfo(const tchar *pagentid,
		const tchar *ppriclordid,const tchar *ppublocalid,
		FIX::Message *pmsg,RMMSMsgSeqNumManager *pseqnummanager = NULL) = 0;
	virtual int32 LocalGetMsgInfo(FIX::Message *pmsg,tstring *pagentid) = 0;
	LIB_NS::Mutex m_rwmutex;
};

RMMS_NS_END

#ifdef _MSC_VER
#pragma warning(pop)
#else
#error "仅支持VC,以待修改"
#endif

#endif