/**
 * 简单业务处理机制,对于消息记录采用简单的处理方式管理
 * 目前暂时按照一个交易所一个处理.这样便于处理各个交易所的不同.
 * 虽然重复代码比较多,但是现在的业务暂时不是很熟悉,直接进行统一难度
 * 太大.所以待将来业务完全弄清楚了,那么再进行一次重构,将重复的地方去除.
 * 这样的方式比较容易.应该是最好的解决方法.
 */
#ifndef __RMMS_EXCH_TRADER_MSG_SIMPLE_MANAGER_H__
#define __RMMS_EXCH_TRADER_MSG_SIMPLE_MANAGER_H__

#include "RMMSExchangeAdapterBase.h"
#include "RMMSExchTraderMsgManager.h"
#include "Mutex.h"

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4251)
#pragma warning(disable:4275)
#else
#error "仅支持VC,以待修改"
#endif

RMMS_NS_BEGIN

class RMMSExchMsgSimpleManagerZZ:
	public IRMMSExchMsgSimpleManager
{
public:
	RMMSExchMsgSimpleManagerZZ(Exchange exch);
	virtual ~RMMSExchMsgSimpleManagerZZ();

public:
	virtual int32 Initialize(tstring databasename);
	virtual void Uninitialize();

protected:
	/* 针对特定交易所和GloTrader的处理 */
	virtual bool IsHaveMsgInfo(const tchar *agentid,const tchar *ppriclordid,
		tstring *ppublocalid);
	virtual int32 LocalSaveMsgInfo(const tchar *pagentid,
		const tchar *ppriclordid,const tchar *ppublocalid,
		FIX::Message *pmsg,RMMSMsgSeqNumManager *pseqnummanager = NULL);
	virtual int32 LocalGetMsgInfo(FIX::Message *pmsg,tstring *pagentid);

private:
	/* FIX消息处理 */
	int32 LocalSave_NewOrderSingle(const tchar *pagentid,
		const tchar *ppriclordid,const tchar *ppublocalid,FIX::Message *pmsg);
	int32 LocalSave_OrderCancelRequest(const tchar *pagentid,
		const tchar *ppriclordid,const tchar *ppublocalid,FIX::Message *pmsg);

	int32 LocalGet_ExecutionReport(FIX::Message *pmsg,
		tstring *pagentid);
	int32 LocalGet_BusinessReject(FIX::Message *pmsg,
		tstring *pagentid);
	int32 LocalGet_OrderCancelReject(FIX::Message *pmsg,
		tstring *pagentid);
	int32 ExecutionReport_Fill(FIX::Message *pmsg,double ordercum,
		double orderqty,const char *ppubordid);
	int32 ExecutionReport_Cancel(FIX::Message *pmsg,tstring *pagentid);

protected:
	RMMSDatabase *m_pdb;
	RMMSBufferManagerZZ *m_pbufmanager;
};

/**
 * 中金/上海简单业务处理
 */
class RMMSExchMsgSimpleManagerZJSH:
	public IRMMSExchMsgSimpleManager
{
public:
	RMMSExchMsgSimpleManagerZJSH(Exchange exch);
	virtual ~RMMSExchMsgSimpleManagerZJSH();

public:
	virtual int32 Initialize(tstring databasename);
	virtual void Uninitialize();

protected:
	/* 针对特定交易所和GloTrader的处理 */
	virtual bool IsHaveMsgInfo(const tchar *agentid,const tchar *ppriclordid,
		tstring *ppublocalid);
	virtual int32 LocalSaveMsgInfo(const tchar *pagentid,
		const tchar *ppriclordid,const tchar *ppublocalid,
		FIX::Message *pmsg,RMMSMsgSeqNumManager *pseqnummanager = NULL);
	virtual int32 LocalGetMsgInfo(FIX::Message *pmsg,tstring *pagentid);

private:
	/* FIX消息处理 */
	int32 LocalSave_NewOrderSingle(const tchar *pagentid,
		const tchar *ppriclordid,const tchar *ppublocalid,FIX::Message *pmsg);
	int32 LocalSave_OrderCancelRequest(const tchar *pagentid,
		const tchar *ppriclordid,const tchar *ppublocalid,FIX::Message *pmsg);

	int32 LocalGet_ExecutionReport(FIX::Message *pmsg,
		tstring *pagentid);
	int32 LocalGet_BusinessReject(FIX::Message *pmsg,
		tstring *pagentid);
	int32 LocalGet_OrderCancelReject(FIX::Message *pmsg,
		tstring *pagentid);
	int32 ExecutionReport_Fill(FIX::Message *pmsg,double ordercum,
		double orderqty,const char *ppubordid);

protected:
	RMMSDatabase *m_pdb;
	RMMSBufferManager *m_pbufmanager;
};

/**
 * 大连简单业务处理
 */
class RMMSExchMsgSimpleManagerDL:
	public IRMMSExchMsgSimpleManager
{
public:
	RMMSExchMsgSimpleManagerDL(Exchange exch);
	virtual ~RMMSExchMsgSimpleManagerDL();

public:
	virtual int32 Initialize(tstring databasename);
	virtual void Uninitialize();

protected:
	/* 针对特定交易所和GloTrader的处理 */
	virtual bool IsHaveMsgInfo(const tchar *agentid,const tchar *ppriclordid,
		tstring *ppublocalid);
	virtual int32 LocalSaveMsgInfo(const tchar *pagentid,
		const tchar *ppriclordid,const tchar *ppublocalid,
		FIX::Message *pmsg,RMMSMsgSeqNumManager *pseqnummanager = NULL);
	virtual int32 LocalGetMsgInfo(FIX::Message *pmsg,tstring *pagentid);

private:
	/* FIX消息处理 */
	int32 LocalSave_NewOrderSingle(const tchar *pagentid,
		const tchar *ppriclordid,const tchar *ppublocalid,FIX::Message *pmsg);
	int32 LocalSave_OrderCancelRequest(const tchar *pagentid,
		const tchar *ppriclordid,const tchar *ppublocalid,FIX::Message *pmsg);

	int32 LocalGet_ExecutionReport(FIX::Message *pmsg,
		tstring *pagentid);
	int32 LocalGet_BusinessReject(FIX::Message *pmsg,
		tstring *pagentid);
	int32 LocalGet_OrderCancelReject(FIX::Message *pmsg,
		tstring *pagentid);
	int32 ExecutionReport_Fill(FIX::Message *pmsg,double ordercum,
		double orderqty,const char *ppubordid);

protected:
	RMMSDatabase *m_pdb;
	RMMSBufferManager *m_pbufmanager;
};

/**
 * 目前认为FIX没有区别
 */
class RMMSExchMsgSimpleManagerFix:
	public IRMMSExchMsgSimpleManager
{
public:
	RMMSExchMsgSimpleManagerFix(Exchange exch);
	virtual ~RMMSExchMsgSimpleManagerFix();

public:
	virtual int32 Initialize(tstring databasename);
	virtual void Uninitialize();

protected:
	/* 针对特定交易所和GloTrader的处理 */
	virtual bool IsHaveMsgInfo(const tchar *agentid,const tchar *ppriclordid,
		tstring *ppublocalid);
	virtual int32 LocalSaveMsgInfo(const tchar *pagentid,
		const tchar *ppriclordid,const tchar *ppublocalid,FIX::Message *pmsg,
		RMMSMsgSeqNumManager *pseqnummanager = NULL);
	virtual int32 LocalGetMsgInfo(FIX::Message *pmsg,tstring *pagentid);

private:
	/* FIX消息处理 */
	int32 LocalSave(const tchar *pagentid,
		const tchar *ppriclordid,const tchar *ppublocalid,FIX::Message *pmsg);
	int32 LocalSave_Cancel(const tchar *pagentid,
		const tchar *ppriclordid,const tchar *ppublocalid,FIX::Message *pmsg);

	int32 LocalGet_ExeReport(FIX::Message *pmsg,tstring *pagentid);
	int32 LocalGet(FIX::Message *pmsg,tstring *pagentid);

protected:
	RMMSDatabase *m_pdb;
	RMMSBufferManager *m_pbufmanager;
};

RMMS_NS_END

#ifdef _MSC_VER
#pragma warning(pop)
#else
#error "仅支持VC,以待修改"
#endif

#endif