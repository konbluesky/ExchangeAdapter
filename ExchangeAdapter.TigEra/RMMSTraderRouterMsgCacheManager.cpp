#include "ExchangeAdapterConfig.h"
#include "RMMSTraderRouterMsgCacheManager.h"
#include "DebugUtils.h"
#include "ExchangeAdapterCompiler.h"

RMMS_NS_BEGIN

RMMSTraderRouterMsgCacheManager::RMMSTraderRouterMsgCacheManager():m_idmanager()
{
	std::memset(&m_para,0,sizeof(m_para));
}

RMMSTraderRouterMsgCacheManager::~RMMSTraderRouterMsgCacheManager(){}

/**
 * 暂时不要用goto来省略代码
 */
int32 RMMSTraderRouterMsgCacheManager::InitExchManager()
{
	if(0 > m_idmanager.Init())
		return -1;

	bool bsuccess = true;
	IRMMSExchMsgManager *pmsgmanager = NULL;

	for (int32 i = 0; i < (int32)RMMS_EXCH_SUM;i++){
		Exchange exch = (Exchange)i;
		if(!m_para.exchparas[exch].bsupport)
			continue;

		pmsgmanager = RMMSExchMsgManagerFactory::CreateManager(exch);
		if(NULL == pmsgmanager){
			bsuccess = false;
			break;
		}

		if(0 > m_idmanager.AddExch(exch)){
			delete pmsgmanager;
			pmsgmanager = NULL;
			bsuccess = false;
			break;
		}

		if(0 > m_seqmanager.AddExch(exch,m_para.exchparas[exch].seqfilename)){
			m_idmanager.DecExch(exch);
			delete pmsgmanager;
			pmsgmanager = NULL;
			bsuccess = false;
			break;
		}

		if(0 > pmsgmanager->Initialize(m_para.exchparas[exch].databasename)){
			m_seqmanager.DecExch(exch);
			m_idmanager.DecExch(exch);
			delete pmsgmanager;
			pmsgmanager = NULL;
			bsuccess = false;
			break;
		}

		m_exchmsg[exch] = pmsgmanager;
	}

	if (false == bsuccess){
		FreeMsgManager();
		return -1;
	}

	return 0;
}

void RMMSTraderRouterMsgCacheManager::FreeMsgManager()
{
	std::map<Exchange,IRMMSExchMsgManager*>::iterator it;
	for(it = m_exchmsg.begin();it != m_exchmsg.end();it++){
		it->second->Uninitialize();
		m_seqmanager.DecExch(it->first);
		m_idmanager.DecExch(it->first);
		delete (it->second);		
	}
	m_exchmsg.clear();
	m_idmanager.Uninit();
}

int32 RMMSTraderRouterMsgCacheManager::Init(
	RMMSTraderRouterMsgCacheManagerPara &para)
{
	m_para = para;

	if(0 > InitExchManager())
		return -1;

	return 0;
}

void RMMSTraderRouterMsgCacheManager::Uninit()
{
	FreeMsgManager();
}

int32 RMMSTraderRouterMsgCacheManager::CatchMsgInfo(const tchar* pagentid,
	const tchar* ppriclordid,Exchange exch,FIX::Message *pmsg)
{
	return m_exchmsg[exch]->CacheMsgInfo(pagentid,ppriclordid,pmsg,
		&m_idmanager,&m_seqmanager);
}

int32 RMMSTraderRouterMsgCacheManager::GetMsgInfo(Exchange exch,
	FIX::Message *pmsg,tstring* pagentid)
{
	return m_exchmsg[exch]->GetMsgInfo(pmsg,pagentid);
}

void RMMSTraderRouterMsgCacheManager::DecMsgSeqNum(Exchange exch)
{
	m_seqmanager.DecMsgSeqNum(exch);
}

uint32 RMMSTraderRouterMsgCacheManager::AddMsgSeqNum(Exchange exch)
{
	return m_seqmanager.AddMsgSeqNum(exch);
}

RMMS_NS_END