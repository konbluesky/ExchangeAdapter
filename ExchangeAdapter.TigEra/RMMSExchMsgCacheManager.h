#ifndef __RMMS_EXCH_MSG_CACHE_MANAGER_H__
#define __RMMS_EXCH_MSG_CACHE_MANAGER_H__

#include "RMMSExchangeAdapterBase.h"
#include "QuickFix.h"
#include "RMMSClOrdIdManager.h"
#include "RMMSMsgSeqNumManager.h"

RMMS_NS_BEGIN

class IRMMSExchMsgManager {
public:
	IRMMSExchMsgManager(Exchange exch):
		m_exch(exch),m_bstarted(false){}
	virtual ~IRMMSExchMsgManager(){}

public:
	virtual int32 Initialize(tstring databasename) = 0;
	virtual void Uninitialize() = 0;

	/***
	 * @Ret:小于0,Error;0,保存成功,并且直接发出了该消息
	 */
	virtual int32 CacheMsgInfo(const tchar *pagentid,const tchar *ppriclordid,
		FIX::Message *pmsg,RMMSClOrdIdManager *pclordidmanager,
		RMMSMsgSeqNumManager *pseqnummanager) = 0;

	/***
	 * @Ret:false,继续响应到客户端,true,处理完毕
	 */
	virtual bool GetMsgInfo(FIX::Message *pmsg,tstring *pagentid) = 0;
	
	bool IsStarted(){return m_bstarted;}

protected:
	Exchange m_exch;
	bool m_bstarted;
};

class RMMSExchMsgManagerFactory {
public:
	static IRMMSExchMsgManager* CreateManager(Exchange exch);
	static void DestoryManager(IRMMSExchMsgManager** pmanager);
};

RMMS_NS_END

#endif