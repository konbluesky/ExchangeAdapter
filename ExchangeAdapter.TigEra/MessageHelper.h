#ifndef __MESSAGEHELPER_H__
#define __MESSAGEHELPER_H__

class FIX::Message;
class MessageHelper
{
private:
	MessageHelper(void);
	~MessageHelper(void);
public:
	static bool IsHeartBeatMessage(const FIX::Message& message);
};

#endif