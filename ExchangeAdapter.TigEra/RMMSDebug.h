#ifndef __RMMS_DEBUG_H__
#define __RMMS_DEBUG_H__

#include "RMMSExchangeAdapterBase.h"
#include "IRMMSDebug.h"
#include <cstdio>
#include <cstdarg>

#ifdef _MSC_VER
#pragma warning(push)
#else
#error "仅支持VC,以待其他"
#endif

RMMS_NS_BEGIN

class RMMS_EXCHANGE_ADAPTER_API RMMSDebug : public IRMMSDebug
{
public:
	void SetDebugLevel(RMMSDebugLevel level){m_level = level;}

	int32 Debug(RMMSDebugLevel curlevel,const tchar* buf,...)
	{
		std::va_list arg_ptr;
		if(curlevel < m_level)
			return -1;

		va_start(arg_ptr,buf);
		std::vprintf(buf,arg_ptr);
		va_end(arg_ptr);

		return 0;
	}

	static IRMMSDebug* Instance(){return new RMMSDebug();}

protected:
	RMMSDebug()	{m_level = RMMS_DEBUG_NULL;}

protected:
	RMMSDebugLevel m_level;
};

RMMS_NS_END

#ifdef _MSC_VER
#pragma warning(pop)
#else
#error "仅支持VC,以待修改"
#endif

#endif