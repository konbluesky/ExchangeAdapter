#ifndef __EXCHANGE_ADAPTER_COMPILER_H__
#define __EXCHANGE_ADAPTER_COMPILER_H__

#ifdef _MSC_VER
#pragma warning(disable:4290)
#pragma warning(disable:4251)
#pragma warning(disable:4127)

/**
 * Release下存在的告警,因为DBG_TRACE不存在
 */
#if defined(NDEBUG)
#pragma warning(disable:4100)
#pragma warning(disable:4101)
#endif

#endif

#endif