#ifndef __RMMS_MARKET_ROUTER_H__
#define __RMMS_MARKET_ROUTER_H__

#include "RMMSExchangeAdapterBase.h"
#include "RMMSFixAcceptor.h"
#include "RMMSGateway.h"
#include "Mutex.h"
#define TIXML_USE_STL
#include "tinyxml.h"
#include "DebugUtils.h"

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4251)
#pragma warning(disable:4512)
#pragma warning(disable:4275)
#else
#error "仅支持VC,以待修改"
#endif

RMMS_NS_BEGIN

class RMMS_EXCHANGE_ADAPTER_API RMMSMarketRouter {
public:
	RMMSMarketRouter();
	virtual ~RMMSMarketRouter();

public:
	/**
	 * 初始化必须正确,才能开始,否则肯定会出错.
	 */
	int32 Init();
	void Uninit();

	int32 Start();
	void Stop();

	int32 StartExch(Exchange exch);
	void StopExch(Exchange exch);

public:
	/**
	 * 各个交易所已经保存所有的信息,所以为减少重复保存,这一步直接将其发送
	 * 后续如果认为必要,可以将所有的数据信息全部保存在相应的数据库,以备查询.
	 */
	virtual void MsgResponse(Exchange exch,const FIX::Message &msg);
	/**
	 * 管理
	 */
	virtual void AdmEvent_NetConnect(Exchange exch);
	virtual void AdmEvent_NetDisConnect(Exchange exch);
	virtual void AdmEvent_NetLogon(Exchange exch);
	virtual void AdmEvent_NetLogout(Exchange exch);

private:
	int32 GetXMLExchCfg(Exchange exch,
		TiXmlNode *pExchNode,RMMSMarketGatewayPara *pgatewaypara);

	int32 InitRouterPara(RMMSMarketGatewayPara *pgatewaypara,
		RMMSFixAcceptorPara *pfixacceptorpara);
	int32 InitFixAcceptor(RMMSFixAcceptorPara *pfixacceptorpara);
	int32 InitExchAdapter();

	void UninitRouterPara(RMMSMarketGatewayPara *pgatewaypara);
	void UninitFixAcceptor();
	void UninitExchAdapter();

	void FreeExchAdapter();

private:
	RMMSMarketGatewayPara m_exchpara;
	RMMSMarketGateway m_gateway;
	LIB_NS::Mutex m_recvmutexs[RMMS_EXCH_SUM];

	RMMSFixAcceptorPara m_fixpara;
	RMMSFixAcceptor m_fixacceptor;
	std::set<FIX::SessionID> m_seids;

	/**
	 * 后面需要支持单个交易所的停止和开始.为了保证这种情况,必须保证
	 * 前面一定不会存在问题,需要加这个参数,从而确保初始化一定没有
	 * 任何错误.
	 */
	bool m_binit;
	std::map<Exchange,bool> m_initexchs;
};

RMMS_NS_END

#ifdef _MSC_VER
#pragma warning(pop)
#else
#error "仅支持VC,以待修改"
#endif

#endif