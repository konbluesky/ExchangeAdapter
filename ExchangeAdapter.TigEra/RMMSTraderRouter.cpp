#include "ExchangeAdapterConfig.h"
#include "RMMSTraderRouter.h"
#include "DebugUtils.h"
#include "StringUtils.h"
#include "ExchangeAdapterCompiler.h"


RMMS_NS_BEGIN
const tchar *g_ptraderrouterxml = "RMMSTraderRouter.xml";

/*****************************************************************************/
/* Fix Session 管理
/*****************************************************************************/
RMMSTraderSessAgent::RMMSTraderSessAgent(FIX::SessionID &seID,
	RMMSTraderRouter *pmsgprocessor): IRMMSFixAcceptorAgent(seID),
	m_pmsgprocessor(pmsgprocessor){}

RMMSTraderSessAgent::~RMMSTraderSessAgent(){}

void RMMSTraderSessAgent::OnRecvApp(const FIX::Message& msg)
{
	m_pmsgprocessor->MsgRequest(m_agentid,msg);
}

void RMMSTraderSessAgent::Init()
{
	m_agentid = GetTargetID();
}

/*****************************************************************************/
/* 处理TraderRouter 
/*****************************************************************************/
RMMSTraderRouter::RMMSTraderRouter():m_sendmutexs(),
	m_fixacceptor(),m_gateway(),m_binit(false)
{
	std::memset(&m_exchpara,0,sizeof(RMMSTraderGatewayPara));
	std::memset(&m_msgmanagerpara,0,sizeof(
		RMMSTraderRouterMsgCacheManagerPara));
	std::memset(&m_fixpara,0,sizeof(RMMSFixAcceptorPara));
}

RMMSTraderRouter::~RMMSTraderRouter(){}

int32 RMMSTraderRouter::GetXMLExchCfg(Exchange exch,
	TiXmlNode *pExchNode,
	RMMSTraderGatewayPara *pgatewaypara,
	RMMSTraderRouterMsgCacheManagerPara *pmsgmanagerpara,
	std::map<tstring,Exchange> *pexchnames)
{
	const tchar *pxmltype = "type";
	const tchar *pxmlpara = "para";
	const tchar *pxmldllname = "dllname";
	const tchar *pxmlseqfilename = "seqfilename";
	const tchar *pxmldatabasename = "databasename";
	const tchar *pxmlexchname = "exchname";	
	TiXmlElement *pelement = NULL;

	pelement = pExchNode->FirstChildElement(pxmltype);
	if(0 == std::strcmp("XML",pelement->GetText()))
		pgatewaypara->exchpara[exch].type = RMMS_NS::XML;
	else
		return -1;

	pelement = pExchNode->FirstChildElement(pxmlpara);
	pgatewaypara->exchpara[exch].para = pelement->GetText();

	pelement = pExchNode->FirstChildElement(pxmldllname);
	pgatewaypara->exchpara[exch].dllname = pelement->GetText();

	switch(exch){
	case RMMS_CN_ZJ:
	case RMMS_CN_SH:
		pelement = pExchNode->FirstChildElement(pxmlseqfilename);
		pmsgmanagerpara->exchparas[exch].seqfilename = pelement->GetText();
		break;
	default:
		break;
	}

	pelement = pExchNode->FirstChildElement(pxmldatabasename);
	pmsgmanagerpara->exchparas[exch].databasename = pelement->GetText();

	pelement = pExchNode->FirstChildElement(pxmlexchname);
	if(pexchnames->end() != pexchnames->find(pelement->GetText()))
		return -1;
	(*pexchnames)[pelement->GetText()] = exch;

	

	return 0;
}

void RMMSTraderRouter::FreeFixSession()
{
	std::map<tstring,RMMSTraderSessAgent*>::iterator it;
	for(it = m_fixsession.begin();it != m_fixsession.end();it++){
		m_fixacceptor.UnregisterAgent(it->second->GetSessionID());
		delete it->second;
	}

	m_fixsession.clear();
}

void RMMSTraderRouter::FreeExchAdapter()
{
}

int32 RMMSTraderRouter::InitRouterPara(RMMSTraderGatewayPara *pgatewaypara,
	RMMSTraderRouterMsgCacheManagerPara *pmsgmanagerpara,
	RMMSFixAcceptorPara *pfixacceptorpara,
	std::map<tstring,Exchange> *pexchnames)
{
	const tchar *psubname = "supportexchs";
	const tchar *pexchname = "exchange";
	const tchar *pfixnodename = "FixAcceptor";
	const tchar *pcfgname = "cfgname";

	TiXmlDocument doc(g_ptraderrouterxml);
	if (!doc.LoadFile())
		return -1;

	TiXmlNode *pExchNode = NULL,*pFixNode = NULL,*pTmpNode = NULL;
	TiXmlElement *pRootElement = NULL,*pTmpElement = NULL;
	RMMS_NS::Exchange exch;

	pRootElement = doc.RootElement();

	pFixNode = pRootElement->FirstChild(pfixnodename);
	pTmpElement = pFixNode->FirstChildElement(pcfgname);
	pfixacceptorpara->cfgname = pTmpElement->GetText();
	
	pTmpNode = pRootElement->FirstChild(psubname);
	pTmpNode = pTmpNode->FirstChild(pexchname);
	for (;pTmpNode != NULL;pTmpNode = pTmpNode->NextSibling(pexchname)){
		pTmpElement = dynamic_cast<TiXmlElement*>(pTmpNode);

		if(0 == std::strcmp("DCE",pTmpElement->GetText()))
			exch = RMMS_CN_DL;
		else if (0 == std::strcmp("ZCE",pTmpElement->GetText()))
			exch = RMMS_CN_ZZ;
		else if (0 == std::strcmp("SHFE",pTmpElement->GetText()))
			exch = RMMS_CN_SH;
		else if (0 == std::strcmp("CFFEX",pTmpElement->GetText()))
			exch = RMMS_CN_ZJ;
		else if (0 == std::strcmp("CME",pTmpElement->GetText()))
			exch = RMMS_US_CME;
		else
			exch = RMMS_EXCH_UNKNOWN;

		if(RMMS_EXCH_UNKNOWN == exch)
			return -1;

		pgatewaypara->exchpara[exch].bAllowedStart = true;
		pmsgmanagerpara->exchparas[exch].bsupport = true;

		pExchNode = pRootElement->FirstChild(pTmpElement->GetText());
		if(0 > GetXMLExchCfg(exch,pExchNode,pgatewaypara,pmsgmanagerpara,
			pexchnames))
			return -1;
	}
	return 0;
}

int32 RMMSTraderRouter::InitFixAcceptor(RMMSFixAcceptorPara *pfixacceptorpara)
{
	std::set<FIX::SessionID> seids;
	std::set<FIX::SessionID>::iterator it;
	try{
		seids = m_fixacceptor.InitSession(pfixacceptorpara->cfgname);
		if (0 >= seids.size())
			return -1;

		for (it = seids.begin();it != seids.end();it++){
			RMMSTraderSessAgent *pagent = new RMMSTraderSessAgent(
				*const_cast<FIX::SessionID*>(&(*it)),this);
			m_fixsession[pagent->GetTargetID()] = pagent;
			m_fixacceptor.RegisterAgent(*it,pagent);
			pagent->Init();
		}
	} catch(...){
		std::cout<<"FIX Acceptor Config Initialize Exception!"<<std::endl;
		FreeFixSession();
		return -1;
	}

	return m_fixacceptor.Init();
}

int32 RMMSTraderRouter::InitExchAdapter()
{
	for (int32 i = 0; i < (int32)RMMS_EXCH_SUM;i++){
		Exchange exch = (Exchange)i;
		m_exchpara.exchpara[exch].presponse = this;
	}

	if(0 > m_gateway.Init(m_exchpara,&m_initexchs))
		return -1;
	
	return 0;
}

int32 RMMSTraderRouter::InitTraderRouter()
{
	if (0 > m_msgmanager.Init(m_msgmanagerpara))
		return -1;

	return 0;
}

void RMMSTraderRouter::UninitRouterPara(RMMSTraderGatewayPara *pgatewaypara)
{
	std::memset(pgatewaypara,0,sizeof(*pgatewaypara));
}

void RMMSTraderRouter::UninitFixAcceptor()
{
	FreeFixSession();
}

void RMMSTraderRouter::UninitExchAdapter()
{
	m_gateway.Uninit(&m_initexchs);
	FreeExchAdapter();
}

void RMMSTraderRouter::UninitTraderRouter()
{
	m_msgmanager.Uninit();
}

int32 RMMSTraderRouter::Init()
{
	if(0 > InitRouterPara(&m_exchpara,&m_msgmanagerpara,&m_fixpara,
		&m_exchnames))
		return -1;
	if(0 > InitFixAcceptor(&m_fixpara)){
		UninitRouterPara(&m_exchpara);
		return -1;
	}
	if(0 > InitTraderRouter()){
		UninitFixAcceptor();
		UninitRouterPara(&m_exchpara);
		return -1;
	}
	
	if(0 > InitExchAdapter()){
		UninitTraderRouter();
		UninitFixAcceptor();
		UninitRouterPara(&m_exchpara);
		return -1;
	}

	m_binit = true;
	return 0;
}

void RMMSTraderRouter::Uninit()
{
	UninitTraderRouter();
	UninitExchAdapter();
	UninitFixAcceptor();
	UninitRouterPara(&m_exchpara);
}

int32 RMMSTraderRouter::Start()
{
	if(!m_binit)
		return -1;

	if(0 > m_fixacceptor.Start())
		return -1;

	std::map<Exchange,bool>::iterator it = m_initexchs.begin();
	for (;it != m_initexchs.end();it++){
		switch(it->first){
		case RMMS_CN_SH:
		case RMMS_CN_ZJ:
			break;
		default:
			StartExch(it->first);
			break;
		}
	}

	return 0;
}

void RMMSTraderRouter::Stop()
{
	std::map<Exchange,bool>::iterator it = m_initexchs.begin();
	for (;it != m_initexchs.end();it++){
		if(false == it->second)
			continue;
		m_gateway.LogoutExch(it->first,(void*)(m_msgmanager.AddMsgSeqNum(
			it->first)),&m_initexchs);
	}
	m_fixacceptor.Stop();
}

int32 RMMSTraderRouter::StartExch(Exchange exch)
{
	if(m_initexchs.end() == m_initexchs.find(exch))
		return -1;
	if(true == m_initexchs[exch])
		return 1;
	if(0 > m_gateway.LoginExch(exch,(void*)(m_msgmanager.AddMsgSeqNum(exch)),
		&m_initexchs)){
		m_msgmanager.DecMsgSeqNum(exch);
		return -1;
	}

	switch(exch){
	case RMMS_CN_ZJ:
		std::cout<<"中金交易所启动成功"<<std::endl;
		break;
	case RMMS_CN_ZZ:
		std::cout<<"郑州交易所启动成功"<<std::endl;
		break;
	case RMMS_CN_SH:
		std::cout<<"上海交易所启动成功"<<std::endl;
		break;
	case RMMS_CN_DL:
		std::cout<<"大连交易所启动成功"<<std::endl;
		break;
	case RMMS_US_CME:
		std::cout<<"CME交易所启动成功"<<std::endl;
		break;
	}

	return 0;
}

void RMMSTraderRouter::StopExch(Exchange exch)
{
	if(m_initexchs.end() == m_initexchs.find(exch))
		return;
	if(false == m_initexchs[exch])
		return;
	m_gateway.LogoutExch(exch,(void*)(m_msgmanager.AddMsgSeqNum(exch)),
		&m_initexchs);
}

int32 RMMSTraderRouter::RejectRequest(const tchar* pagentid,
	const tchar* ppriclordid,int32 errorid,FIX::Message &msg)
{
	UNREF_VAR(pagentid);
	UNREF_VAR(ppriclordid);
	UNREF_VAR(errorid);
	UNREF_VAR(msg);

	tstring tmpstr = "RMMS" + LIB_NS::UStrConvUtils::ToString(errorid);

	FIX::Message tmpmsg;
	FIX::Header &header = tmpmsg.getHeader();
	
	header.setField(FIX::MsgType(FIX::MsgType_BusinessMessageReject));
	tmpmsg.setField(FIX::Text(tmpstr));
	tmpmsg.setField(FIX::RefMsgType(msg.getHeader().getField(
		FIX::FIELD::MsgType)));
	tmpmsg.setField(FIX::ClOrdID(ppriclordid));
	
	LIB_NS::Locker locker(m_sendmutexs);
	RMMSFixAcceptor::SendMsg(*const_cast<FIX::SessionID*>(
		&(m_fixsession[pagentid]->GetSessionID())),
		*const_cast<FIX::Message*>(&tmpmsg));

	return 0;
}

void RMMSTraderRouter::RequestWorkProc(const tstring &agentid,
	const FIX::Message &msg)
{
	FIX::Message *pmsg = const_cast<FIX::Message*>(&msg);
	tstring priclordid = msg.getField(FIX::FIELD::ClOrdID);
	tstring strsecurityexch = msg.getField(FIX::FIELD::SecurityExchange);
	int32 errorcode = 0;

	if(m_exchnames.end() == m_exchnames.find(strsecurityexch)){
		RejectRequest(agentid.c_str(),priclordid.c_str(),RMMS_ERR_NOT_SUPPORT,
			*pmsg);
		return;
	}

	Exchange exch = m_exchnames[strsecurityexch];
	if(m_initexchs.end() == m_initexchs.find(exch)){
		RejectRequest(agentid.c_str(),priclordid.c_str(),RMMS_ERR_EXCH_NAME,
			*pmsg);
		return;
	}

	if(0 > m_msgmanager.CatchMsgInfo(agentid.c_str(),priclordid.c_str(),
		exch,pmsg)){
		RejectRequest(agentid.c_str(),priclordid.c_str(),RMMS_ERR_UNKNOWN,
			*pmsg);
		return;
	}

	errorcode = RMMSTraderGateway::Request(exch,
		*const_cast<FIX::Message*>(&msg));
	if(0 > errorcode){
		m_msgmanager.DecMsgSeqNum(exch);
		RejectRequest(agentid.c_str(),priclordid.c_str(),errorcode,*pmsg);
	}
}

void RMMSTraderRouter::MsgRequest(const tstring &agentid,
	const FIX::Message& msg)
{
	/* 将来建立存储文件 */
	RequestWorkProc(agentid,msg);
}

void RMMSTraderRouter::MsgResponse(Exchange exch,const FIX::Message& msg)
{
	tstring agentid = "";
	m_msgmanager.GetMsgInfo(exch,const_cast<FIX::Message*>(&msg),&agentid);
	if ("" == agentid || m_fixsession.end() == m_fixsession.find(agentid)){
		/* 这一步,目前认为不可能出现,所以如果出现了,那么报错 */
		DBG_TRACE(("!!!!!! 没有找到分配的单子,可能已经丢失\n"));
		// 后面这一步一定要记录日志,方便用户恢复
		return;
	}

	LIB_NS::Locker locker(m_sendmutexs);
	RMMSFixAcceptor::SendMsg(*const_cast<FIX::SessionID*>(
		&(m_fixsession[agentid]->GetSessionID())),
		*const_cast<FIX::Message*>(&msg));
}

void RMMSTraderRouter::AdmEvent_NetConnect(Exchange exch)
{
	switch(exch){
	case RMMS_CN_SH:
	case RMMS_CN_ZJ:
		StartExch(exch);
		break;
	default:
		break;
	}
}

void RMMSTraderRouter::AdmEvent_NetDisConnect(Exchange /*exch*/)
{
}

void RMMSTraderRouter::AdmEvent_NetLogon(Exchange /*exch*/)
{
	std::cout<<"登陆成功"<<std::endl;
}

void RMMSTraderRouter::AdmEvent_NetLogout(Exchange /*exch*/)
{
}

RMMS_NS_END