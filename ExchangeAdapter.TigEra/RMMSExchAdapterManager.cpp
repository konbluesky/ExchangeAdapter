#include "ExchangeAdapterConfig.h"
#include "RMMSExchAdapterCommon.h"
#include "ExchangeAdapterCompiler.h"

/**
 * !!!注意
 * 在C++中,基类到子类的转换过程中,最好不要一步跨过.
 * 即Parent* -> void* -> Parent* -> Son*
 * 不要Parent*- > void* -> void * -> Son*
 */
RMMS_NS_BEGIN

#define TRADER_CREATE "CreateTraderProvider"
#define TRADER_DESTORY "DestoryTraderProvider"
#define MARKET_CREATE "CreateMarketDataProvider"
#define MARKET_DESTORY "DestoryMarketDataProvider"

typedef uint32 (*pCreateTraderFix)(void);
typedef void (*pDestoryTraderFix)(IFixTraderProvider* adapter);

typedef uint32 (*pCreateMarketFix)(void);
typedef void (*pDestoryMarketFix)(IFixMarketProvider* adapter);

typedef uint32 (*pCreateFun)(void);
typedef void (*pDestoryFun)(void *pAdapter);

template <typename TCreateFunc,typename TDestoryFunc,typename TProvider,
	typename TAdapter> 
static inline TAdapter* CreateAdapter(RMMSExchDllManager &exchdll,
	const tchar *pcreatefunname,const tchar *pdestoryfunname)
{
	TCreateFunc pCreateFunc = (TCreateFunc)exchdll.GetDllPFun(pcreatefunname);
	TDestoryFunc pDestoryFunc = (TDestoryFunc)exchdll.GetDllPFun(
		pdestoryfunname);
	DBG_ASSERT(NULL != pCreateFunc);
	DBG_ASSERT(NULL != pDestoryFunc);

	TProvider *_tmpprovder = (TProvider*)pCreateFunc();
	if(NULL == _tmpprovder)		
		return NULL;

	TAdapter *ptmp = new TAdapter(exchdll.GetExch(),_tmpprovder);
	if(NULL == ptmp)
		pDestoryFunc(_tmpprovder);
	
	return ptmp;
}

template <typename TDestoryFunc,typename TProvider,typename TAdapter> 
static inline void DestoryAdapter(RMMSExchDllManager &exchdll,
	TAdapter** pexchadapter,const tchar *pdestoryfunname)
{
	DBG_ASSERT(NULL != pexchadapter && NULL != *pexchadapter);
	TDestoryFunc pDestoryFunc = (TDestoryFunc)exchdll.GetDllPFun(
		pdestoryfunname);
	DBG_ASSERT(NULL != pDestoryFunc);

	TAdapter *ptmp = dynamic_cast<TAdapter *>(*pexchadapter);
	TProvider *_tmpprovider = ptmp->GetProvider();

	pDestoryFunc(_tmpprovider);
	delete ptmp;
	ptmp = NULL;
}

template <typename TProvider,typename TAdapter> 
static inline bool InitAdapter(TAdapter* pexchadapter,RMMSExchPara &para)
{
	bool bret = false;

	TAdapter *ptmp = dynamic_cast<TAdapter *>(pexchadapter);
	TProvider *_tmpprovider = ptmp->GetProvider();
	switch(para.type){
	case XML:
		bret = _tmpprovider->InitializeByXML(para.para.c_str());
		break;
	default:
		return NULL;
	}

	return bret;
}

template <typename TProvider,typename TAdapter> 
static inline void UninitAdapter(TAdapter* pexchadapter)
{
	DBG_ASSERT(NULL != pexchadapter);
	
	TAdapter *ptmp = dynamic_cast<TAdapter *>(pexchadapter);
	TProvider *_tmpprovider = ptmp->GetProvider();
	_tmpprovider->UnInitialize();
}

template<typename TProvider,typename TAdapter>
static inline bool LoginAdapter(TAdapter* pexchadapter,void *ppara)
{
	TAdapter *ptmp = dynamic_cast<TAdapter *>(pexchadapter);
	TProvider *_tmpprovider = ptmp->GetProvider();
	return _tmpprovider->Login(ppara);
}

template<typename TProvider,typename TAdapter>
static inline bool LogoutAdapter(TAdapter* pexchadapter,void *ppara)
{
	TAdapter *ptmp = dynamic_cast<TAdapter *>(pexchadapter);
	TProvider *_tmpprovider = ptmp->GetProvider();
	return _tmpprovider->Logout(ppara);
}

/* 暂时先定为这样,后续根据需要调整代码中的设计,将市场和交易部分分开成不同的协议 */
#define ExchAdapterCommon(func,ExpA,ExpB) \
int32 __tmp = 0;					\
do{									\
	switch(func){					\
	case TRADER:					\
		ExpA;						\
		break;						\
	case MARKET:					\
		ExpB;						\
		break;						\
	}								\
}while(__tmp)

void* RMMSExchAdapterFactory::CreateExchAdapter(FuncType func,
	RMMSExchDllManager &exchdll)
{
	void *pret = NULL;
	ExchAdapterCommon(func,
		(pret = CreateAdapter<pCreateFun,pDestoryFun,IFixTraderProvider,
		RMMSExchTraderAdapter>(exchdll,TRADER_CREATE,TRADER_DESTORY)),
		(pret = CreateAdapter<pCreateFun,pDestoryFun,IFixMarketProvider,
		RMMSExchMarketAdapter>(exchdll,MARKET_CREATE,MARKET_DESTORY)));
	return pret;
}

void RMMSExchAdapterFactory::DestoryExchAdapter(FuncType func,
	RMMSExchDllManager &exchdll,void** pexchadapter)
{
	ExchAdapterCommon(func,
		(DestoryAdapter<pDestoryFun,IFixTraderProvider,
		RMMSExchTraderAdapter>(exchdll,(RMMSExchTraderAdapter**)pexchadapter,
		TRADER_DESTORY)),
		(DestoryAdapter<pDestoryFun,IFixMarketProvider,
		RMMSExchMarketAdapter>(exchdll,(RMMSExchMarketAdapter**)pexchadapter,
		MARKET_DESTORY)));
}

bool RMMSExchAdapterFactory::InitExchAdapter(FuncType func,
	void* pexchadapter,RMMSExchPara &para)
{
	bool pret = false;
	ExchAdapterCommon(func,
		(pret = InitAdapter<IFixTraderProvider,RMMSExchTraderAdapter>(
		(RMMSExchTraderAdapter*)pexchadapter,para)),
		(pret = InitAdapter<IFixMarketProvider,RMMSExchMarketAdapter>(
		(RMMSExchMarketAdapter*)pexchadapter,para)));
	return pret;
}

void RMMSExchAdapterFactory::UninitExchAdapter(FuncType func,
	void* pexchadapter)
{
	ExchAdapterCommon(func,
		(UninitAdapter<IFixTraderProvider,RMMSExchTraderAdapter>(
		(RMMSExchTraderAdapter*)pexchadapter)),
		(UninitAdapter<IFixMarketProvider,RMMSExchMarketAdapter>(
		(RMMSExchMarketAdapter*)pexchadapter)));
}

bool RMMSExchAdapterFactory::LoginExchAdapter(FuncType func,
	void* pexchadapter,void *ppara)
{
	bool bstart = false;
	ExchAdapterCommon(func,
		(bstart = LoginAdapter<IFixTraderProvider,
		RMMSExchTraderAdapter>((RMMSExchTraderAdapter*)pexchadapter,ppara)),
		(bstart = LoginAdapter<IFixMarketProvider,
		RMMSExchMarketAdapter>((RMMSExchMarketAdapter*)pexchadapter,ppara)));
	return bstart;
}

bool RMMSExchAdapterFactory::LogoutExchAdapter(FuncType func,
	void* pexchadapter,void *ppara)
{
	bool bstop = false;
	ExchAdapterCommon(func,
		(bstop = LogoutAdapter<IFixTraderProvider,RMMSExchTraderAdapter>(
		(RMMSExchTraderAdapter*)pexchadapter,ppara)),
		(bstop = LogoutAdapter<IFixMarketProvider,RMMSExchMarketAdapter>(
		(RMMSExchMarketAdapter*)pexchadapter,ppara)));
	return bstop;
}

RMMS_NS_END