#include "ExchangeAdapterConfig.h"
#include "RMMSExchMsgCacheManager.h"
#include "RMMSExchTraderMsgSimpleManager.h"
#include "DebugUtils.h"
#include "ExchangeAdapterCompiler.h"

RMMS_NS_BEGIN

IRMMSExchMsgManager* RMMSExchMsgManagerFactory::CreateManager(
	Exchange exch)
{
	switch(exch){
	case RMMS_CN_DL:
		return new RMMSExchMsgSimpleManagerDL(exch);
		break;
	case RMMS_CN_ZZ:
		return new RMMSExchMsgSimpleManagerZZ(exch);
		break;
	case RMMS_CN_SH:
	case RMMS_CN_ZJ:
		return new RMMSExchMsgSimpleManagerZJSH(exch);
		break;;
	case RMMS_US_CME:
		return new RMMSExchMsgSimpleManagerFix(exch);
		break;
	default:
		DBG_ASSERT(false);
		return NULL;
		break;
	}
}

void RMMSExchMsgManagerFactory::DestoryManager(
	IRMMSExchMsgManager** pmanager)
{
	if(NULL != pmanager && NULL != *pmanager){
		delete *pmanager;
		*pmanager = NULL;
	}
}

RMMS_NS_END

