#ifndef __RMMS_CLORD_ID_MANAGER_H__
#define __RMMS_CLORD_ID_MANAGER_H__

/***
 * 目前的设计,我觉得应该能够尽可能的避开过多的设计或者管理模式,而实现
 * 最简单的管理模式,即将所有的注册管理既骄傲给具体的使用者,而不是管理
 * 的人去完成.
 * 将所有的工作分成阶段,先想清楚具体的事情应该怎么做,再做.
 * 不要光想,不要光做.先做后想,先想后做.
 * 说要表达清楚.做要理顺关系.
 *
 * 做事切忌三心二意.即认定一个目标,就不要再轻易改动,待完成目标之后,在
 * 决定事情的得失,然后继续做事.这样才能保证进步.否则想的过多,老是更改
 * 将很难决定一件事情的发展.
 * 
 * API封装,要求能够透明的传输.而实现功能,则是隐藏细节.
 * 这是两种思维过程.后面要切记.
 */

#include "RMMSExchangeAdapterBase.h"

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4251)
#pragma warning(disable:4275)
#else
#error "仅支持VC,以待修改"
#endif

struct sqlite3;

RMMS_NS_BEGIN

class RMMSClOrdIdManager {
public:
	RMMSClOrdIdManager();
	~RMMSClOrdIdManager();

public:
	int32 Init();
	void Uninit();

	int32 AddExch(Exchange exch);
	void DecExch(Exchange exch);

	const tchar* GetNewLocalId(Exchange exch);

private:
	int32 InitClOrdID(Exchange exch);
	void UinitClOrdID(Exchange exch);
	int32 WriteClOrdID(Exchange exch);
	int32 ReadClOrdID(Exchange exch);
	int32 AddClOrdId(Exchange exch);
	int32 DecClOrdId(Exchange exch);
	int32 TcharInc(Exchange exch,tchar *poldc);
	int32 TcharDec(Exchange exch,tchar *poldc);
	void InitExchInfo(Exchange exch);
	tchar *GetExchID(Exchange exch);

	/* 各个交易所ClOrdID长度 */
	enum {
		ORD_ZJ_LEN = 12,
		ORD_DL_LEN = 10,
		/* 
		 * 实际上支持24位,但是fix只支持21位,所以这里只用20位,最后
		 * 一位'\0' 
		 */
		ORD_ZZ_LEN = 20,
		ORD_SH_LEN = 12,
		ORD_CME_LEN = 20,
	};

	struct ClOrdIDExchInfo {
		bool ballowed;
		tstring cfgfilename;
		FILE *cfgfilehandle;
		uint8 idlen;
		union {
			tchar zjclordid[ORD_ZJ_LEN+1];
			tchar dlclordid[ORD_DL_LEN+1];
			tchar zzclordid[ORD_ZZ_LEN+1];
			tchar shclordid[ORD_SH_LEN+1];
			tchar cmeclordid[ORD_CME_LEN+1];
		};
	};
	ClOrdIDExchInfo m_clordidinfo[RMMS_EXCH_SUM];
};

RMMS_NS_END

#ifdef _MSC_VER
#pragma warning(pop)
#else
#error "仅支持VC,以待修改"
#endif

#endif