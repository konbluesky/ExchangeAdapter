#ifndef __RMMS_TRADER_ROUTER_H__
#define __RMMS_TRADER_ROUTER_H__

/**
 * 目前从我查询的情况来看,STL中的内存代码在如不调用专门的函数,是不会立即释放
 * 内存的.所以后面可以利用这种情况,对于经常用到的地方的tstring,全部用固定的
 * 变量,减少内存申请释放的过程.
 * 
 * 后面最好增加针对国内交易所的处理.针对每个国内交易所的通信方式,都不完全
 * 实现对齐.
 *
 * 以后在设计的时候,如果没有经验,不要一上来就设计统一的接口,这样只会添麻烦
 * 真正的接口是在业务的基础上弄好的.
 */
#include "RMMSExchangeAdapterBase.h"
#include "RMMSFixAcceptor.h"
#include "RMMSGateway.h"
#include "RMMSTraderRouterMsgCacheManager.h"
#define TIXML_USE_STL
#include "tinyxml.h"
#include <map>
#include "Mutex.h"

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4251)
#pragma warning(disable:4275)
#else
#error "仅支持VC,以待修改"
#endif

RMMS_NS_BEGIN

class RMMSTraderRouter;

class RMMSTraderSessAgent : public IRMMSFixAcceptorAgent {
public:
	RMMSTraderSessAgent(FIX::SessionID &seID,RMMSTraderRouter *presponse);
	virtual ~RMMSTraderSessAgent();

public:
	void OnRecvApp(const FIX::Message& msg);
	void Init();
private:
	RMMSTraderRouter *m_pmsgprocessor;
	tstring m_agentid;
};

class RMMS_EXCHANGE_ADAPTER_API RMMSTraderRouter {
public:
	RMMSTraderRouter();
	~RMMSTraderRouter();

public:
	/**
	 * 初始化必须正确,才能开始,否则肯定会出错.
	 */
	int32 Init();
	void Uninit();

	int32 Start();
	void Stop();

	int32 StartExch(Exchange exch);
	void StopExch(Exchange exch);

public:
	virtual void MsgRequest(const tstring &agentid,const FIX::Message &msg);

	/**
	 * 各个交易所已经保存所有的信息,所以为减少重复保存,这一步直接将其发送
	 * 后续如果认为必要,可以将所有的数据信息全部保存在相应的数据库,以备查询.
	 */
	virtual void MsgResponse(Exchange exch,const FIX::Message &msg);

	/**
	 * 管理
	 */
	virtual void AdmEvent_NetConnect(Exchange exch);
	virtual void AdmEvent_NetDisConnect(Exchange exch);
	virtual void AdmEvent_NetLogon(Exchange exch);
	virtual void AdmEvent_NetLogout(Exchange exch);

private:
	int32 GetXMLExchCfg(Exchange exch,
		TiXmlNode *pExchNode,
		RMMSTraderGatewayPara *pgatewaypara,
		RMMSTraderRouterMsgCacheManagerPara *pmsgmanagerpara,
		std::map<tstring,Exchange> *pexchnames);

	int32 InitRouterPara(RMMSTraderGatewayPara *pgatewaypara,
		RMMSTraderRouterMsgCacheManagerPara *pmsgmanagerpara,
		RMMSFixAcceptorPara *pfixacceptorpara,
		std::map<tstring,Exchange> *pexchnames);
	int32 InitFixAcceptor(RMMSFixAcceptorPara *pfixacceptorpara);
	int32 InitExchAdapter();
	int32 InitTraderRouter();

	void UninitRouterPara(RMMSTraderGatewayPara *pgatewaypara);
	void UninitFixAcceptor();
	void UninitExchAdapter();
	void UninitTraderRouter();

	void FreeFixSession();
	void FreeExchAdapter();

	int32 RejectRequest(const tchar* pagentid,
		const tchar* ppriclordid,int32 errorid,FIX::Message &msg);
	void RequestWorkProc(const tstring &agentid,const FIX::Message &msg);
	
private:
	RMMSTraderRouterMsgCacheManagerPara m_msgmanagerpara;
	RMMSTraderRouterMsgCacheManager m_msgmanager;
	std::map<tstring,Exchange> m_exchnames;

	RMMSTraderGatewayPara m_exchpara;
	RMMSTraderGateway m_gateway;
	LIB_NS::Mutex m_sendmutexs;
	
	RMMSFixAcceptorPara m_fixpara;
	RMMSFixAcceptor m_fixacceptor;
	std::map<tstring,RMMSTraderSessAgent*> m_fixsession;
	std::map<tstring,FIX::SessionID> m_fixseids;

	/**
	 * 后面需要支持单个交易所的停止和开始.为了保证这种情况,必须保证
	 * 前面一定不会存在问题,需要加这个参数,从而确保初始化一定没有
	 * 任何错误.
	 */
	std::map<Exchange,bool> m_initexchs;
	bool m_binit;
};

RMMS_NS_END

#ifdef _MSC_VER
#pragma warning(pop)
#else
#error "仅支持VC,以待修改"
#endif

#endif