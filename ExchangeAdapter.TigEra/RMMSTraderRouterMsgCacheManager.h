#ifndef __RMMS_TRADER_ROUTER_CACHE_MSG_MANAGER_H__
#define __RMMS_TRADER_ROUTER_CACHE_MSG_MANAGER_H__

/***
 * 目前的设计,我觉得应该能够尽可能的避开过多的设计或者管理模式,而实现
 * 最简单的管理模式,即将所有的注册管理既骄傲给具体的使用者,而不是管理
 * 的人去完成.
 * 将所有的工作分成阶段,先想清楚具体的事情应该怎么做,再做.
 * 不要光想,不要光做.先做后想,先想后做.
 * 说要表达清楚.做要理顺关系.
 * 
 * 从目前的设想来看,基本的设计思路已经定型.就是所有的数据都写记录.同时
 * 对目前的版本采用缓冲加速.
 */

#include "RMMSExchangeAdapterBase.h"
#include "RMMSExchMsgCacheManager.h"
#include "RMMSClOrdIdManager.h"
#include "RMMSMsgSeqNumManager.h"
#include <map>

RMMS_NS_BEGIN

/*****************************************************************************/
/* RMMSTraderRouterMsgManager
/* 作为消息存储的关键,后面将保持恒定,不再进行接口变动.
/* 然后增加其他加强功能即可.
/* 
/* 以后将保持不变.
/*****************************************************************************/
struct RMMSTraderRouterMsgCacheManagerPara {
	struct {
		bool bsupport;
		tstring databasename;
		tstring seqfilename;
	} exchparas[RMMS_EXCH_SUM];
};

class RMMSTraderRouterMsgCacheManager {
public:
	RMMSTraderRouterMsgCacheManager();
	~RMMSTraderRouterMsgCacheManager();

public:
	int32 Init(RMMSTraderRouterMsgCacheManagerPara &para);
	void Uninit();
	int32 CatchMsgInfo(const tchar* pagentid,const tchar* ppriclordid,
		Exchange exch,FIX::Message *pmsg);
	int32 GetMsgInfo(Exchange exch,FIX::Message *pmsg,tstring* pagentid);
	void DecMsgSeqNum(Exchange exch);
	/* 自动增加 */
	uint32 AddMsgSeqNum(Exchange exch);

private:
	int32 InitExchManager();
	void FreeMsgManager();

private:
	RMMSTraderRouterMsgCacheManagerPara m_para;

	RMMSClOrdIdManager m_idmanager;
	RMMSMsgSeqNumManager m_seqmanager;

	std::map<Exchange,IRMMSExchMsgManager*> m_exchmsg;
};

RMMS_NS_END

#endif