#ifndef __RMMS_EXCHANGE_HELPER_H__
#define __RMMS_EXCHANGE_HELPER_H__

#include "RMMSExchangeAdapterBase.h"

#ifdef _MSC_VER
#pragma warning(push)
#else
#error "仅支持VC,以待其他"
#endif

RMMS_NS_BEGIN

// 暂时去掉该部分的接口,将来可以增加新的接口函数.目前暂时不用该函数.
class RMMS_EXCHANGE_ADAPTER_API RMMSExchangeHelper
{
	static bool SetCurrentVer(RMMSFixVer ver);
	static RMMSFixVer GetCurrentVer(void);
};

RMMS_NS_END

#ifdef _MSC_VER
#pragma warning(pop)
#else
#error "仅支持VC,以待修改"
#endif

#endif