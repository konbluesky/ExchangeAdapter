#ifndef __RMMS_MSG_SEQ_NUM_MANAGER_H__
#define __RMMS_MSG_SEQ_NUM_MANAGER_H__

/**
 * MsgSeqNum不能为0.这样避开特定的Login情况,后面看看如何做到
 * 
 * 至少要做到3点情况:
 * 1.增加Login的控制.
 * 2.增加断链的通知.
 * 3.增加号码的控制情况.这里主要是如何控制这个msgseqnum的情况.
 * 4.基本可以按照北京的情况,将这种方式复制过来.
 */
#include "RMMSExchangeAdapterBase.h"

RMMS_NS_BEGIN

class RMMSMsgSeqNumManager {
public:
	RMMSMsgSeqNumManager();
	~RMMSMsgSeqNumManager();

public:
	int32 AddExch(Exchange exch,const tstring &cfgfilename);
	void DecExch(Exchange exch);

	/* 返回seqnum */
	uint32 AddMsgSeqNum(Exchange exch);
	void DecMsgSeqNum(Exchange exch);

private:
	int32 InitMsgSeqNum(Exchange exch);
	void UninitMsgSeqNum(Exchange exch);
	int32 WriteMsgSeqNum(Exchange exch);
	/* 返回seqnum */
	uint32 ReadMsgSeqNum(Exchange exch);
	void InitExchInfo(Exchange exch);

#define MAX_MSG_SEQ_LEN		10
	struct MsgSeqNumExchInfo {
		bool ballowed;
		tstring cfgfilename;
		FILE *cfgfilehandle;
		uint32 msgseqnum;
		tchar buff[MAX_MSG_SEQ_LEN+1];
	};
	MsgSeqNumExchInfo m_exchs[RMMS_EXCH_SUM];
};

RMMS_NS_END

#endif