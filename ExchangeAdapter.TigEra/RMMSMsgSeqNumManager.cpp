#include "ExchangeAdapterConfig.h"
#include "RMMSMsgSeqNumManager.h"
#include "DebugUtils.h"
#include "UFile.h"
#include "ExchangeAdapterCompiler.h"

RMMS_NS_BEGIN

#define DEFAULT_FILTER_NORET(exch)			\
do{										\
	switch(exch){						\
	case RMMS_CN_ZJ:					\
	case RMMS_CN_SH:					\
		break;							\
	default:							\
		return;							\
		break;							\
	}									\
}while(0)

#define DEFAULT_FILTER_RET(exch)			\
do{										\
	switch(exch){						\
	case RMMS_CN_ZJ:					\
	case RMMS_CN_SH:					\
		break;							\
	default:							\
		return 0;						\
		break;							\
	}									\
}while(0)

RMMSMsgSeqNumManager::RMMSMsgSeqNumManager()
{
	std::memset(m_exchs,0,RMMS_EXCH_SUM*sizeof(MsgSeqNumExchInfo));
}

RMMSMsgSeqNumManager::~RMMSMsgSeqNumManager(){}

void RMMSMsgSeqNumManager::UninitMsgSeqNum(Exchange exch)
{
	if(true != m_exchs[exch].ballowed)
		return;
	std::fclose(m_exchs[exch].cfgfilehandle);
	m_exchs[exch].ballowed = false;
}

int32 RMMSMsgSeqNumManager::InitMsgSeqNum(Exchange exch)
{
	if(!LIB_NS::UFile::IsFileExist(m_exchs[exch].cfgfilename.c_str())){
		m_exchs[exch].cfgfilehandle = std::fopen(
			m_exchs[exch].cfgfilename.c_str(),"w+");
		if(0 > WriteMsgSeqNum(exch))
			return -1;
	}else{
		m_exchs[exch].cfgfilehandle = 
			std::fopen(m_exchs[exch].cfgfilename.c_str(),"r+");
	}

	if(NULL == m_exchs[exch].cfgfilehandle)
		return -1;

	std::fseek(m_exchs[exch].cfgfilehandle,0,SEEK_END);
	if(MAX_MSG_SEQ_LEN != std::ftell(m_exchs[exch].cfgfilehandle)){
		std::fclose(m_exchs[exch].cfgfilehandle);
		m_exchs[exch].cfgfilehandle = NULL;
		return -1;
	}

	ReadMsgSeqNum(exch);
	m_exchs[exch].ballowed = true;

	return 0;
}

int32 RMMSMsgSeqNumManager::AddExch(Exchange exch,const tstring &cfgfilename)
{
	DEFAULT_FILTER_RET(exch);
	m_exchs[exch].cfgfilename = cfgfilename;
	if(0 >InitMsgSeqNum(exch))
		return -1;
	return 0;
}

void RMMSMsgSeqNumManager::DecExch(Exchange exch)
{
	DEFAULT_FILTER_NORET(exch);
	DBG_ASSERT(true == m_exchs[exch].ballowed);
	UninitMsgSeqNum(exch);
}

uint32 RMMSMsgSeqNumManager::AddMsgSeqNum(Exchange exch)
{
	DEFAULT_FILTER_RET(exch);
	uint32 iret = m_exchs[exch].msgseqnum;

	m_exchs[exch].msgseqnum++;
	if(0 > WriteMsgSeqNum(exch)){
		m_exchs[exch].msgseqnum--;
		DBG_ASSERT(false);
	}

	return iret;
}

void RMMSMsgSeqNumManager::DecMsgSeqNum(Exchange exch)
{
	DEFAULT_FILTER_NORET(exch);
	m_exchs[exch].msgseqnum--;
	if(0 > WriteMsgSeqNum(exch)){
		m_exchs[exch].msgseqnum++;
		DBG_ASSERT(false);
	}
}

uint32 RMMSMsgSeqNumManager::ReadMsgSeqNum(Exchange exch)
{
	/* �ַ�������,������� */
	if(0 != std::fseek(m_exchs[exch].cfgfilehandle,0,SEEK_SET)){
		DBG_ASSERT(false);
	}
	if(MAX_MSG_SEQ_LEN != std::fread(m_exchs[exch].buff,1,
		MAX_MSG_SEQ_LEN,m_exchs[exch].cfgfilehandle)){
		DBG_ASSERT(false);
	}
	std::sscanf(m_exchs[exch].buff,"%010u",&(m_exchs[exch].msgseqnum));
	return m_exchs[exch].msgseqnum;
}

int32 RMMSMsgSeqNumManager::WriteMsgSeqNum(Exchange exch)
{
	DBG_ASSERT(NULL != m_exchs[exch].cfgfilehandle);
	if(0 != std::fseek(m_exchs[exch].cfgfilehandle,0,SEEK_SET))
		return -1;

	if(0 == m_exchs[exch].msgseqnum)
		(m_exchs[exch].msgseqnum)++;
	std::sprintf(m_exchs[exch].buff,"%010u",m_exchs[exch].msgseqnum);
	if(MAX_MSG_SEQ_LEN != std::fwrite(m_exchs[exch].buff,1,
		MAX_MSG_SEQ_LEN,m_exchs[exch].cfgfilehandle)){
		(m_exchs[exch].msgseqnum)--;
		return -1;
	}
		
	return 0;
}

RMMS_NS_END