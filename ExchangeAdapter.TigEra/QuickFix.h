#ifndef __EXCHANGE_ADAPTER_QUICK_FIX_H__
#define __EXCHANGE_ADAPTER_QUICK_FIX_H__

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4275)
#pragma warning(disable:4100)
#pragma warning(disable:4512)
#pragma warning(disable:4251)
#pragma warning(disable:4125)
#pragma warning(disable:4290)
#else
#error "仅支持VC,以待其他"
#endif

#include "quickfix/Message.h"
#include "quickfix/Application.h"
#include "quickfix/FileStore.h"
#include "quickfix/SocketAcceptor.h"
#include "quickfix/FileLog.h"
#include "quickfix/Session.h"
#include "quickfix/MessageCracker.h"

#ifdef _MSC_VER
#pragma warning(pop)
#else
#error "仅支持VC,以待修改"
#endif

#endif