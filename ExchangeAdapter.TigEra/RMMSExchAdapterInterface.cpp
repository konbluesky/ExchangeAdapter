#include "RMMSExchAdapterCommon.h"
#include "RMMSMarketRouter.h"
#include "RMMSTraderRouter.h"

RMMS_NS_BEGIN

RMMSExchMarketAdapter::RMMSExchMarketAdapter(Exchange exch,IFixMarketProvider* provider)
		:m_response(NULL),m_priv(NULL),m_exch(exch),m_pprovider(provider)
{
	m_pprovider->SetMarketProcessor(this);
}

RMMSExchMarketAdapter::~RMMSExchMarketAdapter(){}

void RMMSExchMarketAdapter::RegisterResponse(RMMSMarketRouter *response,void* priv) 
{
	DBG_ASSERT(NULL != response);
	m_response = response;
	m_priv = priv;
}

IFixMarketProvider* RMMSExchMarketAdapter::GetProvider(void){return m_pprovider;}

void RMMSExchMarketAdapter::BroadcastFix(const FIX::Message &msg)
{
	m_response->MsgResponse(m_exch,msg);
}

void RMMSExchMarketAdapter::OnNetConnected()
{
	m_response->AdmEvent_NetConnect(m_exch);
}

void RMMSExchMarketAdapter::OnNetDisconnected()
{
	m_response->AdmEvent_NetDisConnect(m_exch);
}

void RMMSExchMarketAdapter::OnLogin()
{
	m_response->AdmEvent_NetLogon(m_exch);
}

void RMMSExchMarketAdapter::OnLogout()
{
	m_response->AdmEvent_NetLogout(m_exch);
}

int32 RMMSExchMarketAdapter::Login(){return -1;}
void RMMSExchMarketAdapter::Logout(){}

/*****************************************************************************/
/* RMMS Trader Adapter 暂时保留这部分,待业务成熟后在优化
/*****************************************************************************/
RMMSExchTraderAdapter::RMMSExchTraderAdapter(Exchange exch,IFixTraderProvider* provider)
	:m_response(NULL),m_priv(NULL),m_exch(exch),m_pprovider(provider)
{
	m_pprovider->SetTraderProcessor(this);
}

RMMSExchTraderAdapter::~RMMSExchTraderAdapter()
{
	m_response = NULL;
	m_priv = NULL;
}

void RMMSExchTraderAdapter::RegisterResponse(RMMSTraderRouter *response,void* priv) 
{
	DBG_ASSERT(NULL != response);
	m_response = response;
	m_priv = priv;
}

int32 RMMSExchTraderAdapter::Request(FIX::Message &msg) const
{
	m_pprovider->RequestFix(msg);
	return 0;
}

IFixTraderProvider* RMMSExchTraderAdapter::GetProvider(void){return m_pprovider;}

void RMMSExchTraderAdapter::ResponseFix(const FIX::Message &msg)
{
	m_response->MsgResponse(m_exch,msg);
}

void RMMSExchTraderAdapter::OnNetConnected()
{
	m_response->AdmEvent_NetConnect(m_exch);
}

void RMMSExchTraderAdapter::OnNetDisconnected()
{
	m_response->AdmEvent_NetConnect(m_exch);
}

void RMMSExchTraderAdapter::OnLogin()
{
	m_response->AdmEvent_NetLogon(m_exch);
}

void RMMSExchTraderAdapter::OnLogout()
{
	m_response->AdmEvent_NetLogout(m_exch);
}	

RMMS_NS_END