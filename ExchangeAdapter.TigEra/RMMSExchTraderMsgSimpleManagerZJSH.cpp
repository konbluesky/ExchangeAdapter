#include "ExchangeAdapterConfig.h"
#include "RMMSExchTraderMsgSimpleManager.h"
#include "DebugUtils.h"
#include "sqlite3.h"
#include "ExchangeAdapterCompiler.h"

RMMS_NS_BEGIN

/*****************************************************************************/
/* 国内数据的管理
/*****************************************************************************/
static const char* g_chinatableformat_order = 
	"(senderid varchar(21),clordid varchar(21),newclordid varchar(21),"
	"price double,ordercum double,orderqty double)";

RMMSExchMsgSimpleManagerZJSH::RMMSExchMsgSimpleManagerZJSH(
	Exchange exch):IRMMSExchMsgSimpleManager(exch),m_pdb(NULL),
	m_pbufmanager(NULL)
{
}

RMMSExchMsgSimpleManagerZJSH::~RMMSExchMsgSimpleManagerZJSH(){}

bool RMMSExchMsgSimpleManagerZJSH::IsHaveMsgInfo(
	const tchar *agentid,const tchar *ppriclordid,tstring *ppublocalid)
{
	DBG_ASSERT(NULL != agentid);
	DBG_ASSERT(NULL != ppriclordid);
	DBG_ASSERT(NULL != ppublocalid);

	sqlite3_stmt *pstmt = 0;
	const char *error = 0;
	tstring selectsql = "select newclordid from neworder where senderid ='";
	
	selectsql += agentid;
	selectsql += "' and clordid = '";
	selectsql += ppriclordid;
	selectsql +=  "'";
	if(SQLITE_OK != sqlite3_prepare(m_pdb->m_pdb,selectsql.c_str(),
		selectsql.length(),&pstmt, &error)){
		DBG_TRACE(("%d:%s",m_exch,error));
		DBG_ASSERT(NULL);
	}
	if(SQLITE_ROW != sqlite3_step(pstmt))
		return 0;

	*ppublocalid = (const int8*)sqlite3_column_text(pstmt,0);

	return 1;
}

int32 RMMSExchMsgSimpleManagerZJSH::LocalSaveMsgInfo(
	const tchar *pagentid,const tchar *ppriclordid,const tchar *ppublocalid,
	FIX::Message *pmsg,RMMSMsgSeqNumManager *pseqnummanager)
{
	DBG_ASSERT(NULL != pseqnummanager);
	int32 errorcode = 0;
	FIX::MsgType type;
	const FIX::Header &header = pmsg->getHeader();
	header.getField(type);
	tstring strtype = type.getValue();

	/* 暂时认为不会出错 */;
	uint32 msgseqnum = pseqnummanager->AddMsgSeqNum(m_exch);
	pmsg->setField(FIX::MsgSeqNum(msgseqnum));

	/* 消息处理 */
	if(FIX::MsgType_NewOrderSingle == strtype){
		errorcode = LocalSave_NewOrderSingle(pagentid,ppriclordid,
			ppublocalid,pmsg);
	}
	else if(FIX::MsgType_OrderCancelRequest == strtype){
		errorcode = LocalSave_OrderCancelRequest(pagentid,ppriclordid,
			ppublocalid,pmsg);
	}
	else
		errorcode = -1;
	
	return errorcode;
}

int32 RMMSExchMsgSimpleManagerZJSH::LocalGetMsgInfo(
	FIX::Message *pmsg,tstring *pagentid)
{
	DBG_ASSERT(NULL != pmsg);
	DBG_ASSERT(NULL != pagentid);

	int32 errorcode = 0;
	FIX::MsgType type;
	const FIX::Header &header = pmsg->getHeader();
	header.getField(type);
	tstring strtype = type.getValue();

	/* 目前程序正常运行的情况下,一定能够找到 */
	if(FIX::MsgType_ExecutionReport == strtype)
		errorcode = LocalGet_ExecutionReport(pmsg,pagentid);
	else if(FIX::MsgType_BusinessMessageReject == strtype)
		errorcode = LocalGet_BusinessReject(pmsg,pagentid);
	else if(FIX::MsgType_OrderCancelReject == strtype)
		errorcode = LocalGet_OrderCancelReject(pmsg,pagentid);
	else
		errorcode = -1;
	return errorcode;
}

int32 RMMSExchMsgSimpleManagerZJSH::LocalSave_NewOrderSingle(
	const tchar *pagentid,const tchar *ppriclordid,const tchar *ppublocalid,
	FIX::Message *pmsg)
{
	DBG_ASSERT(NULL != pagentid);
	DBG_ASSERT(NULL != ppriclordid);
	DBG_ASSERT(NULL != ppublocalid);
	DBG_ASSERT(NULL != pmsg);

	FIX::Price price(0);
	if(pmsg->isSetField(FIX::FIELD::Price))
		pmsg->getField(price);

	FIX::OrderQty orderqty(0);
	if(!pmsg->isSetField(FIX::FIELD::OrderQty))
		return -1;
	pmsg->getField(orderqty);

	/* 保存数据记录 */
	RMMSBufferManager::DataUnit data;
	data.agentid = pagentid;
	data.priclordid = ppriclordid;
	data.pubclordid = ppublocalid;

	tstring sqlinsert = "Insert Into neworder Values('" + data.agentid + 
		"','" + data.priclordid + "','" + data.pubclordid + "'," +
		price.getString() + ",0," + orderqty.getString() + ")";
	if(!m_pdb->ExecuteSQL(sqlinsert.c_str()))		
		return -1;
		
	m_pbufmanager->Insert(ppublocalid,data);
	pmsg->setField(FIX::ClOrdID(data.pubclordid));

	return 0;
}

int32 RMMSExchMsgSimpleManagerZJSH::LocalSave_OrderCancelRequest(
	const tchar *pagentid,const tchar *ppriclordid,const tchar *ppublocalid,
	FIX::Message *pmsg)
{
	DBG_ASSERT(NULL != pagentid);
	DBG_ASSERT(NULL != ppriclordid);
	DBG_ASSERT(NULL != ppublocalid);
	DBG_ASSERT(NULL != pmsg);

	/* 保存数据记录 */
	RMMSBufferManager::DataUnit data;
	data.agentid = pagentid;
	data.priclordid = ppriclordid;
	data.pubclordid = ppublocalid;

	tstring strinsert = "Insert Into neworder Values('" + data.agentid + 
		"','" + data.priclordid + "','" + ppublocalid + "',0,0,0)";
	if(!m_pdb->ExecuteSQL(strinsert.c_str()))
		return -1;

	m_pbufmanager->Insert(ppublocalid,data);
	pmsg->setField(FIX::ClOrdID(ppublocalid));
	
	return 0;
}


int32 RMMSExchMsgSimpleManagerZJSH::ExecutionReport_Fill(
	FIX::Message *pmsg,double ordercum,double orderqty,
	const char *ppubordid)
{
	DBG_ASSERT(NULL != ppubordid);
	DBG_ASSERT(NULL != pmsg);

	/* 业务计算 */
	FIX::LastShares lastshares;
	pmsg->getField(lastshares);
	double dlastshares = lastshares.getValue();
	ordercum = ordercum + dlastshares;
	double leavesqty = orderqty - ordercum;

	if(leavesqty == 0){
		pmsg->setField(FIX::OrdStatus(FIX::OrdStatus_FILLED));
		pmsg->setField(FIX::ExecType(FIX::ExecType_FILL));
	}else if(leavesqty < orderqty){
		pmsg->setField(FIX::OrdStatus(FIX::OrdStatus_PARTIALLY_FILLED));
		pmsg->setField(FIX::ExecType(FIX::ExecType_PARTIAL_FILL));
	}

	FIX::CumQty fixcum(ordercum);
	pmsg->setField(fixcum);
	pmsg->setField(FIX::OrderQty(orderqty));
	pmsg->setField(FIX::LeavesQty(leavesqty));

	/* 更新累积成交量 */
	tstring strinsert = "Update neworder set ordercum = ";
	strinsert += fixcum.getString();
	strinsert += " where newclordid = '";
	strinsert += ppubordid;
	strinsert += "'";
	if(!m_pdb->ExecuteSQL(strinsert.c_str()))
		return -1;

	return 0;
}

int32 RMMSExchMsgSimpleManagerZJSH::LocalGet_ExecutionReport(
	FIX::Message *pmsg,tstring *pagentid)
{
	DBG_ASSERT(NULL != pagentid);
	DBG_ASSERT(NULL != pmsg);
	DBG_ASSERT(pmsg->isSetField(FIX::FIELD::ClOrdID));

	tstring pubclordid = pmsg->getField(FIX::FIELD::ClOrdID);
	RMMSBufferManager::DataUnit datastruct;
	bool bFill = false;

	/**
	 * 成交单一律从数据库中读取,防止出错.
	 */
	if(!pmsg->isSetField(FIX::FIELD::OrdStatus))
		bFill = true;
	else {
		FIX::OrdStatus ordstatus;
		pmsg->getField(ordstatus);
		if(FIX::OrdStatus_NEW != ordstatus && 
			FIX::OrdStatus_CANCELED != ordstatus)
			bFill = true;
	}

	if(false == bFill && 0 <= m_pbufmanager->Get(pubclordid.c_str(),
		&datastruct)){
		*pagentid = datastruct.agentid;
		pmsg->setField(FIX::ClOrdID(datastruct.priclordid));
		return 0;
	}
	
	/* 如果是成交单或者没有找到,那么读取数据库 */
	sqlite3_stmt *pstmt = 0;
	const char *error = 0;
	tstring selectsql = "select senderid,clordid,ordercum,orderqty from "
		"neworder where newclordid ='" + pubclordid + "'";

	if(SQLITE_OK != sqlite3_prepare(m_pdb->m_pdb,selectsql.c_str(),
		selectsql.length(),&pstmt, &error))
		return -1;
	if(SQLITE_ROW != sqlite3_step(pstmt))
		return -1;

	const uint8 *senderid = sqlite3_column_text(pstmt,0);
	const uint8 *priclordid = sqlite3_column_text(pstmt,1);
	double ordcum = sqlite3_column_double(pstmt,2);
	double orderqty = sqlite3_column_double(pstmt,3);
	
	/* 对成交单进行特殊处理 */
	if(true == bFill)
		ExecutionReport_Fill(pmsg,ordcum,orderqty,pubclordid.c_str());
	
	*pagentid = (const int8 *)senderid;
	pmsg->setField(FIX::ClOrdID((const int8*)priclordid));

	return 0;
}

int32 RMMSExchMsgSimpleManagerZJSH::LocalGet_BusinessReject(
	FIX::Message *pmsg,tstring *pagentid)
{
	DBG_ASSERT(pmsg->isSetField(FIX::FIELD::ClOrdID));
	tstring pubclordid = pmsg->getField(FIX::FIELD::ClOrdID);
	RMMSBufferManager::DataUnit datastruct;
	
	if(0 <= m_pbufmanager->Get(pubclordid.c_str(),&datastruct)){
		*pagentid = datastruct.agentid;
		pmsg->setField(FIX::ClOrdID(datastruct.priclordid));
		return 0;
	}
	
	/* 如果没有找到,那么读取数据库 */
	sqlite3_stmt *pstmt = 0;
	const char *error = 0;
	tstring selectsql = "select senderid,clordid from "
		"neworder where newclordid ='" + pubclordid + "'";

	if(SQLITE_OK != sqlite3_prepare(m_pdb->m_pdb,selectsql.c_str(),
		selectsql.length(),&pstmt, &error)){
		DBG_TRACE(("%d:%s",m_exch,error));
		DBG_ASSERT(0);
	}

	/**
	 * 目前认为一定能找到,如果找不到,那么一定是代码书写错误
	 */
	if(SQLITE_ROW != sqlite3_step(pstmt)){
		DBG_ASSERT(0);
		return -1;
	}

	*pagentid = (const int8 *)sqlite3_column_text(pstmt,0);
	pmsg->setField(FIX::ClOrdID((const int8*)sqlite3_column_text(pstmt,1)));

	return 0;
}

int32 RMMSExchMsgSimpleManagerZJSH::LocalGet_OrderCancelReject(
	FIX::Message *pmsg,tstring *pagentid)
{
	DBG_ASSERT(pmsg->isSetField(FIX::FIELD::ClOrdID));
	tstring pubclordid = pmsg->getField(FIX::FIELD::ClOrdID);
	RMMSBufferManager::DataUnit datastruct;

	if(0 <= m_pbufmanager->Get(pubclordid.c_str(),&datastruct)){
		*pagentid = datastruct.agentid;
		pmsg->setField(FIX::ClOrdID(datastruct.priclordid));
		return 0;
	}

	/* 如果没有找到,那么读取数据库 */
	sqlite3_stmt *pstmt = 0;
	const char *error = 0;
	tstring selectsql = "select senderid,clordid from "
		"neworder where newclordid ='" + pubclordid + "'";

	if(SQLITE_OK != sqlite3_prepare(m_pdb->m_pdb,selectsql.c_str(),selectsql.length(),
		&pstmt, &error)){
			DBG_TRACE(("%d:%s",m_exch,error));
			DBG_ASSERT(0);
	}
	/* 目前认为一定能找到,如果找不到,那么一定是代码书写错误 */
	if(SQLITE_ROW != sqlite3_step(pstmt)){
		DBG_ASSERT(0);
		return -1;
	}

	*pagentid = (const int8 *)sqlite3_column_text(pstmt,0);
	pmsg->setField(FIX::ClOrdID((const int8*)sqlite3_column_text(pstmt,1)));

	return 0;
}

int32 RMMSExchMsgSimpleManagerZJSH::Initialize(tstring databasename)
{
	if(m_bstarted)
		return 1;
	if(0 >= databasename.length())
		return -1;
	
	m_pbufmanager = new RMMSBufferManager;
	if(NULL == m_pbufmanager)
		return -1;
	m_pdb = new RMMSDatabase;
	if(NULL == m_pdb){
		delete m_pbufmanager;
		m_pbufmanager = NULL;
		return -1;
	}
	if(0 > m_pdb->Init(databasename.c_str(),g_chinatableformat_order)){
		delete m_pbufmanager;
		m_pbufmanager = NULL;
		delete m_pdb;
		m_pdb = NULL;
		return -1;
	}

	m_bstarted = true;

	return 0;
}

void RMMSExchMsgSimpleManagerZJSH::Uninitialize()
{
	if(!m_bstarted)
		return;

	m_pdb->Uninit();
	delete m_pbufmanager;
	m_pbufmanager = NULL;
	delete m_pdb;
	m_pdb = NULL;

	m_bstarted = false;
}

RMMS_NS_END