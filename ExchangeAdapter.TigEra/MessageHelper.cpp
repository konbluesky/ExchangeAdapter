#include "FixLib.h"
#include "MessageHelper.h"


bool MessageHelper::IsHeartBeatMessage(const FIX::Message& message)
{
	bool ret = message.getHeader().getField(35) == "0";
	return ret;
}