#include "ExchangeAdapterConfig.h"
#include "RMMSExchTraderMsgSimpleManager.h"
#include "DebugUtils.h"
#include "sqlite3.h"
#include "ExchangeAdapterCompiler.h"

RMMS_NS_BEGIN

/*****************************************************************************/
/* FIX
/*****************************************************************************/
static const char* g_fixtableformat_order = 
	"(senderid varchar(21),clordid varchar(21),newclordid varchar(21))";

RMMSExchMsgSimpleManagerFix::RMMSExchMsgSimpleManagerFix(
	Exchange exch): IRMMSExchMsgSimpleManager(exch),
	m_pdb(NULL),m_pbufmanager(NULL)
{
}

RMMSExchMsgSimpleManagerFix::~RMMSExchMsgSimpleManagerFix(){}

bool RMMSExchMsgSimpleManagerFix::IsHaveMsgInfo(
	const tchar *agentid,const tchar *ppriclordid,tstring *ppublocalid)
{
	DBG_ASSERT(NULL != agentid);
	DBG_ASSERT(NULL != ppriclordid);
	DBG_ASSERT(NULL != ppublocalid);

	sqlite3_stmt *pstmt = 0;
	const char *error = 0;
	tstring selectsql = "select newclordid from neworder where senderid ='";
	
	selectsql += agentid;
	selectsql += "' and clordid = '";
	selectsql += ppriclordid;
	selectsql +=  "'";
	
	if(SQLITE_OK != sqlite3_prepare(m_pdb->m_pdb,selectsql.c_str(),
		selectsql.length(),&pstmt, &error)){
		DBG_TRACE(("%d:%s",m_exch,error));
		DBG_ASSERT(NULL);
	}
	if(SQLITE_ROW != sqlite3_step(pstmt))
		return 0;

	*ppublocalid = (const int8*)sqlite3_column_text(pstmt,0);

	return 1;
}

int32 RMMSExchMsgSimpleManagerFix::LocalSaveMsgInfo(
	const tchar *pagentid,const tchar *ppriclordid,const tchar *ppublocalid,
	FIX::Message *pmsg,RMMSMsgSeqNumManager* /*pseqnummanager*/)
{
	int32 errorcode = 0;
	FIX::MsgType type;
	FIX::Header header = pmsg->getHeader();
	header.getField(type);
	tstring strtype = type.getValue();

	/* 暂时认为CME任何消息都支持 */
	if(FIX::MsgType_OrderCancelRequest == strtype || 
		FIX::MsgType_OrderCancelReplaceRequest == strtype)
		errorcode = LocalSave_Cancel(pagentid,ppriclordid,ppublocalid,pmsg);
	else
		errorcode = LocalSave(pagentid,ppriclordid,ppublocalid,pmsg);

	return errorcode;
}

int32 RMMSExchMsgSimpleManagerFix::LocalGetMsgInfo(
	FIX::Message *pmsg,tstring *pagentid)
{
	DBG_ASSERT(NULL != pmsg);
	DBG_ASSERT(NULL != pagentid);

	FIX::MsgType type;
	FIX::Header header = pmsg->getHeader();
	header.getField(type);
	tstring strtype = type.getValue();
	if (FIX::MsgType_ExecutionReport == strtype)
		return LocalGet_ExeReport(pmsg,pagentid);
	else 
		return LocalGet(pmsg,pagentid);
}

int32 RMMSExchMsgSimpleManagerFix::LocalSave(const tchar *pagentid,
	const tchar *ppriclordid,const tchar *ppublocalid,FIX::Message *pmsg)
{
	DBG_ASSERT(NULL != pagentid);
	DBG_ASSERT(NULL != ppriclordid);
	DBG_ASSERT(NULL != ppublocalid);
	DBG_ASSERT(NULL != pmsg);

	/* 保存数据记录 */
	RMMSBufferManager::DataUnit data;
	data.agentid = pagentid;
	data.priclordid = ppriclordid;
	data.pubclordid = ppublocalid;

	tstring sqlinsert = "Insert Into neworder Values('" + data.agentid + 
		"','" + data.priclordid + "','" + data.pubclordid + "')";
	if(!m_pdb->ExecuteSQL(sqlinsert.c_str()))		
		return -1;
		
	m_pbufmanager->Insert(ppublocalid,data);
	pmsg->setField(FIX::ClOrdID(data.pubclordid));

	return 0;
}

int32 RMMSExchMsgSimpleManagerFix::LocalSave_Cancel(const tchar *pagentid,
	const tchar *ppriclordid,const tchar *ppublocalid,FIX::Message *pmsg)
{
	DBG_ASSERT(NULL != pagentid);
	DBG_ASSERT(NULL != ppriclordid);
	DBG_ASSERT(NULL != ppublocalid);
	DBG_ASSERT(NULL != pmsg);

	/* 保存数据记录 */
	if(!pmsg->isSetField(FIX::FIELD::OrigClOrdID))
		return -1;
	tstring priorigclordid = pmsg->getField(FIX::FIELD::OrigClOrdID);
	tstring puborigclordid;
	if (!IsHaveMsgInfo(pagentid,priorigclordid.c_str(),&puborigclordid))
		return -1;
	
	RMMSBufferManager::DataUnit data;
	data.agentid = pagentid;
	data.priclordid = ppriclordid;
	data.pubclordid = ppublocalid;
	data.priorigclordid = priorigclordid;

	tstring strinsert = "Insert Into neworder Values('" + data.agentid + 
		"','" + data.priclordid + "','" + ppublocalid + "')";
	if(!m_pdb->ExecuteSQL(strinsert.c_str()))
		return -1;

	m_pbufmanager->Insert(ppublocalid,data);
	pmsg->setField(FIX::ClOrdID(ppublocalid));
	pmsg->setField(FIX::OrigClOrdID(puborigclordid));
	
	return 0;
}

int32 RMMSExchMsgSimpleManagerFix::LocalGet_ExeReport(FIX::Message *pmsg,
	tstring *pagentid)
{
	DBG_ASSERT(NULL != pagentid);
	DBG_ASSERT(NULL != pmsg);
	FIX::OrdStatus ordstatus;
	pmsg->getField(ordstatus);
	tchar tcstatus = ordstatus.getValue();

	if(FIX::OrdStatus_REPLACED != tcstatus &&
		FIX::OrdStatus_CANCELED != tcstatus)
		return LocalGet(pmsg,pagentid);
	
	tstring pubclordid = pmsg->getField(FIX::FIELD::ClOrdID);
	RMMSBufferManager::DataUnit datastruct;
	if(0 <= m_pbufmanager->Get(pubclordid.c_str(),&datastruct)){
		*pagentid = datastruct.agentid;
		pmsg->setField(FIX::ClOrdID(datastruct.priclordid));
		pmsg->setField(FIX::OrigClOrdID(datastruct.priorigclordid));
		return 0;
	}

	/* 如果没有找到,那么读取数据库 */
	sqlite3_stmt *pstmt = 0;
	const char *error = 0;
	tstring selectsql = "select senderid,clordid from "
		"neworder where newclordid ='" + pubclordid + "'";

	if(SQLITE_OK != sqlite3_prepare(m_pdb->m_pdb,selectsql.c_str(),
		selectsql.length(),&pstmt, &error)){
			DBG_TRACE(("!!!!非常严重的错误\n%s-%d\n",__FILE__,__LINE__));
			DBG_TRACE(("%d:%s",m_exch,error));
	}
	if(SQLITE_ROW != sqlite3_step(pstmt)){
		DBG_TRACE(("!!!!非常严重的错误\n%s-%d\n",__FILE__,__LINE__));
		return -1;
	}

	tstring puborigclordid = pmsg->getField(FIX::FIELD::OrigClOrdID);
	*pagentid = (const int8 *)sqlite3_column_text(pstmt,0);
	pmsg->setField(FIX::ClOrdID((const int8*)sqlite3_column_text(pstmt,1)));

	/* 寻找org对应 */
	selectsql = "select senderid,clordid from "
		"neworder where newclordid ='" + puborigclordid + "'";
	if(SQLITE_OK != sqlite3_prepare(m_pdb->m_pdb,selectsql.c_str(),
		selectsql.length(),&pstmt, &error)){
			DBG_TRACE(("%d:%s",m_exch,error));
			DBG_ASSERT(0);
	}
	if(SQLITE_ROW != sqlite3_step(pstmt)){
		DBG_ASSERT(0);
		return -1;
	}

	DBG_ASSERT(*pagentid == (const int8 *)sqlite3_column_text(pstmt,0));
	pmsg->setField(FIX::OrigClOrdID((const int8*)sqlite3_column_text(pstmt,1)));

	return 0;
}

int32 RMMSExchMsgSimpleManagerFix::LocalGet(FIX::Message *pmsg,
	tstring *pagentid)
{
	DBG_ASSERT(NULL != pagentid);
	DBG_ASSERT(NULL != pmsg);
		
	tstring pubclordid = pmsg->getField(FIX::FIELD::ClOrdID);
	RMMSBufferManager::DataUnit datastruct;
	if(0 <= m_pbufmanager->Get(pubclordid.c_str(),&datastruct)){
		*pagentid = datastruct.agentid;
		pmsg->setField(FIX::ClOrdID(datastruct.priclordid));
		return 0;
	}

	/* 如果没有找到,那么读取数据库 */
	sqlite3_stmt *pstmt = 0;
	const char *error = 0;
	tstring selectsql = "select senderid,clordid from "
		"neworder where newclordid ='" + pubclordid + "'";

	if(SQLITE_OK != sqlite3_prepare(m_pdb->m_pdb,selectsql.c_str(),
		selectsql.length(),&pstmt, &error)){
		DBG_TRACE(("%d:%s",m_exch,error));
		DBG_TRACE(("!!!!数据库执行失败\n%s-%d\n",__FILE__,__LINE__));
	}
	if(SQLITE_ROW != sqlite3_step(pstmt)){
		DBG_TRACE(("!!!!数据库执行失败\n%s-%d\n",__FILE__,__LINE__));
		return -1;
	}

	tstring puborigclordid = pmsg->getField(FIX::FIELD::OrigClOrdID);
	*pagentid = (const int8 *)sqlite3_column_text(pstmt,0);
	pmsg->setField(FIX::ClOrdID((const int8*)sqlite3_column_text(pstmt,1)));

	return 0;
}

int32 RMMSExchMsgSimpleManagerFix::Initialize(tstring databasename)
{
	if(m_bstarted)
		return 1;

	if(0 >= databasename.length())
		return -1;

	m_pbufmanager = new RMMSBufferManager;
	if(NULL == m_pbufmanager)
		return -1;
	m_pdb = new RMMSDatabase;
	if(NULL == m_pdb){
		delete m_pbufmanager;
		m_pbufmanager = NULL;
		return -1;
	}
	if(0 > m_pdb->Init(databasename.c_str(),g_fixtableformat_order)){
		delete m_pbufmanager;
		m_pbufmanager = NULL;
		delete m_pdb;
		m_pdb = NULL;
		return -1;
	}
	
	m_bstarted = true;
	return 0;
}

void RMMSExchMsgSimpleManagerFix::Uninitialize()
{
	if(!m_bstarted)
		return;

	m_pdb->Uninit();
	delete m_pbufmanager;
	m_pbufmanager = NULL;
	delete m_pdb;
	m_pdb = NULL;

	m_bstarted = false;
}

RMMS_NS_END