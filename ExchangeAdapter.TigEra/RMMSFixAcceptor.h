#ifndef __RMMS_FIX_ACCEPTOR_H__
#define __RMMS_FIX_ACCEPTOR_H__

#include "RMMSExchangeAdapterBase.h"
#include "RMMSDebug.h"
#include "autoptr.h"
#include "QuickFix.h"
#include <list>

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4275)
#pragma warning(disable:4251)
#else
#error "仅支持VC,以待其他"
#endif

RMMS_NS_BEGIN

/**
 * FIX的Session Agent
 */
class RMMS_EXCHANGE_ADAPTER_API IRMMSFixAcceptorAgent {
public:
	IRMMSFixAcceptorAgent(FIX::SessionID& seID):m_seid(seID){}
	virtual ~IRMMSFixAcceptorAgent(){}

public:
	virtual void OnRecvApp(const FIX::Message& /*msg*/){};
	virtual void OnRecvAdm(const FIX::Message& /*msg*/){};
	virtual void PreSendAdm(FIX::Message &/*msg*/){}
	virtual void PreSendApp(FIX::Message &/*msg*/){}
	virtual void OnLogOn()
	{
		std::cout<<m_seid.getTargetCompID()<<" Login"<<std::endl;
	}
	virtual void OnLogout()
	{
		std::cout<<m_seid.getTargetCompID()<<" Logout"<<std::endl;
	}

public:
	const FIX::SessionID& GetSessionID() const {return m_seid;}
	tstring GetTargetID() const
	{
		return m_seid.getTargetCompID();
	}
protected:
	FIX::SessionID m_seid;
};

struct RMMSFixAcceptorPara {
	tstring cfgname;
};

class RMMS_EXCHANGE_ADAPTER_API RMMSFixAcceptor : public FIX::Application {
public:
	RMMSFixAcceptor();
	virtual ~RMMSFixAcceptor();

public:
	/**
	 * 在初始化完成之后,注册每一路会话的处理Agent
	 * 只能在Init和Start状态之间进行.
	 */
	std::set<FIX::SessionID> InitSession(const tstring &cfgfilename);
	int32 RegisterAgent(const FIX::SessionID &seID,
		IRMMSFixAcceptorAgent *pagent);
	void UnregisterAgent(const FIX::SessionID &seID);

	int32 Init();
	void Uninit();

	/**
	 * 在启动前,必须先注册完全各路会话的处理Agent,否则将启动失败
	 */
	int32 Start();
	void Stop(bool force = true);

	/**
	 * 公共消息发送函数,注意处理多线程互斥,该函数不负责处理
	 */
	static int32 SendMsg(FIX::SessionID &seid,FIX::Message &msg);

	/**
	 * 下面的函数仅用于内部,不对外开放
	 */
public:
	virtual void onCreate(const FIX::SessionID& seID);
	virtual void onLogon(const FIX::SessionID& seID);
	virtual void onLogout(const FIX::SessionID& seID);
	virtual void fromAdmin(const FIX::Message& msg,const FIX::SessionID& seID);
	virtual void fromApp(const FIX::Message& msg, const FIX::SessionID& seID);
	virtual void toAdmin(FIX::Message& msg, const FIX::SessionID& seID);
	virtual void toApp(FIX::Message& msg, const FIX::SessionID& seID);

protected:
	boost::shared_ptr<FIX::SessionSettings> pSessionSettings;
	boost::shared_ptr<FIX::MessageStoreFactory> pMsgStoreFactory;	
	boost::shared_ptr<FIX::FileLogFactory> pLogFactory;
	boost::shared_ptr<FIX::SocketAcceptor> pAcceptor;

	std::map<FIX::SessionID,IRMMSFixAcceptorAgent*> m_seagent;
};

RMMS_NS_END

#ifdef _MSC_VER
#pragma warning(pop)
#else
#error "仅支持VC,以待修改"
#endif

#endif