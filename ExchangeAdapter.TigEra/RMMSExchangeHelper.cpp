#include "ExchangeAdapterConfig.h"
#include "RMMSExchangeHelper.h"
#include "ExchangeAdapterCompiler.h"

RMMS_NS_BEGIN

/**
 * 暂时先去掉该部分.暂时不考虑该接口.待将来增加该部分的接口.
 */
bool RMMSExchangeHelper::SetCurrentVer(RMMSFixVer /*ver*/)
{
	// return FixHelper::SetCurrentVer(ver);
	return false;
}

RMMSFixVer RMMSExchangeHelper::GetCurrentVer(void)
{
	// return FixHelper::GetCurrentVer();
	return RMMS_VER_UNKNOWN;
}

RMMS_NS_END