代码中对ini文件操作的api，请使用Setting.h
对xml文件操作，请使用tinyxml


程序的框架模型

              Server
           /         \
          /           \ 
 APIFactory         FIXSessionManager
    /\
   /  \ 
 concreateAPIs
 
5.工程在初期建设时，代码变动非常频繁，请经常更新。

4.LibUtils工程定义基本需要使用的额外函数库，采用类的方式定义所有要使用的函数，并会考虑跨平台的要求。LibUtils.Aux定义相对高级的函数库
3.所有工程都是基于ansi编码，都是多线程工程。
2.每个ExchagneAdapter的代码大部分都在自己目录底下，但有些通用的定义业务代码，定义在ExchangeAdapter/include和 ExchangeAdapter/src里
1.工程 ExchangeAdapter.xxx 都是动态库工程，因此对于需要Export出位动态库函数的类，加EXAPI宏



===================================
基本上所有交易所的API都是基于C/C++，但其风格各不相同。

有基于通信协议的(类似CME标准的FIX)
有基于数据结构的(类似郑州的，自己填充一个数据结构，然后调用api发送，其实也类似FIX，但相对而言做了层隔离)
有基于业务模型的(国内中金/大连/上海，有具体的业务模型实现类，类实现了发送请求和请求返回回调函数接口，应该说是更高的一层，让开发人员可以完全不了解通讯协议)


这里，我们要做的工作是：每个交易所API<==>FIX的转换映射，使用的开发语言C++





注：

由于C/C++是个非常精细，又非常灵活的语言，内容也太丰富，所以在我们业务开发过程必须控制其技术使用范围和使用度,
使用的函数库限制在:标准的C库，STL，boost和自定义的一些函数库。




C/C++几十年的发展，并没有一套适用所有开发的标准规范API库，基本上每个公司成熟的产品或库都有自己一套的算法、数据结构、数据类型定义实现
(比如MFC/STL/QT/wxWidgets都有自己的一套string/list/vector等基本数据结构的重新实现)


而C++的编程模型也是五花八门的，面向过程、面向对象、面向泛型;  宏、继承、模板等的语法。数据类型、函数库很混乱，


对于我们的开发使用，需要使用的API库：

1：统一的数据类型定义(主要是int32/int64的定义，由于会考虑到64位、windows/unix平台，因此数据类型都会用宏/typedef重新定义标准)

2:  统一的字符串操作库（使用基于stl的string，并扩展些辅助函数，字符串操作是编程最基础的，但在C＋＋也算最复杂的内容之一）

3：统一的数据结构使用函数库（基于stl的vector，list，map，Ｃ＋＋的数据结构是基于模板编程，模板编程的编译和调试对初学者会比较痛苦的）

3：统一的文件操作库（基于标准的FILE，做一层C++封装(C++标准的文件io类接口并不如何)），目录操作库。

4：统一的debug/log/unittest函数库（自己实现一套简单的,CppUnit工程累赘了些)

5：统一的辅助函数库：如math库（对原本的math函数做C++封装)，time库，系统功能库

6: 统一的内存分配控制函数(对malloc、free、new、delete进行包装，包含了debug功能),并会使用share_ptr简化内存操作

7: 统一异常处理函数

8: 需要一个第三方的xml解析类(实用tinyxml?)

9: 以后可能需要线程库，这个会使用boost里的thread库。


开发的api完全以类的形式封装

[loki/ACE暂时不考虑]



开发过程不要修改原交易所的api文件。

开发规范:

1.不要自己引用预先定义之外的头文件(不要使用未规范定义的函数)




C++ trap & tip

1.堆栈问题
example<1>
char* func()
{
	char* ret="this is buff would invalid when out the scope";
	return ret;
}

C/C++是严格的堆栈语言，一个栈对象出了其作用域，其析构函数将自动被调用，对象也失去作用，这个有好处，也有坏处。

上面看到的是坏处，但在C#里我们不清楚对象什么时候被释放，导致代码行为异常。但在C++里，代码的行为是明确的。



2.对象引用、拷贝问题
example<2>
class A
{
private:
	char* buff
public:
	A(char* data)//这里简化了data的判断
	{
		buff = new char[strlen(data)+1];
		strcpy(buff,data);
	}
	A()
	{}
	
	~A()
	{
		if(buff!=null)
		delete buff;
		buff = null;
	}
}

A x  = new A("data1");
{
	A y;
	y=x;//这时y与x有共同的buff,这样y的析构函数会把buff内容给释放，这时x的析构函数将报错。
}
....//



3.数据类型问题：
int x=0;
if(x)
....



4.


4.函数隐藏问题
void func(...)
{
 printf("func...");
}
template<T> func(int value)
{
	
}