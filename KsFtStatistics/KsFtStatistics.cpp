// KsFtStatistics.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <cassert>
#include <fstream>
#include <sstream>
#include <map>

struct ConstractStatic {
	int32 iseq;
	int32 ilost;
//	string8 iloststring;
};

typedef struct ksftquota_pubdata_item_tag
{
	int	contract_id;				//由交易品种和交割期算出来的id,对应：id号;
	int	upd_serial;				//行情更新序号，对应：序号
	int	upd_date;				//行情日期,格式：YYYYMMDD
	int	pre_upd_date;				//行情上次更新日期(保留)
	int	pre_upd_serial;				//上次更新时的序号(保留)
	char	sys_recv_time[9];		//行情服务器收到行情的时间，行情服务器唯一维护，格式：HH:MM:SS

	char	exchCode[6];		//交易所代码
	char	varity_code[32];		//品种代码
	char	deliv_date[9];		//交割期
	char	chgStatus[2];	//对应：状态
	//1-2bit表示：买入;3-4bit表示：卖出;
	//5-6bit表示：最新;7-8bit不用;
	//00->新行情    01->低于以前的行情
	//11->高于以前的行情    00->与以前相平

	double	openPrice;				//开盘价
	double	lastPrice;				//最新价
	double	highestPrice;				//最高价
	double	lowestPrice;				//最低价
	int	doneVolume;				//成交量
	double	chgPrice;				//涨跌
	double	upperLimitPrice;			//涨停板
	double	lowerLimitPrice;			//跌停板
	double	hisHighestPrice;			//历史最高价
	double	hisLowestPrice;				//历史最低价
	int	openInterest;				//净持仓
	double	preSettlePrice;				//昨日结算
	double	preClosePrice;				//昨日收盘
	double	settlePrice;				//今日结算
	double	turnover;				//成交金额
	//20061208新增,qbin
	int	preOpenInterest;			//昨日持仓量
	double	closePrice;				//今日收盘
	double	preDelta;				//昨虚实度
	double	currDelta;				//今虚实度


	double	bidPrice1;				//买入价1
	int	bidVolume1;				//买入量1
	double	bidPrice2;				//买入价2
	int	bidVolume2;				//买入量2
	double	bidPrice3;				//买入价3
	int	bidVolume3;				//买入量3
	double	bidPrice4;				//买入价4
	int	bidVolume4;				//买入量4
	double	bidPrice5;				//买入价5
	int	bidVolume5;				//买入量5

	double	askPrice1;				//卖出价1
	int	askVolume1;				//卖出量1
	double	askPrice2;				//卖出价2
	int	askVolume2;				//卖出量2
	double	askPrice3;				//卖出价3
	int	askVolume3;				//卖出量3
	double	askPrice4;				//卖出价4
	int	askVolume4;				//卖出量4
	double	askPrice5;				//卖出价5
	int	askVolume5;				//卖出量5

	char cmbtype[2];   //‘0’或NULL:普通行情
	//‘1’：组合套利行情
	//20080514
	int derive_bidlot;   //买推导量   组合买入数量
	int derive_asklot;   //卖推导量   组合卖出数量

}KSFT_QUOTA_PUBDATA_ITEM;

int32 GetBuffer(char8 *tmpbuffer,char8 **pindex)
{
	char8 *pnewindex = tmpbuffer;
	bool bflag = true;
	do {
		*pnewindex = **pindex;
		if(',' == *pnewindex)
			*pnewindex = '\0';
		if('\0' == *pnewindex)
			break;
		pnewindex++;
		(*pindex)++;
	} while (bflag);
	(*pindex)++;
	return 0;
}

void GetVar(char8 **pindex,int32 *idata)
{
	char8 tmpbuffer[64] = {'\0'};
	GetBuffer(tmpbuffer,pindex);
	std::stringstream ss;
	ss<<tmpbuffer;
	ss>>*idata;
}

void GetVar(char8 **pindex,double *idata)
{
	char8 tmpbuffer[64] = {'\0'};
	GetBuffer(tmpbuffer,pindex);
	std::stringstream ss;
	ss<<tmpbuffer;
	ss>>*idata;
}

void GetVar(char8 **pindex,char8 *idata)
{
	string8 str;
	char tmpbuffer[64] = {'\0'};
	GetBuffer(tmpbuffer,pindex);
	std::stringstream ss;
	ss<<tmpbuffer;
	ss>>str;
	std::strcpy(idata,str.c_str());
}

int8 LogFile(KSFT_QUOTA_PUBDATA_ITEM &item,const char8 *pexchcode,
	const char8 *pvaritycode,const char8 *pdelivdate,int iseq)
{
	int8 flag;
	if(0 != std::strcmp(pexchcode,item.exchCode) || 
		0 != std::strcmp(pvaritycode,item.varity_code) ||
		0 != std::strcmp(pdelivdate,item.deliv_date))
		return -1;

	if(1 == item.upd_serial)
		flag = -2;
	else if(iseq >= item.upd_serial)
		return -1;
	else if(iseq != 0 && 1 != item.upd_serial && iseq + 1 != item.upd_serial)
		flag = 0;
	else
		flag = 1;

	return flag;
}

void GetStruct(const char8 *pbuffer,KSFT_QUOTA_PUBDATA_ITEM *pitem)
{
	char8 *pindex = const_cast<char8*>(pbuffer);
	GetVar(&pindex,&(pitem->contract_id));
	GetVar(&pindex,&(pitem->upd_serial));
	GetVar(&pindex,&(pitem->upd_date));
	GetVar(&pindex,&(pitem->pre_upd_date));
	GetVar(&pindex,&(pitem->pre_upd_serial));
	GetVar(&pindex,pitem->sys_recv_time);
	GetVar(&pindex,pitem->exchCode);
	GetVar(&pindex,pitem->varity_code);
	GetVar(&pindex,pitem->deliv_date);
	GetVar(&pindex,pitem->chgStatus);
	GetVar(&pindex,&(pitem->openPrice));
	GetVar(&pindex,&(pitem->lastPrice));
	GetVar(&pindex,&(pitem->highestPrice));
	GetVar(&pindex,&(pitem->lowestPrice));
	GetVar(&pindex,&(pitem->doneVolume));
	GetVar(&pindex,&(pitem->chgPrice));
	GetVar(&pindex,&(pitem->upperLimitPrice));
	GetVar(&pindex,&(pitem->lowerLimitPrice));
	GetVar(&pindex,&(pitem->hisHighestPrice));
	GetVar(&pindex,&(pitem->hisLowestPrice));
	GetVar(&pindex,&(pitem->openInterest));
	GetVar(&pindex,&(pitem->preSettlePrice));
	GetVar(&pindex,&(pitem->preClosePrice));
	GetVar(&pindex,&(pitem->settlePrice));
	GetVar(&pindex,&(pitem->turnover));
	GetVar(&pindex,&(pitem->preOpenInterest));
	GetVar(&pindex,&(pitem->closePrice));
	GetVar(&pindex,&(pitem->preDelta));
	GetVar(&pindex,&(pitem->currDelta));
	GetVar(&pindex,&(pitem->bidPrice1));
	GetVar(&pindex,&(pitem->bidVolume1));
	GetVar(&pindex,&(pitem->askPrice1));
	GetVar(&pindex,&(pitem->askVolume1));
	GetVar(&pindex,pitem->cmbtype);
	GetVar(&pindex,&(pitem->derive_bidlot));
	GetVar(&pindex,&(pitem->derive_asklot));
}

void ReadLogFile(string8 logfilename,string8 formatfilename,int32 lines)
{
	std::FILE *file = std::fopen(logfilename.c_str(),"r");
	if (NULL == file)
		assert(false);
	std::FILE *newfile = std::fopen(formatfilename.c_str(),"a+");
	if (NULL == newfile)
		assert(false);
	
	char8 buffer[512];
	KSFT_QUOTA_PUBDATA_ITEM item;
	std::map<string8,ConstractStatic> constracts;
	ConstractStatic tractstatic;
	std::memset(&tractstatic,0,sizeof(ConstractStatic));
	int32 flag;
	string8 constractuni;

	for(int32 index = 0;index < lines;index++){
		std::fgets(buffer,512,file);
		
		GetStruct(buffer,&item);
		constractuni = item.exchCode;
		constractuni += item.varity_code;
		constractuni += item.deliv_date;

		if(constracts.end() == constracts.find(constractuni))
			constracts[constractuni] = tractstatic;

		flag = LogFile(item,item.exchCode,item.varity_code,
			item.deliv_date,constracts[constractuni].iseq);
		if (0 < flag)
			constracts[constractuni].iseq = item.upd_serial;
		else if (0 == flag){
			constracts[constractuni].ilost += 1;
			constracts[constractuni].iseq = item.upd_serial;
		}
	}

	if(0 < constracts.size()){
		std::map<string8,ConstractStatic>::iterator it = constracts.begin();
		for(;it != constracts.end();it++)
			std::fprintf(newfile,"%s--lost:%d\n",it->first.c_str(),it->second.ilost);
	}

	std::fclose(file);
	std::fclose(newfile);
}

int _tmain(int argc, _TCHAR* argv[])
{
	if(4 > argc){
		std::cout<<"缺少参数.格式为:Path <File> <newFile> <lines>"
			<<std::endl<<"退出请按'q':";
		while('q' != std::getchar());
		return -1;
	} else {
		string8 logfilename = argv[1];
		string8 formatfilename = argv[2];
		int32 lines = std::atoi(argv[3]);
		ReadLogFile(logfilename,formatfilename,lines);
		return 0;
	}
}
