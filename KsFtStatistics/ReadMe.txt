========================================================================
    CONSOLE APPLICATION : KsFtStatistics Project Overview
========================================================================

AppWizard has created this KsFtStatistics application for you.

This file contains a summary of what you will find in each of the files that
make up your KsFtStatistics application.


KsFtStatistics.vcxproj
    This is the main project file for VC++ projects generated using an Application Wizard.
    It contains information about the version of Visual C++ that generated the file, and
    information about the platforms, configurations, and project features selected with the
    Application Wizard.

KsFtStatistics.vcxproj.filters
    This is the filters file for VC++ projects generated using an Application Wizard. 
    It contains information about the association between the files in your project 
    and the filters. This association is used in the IDE to show grouping of files with
    similar extensions under a specific node (for e.g. ".cpp" files are associated with the
    "Source Files" filter).

KsFtStatistics.cpp
    This is the main application source file.

/////////////////////////////////////////////////////////////////////////////
Other standard files:

StdAfx.h, StdAfx.cpp
    These files are used to build a precompiled header (PCH) file
    named KsFtStatistics.pch and a precompiled types file named StdAfx.obj.

/////////////////////////////////////////////////////////////////////////////
Other notes:

AppWizard uses "TODO:" comments to indicate parts of the source code you
should add to or customize.

/////////////////////////////////////////////////////////////////////////////

项目说明:
	本工具主要是针对KsFt的日志统计工具.主要用于对南华日志中的所有数据进行提取,
	并统计所有合约的消息丢失情况.