#include "stdafx.h"
#include "autoptr.h"
#include "cxxunit.h"
using namespace boost;
//智能指针在我们开发中会使用的比较多

class ShareInstance
{
public :
	
	int value;
};

class mytest2 : public CTestSuit<mytest2>
{
public:
	TEST_BEGIN(mytest2)
		TEST_CASE(mytest2,func)
		TEST_CASE(mytest2,func1)
	TEST_END()

	void func()
	{
		shared_ptr<ShareInstance> x = shared_ptr<ShareInstance>(new ShareInstance) ;
		x->value = 1;

		_ASSERT_(x.use_count() == 1);
	}

	void func1()
	{
		shared_ptr<ShareInstance> x = shared_ptr<ShareInstance>(new ShareInstance) ;
		x->value = 1;
		_ASSERT_(x.use_count() == 1);
	}
	

protected:
private:
};
REGISTER_TESTSUIT(mytest2)

