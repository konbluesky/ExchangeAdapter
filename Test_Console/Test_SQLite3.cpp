#include "stdafx.h"
#include <list>
#include <vector>
#include <map>
#include <iostream>
#include "cxxunit.h"
#include "debugutils.h"
#include "sqlite3.h"

/*******************************************************************
* ���Կ��
*/
static void TestList();
class TestSQLLite3 : public CTestSuit<TestSQLLite3>
{
public:
	TEST_BEGIN(TestSQLLite3)
		TEST_CASE(TestSQLLite3,func)
		TEST_END()

		void func()
	{
		TestList();
	}
protected:
private:
};
REGISTER_TESTSUIT(TestSQLLite3)

/************************************************************************
* ���Ժ���
*/
static void TestSQLLite()
{
	int iRet = 0;
	sqlite3 *pDb = NULL;
	const char *pCreate = "create table tbl1(one varchar(21),two smallint)";
	const char *pInsert = "insert into tbl1 values('hello,world,myname!',10)";
	iRet = sqlite3_open("./prog.db",&pDb);
	if (SQLITE_OK != iRet)
	{
		DBG_TRACE(("�޷������ݿ�: %s",sqlite3_errmsg(pDb)));
		return;
	}

	iRet = sqlite3_exec(pDb,pCreate,NULL,NULL,NULL);
	if (SQLITE_OK != iRet)
	{
		DBG_TRACE(("����ʧ��"));
	}


	iRet = sqlite3_exec(pDb,pInsert,NULL,NULL,NULL);
	if (SQLITE_OK != iRet)
	{
		DBG_TRACE(("����ʧ��"));
	}

	sqlite3_close(pDb);
}

static void TestSelectData()
{
	sqlite3 *pDb = NULL;
	if(SQLITE_OK != sqlite3_open("./prog.db",&pDb))
		return;

	const char *pSelect = "select one from tbl1";
	sqlite3_stmt * stat;
	if(SQLITE_OK != sqlite3_prepare(pDb,pSelect,-1,&stat,NULL)){
		sqlite3_close(pDb);
		return;
	}
	
	while (SQLITE_ROW == sqlite3_step(stat))
	{
		const unsigned char* text = sqlite3_column_text(stat,0);
		std::cout<<text<<std::endl;
	}

	sqlite3_close(pDb);
}

static void TestList()
{
	TestSQLLite();
	TestSelectData();
}