#include "stdafx.h"
#include <list>
#include <vector>
#include <map>
#include <iostream>
#include "cxxunit.h"
#include "debugutils.h"
#include "urandom.h"
#include "libutilsbase.h"

/********************************************************************
 * 测试框架 
 */
static void TestList();
class TestDataStruct : public CTestSuit<TestDataStruct>
{
public:
	TEST_BEGIN(TestDataStruct)
		TEST_CASE(TestDataStruct,func)
	TEST_END()

	void func()
	{
		TestList();
	}
protected:
private:
};
REGISTER_TESTSUIT(TestDataStruct)

/********************************************************************
 * 自定义函数,作为使用样例
 */

/* vector使用 */
void TestVector()
{
	std::vector<int> vec;
	vec.push_back(1);
	vec.push_back(3);
	vec.push_back(4);

	using namespace std;
	for(vector<int>::iterator it = vec.begin(); it != vec.end(); ++it)
	{
		std::cout<<*it<<endl;
	}
}

/* string 使用 */
void TestString(void)
{
	tstring str;

	/* 单个的引用 */
	str = "temp";
	tstring::iterator iter = str.begin();
	std::cout<<*iter<<std::endl;
	*iter = 's';
	iter++;
	std::cout<<str<<std::endl;
	
	tstring::const_pointer ipot = str.c_str();
	std::cout<<ipot<<std::endl;

	/* 在尾部增加字符串 */
	str = "RMMSoft";
	str.append(" Test");
	std::printf(str.c_str());
	str.clear();
	std::printf("\n");

	/* 操作符重载 */
	str = "temp";
	str += " Test";
	std::cout<<str<<std::endl;

	/* 遍历字符 */
	tstring::size_type sizeLen = str.length();
	tstring::iterator iIndex;
	for(iIndex = str.begin();iIndex != str.end();iIndex++)
	{
		std::printf("%c\n",(*iIndex));
	}
	
	/* 访问某个字符 */
	tstring::reference refIndex = str.at(0);
	std::printf("%c\n",refIndex);
	std::printf("%c\n",str[1]);

	/* 截取尾部,剩下头部两个字符 */
	sizeLen = str.size();
	str.resize(2);
	std::cout<<str<<std::endl;

	/* 内存增长方式 */
	str.clear();
	std::cout<<"当前的最大容量为:"<<str.capacity()<<std::endl;
	str = "temp";
	std::cout<<"当前的最大容量为:"<<str.capacity()<<std::endl;
	str += " temp";
	std::cout<<"当前的最大容量为:"<<str.capacity()<<std::endl;
	str += " temp temp";
	std::cout<<"当前的最大容量为:"<<str.capacity()<<std::endl;
	for(int i = 0; i < 5;i++)
	{
		str += " temp";
	}
	std::cout<<"当前的最大容量为:"<<str.capacity()<<std::endl;

	return;
}

/* Map 使用 */
void TestMap()
{
	typedef std::pair<tstring,tstring> mapPair;
	std::map<tstring,tstring> mapTemp;
	std::map<tstring,tstring>::iterator m1_Iter;

	/* 初始化 */
	mapTemp.insert(mapPair("temp1","k1"));
	mapTemp.insert(mapPair("temp2","k2"));

	/* 遍历 */
	for(m1_Iter = mapTemp.begin();m1_Iter != mapTemp.end();m1_Iter++)
	{
		std::cout<<m1_Iter->first<<std::endl;
	}

	/* Key存在将无法增加 */
	m1_Iter = mapTemp.begin();
	std::cout<<m1_Iter->second<<std::endl;
	mapTemp.insert(mapPair("temp1","k3"));
	std::cout<<
		m1_Iter->second<<std::endl<<"当前长度为:"<<mapTemp.size()<<std::endl;
	m1_Iter->second = "k3";
	std::cout<<m1_Iter->second<<std::endl;
	
	return;
}

static void TestList()
{
	TestVector();
	TestString();
	TestMap();
}