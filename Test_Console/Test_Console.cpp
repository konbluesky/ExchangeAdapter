// Test_Console.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "cxxunit.h"

//这里会写些简单的测试代码，
int _tmain(int argc, _TCHAR* argv[])
{
	CTestSuitsFactory::Instance().Run();
	return 0;
}

