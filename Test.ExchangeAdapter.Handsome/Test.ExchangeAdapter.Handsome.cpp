
// Test.ExchangeAdapter.HengSheng.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "Test.ExchangeAdapter.Handsome.h"
#include "Test.ExchangeAdapter.HengShengDlg.h"
#include "OrderServiceDlg.h"
#include "udirectory.h"
USING_LIBSPACE
#ifdef _DEBUG
#define new DEBUG_NEW
#endif



// CTestExchangeAdapterHengShengApp

BEGIN_MESSAGE_MAP(CTestExchangeAdapterHengShengApp, CWinAppEx)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CTestExchangeAdapterHengShengApp construction

CTestExchangeAdapterHengShengApp::CTestExchangeAdapterHengShengApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CTestExchangeAdapterHengShengApp object

CTestExchangeAdapterHengShengApp theApp;


// CTestExchangeAdapterHengShengApp initialization

BOOL CTestExchangeAdapterHengShengApp::InitInstance()
{
	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	UDirectory::SetAppDirectory();
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	
	CWinAppEx::InitInstance();

	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization

	SetRegistryKey(_T("Local AppWizard-Generated Applications"));



	CTestExchangeAdapterHengShengDlg dlg;
	dlg.DoModal();



	try
	{

		
		COrderServiceDlg xdlg;
		xdlg.DoModal();
	}
	catch (...)
	{
		Sleep(100);
	}
	

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}


#pragma comment(linker,"/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")

