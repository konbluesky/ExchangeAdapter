
// Test.ExchangeAdapter.HengShengDlg.cpp : implementation file
//

#include "stdafx.h"
#include"ustringutils.h"

#include "Test.ExchangeAdapter.HengShengDlg.h"
#include "iostream"
#include "ucodehelper.h"
#include "HSCOMTradeAdapter.h"
#include "umaths.h"
#include "OrderServiceDlg.h"
#include "vld.h"
#pragma comment(lib,"vld.lib")
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace std;
using namespace tradeadapter;


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CTestExchangeAdapterHengShengDlg dialog




CTestExchangeAdapterHengShengDlg::CTestExchangeAdapterHengShengDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTestExchangeAdapterHengShengDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CTestExchangeAdapterHengShengDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CTestExchangeAdapterHengShengDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	
	
	ON_BN_CLICKED(IDC_BUTTON3, &CTestExchangeAdapterHengShengDlg::OnBnClickedButton3)
	
	ON_BN_CLICKED(IDC_BUTTON5, &CTestExchangeAdapterHengShengDlg::OnBnClickedButton5)
	
	ON_BN_CLICKED(IDC_BUTTON7, &CTestExchangeAdapterHengShengDlg::OnBnClickedButton7)
	ON_BN_CLICKED(IDOK2, &CTestExchangeAdapterHengShengDlg::OnBnClickedOk2)
	ON_BN_CLICKED(IDC_BUTTON100, &CTestExchangeAdapterHengShengDlg::OnBnClickedButton100)
	ON_BN_CLICKED(IDC_BUTTON9, &CTestExchangeAdapterHengShengDlg::OnBnClickedButton9)
	
	ON_BN_CLICKED(IDC_BUTTON_INITIALIZE, &CTestExchangeAdapterHengShengDlg::OnBnClickedButtonInitialize)
END_MESSAGE_MAP()


// CTestExchangeAdapterHengShengDlg message handlers

BOOL CTestExchangeAdapterHengShengDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	SetWindowText(_T("adapter����"));
	// TODO: Add extra initialization here

	GetDlgItem(IDC_EDIT_PORT)->SetWindowText(_T("8003"));
	GetDlgItem(IDC_EDIT_IP)->SetWindowText(_T("192.168.91.2"));
	GetDlgItem(IDC_EDIT_OPE)->SetWindowText(_T("607"));
	GetDlgItem(IDC_EDIT_BRANCH)->SetWindowText(_T("850"));
	GetDlgItem(IDC_EDIT_FACCOUNT)->SetWindowText(_T("60000005"));
	GetDlgItem(IDC_EDIT_USR)->SetWindowText(_T("607"));
	GetDlgItem(IDC_EDIT_PWD)->SetWindowText(_T("0"));
	//sbranchno
	

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CTestExchangeAdapterHengShengDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CTestExchangeAdapterHengShengDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CTestExchangeAdapterHengShengDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


#include <string>
#include <comutil.h>
using namespace std;
#pragma comment(lib, "comsuppw.lib") 




void CTestExchangeAdapterHengShengDlg::OnBnClickedButton3()
{
	// TODO: Add your control notification handler code here
	wchar_t * p = L"002535";
	instance->GetStockInfo(p);

}
//#import   "C:\windows\HsCommX.dll" 


void CTestExchangeAdapterHengShengDlg::OnBnClickedButton5()
{
	// TODO: Add your control notification handler code here
	Order xx;
	CString tmp;
	//GetDlgItem(IDC_EDIT_STOCKID)->get
	xx.ContractCode = GetIDText(IDC_EDIT_STOCKID);
	xx.Price = UMaths::ToDouble(GetIDText(IDC_EDIT_PRICE));
	xx.Amount = UMaths::ToInt32(GetIDText(IDC_EDIT_AMOUNT));
	xx.OpeDir = ((CButton*)GetDlgItem(IDC_RADIO_CHECKBUY))->GetCheck() ? Buy : Sell;
	int ret = instance->EntrustOrder(xx);
	GetDlgItem(IDC_EDIT1)->SetWindowText(UStrConvUtils::ToString(ret).c_str());
	
}

void CTestExchangeAdapterHengShengDlg::OnBnClickedButton7()
{
	// TODO: Add your control notification handler code here
	
	instance->QueryAccountCapital();
}

void CTestExchangeAdapterHengShengDlg::OnBnClickedOk2()
{
	// TODO: Add your control notification handler code here
	
	instance->QuerySystemStatus();
}

void CTestExchangeAdapterHengShengDlg::OnBnClickedButton100()
{
	// TODO: Add your control notification handler code here
	int ret = GetORderID();
	instance->CancelOrder(ret);
}

int CTestExchangeAdapterHengShengDlg::GetORderID()
{
	CEdit* pEdit = (CEdit*) GetDlgItem(IDC_EDIT1);
	CString txt;
	pEdit->GetWindowText(txt);
	int ret = UMaths::ToInt32(txt);
	return ret;

}

void CTestExchangeAdapterHengShengDlg::OnBnClickedButton9()
{

	// TODO: Add your control notification handler code here
	std::list<DealedOrder> orders;
	instance->GetDealedOrderInfo(&orders);
	for(std::list<DealedOrder>::iterator it = orders.begin(); it != orders.end(); ++it)
	{
		std::wcout<<it->business_time << _T("  ") <<it->stock_code << _T(" ") << it->entrust_no<<std::endl;
	}
	return;
	// TODO: Add your control notification handler code here
	int ret = GetORderID();
	int rets = instance->QueryOrder(ret);
	std::cout<<"=============="<<rets<<std::endl;
}


void CTestExchangeAdapterHengShengDlg::OnBnClickedButtonInitialize()
{
	// TODO: Add your control notification handler code here

	// TODO: Add your control notification handler code here

	// TODO: Add your control notification handler code here
	tradeadapter::HSIniParam usrinfo;
	CString tmp;
	GetDlgItem(IDC_EDIT_BRANCH)->GetWindowText(tmp);

	
	usrinfo.sbranch_no = (LPCTSTR)tmp;
	usrinfo.branch_no =  UMaths::ToInt32(usrinfo.sbranch_no.c_str());
	//3001

	GetDlgItem(IDC_EDIT_USR)->GetWindowText(tmp);
	usrinfo.usr = (LPCTSTR)tmp;

	GetDlgItem(IDC_EDIT_PWD)->GetWindowText(tmp);
	usrinfo.password = tmp;

	
	usrinfo.fund_account = GetIDText(IDC_EDIT_FACCOUNT);
	usrinfo.op_station = usrinfo.branch_no;

	usrinfo.op_entrust_way = L"0";

	GetDlgItem(IDC_EDIT_IP)->GetWindowText(tmp);
	usrinfo.addr = (LPCTSTR) tmp;
	GetDlgItem(IDC_EDIT_PORT)->GetWindowText(tmp);
	usrinfo.port = UMaths::ToInt32(tmp);

	usrinfo.l_op_code = _T("607");
	usrinfo.vc_op_password = _T("0");
	usrinfo.op_station = _T("001E90AEABEA");


	instance = new tradeadapter::handsome::CHSTradeAdapter();


	instance->Initialize(&usrinfo);	
}
