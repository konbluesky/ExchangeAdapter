
// Test.ExchangeAdapter.HengShengDlg.h : header file
//

#pragma once
#include "Resource.h"
namespace
tradeadapter
{
	namespace handsome
	{
		class CHSTradeAdapter;
	}
};

// CTestExchangeAdapterHengShengDlg dialog
class CTestExchangeAdapterHengShengDlg : public CDialog
{
// Construction
public:
	CTestExchangeAdapterHengShengDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_TESTEXCHANGEADAPTERHENGSHENG_DIALOG };

	int GetORderID();

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

	CString GetIDText(int id)
	{
		CString tmp;
		GetDlgItem(id)->GetWindowText(tmp);
		return tmp;
	}
// Implementation
protected:
	HICON m_hIcon;

	tradeadapter::handsome::CHSTradeAdapter* instance;


	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton4();
	afx_msg void OnBnClickedButton5();
	afx_msg void OnBnClickedButton6();
	afx_msg void OnBnClickedButton7();
	afx_msg void OnBnClickedOk2();
	afx_msg void OnBnClickedButton100();
	afx_msg void OnBnClickedButton9();
	
	afx_msg void OnBnClickedButtonInitialize();
};
