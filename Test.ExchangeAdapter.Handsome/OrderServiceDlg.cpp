// OrderServiceDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Test.ExchangeAdapter.Handsome.h"
#include "OrderServiceDlg.h"
#include "..\..\public\IOrderService.h"
#include "..\ExchangeAdapter.Handsome\HSCOMTradeAdapter.h"
#include "iostream"
using namespace tradeadapter;
using namespace tradeadapter::handsome;

// COrderServiceDlg dialog

IMPLEMENT_DYNAMIC(COrderServiceDlg, CDialog)

COrderServiceDlg::COrderServiceDlg(CWnd* pParent /*=NULL*/)
	: CDialog(COrderServiceDlg::IDD, pParent)
{
	 

}

COrderServiceDlg::~COrderServiceDlg()
{
}

void COrderServiceDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(COrderServiceDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON1, &COrderServiceDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &COrderServiceDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON5, &COrderServiceDlg::OnBnClickedButton5)
END_MESSAGE_MAP()


// COrderServiceDlg message handlers

void COrderServiceDlg::OnBnClickedButton1()
{
	// TODO: Add your control notification handler code here
	 instance = IOrderService::CreateInstance(_T("handsome"));
	instance->Initialize(NULL);
}

void COrderServiceDlg::OnBnClickedButton2()
{
	// TODO: Add your control notification handler code here
	Order order;
	order.ContractCode = L"002535";
	order.Price = (float)7.80;
	order.Amount = 1000;
	order.OpeDir = Buy;
	
//	IOrderService& instance = IOrderService::Instance();
	instance->GetAdapter()->EntrustOrder(order);
	int x = instance->EntrustOrder(L"11",order);

	std::cout<<"order id " <<x<<std::endl;
	

}

void COrderServiceDlg::OnBnClickedButton5()
{
	// TODO: Add your control notification handler code here
	//IOrderService& instance = IOrderService::Instance();
	handsome::CHSTradeAdapter* p = (handsome::CHSTradeAdapter*)instance->GetAdapter();
	p->QueryAccountCapital();
}

BOOL COrderServiceDlg::OnInitDialog()
{
	BOOL RET = CDialog::OnInitDialog();
	SetWindowText(_T("Service����"));
	return RET;
}
