
// Test.ExchangeAdapter.HengSheng.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CTestExchangeAdapterHengShengApp:
// See Test.ExchangeAdapter.HengSheng.cpp for the implementation of this class
//

class CTestExchangeAdapterHengShengApp : public CWinAppEx
{
public:
	CTestExchangeAdapterHengShengApp();

// Overrides
	public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CTestExchangeAdapterHengShengApp theApp;