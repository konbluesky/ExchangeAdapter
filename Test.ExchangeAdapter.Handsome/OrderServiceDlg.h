#pragma once

namespace tradeadapter
{
	class IOrderService;
};

// COrderServiceDlg dialog

class COrderServiceDlg : public CDialog
{
	DECLARE_DYNAMIC(COrderServiceDlg)

public:
	COrderServiceDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~COrderServiceDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG1 };


	tradeadapter::IOrderService* instance;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton5();

	virtual BOOL OnInitDialog();

};
