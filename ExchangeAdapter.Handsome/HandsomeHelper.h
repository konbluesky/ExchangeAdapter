#ifndef __HANDSOME_HELPER_H__
#define __HANDSOME_HELPER_H__

#include <iostream>
#include <string>
#include <map>
#include <list>
#include "ustringutils.h"
#include "IOrderService.h"
USING_LIBSPACE


namespace tradeadapter
{


	namespace handsome
	{

		//comment: handsome is based on com, which means all char/string  type is widechar
		typedef wchar_t hschar;
		typedef std::wstring hsstring;

		typedef std::map<std::wstring, std::wstring> ResultSet;
		std::ostream& operator << (std::ostream&, const  ResultSet& res);
		typedef std::list<ResultSet> ResultSets;



		ETradeUnit FromHSStringToTradeUnit(const tstring  pinfo);



		hsstring BSToHSString(EBuySell ope);



		enum MoneyType
		{
			人民币 = '0',
			美圆 = '1',
			港币 = '2'
		};

		//注
		enum Exchange_Type
		{
			前台未知交易所 = '0',
			上海 = '1',
			深圳 = '2',
			金交所 = '5',
			创业板 = '8',
			上海B = 'D',
			深圳B = 'H',
		};

		enum FunctionCode
		{
			INQUERY_SYSTEM_STATUS = 100,
			GET_MARKETINFO = 105,
			ENTRUST_STOCK = 300,
			USER_CONFIRM = 200,

			//委托股票价格
			ENTRUST_PRICE = 301,

			//这个才是真正买卖股票的功能
			ENTRUST_CONFRIM = 302,
			ENTRUST_CANCEL = 304,
			INQUIRY_SETTLEMENT_STREAM = 308,
			INQUIRY_QUOTE = 400,
			INQUIRY_ENTRUST_STATUS = 401,
			//查询成交单的内容
			INQUIRY_KNOCKDOWN = 402,
			INQUIRY_STOCK = 403,
			INQUIRY_CAPITAL_STREAM = 404,
			INQUIRY_CAPTIAL = 405,
			INQUIRY_STOCKHOLDER = 407,
			INQUIRY_HISTORY_KNOCKDOWN = 411,
			INQUIRY_HISTORY_CAPTITAL_SECURITY_STREAM = 412,
			MASS_INQUIRY = 413,
			INQUIRY_HISTORY_ENTRUST = 421,
			MASS_ENTRUST = 2007,
			MASS_ENTRUST_CONCEL = 2016,
			MASS_RISK_CONTROL = 2031,
			MULTI_CREATE_POSITION = 2008,
			MULTI_ENTRUST = 2020,
			MULTI_ENTRUST_CONCEL = 2019,  //暂定MULTI_CONCEL=2019;//暂定
			MULTI_INQUIRY_ENTRUST = 2017,
			MULTI_INQUIRY_KNOCKDOWN = 2018,
			MULTI_INQUIRY_TRANSACTION_INFO = 2011,
			MULTI_INQUIRY_TRANSACTION_DETAILS = 2012,
			MULTI_INQUIRY_AGGREGATION_POSITION = 2013,
			MULTI_INQUIRY_POSITION_DETAILS = 2014,
		};


		enum entrust_way//：委托方式
		{
			自助委托 = '0',
			电话委托 = '1',
			驻留委托 = '2',
			远程委托 = '3',
			柜台委托 = '4',
			漫游委托 = '5',
			分支委托 = '6',
			internet委托 = '7',
		};


		//'1' -- 买入  	'2' -- 卖出 		对于期货 1 开仓	2 平仓//
		enum Entrust_bs
		{
			buy = 1,
			sell = 2,
		};


		//'1' -- 买入  	'2' -- 卖出 		对于期货 1 开仓	2 平仓

		std::ostream& GetLogStream();




	};
}
#endif