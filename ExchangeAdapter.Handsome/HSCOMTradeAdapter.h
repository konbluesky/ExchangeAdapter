#pragma once
#include <string>
#include <vector>
#include <algorithm>
#include <tchar.h>
#include "ITradeAdapter.h"
#include "IOrderService.h"
#include "HandsomeHelper.h"
#include "ustringutils.h"
#include "threadlock.h"
#include "ucodehelper.h"
USING_LIBSPACE

namespace tradeadapter
{


	namespace handsome
	{

		enum OrderStatus
		{
			Failed,
			Online,
			PartDealed,
			CancelOnline,
			AllDealed,

		};


		struct HSIniParam
		{
			tstring	addr;			//必须
			int				port;			//必须

			tstring	l_op_code;		//代操作id，可以为空
			tstring	vc_op_password; //代操作密码，可以为空
			tstring	op_branch_no;	//代操作机构id，可以为空
			tstring	op_station;		//可以为空

			//结构
			tstring	sbranch_no;		//必须 这个字段和下面的字段内容相同。
			int				branch_no;		//必须
			tstring	usr;			//必须
			tstring	password;		//必须			
			tstring	op_entrust_way;	//必须

			tstring	fund_account;
			tstring	stock_account;
			tstring	futures_account;	// 期货账号
			bool IsVaild() //检查参数是否都填满了
			{
				return addr != _T("") &&
					port != 0 &&
					sbranch_no != _T("") &&
					branch_no != 0 &&
					usr != _T("") &&
					op_entrust_way != _T("");
			}
		};


		struct HSIniParamW
		{
			std::wstring	addr;			//必须
			int				port;			//必须

			std::wstring	l_op_code;		//代操作id，可以为空
			std::wstring	vc_op_password; //代操作密码，可以为空
			std::wstring	op_branch_no;	//代操作机构id，可以为空
			std::wstring	op_station;		//可以为空

			//结构
			std::wstring	sbranch_no;		//必须 这个字段和下面的字段内容相同。
			int				branch_no;		//必须
			std::wstring	usr;			//必须
			std::wstring	password;		//必须			
			std::wstring	op_entrust_way;	//必须 委托方式

			std::wstring	fund_account;
			std::wstring	stock_account;
			std::wstring	futures_account;	// 期货账号

			static HSIniParamW FromHSIniParam(HSIniParam input)
			{
				HSIniParamW ret;
				ret.addr = LIB_NS::UCodeHelper::ToWS(input.addr);
				ret.port = input.port;
				ret.l_op_code = LIB_NS::UCodeHelper::ToWS(input.l_op_code);
				ret.vc_op_password = LIB_NS::UCodeHelper::ToWS(input.vc_op_password);
				ret.op_branch_no = LIB_NS::UCodeHelper::ToWS(input.op_branch_no);
				ret.op_station = LIB_NS::UCodeHelper::ToWS(input.op_station);
				ret.sbranch_no = LIB_NS::UCodeHelper::ToWS(input.sbranch_no);
				ret.branch_no = input.branch_no;
				ret.usr = LIB_NS::UCodeHelper::ToWS(input.usr);
				ret.password = LIB_NS::UCodeHelper::ToWS(input.password);
				ret.op_entrust_way = LIB_NS::UCodeHelper::ToWS(input.op_entrust_way);
				ret.fund_account = LIB_NS::UCodeHelper::ToWS(input.fund_account);
				ret.stock_account = LIB_NS::UCodeHelper::ToWS(input.stock_account);
				ret.futures_account = LIB_NS::UCodeHelper::ToWS(input.futures_account);
				return ret;
			}
			bool IsVaild() //检查参数是否都填满了
			{
				return addr != L"" &&
					port != 0 &&
					sbranch_no != L"" &&
					branch_no != 0 &&
					usr != L"" &&
					op_entrust_way != L"";
			}
		};

		class TAIAPI CHSTradeAdapter : public ITradeAdapter
		{
		public:
			CHSTradeAdapter();

			virtual bool Initialize(void* InitParam);
			virtual bool InitializeByXML(const TCHAR* fname);


			virtual bool QueryOrder(OrderID id, int* pCount);



			virtual bool EntrustOrder(Order order, OrderID* pOutID);

			virtual	bool CancelOrder(OrderID orderid);

			bool _cancelorder(OrderID orderid);

			virtual void UnInitialize();

			bool Connect(const wchar_t* addr, int port);
			std::pair<bool, Order> GetOrderInfo(OrderID id);



			virtual bool QueryPostions(std::list<PositionInfo>* pPoss, const TCHAR* pstock);






			struct StockInfo
			{
				hsstring stock_code;
				hsstring stock_name;
				hsstring exchange_type;
				hsstring stock_type;
			};
			StockInfo GetStockInfo(const TCHAR* stock_code);

			bool ReConnect();
			//如果不成功则返回0，如果成功，则返回成交量
			bool GetDealedOrderInfo(std::list<DealedOrder>* pinfos, CONST TCHAR* stockid = _T(""), OrderID id = -1);
			std::pair<int, OrderStatus>  GetOnlinedOrderInfo(OrderID id);
			ResultSet		QuerySystemStatus();
			ResultSets	QueryAccountCapital(void);





		private:
			void BeginJob(int jobid);
			void AddFieldValue(const wchar_t* pKey, const wchar_t* pVal);
			void AddFieldValue(const wchar_t* pKey, const char* pVal);
			bool CommitJob();
			hsstring GetFieldValW(const wchar_t* key);
			

			std::string GetFieldValA(const wchar_t* key);

			StockInfo GetStockInfoFromOrderID(OrderID id);
			void _setLastError();

			tstring GetFieldValT(const char* key);
			tstring GetFieldValT(const wchar_t* key);

			virtual bool QueryDealedOrders(std::list<DealedOrder>* pres);
			bool GetDealedOrderInfoVithPos(std::list<DealedOrder>* pinfos, CONST TCHAR* stockcode, OrderID id, int iPos);

			void QueryAccountBasicInfo1();
			void QueryAccountBasicInfo2();
			void QueryAccountBasicInfo3();

			virtual bool QueryAccountInfo(BasicAccountInfo* info);

			



		private:
			

			virtual bool IsInitialized();

			virtual bool QueryEntrustInfos(std::list<EntrustInfo>* pinfo);

			virtual tstring GetName();

		private:
#pragma warning(disable : 4251)	

			//注这个信息是从系统查出来的。
			BasicAccountInfo m_accountInfo;
			threadlock::multi_threaded_local m_lock;
			HSIniParamW m_usrinfo;
			std::map<int, Order> m_idorders;
			ICommPtr m_hsconn;
			StockInfo m_laststockinfo;
			std::vector<std::pair<std::wstring, std::wstring> > m_inputdatas;
			//std::pair<int,tstring> m_lasterrors; //注：这个后面要扩展为链表
			
			bool m_conneted;
		};
	};
};