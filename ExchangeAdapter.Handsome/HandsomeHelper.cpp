#include "stdafx.h"
#include "ucodehelper.h"
#include "HandsomeHelper.h"
#include "tconfigsetting.h"
#include "tinyxml.h"
#include <fstream>
#include "texceptionex.h"


USING_LIBSPACE
namespace tradeadapter
{
	namespace handsome
	{
		std::ostream& operator << (std::ostream& os, const ResultSet& res)
		{
			os << std::endl;
			for (ResultSet::const_iterator it = res.begin(); it != res.end(); ++it)
			{
				_bstr_t first(it->first.c_str());
				_bstr_t second(it->second.c_str());
				os << (char*)first << ":" << (char*)second << std::endl;
			}
			os << std::endl;
			return os;
		}



		handsome::hsstring BSToHSString(EBuySell ope)
		{
			if (ope == eBuy)
			{
				return L"1";
			}
			else if (ope == eSell)
			{
				return L"2";
			}
			throw TException(_T("not definition IOrderService::BuySell types"));
		}
		class nullstream : public std::ostream
		{
		public:
			nullstream() :std::ostream(NULL)
			{

			}
		};

		nullstream& operator << (nullstream& os, const ResultSet& res)
		{
			return os;
		}

		static std::ostream* pss = NULL;

		std::ostream& GetLogStream()
		{
			if (pss != NULL)
			{
				return *pss;
			}

			TConfigSetting _cfg;
			bool f = _cfg.LoadConfig(_T("ExchangeAdapter.Handsome.xml"));
			if (!f)
			{
				return std::cout;
			}
			std::string stream = _cfg["configuration"]["logstream"].EleVal();
			if (stream == "cout")
			{
				pss = &std::cout;

			}
			else if (stream == "null")
			{
				static nullstream nu;
				pss = &nu;
			}
			else
			{
				static std::ofstream of(stream.c_str(), std::ofstream::app);
				if (of.fail())
				{
					pss = &std::cout;
				}
				else
				{
					pss = &of;
				}
			}
			return *pss;
		}
#pragma warning(disable : 4482)
		tradeadapter::ETradeUnit FromHSStringToTradeUnit(const tstring pinfo)
		{
			if (pinfo == _T("0"))
				return ETradeUnit::eSection;
			else if (pinfo == _T("1"))
				return ETradeUnit::eHand;
			else
				return ETradeUnit::eSection;
		}

	};
}