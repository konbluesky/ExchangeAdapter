// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>




#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // some CString constructors will be explicit

#include <atlbase.h>
#include <atlstr.h>
#import ".\hscom\HSCOMMX.dll" no_namespace   rename( "EOF",   "EndOfFile ") 

//#ifndef UNICODE
//#error Handsome dll is based on COM which must compiled with unicode
//#endif