#include "stdafx.h"
#include "ucodehelper.h"
#include "ExchangeAdapterDef.h"
#include <iostream>

#pragma warning(disable : 4996)
#pragma warning(disable : 4251)
#pragma warning(disable : 4390)
#pragma warning(disable : 4018)
#pragma warning(disable : 4482)

#include "texceptionex.h"
#include "LogHelper.h"
#include "tconfigsetting.h"
#include "tinyxml.h"
#include "umaths.h"
#include "uinteger.h"
#include "ucodehelper.h"
#include "HSCOMTradeAdapter.h"
#pragma warning(disable : 4273)

#define AUTOGARD()   threadlock::autoguard<threadlock::multi_threaded_local>  _guard_(&m_lock)

USING_LIBSPACE


#define CHECK_RESULT(ret,expt,msg) if(ret != expt) { SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,-1,msg));  goto LEXIST;}
#define LOG(msg)   
namespace tradeadapter
{


	namespace handsome
	{
		void CHSTradeAdapter::AddFieldValue(const wchar_t* pKey, const wchar_t* pVal)
		{
			m_inputdatas.push_back(std::make_pair<std::wstring, std::wstring>(pKey, pVal));
		}

		void CHSTradeAdapter::AddFieldValue(const wchar_t* pKey, const char* pVal)
		{

			m_inputdatas.push_back(std::make_pair<std::wstring, std::wstring>(pKey, UCodeHelper::ToWS(pVal)));
		}


		std::string CHSTradeAdapter::GetFieldValA(const wchar_t* key)
		{
			try
			{
				
				_bstr_t  data = m_hsconn->FieldByName(key);
				char* aptr = (char*)data;
				return aptr == NULL ? "" : aptr;
			}
			catch (...)
			{
				return "";
			}

		}

		hsstring CHSTradeAdapter::GetFieldValW(const  wchar_t* key)
		{
			try
			{
				_bstr_t		data = m_hsconn->FieldByName(key);
				wchar_t*	ptr = (wchar_t*)data;
				return ptr == NULL ? L"" : ptr;
			}
			catch (...)
			{
				return L"";
			}
		}

		
		
		void CHSTradeAdapter::_setLastError()
		{
			long code = m_hsconn->GetErrorNo();

			if (code)
			{
				_bstr_t msg = m_hsconn->GetErrorMsg();

				const TCHAR* ret = (const TCHAR*)msg;

				SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter, -1, (const TCHAR*)msg));
				

				GetLogStream() << (const char*)msg;
			}
			

		}



		bool CHSTradeAdapter::Initialize(void* pParam /*= NULL*/)
		{
			HSIniParam* input = (HSIniParam *)pParam;
			m_usrinfo = HSIniParamW::FromHSIniParam(*input);

			HRESULT ret = S_FALSE;
			ret = ::CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);



			bool runret = this->Connect(m_usrinfo.addr.c_str(), m_usrinfo.port);
			CHECK_RESULT(runret, true, (_T("连接服务器出错")));

		LEXIST:

			return runret;
		}



		bool CHSTradeAdapter::Connect(const wchar_t* addr, int port)
		{
			AUTOGARD();

			HRESULT ret = S_FALSE;
			long runret = -1;

			//1.Create instance
			ret = m_hsconn.CreateInstance(__uuidof(Comm));
			CHECK_RESULT(ret, S_OK, (_T("m_hsconn.CreateInstance(__uuidof(Comm)); failed")));

			//2.it's clumzy
			try
			{
				ret = m_hsconn->Create();
			}
			catch (...)
			{
			}



			//3.get ip address & port
			//m_usrinfo.addr = UComHelper::ToWS(addr);
			//	WriteDetailLog(_T("debug"),LTALL,(m_usrinfo.addr.c_str()));;
			m_usrinfo.port = port;

			//4. connect server
			runret = m_hsconn->ConnectX(1, m_usrinfo.addr.c_str(), m_usrinfo.port, 0, L"", 0);
			CHECK_RESULT(runret, 0, (_T("m_hsconn->ConnectX; failed")));

			if (runret != 0)
			{
				_setLastError();
			}
		LEXIST:
			if (runret == 0)
			{
				m_conneted = true;
			}
			return runret == 0;
		}

		void CHSTradeAdapter::UnInitialize()
		{
			AUTOGARD();

			if (m_hsconn != NULL)
			{
				m_hsconn->DisConnect();
				m_hsconn = NULL;
			}
			m_conneted = false;
			::CoUninitialize();
		}




		CHSTradeAdapter::StockInfo CHSTradeAdapter::GetStockInfo(const TCHAR* tstock_code)
		{
			AUTOGARD();
			std::wstring stock_code = UCodeHelper::ToWS(tstock_code);
			// StockInfo m_laststockinfo; //for cache
			if (m_laststockinfo.stock_code == stock_code)
			{
				return m_laststockinfo;
			}
			bool ret = false;

			BeginJob(GET_MARKETINFO);
			AddFieldValue(L"version", L"1");
			AddFieldValue(L"op_entrust_way", m_usrinfo.op_entrust_way.c_str());
			AddFieldValue(L"op_station", m_usrinfo.op_station.c_str());
			AddFieldValue(L"branch_no", m_usrinfo.sbranch_no.c_str());
			AddFieldValue(L"stock_code", stock_code.c_str());
			CommitJob();

			if (m_hsconn->ErrorNo != 0)
			{
				_setLastError();
				//return 
			}

			StockInfo info;

			info.stock_code = stock_code;
			info.stock_name = GetFieldValW(L"stock_name");
			info.exchange_type = GetFieldValW(L"exchange_type");
			info.stock_type = GetFieldValW(L"stock_type");
			m_laststockinfo = info;

			return info;
		}




		//委托下单
		bool CHSTradeAdapter::EntrustOrder(Order order, OrderID* pOutID)
		{
			//std::wstring wcode = UCodeHelper::ToWS(order.ContractCode);
			hsstring exchange_type = GetStockInfo(order.ContractCode.c_str()).exchange_type;

			*pOutID = tradeadapter::InvalidOrderId;

			AUTOGARD();
			bool ret = false;
			BeginJob(ENTRUST_CONFRIM);

			AddFieldValue(L"version", L"1");
			AddFieldValue(L"l_op_code", m_usrinfo.l_op_code.c_str());
			AddFieldValue(L"vc_op_password", m_usrinfo.vc_op_password.c_str());
			AddFieldValue(L"op_branch_no", m_usrinfo.op_branch_no.c_str());
			AddFieldValue(L"op_entrust_way", m_usrinfo.op_entrust_way.c_str());
			AddFieldValue(L"op_station", m_usrinfo.op_station.c_str());
			AddFieldValue(L"branch_no", m_usrinfo.sbranch_no.c_str());
			AddFieldValue(L"fund_account", m_usrinfo.fund_account.c_str());
			AddFieldValue(L"password", m_usrinfo.password.c_str());
			AddFieldValue(L"exchange_type", exchange_type.c_str()); //注:TODO ,LIN ZOU ZHONGJI SHIYONG 2
			AddFieldValue(L"stock_account", L"");//m_usrinfo.stock_account.c_str()); // 文档说传空使用默认
			AddFieldValue(L"stock_code", order.ContractCode.c_str());
			AddFieldValue(L"entrust_amount", UCodeHelper::ToWS(UStrConvUtils::ToString(order.Amount)).c_str());
			AddFieldValue(L"entrust_price", UStrConvUtils::ToString(order.Price).c_str());
			AddFieldValue(L"entrust_prop", L"");//这个可以不传
			hsstring data = BSToHSString(order.OpeDir);
			AddFieldValue(L"entrust_bs", data.c_str());
			AddFieldValue(L"c_bs", data.c_str());
			AddFieldValue(L"bank_no", L"");
			AddFieldValue(L"client_rights", L"");

			CommitJob();

			if (m_hsconn->ErrorNo != 0)
			{

				_setLastError();
				/*
				错误号：-52：“此股票不存在”
				-51：“未登记该交易所帐号”
				-64:“此股不能进行此类委托”
				-66：“帐户状态不正常”
				-67：“您未作指定交易”
				-57：“可用金额不足”
				-58：“股票余额不足”
				-55：“数量不符合交易所规定”
				-56：“委托价格不符合交易所规定”
				-59：“闭市时间不能委托”
				-65：“此股票停牌，不能委托”    （如果停牌允许委托，不报错误）
				-61：“委托失败”                （其他错误）
				80520：“该资金帐号禁止周边交易”
				*/

				return false;
			}

			ResultSet res;
			res[L"_下单"] = L"\n";
			//res[L"error_no"] = GetFieldValW(L"error_no");
			res[L"error_info"] = GetFieldValW(L"error_info");
			res[L"entrust_no"] = GetFieldValW(L"entrust_no");
			res[L"batch_no"] = GetFieldValW(L"batch_no");
			GetLogStream() << res;



			tstring no = UCodeHelper::ToTS(res[L"entrust_no"]);
			int order_no = UMaths::ToInt32(no.c_str());

			*pOutID = order_no;
			return true;
		}



		//comment: when we call cancel order api, we must call query order status info
		//to make sure it's turely canceled.
		//if not, we should not 

		//TODO 注：handsome的撤单，返回的有待撤的状态，这个会使业务调用撤单函数后单子的状态未知，因此需要修改完善此函数，对于自动交易的
		//应该继续调用撤单的函数，知道这个返回的状态是明确的。
		bool CHSTradeAdapter::CancelOrder(OrderID orderid)
		{
			int n = 0;

			_cancelorder(orderid);

			while (true)
			{

				if (GetOnlinedOrderInfo(orderid).second != OrderStatus::CancelOnline)
				{
					break;
				}
				::Sleep(1000);
				n++;
				if (n > 30)
				{
					break;
				}

			}
			return true;

		}



		bool CHSTradeAdapter::CommitJob()
		{
			HRESULT ret = S_FALSE;
			int cout = m_inputdatas.size();

			//default 1 row, 
			ret = this->m_hsconn->SetRange(cout, 1);

			for (int i = 0; i < m_inputdatas.size(); ++i)
			{
				this->m_hsconn->AddField(m_inputdatas[i].first.c_str());
			}

			for (int i = 0; i < m_inputdatas.size(); ++i)
			{
				this->m_hsconn->AddValue(m_inputdatas[i].second.c_str());
			}

			static long sendid = UTimeUtils::GetNowTime();

			m_hsconn->SenderId = sendid++;

			ret = m_hsconn->Send();
			ret = m_hsconn->FreePack();
			ret = m_hsconn->Receive();
			return true;
		}



		void CHSTradeAdapter::BeginJob(int jobid)
		{
			HRESULT ret = S_FALSE;
			m_inputdatas.clear();
			if (m_hsconn == NULL)
			{
				GetLogStream() << "m_hsconn == NULL" << std::endl;
				throw TException(_T("com对象为空"));
			}
			ret = m_hsconn->SetHead(m_usrinfo.branch_no, jobid);
			CHECK_RESULT(ret, S_OK, (_T("called m_hsconn->SetHead(m_branch_no, jobid) failed")));
		LEXIST:
			;
		}



		ResultSets CHSTradeAdapter::QueryAccountCapital(void)
		{
			AUTOGARD();
			BeginJob(INQUIRY_CAPTIAL);
			AddFieldValue(L"version", L"1");
			AddFieldValue(L"op_branch_no", m_usrinfo.op_branch_no.c_str());
			AddFieldValue(L"op_entrust_way", m_usrinfo.op_entrust_way.c_str());
			AddFieldValue(L"op_station", m_usrinfo.op_station.c_str());
			AddFieldValue(L"branch_no", m_usrinfo.sbranch_no.c_str());
			AddFieldValue(L"fund_account", m_usrinfo.fund_account.c_str());
			AddFieldValue(L"password", m_usrinfo.password.c_str());
			AddFieldValue(L"money_type", L"0");
			CommitJob();

			ResultSets ret;
			while (m_hsconn->Eof == 0)
			{
				ResultSet res;

				res[L"money_type"] = GetFieldValW(L"money_type");
				res[L"current_balance"] = GetFieldValW(L"current_balance");
				res[L"enable_balance"] = GetFieldValW(L"enable_balance");
				res[L"fetch_balance"] = GetFieldValW(L"fetch_balance");
				//res[L"interest"] = GetFieldValW(L"interest");
				res[L"asset_balance"] = GetFieldValW(L"asset_balance");
				//			res[L"fetch_cash"] = GetFieldValW(L"fetch_cash");
				//			res[L"fund_balance"] = GetFieldValW(L"fund_balance");
				//			res[L"market_value"] = GetFieldValW(L"market_value");
				//			res[L"opfund_market_value"] = GetFieldValW(L"opfund_market_value");
				res[L"pre_interest"] = GetFieldValW(L"pre_interest");

				ret.push_back(res);

				m_hsconn->MoveBy(1);
			}
			return ret;

		}

		ResultSet CHSTradeAdapter::QuerySystemStatus()
		{
			AUTOGARD();
			BeginJob(INQUERY_SYSTEM_STATUS);
			AddFieldValue(L"version", L"1");
			AddFieldValue(L"op_entrust_way", m_usrinfo.op_entrust_way.c_str());
			AddFieldValue(L"op_station", m_usrinfo.op_station.c_str());
			CommitJob();

			ResultSet res;

			if (m_hsconn->Eof == 0)
			{
				res[L"_查询系统状态"] = L"\n";
				res[L"init_date"] = GetFieldValW(L"init_date");
				res[L"sys_status"] = GetFieldValW(L"sys_status");
				res[L"sys_status_name"] = GetFieldValW(L"sys_status_name");
				GetLogStream() << res;
			}
			return res;
		}

		bool CHSTradeAdapter::InitializeByXML(const TCHAR* fname)
		{
			TConfigSetting cfgsetting;
			bool ret = cfgsetting.LoadConfig(fname);
			if (!ret)
			{
				std::cout << "无法加载配置文件，是否是当前目录" << std::endl;
				return ret;
			}
			HSIniParam input;


			std::string ipaddr = cfgsetting["configuration"]["saddressip"].EleVal();
			input.addr = UCodeHelper::ToTS(ipaddr);

			std::string ipport = cfgsetting["configuration"]["iport"].EleVal();
			input.port = UMaths::ToInt32(UCodeHelper::ToWS(ipport).c_str());

			std::string sbranchno = cfgsetting["configuration"]["sbranchno"].EleVal();
			input.sbranch_no = UCodeHelper::ToTS(sbranchno);

			std::string usr = cfgsetting["configuration"]["susr"].EleVal();
			input.usr = UCodeHelper::ToTS(usr);

			std::string password = cfgsetting["configuration"]["spwd"].EleVal();
			input.password = UCodeHelper::ToTS(password);

			input.branch_no = UMaths::ToInt32(m_usrinfo.sbranch_no.c_str());

			std::string op_station = cfgsetting["configuration"]["op_station"].EleVal();
			input.op_station = UCodeHelper::ToTS(op_station);

			std::string op_entrust_way = cfgsetting["configuration"]["op_entrust_way"].EleVal();
			input.op_entrust_way = UCodeHelper::ToTS(op_entrust_way);

			std::string fund_account = cfgsetting["configuration"]["fund_account"].EleVal();
			input.fund_account = UCodeHelper::ToTS(fund_account);

			std::string l_op_code = cfgsetting["configuration"]["l_op_code"].EleVal();
			input.l_op_code = UCodeHelper::ToTS(l_op_code);

			std::string vc_op_password = cfgsetting["configuration"]["vc_op_password"].EleVal();
			input.vc_op_password = UCodeHelper::ToTS(vc_op_password);


			ret = this->Initialize(&input);
			return ret;
		}



		bool CHSTradeAdapter::QueryOrder(OrderID id, int* pCount)
		{
			*pCount = 0;
			StockInfo info = GetStockInfoFromOrderID(id);

			std::list<DealedOrder> dealedorders;

			bool ret = GetDealedOrderInfo(&dealedorders,UCodeHelper::ToTS( info.stock_code).c_str());
			if (ret)
			{
				for (std::list<DealedOrder>::iterator it = dealedorders.begin(); it != dealedorders.end(); ++it)
				{
					if (it->entrust_no == id)
					{
						*pCount =  (int)it->business_amount;
						return true;
					}
				}
			}

			*pCount = GetOnlinedOrderInfo(id).first;
			return true;
		}

		

		std::pair<bool, Order> CHSTradeAdapter::GetOrderInfo(OrderID id)
		{
			std::map<int, Order>::iterator it;
			it = m_idorders.find(id);
			if (it == m_idorders.end())
			{
				Order dummy;
				return std::make_pair<bool, Order>(PAIR_WRAP(false), PAIR_WRAP(dummy));
			}
			return std::make_pair<bool, Order>(PAIR_WRAP(true), PAIR_WRAP(it->second));
		}

		handsome::CHSTradeAdapter::StockInfo CHSTradeAdapter::GetStockInfoFromOrderID(OrderID id)
		{
			StockInfo info;
			if (GetOrderInfo(id).first == true)
			{
				info = GetStockInfo(GetOrderInfo(id).second.ContractCode.c_str());
			}
			else
			{
				//注：在交易中，这些是必须要填的字段，
				info.exchange_type = L"2";
				info.stock_code = L"002535";
			}
			return info;
		}



		bool CHSTradeAdapter::GetDealedOrderInfo(std::list<DealedOrder>* pinfos, CONST TCHAR* stockcode, OrderID id)
		{
			/*
			static std::map<tstring,std::list<DealedOrder> > xmaps;
			static std::map<tstring,int> xxmap;
			*/
			AUTOGARD();
			int POS = 0;
			std::list<DealedOrder> tmp;
			pinfos->clear();
			while (true)
			{
				GetDealedOrderInfoVithPos(&tmp, stockcode, id, POS);
				if (tmp.size() == 0)
				{
					return true;
				}
				else
				{
					for (std::list<DealedOrder>::iterator it = tmp.begin(); it != tmp.end(); ++it)
					{
						pinfos->push_back(*it);
					}
					POS = pinfos->size();
				}
			}
			return true;

		}
		bool CHSTradeAdapter::GetDealedOrderInfoVithPos(std::list<DealedOrder>* pinfos, CONST TCHAR* stockcode, OrderID id, int Pos)
		{

			TCHAR pos[12] = { 0 };
			_stprintf(pos, _T("%d"), Pos);

			pinfos->clear();

			BeginJob(INQUIRY_KNOCKDOWN);
			AddFieldValue(L"version", L"1");
			AddFieldValue(L"op_branch_no", m_usrinfo.op_branch_no.c_str());
			AddFieldValue(L"op_entrust_way", m_usrinfo.op_entrust_way.c_str());
			AddFieldValue(L"op_station", m_usrinfo.op_station.c_str());
			AddFieldValue(L"branch_no", m_usrinfo.sbranch_no.c_str());
			AddFieldValue(L"fund_account", m_usrinfo.fund_account.c_str());
			AddFieldValue(L"password", m_usrinfo.password.c_str());
			AddFieldValue(L"exchange_type", L"");
			AddFieldValue(L"stock_account", m_usrinfo.stock_account.c_str());
			AddFieldValue(L"stock_code", stockcode);
			AddFieldValue(L"query_direction", L"");
			AddFieldValue(L"query_mode", L"0");//查询模式 '0' 明细 '1' 委托合并 '2' 证券汇总 '3' 按资金帐号汇总
			AddFieldValue(L"request_num", L"1000");
			AddFieldValue(L"position_str", pos);
			CommitJob();

			if (m_hsconn->ErrorNo != 0)
			{
				_setLastError();
				return false;
			}

			while (m_hsconn->Eof == 0)
			{
				DealedOrder order;

				order.serial_no = UMaths::ToInt32(GetFieldValW(L"serial_no").c_str());
				//	order.date = UMaths::ToInt32( GetFieldValW(L"date").c_str());
				//order.exchange_type = GetFieldValW(L"exchange_type");
				order.stock_account = UCodeHelper::ToTS(GetFieldValW(L"stock_account"));
				order.stock_code = UCodeHelper::ToTS(GetFieldValW(L"stock_code"));
				order.stock_name = UCodeHelper::ToTS(GetFieldValW(L"stock_name"));
				order.entrust_bs = (EBuySell)UMaths::ToInt32(GetFieldValW(L"entrust_bs").c_str());
				//		order.bs_name = GetFieldValW(L"bs_name");
				order.business_price = UMaths::ToDouble(GetFieldValW(L"business_price").c_str());
				order.business_amount = UMaths::ToDouble(GetFieldValW(L"business_amount").c_str());
				order.business_time = UMaths::ToInt32(GetFieldValW(L"business_time"));
				//		order.business_status = GetFieldValW(L"business_status");
				//order.status_name = GetFieldValW(L"status_name");
				//		order.business_type = GetFieldValW(L"business_type");
				///order.type_name = GetFieldValW(L"type_name");
				//		order.business_times = UMaths::ToInt32( GetFieldValW(L"business_times").c_str() );
				order.entrust_no = UMaths::ToInt32(GetFieldValW(L"entrust_no").c_str());;
				//		order.report_no = UMaths::ToInt32( GetFieldValW(L"report_no") .c_str());
				order.business_balance = UMaths::ToDouble(GetFieldValW(L"business_balance").c_str());

				m_hsconn->MoveBy(1);
				pinfos->push_back(order);

			}
			return true;
		}

		std::pair<int, OrderStatus> CHSTradeAdapter::GetOnlinedOrderInfo(OrderID id)
		{
			AUTOGARD();
			OrderStatus status = OrderStatus::AllDealed;
			BeginJob(INQUIRY_ENTRUST_STATUS);
			AddFieldValue(L"version", L"1");
			AddFieldValue(L"op_branch_no", m_usrinfo.op_branch_no.c_str());
			AddFieldValue(L"op_entrust_way", m_usrinfo.op_entrust_way.c_str());
			AddFieldValue(L"op_station", m_usrinfo.op_station.c_str());
			AddFieldValue(L"branch_no", m_usrinfo.sbranch_no.c_str());
			AddFieldValue(L"fund_account", m_usrinfo.fund_account.c_str());
			AddFieldValue(L"password", m_usrinfo.password.c_str());
			AddFieldValue(L"exchange_type", L"");//info.exchange_type.c_str());
			AddFieldValue(L"stock_account", m_usrinfo.stock_account.c_str());
			AddFieldValue(L"stock_code", L"");//info.stock_code.c_str());
			AddFieldValue(L"locate_entrust_no", UStrConvUtils::ToString(id).c_str()); //指定委托号，查指定委托号的委托，且request_num为1，且前台在收到应答后要检查返回的委托的委托号是否与要求查询的一致，此时exchange_type和stock_code要为空串。</param>
			AddFieldValue(L"query_type", L"0");  //查询类别，是否查委托类型为撤单的委托，0全部(默认)；1不查委托类型为撤单的委托</param>
			AddFieldValue(L"query_direction", L"0");//'1'--往前翻（顺序） 	 '0'--往后翻（倒序）
			AddFieldValue(L"sort_direction", L"0");//排序方式 '0' 正常 '1'倒序</param>
			AddFieldValue(L"request_num", L"");//请求行数，不送按50行处理，超过系统指定值（1000行）按系统指定值（1000行）处理</param>
			AddFieldValue(L"position_str", L"");
			AddFieldValue(L"action_in", L"0");//操作确认，1，则只查询可以撤单的委托；0及其他，则查询全部委托</param>
			CommitJob();

			if (m_hsconn->ErrorNo != 0)
			{
				_setLastError();
				return std::make_pair<int, OrderStatus>(0, Failed);
			}
			///-52	无此股票	（此错误可不作判断）
			///-60	没有委托记录
			///-61	查询委托失败	（其他错误）


			int sum = 0;
			if (m_hsconn->Eof == 0)
			{
				int num = 0;
				ResultSet res;
				res[L"_获取在途订单信息"] = L"";
				res[L"position_str"] = GetFieldValW(L"position_str");
				res[L"entrust_no"] = GetFieldValW(L"entrust_no");
				res[L"exchange_type"] = GetFieldValW(L"exchange_type");
				res[L"stock_account"] = GetFieldValW(L"stock_account");
				res[L"stock_code"] = GetFieldValW(L"stock_code");
				res[L"stock_name"] = GetFieldValW(L"stock_name");
				res[L"entrust_bs"] = GetFieldValW(L"entrust_bs");
				res[L"bs_name"] = GetFieldValW(L"bs_name");
				res[L"entrust_price"] = GetFieldValW(L"entrust_price");
				res[L"entrust_amount"] = GetFieldValW(L"entrust_amount");
				res[L"business_amount"] = GetFieldValW(L"business_amount");
				res[L"business_price"] = GetFieldValW(L"business_price");
				res[L"report_no"] = GetFieldValW(L"report_no");
				res[L"report_time"] = GetFieldValW(L"report_time");
				res[L"entrust_type"] = GetFieldValW(L"entrust_type");
				res[L"type_name"] = GetFieldValW(L"type_name");
				res[L"entrust_status"] = GetFieldValW(L"entrust_status");
				res[L"status_name"] = GetFieldValW(L"status_name");
				res[L"cancel_info"] = GetFieldValW(L"cancel_info");
				res[L"entrust_prop"] = GetFieldValW(L"entrust_prop");
				res[L"entrust_prop_name"] = GetFieldValW(L"entrust_prop_name");
				m_hsconn->MoveBy(1);
				if (res[L"status_name"] == L"待撤")
				{
					status = OrderStatus::CancelOnline;
				}

				num = UMaths::ToInt32(res[L"business_amount"].c_str());
				sum += num;
				GetLogStream() << res;
			}
			return std::make_pair<int, OrderStatus>(PAIR_WRAP(sum), PAIR_WRAP(status));
		}

		bool CHSTradeAdapter::ReConnect()
		{
			if (m_hsconn != NULL)
			{
				m_hsconn->DisConnect();
				m_hsconn = NULL;
			}
			m_hsconn->Connect();
			return false;
		}

		bool CHSTradeAdapter::QueryDealedOrders(std::list<DealedOrder>* pres)
		{
			pres->clear();

			bool ret = this->GetDealedOrderInfo(pres);
			return ret;

		}

		void CHSTradeAdapter::QueryAccountBasicInfo1()
		{

			if (m_accountInfo.ClientName != _T(""))
			{
				return;
			}
			AUTOGARD();

			BeginJob(USER_CONFIRM);
			AddFieldValue(L"version", L"1");
			AddFieldValue(L"op_branch_no", m_usrinfo.op_branch_no.c_str());
			AddFieldValue(L"op_entrust_way", m_usrinfo.op_entrust_way.c_str());
			AddFieldValue(L"op_station", m_usrinfo.op_station.c_str());
			AddFieldValue(L"branch_no", m_usrinfo.sbranch_no.c_str());
			AddFieldValue(L"input_content", L"1");
			AddFieldValue(L"password", m_usrinfo.password.c_str());
			AddFieldValue(L"content_type", L"0");
			AddFieldValue(L"account_content", m_usrinfo.usr.c_str());
			CommitJob();

			if (m_hsconn->ErrorNo != 0)
			{
				_setLastError();
				return;
			}
			m_accountInfo.FundAccount = UCodeHelper::ToTS(GetFieldValW(L"fund_account"));
			m_accountInfo.ClientName = UCodeHelper::ToTS(GetFieldValW(L"client_name"));
			m_accountInfo.MoneyType = _T("0");
			/*
			money_count = Convert.ToInt32(GetFieldValW(L"money_count"));
			money_type = GetFieldValW(L"money_type");
			client_rights = GetFieldValW(L"client_rights");
			exchange_type = GetFieldValW(L"exchange_type");
			stock_account = GetFieldValW(L"stock_account");
			}
			*/

		}


		void CHSTradeAdapter::QueryAccountBasicInfo2()
		{
			ResultSets res = QueryAccountCapital();
			if (res.size() > 0)
			{
				ResultSet first = *res.begin();
				m_accountInfo.CurrentBalance = UMaths::ToDouble(first[L"current_balance"]);
				m_accountInfo.EnableBalance = UMaths::ToDouble(first[L"enable_balance"]);
				m_accountInfo.FetchCash = UMaths::ToDouble(first[L"fetch_cash"]);
				m_accountInfo.AssetBalance = UMaths::ToDouble(first[L"asset_balance"]);
			}
		}

		void CHSTradeAdapter::QueryAccountBasicInfo3()
		{
			//查询股东
		}

		bool CHSTradeAdapter::QueryAccountInfo(BasicAccountInfo* info)
		{
			QueryAccountBasicInfo1();
			QueryAccountBasicInfo2();
			QueryAccountBasicInfo3();
			*info = m_accountInfo;
			return true;
		}



		bool CHSTradeAdapter::QueryPostions(std::list<PositionInfo>* presult, const TCHAR* stock)
		{
			AUTOGARD();
			presult->clear();
			BeginJob(INQUIRY_STOCK);

			AddFieldValue(L"version", L"1");
			AddFieldValue(L"op_branch_no", m_usrinfo.op_branch_no.c_str());
			AddFieldValue(L"op_entrust_way", m_usrinfo.op_entrust_way.c_str());
			AddFieldValue(L"op_station", m_usrinfo.op_station.c_str());
			AddFieldValue(L"branch_no", m_usrinfo.sbranch_no.c_str());
			AddFieldValue(L"fund_account", m_usrinfo.fund_account.c_str());
			AddFieldValue(L"password", m_usrinfo.password.c_str());
			AddFieldValue(L"exchange_type", L"");
			AddFieldValue(L"stock_account", m_usrinfo.stock_account.c_str());
			AddFieldValue(L"position_str", L"");
			if (stock == NULL)
			{
				AddFieldValue(L"stock_code", L"");
			}
			else
			{
				AddFieldValue(L"stock_code", stock);
			}

			AddFieldValue(L"query_direction", L"");
			AddFieldValue(L"query_mode", L"0"); //0明细，1汇总
			AddFieldValue(L"request_num", L"1000");
			CommitJob();


			if (m_hsconn->ErrorNo != 0)
			{
				_setLastError();
				return false;
			}

			while (m_hsconn->Eof == 0)
			{
				PositionInfo info;

				//		info.exchange_type = GetFieldValW(L"exchange_type");
				info.stock_account = GetFieldValT(L"stock_account");
				info.code = GetFieldValT(_T("stock_code"));
				info.stock_name = UCodeHelper::ToTS(GetFieldValW(L"stock_name"));
				info.current_amount = UMaths::ToDouble(GetFieldValW(L"current_amount"));
				info.enable_amount = UMaths::ToDouble(GetFieldValW(L"enable_amount"));
				info.last_price = UMaths::ToDouble(GetFieldValW(L"last_price"));
				info.cost_price = UMaths::ToDouble(GetFieldValW(L"cost_price"));
				info.income_balance = UMaths::ToDouble(GetFieldValW(L"income_balance"));
				info.hand_flag = FromHSStringToTradeUnit(GetFieldValT(L"hand_flag"));
				info.market_value = UMaths::ToDouble(GetFieldValT(L"market_value"));
				presult->push_back(info);
				m_hsconn->MoveBy(1);
			}
			return true;
		}


		bool CHSTradeAdapter::IsInitialized()
		{
			AUTOGARD();
			return m_hsconn != NULL && m_conneted == true;
		}

		CHSTradeAdapter::CHSTradeAdapter()
		{
			m_conneted = false;
		}

		bool CHSTradeAdapter::_cancelorder(OrderID orderid)
		{
			AUTOGARD();
			bool ret = false;
			BeginJob(ENTRUST_CANCEL);

			AddFieldValue(L"version", L"1");
			AddFieldValue(L"l_op_code", m_usrinfo.l_op_code.c_str());
			AddFieldValue(L"vc_op_password", m_usrinfo.vc_op_password.c_str());
			AddFieldValue(L"op_branch_no", m_usrinfo.op_branch_no.c_str());

			AddFieldValue(L"op_entrust_way", m_usrinfo.op_entrust_way.c_str());
			AddFieldValue(L"op_station", m_usrinfo.op_station.c_str());
			AddFieldValue(L"branch_no", m_usrinfo.sbranch_no.c_str());

			AddFieldValue(L"fund_account", m_usrinfo.fund_account.c_str());
			AddFieldValue(L"password", m_usrinfo.password.c_str());
			AddFieldValue(L"entrust_no", UStrConvUtils::ToString(orderid).c_str()); //注:TODO ,LIN ZOU ZHONGJI SHIYONG 2
			AddFieldValue(L"batch_flag", L"0");
			AddFieldValue(L"exchange_type", L"");

			CommitJob();

			ResultSet res;
			try
			{
				res[L"_取消订单:"] = L"\n";
				res[L"error_no"] = GetFieldValW(L"error_no");
				res[L"error_info"] = GetFieldValW(L"error_info");
				res[L"entrust_no"] = GetFieldValW(L"entrust_no");
				//	res[L"batch_no"] = GetFieldValW(L"batch_no"); //注：这个字段是自己填写
				GetLogStream() << res;

				tstring error_no = UCodeHelper::ToTS(res[L"error_no"]);
				int ner_no = UMaths::ToInt32(error_no.c_str());
				if (ner_no == -53)		// -53:“无此委托”            （包含此委托不是该股民的）文档不对
				{
					return true;
				}
				if (ner_no == 81501)		//     -54:“此委托不能撤单”      （包含其他错误） 
				{
					return true;
				}
			}
			catch (...)
			{

			}
			return true;
		}

		bool CHSTradeAdapter::QueryEntrustInfos(std::list<EntrustInfo>* pinfo)
		{
			
			SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter, -1, _T("not implement")));
			return false;
		}

		tstring CHSTradeAdapter::GetName()
		{
			return _T("HS Trade Adapter");
		}

		

		tstring CHSTradeAdapter::GetFieldValT(const wchar_t* key)
		{
#if defined(UNICODE) || defined(_UNICODE)
			return GetFieldValW(key);
#else
			return GetFieldValA(key);
#endif
		}

		tstring CHSTradeAdapter::GetFieldValT(const char* key)
		{
			auto wkey = UCodeHelper::ToWS(key);
			return GetFieldValT(wkey.c_str());
		}

	};


}