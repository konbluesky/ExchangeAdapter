#ifndef _GTA_SENDER_MGR_H_
#define _GTA_SENDER_MGR_H_

#include <string>
#include <map>
#include "threadlock.h"

class ACE_Stream;
namespace gta
{
	class CGTASender;
	class CGTATradeProvider;
	class CGTASenderMgr
	{
	public:
		CGTASenderMgr(std::string addr,int port,CGTATradeProvider* pProvider);
		std::shared_ptr<CGTASender> GetSender();
		std::shared_ptr<CGTASender> GetSpecialSender(std::string addr,int port);
		void RemoveSender(std::string key);
		
		///
		ACE_Stream* GetGTASharedStream();
		void SetGTASharedStream(ACE_Stream* pstream);

	private:
		std::map<std::string , std::shared_ptr<CGTASender> > m_map;
		std::string m_addr;
		int m_port;
		threadlock::multi_threaded_local m_lock;

		ACE_Stream* m_gtasharedstream;
		CGTATradeProvider* m_pProvider;
	};
}
#endif