#include <stdarg.h>
#include "StringBuilder.h"

namespace utils
{
	CStringBuilder::CStringBuilder(char* pbuff)
	{
		m_pbuff = pbuff;
		m_currentindex = 0;
	}


	CStringBuilder::~CStringBuilder(void)
	{
	}

	char* CStringBuilder::GetBuffer() const
	{
		return m_pbuff;
	}

	CStringBuilder& CStringBuilder::AppendFormat( const char* fmt,... )
	{
		va_list pArg;
		va_start(pArg, fmt);
		vsprintf(&m_pbuff[m_currentindex], fmt, pArg);
		va_end(pArg);
		m_currentindex += strlen(&m_pbuff[m_currentindex]);
		return *this;
	}

	std::string CStringBuilder::Format( const char* fmt )
	{
		char buff[1024*8] = {0};
		CStringBuilder cb(buff);
		cb.AppendFormat(fmt);
		return buff;
	}

	int CStringBuilder::GetIndex() const
	{
		return m_currentindex;
	}

}