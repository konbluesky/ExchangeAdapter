/***********************************************************************
 * Module:  CCrypticDES.cpp
 * Author:  刘青勇
 * Modified: 2007年9月10日
 * Purpose: Implementation of the class CCrypticDES
 * Comment: 对明文数据进行加密工作，密文数据进行解密工作，
 ***********************************************************************/

#include <stdio.h>
#include <windows.h>

#include "CCrypticDES.h"
#include "des.h"


namespace utils

{




CCrypticDES::CCrypticDES()
{
	char szBuffer[128];
	memset(m_szKey, 0, sizeof(m_szKey));
	memset(m_szSaveKey, 0, sizeof(m_szSaveKey));
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	strcpy(szBuffer, "12345678");
	rv_MD5_BYTE(8, (BYTE*)szBuffer, (BYTE*)m_szSaveKey);
	m_lKeyLen = 0;
}

CCrypticDES::~CCrypticDES()
{}

/**********************************************************
* 函 数 名： CCrypticDES::rpsz_GetErrMsg
* 功能描述：返回错误信息
* 返回说明： 返回错误信息
* 开发历史：liuqy   于 20070910 创建
************************************************************/

char * CCrypticDES::rpsz_GetErrMsg(void)
{
	// TODO : implement
	return m_szErrMsg;
}
/**********************************************************
* 函 数 名： CCrypticDES::rl_SetKey
* 功能描述：设置系统当前工作密钥
* 注意：加密数据为：xxMMM,其中xx表示MMM的长度，MMM的原始数据数据，密钥只取MMM
*参数：
*- in_pszCryticKey      输入 --是经过保存方式加过密的钥密，是ASCII字符串
* 返回说明： 成功--0，失败--小于0
* 开发历史：liuqy   于 20070910 创建
************************************************************/
long	CCrypticDES::rl_SetKey(const char * in_pszCryticKey)
{
	long lLen;
	long lOutLen;
	char szBuffer[256]	= {0};
	char szTmp[256]		= {0};
	//判断长度是否有效
	lLen = strlen(in_pszCryticKey);
	if (0 != lLen % 8)
	{
		sprintf(m_szErrMsg, "设置系统工作密钥长度[%ld]必须是8的倍数", lLen);
		return -1;
	}
	lOutLen = 0;
	//memset(szBuffer, 0, sizeof(szBuffer));	2011-4-8 为提升速度优化掉 by zhugp
	lLen = 0;
	//先将ASCII转化为BYTE
	rv_ASC2BYTE(in_pszCryticKey, &lLen, (BYTE*)szBuffer);
	//memset(szTmp, 0, sizeof(szTmp));			2011-4-8 为提升速度优化掉 by zhugp
	//解密
	if (rl_DESDec_BYTE(16, (BYTE*)m_szSaveKey, lLen, (BYTE*)szBuffer, &lOutLen, (BYTE*)szTmp))
	{
		
		return -1;
	}
	//得到明文长度
	memset(szBuffer, 0, 4);
	memcpy(szBuffer, szTmp, 2);
	m_lKeyLen = atol(szBuffer);
	if (8 != m_lKeyLen && 16 != m_lKeyLen && 32 != m_lKeyLen)
	{
		sprintf(m_szErrMsg, "设置系统工作密钥长度[%ld]必须是8、16或32位数", m_lKeyLen);
		m_lKeyLen = 0;
		return -1;
	}
	//得到密钥
	memcpy(m_szKey, &szTmp[2], m_lKeyLen);
	return 0;


}
/**********************************************************
* 函 数 名： CCrypticDES::rl_EncryptPwd
* 功能描述：对明文数据进行加密，加密使用rl_SetKey设置的密钥进行加密
* 注意：加密数据为：xxMMM,其中xx表示MMM的长度，MMM的原始数据数据,即待加密数据
*参数：
*- in_pszGram      输入 --待加密的明文数据最长不能超过64，是ASCII字符串
*- out_pszRes      输出 --加密后的返回密文数据的指针，空间需要分配，系统将BYTE转换为ASCII字符串
*- in_loutResSize	输入 --返回密文数据指向的空间大小
* 返回说明： 成功--加密后数据长度，失败--小于0
* 开发历史：liuqy   于 20070910 创建
************************************************************/
long CCrypticDES::rl_EncryptPwd(char *in_pszGram, char *out_pszRes, long in_loutResSize)
{
	long lLen;
	long lOutLen;
	char szBuffer[256]	= {0};
	char szTmp[256];
	//检查加密数据长度
	lLen = strlen(in_pszGram);
	if (64 < lLen)
	{
		strcpy(m_szErrMsg, "等待加密保存的数据不能超过64位");
		return -1;
	}
	if (0 == m_lKeyLen)
	{
		strcpy(m_szErrMsg, "还没有设置工作密钥");
		return -1;
	}
	//组装原始数据：xxMMM,其中xx表示MMM的长度，MMM的原始数据数据,即待加密数据
	sprintf(szTmp, "%02d%s", lLen, in_pszGram);
	lLen += 2;

	lOutLen = 0;
	//memset(szBuffer, 0, sizeof(szBuffer));	2011-4-8 为提升速度优化掉 by zhugp
	if (rl_DESEnc_BYTE(m_lKeyLen, (BYTE*)m_szKey, lLen, (BYTE*)szTmp, &lOutLen, (BYTE*)szBuffer))
	{
		
		return -1;
	}
	lLen = lOutLen * 2;
	if (lLen >= in_loutResSize)
	{
		sprintf(m_szErrMsg, "返回保存数据的空间需要[%ld]，空间[%ld]不够", lLen, in_loutResSize);
		return -1;
	}
	memset(out_pszRes, 0, in_loutResSize);
	rv_BYTE2ASC(lOutLen, (BYTE*)szBuffer, out_pszRes);
	return 0;
}
/**********************************************************
* 函 数 名： CCrypticDES::rl_DecryptPwd
* 功能描述：对密文的ASCII数据进行解密，加密使用rl_SetKey设置的密钥进行解密
* 注意：加密数据为：xxMMM,其中xx表示MMM的长度，MMM的原始数据数据，解密后只返回MMM
*参数：
*- in_pszGram      输入 --待解密的密文数据最长不能超过128，是ASCII字符串
*- out_pszRes      输出 --解密后的返回明文数据的指针，空间需要分配，系统将返回原加密状态的数据
*- in_loutResSize	输入 --返回明文数据指向的空间大小
* 返回说明： 成功--0，失败--小于0
* 开发历史：liuqy   于 20070910 创建
************************************************************/
long CCrypticDES::rl_DecryptPwd(char *in_pszGram, char *out_pszRes, long in_loutResSize)
{
	long lLen;
	long lOutLen;
	char szBuffer[256]	= {0};
	char szTmp[256]		= {0};
	//判断长度是否有效
	lLen = strlen(in_pszGram);
	if (128 < lLen )
	{
		sprintf(m_szErrMsg, "待解密的密文数据长度[%ld]最长不能超过128", lLen);
		return -1;
	}
	if (0 == m_lKeyLen)
	{
		strcpy(m_szErrMsg, "还没有设置工作密钥");
		return -1;
	}
	lOutLen = 0;
	//memset(szBuffer, 0, sizeof(szBuffer));	2011-4-8 为提升速度优化掉 by zhugp
	lLen = 0;
	//先将ASCII转化为BYTE
	rv_ASC2BYTE(in_pszGram, &lLen, (BYTE*)szBuffer);
	//memset(szTmp, 0, sizeof(szTmp));			2011-4-8 为提升速度优化掉 by zhugp
	//解密
	if (rl_DESDec_BYTE(m_lKeyLen, (BYTE*)m_szKey, lLen, (BYTE*)szBuffer, &lOutLen, (BYTE*)szTmp))
	{
		sprintf(m_szErrMsg, "解密数据失败");
		return -1;
	}
	//得到明文长度
	memset(szBuffer, 0, 4);
	memcpy(szBuffer, szTmp, 2);
	lLen = atol(szBuffer);
	//判断空间是否够存放
	if (lLen >= in_loutResSize)
	{
		sprintf(m_szErrMsg, "返回保存数据的空间需要[%ld]，空间[%ld]不够", lLen+1, in_loutResSize);
		return -1;
	}
	memset(out_pszRes, 0, in_loutResSize);
	//得到数据
	memcpy(out_pszRes, &szTmp[2], lLen);
	return 0;

}
/**********************************************************
* 函 数 名： rl_GetDataForSave
* 功能描述：对需要保存到参数文件中的明文ASCII数据进行加密，加密使用保存所有的密钥进行加密
* 注意：加密数据为：xxMMM,其中xx表示MMM的长度，MMM的原始数据数据,即待加密的保存数据
*参数：
*- in_pszGram      输入 --待保存的明文数据最长不能超过64，是ASCII字符串
*- out_pszRes      输出 --加密后的返回保存的密文数据的指针，空间需要分配
*- in_loutResSize	输入 --返回密文数据指向的空间大小
* 返回说明： 成功--0，失败--小于0
* 开发历史：liuqy   于 20070910 创建
************************************************************/
long CCrypticDES::rl_GetDataForSave(char *in_pszGram, char *out_pszRes, long in_loutResSize)
{
	long lLen;
	long lOutLen;
	char szBuffer[256]	= {0};
	char szTmp[256];
	//检查加密数据长度
	lLen = strlen(in_pszGram);
	if (64 < lLen)
	{
		strcpy(m_szErrMsg, "等待加密保存的数据不能超过64位");
		return -1;
	}
	lOutLen = 0;
	//memset(szBuffer, 0, sizeof(szBuffer));	2011-4-8 为提升速度优化掉 by zhugp
	//组装原始数据：xxMMM,其中xx表示MMM的长度，MMM的原始数据数据,即待加密数据
	sprintf(szTmp, "%02d%s", lLen, in_pszGram);
	lLen += 2;
	if (rl_DESEnc_BYTE(16, (BYTE*)m_szSaveKey, lLen, (BYTE*)szTmp, &lOutLen, (BYTE*)szBuffer))
	{
		sprintf(m_szErrMsg, "加密数据失败");
		return -1;
	}
	lLen = lOutLen * 2;
	if (lLen >= in_loutResSize)
	{
		sprintf(m_szErrMsg, "返回保存数据的空间需要[%ld]，空间[%ld]不够", lLen+1, in_loutResSize);
		return -1;
	}
	memset(out_pszRes, 0, in_loutResSize);
	rv_BYTE2ASC(lOutLen, (BYTE*)szBuffer, out_pszRes);
	return 0;

}
/**********************************************************
* 函 数 名： CCrypticDES::rl_GetDataFromSave
* 功能描述：对保存的密文的ASCII数据进行解密
* 注意：加密数据为：xxMMM,其中xx表示MMM的长度，MMM的原始数据数据，解密后只返回MMM
*参数：
*- in_pszGram      输入 --待解密的密文数据最长不能超过128，是ASCII字符串
*- out_pszRes      输出 --解密后的返回明文数据的指针，空间需要分配，系统将返回原加密状态的数据
*- in_loutResSize	输入 --返回明文数据指向的空间大小
* 返回说明： 成功--0，失败--小于0
* 开发历史：liuqy   于 20070910 创建
************************************************************/
long CCrypticDES::rl_GetDataFromSave(char *in_pszGram, char *out_pszRes, long in_loutResSize)
{
	long lLen;
	long lOutLen;
	char szBuffer[256]	= {0};
	char szTmp[256]		= {0};
	//判断长度是否有效
	lLen = strlen(in_pszGram);
	if (128 < lLen )
	{
		sprintf(m_szErrMsg, "待解密的密文数据长度[%ld]最长不能超过128", lLen);
		return -1;
	}
	lOutLen = 0;
	//memset(szBuffer, 0, sizeof(szBuffer));	2011-4-8 为提升速度优化掉 by zhugp
	lLen = 0;
	//先将ASCII转化为BYTE
	rv_ASC2BYTE(in_pszGram, &lLen, (BYTE*)szBuffer);
	//memset(szTmp, 0, sizeof(szTmp));			2011-4-8 为提升速度优化掉 by zhugp
	//解密
	if (rl_DESDec_BYTE(16, (BYTE*)m_szSaveKey, lLen, (BYTE*)szBuffer, &lOutLen, (BYTE*)szTmp))
	{
		sprintf(m_szErrMsg, "解密数据失败");
		return -1;
	}
	//得到明文长度
	memset(szBuffer, 0, 4);
	memcpy(szBuffer, szTmp, 2);
	lLen = atol(szBuffer);
	//判断空间是否够存放
	if (lLen >= in_loutResSize)
	{
		sprintf(m_szErrMsg, "返回保存数据的空间需要[%ld]，空间[%ld]不够", lLen+1, in_loutResSize);
		return -1;
	}
	memset(out_pszRes, 0, in_loutResSize);
	//得到数据
	memcpy(out_pszRes, &szTmp[2], lLen);
	return 0;
}
}