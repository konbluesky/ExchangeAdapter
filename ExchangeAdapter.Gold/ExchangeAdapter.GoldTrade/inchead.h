/******************************************************************************
	Copyright(C)   TechInfo Co., Ltd. All Right Reserved
	文 件 名：mybase.h
	文件内容：
	版本信息：当前版本MYBASE-1.00-00XXX
	编 写 者：刘青勇
	时间地点：200604  于成都
	开发历史：
									
 ******************************************************************************/
#ifndef	_INCHEAD_H
#define	_INCHEAD_H

/******************************************************************************
	头文件引用区域
 ******************************************************************************/
#include	<time.h>
#include	<fcntl.h>
#include	<errno.h>
#include	<stdio.h>
#include	<ctype.h>
#include	<stdarg.h>
#include	<setjmp.h>
#include	<signal.h>
#include	<stdlib.h>
#include	<string.h>
#include	<malloc.h>
#include	<memory.h>
#include	<assert.h>
#include	<sys/types.h>
#include	<sys/timeb.h>
#include	<sys/stat.h>

#if	defined(MS_WINDOWS) || defined(WIN32)
#include	<io.h>
#include	<direct.h>
#include	<sys/stat.h>
#include 	<process.h>
#include 	<windows.h>
#endif

#if	defined(SCO_OPENSERVER) || defined(IBM_AIX) || defined(HPUX)|| defined(IBM_AIX64) 
#include	<math.h>
#include	<netdb.h>
#include	<unistd.h>
#include	<termio.h>
#include	<sys/ipc.h>
#include	<sys/sem.h>
#include	<sys/msg.h>
#include	<sys/shm.h>
#include    <sys/uio.h>
#include    <sys/mman.h>
#include	<sys/errno.h>
#include	<sys/socket.h>
#include	<netinet/in.h>
#include	<arpa/inet.h>
#include	<extension.h>
#endif

#if	defined(IBM_AIX) || defined(HPUX)
#include	<sys/sem.h>
#endif

#if	defined(SCO_OPENSERVER)
#include	<prototypes.h>
#include	<sys/itimer.h>
#include	<sys/sudstime.h>
#include	<sys/semaphore.h>
#endif

#endif
