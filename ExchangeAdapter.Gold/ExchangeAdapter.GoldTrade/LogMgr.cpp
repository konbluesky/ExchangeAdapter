#include <cstdio>
#include <iostream>
#include <map>
#include "BufferPool.h"
#include "sharedqueue.h"
#define TIXML_USE_STL
#include "tinyxml.h"
#include "AnyCast.h"
#include "util.h"
#include "StringBuilder.h"
#include "LogMgr.h"

namespace utils
{

	
	ILog::LogType utils::ILog::ParseString( const char* str )
	{
		static std::map<std::string,LogType> _map;
		static bool _loaded = false;
		if(!_loaded)
		{		
			_map["none"] = TNone;
			_map["debug"] = TDebug;
			_map["log"] = TLog;
			_map["success"] = TSuccess;
			_map["warning"] = TWarning;
			_map["error"] = TError;
			_map["all"] = TALL;
			_loaded = true;
		}
		auto it = _map.find(str);
		if(it != _map.end())
			return _map[str];
		else
			return TNone;
	}

	
	//static utils::CBufferPool<threadlock::multi_threaded_local> _CLoggerBufferPool(1024);

	struct LogInfo
	{
		FILE* _fp;
		char* _pdata;
		int _datalen;
		struct LogInfo() : _fp(NULL),_pdata(NULL),_datalen(0)
		{
		}


	};


	///如果构造函数不指定关心的类型，则使用默认的监听所有类型
	class CLogger : public ILog
	{
	public:
		friend class  CLogMgr;

		CLogger(CLogMgr* pMgr,const std::string& fname ,DWORD favoritetypes = ILog::TALL)
		{
			m_fp = NULL;
			m_favorites = favoritetypes;
			m_fname = fname;
			m_plogmgr = pMgr;
		}

		void SetFavorite(DWORD type)
		{
			m_favorites = type;
		}
		~CLogger()
		{
			Close();
		}
		void Close()
		{
			if (m_fp != NULL)
			{
				fclose(m_fp);
				m_fp = NULL;
			}
		}

		virtual DWORD GetFavorite()
		{
			return m_favorites;
		}
		void setFILE()
		{
			//TODO 这里有个bug，在多线程下_fp可能出问题，需要用double-lock
			if(m_fp == NULL)
			{
				threadlock::autoguard<threadlock::multi_threaded_local> _autolock(&_CLogger_lock);
				if(m_fp == NULL)
				{
					if(m_fname == CLogMgr::STDOUT)
					{
						m_fp = stdout;
					}
					else if(m_fname == CLogMgr::STDERR)
					{
						m_fp = stderr;
					}
					else
					{
						m_fp = fopen(m_fname.c_str(),"a+");
						if(m_fp == NULL)
						{
							m_fp = stderr;
						}
					}
				}	
			}
		}

		virtual void WriteLog(LogType type, char* data, int len)
		{
			if(type & m_favorites)
			{	
				setFILE();

				LogInfo info;
				info._datalen = len;
				info._fp = m_fp;
				info._pdata = data;

				_datas.Enqueue(info);

				OpenThreadWork();
			}
		}

	private:
		FILE* m_fp;
		std::string m_fname;
		DWORD m_favorites;
		CLogMgr* m_plogmgr;

	private:
		void OpenThreadWork()
		{
			if(m_plogmgr->m_hWorkthreadhandler == NULL)
			{
				threadlock::autoguard<threadlock::multi_threaded_local> _autolock(&_CLogger_lock);
				if(m_plogmgr->m_hWorkthreadhandler == NULL)
				{				
					m_plogmgr->m_hWorkthreadhandler = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)WriteThread, this->m_plogmgr, 0, NULL);
					if(m_plogmgr->m_hWorkthreadhandler == NULL)
						std::cout<<"Crate Write Log Work Thread Failed";
				}
			}	
		}

		static DWORD WriteThread(void* pParam)
		{
			CLogMgr* pmgr = (CLogMgr*)pParam;
			while(true)
			{
				LogInfo data = _datas.Dequeue();
				try
				{
					if(data._pdata == NULL)
						break;

					fwrite(data._pdata,data._datalen,1,data._fp);
					fflush(data._fp);
				}
				catch (...)
				{
					std::cout<<"Write Log Failed"<<std::endl;
				}
				if(pmgr != NULL)
				{
					pmgr->GetBufferPool()->Push(data._pdata);
				}
			}
			return 0;
		}

	private:
		static BlockableQueue<LogInfo,threadlock::multi_threaded_local> _datas;
		static threadlock::multi_threaded_local _CLogger_lock;
	};

	
	BlockableQueue<LogInfo,threadlock::multi_threaded_local> CLogger::_datas;
	threadlock::multi_threaded_local  CLogger::_CLogger_lock;




	CLogMgr::CLogMgr()
	{
		m_hWorkthreadhandler = NULL;
		m_pBufferPool = NULL;
		m_currentdate = utils::UTimeHelper::GetCurrentFormatDate();		
	}

	CLogMgr::~CLogMgr()
	{
		Close();
	}

	void CLogMgr::Close()
	{
		threadlock::autoguard<threadlock::multi_threaded_local> autoguard(&m_lock);

		CloseThreadWork();
		for(std::map<std::string,ILog*>::iterator it = _logmap.begin(); it != _logmap.end(); ++it)
		{
			delete it->second;
		}
		_logmap.clear();
		if(this->m_pBufferPool != NULL)
		{
			m_pBufferPool->Close();
			delete m_pBufferPool;
		}

	}


	
	ILog& CLogMgr::GetLog(const std::string& fname )
	{
		threadlock::autoguard<threadlock::multi_threaded_local> _autoguad(&m_lock);

		///注：这里需要考虑字符的大小写,如果混用则_logmap是找不到的。
		std::string fpath = utils::UStringHelper::ToLower(getlogfilepath(fname));
		auto f = _logmap.find(fpath);
		if(f != _logmap.end())
		{
			return *f->second;
		}
		else
		{
			CLogger* p = new CLogger(this,fpath);
			_logmap[fpath] = p;
			return *p;
		}
	}

	bool CLogMgr::InitializeByXML(const std::string& fname )
	{
		TiXmlDocument doc(fname);

		bool loadOkay = doc.LoadFile();
		if (!loadOkay)
		{
			std::cout<<"加载log配置文件" << fname << "出错" << std::endl;
			return false;
		}

		TiXmlNode* pNode = doc.FirstChild("configure");
		TiXmlNode* ele = pNode->FirstChild("logdir");
		m_logdir = ele->FirstChild()->Value();

		ele = pNode->FirstChild("bufferlen");
		if(m_pBufferPool!=NULL)
		{
			delete m_pBufferPool;
		}

		m_pBufferPool = new CThreadSafeBuffPool(atoi(ele->FirstChild()->Value()));


		ele = pNode->FirstChild();

		while(ele != NULL)
		{

			TiXmlNode* datanode;
			if(ele->ValueStr() == "logfile")
			{
				datanode = ele->FirstChild("filename");	
				ILog& log = this->GetLog(datanode->FirstChild()->Value());

				datanode = ele->FirstChild("favorites");
				auto fs = utils::UStringHelper::Spilt(datanode->FirstChild()->Value(),",");
				DWORD type = ILog::TNone;

				for(auto it = fs.begin(); it != fs.end(); ++it)
				{
					type |= ILog::ParseString(it->c_str());
				}
				log.SetFavorite(type);				
			}
			ele = ele->NextSibling();
		}

		return true;
	}


	std::string CLogMgr::getlogfilepath(std::string fname )
	{
		if(fname == CLogMgr::STDOUT || fname == CLogMgr::STDERR)
		{
			return fname;
		}
		else
		{
			return utils::UFileSystemHelper::GetAppDirectory() + m_logdir + m_currentdate +"."+ fname + ".LOG";
		}
	}

	CLogMgr& CLogMgr::Instance()
	{
		static CLogMgr instance;
		return instance;
	}

	CBufferPool<threadlock::multi_threaded_local>* CLogMgr::GetBufferPool()
	{
		if(m_pBufferPool == NULL)
		{
			threadlock::autoguard<threadlock::multi_threaded_local> _autoguad(&m_lock);
			if(m_pBufferPool == NULL)
			{
				m_pBufferPool = new CBufferPool<threadlock::multi_threaded_local>(1024);
			}			
		}
		return m_pBufferPool;		
	}

	void CLogMgr::CloseThreadWork()
	{
		if (m_hWorkthreadhandler)
		{
			TerminateThread(m_hWorkthreadhandler,0);
			m_hWorkthreadhandler = NULL;			
		}
	}

	const char* CLogMgr::STDERR = "stderr";
	const char* CLogMgr::STDOUT = "stdout";	
}