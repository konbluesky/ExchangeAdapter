#ifndef _TRADE_API_HANDLER_H_
#define _TRADE_API_HANDLER_H_
#include <string>
#include <vector>
#include <map>
#include "threadlock.h"
#include "TraderApi.h"
#include "LogHelper.h"

namespace gta
{
	typedef struct _tNtfQuotation
	{
		char	szInstID[8];
		long lLastNtfTimeSecond;
		_tNtfQuotation(std::string SZINSTID,long LLASTNTFTIMESECOND): lLastNtfTimeSecond(LLASTNTFTIMESECOND)
		{
			memset(szInstID,0,sizeof(szInstID));
		    strcpy(szInstID,SZINSTID.c_str());
		}

	}TNtfQuotation;


	class CGTATradeProvider;

	class CGTATradeApiHandler : public CTraderApi
	{	
	public: 
		struct InitParam
		{
			bool DemoMode;
			std::vector<std::pair<std::string,int> > addrs;
			std::string GoldICPWD;
			short ICMode;
			short ICComID;
			std::vector<std::string> SpotInstIDList;
			std::vector<std::string> DeferInstIDList;

			InitParam()
			{
				DemoMode = false;
			}
		};

		CGTATradeApiHandler();
		bool Initialize(InitParam* p);
		void Unitialize();
		bool InitializeByXML(std::string fname);
		//注：对发起的交易请求都需要加lock使顺序进行
		int Request(std::string tradecode,void* data1,std::string rootkey);

		///
		int ReqDemo(void* pdata,char* pkey);

		bool SetDataProcessor(CGTATradeProvider* Processor);//他和从私有变量取一个数据的效果一样？？？？


		long	m_lNtfQuoationWaitSecond;

		long    m_lDeferNtfQuoationWaitSecond;

		int m_DeliveryQuotation;

		std::string m_szBeginUnlimited1;
		std::string m_szEndUnlimited1;	
		std::string m_szBeginUnlimited2;
		std::string m_szEndUnlimited2;
		std::string m_szBeginUnlimited3;
		std::string m_szEndUnlimited3;

		std::vector<TNtfQuotation>& GetSpotQuotation();
		std::vector<TNtfQuotation>& GetDeferQuotation();
	

	private:
		void InitMap();
	private:

		std::vector<TNtfQuotation>  m_vNtfSpotQuotation;

		std::vector<TNtfQuotation>  m_vNtfDeferQuotation;		
		typedef  int (CTraderApi::*PFUNC)(void * , char * );
		std::map<std::string,PFUNC> m_map;
		threadlock::multi_threaded_local m_lock;
		CGTATradeProvider* pProvider;
	private:
		int  m_LostChannelTimes;
		time_t m_lastLostChannelTime;

	public:
		/**********************************************************
		* 函 数 名： onChannelLost
		* 功能描述：   返回与前置连接断开时的错误信息
		* 参数：
		*- in_pszErrMsg      输入 --返回错误信息
		* 返回说明： 无
		************************************************************/
		virtual void onChannelLost(char * in_pszErrMsg);
		/**********************************************************
		* 函 数 名： onException
		* 功能描述：   返回应用的错误信息
		* 参数：
		*- in_pszErrMsg      输入 --返回错误信息
		* 返回说明： 无
		************************************************************/
		virtual void onException(char * in_pszErrMsg);
		/**********************************************************
		* 函 数 名： onRecvSysErrorNtf
		* 功能描述：   返回系统错误通知信息
		* 参数：
		*- in_pszErrMsg      输入 --返回错误信息
		* 返回说明： 无
		************************************************************/
		virtual void onRecvSysErrorNtf(char * in_pszErrMsg);
		/**********************************************************
		* 函 数 名： GetTradeDate
		* 功能描述：   获取当前的交易日期
		* 参数：
		*- in_pszInMessage      输入 --发送报文
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		void GetTradeDate(char * in_pszInMessage, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRspTraderLogin
		* 功能描述：   交易员登录应答
		* 参数：
		*- in_pstReqData      输入 --交易员登录请求信息
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual void onRecvRspTraderLogin(TReqTraderLogin * in_pstReqData, TTRspMsg * in_pstRspMsg,char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRspTraderLogout
		* 功能描述：   交易员登出应答
		* 参数：
		*- in_pstReqData      输入 --交易员登出请求信息
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual void onRecvRspTraderLogout(TReqTraderLogout * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRspSpotOrder
		* 功能描述：   现货报单应答
		* 参数：
		*- in_pstReqData      输入 --现货报单信息
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual void onRecvRspSpotOrder(TSpotOrder * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRspSpotOrderCancel
		* 功能描述：   现货撤单应答
		* 参数：
		*- in_pstReqData      输入 --现货撤单请求信息
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspSpotOrderCancel(TSpotOrderCancel * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRspDeferOrder
		* 功能描述：   现货延期交收报单应答
		* 参数：
		*- in_pstReqData      输入 --现货延期交收报单信息
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspDeferOrder(TDeferOrder  * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRspDeferOrderCancel
		* 功能描述：   现货延期交收撤单应答
		* 参数：
		*- in_pstReqData      输入 --现货延期交收撤单请求信息
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspDeferOrderCancel(TDeferOrderCancel  * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRspDeferDeliveryAppOrder
		* 功能描述：   现货延期交收交割申报应答
		* 参数：
		*- in_pstReqData      输入 --现货延期交收交割申报报单
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspDeferDeliveryAppOrder(TDeferDeliveryAppOrder  * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);;
		/**********************************************************
		* 函 数 名： onRecvRspDeferDeliveryAppOrderCancel
		* 功能描述：   现货延期交收交割申报撤消应答
		* 参数：
		*- in_pstReqData      输入 --现货延期交收交割申报撤单请求
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspDeferDeliveryAppOrderCancel(TDeferDeliveryAppOrderCancel * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRspMiddleAppOrder
		* 功能描述：   现货延期交收中立仓申报应答
		* 参数：
		*- in_pstReqData      输入 --现货延期交收中立仓申报报单
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspMiddleAppOrder(TMiddleAppOrder * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRspMiddleAppOrderCancel
		* 功能描述：   现货延期交收中立仓申报撤单应答
		* 参数：
		*- in_pstReqData      输入 --现货延期交收中立仓申报报单
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspMiddleAppOrderCancel(TMiddleAppOrderCancel * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRspFuturesOrder
		* 功能描述：   期货报单应答
		* 参数：
		*- in_pstReqData      输入 --期货报单
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspFuturesOrder(TFuturesOrder * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRspFuturesOrderCancel
		* 功能描述：   期货撤单应答
		* 参数：
		*- in_pstReqData      输入 --期货撤消报单
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspFuturesOrderCancel(TFuturesOrderCancel * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);


		/**********************************************************
		* 函 数 名： onRecvRspQryDeferLocalOrder
		* 功能描述：   现货延期交收本地报单号查询应答
		* 参数：
		*- in_pstReqData      输入 --现货延期交收报单信息
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspQryDeferLocalOrder(TDeferOrder  * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRspQryForwardLocalOrder
		* 功能描述：   现货T+N本地报单号查询应答
		* 参数：
		*- in_pstReqData      输入 --现货T+N报单
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspQryForwardLocalOrder(TForwardOrder * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);


		/**********************************************************
		* 函 数 名： onRecvRspForwardOrder
		* 功能描述：   现货T+N报单应答
		* 参数：
		*- in_pstReqData      输入 --现货T+N报单
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspForwardOrder(TForwardOrder * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRspForwardOrderCancel
		* 功能描述：   现货T+N撤单应答
		* 参数：
		*- in_pstReqData      输入 --现货T+N撤单
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspForwardOrderCancel(TForwardOrderCancel* in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRspForwardEndorseOrder
		* 功能描述：   现货T+N到期转让报单应答
		* 参数：
		*- in_pstReqData      输入 --现货T+N到期转让报单信息
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspForwardEndorseOrder(TSpotOrder * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRspForwardEndorseOrderCancel
		* 功能描述：   现货T+N到期转让撤单应答
		* 参数：
		*- in_pstReqData      输入 --现货T+N到期转让撤单请求
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspForwardEndorseOrderCancel(TForwardEndorseOrderCancel * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRspTraderPasswordUpdate
		* 功能描述：   交易员密码修改应答
		* 参数：
		*- in_pstReqData      输入 --交易员密码修改请求
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspTraderPasswordUpdate(TReqTraderPasswordUpdate * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRspMemberUrgentPasswordUpdate
		* 功能描述：   会员应急密码修改应答
		* 参数：
		*- in_pstReqData      输入 --会员应急密码修改请求
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspMemberUrgentPasswordUpdate(TReqMemberUrgentPasswordUpdate * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRspQryMemberCapital
		* 功能描述：   会员资金查询应答
		* 参数：
		*- in_pstReqData      输入 --会员资金
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspQryMemberCapital(TMemberCapital * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRspQryClientStorage
		* 功能描述：   客户库存查询应答
		* 参数：
		*- in_pstReqData      输入 --客户库存
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspQryClientStorage(TClientStorage * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRspQrySpotOrder
		* 功能描述：   现货报单查询应答
		* 参数：
		*- in_pstReqData      输入 --现货报单信息
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspQrySpotOrder(TSpotOrder * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRspQryDeferOrder
		* 功能描述：   现货延期交收报单查询应答
		* 参数：
		*- in_pstReqData      输入 --现货延期交收报单
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspQryDeferOrder(TDeferOrder * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRspQryDeferDeliveryAppOrder
		* 功能描述：   现货延期交收交割申报报单查询应答
		* 参数：
		*- in_pstReqData      输入 --现货延期交收交割申报报单
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual void onRecvRspQryDeferDeliveryAppOrder(TDeferDeliveryAppOrder * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRspQryMiddleAppOrder
		* 功能描述：   现货延期交收中立仓申报报单查询应答
		* 参数：
		*- in_pstReqData      输入 --现货延期交收中立仓申报报单
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual void onRecvRspQryMiddleAppOrder(TMiddleAppOrder * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRspQryFuturesOrder
		* 功能描述：   期货报单查询应答
		* 参数：
		*- in_pstReqData      输入 --期货报单
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspQryFuturesOrder(TFuturesOrder * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRspQryForwardOrder
		* 功能描述：   现货T+N报单查询应答
		* 参数：
		*- in_pstReqData      输入 --现货T+N报单
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspQryForwardOrder(TForwardOrder * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRspQryForwardEndorseOrder
		* 功能描述：   现货T+N到期转让报单查询应答
		* 参数：
		*- in_pstReqData      输入 --现货报单信息
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspQryForwardEndorseOrder(TSpotOrder * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRspQrySpotMatch
		* 功能描述：   现货成交单查询应答
		* 参数：
		*- in_pstReqData      输入 --现货成交单信息
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual void onRecvRspQrySpotMatch(TSpotMatch * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRspQryDeferMatch
		* 功能描述：   现货延期交收成交单查询应答
		* 参数：
		*- in_pstReqData      输入 --现货成交单信息
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspQryDeferMatch(TDeferMatch * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRspQryDeferDeliveryAppMatch
		* 功能描述：   现货延期交收交割申报成交单查询应答
		* 参数：
		*- in_pstReqData      输入 --递延交割申报成交单
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspQryDeferDeliveryAppMatch(TDeferDeliveryAppMatch * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID){WriteLog("stdout",LTALL,"not implemented !!!");}
		/**********************************************************
		* 函 数 名： onRecvRspQryFuturesMatch
		* 功能描述：   期货成交单查询应答
		* 参数：
		*- in_pstReqData      输入 --期货成交单
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspQryFuturesMatch(TFuturesMatch * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID){WriteLog("stdout",LTALL,"not implemented !!!");}
		/**********************************************************
		* 函 数 名： onRecvRspQryForwardMatch
		* 功能描述：   现货T+N成交单查询应答
		* 参数：
		*- in_pstReqData      输入 --现货T+N成交单
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspQryForwardMatch(TForwardMatch * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID){WriteLog("stdout",LTALL,"not implemented !!!");}
		/**********************************************************
		* 函 数 名： onRecvRspQryForwardEndorseMatch
		* 功能描述：   现货T+N到期转让成交单查询应答
		* 参数：
		*- in_pstReqData      输入 --现货成交单信息
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspQryForwardEndorseMatch(TSpotMatch * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID){WriteLog("stdout",LTALL,"not implemented !!!");}
		/**********************************************************
		* 函 数 名： onRecvRspQryDeferMemberPosi
		* 功能描述：   会员现货延期交收持仓查询应答
		* 参数：
		*- in_pstReqData      输入 --会员现货延期交收持仓
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspQryDeferMemberPosi(TDeferMemberPosi * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID){WriteLog("stdout",LTALL,"not implemented !!!");}
		/**********************************************************
		* 函 数 名： onRecvRspQryFuturesMemberPosi
		* 功能描述：   会员期货持仓查询应答
		* 参数：
		*- in_pstReqData      输入 --会员期货持仓
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspQryFuturesMemberPosi(TFuturesMemberPosi * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID){WriteLog("stdout",LTALL,"not implemented !!!");}
		/**********************************************************
		* 函 数 名： onRecvRspQryForwardMemberPosi
		* 功能描述：   会员现货T+N持仓查询应答
		* 参数：
		*- in_pstReqData      输入 --会员现货T+N持仓
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspQryForwardMemberPosi(TForwardMemberPosi * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID){WriteLog("stdout",LTALL,"not implemented !!!");}
		/**********************************************************
		* 函 数 名： onRecvRspQryDeferClientPosi
		* 功能描述：   客户现货延期交收持仓查询应答
		* 参数：
		*- in_pstReqData      输入 --客户现货延期交收持仓
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspQryDeferClientPosi(TDeferClientPosi * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID){WriteLog("stdout",LTALL,"not implemented !!!");};
		/**********************************************************
		* 函 数 名： onRecvRspQryFuturesClientPosi
		* 功能描述：   客户期货持仓查询应答
		* 参数：
		*- in_pstReqData      输入 --客户期货持仓
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspQryFuturesClientPosi(TFuturesClientPosi * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID){WriteLog("stdout",LTALL,"not implemented !!!");}
		/**********************************************************
		* 函 数 名： onRecvRspQryForwardClientPosi
		* 功能描述：   客户现货T+N持仓查询应答
		* 参数：
		*- in_pstReqData      输入 --客户现货T+N持仓
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspQryForwardClientPosi(TForwardClientPosi * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID){WriteLog("stdout",LTALL,"not implemented !!!");}
		/**********************************************************
		* 函 数 名： onRecvRspQrySpotQuotation
		* 功能描述：   现货行情查询应答
		* 参数：
		*- in_pstReqData      输入 --现货行情
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspQrySpotQuotation(TSpotQuotation * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID){WriteLog("stdout",LTALL,"not implemented !!!");}
		/**********************************************************
		* 函 数 名： onRecvRspQryDeferQuotation
		* 功能描述：   现货延期交收行情查询应答
		* 参数：
		*- in_pstReqData      输入 --现货延期交收行情
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspQryDeferQuotation(TDeferQuotation * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID){WriteLog("stdout",LTALL,"not implemented !!!");}
		/**********************************************************
		* 函 数 名： onRecvRspQryFuturesQuotation
		* 功能描述：   期货行情查询应答
		* 参数：
		*- in_pstReqData      输入 --期货行情
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspQryFuturesQuotation(TFuturesQuotation * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID){WriteLog("stdout",LTALL,"not implemented !!!");}
		/**********************************************************
		* 函 数 名： onRecvRspQryForwardQuotation
		* 功能描述：   现货T+N行情查询应答
		* 参数：
		*- in_pstReqData      输入 --现货T+N行情
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspQryForwardQuotation(TForwardQuotation * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID){WriteLog("stdout",LTALL,"not implemented !!!");}
		/**********************************************************
		* 函 数 名： onRecvRspQryDeferFeeRate
		* 功能描述：   递延费率查询请求应答
		* 参数：
		*- in_pstReqData      输入 --现货延期交收补偿费率信息
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspQryDeferFeeRate(TDeferFeeRate * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID){WriteLog("stdout",LTALL,"not implemented !!!");}
		/**********************************************************
		* 函 数 名： onRecvRspQryBulletinBoard
		* 功能描述：   交易所公告查询请求应答
		* 参数：
		*- in_pstReqData      输入 --交易所公告信息
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual void onRecvRspQryBulletinBoard(TBulletinBoard * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID){WriteLog("stdout",LTALL,"not implemented !!!");}
		/**********************************************************
		* 函 数 名： onRecvRspQryInterQuotation
		* 功能描述：   国际行情查询请求应答
		* 参数：
		*- in_pstReqData      输入 --国际行情信息
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspQryInterQuotation(TInterQuotation * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID){WriteLog("stdout",LTALL,"not implemented !!!");}
		/**********************************************************
		* 函 数 名： onRecvRspQrySpotMinsQuotation
		* 功能描述：   现货分钟行情查询应答
		* 参数：
		*- in_pstReqData      输入 --当天现货分钟行情
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspQrySpotMinsQuotation(TMinsSpotQuotation * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID){WriteLog("stdout",LTALL,"not implemented !!!");}
		/**********************************************************
		* 函 数 名： onRecvRspQryDeferMinsQuotation
		* 功能描述：   现货延期交收分钟行情查询应答
		* 参数：
		*- in_pstReqData      输入 --递延分钟行情
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspQryDeferMinsQuotation(TMinsDeferQuotation * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID){WriteLog("stdout",LTALL,"not implemented !!!");}
		/**********************************************************
		* 函 数 名： onRecvRspQryFuturesMinsQuotation
		* 功能描述：   期货分钟行情查询应答
		* 参数：
		*- in_pstReqData      输入 --期货分钟行情
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspQryFuturesMinsQuotation(TMinsFuturesQuotation * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID){WriteLog("stdout",LTALL,"not implemented !!!");}
		/**********************************************************
		* 函 数 名： onRecvRspQryForwardMinsQuotation
		* 功能描述：   现货T+N分钟行情查询应答
		* 参数：
		*- in_pstReqData      输入 --远期分钟行情
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspQryForwardMinsQuotation(TMinsForwardQuotation * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID){WriteLog("stdout",LTALL,"not implemented !!!");}
		/**********************************************************
		* 函 数 名： onRecvRspQryClient
		* 功能描述：   会员客户查询响应
		* 参数：
		*- in_pstReqData      输入 --远期分钟行情
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRspQryClient(TClient * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID){WriteLog("stdout",LTALL,"not implemented onRecvRspQryClient!!!");}
		/**********************************************************
		* 函 数 名： onRecvRtnSpotOrder
		* 功能描述：   现货报单回报
		* 参数：
		*- in_pstRspData      输入 --现货报单信息
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRtnSpotOrder(TSpotOrder * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRtnDeferOrder
		* 功能描述：   现货延期交收报单回报
		* 参数：
		*- in_pstRspData      输入 --现货延期交收报单
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRtnDeferOrder(TDeferOrder * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRtnSpotOrderCancel
		* 功能描述：   现货撤单回报
		* 参数：
		*- in_pstRspData      输入 --现货报单信息
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRtnSpotOrderCancel(TSpotOrder * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRtnDeferOrderCancel
		* 功能描述：   现货延期交收撤单回报
		* 参数：
		*- in_pstRspData      输入 --现货延期交收报单
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRtnDeferOrderCancel(TDeferOrder * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRtnDeferDeliveryAppOrder
		* 功能描述：   现货延期交收交割申报回报
		* 参数：
		*- in_pstRspData      输入 --现货延期交收交割申报报单
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRtnDeferDeliveryAppOrder(TDeferDeliveryAppOrder * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRtnMiddleAppOrder
		* 功能描述：   现货延期交收中立仓申报回报
		* 参数：
		*- in_pstRspData      输入 --现货延期交收中立仓申报报单
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRtnMiddleAppOrder(TMiddleAppOrder * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRtnDeferDeliveryAppOrderCancel
		* 功能描述：   现货延期交收交割申报撤消回报
		* 参数：
		*- in_pstRspData      输入 --现货延期交收交割申报报单
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRtnDeferDeliveryAppOrderCancel(TDeferDeliveryAppOrder * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRtnMiddleAppOrderCancel
		* 功能描述：   现货延期交收中立仓申报撤单回报
		* 参数：
		*- in_pstRspData      输入 --现货延期交收中立仓申报报单
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRtnMiddleAppOrderCancel(TMiddleAppOrder * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRtnFuturesOrder
		* 功能描述：   期货报单回报
		* 参数：
		*- in_pstRspData      输入 --期货报单
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRtnFuturesOrder(TFuturesOrder * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID){WriteLog("stdout",LTALL,"not implemented !!!");}
		/**********************************************************
		* 函 数 名： onRecvRtnForwardOrder
		* 功能描述：   现货T+N报单回报
		* 参数：
		*- in_pstRspData      输入 --现货T+N报单
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRtnForwardOrder(TForwardOrder * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID){WriteLog("stdout",LTALL,"not implemented !!!");}
		/**********************************************************
		* 函 数 名： onRecvRtnFuturesOrderCancel
		* 功能描述：   期货撤单回报
		* 参数：
		*- in_pstRspData      输入 --期货报单
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual void onRecvRtnFuturesOrderCancel(TFuturesOrder * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID){WriteLog("stdout",LTALL,"not implemented !!!");}
		/**********************************************************
		* 函 数 名： onRecvRtnForwardOrderCancel
		* 功能描述：   现货T+N撤单回报
		* 参数：
		*- in_pstRspData      输入 --现货T+N报单
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual void onRecvRtnForwardOrderCancel(TForwardOrder * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID){WriteLog("stdout",LTALL,"not implemented !!!");}
		/**********************************************************
		* 函 数 名： onRecvRtnForwardEndorseOrder
		* 功能描述：   现货T+N到期转让报单回报
		* 参数：
		*- in_pstRspData      输入 --现货报单信息
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual void onRecvRtnForwardEndorseOrder(TSpotOrder * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID){WriteLog("stdout",LTALL,"not implemented !!!");}
		/**********************************************************
		* 函 数 名： onRecvRtnForwardEndorseOrderCancel
		* 功能描述：   现货T+N到期转让撤单回报
		* 参数：
		*- in_pstRspData      输入 --现货报单信息
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual void onRecvRtnForwardEndorseOrderCancel(TSpotOrder * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID){WriteLog("stdout",LTALL,"not implemented !!!");}
		/**********************************************************
		* 函 数 名： onRecvRtnSpotMatch
		* 功能描述：   现货成交回报
		* 参数：
		*- in_pstRspData      输入 --现货成交单信息
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual void onRecvRtnSpotMatch(TSpotMatch * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRtnDeferMatch
		* 功能描述：   现货延期交收成交回报
		* 参数：
		*- in_pstRspData      输入 --现货延期交收成交单
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRtnDeferMatch(TDeferMatch * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRtnDeferDeliveryAppMatch
		* 功能描述：   现货延期交收交割申报成交回报
		* 参数：
		*- in_pstRspData      输入 --递延交割申报成交单
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRtnDeferDeliveryAppMatch(TDeferDeliveryAppMatch * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID){WriteLog("stdout",LTALL,"not implemented !!!");}
		/**********************************************************
		* 函 数 名： onRecvRtnFuturesMatch
		* 功能描述：   期货成交回报
		* 参数：
		*- in_pstRspData      输入 --期货成交单
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRtnFuturesMatch(TFuturesMatch * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID){WriteLog("stdout",LTALL,"not implemented !!!");}
		/**********************************************************
		* 函 数 名： onRecvRtnForwardMatch
		* 功能描述：   现货T+N成交回报
		* 参数：
		*- in_pstRspData      输入 --现货T+N成交单
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual 	void onRecvRtnForwardMatch(TForwardMatch * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID){WriteLog("stdout",LTALL,"not implemented !!!");}
		/**********************************************************
		* 函 数 名： onRecvRtnForwardEndorseMatch
		* 功能描述：   现货T+N到期转让成交回报
		* 参数：
		*- in_pstRspData      输入 --现货成交单信息
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual void onRecvRtnForwardEndorseMatch(TSpotMatch * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvMarketInfo
		* 功能描述：   市场信息回报
		* 参数：
		*- in_pstRspData      输入 --市场信息
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual void onRecvMarketInfo(TMarket * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvSpotInstInfo
		* 功能描述：   现货合约信息回报
		* 参数：
		*- in_pstRspData      输入 --现货合约
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual void onRecvSpotInstInfo(TSpotInst * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvDeferInstInfo
		* 功能描述：   递延合约信息回报
		* 参数：
		*- in_pstRspData      输入 --现货延期交收合约
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual void onRecvDeferInstInfo(TDeferInst * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvFuturesInstInfo
		* 功能描述：    期货合约信息回报
		* 参数：
		*- in_pstRspData      输入 --期货合约
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual void onRecvFuturesInstInfo(TFuturesInst * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************

		* 函 数 名： onRecvForwardInstInfo
		* 功能描述：   远期合约信息回报
		* 参数：
		*- in_pstRspData      输入 --现货T+N合约
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual void onRecvForwardInstInfo(TForwardInst * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRtnSpotInstStateUpdate
		* 功能描述：   现货合约交易状态改变回报
		* 参数：
		*- in_pstRspData      输入 --合约交易状态信息
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual void onRecvRtnSpotInstStateUpdate(TInstState * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRtnSpotMarketStateUpdate
		* 功能描述：   现货市场交易状态改变回报
		* 参数：
		*- in_pstRspData      输入 --市场交易状态信息
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual void onRecvRtnSpotMarketStateUpdate(TMarketState * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRtnDeferInstStateUpdate
		* 功能描述：   递延合约交易状态改变回报
		* 参数：
		*- in_pstRspData      输入 --合约交易状态信息
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual void onRecvRtnDeferInstStateUpdate(TInstState * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRtnDeferMarketStateUpdate
		* 功能描述：   递延市场交易状态改变回报
		* 参数：
		*- in_pstRspData      输入 --市场交易状态信息
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual void onRecvRtnDeferMarketStateUpdate(TMarketState * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRtnFuturesInstStateUpdate
		* 功能描述：   期货合约交易状态改变回报
		* 参数：
		*- in_pstRspData      输入 --合约交易状态信息
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual void onRecvRtnFuturesInstStateUpdate(TInstState * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRtnFuturesMarketStateUpdate
		* 功能描述：   期货市场交易状态改变回报
		* 参数：
		*- in_pstRspData      输入 --市场交易状态信息
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual void onRecvRtnFuturesMarketStateUpdate(TMarketState * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRtnForwardInstStateUpdate
		* 功能描述：   远期合约交易状态改变回报
		* 参数：
		*- in_pstRspData      输入 --合约交易状态信息
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual void onRecvRtnForwardInstStateUpdate(TInstState * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvRtnForwardMarketStateUpdate
		* 功能描述：   远期市场交易状态改变回报
		* 参数：
		*- in_pstRspData      输入 --市场交易状态信息
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual void onRecvRtnForwardMarketStateUpdate(TMarketState * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID){WriteLog("stdout",LTALL,"not implemented !!!");}
		/**********************************************************
		* 函 数 名： onRecvRtnBulletinBoard
		* 功能描述：   公告
		* 参数：
		*- in_pstRspData      输入 --交易所公告信息
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual void onRecvRtnBulletinBoard(TBulletinBoard * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID){WriteLog("stdout",LTALL,"not implemented !!!");}
		/**********************************************************
		* 函 数 名： onRecvRtnInterQuotation
		* 功能描述：   国际行情
		* 参数：
		*- in_pstRspData      输入 --国际行情信息
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual void onRecvRtnInterQuotation(TInterQuotation * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID){WriteLog("stdout",LTALL,"not implemented !!!");}
		/**********************************************************
		* 函 数 名： onRecvSpotQuotation
		* 功能描述：   现货行情
		* 参数：
		*- in_pstRspData      输入 --现货行情
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual void onRecvSpotQuotation(TSpotQuotation * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvDeferQuotation
		* 功能描述：   现货延期交收行情
		* 参数：
		*- in_pstRspData      输入 --现货延期交收行情
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual void onRecvDeferQuotation(TDeferQuotation * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvFuturesQuotation
		* 功能描述：   期货行情
		* 参数：
		*- in_pstRspData      输入 --期货行情
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual void onRecvFuturesQuotation(TFuturesQuotation * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID){WriteLog("stdout",LTALL,"not implemented !!!");}
		/**********************************************************
		* 函 数 名： onRecvForwardQuotation
		* 功能描述：   现货T+N行情
		* 参数：
		*- in_pstRspData      输入 --现货T+N行情
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual void onRecvForwardQuotation(TForwardQuotation * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID){WriteLog("stdout",LTALL,"not implemented !!!");}
		/**********************************************************
		* 函 数 名： onRecvMinsSpotQuotation
		* 功能描述：   当天现货分钟行情
		* 参数：
		*- in_pstRspData      输入 --当天现货分钟行情
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual void onRecvMinsSpotQuotation(TMinsSpotQuotation * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID){WriteLog("stdout",LTALL,"not implemented !!!");}
		/**********************************************************
		* 函 数 名： onRecvMinsFuturesQuotation
		* 功能描述：   当天期货分钟行情
		* 参数：
		*- in_pstRspData      输入 --期货分钟行情
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual void onRecvMinsFuturesQuotation(TMinsFuturesQuotation * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID){WriteLog("stdout",LTALL,"not implemented !!!");}

		/**********************************************************
		* 函 数 名： onRecvRspQrySpotLocalOrder
		* 功能描述：   现货本地报单号查询应答
		* 参数：
		*- in_pstReqData      输入 --现货报单信息
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual void onRecvRspQrySpotLocalOrder(TSpotOrder * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID){WriteLog("stdout",LTALL,"not implemented !!!");}


		/**********************************************************
		* 函 数 名： onRecvMinsForwardQuotation
		* 功能描述：   当天远期分钟行情回报
		* 参数：
		*- in_pstRspData      输入 --远期分钟行情
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual void onRecvMinsForwardQuotation(TMinsForwardQuotation * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID){WriteLog("stdout",LTALL,"not implemented !!!");}
		/**********************************************************
		* 函 数 名： onRecvMinsDeferQuotation
		* 功能描述：   当天递延分钟行情
		* 参数：
		*- in_pstRspData      输入 --递延分钟行情
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual void onRecvMinsDeferQuotation(TMinsDeferQuotation * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvDeferFeeRateUpdate
		* 功能描述：   发布现货延期交收补偿费率通知
		* 参数：
		*- in_pstRspData      输入 --现货延期交收补偿费率信息
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual void onRecvDeferFeeRateUpdate(TDeferFeeRate * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvDeferDeliveryQuotation
		* 功能描述：    递延交收行情回报
		* 参数：
		*- in_pstRspData      输入 --递延交收行情
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual void onRecvDeferDeliveryQuotation(TDeferDeliveryQuotation * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);
		/**********************************************************
		* 函 数 名： onRecvVarietyInfo
		* 功能描述：   交割品种代码信息回报
		* 参数：
		*- in_pstRspData      输入 --交割品种代码
		*- in_pstRspMsg      输入 --请求执行结果和连续包标志
		*- in_pszRootID      输入 --与请求包对应的唯一标示
		* 返回说明： 无
		************************************************************/
		virtual void onRecvVarietyInfo(TVariety * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID);

	};
}
#endif