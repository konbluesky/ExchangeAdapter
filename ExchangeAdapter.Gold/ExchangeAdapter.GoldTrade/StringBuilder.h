#ifndef _STRING_BUILDER_H_
#define _STRING_BUILDER_H_
#include <string>

namespace utils
{
	///注：这里的CStringBuilder不是自己管理内存
	class CStringBuilder
	{
	public:
		CStringBuilder(char* pbuff);
		~CStringBuilder(void);
		char* GetBuffer() const;
		CStringBuilder&  AppendFormat(const char* fmt,...);
		int GetIndex() const;

		///类MFC的CString.Format
		static std::string Format(const char* fmt);

	private:
		char* m_pbuff;
		int m_currentindex;
	};
};

#endif