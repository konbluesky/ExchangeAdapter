#ifndef _LOGMGR_H_
#define _LOGMGR_H_

#pragma warning(disable:4786)
#include <string>
#include <list>
#include <map>
#include "threadlock.h"

namespace utils
{

	class ILog
	{
	public:
		///Listen category 
		enum LogType
		{
			TNone   = 0x00000000,
			TDebug	= 0x00000001,
			TLog	= 0x00000010,
			TSuccess= 0x00000100,
			TWarning= 0x00001000,
			TError	= 0x00010000,	
			TALL	= 0x11111111,
		};
		static LogType ParseString(const char* str);
		
		virtual void SetFavorite(DWORD type) = 0;
		virtual DWORD GetFavorite() = 0;

		///Write formated string, the data len is limited 1024
		virtual void WriteLog(ILog::LogType type, char* data, int len) = 0;
	};


	 template<typename T> class CBufferPool;
	///
	///专门用来写log的，为提高应用程序效率，写log的文件IO放在单独线程完成(注：使用异步io虽然能使速度提高，但对于数据写入顺序无法保障)
	///注：对于日志的文件名，将把日期添加进文件名中，代码不考虑跨日情况(注：即程序启动一次后，将唯一读取一次日期时间，当然也可以
	///
	class CLogMgr  
	{
	public:
		///默认终端out的文件名
		static const char* STDOUT; 
		///默认终端err的文件名
		static const char* STDERR;
		static CLogMgr& Instance();

	public:			
		virtual ~CLogMgr();	
		///从xml文件读取log的配置文件
		bool InitializeByXML(const std::string& fname);	
		ILog&	GetLog(const std::string& fname);	//从map地址里查找指定路径的指定文件是否是否存在，不存在则生成一个对象	
		void	Close();	
		CBufferPool<threadlock::multi_threaded_local>* GetBufferPool();//指定生成对象的内存空间

	public:
		HANDLE m_hWorkthreadhandler;


	private:
		///获取log的文件名，根据规则，会给文件增加date前缀（注：如果是输出到终端，则无)
		std::string getlogfilepath(std::string fname);
		CLogMgr();
		void CloseThreadWork();
	
	private:
		std::string m_currentdate;
		std::string m_logdir;
		threadlock::multi_threaded_local m_lock;		
		std::map<std::string,ILog*> _logmap;
		CThreadSafeBuffPool* m_pBufferPool;
		
	};

}

#endif 