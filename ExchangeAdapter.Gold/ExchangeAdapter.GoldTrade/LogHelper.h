#ifndef _LOG_HELPER_H_
#define _LOG_HELPER_H_
#include <string>
#include "util.h"
#include "BufferPool.h"
#include "StringBuilder.h"
#include "LogMgr.h"

using namespace utils;


bool InitializeLog(const char* fname);

///写log 字符串 到fname文件里,注：根据业务需要，文件名自动增加日期
void WriteLog(const char* fname,ILog::LogType type ,const char* fmt,... );

///写log data block 到fname文件里
void WriteBlockLog(const char* fname,ILog::LogType type,char* block ,int blocklen);

///
void UninitializeLog();

///宏实现

#define  WriteDetailLog(fname,type,info)  do{ ILog& log = CLogMgr::Instance().GetLog(fname); if(log.GetFavorite() & type) {  char* buff = CLogMgr::Instance().GetBufferPool()->Pop();  CStringBuilder cb(buff); cb.AppendFormat("%-16sF:%-16s L:%-5d T:%-8dM:",UTimeHelper::GetCurrentFormatTime().c_str(), UFileSystemHelper::GetName(__FILE__).c_str(),__LINE__, \
	::GetCurrentThreadId()).AppendFormat info .AppendFormat("\n");  log.WriteLog(type,cb.GetBuffer(),cb.GetIndex());} }while(0)


#define ExAssert(x,msg)  do{  if(!(x)){ WriteDetailLog("stdout",ILog::TALL,("assert %s failed  %s ", #x,std::string(msg).c_str()) ); WriteDetailLog("assert",ILog::TALL,("assert %s failed  %s ", #x,std::string(msg).c_str()) ); }  }while (0)

#ifndef LTALL
	#define LTALL ::utils::ILog::TALL
#endif


///注：call stack比较注重线程，所以这里需要扩展一下
class _privateCallStackLog
{
public:
	_privateCallStackLog(const char* fname,const char* msg );
	~_privateCallStackLog();
private:
	std::string _fname;
	std::string _msg;
	DWORD _thread;
};

///注可以用这个来简化记录函数调用堆栈过程
#define LogCallStack(msg)	_privateCallStackLog callstackLog##msg(__FILE__,#msg)

#endif