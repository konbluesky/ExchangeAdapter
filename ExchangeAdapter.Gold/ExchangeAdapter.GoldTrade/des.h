/*
**	用软件实现的DES算法(s_des.cpp)
*/

#define	FES_RANDOMLEN	8
#define	FES_SKEYLEN		32
#define	FES_LKEYLEN		128


#ifndef des_inc
#define des_inc

#define ENCRYPT		0
#define DECRYPT		1
#ifndef BYTE
#define BYTE unsigned char
#endif
#ifndef WORD
#define WORD unsigned short
#endif
#ifndef DWORD
#define DWORD unsigned long
#endif

//DES算法（根据密钥长度决定选择TripleDES OR DES）
void	rv_DES(BYTE, BYTE*, BYTE*, BYTE*);
void	rv_Triple_DES(long, BYTE*, BYTE*, BYTE*);
long	rl_DESEnc_ASC(char*, char*, char*);
long	rl_DESDec_ASC(char*, char*, char*);
long	rl_DESEnc_BYTE(long, BYTE*, long, BYTE*, long*, BYTE*);
long	rl_DESDec_BYTE(long, BYTE*, long, BYTE*, long*, BYTE*);
#endif

//void    rv_ICLog(const char*, ...);
//void	rv_ICSysLog(const char*, long);

//日志记录的宏定义
//#define	RXLOG	rv_ICSysLog(__FILE__, __LINE__);rv_ICLog
void	rv_BYTE2ASC(long, const BYTE *, char *);
void	rv_ASC2BYTE(const char *, long *, BYTE *);

/*
**	实现基于ASC字符输入输出的MD5算法，
**	要求输入数据中只能包括(0-F)之间的数
**	输出为0-F之间的32位串
*/
long	rl_MD5_ASC(char *ins_InBuf, char *ins_OutBuf);
/*
**	实现基于BYTE字符输入输出的MD5算法，
**	输出为16位的BYTE串
*/
void	rv_MD5_BYTE(long inl_inLen, BYTE *inb_InBuf, BYTE *inb_OutBuf);


