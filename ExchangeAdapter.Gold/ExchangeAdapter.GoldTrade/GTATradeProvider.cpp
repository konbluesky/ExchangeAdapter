
#pragma warning(disable:4244)
#include <algorithm>
#include "simsingleton.h"
#define TIXML_USE_STL
#include "tinyxml.h"
#include "GTAListener.h"
#include "GTAReciever.h"
#include "GTASender.h"
#include "GTARecieverMgr.h"
#include "GTASenderMgr.h"
#include "GTATradeProvider.h"
#include "GTATradeCodeMap.h"
#include "GTATradeApiHandler.h"
#include "util.h"
#include "LogHelper.h"
#include "CCrypticDES.h"
#include "GTAReciever.h"
using namespace simpattern;
namespace gta
{
	bool CGTATradeProvider::Initialize(InitParam* param)
	{
		LogCallStack(CGTATradeProvider_Initialize);


		//-1
		m_bufferpool = std::shared_ptr<CThreadSafeBuffPool>(new CThreadSafeBuffPool(1024*8) );
		

		//0 
		long lret = Singleton_InFunc<CCrypticDES>::Instance().rl_SetKey(param->commkey.c_str());
		if(lret)
		{
			WriteLog("stdout",LTALL,"Singleton_InFunc<CCrypticDES>::Instance().rl_SetKey(param->commkey.c_str())= %d",lret	);
			WriteLog("error",LTALL,"Singleton_InFunc<CCrypticDES>::Instance().rl_SetKey(param->commkey.c_str())= %d",lret	);
			return false;

		}

		//1 initialize recieverMgr
		m_revievermgr = std::shared_ptr<CGTARecieverMgr>(new CGTARecieverMgr );


		

		//4 initialize sendermgr
		m_sendermgr = std::shared_ptr<CGTASenderMgr>(new CGTASenderMgr(param->GTAServerAddr,param->GTAServerPort,this));

		//5 initialize listener,this should start last till 
		m_listener = std::shared_ptr<CGTAListener>(new CGTAListener(this) );
		
		
		bool bret = m_listener->Start(param->ilistenport);//创建一个线程进行轮询
		if (!bret)
		{
			WriteLog("error",LTALL,"start listen false");
			return false;
		}
		return true;
	}

	void CGTATradeProvider::Stop()
	{
	}

	CGTATradeProvider::CGTATradeProvider()
	{
	}

	CGTATradeProvider::~CGTATradeProvider()
	{
		WriteLog("stdout",LTALL,"CGTATradeProvider::~CGTATradeProvider()");		
	}

	bool CGTATradeProvider::InitializeByXML( std::string fname )
	{
		LogCallStack(CGTATradeProvider_InitializeByXML);
		TiXmlDocument doc(fname);
		CGTATradeProvider::InitParam param;
		bool loadOkay = doc.LoadFile();
		if (!loadOkay)
		{
			WriteDetailLog("error",LTALL,("没有找到配置文件%s, 当前程序路径是：%s",fname.c_str(),utils::UFileSystemHelper::GetAppDirectory().c_str()));
			WriteDetailLog("stdout",LTALL,("没有找到配置文件%s, 当前程序路径是：%s",fname.c_str(),utils::UFileSystemHelper::GetAppDirectory().c_str()));
			return false;
		}

		TiXmlNode* pNode = doc.FirstChild("configure");
		TiXmlNode* ele = pNode->FirstChild();	

		ele = pNode->FirstChild("ilistenport");
		param.ilistenport = atoi(ele->FirstChild()->Value());
		WriteLog("log",LTALL,"监听gta的端口%d",param.ilistenport);
		WriteLog("stdout",LTALL,"监听gta的端口%d",param.ilistenport);
		ele = pNode->FirstChild("commkey");
		param.commkey = ele->FirstChild()->Value();		
		
		//找到映射表，
		ele = pNode->FirstChild("tradecodemap");
		//初始化map指针
		CGTATradeCodeMap::Instance().InitializeTradecodeByXMLNode(ele);

		ele = pNode->FirstChild("replyconfig");
		CGTATradeCodeMap::Instance().InitializeReplycodeByXMLNode(ele);

		ele = pNode->FirstChild("gtaserveraddr");
		if(ele != NULL)
		{
			TiXmlNode* addressNode;

			addressNode = ele->FirstChild("saddressip");
			param.GTAServerAddr = addressNode->FirstChild()->Value();
			addressNode = ele->FirstChild("iport");
			param.GTAServerPort = atoi(addressNode->FirstChild()->Value());			
		}
		WriteLog("log",LTALL,"gta服务器的设置:%s  ：%d",param.GTAServerAddr.c_str() ,param.GTAServerPort);
		WriteLog("stdout",LTALL,"gta服务器的设置:%s  ：%d",param.GTAServerAddr.c_str() ,param.GTAServerPort);	

		return Initialize(&param);
	}

	std::shared_ptr<CGTAListener> CGTATradeProvider::GetCGTAListener()
	{
		return m_listener;
	}

	std::shared_ptr<CGTARecieverMgr> CGTATradeProvider::GetCGTARecieverMgr()
	{
		return m_revievermgr;
	}

	CGTATradeApiHandler* CGTATradeProvider::GetCGTATradeApiHandler()
	{
		return m_phandler;
	}

	std::shared_ptr<CGTASenderMgr> CGTATradeProvider::GetCGTASenderMgr()
	{
		return m_sendermgr;
	}

	void CGTATradeProvider::ProcessData(char* buff,int bufflen,const char* rootkey,RetType TYPE,const char* tradecode)
	{
		OldProcessData(buff, bufflen,rootkey, TYPE,tradecode);
		/*
		{		
		auto sender = this->GetCGTASenderMgr()->GetSender();
		sender->SendData(buff,bufflen);
		}
		*/
	}


	void CGTATradeProvider::OldProcessData(char* buff,int bufflen,const char* rootkey,RetType TYPE,const char* tradecode)
	{
		if(TYPE == CGTATradeProvider::RESPONSE)
		{
			//注：reciever			
			auto ret = this->GetCGTARecieverMgr()->FindReciever(rootkey);
			if(ret.first)
			{
				ret.second->OldSendGTAMessage(buff,bufflen);
			}
		}
		else
		{
			auto pinfo = CGTATradeCodeMap::Instance().GetRspInfo(tradecode);

 			if(pinfo == NULL)
 			{ 							
 				auto sender = this->GetCGTASenderMgr()->GetSender(); 				
				sender->SendData(buff,bufflen);
 			}
 			else
 			{
 				auto sender = this->GetCGTASenderMgr()->GetSpecialSender(pinfo->gtaaddr.c_str(),pinfo->port);
				sender->SendData(buff,bufflen);
 			}
			
			
		}
	}

	std::shared_ptr<CThreadSafeBuffPool> CGTATradeProvider::GetShareDataPool()
	{
		return m_bufferpool;
	}

	bool CGTATradeProvider::SetAPIHandler( CGTATradeApiHandler* handler )
	{
		m_phandler = handler;
		return true;
	}

}