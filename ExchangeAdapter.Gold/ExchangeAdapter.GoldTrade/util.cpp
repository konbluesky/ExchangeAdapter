#include <time.h>
#include <algorithm>
#include <windows.h>
//#include "mybase.h"
//#include "GoldTradeSVR.h"
#include "util.h"
//#include "ServiceHelper.h"

namespace utils
{
	double CGTABufferBuilder::rd_RoundFloat(double in_dData, long in_lDigital)
	{
		long	lSign = 0, lRounded = 0, lTemp = 0;
		char	szBuf[30] = "\0", *psz = NULL;
		double 	dResult = 0.00, dRest = 0.00;

		// Prints the double data to a character buffer. 

		sprintf( szBuf, "%lf", in_dData );

		// Finds the decimal point in the string. 

		for ( psz = szBuf; *psz != '.'; psz++ );

		// Forces the digital into it's range.

		if ( in_lDigital < 0 )
			in_lDigital = 0;
		if ( in_lDigital > 5 )
			in_lDigital = 5;

		// Checks whether it will be rounded.

		if ( *( psz + in_lDigital + 1 ) >= '5' ) {
			lRounded = 1;
			if ( in_dData < 0 )
				lSign = -1;
			else
				lSign = 1;
		}
		else
			lRounded = 0;

		// Finds and clears the rest of digital in the buffer.

		if ( in_lDigital > 0 )
			psz += in_lDigital + 1;

		*psz = '\0';

		// Converts the data breaded in the buffer into the double format.

		sscanf( szBuf, "%lf", &dResult );

		// If the round occured.

		if ( lRounded == 1 ) {
			psz = szBuf;
			if ( in_lDigital > 0 ) {
				strcpy( psz, "0." );
				psz += 2;
				for( lTemp = 1; lTemp < in_lDigital; *psz = '0', psz++, lTemp++ );
			}
			*psz = '1';
			*( psz + 1 ) = '\0';
			sscanf( szBuf, "%lf", &dRest );
			if ( lSign == 1 )
				dResult += dRest;
			else
				dResult -= dRest;
		}

		return( dResult );
	}

	void UTF8GBKHelper::GBKToUTF8( char* &szOut )
	{
		char* strGBK = szOut;

		int len=MultiByteToWideChar(CP_ACP, 0, (LPCSTR)strGBK, -1, NULL,0);
		unsigned short * wszUtf8 = new unsigned short[len+1];
		memset(wszUtf8, 0, len * 2 + 2);
		MultiByteToWideChar(CP_ACP, 0, (LPCSTR)strGBK, -1, (LPWSTR)wszUtf8, len);

		len = WideCharToMultiByte(CP_UTF8, 0, (LPWSTR)wszUtf8, -1, NULL, 0, NULL, NULL);
		char *szUtf8=new char[len + 1];
		memset(szUtf8, 0, len + 1);
		WideCharToMultiByte (CP_UTF8, 0, (LPWSTR)wszUtf8, -1, szUtf8, len, NULL,NULL);

		//szOut = szUtf8;
		memset(szOut,0,strlen(szUtf8)+1);
		memcpy(szOut,szUtf8,strlen(szUtf8));

		delete[] szUtf8;
		delete[] wszUtf8;
	}

	void UTF8GBKHelper::UTF8ToGBK( char *&szOut )
	{
		unsigned short *wszGBK;
		char *szGBK;
		//长度
		int len = MultiByteToWideChar(CP_UTF8, 0, (LPCSTR)szOut, -1, NULL, 0);
		wszGBK = new unsigned short[len+1];
		memset(wszGBK, 0, len * 2 + 2);
		MultiByteToWideChar(CP_UTF8, 0, (LPCSTR)szOut, -1, (LPWSTR)wszGBK, len);

		//长度
		len = WideCharToMultiByte(CP_ACP, 0, (LPWSTR)wszGBK, -1, NULL, 0, NULL, NULL);
		szGBK = new char[len+1];
		memset(szGBK, 0, len + 1);
		WideCharToMultiByte(CP_ACP, 0, (LPWSTR)wszGBK, -1, szGBK, len, NULL, NULL);

		//szOut = szGBK; //这样得到的szOut不正确，因为此句意义是将szGBK的首地址赋给szOut，当delete []szGBK执行后szGBK的内

		//存空间将被释放，此时将得不到szOut的内容

		memset(szOut,0,strlen(szGBK)+1); //改将szGBK的内容赋给szOut ，这样即使szGBK被释放也能得到正确的值
		memcpy(szOut,szGBK,strlen(szGBK));


		delete []szGBK;
		delete []wszGBK;
	}

	char* UStringHelper::TrimLeft( char* in_szStr )
	{
		long	lIndex = 0 , i;
		long	lLength = 0;
		if(NULL == in_szStr)
			return in_szStr;
		if(in_szStr[0] == 0)
			return in_szStr;

		lLength = strlen(in_szStr);
		lIndex = 0;
		while((in_szStr[lIndex] == ' ' || in_szStr[lIndex] == '\n'
			|| in_szStr[lIndex] == '\t') ) 
		{
			lIndex++;
			if(lLength <= lIndex )
				break;
		}
		//前面有空格
		if(0 < lIndex )
		{
			//移动字符除去空格
			for(i=0 ; i< lLength - lIndex; i++)
				in_szStr[i] = in_szStr[i+lIndex];
			in_szStr[i] = 0;
		}
		return(in_szStr);
	}

	char* UStringHelper::Trim( char* p )
	{
		return TrimLeft(TrimRight(p));
	}

	char* UStringHelper::TrimRight( char* in_szStr )
	{
		long	lIndex = 0;

		if(NULL == in_szStr)
			return in_szStr;
		if(in_szStr[0] == 0)
			return in_szStr;

		lIndex = strlen(in_szStr)-1;

		while((in_szStr[lIndex] == ' ' || in_szStr[lIndex] == '\n' 
			|| in_szStr[lIndex] == '\t' || in_szStr[lIndex] == '\r') ) 
		{
			in_szStr[lIndex] = '\0';
			lIndex--;
			if(0 > lIndex )
				break;
		}

		return(in_szStr);
	}

	std::vector<std::string> UStringHelper::Spilt( const char* input,const char* spiltter )
	{
		std::vector<std::string> ret;
		std::string data = input;
		std::string delimiter = spiltter;
		int begin = 0;			
		int index = data.find_first_of (delimiter);
		while (index != std::string::npos)
		{
			std::string tmp = data.substr (begin,index-begin);
			ret.push_back (tmp);
			begin = index+delimiter.size();
			index = data.find_first_of (delimiter,begin);
		}
		if (!data.empty ())
		{
			std::string tmp = data.substr (begin);
			ret.push_back (tmp);
		}
		return ret;
	}

	std::string UStringHelper::ToUpper( const std::string& value )
	{
		std::string ret = value;
		std::transform(ret.begin(), ret.end(), ret.begin(), toupper);
		return ret;
	}


	std::string UStringHelper::ToLower( const std::string& value )
	{
		std::string ret = value;
		std::transform(ret.begin(), ret.end(), ret.begin(), tolower);
		return ret;
	}

	std::string UFileSystemHelper::GetAppPath()
	{
		char path[MAX_PATH];
		::GetModuleFileNameA(NULL,path,MAX_PATH);
		return path;
	}

	std::string UFileSystemHelper::GetAppDirectory()
	{
		std::string path = GetAppPath();
		return GetDirectory(path);
	}

	std::string UFileSystemHelper::GetDirectory( const std::string& path )
	{
		TCHAR drive[MAX_PATH],dir[MAX_PATH],fname[MAX_PATH],ext[MAX_PATH];
		_splitpath(path.c_str(),drive,dir,fname,ext);
		return std::string(drive) + dir;
	}

	std::string UFileSystemHelper::GetName( const std::string& path )
	{
		TCHAR drive[MAX_PATH],dir[MAX_PATH],fname[MAX_PATH],ext[MAX_PATH];
		_splitpath(path.c_str(),drive,dir,fname,ext);
		return std::string(fname) +ext;
	}

	void UFileSystemHelper::SetAppDirectory()
	{
		TCHAR path[MAX_PATH];
		::GetModuleFileName(NULL,path,MAX_PATH);
		std::string ret = GetDirectory(path);
		::SetCurrentDirectory(ret.c_str());
	}

	CGTABufferBuilder::CGTABufferBuilder( char* buff,int startindex )
	{
		m_curindex = startindex;
		m_pbuff = buff;
	}

	int CGTABufferBuilder::GetIndex() const
	{
		return m_curindex;
	}

	void CGTABufferBuilder::FillBlock( int startindex,void* data,int datalen,int len )
	{
		ASSERT(datalen<len);
		fastmemcpy(&m_pbuff[startindex],(char*)data,datalen);
	}

	char* CGTABufferBuilder::GetBuff()
	{
		return m_pbuff;
	}

	/*CGTABufferBuilder& CGTABufferBuilder::AppendString( char* strdata,int len )
	{
		int copylen = strlen(strdata)+1;
		AppendBlock(strdata,copylen,len);
		return *this;
	}
*/
	CGTABufferBuilder& CGTABufferBuilder::AppendBlock(const  void* block,int blocklen, int len )
	{
		ASSERT(len>=blocklen);
		fastmemcpy(&m_pbuff[m_curindex],(char*)block,blocklen);
		m_curindex +=len;

		return *this;
	}

	CGTABufferBuilder& CGTABufferBuilder::AppendChar( char v )
	{
		return AppendBlock(&v,1,1);
	}

	CGTABufferBuilder& CGTABufferBuilder::AppendDouble( double v )
	{
		char szTmp[32];		

		v = rd_RoundFloat(v,2);
		sprintf(szTmp, "%-17f", v);

		return AppendBlock(szTmp,17,17);
	}

	CGTABufferBuilder& CGTABufferBuilder::AppendInt( int v )
	{
		char szTmp[32];		
		sprintf(szTmp, "%-10d", v);

		return AppendBlock(szTmp,10,10);
	}

	void CGTABufferBuilder::fastmemcpy( char* d,char* s,int len )
	{
// 		for (int i=0; i<len; ++i)
// 			d[i] = s[i];
		memcpy(d,s,len);
	}

	
	int UTimeHelper::GetCurrentDate()
	{
		SYSTEMTIME systime;/* Get time as long integer. */
		::GetLocalTime( &systime ); /* Convert to local time. */			
		int date = (systime.wYear)*10000 + (systime.wMonth+1)*100 + systime.wDay;		
		return date;
	}

	std::string UTimeHelper::GetCurrentFormatDate()
	{
		char buff[16];

		SYSTEMTIME systime;/* Get time as long integer. */
		::GetLocalTime( &systime ); /* Convert to local time. */			
		sprintf(buff,"%d.%2d.%2d",systime.wYear,systime.wMonth,systime.wDay);
		return buff;
	}

	std::string UTimeHelper::GetCurrentFormatTime()
	{
		SYSTEMTIME systime;/* Get time as long integer. */
		::GetLocalTime( &systime ); /* Convert to local time. */			
		char buff [16] ;
		sprintf(buff,"%2d:%2d:%2d-%3d",systime.wHour,systime.wMinute,systime.wSecond,systime.wMilliseconds);
		return buff;
	}

	long UTimeHelper::GetUniqLong()
	{
		__time32_t t;
		_time32(&t);
		return t;
		
	}

}