// dllmain.cpp : Defines the entry point for the DLL application.

#include <Windows.h>
#pragma message("注：由于vs2010 对工程的引用使用有bug，所以这里直接使用指令")
#ifdef _DEBUG
#pragma comment(lib,"ACEd")
#pragma comment(lib,"tinyxmlSTL")
#else
#pragma comment(lib,"ACE")
#pragma comment(lib,"tinyxmlSTL")
#endif
BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}




#define  LIB_BUILD_DLL

#ifdef WIN32	
#ifdef LIB_BUILD_DLL										//dll build.
#define LIBUTIL_DLL_PORT __declspec(dllexport)		
#elif  defined LIB_USE_DLL									//use dll.
#define LIBUTIL_DLL_PORT __declspec(dllimport)
#else
#define LIBUTIL_DLL_PORT	
#endif	
#else
#define LIBUTIL_DLL_PORT
#endif
#ifdef __cplusplus
#define LIBUTILS_EXTERN_C extern"C"
#else
#define LIBUTILS_EXTERN_C extern
#endif

#define LIBAPI	LIBUTIL_DLL_PORT
#define LIBCAPI	LIBUTILS_EXTERN_C	LIBUTIL_DLL_PORT
extern int MainEnterance();
LIBCAPI int exportmain()
{
	int ret = MainEnterance();
	return ret;
}

















