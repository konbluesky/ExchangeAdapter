#ifndef _GTA_SENDER_H_
#define _GTA_SENDER_H_

#include <string>
#include <memory>
//#include <ace/Task.h>
#include "ThreadTask.h"
#include "threadlock.h"

namespace gta
{

	class CGTASenderMgr;
	class CGTATradeProvider;
	class CGTASender : public utils::ThreadTask//ACE_Task<ACE_MT_SYNCH>
	{
	public:
		//注：key在sender里并没有用，不过由于sender的内存是由智能指针管理的，当离开函数作用范围，如果没有任何对象对其
		//引用，则其会自动析构，因此由SenderMgr保留其的引用
		CGTASender(std::string key ,std::string addr,int port,CGTATradeProvider* pProvider);
		virtual ~CGTASender();
		

		bool SendData(char * pdata,int len);
		virtual int svc();
		std::string GetKey() const;
		void Dispose();
		int OldSenddataJob();
		
	private:
		std::string m_addr;
		int m_port;
		std::string m_key;
		char* m_pdata;
		int m_datalen;
		
		CGTATradeProvider* m_pProvider;
		int m_trytimes;
	};

}
#endif