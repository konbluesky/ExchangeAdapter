/******************************************************************************
 *  rv_DES.C                                                     VERSION 4.00    *
 *----------------------------------------------------------------------------*
 *  D A T A   E N C R Y P T I O N   S T A N D A R D                           *
 *  FEDERAL INFORMATION PROCESSING STANDARDS PUBLICATION (FIPS PUB) 46        *
 *  JANUARY 15, 1977                                                          *
 *----------------------------------------------------------------------------*
 *  This software was produced at the National Bureau of Standards (NBS) as   *
 *  a part of research efforts and for demonstration purposes only.  Our      *
 *  primary goals in its design did not include widespread use outside of     *
 *  our own laboratories.  Acceptance of this software implies that you       *
 *  agree to use it for non-commercial purposes only and that you agree to    *
 *  accept it as nonproprietary and unlicensed, not supported by NBS, and     *
 *  not carrying any warranty, either expressed or implied, as to its         *
 *  performance or fitness for any particular purpose.                        *
 *----------------------------------------------------------------------------*
 *  Cryptographic devices and technical data regarding them are subject to    *
 *  Federal Government export controls as specified in Title 22, Code of      *
 *  Federal Regulations, Parts 121 through 128.  Cryptographic devices        *
 *  implementing the Data Encryption Standard (rv_DES) and technical data        *
 *  regarding them must comply with these Federal regulations.                *
 *----------------------------------------------------------------------------*
 *  Produced at the National Bureau of Standards (NBS),                       *
 *  Institute for Computer Sciences and Technology (ICST).                    *
 *  by David M. Balenson, September 1986.                                     *
 ******************************************************************************/

#include <stdio.h>
#include <string.h>
#include "DES.h"



/* Key schedule of 16 48-bit subkeys generated from 64-bit key */
BYTE szKS[16][48];
/* rv_DES
   Description : Decrypt or Encrypt data with rv_DES algorithm
*/
/*------------------------------------------------------------------------------*/
static void rv_Pack8( BYTE *, BYTE * );
static void rv_Unpack8( BYTE *, BYTE * );
void	rv_DES_setkey( BYTE   Decrypt , BYTE * Key );
void	rv_DES_calcul( BYTE * Key_In  , BYTE * KeyOut );
void	rv_DES(BYTE Decrypt, BYTE *Key, BYTE *Data, BYTE *Result)
{
   /* Set decryption or encryption key parameter for rv_DES calculation */
   /*----------------------------------------------------------------*/
   rv_DES_setkey(Decrypt, Key);

   /* Decrypt or Encrypt the data and replace */
   /*-----------------------------------------*/
   rv_DES_calcul(Data, Result);
   return;
}

//将无符号字符串转换为0-F之间的ASC字符串
void	rv_BYTE2ASC(long inl_InLen, const BYTE *ins_InBuf, char *ins_OutBuf)
{

	register long	i, k = 0;
	register unsigned char	ch;
	
	for(i = 0; i < inl_InLen; i++)
	{
		ch = ins_InBuf[i] & '\x0F0';
		ch = ch >> 4;
		if(ch <= '\x09')
			ins_OutBuf[k] = '\x30' | ch;
		else
			ins_OutBuf[k] = '\x40' | (ch - '\x09');
		k++;

		ch = ins_InBuf[i] & '\x0f';
		if(ch <= '\x09')
			ins_OutBuf[k] = '\x30' | ch;
		else
			ins_OutBuf[k] = '\x40' | (ch - '\x09');
		k++;
	}
	ins_OutBuf[k] = 0;
}

//将ASC字符压缩为无符号字符串，输入串为奇数个字符时扔掉最后一个字符
void	rv_ASC2BYTE(const char *ins_InBuf, long *inl_OutLen, BYTE *ins_OutBuf)
{

	long		len;
	register long	i, k = 0;
	register unsigned char ch;
	
	len = strlen(ins_InBuf);


	for(i = 0; i < len; i = i + 2)
	{
		if(ins_InBuf[i] <= '9')
			ch = ins_InBuf[i] & '\x0f';
		else
			ch = (ins_InBuf[i] & '\x0f') + '\x09';

		ch = ch << 4;
		if(ins_InBuf[i+1] <= '9')
			ch |= ins_InBuf[i+1] & '\x0f';
		else
			ch |= (ins_InBuf[i+1] & '\x0f') + '\x09';

		ins_OutBuf[k] = ch;k++;
	}
	*inl_OutLen = k;
}

void	rv_Triple_DES(long Decrypt, BYTE* Key, BYTE* Data, BYTE* Result)
{

	BYTE	Tmp1[16], Tmp2[16];

	if(Decrypt == ENCRYPT)
	{
		rv_DES(ENCRYPT, Key    , Data, Tmp1  );
		rv_DES(DECRYPT, Key + 8, Tmp1, Tmp2  );
		rv_DES(ENCRYPT, Key    , Tmp2, Result);
	}
	else
	{
		rv_DES(DECRYPT, Key    , Data, Tmp1  );
		rv_DES(ENCRYPT, Key + 8, Tmp1, Tmp2  );
		rv_DES(DECRYPT, Key    , Tmp2, Result);
	}
	return;
}

/*==============================================================================*/
/*
   rv_DES_setkey
*/
/*------------------------------------------------------------------------------ */
void rv_DES_setkey( BYTE sw,             /* type cryption: 0=encrypt,1=decrypt   */
				 BYTE *pkey           /* 64-bit key packed into 8 bytes       */
			   )
{
	register WORD	i, j, k, t1, t2;
	static	BYTE CD[56];
	static	BYTE key[64];
	
	/* Schedule of left shifts for C and D blocks */
	unsigned short shifts[] = {1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1};

	/* PERMUTED CHOICE 1 (PC1) */
	WORD PC1[] = 
	{
			57, 49, 41, 33, 25, 17,  9,
			 1, 58, 50, 42, 34, 26, 18,
			10,  2, 59, 51, 43, 35, 27,
			19, 11,  3, 60, 52, 44, 36,
			63, 55, 47, 39, 31, 23, 15,
			 7, 62, 54, 46, 38, 30, 22,
			14,  6, 61, 53, 45, 37, 29,
			21, 13,  5, 28, 20, 12,  4
	};

   /* PERMUTED CHOICE 2 (PC2) */
	WORD PC2[] = 
	{
			14, 17, 11, 24,  1,  5,
			 3, 28, 15,  6, 21, 10,
			23, 19, 12,  4, 26,  8,
			16,  7, 27, 20, 13,  2,
			41, 52, 31, 37, 47, 55,
			30, 40, 51, 45, 33, 48,
			44, 49, 39, 56, 34, 53,
			46, 42, 50, 36, 29, 32
	};

	/* Assume default algorymth = crypt */
	if(sw != 0 && sw != 1) sw = 0;

	/* Unpack KEY from 8 bits/byte into 1 bit/byte */
	rv_Unpack8(pkey, key);

	/* Permute unpacked key with PC1 to generate C and D */
	for(i = 0; i < 56; i++)
		CD[i] = key[PC1[i] - 1];

	/* Rotate and permute C and D to generate 16 subkeys */
	for(i = 0; i < 16; i++)
	{
		/* Rotate C and D */
		for(j = 0; j < shifts[i]; j++)
		{
			t1 = CD[0];
			t2 = CD[28];
			for(k = 0; k < 27; k++)
			{
				CD[k] = CD[k+1];
				CD[k+28] = CD[k+29];
			}
			CD[27] = (unsigned char)t1;
			CD[55] = (unsigned char)t2;
		}
		/* Set order of subkeys for type of cryption */
		j = sw ? 15-i : i;
		/* Permute C and D with PC2 to generate szKS[j] */
		for(k = 0; k < 48; k++)
			szKS[j][k] = CD[PC2[k] - 1];
	}
   return;
}


/******************************************************************************
 *  rv_DES()                                                                     *
 ******************************************************************************/
void rv_DES_calcul( BYTE *in,             /* packed 64-bit INPUT block           */
				 BYTE *out             /* packed 64-bit OUTPUT block          */
			   )
{
	register WORD	i, j, k, t;
	static	BYTE block[64];            /* unpacked 64-bit input/output block  */
	static	BYTE LR[64], f[32], preS[48];

	/* INITIAL PERMUTATION (IP) */
	WORD IP[] = 
	{
			58, 50, 42, 34, 26, 18, 10,  2,
			60, 52, 44, 36, 28, 20, 12,  4,
			62, 54, 46, 38, 30, 22, 14,  6,
			64, 56, 48, 40, 32, 24, 16,  8,
			57, 49, 41, 33, 25, 17,  9,  1,
			59, 51, 43, 35, 27, 19, 11,  3,
			61, 53, 45, 37, 29, 21, 13,  5,
			63, 55, 47, 39, 31, 23, 15,  7
	};

	/* REVERSE FINAL PERMUTATION (IP-1) */
	WORD RFP[] = 
	{
			 8, 40, 16, 48, 24, 56, 32, 64,
			 7, 39, 15, 47, 23, 55, 31, 63,
			 6, 38, 14, 46, 22, 54, 30, 62,
			 5, 37, 13, 45, 21, 53, 29, 61,
			 4, 36, 12, 44, 20, 52, 28, 60,
			 3, 35, 11, 43, 19, 51, 27, 59,
			 2, 34, 10, 42, 18, 50, 26, 58,
			 1, 33,  9, 41, 17, 49, 25, 57
	};

   /* E BIT-SELECTION TABLE */
	WORD E[] = 
	{
			32,  1,  2,  3,  4,  5,
			 4,  5,  6,  7,  8,  9,
			 8,  9, 10, 11, 12, 13,
			12, 13, 14, 15, 16, 17,
			16, 17, 18, 19, 20, 21,
			20, 21, 22, 23, 24, 25,
			24, 25, 26, 27, 28, 29,
			28, 29, 30, 31, 32,  1
	};

	/* PERMUTATION FUNCTION P */
	WORD P[] = 
	{
			16,  7, 20, 21,
			29, 12, 28, 17,
			 1, 15, 23, 26,
			 5, 18, 31, 10,
			 2,  8, 24, 14,
			32, 27,  3,  9,
			19, 13, 30,  6,
			22, 11,  4, 25
	};

	/* 8 S-BOXES */
	WORD S[8][64] = 
	{
			14,  4, 13,  1,  2, 15, 11,  8,  3, 10,  6, 12,  5,  9,  0,  7,
			 0, 15,  7,  4, 14,  2, 13,  1, 10,  6, 12, 11,  9,  5,  3,  8,
			 4,  1, 14,  8, 13,  6,  2, 11, 15, 12,  9,  7,  3, 10,  5,  0,
			15, 12,  8,  2,  4,  9,  1,  7,  5, 11,  3, 14, 10,  0,  6, 13,

			15,  1,  8, 14,  6, 11,  3,  4,  9,  7,  2, 13, 12,  0,  5, 10,
			 3, 13,  4,  7, 15,  2,  8, 14, 12,  0,  1, 10,  6,  9, 11,  5,
			 0, 14,  7, 11, 10,  4, 13,  1,  5,  8, 12,  6,  9,  3,  2, 15,
			13,  8, 10,  1,  3, 15,  4,  2, 11,  6,  7, 12,  0,  5, 14,  9,

			10,  0,  9, 14,  6,  3, 15,  5,  1, 13, 12,  7, 11,  4,  2,  8,
			13,  7,  0,  9,  3,  4,  6, 10,  2,  8,  5, 14, 12, 11, 15,  1,
			13,  6,  4,  9,  8, 15,  3,  0, 11,  1,  2, 12,  5, 10, 14,  7,
			 1, 10, 13,  0,  6,  9,  8,  7,  4, 15, 14,  3, 11,  5,  2, 12,

			 7, 13, 14,  3,  0,  6,  9, 10,  1,  2,  8,  5, 11, 12,  4, 15,
			13,  8, 11,  5,  6, 15,  0,  3,  4,  7,  2, 12,  1, 10, 14,  9,
			10,  6,  9,  0, 12, 11,  7, 13, 15,  1,  3, 14,  5,  2,  8,  4,
			 3, 15,  0,  6, 10,  1, 13,  8,  9,  4,  5, 11, 12,  7,  2, 14,

			 2, 12,  4,  1,  7, 10, 11,  6,  8,  5,  3, 15, 13,  0, 14,  9,
			14, 11,  2, 12,  4,  7, 13,  1,  5,  0, 15, 10,  3,  9,  8,  6,
			 4,  2,  1, 11, 10, 13,  7,  8, 15,  9, 12,  5,  6,  3,  0, 14,
			11,  8, 12,  7,  1, 14,  2, 13,  6, 15,  0,  9, 10,  4,  5,  3,

			12,  1, 10, 15,  9,  2,  6,  8,  0, 13,  3,  4, 14,  7,  5, 11,
			10, 15,  4,  2,  7, 12,  9,  5,  6,  1, 13, 14,  0, 11,  3,  8,
			 9, 14, 15,  5,  2,  8, 12,  3,  7,  0,  4, 10,  1, 13, 11,  6,
			 4,  3,  2, 12,  9,  5, 15, 10, 11, 14,  1,  7,  6,  0,  8, 13,

			 4, 11,  2, 14, 15,  0,  8, 13,  3, 12,  9,  7,  5, 10,  6,  1,
			13,  0, 11,  7,  4,  9,  1, 10, 14,  3,  5, 12,  2, 15,  8,  6,
			 1,  4, 11, 13, 12,  3,  7, 14, 10, 15,  6,  8,  0,  5,  9,  2,
			 6, 11, 13,  8,  1,  4, 10,  7,  9,  5,  0, 15, 14,  2,  3, 12,

			13,  2,  8,  4,  6, 15, 11,  1, 10,  9,  3, 14,  5,  0, 12,  7,
			 1, 15, 13,  8, 10,  3,  7,  4, 12,  5,  6, 11,  0, 14,  9,  2,
			 7, 11,  4,  1,  9, 12, 14,  2,  0,  6, 10, 13, 15,  3,  5,  8,
			 2,  1, 14,  7,  4, 10,  8, 13, 15, 12,  9,  0,  3,  5,  6, 11
	};

	/* Unpack the INPUT block */
	rv_Unpack8(in, block);

	/* Permute unpacked input block with IP to generate L and R */
	for(j = 0; j < 64; j++)
		LR[j] = block[IP[j] - 1];

	/* Perform 16 rounds */
	for(i = 0; i < 16; i++)
	{
		/* Expand R to 48 bits with E and XOR with i-th subkey */
		for(j = 0; j < 48; j++)
			preS[j] = LR[E[j]+31] ^ szKS[i][j];

		/* Map 8 6-bit blocks into 8 4-bit blocks using S-Boxes */
		for(j = 0; j < 8; j++) 
		{
			/* Compute index t into j-th S-box */
			k = 6 * j;
			t = preS[k];
			t = ( t << 1 ) | preS[k+5];
			t = ( t << 1 ) | preS[k+1];
			t = ( t << 1 ) | preS[k+2];
			t = ( t << 1 ) | preS[k+3];
			t = ( t << 1 ) | preS[k+4];

			/* Fetch t-th entry from j-th S-box */
			t = S[j][t];
			/* Generate 4-bit block from S-box entry */
			k = 4 * j;
			f[k]   = ( t >> 3 ) & 1;
			f[k+1] = ( t >> 2 ) & 1;
			f[k+2] = ( t >> 1 ) & 1;
			f[k+3] = t & 1;
		}

		for(j = 0; j < 32; j++)
		{
			/* Copy R */
			t = LR[j+32];
			/* Permute f w/ P and XOR w/ L to generate new R */
			LR[j+32] = LR[j] ^ f[P[j]-1];
			/* Copy original R to new L */
			LR[j] = (unsigned char)t;
		}
	}

	/* Permute L and R with reverse IP-1 to generate output block */
	for(j = 0; j < 64; j++)
		block[j] = LR[RFP[j] - 1];

	/* Pack data into 8 bits per byte */
	rv_Pack8(out, block);
}


/******************************************************************************
 *  rv_Pack8()  Pack 64 bytes at 1 bit/byte into 8 bytes at 8 bits/byte          *
 ******************************************************************************/
static void	rv_Pack8(
		BYTE	*packed,               /* packed block ( 8 bytes at 8 bits/byte)      */
		BYTE	*binary                /* the unpacked block (64 bytes at 1 bit/byte) */
				)
{
	register WORD	i, j, k;

	for(i = 0; i < 8; i++)
	{
		k = 0;
		for(j = 0; j < 8; j++)
			k = ( k<<1 ) + *binary++;
		*packed++ = (unsigned char)k;
	}
}

/******************************************************************************
 *  rv_Unpack8()  Unpack 8 bytes at 8 bits/byte into 64 bytes at 1 bit/byte      *
 ******************************************************************************/
static void	rv_Unpack8(
		BYTE	*packed,             /* packed block (8 bytes at 8 bits/byte)         */
		BYTE	*binary              /* unpacked block (64 bytes at 1 bit/byte)       */
					)
{
	register WORD	i, j, k;

	for(i = 0; i < 8; i++)
	{
		k = *packed++;
		for(j = 0; j < 8; j++)
			*binary++ = ( k >> (7-j) ) & 0x01;
	}
}
/******************************************************************************/

/*
**	要求输入的数据必须为
*/
long	rl_DESEnc_BYTE(long inl_KeyLen, BYTE *inb_Key, long inl_InLen, 
				BYTE *inb_InBuf, long *inl_OutLen, BYTE *inb_OutBuf)
{
	long	i;
	BYTE	b_buf[8];

	//判断输入的密钥长度
	if(inl_KeyLen !=8 && inl_KeyLen != 16)
	{
		return -1;
	}
	
	for(i = 0; i < inl_InLen / 8; i++)
	{
		//根据密钥长度的不同选择不同的算法
		if(inl_KeyLen == 8) 
			rv_DES(ENCRYPT, inb_Key, inb_InBuf + i * 8, inb_OutBuf + i * 8);
		else
			rv_Triple_DES(ENCRYPT, inb_Key, inb_InBuf + i * 8, inb_OutBuf + i * 8);
	}

	if((inl_InLen % 8) != 0)
	{
		memset(b_buf, 0, sizeof(b_buf));
		memcpy(b_buf, inb_InBuf + i * 8, inl_InLen % 8);
		b_buf[inl_InLen % 8] = '\x80';
		if(inl_KeyLen ==8)
			rv_DES(ENCRYPT, inb_Key, b_buf, inb_OutBuf + i * 8);
		else
			rv_Triple_DES(ENCRYPT, inb_Key, b_buf, inb_OutBuf + i * 8);
		i++;
	}
	*inl_OutLen = i * 8;
	return 0;
}

//基于BYTE类型的rv_DES解密实现
//密钥必须为8或16位, 解密数据必须为8的倍数
long	rl_DESDec_BYTE(long inl_KeyLen, BYTE *inb_Key,
					   long inl_InLen,  BYTE *inb_InBuf, 
					   long	*inl_OutLen, BYTE *inb_OutBuf)
{
	long	i;

	if(inl_KeyLen != 8 && inl_KeyLen != 16)
	{
		
		return -1;
	}
	
	if(inl_InLen % 8)
	{
		
		return -1;
	}
	
	for(i = 0; i < inl_InLen / 8; i++)
	{
		if( inl_KeyLen ==8)
			rv_DES(DECRYPT, inb_Key, inb_InBuf + i * 8, inb_OutBuf + i*8);
		else
			rv_Triple_DES(DECRYPT, inb_Key, inb_InBuf + i*8, inb_OutBuf + i*8);
	}
	*inl_OutLen = inl_InLen;
	return 0;
}

//基于ASCII形式输入的rv_DES加密处理，输入输出和密钥都为ASC串
long	rl_DESEnc_ASC(char *ins_Key, char *ins_InBuf, char *ins_OutBuf)
{
	long	l_InLen, l_OutLen, l_KeyLen;
	BYTE	b_Key[17];
	BYTE	szInByteBuf[256], szOutByteBuf[256];

	if(strlen(ins_Key) != 16 && strlen(ins_Key) != 32)
	{
		
		return -1;
	}
	rv_ASC2BYTE(ins_Key, &l_KeyLen, b_Key);
	//没有足够空间
	if(sizeof(szInByteBuf) <= strlen(ins_InBuf) 
		||	sizeof(szOutByteBuf) <= strlen(ins_InBuf))
	{
		
		return -1;
	}
	rv_ASC2BYTE(ins_InBuf, &l_InLen, szInByteBuf);
	if(rl_DESEnc_BYTE(l_KeyLen, b_Key, l_InLen, szInByteBuf, &l_OutLen, szOutByteBuf))
	{
		return -1;
	}
	rv_BYTE2ASC(l_OutLen, szOutByteBuf, ins_OutBuf);
	return 0;
}

//基于ASCII形式输入的rv_DES解密处理，输入输出和密钥都为ASC串
long	rl_DESDec_ASC(char *ins_Key, char *ins_InBuf, char *ins_OutBuf)
{
	long	l_InLen, l_OutLen, l_KeyLen;
	BYTE	b_Key[17];
	BYTE	szInByteBuf[256], szOutByteBuf[256];

	if(strlen(ins_Key) != 16 && strlen(ins_Key) != 32)
	{
		
		return -1;
	}
	rv_ASC2BYTE(ins_Key, &l_KeyLen, b_Key);
	//没有足够空间
	if(sizeof(szInByteBuf) <= strlen(ins_InBuf) 
		||	sizeof(szOutByteBuf) <= strlen(ins_InBuf))
	{
		
		return -1;
	}

	rv_ASC2BYTE(ins_InBuf, &l_InLen, szInByteBuf);
	if(rl_DESDec_BYTE(l_KeyLen, b_Key, l_InLen, szInByteBuf, &l_OutLen, szOutByteBuf))
	{
		return -1;
	}
	rv_BYTE2ASC(l_OutLen, szOutByteBuf, ins_OutBuf);
	return 0;
}

