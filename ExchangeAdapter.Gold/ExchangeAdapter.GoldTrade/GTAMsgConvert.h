/***********************************************************************
* Module:  CTrMsgConvert.h
* Author:  Administrator
* Modified: 2007年8月31日 9:33:34
* Purpose: Declaration of the class CTrMsgConvert
* Comment: 交行黄金交易系统日间交易有序报文和API使用的结构类实现互转功能。转换分为调用API输入转化和API回调输出转化，转化调用时需要传入转化函数名，转化输入报文，转化输出报文及其他标志等
*    写日志说明，转化所有域值都写到MY_D2中，错误都写到MY_ERR中
*    如写错误日志：rv_TraceLog(MY_ERR, "IP[%s]交易码[%s]发送数据错%s", szIP, szTrCode, m_szErrMsg)
***********************************************************************/

#if !defined(__TrGoldCtrl_CTrMsgConvert_h)
#define __TrGoldCtrl_CTrMsgConvert_h


#include "APIStruct.h"
#include <string>
#include <map>
namespace gta
{
	class CGTAMsgConvert 
	{
	public:
		static CGTAMsgConvert& Instance();


		//////////////////////
	public:
		long m_lFlag;
		/**********************************************************
		* 函 数 名： CTrMsgConvert::rpsz_GetErrMsg
		* 功能描述：返回错误信息
		* 返回说明： 返回错误信息
		* 开发历史：xxxx   于 200709XX 创建
		************************************************************/
		char * GetErrorMsg(void);
		/**********************************************************
		* 函 数 名： rl_BuildRspErrMsg
		* 功能描述：   将传入的ErrMsg生成返回平台报文
		* 参数：
		*- in_pszErrNo      输入 --错误代码
		*- in_pszErrMsg      输入 --错误信息
		*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
		*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
		* 返回说明： 成功--转化报文长度，失败--小于0
		************************************************************/
		long rl_BuildRspErrMsg(char * in_pszErrNo, char * in_pszErrMsg, char * out_pszOutMsg, long in_lOutMsgSize);
		/**********************************************************
		* 函 数 名： rl_SetSystemParameter
		* 功能描述： 设置系统参数
		* 参数：
		*- in_blNtfWriteLogFlag 输入 --通知回报写日志标志，为false时表示除ERR之外的日志都不写
		*- in_poCrypticDES   输入 --加解密类对象指针
		*- in_pszWorkHomeDir	输入 --工作目录
		*- in_pszLogMask	输入-- 日志掩码
		* 返回说明： 成功--0，失败--小于0
		************************************************************/
		long rl_SetSystemParameter(bool in_blNtfWriteLogFlag, char * in_pszWorkHomeDir, char * in_pszLogMask);


		/* 存放错误信息 */
		char m_szErrMsg[512];
		//通知回报写日志标志
		bool m_blNtfWriteLogFlag;















		/////////////////
	public:
		/**********************************************************
		* 函 数 名： rl_RunInAPI
		* 功能描述：   将平台报文转化为调用API输入报文的总调入口
		* 参数：
		*- in_pszMethod      输入 --调用方法名称
		*- in_pszInMsg      输入 --待转化报文
		*- in_lInMsgLength      输入 --待转化报文长度
		*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
		*- in_lOutMsgSize      输入 --输出空间的大小
		* 返回说明： 成功--转化报文长度，失败--小于0
		************************************************************/
		long ConvertString2Struct(const char * in_pszMethod, char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize);
		/**********************************************************
		* 函 数 名： rl_RunOutAPI
		* 功能描述：   将回调API响应报文转化为平台报文的总调入口
		* 参数：
		*- in_pszMethod      输入 --调用方法名称
		*- in_pszInMsg      输入 --待转化报文
		*- in_lInMsgLength      输入 --待转化报文长度
		*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
		*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
		*- inout_plOutMsgPos         输入输出 --保存在outMsg报文当前保存位置
		*- inout_ppszOutFileName         输入输出 --存文件的文件名
		*- out_pblFollowupFlag         输出 --是否有后续报文
		* 返回说明： 成功--转化报文长度，失败--小于0
		************************************************************/
		long rl_RunOutAPI(char * in_pszMethod, char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag);
		/**********************************************************
		* 函 数 名： rl_RunRspInitiative
		* 功能描述：   将主动回报API报文转化为平台报文的总调入口
		* 参数：
		*- in_pszMethod      输入 --调用方法名称
		*- in_pszInMsg      输入 --待转化报文
		*- in_lInMsgLength      输入 --待转化报文长度
		*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
		*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
		*- in_pszTrCode      输入 --交易代号
		*- in_lConvertFlag      输入 --需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文
		* 返回说明： 成功--转化报文长度，失败--小于0
		************************************************************/


		/**********************************************************
		* 函 数 名： CTrMsgConvert
		* 功能描述：
		* 返回说明： Constructor
		************************************************************/
		CGTAMsgConvert();


	protected:
		void InitInAPIAFunc();


	private:
		/*    InAPI-将平台交易员登录报文转化为调用API输入报文 */
		long rl_ReqTrLogin(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize);
		/*    OutAPI-将交易员登录响应回调API报文转化为平台报文 */
		long rl_RspTrLogin(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag);
		/*    InAPI-将平台交易员登出报文转化为调用API输入报文 */
		long rl_ReqTrLogout(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize);
		/*    OutAPI-将交易员登出响应回调API报文转化为平台报文 */
		long rl_RspTrLogout(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag);
		/*    InAPI-将平台交易员修改密码报文转化为调用API输入报文 */
		long rl_ReqTrPwdUpdate(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize);
		/*    OutAPI-将交易员密码修改响应回调API报文转化为平台报文 */
		long rl_RspTrPwdUpdate(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag);
		/*    InAPI-将平台会员修改应急密码报文转化为调用API输入报文 */
		long rl_ReqMemberUrgentPwdUpdate(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize);
		/*    OutAPI-将会员修改应急密码响应回调API报文转化为平台报文 */
		long rl_RspMemberUrgentPwdUpdate(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag);
		/*    InAPI-将平台会员资金查询报文转化为调用API输入报文 */
		long rl_ReqQryMemberCapital(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize);
		/*    OutAPI-将会员资金查询响应回调API报文转化为平台报文 */
		long rl_RspQryMemberCapital(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag);
		/*    InAPI-将平台客户库存查询报文转化为调用API输入报文 */
		long rl_ReqQryClientStorage(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize);
		/*    OutAPI-将客户库存查询响应回调API报文转化为平台报文 */
		long rl_RspQryClientStorage(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag);
		/*    InAPI-将平台交易所公告查询报文转化为调用API输入报文 */
		long rl_ReqQryBulletinBoard(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize);
		/*    OutAPI-将交易所公告查询响应回调API报文转化为平台报文 */
		long rl_RspQryBulletinBoard(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag);
		/*    InAPI-将平台国际行情查询报文转化为调用API输入报文 */
		long rl_ReqQryInterQuotation(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize);
		/*    OutAPI-将国际行情查询响应回调API报文转化为平台报文 */
		long rl_RspQryInterQuotation(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag);
		/*    InAPI-将平台会员客户查询报文转化为调用API输入报文 */
		long rl_ReqQryClient(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize);
		/*    OutAPI-将会员客户查询响应回调API报文转化为平台报文 */
		long rl_RspQryClient(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag);
		/*    InAPI-将平台现货报单请求报文转化为调用API输入报文 */
		long rl_ReqSpotOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize);
		/*    OutAPI-将现货报单和现货报单查询响应回调API报文转化为平台报文 */
		long rl_RspSpotOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag);
		/*    InAPI-将平台现货撤单请求报文转化为调用API输入报文 */
		long rl_ReqSpotOrderCancel(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize);
		/*    OutAPI-将现货撤单响应回调API报文转化为平台报文 */
		long rl_RspSpotOrderCancel(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag);
		/*    InAPI-将平台现货报单查询报文转化为调用API输入报文 */
		long rl_ReqQrySpotOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize);
		/*    InAPI-将平台现货成交单查询报文转化为调用API输入报文 */
		long rl_ReqQrySpotMatch(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize);
		/*    OutAPI-将现货成交单查询响应回调API报文转化为平台报文 */
		long rl_RspQrySpotMatch(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag);
		/*    InAPI-将平台现货行情查询请求报文转化为调用API输入报文 */
		long rl_ReqQrySpotQuotation(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize);
		/*    OutAPI-将现货行情查询响应回调API报文转化为平台报文 */
		long rl_RspQrySpotQuotation(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag);
		/*    InAPI-将平台现货分钟行情查询请求报文转化为调用API输入报文 */
		long rl_ReqQrySpotMinsQuotation(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize);
		/*    OutAPI-将现货分钟行情查询响应回调API报文转化为平台报文 */
		long rl_RspQrySpotMinsQuotation(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag);
		/*    RspInitiative将现货报单回报API报文转化为平台请求报文 */
		long rl_OnRecvRtnSpotOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag);
		/*    RspInitiative将现货撤单回报API报文转化为平台请求报文 */
		long rl_OnRecvRtnSpotOrderCancel(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag);
		/*    RspInitiative将现货成交回报API报文转化为平台请求报文 */
		long rl_OnRecvRtnSpotMatch(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag);
		/*    RspInitiative将市场信息回报API报文转化为平台请求报文 */
		long rl_OnRecvMarketInfo(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag);
		/*    RspInitiative将现货合约信息回报API报文转化为平台请求报文 */
		long rl_OnRecvSpotInstInfo(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag);
		/*    RspInitiative将现货合约交易状态改变回报API报文转化为平台请求报文 */
		long rl_OnRecvRtnSpotInstStateUpdate(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag);
		/*    RspInitiative将现货市场交易状态改变回报API报文转化为平台请求报文 */
		long rl_OnRecvRtnSpotMarketStateUpdate(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag);
		/*    RspInitiative将公告回报API报文转化为平台请求报文 */
		long rl_OnRecvRtnBulletinBoard(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag);
		/*    RspInitiative将国际行情回报API报文转化为平台请求报文 */
		long rl_OnRecvRtnInterQuotation(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag);
		/*    RspInitiative将现货行情回报API报文转化为平台请求报文 */
		long rl_OnRecvSpotQuotation(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag);
		/*    RspInitiative将当天现货分钟行情回报API报文转化为平台请求报文 */
		long rl_OnRecvMinsSpotQuotation(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag);
		/*    RspInitiative将交割品种代码信息回报API报文转化为平台请求报文 */
		long rl_OnRecvVarietyInfo(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag);
		/* RspInitiative将路线丢失的回报API报文转化为平台请求报文*/
		long rl_OnChannelLost(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag);

		/************************add by wangfei for T+D 091009********************/
		/* RspInitiative将当天Au(T+D)分钟行情回报API报文转化为平台请求报文*/
		//	long rl_OnRecvMinsDeferQuotation(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag);
		long rl_OnRecvDeferQuotation(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag);

		/*    InAPI-将平台现货延期报单请求报文转化为调用API输入报文*/
		long rl_ReqDeferOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize);
		/*    OutAPI-将现货延期报单和现货延期报单查询响应回调API报文转化为平台报文 */
		long rl_RspDeferOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag);
		/* RspInitiative将现货延期报单和现货延期报单查询响应回调API报文转化为平台报文*/
		long rl_OnRecvRtnDeferOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag);
		/* RspInitiative将现货延期成交回报API报文转化为平台报文*/
		long rl_OnRecvRtnDeferMatch(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag);
		/*    InAPI-将平台现货延期交收撤单请求报文转化为调用API输入报文*/
		long rl_ReqDeferOrderCancel(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize);
		/*    InAPI-将平台现货延期交收撤单请求相应回调API报文转化为平台报文*/
		long rl_RspDeferOrderCancel(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag);
		/*    RspInitiative将现货延期撤单回报API报文转化为平台请求报文*/
		long rl_OnRecvRtnDeferOrderCancel(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag);
		/*    InAPI-将平台现货延期交收交割报单请求报文转化为调用API输入报文*/
		long rl_ReqDeferDeliveryAppOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize);
		/*    InAPI-将平台现货延期交收交割请求相应回调API报文转化为平台报文*/
		long rl_RspDeferDeliveryAppOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag);
		/* RspInitiative将现货延期交收交割报单和现货延期交收交割报单查询响应回调API报文转化为平台报文*/
		long rl_OnRecvRtnDeferDeliveryAppOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag);
		/* RspInitiative将现货延期交收交割申报成交回报回调API报文转化为平台报文*/
		long rl_OnRecvRtnDeferDeliveryAppOrderMatch(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag);
		/*    InAPI-将平台现货延期中立仓申报请求报文转化为调用API输入报文*/
		long rl_ReqMiddleAppOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize);
		/*    OutAPI-将现货延期中立仓申报响应回调API报文转化为平台报文 */
		long rl_RspMiddleAppOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag);
		/* RspInitiative将现货延期中立仓申报回调API报文转化为平台报文*/
		long rl_OnRecvRtnMiddleAppOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag);
		/*    InAPI-将平台现货延期中立仓撤销申报请求报文转化为调用API输入报文*/
		long rl_ReqMiddleAppOrderCancel(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize);
		/*    OutAPI-将现货延期中立仓申报撤销响应回调API报文转化为平台报文 */
		long rl_RspMiddleAppOrderCancel(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag);
		/*    RspInitiative将现货延期中立仓申报撤单回报API报文转化为平台请求报文*/
		long rl_OnRecvRtnMiddleAppOrderCancel(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag);
		/*    InAPI-将平台现货延期交收交割撤销请求报文转化为调用API输入报文*/
		long rl_ReqDeferDeliveryAppOrderCancel(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize);
		/*    OutAPI-将现货延期交收交割申报撤销响应回调API报文转化为平台报文 */
		long rl_RspDeferDeliveryAppOrderCancel(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag);
		/*    RspInitiative将现货延期交收交割申报撤单回报API报文转化为平台请求报文*/
		long rl_OnRecvRtnDeferDeliveryAppOrderCancel(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag);
		/*    InAPI-将客户现货延期交收持仓查询报文转化为调用API输入报文 */
		long rl_ReqQryDeferClientPosi(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize);
		/*    OutAPI-将客户现货延期交收持仓查询响应回调API报文转化为平台报文 */
		long rl_RspQryDeferClientPosi(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag);
		/*    InAPI-将现货延期交收交割申报成交单查询报文转化为调用API输入报文 */
		long rl_ReqQryDeferDeliveryAppMatch(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize);
		/*    OutAPI-将现货延期交收交割申报成交单查询响应回调API报文转化为平台报文 */
		long rl_RspQryDeferDeliveryAppMatch(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag);
		/*    InAPI-将现货延期交收成交单查询请求报文转化为调用API输入报文 */
		long rl_ReqQryDeferMatch(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize);
		/*    OutAPI-将现货延期交收成交单查询应答回调API报文转化为平台报文 */
		long rl_RspQryDeferMatch(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag);
		/*    InAPI-将递延费率查询请求报文转化为调用API输入报文 */
		long rl_ReqQryDeferFeeRate(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize);
		/*    OutAPI-将递延费率查询请求应答回调API报文转化为平台报文 */
		long rl_RspQryDeferFeeRate(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag);
		/*    InAPI-将现货延期交收交割申报报单查询请求报文转化为调用API输入报文 */
		long rl_ReqQryDeferDeliveryAppOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize);
		/*    OutAPI-将现货延期交收交割申报报单查询应答响应回调API报文转化为平台报文 */
		long rl_RspQryDeferDeliveryAppOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag);
		/*    InAPI-将现货延期交收分钟行情查询请求报文转化为调用API输入报文 */
		long rl_ReqQryDeferMinsQuotation(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize);
		/*    OutAPI-将现货延期交收分钟行情查询应答响应回调API报文转化为平台报文 */
		long rl_RspQryDeferMinsQuotation(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag);
		/*    InAPI-将会员现货延期交收持仓查询请求报文转化为调用API输入报文 */
		long rl_ReqQryDeferMemberPosi(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize);
		/*    OutAPI-将会员现货延期交收持仓查询应答回调API报文转化为平台报文 */
		long rl_RspQryDeferMemberPosi(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag);
		/*    InAPI-将现货延期交收行情查询请求报文转化为调用API输入报文 */
		long rl_ReqQryDeferQuotation(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize);
		/*    OutAPI-将现货延期交收行情查询应答回调API报文转化为平台报文 */
		long rl_RspQryDeferQuotation(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag);
		/*    InAPI-将现货延期交收报单查询请求报文转化为调用API输入报文 */
		long rl_ReqQryDeferOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize);
		/*    OutAPI-将现货延期交收报单查询请求应答回调API报文转化为平台报文 */
		long rl_RspQryDeferOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag);
		/*    RspInitiative将现货延期交收交割行情回报API报文转化为平台报文*/
		long rl_OnRecvDeferDeliveryQuotation(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag);
		/*    RspInitiative将现货延期递延市场交易状态改变回报API报文转化为平台报文*/
		long rl_OnRecvRtnDeferMarketStateUpdate(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag);
		/*    RspInitiative将递延合约信息回报API报文转化为平台报文*/
		long rl_OnRecvDeferInstInfo(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag);
		/*    RspInitiative将递延合约交易状态改变回报API报文转化为平台报文*/
		long rl_OnRecvRtnDeferInstStateUpdate(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag);
		/*    RspInitiative将发布现货延期交收补偿费率通知API报文转化为平台报文*/
		long rl_OnRecvDeferFeeRateUpdate(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag);

		/************************add by wangfei for T+D end********************/


		/************************add by sunwenna for 本地保单查询 110715********************/
		/*    InAPI-将现货本地保单号查询请求报文转化为调用API输入报文*/
		long rl_ReqQrySpotLocalOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize);
		/*    OutAPI-将现货本地保单号查询应答回调API报文转化为平台报文 */
		long rl_RspQrySpotLocalOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag);
		/*    RspInitiative将现货本地保单号查询通知API报文转化为平台报文*/
		long rl_onRecvRspQrySpotLocalOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag);
		/*    InAPI-将现货延期交收本地保单号查询请求报文转化为调用API输入报文*/
		long rl_ReqQryDeferLocalOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize);
		/*    OutAPI-将现货延期交收本地保单号查询应答回调API报文转化为平台报文 */
		long rl_RspQryDeferLocalOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag);
		/*    RspInitiative将现货延期交收本地保单号查询通知API报文转化为平台报文*/
		long rl_onRecvRspQryDeferLocalOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag);
		/*    InAPI-将现货T+N本地保单号查询请求报文转化为调用API输入报文*/
		long rl_ReqQryForwardLocalOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize);
		/*    OutAPI-将现货T+N本地保单号查询应答回调API报文转化为平台报文 */
		long rl_RspQryForwardLocalOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag);
		/*    RspInitiative将现货T+N本地保单号查询通知API报文转化为平台报文*/
		long rl_onRecvRspQryForwardLocalOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag);
		/************************add by sunwenna for 本地保单查询 110715 end********************/






		/* 需要建立平台报文转化为调用API输入的内部函数列表结构体数组，包含函数名字和调用方法
		*/
		//static TInAPIFunc m_stInAPIAFunc[];
		typedef long (CGTAMsgConvert::*pInFunc)(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize);

		std::map<std::string,pInFunc> m_stInAPIAFunc;








		////////////===chh
	public:

		static bool onRecvRspSpotOrder(TSpotOrder * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize);

		static bool  onRecvRtnSpotOrder(TSpotOrder * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize);

		static bool  onRecvRtnSpotMatch(TSpotMatch * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize);

		static bool  onRecvRspSpotOrderCancel(TSpotOrderCancel * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize);
		static bool  onRecvRspQrySpotOrder(TSpotOrder * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize);
		//现货成交查询
		static bool  onRecvRspQrySpotMatch(TSpotMatch * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize);
		//现货撤单回报
		static bool  onRecvRtnSpotOrderCancel(TSpotOrder * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize);
		//市场信息回报
		static bool  onRecvMarketInfo(TMarket * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize);
		//现货合约信息回报
		static bool  onRecvSpotInstInfo(TSpotInst * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize);
		//交易员登陆响应
		static bool  onRecvRspTraderLogin(TReqTraderLogin * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize);

		//交易员登出响应
		static bool  onRecvRspTraderLogout(TReqTraderLogout * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize);

		//现货合约状态改变
		static bool  onRecvRtnSpotInstStateUpdate(TInstState * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize);

		//现货市场状态改变
		static bool  onRecvRtnSpotMarketStateUpdate(TMarketState * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize);
		//现货行情
		static bool  onRecvSpotQuotation(TSpotQuotation * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize);
		//交割品种代码信息
		static bool  onRecvVarietyInfo(TVariety * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize);
		//延期委托报单
		static bool  onRecvRspDeferOrder(TDeferOrder * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize);

		//延期委托报单
		static bool  onRecvRspDeferOrderCancel(TDeferOrderCancel * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize);

		//延期客户持仓查询
		static bool  onRecvRspQryDeferClientPosi(TDeferClientPosi * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize);
		//延期行情
		static bool  onRecvDeferQuotation(TDeferQuotation * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize);
		//延期报单反推
		static bool  onRecvRtnDeferOrder(TDeferOrder * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize);
		//延期成交反推
		static bool  onRecvRtnDeferMatch(TDeferMatch * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize);
		//延期撤单反推
		static bool  onRecvRtnDeferOrderCancel(TDeferOrder * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize);
		//延期市场状态改变
		static bool  onRecvRtnDeferMarketStateUpdate(TMarketState * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize);

	//延期合约信息
		static bool  onRecvDeferInstInfo(TDeferInst * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize);

	//延期合约状态改变
		static bool  onRecvRtnDeferInstStateUpdate(TInstState * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize);
	//延期费率改变
		static bool  onRecvDeferFeeRateUpdate(TDeferFeeRate * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize);

//延期交割申报反推
		static bool  onRecvRtnDeferDeliveryAppOrder(TDeferDeliveryAppOrder * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize);
		
		static  bool  OnChannelLost(char * in_pszInMsg, char * out_pszOutMsg, long* in_lOutMsgSize);
		static bool  OnRecvDeferDeliveryQuotation(TDeferDeliveryQuotation  *pstUsrData, TTRspMsg *pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize);
	};


}
#endif
