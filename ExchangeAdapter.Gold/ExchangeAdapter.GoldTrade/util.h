#ifndef __UTILS_H__
#define __UTILS_H__
#include <tchar.h>
#include <vector>
#include <string>


namespace utils
{
	

	class UFileSystemHelper
	{
	public:
		static std::string GetAppPath();
		static std::string GetAppDirectory();
		static std::string GetDirectory(const std::string& path);
		static std::string GetName(const std::string& path);

		static void SetAppDirectory();
	};

	

	///注：这里的内容append并没有具有完全的通用性，完全是GTA自带的编码规则。
	class CGTABufferBuilder
	{
	public:
#ifndef ASSERT
#define ASSERT(x)
#endif

		CGTABufferBuilder(char* buff,int startindex);
		int GetIndex() const;
		void FillBlock(int startindex,void* data,int datalen,int len);
		char* GetBuff();

		//CGTABufferBuilder& AppendString(char* strdata,int len);
		CGTABufferBuilder& AppendBlock(const void* block,int blocklen, int len);


		CGTABufferBuilder& AppendChar(char v);
		CGTABufferBuilder& AppendDouble(double v);
		CGTABufferBuilder& AppendInt(int v);
		//CGTABufferBuilder& AppendFormat(const char* fmt,...);
		static double rd_RoundFloat(double in_dData, long in_lDigital);
	private: 
		static void fastmemcpy(char* d,char* s,int len);
		int m_curindex;
		char* m_pbuff;
	};
	class UStringHelper
	{
	public:
		static char* TrimRight(char* in_szStr);		
		static char* Trim(char* p);
		static char* TrimLeft(char* in_szStr);
		static std::vector<std::string> Spilt(const char* input,const char* spiltter);
		static std::string ToUpper( const std::string& value );
		static std::string  ToLower( const std::string& value );

	};



	class UTF8GBKHelper
	{
	public:
		static void GBKToUTF8(char* &szOut);
		static void UTF8ToGBK( char *&szOut );
	};

	//注获取时间的函数，主要使用WIN32API: GetLocalTime
	class UTimeHelper
	{
	public:
		//格式：20110801
		static int GetCurrentDate();
		//格式：2011.08.07
		static std::string GetCurrentFormatDate();		
		//格式：12:11:32-019
		static std::string GetCurrentFormatTime();
		
		//注：这个是为RootCreate使用，用时间获取一个应用系统不会重复的long数据
		static long GetUniqLong();

	private:
		
	};
}

#endif