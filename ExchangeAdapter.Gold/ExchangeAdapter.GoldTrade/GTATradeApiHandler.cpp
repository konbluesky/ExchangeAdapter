#include "simsingleton.h"
#define TIXML_USE_STL
#include "tinyxml.h"
#include "GTASender.h"
#include "GTASenderMgr.h"
#include "GTATradeApiHandler.h"
#include "GTATradeProvider.h"
#include "GTATradeCodeMap.h"
#include "LogHelper.h"
#include "CCrypticDES.h"
using namespace simpattern;
namespace gta
{
	//这里对trade的调用需要进行顺序调用
	int CGTATradeApiHandler::Request( std::string tradecode,void* data1,std::string rootkey )
	{

		threadlock::autoguard<threadlock::multi_threaded_local> a(&m_lock);
		int ret = -1;
		char key[32] ={0};
		strcpy(key,rootkey.c_str());

		std::string funcname = CGTATradeCodeMap::Instance().m_map[tradecode].Apifunction;

		auto it = m_map.find(funcname);
		if (it != m_map.end())
		{
			PFUNC pfunc = it->second;
			try
			{		
				ret = ( this->*pfunc)(data1,key);		//TODO CHENHU 测试时使用，把功能函数给屏蔽掉
				//this->ReqDemo(data1,key);
			}
			catch(...)
			{
				WriteDetailLog("error",LTALL,("调用交易所API 出错"));
			}
		}
		return ret;
	}


	CGTATradeApiHandler::CGTATradeApiHandler()
	{		
		InitMap();
	}

	//注：从GTA过来的
	void CGTATradeApiHandler::InitMap()
	{
		LogCallStack(CGTATradeApiHandler_InitMap);
#define REGISTER_REQ_FUNCTION(funcname) m_map[#funcname] =(PFUNC) &CTraderApi::funcname

		REGISTER_REQ_FUNCTION(ReqDeferDeliveryAppOrder);		
		REGISTER_REQ_FUNCTION(ReqDeferDeliveryAppOrderCancel);
		REGISTER_REQ_FUNCTION(ReqDeferOrder);
		REGISTER_REQ_FUNCTION(ReqDeferOrderCancel);
		REGISTER_REQ_FUNCTION(ReqForwardEndorseOrder);
		REGISTER_REQ_FUNCTION(ReqForwardEndorseOrderCancel);
		REGISTER_REQ_FUNCTION(ReqForwardOrder);
		REGISTER_REQ_FUNCTION(ReqForwardOrderCancel);
		REGISTER_REQ_FUNCTION(ReqFuturesOrder);
		REGISTER_REQ_FUNCTION(ReqFuturesOrderCancel);
		REGISTER_REQ_FUNCTION(ReqMemberUrgentPasswordUpdate);
		REGISTER_REQ_FUNCTION(ReqMiddleAppOrder);
		REGISTER_REQ_FUNCTION(ReqMiddleAppOrderCancel);
		REGISTER_REQ_FUNCTION(ReqQryBulletinBoard);
		REGISTER_REQ_FUNCTION(ReqQryClient);
		REGISTER_REQ_FUNCTION(ReqQryClientStorage);
		REGISTER_REQ_FUNCTION(ReqQryDeferClientPosi);
		REGISTER_REQ_FUNCTION(ReqQryDeferDeliveryAppMatch);
		REGISTER_REQ_FUNCTION(ReqQryDeferDeliveryAppOrder);
		REGISTER_REQ_FUNCTION(ReqQryDeferFeeRate);
		REGISTER_REQ_FUNCTION(ReqQryDeferMatch);
		REGISTER_REQ_FUNCTION(ReqQryDeferMemberPosi);
		REGISTER_REQ_FUNCTION(ReqQryDeferMinsQuotation);
		REGISTER_REQ_FUNCTION(ReqQryDeferOrder);
		REGISTER_REQ_FUNCTION(ReqQryDeferQuotation);
		REGISTER_REQ_FUNCTION(ReqQryForwardClientPosi);
		REGISTER_REQ_FUNCTION(ReqQryForwardEndorseMatch);
		REGISTER_REQ_FUNCTION(ReqQryForwardEndorseOrder);
		REGISTER_REQ_FUNCTION(ReqQryForwardMatch);
		REGISTER_REQ_FUNCTION(ReqQryForwardMemberPosi);
		REGISTER_REQ_FUNCTION(ReqQryForwardMinsQuotation);
		REGISTER_REQ_FUNCTION(ReqQryForwardOrder);
		REGISTER_REQ_FUNCTION(ReqQryForwardQuotation);
		REGISTER_REQ_FUNCTION(ReqQryFuturesClientPosi);
		REGISTER_REQ_FUNCTION(ReqQryFuturesMatch);
		REGISTER_REQ_FUNCTION(ReqQryFuturesMemberPosi);
		REGISTER_REQ_FUNCTION(ReqQryFuturesMinsQuotation);
		REGISTER_REQ_FUNCTION(ReqQryFuturesOrder);
		REGISTER_REQ_FUNCTION(ReqQryFuturesQuotation);
		REGISTER_REQ_FUNCTION(ReqQryInterQuotation);
		REGISTER_REQ_FUNCTION(ReqQryMemberCapital);
		REGISTER_REQ_FUNCTION(ReqQryMiddleAppOrder);
		REGISTER_REQ_FUNCTION(ReqQrySpotMatch);
		REGISTER_REQ_FUNCTION(ReqQrySpotMinsQuotation);
		REGISTER_REQ_FUNCTION(ReqQrySpotOrder);
		REGISTER_REQ_FUNCTION(ReqQrySpotQuotation);
		REGISTER_REQ_FUNCTION(ReqSpotOrder);
		REGISTER_REQ_FUNCTION(ReqSpotOrderCancel);
		REGISTER_REQ_FUNCTION(ReqTraderLogin);
		//m_stInAPIAFunc1["ReqTraderLogin"]= (CTrMyTrader::PFUNC)&CTrMyTrader::rl_MyLogin;

		REGISTER_REQ_FUNCTION(ReqTraderLogout);
		REGISTER_REQ_FUNCTION(ReqTraderPasswordUpdate);
		REGISTER_REQ_FUNCTION(ReqQrySpotLocalOrder);
		REGISTER_REQ_FUNCTION(ReqQryDeferLocalOrder);
		REGISTER_REQ_FUNCTION(ReqQryForwardLocalOrder);			
	}

	bool CGTATradeApiHandler::Initialize( InitParam* pparam)
	{
		LogCallStack(CGTATradeApiHandler_Initialize);		

		int len =pparam->GoldICPWD.size();
		if(16 != len && 32 != len && 48 != len)
		{
			WriteDetailLog("error",LTALL,("配置文件里的PWD长度不对，应该是 16/32/48"));
			return false;
		}

		char pwd[65] = {0};
	
		Singleton_InFunc<CCrypticDES>::Instance().rl_GetDataFromSave((char*)pparam->GoldICPWD.c_str(),pwd,65);
		
		for(auto addr = pparam->addrs.begin(); addr != pparam->addrs.end(); addr++)
		{
			WriteLog("stdout",LTALL,"set up TradeAPI ip %s ,%d",addr->first.c_str(),addr->second);
			char saddr[65];
			strcpy(saddr,addr->first.c_str());
			this->SetService(saddr,addr->second);
		}
		int ret = this->init();
		if( ret !=0 )
		{
			WriteDetailLog("error",LTALL,("调用TradeApi::Init 初始化失败,CTrade::Init() = %d ",ret));
			WriteDetailLog("stdout",LTALL,("调用TradeApi::Init 初始化失败,CTrade::Init() = %d ",ret));
			return false;
		}
		WriteLog("log",LTALL,"CTrade::Init() successed");
		WriteLog("stdout",LTALL," call CTrade::Init() successed");

		this->SetIcCardPwd(pwd,pparam->ICMode,pparam->ICComID);		
		return true;
	}



	void CGTATradeApiHandler::Unitialize()
	{
	}

	int CGTATradeApiHandler::ReqDemo( void* data1,char* key )
	{
		static long TestOrderNumber = 111111;
		static long TestOrderMatchNumber = 111111;
		if(1)
		{
			TTRspMsg msg = {0};
			sprintf(msg.RspCode,"%20s","RSP000000");
			sprintf(msg.RspMsg,"%40s","0");
			this->onRecvRspSpotOrder((TSpotOrder*)data1, &msg,key);

			::InterlockedIncrement(&TestOrderNumber);
			char strOrderNumber[16]={0};
			sprintf(strOrderNumber,"%d",TestOrderNumber);

			TSpotOrder* data2=(TSpotOrder*)data1;
			strcpy(data2->orderNo,strOrderNumber);
			this->onRecvRtnSpotOrder(data2, &msg,key);

			::InterlockedIncrement(&TestOrderMatchNumber);
			char strmatchNo[16]={0};
			sprintf(strmatchNo,"%d",TestOrderMatchNumber);
			TSpotOrder* p = (TSpotOrder*)data1;
			TSpotMatch match;
			match.buyOrSell = p->buyOrSell;
			match.clientID = p->clientID;
			match.clientType = '1';
			match.instID = p->instID;
			match.localOrderNo = p->localOrderNo;
			match.matchDate = p->applyDate;
			strcpy(match.matchNo,strOrderNumber);

			match.matchTime = p->applyTime;
			match.matchType = p->matchType;
			match.memberID = p->memberID;
			match.orderNo = p->orderNo;
			match.price= p->price;
			match.volume = p->amount;


			this->onRecvRtnSpotMatch(&match, &msg, key);
		}
		return 0;
	}

	bool CGTATradeApiHandler::InitializeByXML( std::string fname )
	{
		TiXmlDocument doc(fname);
		InitParam param;
		bool loadOkay = doc.LoadFile();
		if (!loadOkay)
		{
			WriteLog("stdout",LTALL,"加载配置文件出错%s",fname.c_str());
			WriteLog("error",LTALL,"加载配置文件出错%s",fname.c_str());
			return false;
		}

		TiXmlNode* pNode = doc.FirstChild("configure");
		TiXmlNode* ele = pNode->FirstChild();



		while(ele != NULL)
		{
			TiXmlNode* addressNode;
			TiXmlNode* portNode;

			if(ele->ValueStr() == "saddress")
			{
				addressNode = ele->FirstChild("saddressip");				
				portNode = ele->FirstChild("iport");
				param.addrs.push_back(std::make_pair<std::string,int>(addressNode->FirstChild()->Value(),atoi(portNode->FirstChild()->Value())));
				WriteLog("log",LTALL,"金交所的ip %s : %d",addressNode->FirstChild()->Value(),atoi(portNode->FirstChild()->Value()));
			}
			ele = ele->NextSibling();
		}
		ele = pNode->FirstChild("demomode");
		if(ele!=NULL)
		{
			param.DemoMode = ele->ValueStr() == "true";
		}
		ele = pNode->FirstChild("GoldICPWD");
		if(ele != NULL)
		{
			param.GoldICPWD = ele->FirstChild()->Value();
		}
		ele = pNode->FirstChild("ICMode");
		param.ICMode = (short)atoi( ele->FirstChild()->Value());
		ele = pNode->FirstChild("ICComID");
		param.ICComID = (short)atoi(ele->FirstChild()->Value());




		ele = pNode->FirstChild("Notify");
		if(ele != NULL)
		{
			TiXmlNode* QuotationNode;
			QuotationNode = ele->FirstChild("WaitSpotTimeSecond");
			m_lNtfQuoationWaitSecond =atoi(QuotationNode->FirstChild()->Value()); 
			QuotationNode= ele->FirstChild("WaitDeferTimeSecond");
			m_lDeferNtfQuoationWaitSecond= atoi(QuotationNode->FirstChild()->Value());
			QuotationNode =ele->FirstChild("SpotInstIDList");

			auto f =utils::UStringHelper::Spilt(QuotationNode->FirstChild()->Value(),",");			

			for(auto it = f.begin(); it != f.end(); ++it)
			{
				TNtfQuotation a(*it,0);
				m_vNtfSpotQuotation.push_back(a);
			}

			QuotationNode = ele->FirstChild("DeferInstIDList");
			f = utils::UStringHelper::Spilt(QuotationNode->FirstChild()->Value(),",");
	
			for(auto  it = f.begin(); it != f.end(); ++it)
			{
				TNtfQuotation a(*it,0);
				m_vNtfDeferQuotation.push_back(a);				
			}			
		}

		//是否取消发送交割行情
		ele = pNode->FirstChild("DeliveryQuotationFlay");
		m_DeliveryQuotation = atoi(ele->FirstChild()->Value());
		ele = pNode->FirstChild("TimeNode");                     
		if(ele != NULL)
		{
			TiXmlNode* TimeNodeTemp;
			TimeNodeTemp = ele->FirstChild("BeginUnlimited1");
			m_szBeginUnlimited1 = TimeNodeTemp->FirstChild()->Value();

			TimeNodeTemp = ele->FirstChild("EndUnlimited1");
			m_szEndUnlimited1 = TimeNodeTemp->FirstChild()->Value();
			TimeNodeTemp = ele->FirstChild("BeginUnlimited2");
			m_szBeginUnlimited2 = TimeNodeTemp->FirstChild()->Value();
			TimeNodeTemp = ele->FirstChild("EndUnlimited2");
			m_szEndUnlimited2 = TimeNodeTemp->FirstChild()->Value();
			TimeNodeTemp = ele->FirstChild("BeginUnlimited3");
			m_szBeginUnlimited3 = TimeNodeTemp->FirstChild()->Value();
			TimeNodeTemp = ele->FirstChild("EndUnlimited3");
			m_szEndUnlimited3 = TimeNodeTemp->FirstChild()->Value();
		}

		return Initialize(&param);		
	}

	bool CGTATradeApiHandler::SetDataProcessor( CGTATradeProvider* Processor )
	{
		ExAssert(Processor != NULL,"");
		pProvider = Processor;
		return true;
	}



	std::vector<TNtfQuotation> & CGTATradeApiHandler::GetSpotQuotation()
	{
		return m_vNtfSpotQuotation;
	}

	std::vector<TNtfQuotation> & CGTATradeApiHandler::GetDeferQuotation()
	{
		return m_vNtfDeferQuotation;
	}

	void CGTATradeApiHandler::onRecvRspDeferOrderCancel( TDeferOrderCancel * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID )
	{
		WriteLog("stdout",LTALL,"not implemented onRecvRspDeferOrderCancel!!!");
	}

	void CGTATradeApiHandler::onRecvRspDeferDeliveryAppOrder( TDeferDeliveryAppOrder * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID )
	{
		WriteLog("stdout",LTALL,"not implemented onRecvRspDeferDeliveryAppOrder!!!");
	}

	void CGTATradeApiHandler::onRecvRspFuturesOrder( TFuturesOrder * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID )
	{
		WriteLog("stdout",LTALL,"not implemented onRecvRspFuturesOrder!!!");
	}

	void CGTATradeApiHandler::onRecvRspDeferDeliveryAppOrderCancel( TDeferDeliveryAppOrderCancel * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID )
	{
		WriteLog("stdout",LTALL,"not implemented onRecvRspDeferDeliveryAppOrderCancel!!!");
	}

	void CGTATradeApiHandler::onRecvRspMiddleAppOrder( TMiddleAppOrder * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID )
	{
		WriteLog("stdout",LTALL,"not implemented onRecvRspMiddleAppOrder!!!");
	}

	void CGTATradeApiHandler::onRecvDeferDeliveryQuotation( TDeferDeliveryQuotation * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID )
	{
		WriteLog("stdout",LTALL,"not implemented onRecvDeferDeliveryQuotation!!!");
	}

	void CGTATradeApiHandler::onRecvRspFuturesOrderCancel( TFuturesOrderCancel * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID )
	{
		WriteLog("stdout",LTALL,"not implemented onRecvRspFuturesOrderCancel!!!");
	}

	void CGTATradeApiHandler::onRecvRtnForwardEndorseMatch( TSpotMatch * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID )
	{
		WriteLog("stdout",LTALL,"not implemented onRecvRtnForwardEndorseMatch!!!");
	}

	void CGTATradeApiHandler::onRecvForwardInstInfo( TForwardInst * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID )
	{
		WriteLog("stdout",LTALL,"not implemented onRecvForwardInstInfo!!!");
	}

	void CGTATradeApiHandler::onRecvFuturesInstInfo( TFuturesInst * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID )
	{
		WriteLog("stdout",LTALL,"not implemented onRecvFuturesInstInfo!!!");
	}

	void CGTATradeApiHandler::onRecvRtnFuturesInstStateUpdate( TInstState * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID )
	{
		WriteLog("stdout",LTALL,"not implemented onRecvRtnFuturesInstStateUpdate!!!");
	}

	void CGTATradeApiHandler::onRecvRtnFuturesMarketStateUpdate( TMarketState * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID )
	{
		WriteLog("stdout",LTALL,"not implemented onRecvRtnFuturesMarketStateUpdate!!!");
	}

	void CGTATradeApiHandler::onRecvRtnForwardInstStateUpdate( TInstState * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID )
	{
		WriteLog("stdout",LTALL,"not implemented onRecvRtnForwardInstStateUpdate!!!");
	}

	void CGTATradeApiHandler::onRecvMinsDeferQuotation( TMinsDeferQuotation * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID )
	{
		WriteLog("stdout",LTALL,"not implemented onRecvMinsDeferQuotation!!!");
	}

	void CGTATradeApiHandler::onRecvRspMiddleAppOrderCancel( TMiddleAppOrderCancel * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID )
	{
		WriteLog("stdout",LTALL,"not implemented onRecvRspMiddleAppOrderCancel!!!");
	}

	void CGTATradeApiHandler::onRecvRspQryDeferLocalOrder( TDeferOrder * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID )
	{
		WriteLog("stdout",LTALL,"not implemented onRecvRspQryDeferLocalOrder!!!");
	}

	void CGTATradeApiHandler::onRecvRspQryForwardLocalOrder( TForwardOrder * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID )
	{
		WriteLog("stdout",LTALL,"not implemented onRecvRspQryForwardLocalOrder!!!");
	}

	void CGTATradeApiHandler::onRecvRspForwardOrder( TForwardOrder * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID )
	{
		WriteLog("stdout",LTALL,"not implemented onRecvRspForwardOrder!!!");
	}

	void CGTATradeApiHandler::onRecvRspForwardOrderCancel( TForwardOrderCancel* in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID )
	{
		WriteLog("stdout",LTALL,"not implemented onRecvRspForwardOrderCancel!!!");
	}

	void CGTATradeApiHandler::onRecvRspForwardEndorseOrder( TSpotOrder * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID )
	{
		WriteLog("stdout",LTALL,"not implemented onRecvRspForwardEndorseOrder!!!");
	}

	void CGTATradeApiHandler::onRecvRspForwardEndorseOrderCancel( TForwardEndorseOrderCancel * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID )
	{
		WriteLog("stdout",LTALL,"not implemented onRecvRspForwardEndorseOrderCancel!!!");
	}

	void CGTATradeApiHandler::onRecvRspTraderPasswordUpdate( TReqTraderPasswordUpdate * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID )
	{
		WriteLog("stdout",LTALL,"not implemented onRecvRspTraderPasswordUpdate!!!");
	}

	void CGTATradeApiHandler::onRecvRspMemberUrgentPasswordUpdate( TReqMemberUrgentPasswordUpdate * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID )
	{
		WriteLog("stdout",LTALL,"not implemented onRecvRspMemberUrgentPasswordUpdate!!!");
	}

	void CGTATradeApiHandler::onRecvRspQryMemberCapital( TMemberCapital * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID )
	{
		WriteLog("stdout",LTALL,"not implemented onRecvRspQryMemberCapital!!!");
	}

	void CGTATradeApiHandler::onRecvRspQryClientStorage( TClientStorage * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID )
	{
		WriteLog("stdout",LTALL,"not implemented onRecvRspQryClientStorage!!!");
	}

	void CGTATradeApiHandler::onRecvRspQryDeferOrder( TDeferOrder * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID )
	{
		WriteLog("stdout",LTALL,"not implemented onRecvRspQryDeferOrder!!!");
	}

	void CGTATradeApiHandler::onRecvRspQryDeferDeliveryAppOrder( TDeferDeliveryAppOrder * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID )
	{
		WriteLog("stdout",LTALL,"not implemented onRecvRspQryDeferDeliveryAppOrder!!!");
	}

	void CGTATradeApiHandler::onRecvRspQryMiddleAppOrder( TMiddleAppOrder * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID )
	{
		WriteLog("stdout",LTALL,"not implemented onRecvRspQryMiddleAppOrder!!!");
	}

	void CGTATradeApiHandler::onRecvRspQryFuturesOrder( TFuturesOrder * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID )
	{
		WriteLog("stdout",LTALL,"not implemented onRecvRspQryFuturesOrder!!!");
	}

	void CGTATradeApiHandler::onRecvRspQryForwardOrder( TForwardOrder * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID )
	{
		WriteLog("stdout",LTALL,"not implemented onRecvRspQryForwardOrder!!!");
	}

	void CGTATradeApiHandler::onRecvRspQryForwardEndorseOrder( TSpotOrder * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID )
	{
		WriteLog("stdout",LTALL,"not implemented onRecvRspQryForwardEndorseOrder!!!");
	}

	void CGTATradeApiHandler::onRecvRspQryDeferMatch( TDeferMatch * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID )
	{
		WriteLog("stdout",LTALL,"not implemented onRecvRspQryDeferMatch!!!");
	}

	void CGTATradeApiHandler::onRecvRtnMiddleAppOrder( TMiddleAppOrder * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID )
	{
		WriteLog("stdout",LTALL,"not implemented onRecvRtnMiddleAppOrder!!!");
	}

	void CGTATradeApiHandler::onRecvRtnDeferDeliveryAppOrderCancel( TDeferDeliveryAppOrder * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID )
	{
		WriteLog("stdout",LTALL,"not implemented onRecvRtnDeferDeliveryAppOrderCancel!!!");
	}

	void CGTATradeApiHandler::onRecvRtnMiddleAppOrderCancel( TMiddleAppOrder * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID )
	{
		WriteLog("stdout",LTALL,"not implemented onRecvRtnMiddleAppOrderCancel!!!");
	}


}
