/***********************************************************************
* Module:  CTrMsgConvert.cpp
* Author:  Administrator
* Modified: 2007年8月31日 9:33:34
* Purpose: Implementation of the class CTrMsgConvert
* Comment: 交行黄金交易系统日间交易有序报文和API使用的结构类实现互转功能。转换分为调用API输入转化和API回调输出转化，转化调用时需要传入转化函数名，转化输入报文，转化输出报文及其他标志等
*    写日志说明，转化所有域值都写到MY_D2中，错误都写到MY_ERR中
***********************************************************************/


#pragma warning(disable:4786 4996 4018)
#include <process.h>
#include <windows.h>
#include <stdio.h>
#include "s_defRetCode.h"
//#include "mybase.h"
#include "GTAMsgConvert.h"
#include "CCrypticDES.h"
#include "util.h"
#include "LogHelper.h"
#include "GTATradeCodeMap.h"
#include <simsingleton.h>
using namespace simpattern;
using namespace utils;

namespace gta
{


bool CGTAMsgConvert::onRecvRspSpotOrder(TSpotOrder * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize)
{
	/*	std::string tradecode = CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvRspSpotOrder");*/
	CGTABufferBuilder cb(out_pszOutMsg,0);
	cb.AppendBlock("\0\0\0\0",strlen("\0\0\0\0"),4).
	//	AppendBlock(tradecode.c_str(),tradecode.size(),10).
	AppendBlock(pstTRspMsg->RspCode,strlen(pstTRspMsg->RspCode),20).
	AppendBlock(pstTRspMsg->RspMsg,strlen(pstTRspMsg->RspMsg),40).
	AppendBlock(pstUsrData->orderNo,strlen(pstUsrData->orderNo),16).
	AppendBlock(pstUsrData->instID,strlen(pstUsrData->instID),8).
	AppendBlock(pstUsrData->clientID,strlen(pstUsrData->clientID),12).
	AppendBlock(pstUsrData->memberID,strlen(pstUsrData->memberID),6).
	AppendChar(pstUsrData->buyOrSell).
	AppendBlock(pstUsrData->applyDate,strlen(pstUsrData->applyDate),8).
	AppendBlock(pstUsrData->applyTime,strlen(pstUsrData->applyTime),8).
	AppendBlock(pstUsrData->traderID,strlen(pstUsrData->traderID),10).
	AppendDouble(pstUsrData->price).
	AppendInt(pstUsrData->amount).
	AppendInt(pstUsrData->remainAmount).
	AppendBlock(pstUsrData->cancelTime,strlen(pstUsrData->cancelTime),8).
	AppendBlock(pstUsrData->cancelID,strlen(pstUsrData->cancelID),10).
	AppendChar(pstUsrData->status).
	AppendChar(pstUsrData->matchType).
	AppendBlock(pstUsrData->endorseInstID,strlen(pstUsrData->endorseInstID),8).
	AppendBlock(pstUsrData->localOrderNo,strlen(pstUsrData->localOrderNo),14);

	*in_lOutMsgSize = cb.GetIndex();
	int datalen = *in_lOutMsgSize-4;
	char buff[5]={0};
	sprintf(buff,"%04d",datalen);
	cb.FillBlock(0,buff,4,4);



	return true;
}



bool CGTAMsgConvert::onRecvRtnSpotOrder(TSpotOrder * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize)
{
	LogCallStack(CGTAMsgConvert_onRecvRtnSpotOrder);
	WriteLog("msgconvt",ILog::TALL,"pstTRspMsg->RspMsg = %s",(char*)pstTRspMsg->RspMsg);

	std::string tradecode = CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvRtnSpotOrder");
	CGTABufferBuilder cb(out_pszOutMsg,0);
	cb.AppendBlock("\0\0\0\0",strlen("\0\0\0\0"),4).
	AppendBlock(tradecode.c_str(),tradecode.size(),10).
	AppendBlock(pstTRspMsg->RspCode,strlen(pstTRspMsg->RspCode),20).
	AppendBlock(pstTRspMsg->RspMsg,strlen(pstTRspMsg->RspMsg),40).
	AppendBlock(pstUsrData->orderNo,strlen(pstUsrData->orderNo),16).
	AppendBlock(pstUsrData->instID,strlen(pstUsrData->instID),8).
	AppendBlock(pstUsrData->clientID,strlen(pstUsrData->clientID),12).
	AppendBlock(pstUsrData->memberID,strlen(pstUsrData->memberID),6).
	AppendChar(pstUsrData->buyOrSell).
	AppendBlock(pstUsrData->applyDate,strlen(pstUsrData->applyDate),8).
	AppendBlock(pstUsrData->applyTime,strlen(pstUsrData->applyTime),8).
	AppendBlock(pstUsrData->traderID,strlen(pstUsrData->traderID),10).
	AppendDouble(pstUsrData->price).
	AppendInt(pstUsrData->amount).
	AppendInt(pstUsrData->remainAmount).
	AppendBlock(pstUsrData->cancelTime,strlen(pstUsrData->cancelTime),8).
	AppendBlock(pstUsrData->cancelID,strlen(pstUsrData->cancelID),10).
	AppendChar(pstUsrData->status).
	AppendChar(pstUsrData->matchType).
	AppendBlock(pstUsrData->endorseInstID,strlen(pstUsrData->endorseInstID),8).
	AppendBlock(pstUsrData->localOrderNo,strlen(pstUsrData->localOrderNo),14);

	*in_lOutMsgSize = cb.GetIndex();
	int datalen = *in_lOutMsgSize-4;
	char buff[5]={0};
	sprintf(buff,"%04d",datalen);
	cb.FillBlock(0,buff,4,4);

	return true;
}

bool CGTAMsgConvert::onRecvRtnSpotMatch(TSpotMatch * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize)
{
	std::string tradecode = CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvRtnSpotMatch");
	CGTABufferBuilder cb(out_pszOutMsg,0);

	cb.AppendBlock("\0\0\0\0",strlen("\0\0\0\0"),4).
	AppendBlock(tradecode.c_str(),tradecode.size(),10).
	AppendBlock(pstTRspMsg->RspCode,strlen(pstTRspMsg->RspCode),20).
	AppendBlock(pstTRspMsg->RspMsg,strlen(pstTRspMsg->RspMsg),40).

	AppendBlock(pstUsrData->matchNo,strlen(pstUsrData->matchNo),16).// TODO CHH
	//	Append(strmatchNo,16).

	AppendChar(pstUsrData->buyOrSell).
	AppendBlock(pstUsrData->clientID,strlen(pstUsrData->clientID),12).
	AppendChar(pstUsrData->clientType).
	AppendBlock(pstUsrData->memberID,strlen(pstUsrData->memberID),6).
	AppendBlock(pstUsrData->instID,strlen(pstUsrData->instID),8).
	AppendBlock(pstUsrData->matchDate,strlen(pstUsrData->matchDate),8).
	AppendBlock(pstUsrData->matchTime,strlen(pstUsrData->matchTime),8).
	AppendDouble(pstUsrData->price).
	AppendInt(pstUsrData->volume).
	AppendBlock(pstUsrData->orderNo,strlen(pstUsrData->orderNo),14).
	AppendBlock(pstUsrData->localOrderNo,strlen(pstUsrData->localOrderNo),14).
	AppendChar(pstUsrData->matchType);



	*in_lOutMsgSize = cb.GetIndex();
	int datalen = *in_lOutMsgSize-4;
	char buff[5]={0};
	sprintf(buff,"%04d",datalen);
	cb.FillBlock(0,buff,4,4);


	return true;
}

//现货撤单
bool CGTAMsgConvert::onRecvRspSpotOrderCancel(TSpotOrderCancel * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize)
{
	/*std::string tradecode = CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvRspSpotOrderCancel");*/
	CGTABufferBuilder cb(out_pszOutMsg,0);

	cb.AppendBlock("\0\0\0\0",strlen("\0\0\0\0"),4).
	//	AppendBlock(tradecode.c_str(),tradecode.size(),10).
	AppendBlock(pstTRspMsg->RspCode,strlen(pstTRspMsg->RspCode),20).
	AppendBlock(pstTRspMsg->RspMsg,strlen(pstTRspMsg->RspMsg),40).
	AppendBlock(pstUsrData->orderNo,strlen(pstUsrData->orderNo),16).
	AppendBlock(pstUsrData->instID,strlen(pstUsrData->instID),8).
	AppendBlock(pstUsrData->clientID,strlen(pstUsrData->clientID),12).
	AppendBlock(pstUsrData->memberID,strlen(pstUsrData->memberID),6).
	AppendBlock(pstUsrData->traderID,strlen(pstUsrData->traderID),10);
	*in_lOutMsgSize = cb.GetIndex();
	int datalen = *in_lOutMsgSize-4;
	char buff[5]={0};
	sprintf(buff,"%04d",datalen);
	cb.FillBlock(0,buff,4,4);
	return true;
}
//现货报单查询
bool CGTAMsgConvert::onRecvRspQrySpotOrder(TSpotOrder * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize)
{
	std::string tradecode = CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvRspQrySpotOrder");
	CGTABufferBuilder cb(out_pszOutMsg,0);
	cb.AppendBlock("\0\0\0\0",strlen("\0\0\0\0"),4).
	AppendBlock(tradecode.c_str(),tradecode.size(),10).
	AppendBlock(pstTRspMsg->RspCode,strlen(pstTRspMsg->RspCode),20).
	AppendBlock(pstTRspMsg->RspMsg,strlen(pstTRspMsg->RspMsg),40).
	AppendBlock(pstUsrData->orderNo,strlen(pstUsrData->orderNo),16).
	AppendBlock(pstUsrData->instID,strlen(pstUsrData->instID),8).
	AppendBlock(pstUsrData->clientID,strlen(pstUsrData->clientID),12).
	AppendBlock(pstUsrData->memberID,strlen(pstUsrData->memberID),6).
	AppendChar(pstUsrData->buyOrSell).
	AppendBlock(pstUsrData->applyDate,strlen(pstUsrData->applyDate),8).
	AppendBlock(pstUsrData->applyTime,strlen(pstUsrData->applyTime),8).
	AppendBlock(pstUsrData->traderID,strlen(pstUsrData->traderID),10).
	AppendDouble(pstUsrData->price).
	AppendInt(pstUsrData->amount).
	AppendInt(pstUsrData->remainAmount).
	AppendBlock(pstUsrData->cancelTime,strlen(pstUsrData->cancelTime),8).
	AppendBlock(pstUsrData->cancelID,strlen(pstUsrData->cancelID),10).
	AppendChar(pstUsrData->status).
	AppendChar(pstUsrData->matchType).
	AppendBlock(pstUsrData->endorseInstID,strlen(pstUsrData->endorseInstID),8).
	AppendBlock(pstUsrData->localOrderNo,strlen(pstUsrData->localOrderNo),14);

	*in_lOutMsgSize = cb.GetIndex();
	int datalen = *in_lOutMsgSize-4;
	char buff[5]={0};
	sprintf(buff,"%04d",datalen);
	cb.FillBlock(0,buff,4,4);
	return true;
}

//现货成交查询
bool CGTAMsgConvert::onRecvRspQrySpotMatch(TSpotMatch * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize)
{
	std::string tradecode = CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvRspQrySpotMatch");
	CGTABufferBuilder cb(out_pszOutMsg,0);

	cb.AppendBlock("\0\0\0\0",strlen("\0\0\0\0"),4).
	AppendBlock(tradecode.c_str(),tradecode.size(),10).
	AppendBlock(pstTRspMsg->RspCode,strlen(pstTRspMsg->RspCode),20).
	AppendBlock(pstTRspMsg->RspMsg,strlen(pstTRspMsg->RspMsg),40).

	AppendBlock(pstUsrData->matchNo,strlen(pstUsrData->matchNo),16).// TODO CHH
	//	Append(strmatchNo,16).

	AppendChar(pstUsrData->buyOrSell).
	AppendBlock(pstUsrData->clientID,strlen(pstUsrData->clientID),12).
	AppendChar(pstUsrData->clientType).
	AppendBlock(pstUsrData->memberID,strlen(pstUsrData->memberID),6).
	AppendBlock(pstUsrData->instID,strlen(pstUsrData->instID),8).
	AppendBlock(pstUsrData->matchDate,strlen(pstUsrData->matchDate),8).
	AppendBlock(pstUsrData->matchTime,strlen(pstUsrData->matchTime),8).
	AppendDouble(pstUsrData->price).
	AppendInt(pstUsrData->volume).
	AppendBlock(pstUsrData->orderNo,strlen(pstUsrData->orderNo),14).
	AppendBlock(pstUsrData->localOrderNo,strlen(pstUsrData->localOrderNo),14).
	AppendChar(pstUsrData->matchType);



	*in_lOutMsgSize = cb.GetIndex();
	int datalen = *in_lOutMsgSize-4;
	char buff[5]={0};
	sprintf(buff,"%04d",datalen);
	cb.FillBlock(0,buff,4,4);


	return true;
}

//现货撤单反推
bool CGTAMsgConvert::onRecvRtnSpotOrderCancel(TSpotOrder * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize)
{
	std::string tradecode = CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvRtnSpotOrderCancel");
	CGTABufferBuilder cb(out_pszOutMsg,0);
	cb.AppendBlock("\0\0\0\0",strlen("\0\0\0\0"),4).
	AppendBlock(tradecode.c_str(),tradecode.size(),10).
	AppendBlock(pstTRspMsg->RspCode,strlen(pstTRspMsg->RspCode),20).
	AppendBlock(pstTRspMsg->RspMsg,strlen(pstTRspMsg->RspMsg),40).
	AppendBlock(pstUsrData->orderNo,strlen(pstUsrData->orderNo),16).
	AppendBlock(pstUsrData->instID,strlen(pstUsrData->instID),8).
	AppendBlock(pstUsrData->clientID,strlen(pstUsrData->clientID),12).
	AppendBlock(pstUsrData->memberID,strlen(pstUsrData->memberID),6).
	AppendChar(pstUsrData->buyOrSell).
	AppendBlock(pstUsrData->applyDate,strlen(pstUsrData->applyDate),8).
	AppendBlock(pstUsrData->applyTime,strlen(pstUsrData->applyTime),8).
	AppendBlock(pstUsrData->traderID,strlen(pstUsrData->traderID),10).
	AppendDouble(pstUsrData->price).
	AppendInt(pstUsrData->amount).
	AppendInt(pstUsrData->remainAmount).
	AppendBlock(pstUsrData->cancelTime,strlen(pstUsrData->cancelTime),8).
	AppendBlock(pstUsrData->cancelID,strlen(pstUsrData->cancelID),10).
	AppendChar(pstUsrData->status).
	AppendChar(pstUsrData->matchType).
	AppendBlock(pstUsrData->endorseInstID,strlen(pstUsrData->endorseInstID),8).
	AppendBlock(pstUsrData->localOrderNo,strlen(pstUsrData->localOrderNo),14);

	*in_lOutMsgSize = cb.GetIndex();
	int datalen = *in_lOutMsgSize-4;
	char buff[5]={0};
	sprintf(buff,"%04d",datalen);
	cb.FillBlock(0,buff,4,4);
	return true;
}

//交易员登陆响应
bool CGTAMsgConvert::onRecvRspTraderLogin(TReqTraderLogin * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize)
{
	/*	std::string tradecode = CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvRspTraderLogin");*/
	CGTABufferBuilder cb(out_pszOutMsg,0);
	char szTmp[512];
	char szPwd[512];
	char m_sErrMsg[512];

	//转换密码
	memset(szTmp, 0, sizeof(szTmp));
	memset(szPwd,32,sizeof(szPwd));
	memcpy(szTmp, pstUsrData->password, strlen(pstUsrData->password));
	//密码需要加密
	if (0 > Singleton_InFunc<CCrypticDES>::Instance().rl_EncryptPwd(szTmp, szPwd, sizeof(szPwd)))
	{
		strcpy(m_sErrMsg, "加密失败");
		//rv_TraceLog(MY_ERR, m_szErrMsg);
		WriteLog("Error.log",utils::ILog::TALL,m_sErrMsg);
	}
	cb.AppendBlock("\0\0\0\0",strlen("\0\0\0\0"),4).
	//AppendBlock(tradecode.c_str(),tradecode.size(),10).
	AppendBlock(pstTRspMsg->RspCode,strlen(pstTRspMsg->RspCode),20).
	AppendBlock(pstTRspMsg->RspMsg,strlen(pstTRspMsg->RspMsg),40).
	AppendBlock(pstUsrData->traderID,strlen(pstUsrData->traderID),10).
	AppendBlock(pstUsrData->memberID,strlen(pstUsrData->memberID),6).
	AppendBlock(szPwd,strlen(szPwd),32).
	AppendBlock(pstUsrData->tradeDate,strlen(pstUsrData->tradeDate),8).
	AppendBlock(pstUsrData->ipAddress,strlen(pstUsrData->ipAddress),30).
	AppendBlock(pstUsrData->localOrderNo,strlen(pstUsrData->localOrderNo),14).
	AppendInt(pstUsrData->machineID);

	*in_lOutMsgSize = cb.GetIndex();
	int datalen = *in_lOutMsgSize-4;
	char buff[5]={0};
	sprintf(buff,"%04d",datalen);
	cb.FillBlock(0,buff,4,4);
	return true;
}

//交易员登出响应
bool CGTAMsgConvert::onRecvRspTraderLogout(TReqTraderLogout * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize)
{
	/*	std::string tradecode = CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvRspTraderLogout");*/
	CGTABufferBuilder cb(out_pszOutMsg,0);
	cb.AppendBlock("\0\0\0\0",strlen("\0\0\0\0"),4).
	//AppendBlock(tradecode.c_str(),tradecode.size(),10).
	AppendBlock(pstTRspMsg->RspCode,strlen(pstTRspMsg->RspCode),20).
	AppendBlock(pstTRspMsg->RspMsg,strlen(pstTRspMsg->RspMsg),40).
	AppendBlock(pstUsrData->traderID,strlen(pstUsrData->traderID),10).
	AppendBlock(pstUsrData->memberID,strlen(pstUsrData->memberID),6);

	*in_lOutMsgSize = cb.GetIndex();
	int datalen = *in_lOutMsgSize-4;
	char buff[5]={0};
	sprintf(buff,"%04d",datalen);
	cb.FillBlock(0,buff,4,4);
	return true;
}





//市场信息回报
bool CGTAMsgConvert::onRecvMarketInfo(TMarket * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize)
{
	std::string tradecode = CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvMarketInfo");
	CGTABufferBuilder cb(out_pszOutMsg,0);
	cb.AppendBlock("\0\0\0\0",strlen("\0\0\0\0"),4).
	AppendBlock(tradecode.c_str(),tradecode.size(),10).
	AppendBlock(pstTRspMsg->RspCode,strlen(pstTRspMsg->RspCode),20).
	AppendBlock(pstTRspMsg->RspMsg,strlen(pstTRspMsg->RspMsg),40).
	AppendBlock(pstUsrData->marketID,strlen(pstUsrData->marketID),2).
	AppendBlock(pstUsrData->name,strlen(pstUsrData->name),20).
	AppendChar(pstUsrData->type).
	AppendChar(pstUsrData->openFlag).
	AppendChar(pstUsrData->marketState);

	*in_lOutMsgSize = cb.GetIndex();
	int datalen = *in_lOutMsgSize-4;
	char buff[5]={0};
	sprintf(buff,"%04d",datalen);
	cb.FillBlock(0,buff,4,4);
	return true;
}

//现货合约信息回报
bool CGTAMsgConvert::onRecvSpotInstInfo(TSpotInst * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize)
{
	std::string tradecode = CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvSpotInstInfo");
	CGTABufferBuilder cb(out_pszOutMsg,0);
	cb.AppendBlock("\0\0\0\0",strlen("\0\0\0\0"),4).
	AppendBlock(tradecode.c_str(),tradecode.size(),10).
	AppendBlock(pstTRspMsg->RspCode,strlen(pstTRspMsg->RspCode),20).
	AppendBlock(pstTRspMsg->RspMsg,strlen(pstTRspMsg->RspMsg),40).
	AppendBlock(pstUsrData->instID,strlen(pstUsrData->instID),8).
	AppendBlock(pstUsrData->name,strlen(pstUsrData->name),12).
	AppendBlock(pstUsrData->marketID,strlen(pstUsrData->marketID),2).
	AppendChar(pstUsrData->varietyType).
	AppendBlock(pstUsrData->varietyID,strlen(pstUsrData->varietyID),3).
	AppendInt(pstUsrData->unit).
	AppendDouble(pstUsrData->tick).
	AppendInt(pstUsrData->maxHand).
	AppendInt(pstUsrData->minHand).
	AppendDouble(pstUsrData->upperLimit).
	AppendDouble(pstUsrData->lowerLimit).
	AppendChar(pstUsrData->openFlag).
	AppendChar(pstUsrData->tradeState).
	AppendDouble(pstUsrData->refPrice).
	AppendDouble(pstUsrData->recvRate);

	*in_lOutMsgSize = cb.GetIndex();
	int datalen = *in_lOutMsgSize-4;
	char buff[5]={0};
	sprintf(buff,"%04d",datalen);
	cb.FillBlock(0,buff,4,4);
	return true;
}

//现货合约状态改变
bool CGTAMsgConvert::onRecvRtnSpotInstStateUpdate(TInstState * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize)
{
	std::string tradecode = CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvRtnSpotInstStateUpdate");
	CGTABufferBuilder cb(out_pszOutMsg,0);
	cb.AppendBlock("\0\0\0\0",strlen("\0\0\0\0"),4).
	AppendBlock(tradecode.c_str(),tradecode.size(),10).
	AppendBlock(pstTRspMsg->RspCode,strlen(pstTRspMsg->RspCode),20).
	AppendBlock(pstTRspMsg->RspMsg,strlen(pstTRspMsg->RspMsg),40).
	AppendBlock(pstUsrData->instID,strlen(pstUsrData->instID),8).
	AppendChar(pstUsrData->tradeState);

	*in_lOutMsgSize = cb.GetIndex();
	int datalen = *in_lOutMsgSize-4;
	char buff[5]={0};
	sprintf(buff,"%04d",datalen);
	cb.FillBlock(0,buff,4,4);
	return true;
}

//现货市场状态改变

bool CGTAMsgConvert::onRecvRtnSpotMarketStateUpdate(TMarketState * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize)
{
	std::string tradecode = CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvRtnSpotMarketStateUpdate");
	CGTABufferBuilder cb(out_pszOutMsg,0);
	cb.AppendBlock("\0\0\0\0",strlen("\0\0\0\0"),4).
	AppendBlock(tradecode.c_str(),tradecode.size(),10).
	AppendBlock(pstTRspMsg->RspCode,strlen(pstTRspMsg->RspCode),20).
	AppendBlock(pstTRspMsg->RspMsg,strlen(pstTRspMsg->RspMsg),40).
	AppendBlock(pstUsrData->marketID,strlen(pstUsrData->marketID),2).
	AppendChar(pstUsrData->marketState);

	*in_lOutMsgSize = cb.GetIndex();
	out_pszOutMsg[cb.GetIndex()] = '\0';
	int datalen = *in_lOutMsgSize-4;
	char buff[5]={32};
	sprintf(buff,"%04d",datalen);
	cb.FillBlock(0,buff,4,4);
	return true;
}


//现货行情

bool CGTAMsgConvert::onRecvSpotQuotation(TSpotQuotation * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize)
{
	std::string tradecode = CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvSpotQuotation");
	CGTABufferBuilder cb(out_pszOutMsg,0);
	cb.AppendBlock("\0\0\0\0",strlen("\0\0\0\0"),4).
	AppendBlock(tradecode.c_str(),tradecode.size(),10).
	AppendBlock(pstTRspMsg->RspCode,strlen(pstTRspMsg->RspCode),20).
	AppendBlock(pstTRspMsg->RspMsg,strlen(pstTRspMsg->RspMsg),40).
	AppendBlock(pstUsrData->instID,strlen(pstUsrData->instID),8).
	AppendBlock(pstUsrData->name,strlen(pstUsrData->name),12).
	AppendDouble(pstUsrData->lastClose).
	AppendDouble(pstUsrData->open).
	AppendDouble(pstUsrData->high).
	AppendDouble(pstUsrData->low).
	AppendDouble(pstUsrData->last).
	AppendDouble(pstUsrData->close).
	AppendDouble(pstUsrData->bid1).
	AppendInt(pstUsrData->bidLot1).
	AppendDouble(pstUsrData->bid2).
	AppendInt(pstUsrData->bidLot2).
	AppendDouble(pstUsrData->bid3).
	AppendInt(pstUsrData->bidLot3).
	AppendDouble(pstUsrData->bid4).
	AppendInt(pstUsrData->bidLot4).
	AppendDouble(pstUsrData->bid5).
	AppendInt(pstUsrData->bidLot5).
	AppendDouble(pstUsrData->ask1).
	AppendInt(pstUsrData->askLot1).
	AppendDouble(pstUsrData->ask2).
	AppendInt(pstUsrData->askLot2).
	AppendDouble(pstUsrData->ask3).
	AppendInt(pstUsrData->askLot3).
	AppendDouble(pstUsrData->ask5).
	AppendInt(pstUsrData->askLot4).
	AppendDouble(pstUsrData->ask5).
	AppendInt(pstUsrData->askLot5).
	AppendInt(pstUsrData->volume).
	AppendDouble(pstUsrData->weight).
	AppendDouble(pstUsrData->highLimit).
	AppendDouble(pstUsrData->lowLimit).
	AppendDouble(pstUsrData->upDown).
	AppendDouble(pstUsrData->upDownRate).
	AppendDouble(pstUsrData->turnOver).
	AppendDouble(pstUsrData->average).
	AppendBlock(pstUsrData->quoteDate,strlen(pstUsrData->quoteDate),8).
	AppendBlock(pstUsrData->quoteDate,strlen(pstUsrData->quoteTime),8).
	AppendInt(pstUsrData->sequenceNo);
	*in_lOutMsgSize = cb.GetIndex();
	out_pszOutMsg[cb.GetIndex()] = '\0';
	int datalen = *in_lOutMsgSize-4;
	char buff[5]={0};
	sprintf(buff,"%04d",datalen);
	cb.FillBlock(0,buff,4,4);
	return true;
}

//交割品种代码信息

bool CGTAMsgConvert::onRecvVarietyInfo(TVariety * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize)
{
	std::string tradecode = CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvVarietyInfo");
	CGTABufferBuilder cb(out_pszOutMsg,0);
	cb.AppendBlock("\0\0\0\0",strlen("\0\0\0\0"),4).
	AppendBlock(tradecode.c_str(),tradecode.size(),10).
	AppendBlock(pstTRspMsg->RspCode,strlen(pstTRspMsg->RspCode),20).
	AppendBlock(pstTRspMsg->RspMsg,strlen(pstTRspMsg->RspMsg),40).
	AppendBlock(pstUsrData->varietyID,strlen(pstUsrData->varietyID),3).
	AppendBlock(pstUsrData->name,strlen(pstUsrData->name),40).
	AppendBlock(pstUsrData->abbr,strlen(pstUsrData->abbr),20).
	AppendChar(pstUsrData->varietyType).
	AppendDouble(pstUsrData->minPickup).
	AppendDouble(pstUsrData->defaultStdWeight).
	AppendDouble(pstUsrData->pickupBase).
	AppendBlock(pstUsrData->weightUnit,strlen(pstUsrData->weightUnit),2).
	AppendChar(pstUsrData->destroyFlag);
	*in_lOutMsgSize = cb.GetIndex();
	int datalen = *in_lOutMsgSize-4;
	char buff[5]={0};
	sprintf(buff,"%04d",datalen);
	cb.FillBlock(0,buff,4,4);
	return true;
}

//延期委托报单
bool CGTAMsgConvert::onRecvRspDeferOrder(TDeferOrder * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize)
{
	/*	std::string tradecode = CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvRspDeferOrder");*/
	CGTABufferBuilder cb(out_pszOutMsg,0);
	cb.AppendBlock("\0\0\0\0",strlen("\0\0\0\0"),4).
	//AppendBlock(tradecode.c_str(),tradecode.size(),10).
	AppendBlock(pstTRspMsg->RspCode,strlen(pstTRspMsg->RspCode),20).
	AppendBlock(pstTRspMsg->RspMsg,strlen(pstTRspMsg->RspMsg),40).
	AppendBlock(pstUsrData->orderNo,strlen(pstUsrData->orderNo),16).
	AppendBlock(pstUsrData->clientID,strlen(pstUsrData->clientID),12).
	AppendBlock(pstUsrData->instID,strlen(pstUsrData->instID),8).
	AppendBlock(pstUsrData->memberID,strlen(pstUsrData->memberID),6).
	AppendChar(pstUsrData->buyOrSell).
	AppendBlock(pstUsrData->applyDate,strlen(pstUsrData->applyDate),8).
	AppendBlock(pstUsrData->applyTime,strlen(pstUsrData->applyTime),8).
	AppendChar(pstUsrData->offSetFlag).
	AppendDouble(pstUsrData->price).AppendInt(pstUsrData->amount).AppendInt(pstUsrData->remainAmount).AppendBlock(pstUsrData->cancelTime,strlen(pstUsrData->cancelTime),8).AppendChar(pstUsrData->status).AppendBlock(pstUsrData->localOrderNo,strlen(pstUsrData->localOrderNo),14).AppendDouble(pstUsrData->margin).AppendChar(pstUsrData->marginType).AppendBlock(pstUsrData->traderID,strlen(pstUsrData->traderID),10).AppendBlock(pstUsrData->cancelID,strlen(pstUsrData->cancelID),10).AppendChar(pstUsrData->matchType);

	*in_lOutMsgSize = cb.GetIndex();
	int datalen = *in_lOutMsgSize-4;
	char buff[5]={0};
	sprintf(buff,"%04d",datalen);
	cb.FillBlock(0,buff,4,4);
	return true;
}

//延期委托撤单
bool CGTAMsgConvert::onRecvRspDeferOrderCancel(TDeferOrderCancel * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize)
{
	/*	std::string tradecode = CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvRspDeferOrderCancel");*/
	CGTABufferBuilder cb(out_pszOutMsg,0);
	cb.AppendBlock("\0\0\0\0",strlen("\0\0\0\0"),4).
	/*AppendBlock(tradecode.c_str(),tradecode.size(),10).*/
	AppendBlock(pstTRspMsg->RspCode,strlen(pstTRspMsg->RspCode),20).
	AppendBlock(pstTRspMsg->RspMsg,strlen(pstTRspMsg->RspMsg),40).
	AppendBlock(pstUsrData->orderNo,strlen(pstUsrData->orderNo),16).
	AppendBlock(pstUsrData->instID,strlen(pstUsrData->instID),8).
	AppendBlock(pstUsrData->clientID,strlen(pstUsrData->clientID),12).
	AppendBlock(pstUsrData->memberID,strlen(pstUsrData->memberID),6).
	AppendBlock(pstUsrData->traderID,strlen(pstUsrData->traderID),10);

	*in_lOutMsgSize = cb.GetIndex();
	int datalen = *in_lOutMsgSize-4;
	char buff[5]={0};
	sprintf(buff,"%04d",datalen);
	cb.FillBlock(0,buff,4,4);
	return true;
}

//延期行情
bool CGTAMsgConvert::onRecvDeferQuotation(TDeferQuotation * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize)
{
	std::string tradecode = CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvDeferQuotation");
	CGTABufferBuilder cb(out_pszOutMsg,0);
	cb.AppendBlock("\0\0\0\0",strlen("\0\0\0\0"),4).
	AppendBlock(tradecode.c_str(),tradecode.size(),10).
	AppendBlock(pstTRspMsg->RspCode,strlen(pstTRspMsg->RspCode),20).
	AppendBlock(pstTRspMsg->RspMsg,strlen(pstTRspMsg->RspMsg),40).
	AppendBlock(pstUsrData->instID,strlen(pstUsrData->instID),8).
	AppendBlock(pstUsrData->name,strlen(pstUsrData->name),12).
	AppendDouble(pstUsrData->lastSettle).
	AppendDouble(pstUsrData->lastClose).
	AppendDouble(pstUsrData->open).
	AppendDouble(pstUsrData->high).
	AppendDouble(pstUsrData->low).
	AppendDouble(pstUsrData->last).
	AppendDouble(pstUsrData->close).
	AppendDouble(pstUsrData->settle).
	AppendDouble(pstUsrData->bid1).
	AppendInt(pstUsrData->bidLot1).
	AppendDouble(pstUsrData->bid2).
	AppendInt(pstUsrData->bidLot2).
	AppendDouble(pstUsrData->bid3).
	AppendInt(pstUsrData->bidLot3).
	AppendDouble(pstUsrData->bid4).
	AppendInt(pstUsrData->bidLot4).
	AppendDouble(pstUsrData->bid5).
	AppendInt(pstUsrData->bidLot5).
	AppendDouble(pstUsrData->ask1).
	AppendInt(pstUsrData->askLot1).
	AppendDouble(pstUsrData->ask2).
	AppendInt(pstUsrData->askLot2).
	AppendDouble(pstUsrData->ask3).
	AppendInt(pstUsrData->askLot3).
	AppendDouble(pstUsrData->ask4).
	AppendInt(pstUsrData->askLot4).
	AppendDouble(pstUsrData->ask5).
	AppendInt(pstUsrData->askLot5).
	AppendInt(pstUsrData->volume).
	AppendDouble(pstUsrData->weight).
	AppendDouble(pstUsrData->highLimit).
	AppendDouble(pstUsrData->lowLimit).
	AppendDouble(pstUsrData->Posi).
	AppendDouble(pstUsrData->upDown).
	AppendDouble(pstUsrData->upDownRate).
	AppendDouble(pstUsrData->turnOver).
	AppendDouble(pstUsrData->average).
	AppendBlock(pstUsrData->quoteDate,strlen(pstUsrData->quoteDate),8).
	AppendBlock(pstUsrData->quoteDate,strlen(pstUsrData->quoteTime),8).
	AppendInt(pstUsrData->sequenceNo);


	*in_lOutMsgSize = cb.GetIndex();
	int datalen = *in_lOutMsgSize-4;
	char buff[5]={0};
	sprintf(buff,"%04d",datalen);
	cb.FillBlock(0,buff,4,4);
	return true;
}
//延期合约信息
bool CGTAMsgConvert::onRecvDeferInstInfo(TDeferInst * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize)
{
	std::string tradecode = CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvDeferInstInfo");
	CGTABufferBuilder cb(out_pszOutMsg,0);
	cb.AppendBlock("\0\0\0\0",strlen("\0\0\0\0"),4).
	AppendBlock(tradecode.c_str(),tradecode.size(),10).
	AppendBlock(pstTRspMsg->RspCode,strlen(pstTRspMsg->RspCode),20).
	AppendBlock(pstTRspMsg->RspMsg,strlen(pstTRspMsg->RspMsg),40).
	AppendBlock(pstUsrData->instID,strlen(pstUsrData->instID),8).
	AppendBlock(pstUsrData->name,strlen(pstUsrData->name),12).
	AppendBlock(pstUsrData->marketID,strlen(pstUsrData->marketID),2).
	AppendChar(pstUsrData->varietyType).
	AppendBlock(pstUsrData->varietyID,strlen(pstUsrData->varietyID),3).
	AppendInt(pstUsrData->unit).
	AppendDouble(pstUsrData->tick).
	AppendInt(pstUsrData->maxHand).
	AppendInt(pstUsrData->minHand).
	AppendDouble(pstUsrData->upperLimit).
	AppendDouble(pstUsrData->lowerLimit).
	AppendChar(pstUsrData->openFlag).
	AppendChar(pstUsrData->tradeState).
	AppendDouble(pstUsrData->refPrice);


	*in_lOutMsgSize = cb.GetIndex();
	int datalen = *in_lOutMsgSize-4;
	char buff[5]={0};
	sprintf(buff,"%04d",datalen);
	cb.FillBlock(0,buff,4,4);
	return true;
}




//延期客户持仓查询
/*
bool CGTAMsgConvert::onRecvRspQryDeferClientPosi(TDeferClientPosi * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize)
{
	std::string tradecode = CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvRspQryDeferClientPosi");
	CGTABufferBuilder cb(out_pszOutMsg,0);
	cb.AppendBlock("\0\0\0\0",strlen("\0\0\0\0"),4).
		AppendBlock(tradecode.c_str(),tradecode.size(),10).
		AppendBlock(pstTRspMsg->RspCode,strlen(pstTRspMsg->RspCode),20).
		AppendBlock(pstTRspMsg->RspMsg,strlen(pstTRspMsg->RspMsg),40).

		AppendBlock(pstUsrData->orderNo,strlen(pstUsrData->orderNo),16).
		AppendBlock(pstUsrData->instID,strlen(pstUsrData->instID),8).
		AppendBlock(pstUsrData->clientID,strlen(pstUsrData->clientID),12).
		AppendBlock(pstUsrData->memberID,strlen(pstUsrData->memberID),6).
		AppendBlock(pstUsrData->traderID,strlen(pstUsrData->traderID),10);

	*in_lOutMsgSize = cb.GetIndex();
	int datalen = *in_lOutMsgSize-4;
	char buff[5]={0};
	sprintf(buff,"%04d",datalen);
	cb.FillBlock(0,buff,4,4);
	return true;
}
*/

//延期报单反推
bool CGTAMsgConvert::onRecvRtnDeferOrder(TDeferOrder * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize)
{
	std::string tradecode = CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvRtnDeferOrder");
	CGTABufferBuilder cb(out_pszOutMsg,0);
	cb.AppendBlock("\0\0\0\0",strlen("\0\0\0\0"),4).
	AppendBlock(tradecode.c_str(),tradecode.size(),10).
	AppendBlock(pstTRspMsg->RspCode,strlen(pstTRspMsg->RspCode),20).
	AppendBlock(pstTRspMsg->RspMsg,strlen(pstTRspMsg->RspMsg),40).
	AppendBlock(pstUsrData->orderNo,strlen(pstUsrData->orderNo),16).
	AppendBlock(pstUsrData->clientID,strlen(pstUsrData->clientID),12).
	AppendBlock(pstUsrData->instID,strlen(pstUsrData->instID),8).
	AppendBlock(pstUsrData->memberID,strlen(pstUsrData->memberID),6).
	AppendChar(pstUsrData->buyOrSell).
	AppendBlock(pstUsrData->applyDate,strlen(pstUsrData->applyDate),8).
	AppendBlock(pstUsrData->applyTime,strlen(pstUsrData->applyTime),8).
	AppendChar(pstUsrData->offSetFlag).
	AppendDouble(pstUsrData->price).AppendInt(pstUsrData->amount).AppendInt(pstUsrData->remainAmount).AppendBlock(pstUsrData->cancelTime,strlen(pstUsrData->cancelTime),8).AppendChar(pstUsrData->status).AppendBlock(pstUsrData->localOrderNo,strlen(pstUsrData->localOrderNo),14).AppendDouble(pstUsrData->margin).AppendChar(pstUsrData->marginType).AppendBlock(pstUsrData->traderID,strlen(pstUsrData->traderID),10).AppendBlock(pstUsrData->cancelID,strlen(pstUsrData->cancelID),10).AppendChar(pstUsrData->matchType);

	*in_lOutMsgSize = cb.GetIndex();
	int datalen = *in_lOutMsgSize-4;
	char buff[5]={0};
	sprintf(buff,"%04d",datalen);
	cb.FillBlock(0,buff,4,4);
	return true;
}

//延期成交反推
bool CGTAMsgConvert::onRecvRtnDeferMatch(TDeferMatch * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize)
{
	std::string tradecode = CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvRtnDeferMatch");
	CGTABufferBuilder cb(out_pszOutMsg,0);
	cb.AppendBlock("\0\0\0\0",strlen("\0\0\0\0"),4).
	AppendBlock(tradecode.c_str(),tradecode.size(),10).
	AppendBlock(pstTRspMsg->RspCode,strlen(pstTRspMsg->RspCode),20).
	AppendBlock(pstTRspMsg->RspMsg,strlen(pstTRspMsg->RspMsg),40).
	AppendBlock(pstUsrData->matchNo,strlen(pstUsrData->matchNo),16).
	AppendChar(pstUsrData->buyOrSell).
	AppendBlock(pstUsrData->clientID,strlen(pstUsrData->clientID),12).
	AppendChar(pstUsrData->clientType).
	AppendBlock(pstUsrData->instID,strlen(pstUsrData->instID),8).
	AppendBlock(pstUsrData->memberID,strlen(pstUsrData->memberID),6).
	AppendBlock(pstUsrData->matchDate,strlen(pstUsrData->matchDate),8).
	AppendBlock(pstUsrData->matchTime,strlen(pstUsrData->matchTime),8).
	AppendDouble(pstUsrData->price).
	AppendInt(pstUsrData->volume).
	AppendChar(pstUsrData->offsetFlag).
	AppendBlock(pstUsrData->orderNo,strlen(pstUsrData->orderNo),14).
	AppendBlock(pstUsrData->localOrderNo,strlen(pstUsrData->localOrderNo),14);

	*in_lOutMsgSize = cb.GetIndex();
	int datalen = *in_lOutMsgSize-4;
	char buff[5]={0};
	sprintf(buff,"%04d",datalen);
	cb.FillBlock(0,buff,4,4);
	return true;
}

//延期报单撤销反推
bool CGTAMsgConvert::onRecvRtnDeferOrderCancel(TDeferOrder * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize)
{
	std::string tradecode = CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvRtnDeferOrderCancel");
	CGTABufferBuilder cb(out_pszOutMsg,0);
	cb.AppendBlock("\0\0\0\0",strlen("\0\0\0\0"),4).
	AppendBlock(tradecode.c_str(),tradecode.size(),10).
	AppendBlock(pstTRspMsg->RspCode,strlen(pstTRspMsg->RspCode),20).
	AppendBlock(pstTRspMsg->RspMsg,strlen(pstTRspMsg->RspMsg),40).
	AppendBlock(pstUsrData->orderNo,strlen(pstUsrData->orderNo),16).
	AppendBlock(pstUsrData->clientID,strlen(pstUsrData->clientID),12).
	AppendBlock(pstUsrData->instID,strlen(pstUsrData->instID),8).
	AppendBlock(pstUsrData->memberID,strlen(pstUsrData->memberID),6).
	AppendChar(pstUsrData->buyOrSell).
	AppendBlock(pstUsrData->applyDate,strlen(pstUsrData->applyDate),8).
	AppendBlock(pstUsrData->applyTime,strlen(pstUsrData->applyTime),8).
	AppendChar(pstUsrData->offSetFlag).
	AppendDouble(pstUsrData->price).AppendInt(pstUsrData->amount).AppendInt(pstUsrData->remainAmount).AppendBlock(pstUsrData->cancelTime,strlen(pstUsrData->cancelTime),8).AppendChar(pstUsrData->status).AppendBlock(pstUsrData->localOrderNo,strlen(pstUsrData->localOrderNo),14).AppendDouble(pstUsrData->margin).AppendChar(pstUsrData->marginType).AppendBlock(pstUsrData->traderID,strlen(pstUsrData->traderID),10).AppendBlock(pstUsrData->cancelID,strlen(pstUsrData->cancelID),10).AppendChar(pstUsrData->matchType);

	*in_lOutMsgSize = cb.GetIndex();
	int datalen = *in_lOutMsgSize-4;
	char buff[5]={0};
	sprintf(buff,"%04d",datalen);
	cb.FillBlock(0,buff,4,4);
	return true;
}
//延期市场状态改变
bool CGTAMsgConvert::onRecvRtnDeferMarketStateUpdate(TMarketState * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize)
{
	std::string tradecode = CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvRtnDeferMarketStateUpdate");
	CGTABufferBuilder cb(out_pszOutMsg,0);
	cb.AppendBlock("\0\0\0\0",strlen("\0\0\0\0"),4).
	AppendBlock(tradecode.c_str(),tradecode.size(),10).
	AppendBlock(pstTRspMsg->RspCode,strlen(pstTRspMsg->RspCode),20).
	AppendBlock(pstTRspMsg->RspMsg,strlen(pstTRspMsg->RspMsg),40).
	AppendBlock(pstUsrData->marketID,strlen(pstUsrData->marketID),2).
	AppendChar(pstUsrData->marketState);

	*in_lOutMsgSize = cb.GetIndex();
	int datalen = *in_lOutMsgSize-4;
	char buff[5]={0};
	sprintf(buff,"%04d",datalen);
	cb.FillBlock(0,buff,4,4);
	return true;
}

//延期合约状态改变
bool CGTAMsgConvert::onRecvRtnDeferInstStateUpdate(TInstState * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize)
{
	std::string tradecode = CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvRtnDeferInstStateUpdate");
	CGTABufferBuilder cb(out_pszOutMsg,0);
	cb.AppendBlock("\0\0\0\0",strlen("\0\0\0\0"),4).
	AppendBlock(tradecode.c_str(),tradecode.size(),10).
	AppendBlock(pstTRspMsg->RspCode,strlen(pstTRspMsg->RspCode),20).
	AppendBlock(pstTRspMsg->RspMsg,strlen(pstTRspMsg->RspMsg),40).
	AppendBlock(pstUsrData->instID,strlen(pstUsrData->instID),8).
	AppendChar(pstUsrData->tradeState);

	*in_lOutMsgSize = cb.GetIndex();
	int datalen = *in_lOutMsgSize-4;
	char buff[5]={0};
	sprintf(buff,"%04d",datalen);
	cb.FillBlock(0,buff,4,4);
	return true;
}

//延期费率改变
bool CGTAMsgConvert::onRecvDeferFeeRateUpdate(TDeferFeeRate * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize)
{
	std::string tradecode = CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvDeferFeeRateUpdate");
	CGTABufferBuilder cb(out_pszOutMsg,0);
	cb.AppendBlock("\0\0\0\0",strlen("\0\0\0\0"),4).
	AppendBlock(tradecode.c_str(),tradecode.size(),10).
	AppendBlock(pstTRspMsg->RspCode,strlen(pstTRspMsg->RspCode),20).
	AppendBlock(pstTRspMsg->RspMsg,strlen(pstTRspMsg->RspMsg),40).
	AppendBlock(pstUsrData->effectDate,strlen(pstUsrData->effectDate),8).
	AppendBlock(pstUsrData->instID,strlen(pstUsrData->instID),8).
	AppendChar(pstUsrData->payDirection).
	AppendDouble(pstUsrData->feeRate);

	*in_lOutMsgSize = cb.GetIndex();
	int datalen = *in_lOutMsgSize-4;
	char buff[5]={0};
	sprintf(buff,"%04d",datalen);
	cb.FillBlock(0,buff,4,4);
	return true;
}
//延期交割申报反推
bool CGTAMsgConvert::onRecvRtnDeferDeliveryAppOrder(TDeferDeliveryAppOrder * pstUsrData, TTRspMsg * pstTRspMsg, char * out_pszOutMsg, long* in_lOutMsgSize)
{
	std::string tradecode = CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvRtnDeferDeliveryAppOrder");
	CGTABufferBuilder cb(out_pszOutMsg,0);
	cb.AppendBlock("\0\0\0\0",strlen("\0\0\0\0"),4).
	AppendBlock(tradecode.c_str(),tradecode.size(),10).
	AppendBlock(pstTRspMsg->RspCode,strlen(pstTRspMsg->RspCode),20).
	AppendBlock(pstTRspMsg->RspMsg,strlen(pstTRspMsg->RspMsg),40).
	AppendBlock(pstUsrData->orderNo,strlen(pstUsrData->orderNo),16).
	AppendBlock(pstUsrData->clientID,strlen(pstUsrData->clientID),12).
	AppendBlock(pstUsrData->instID,strlen(pstUsrData->instID),8).
	AppendChar(pstUsrData->buyOrSell).
	AppendBlock(pstUsrData->applyDate,strlen(pstUsrData->applyDate),8).
	AppendBlock(pstUsrData->applyTime,strlen(pstUsrData->applyTime),8).
	AppendDouble(pstUsrData->amount).AppendBlock(pstUsrData->cancelTime,strlen(pstUsrData->cancelTime),8).AppendChar(pstUsrData->status).AppendBlock(pstUsrData->localOrderID,strlen(pstUsrData->localOrderID),14).AppendBlock(pstUsrData->memberID,strlen(pstUsrData->memberID),6).AppendBlock(pstUsrData->traderID,strlen(pstUsrData->traderID),10).AppendBlock(pstUsrData->cancelID,strlen(pstUsrData->cancelID),10).AppendInt(pstUsrData->remainAmount);

	*in_lOutMsgSize = cb.GetIndex();
	int datalen = *in_lOutMsgSize-4;
	char buff[5]={0};
	sprintf(buff,"%04d",datalen);
	cb.FillBlock(0,buff,4,4);
	return true;
}















//TODO CHH
bool CGTAMsgConvert::OnChannelLost(char * in_pszInMsg, char * out_pszOutMsg, long* in_lOutMsgSize)
{
	CGTABufferBuilder cb(out_pszOutMsg,0);
	cb.AppendBlock(in_pszInMsg,strlen(in_pszInMsg),strlen(in_pszInMsg));

	*in_lOutMsgSize = cb.GetIndex();
	return true;
}
/**注：这里的函数不要使用memcpy，由于都是小内存copy，
*这个函数效率低，后面将改成自定义的one-by-one copy.
*注这里的Append函数不要采用重载方式，容易出错。
*/

long CGTAMsgConvert::ConvertString2Struct(const char * in_pszMethod, char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize)
{
	std::map<std::string,pInFunc>::iterator it =	m_stInAPIAFunc.find(in_pszMethod);
	if (it != m_stInAPIAFunc.end())
	{
		pInFunc pfunc = it->second;
		return (this->*pfunc)(in_pszInMsg,  in_lInMsgLength,  out_pszOutMsg, in_lOutMsgSize);
	}

	ExAssert(0,std::string("在请求中没有找到报文转化方法[") + in_pszMethod + "]");


	return( -1 );


}




/**********************************************************
* 函 数 名： CTrMsgConvert::CTrMsgConvert
* 功能描述：* 返回说明： Constructor
* 开发历史：ukyo  于 20071014创建
************************************************************/

CGTAMsgConvert::CGTAMsgConvert()
{
	InitInAPIAFunc();		//add by zhugp 2011-3-28
}

CGTAMsgConvert instance;
CGTAMsgConvert& CGTAMsgConvert::Instance()
{
	return instance;
}


/**********************************************************
* 函 数 名： CTrMsgConvert::rl_ReqTrLogin
* 功能描述：InAPI-将平台交易员登录报文转化为调用API输入报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --输出空间的大小
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wupy   于 20070831 创建
************************************************************/

long CGTAMsgConvert::rl_ReqTrLogin(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize)
{
	// 定义返回值
	long	lRet = -1, lCount = 0;
	// 定义下一个开始取值的位置 和 下一个要取值的长度
	long lPos = 0, lLen = 0;
	// 交易员登录请求信息
	TReqTraderLogin *pstUsrData;
	char 	szTmp[512];
	char	szTmpD[512];

	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输入日志MY_NORMAL，MY_D3




	//将平台上传的结构转化为API所需要的结构TReqTraderLogin
	pstUsrData = (TReqTraderLogin*)out_pszOutMsg;

//	pstUsrData->ipAddress = "22.5.208.248";
	

	//1取交易员代码

	memset(szTmp, 0, sizeof(szTmp));
	lLen = 10;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据


	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "交易员代码为空");

		return -1;
	}
	pstUsrData->traderID = szTmp;
	lCount += strlen(szTmp);

	//2取会员代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 6;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取会员代码错误-报文长度%ld不够", in_lInMsgLength);

		return -1;
	}


	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "会员代码为空");

		return -1;
	}
	pstUsrData->memberID = szTmp;
	lCount += strlen(szTmp);

	//3取密码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 32;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取密码错误-报文长度%ld不够", in_lInMsgLength);

		return -1;
	}


	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "密码为空");

		return -1;
	}
	//对密码进行解密...
	if (0 > Singleton_InFunc<CCrypticDES>::Instance().rl_DecryptPwd(szTmp, szTmpD, sizeof(szTmp)))
	{
		strcpy(m_szErrMsg, "解密失败");

	}

	//

	pstUsrData->password = szTmpD;
	lCount += strlen(szTmp);

	//4取交易日期
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 8;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取交易日期错误-报文长度%ld不够", in_lInMsgLength);

		return -1;
	}


	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "交易日期为空");

		return -1;
	}
	pstUsrData->tradeDate = szTmp;
	lCount += strlen(szTmp);

	//IP地址在登录时使用，为前置机IP地址



	//6取最大本地报单号
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 14;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取最大本地报单号错误-报文长度%ld不够", in_lInMsgLength);

		return -1;
	}


	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "最大本地报单号为空");

		return -1;
	}
	pstUsrData->localOrderNo = szTmp;
	lCount += strlen(szTmp);

	//7取前置机编号
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 10;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取前置机编号错误-报文长度%ld不够", in_lInMsgLength);

		return -1;
	}


	UStringHelper::Trim(szTmp);
	pstUsrData->machineID = atoi(szTmp);
	lCount += sizeof(pstUsrData->machineID);


	//输入日志MY_D2，MY_D3





	//成功返回转化后的字节数，失败返回-1
	lRet = sizeof(TReqTraderLogin);
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_RspTrLogin
* 功能描述：OutAPI-将交易员登录响应回调API报文转化为平台报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- inout_plOutMsgPos         输入输出 --保存在outMsg报文当前保存位置
*- inout_ppszOutFileName         输入输出 --存文件的文件名
*- out_pblFollowupFlag         输出 --是否有后续报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：xxxx   于 20070831 创建
************************************************************/

long CGTAMsgConvert::rl_RspTrLogin(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag)
{
	long	lRet = -1;
	TTRspMsg * pstTRspMsg;
	TReqTraderLogin *pstUsrData;

	char szTmp[512];
	char szPwd[512];
	long lPos = *inout_plOutMsgPos, lLen = 0;
	int	 i;

	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3


	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");

		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");

		return -1;
	}
	//API回调返回结构是按TTRspMsg、TReqTraderLogin顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TReqTraderLogin*)((char*)in_pszInMsg + sizeof(TTRspMsg));

	//缺省为没有后续报文
	*out_pblFollowupFlag = false;

	//1、将API回调返回的结构TTRspMsg和TReqTraderLogin转化为平台下传的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//    (2)inout_plOutMsgPos为保存转化后内容的开始配置，转化完成后需要修改位置为当前写入内容的最位置后一字节
	//    以下项只在特定的函数中才有，如查询之类
	//    (3)out_pblFollowupFlag当有后续报文时，需要设置为true，否则设置为false
	//    (4)如果inout_plOutMsgPos不是0表示当前为后续报文，前面的报文存放在out_pszOutMsg中，转化后需要重新修改记录数

	//1.1转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;

	/*add by wangfei 091202
	char lBounce[256];

	memset(lBounce,0,sizeof(lBounce));
	GetPrivateProfileString("BOUNCE","CHANNEL_LOST","0",lBounce,sizeof(lBounce),"e:\\bounce_config.ini");

	int temp = atol(lBounce);
	if(temp!=0)
	{

	memcpy(&out_pszOutMsg[lPos], "RSP111111", lLen);
	}
	add by wangfei 091202 end*/
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);

	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);

		return -1;
	}



	//1.2转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);

		return -1;
	}



	//2.1转换交易员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->traderID, strlen(pstUsrData->traderID));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);

		return -1;
	}



	//2.2转换会员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->memberID, strlen(pstUsrData->memberID));
	lLen = 6;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换会员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);

		return -1;
	}



	//2.3转换密码
	memset(szTmp, 0, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->password, strlen(pstUsrData->password));

	//密码需要加密
	if (0 > Singleton_InFunc<CCrypticDES>::Instance().rl_EncryptPwd(szTmp, szPwd, sizeof(szPwd)))
	{
		strcpy(m_szErrMsg, "加密失败");

	}


	lLen = 32;
	//复制加密密码
	i = strlen(szPwd);
	if (lLen > i)
	{
		memset(&szPwd[i], 32, lLen -i);
	}
	memcpy(&out_pszOutMsg[lPos], szPwd, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换密码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);

		return -1;
	}

	//2.4转换交易日期
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->tradeDate, strlen(pstUsrData->tradeDate));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易日期错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);

		return -1;
	}



	//2.5转换IP地址
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->ipAddress, strlen(pstUsrData->ipAddress));
	lLen = 30;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换IP地址错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);

		return -1;
	}



	//2.6最大本地报单号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->localOrderNo, strlen(pstUsrData->localOrderNo));
	lLen = 14;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换最大本地报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);

		return -1;
	}



	//2.7前置机编号
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10ld",pstUsrData->machineID);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换前置机编号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);

		return -1;
	}



	




	//成功返回转化后的字节数，失败返回-1
	lRet = lPos - (*inout_plOutMsgPos);
	*inout_plOutMsgPos = lPos;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_ReqTrLogout
* 功能描述：InAPI-将平台交易员登出报文转化为调用API输入报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --输出空间的大小
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wupy   于 20070903 创建
************************************************************/

long CGTAMsgConvert::rl_ReqTrLogout(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize)
{
	// 定义返回值
	long	lRet = -1, lCount = 0;
	long lPos = 0, lLen = 0;
	// 交易员登出请求信息
	TReqTraderLogout *pstUsrData;
	char 	szTmp[512];

	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输入日志MY_NORMAL，MY_D3



	//有效性检查

	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");

		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");

		return -1;
	}
	if (sizeof(TReqTraderLogout) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "转化后的报文存放空间不足");

		return -1;
	}
	//1、将平台上传的结构转化为API所需要的结构TReqTraderLogout
	pstUsrData = (TReqTraderLogout*)out_pszOutMsg;



	//1取交易员代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 10;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取交易员代码错误-报文长度%ld不够", in_lInMsgLength);

		return -1;
	}


	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "交易员代码为空");

		return -1;
	}
	pstUsrData->traderID = szTmp;
	lCount += strlen(szTmp);

	//2取会员代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 6;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取会员代码错误-报文长度%ld不够", in_lInMsgLength);

		return -1;
	}


	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "会员代码为空");

		return -1;
	}
	pstUsrData->memberID = szTmp;
	lCount += strlen(szTmp);
	//输入日志MY_D2，MY_D3



	//成功返回转化后的字节数，失败返回-1
	lRet = sizeof(TReqTraderLogout);
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_RspTrLogout
* 功能描述：OutAPI-将交易员登出响应回调API报文转化为平台报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- inout_plOutMsgPos         输入输出 --保存在outMsg报文当前保存位置
*- inout_ppszOutFileName         输入输出 --存文件的文件名
*- out_pblFollowupFlag         输出 --是否有后续报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wupy   于 20070903 创建
************************************************************/

long CGTAMsgConvert::rl_RspTrLogout(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag)
{
	long	lRet = -1;
	TTRspMsg * pstTRspMsg;
	TReqTraderLogout *pstUsrData;

	char szTmp[512];
	long lPos = *inout_plOutMsgPos, lLen = 0;

	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3


	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");

		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");

		return -1;
	}

	//API回调返回结构是按TTRspMsg、TReqTraderLogout顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TReqTraderLogout*)((char*)in_pszInMsg + sizeof(TTRspMsg));

	//缺省为没有后续报文
	*out_pblFollowupFlag = false;
	//1、将API回调返回的结构TTRspMsg和TReqTraderLogout转化为平台下传的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//    (2)inout_plOutMsgPos为保存转化后内容的开始配置，转化完成后需要修改位置为当前写入内容的最位置后一字节
	//    以下项只在特定的函数中才有，如查询之类
	//    (3)out_pblFollowupFlag当有后续报文时，需要设置为true，否则设置为false
	//    (4)如果inout_plOutMsgPos不是0表示当前为后续报文，前面的报文存放在out_pszOutMsg中，转化后需要重新修改记录数

	//1.1转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;




	//1.2转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;




	//2.1转换交易员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->traderID, strlen(pstUsrData->traderID));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;




	//2.2转换会员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->memberID, strlen(pstUsrData->memberID));
	lLen = 6;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;




	



	//成功返回转化后的字节数，失败返回-1
	lRet = lPos - (*inout_plOutMsgPos);
	*inout_plOutMsgPos = lPos;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_ReqTrPwdUpdate
* 功能描述：InAPI-将平台交易员修改密码报文转化为调用API输入报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --输出空间的大小
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wupy   于 20070903 创建
************************************************************/

long CGTAMsgConvert::rl_ReqTrPwdUpdate(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize)
{
	long	lRet = -1, lCount = 0;
	long    lPos = 0, lLen = 0;
	TReqTraderPasswordUpdate *pstUsrData;
	char 	szTmp[512];
	char	szTmpD[512];
	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输入日志MY_NORMAL，MY_D3



	//有效性检查


	//1、将平台上传的结构转化为API所需要的结构TReqTraderPasswordUpdate
	pstUsrData = (TReqTraderPasswordUpdate*)out_pszOutMsg;


	//1取交易员代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 10;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "交易员代码为空");

		return -1;
	}
	pstUsrData->traderID = szTmp;
	lCount += strlen(szTmp);

	//2取会员代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 6;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;



	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "会员代码为空");

		return -1;
	}
	pstUsrData->memberID = szTmp;
	lCount += strlen(szTmp);

	//3取旧密码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 32;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取旧密码错误-报文长度%ld不够", in_lInMsgLength);

		return -1;
	}


	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "旧密码为空");

		return -1;
	}
	//解密密码
	if (0 > Singleton_InFunc<CCrypticDES>::Instance().rl_DecryptPwd(szTmp, szTmpD, sizeof(szTmpD)))
	{
		strcpy(m_szErrMsg, "解密失败");

	}

	pstUsrData->oldPassword = szTmpD;
	lCount += strlen(szTmp);

	//4取新密码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 32;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取新密码错误-报文长度%ld不够", in_lInMsgLength);

		return -1;
	}


	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "新密码为空");

		return -1;
	}
	//解密密码
	if (0 > Singleton_InFunc<CCrypticDES>::Instance().rl_DecryptPwd(szTmp, szTmpD, sizeof(szTmp)))
	{
		strcpy(m_szErrMsg, "解密失败");

	}

	pstUsrData->newPassword = szTmpD;
	lCount += strlen(szTmp);
	//输入日志MY_D2，MY_D3



	//成功返回转化后的字节数，失败返回-1
	lRet = sizeof(TReqTraderPasswordUpdate);
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_RspTrPwdUpdate
* 功能描述：OutAPI-将交易员密码修改响应回调API报文转化为平台报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- inout_plOutMsgPos         输入输出 --保存在outMsg报文当前保存位置
*- inout_ppszOutFileName         输入输出 --存文件的文件名
*- out_pblFollowupFlag         输出 --是否有后续报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wupy   于 20070903 创建
************************************************************/

long CGTAMsgConvert::rl_RspTrPwdUpdate(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag)
{
	long	lRet = -1;
	TTRspMsg * pstTRspMsg;
	TReqTraderPasswordUpdate *pstUsrData;
	char szTmp[512];
	char szPwd[512];
	int i;
	long lPos = *inout_plOutMsgPos, lLen = 0;
	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3


	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");

		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");

		return -1;
	}
	//API回调返回结构是按TTRspMsg、TReqTraderPasswordUpdate顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TReqTraderPasswordUpdate*)((char*)in_pszInMsg + sizeof(TTRspMsg));
	//缺省为没有后续报文
	*out_pblFollowupFlag = false;
	//1、将API回调返回的结构TTRspMsg和TReqTraderPasswordUpdate转化为平台下传的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//    (2)inout_plOutMsgPos为保存转化后内容的开始配置，转化完成后需要修改位置为当前写入内容的最位置后一字节
	//    以下项只在特定的函数中才有，如查询之类
	//    (3)out_pblFollowupFlag当有后续报文时，需要设置为true，否则设置为false
	//    (4)如果inout_plOutMsgPos不是0表示当前为后续报文，前面的报文存放在out_pszOutMsg中，转化后需要重新修改记录数

	//1.1转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;




	//1.2转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;



	//2.1转换交易员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->traderID, strlen(pstUsrData->traderID));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.2转换会员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->memberID, strlen(pstUsrData->memberID));
	lLen = 6;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换会员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.3转换旧密码
	memset(szTmp, 0, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->oldPassword, strlen(pstUsrData->oldPassword));

	//密码需要加密
	if (0 > Singleton_InFunc<CCrypticDES>::Instance().rl_EncryptPwd(szTmp, szPwd, sizeof(szPwd)))
	{
		strcpy(m_szErrMsg, "加密失败");
	}

	lLen = 32;
	//memset(szTmp, 32, sizeof(szPwd));
	//复制加密密码
	i = strlen(szPwd);
	if (lLen > i)
	{
		memset(&szPwd[i], 32, lLen -i);
	}
	memcpy(&out_pszOutMsg[lPos], szPwd, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换旧密码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.4转换新密码
	memset(szTmp, 0, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->newPassword, strlen(pstUsrData->newPassword));
	//密码需要加密
	if (0 > Singleton_InFunc<CCrypticDES>::Instance().rl_EncryptPwd(szTmp, szPwd, sizeof(szPwd)))
	{
		strcpy(m_szErrMsg, "加密失败");
	}

	lLen = 32;
	//memset(szTmp, 32, sizeof(szTmp));
	//复制加密密码
	i = strlen(szPwd);
	if (lLen > i)
	{
		memset(&szPwd[i], 32, lLen -i);
	}
	memcpy(&out_pszOutMsg[lPos], szPwd, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换新密码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	
	//成功返回转化后的字节数，失败返回-1
	lRet = lPos - (*inout_plOutMsgPos);
	*inout_plOutMsgPos = lPos;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_ReqMemberUrgentPwdUpdate
* 功能描述：InAPI-将平台会员修改应急密码报文转化为调用API输入报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --输出空间的大小
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wupy   于 20070903 创建
************************************************************/

long CGTAMsgConvert::rl_ReqMemberUrgentPwdUpdate(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize)
{
	long	lRet = -1, lCount = 0;
	long	lPos = 0, lLen = 0;
	char 	szTmp[512];
	char    szTmpD[512];
	TReqMemberUrgentPasswordUpdate *pstUsrData;

	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输入日志MY_NORMAL，MY_D3

	//有效性检查

	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (sizeof(TReqMemberUrgentPasswordUpdate) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "转化后的报文存放空间不足");
		return -1;
	}
	//1、将平台上传的结构转化为API所需要的结构TReqMemberUrgentPasswordUpdate
	pstUsrData = (TReqMemberUrgentPasswordUpdate*)out_pszOutMsg;

	//1取会员代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 6;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取会员代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "会员代码为空");
		return -1;
	}
	pstUsrData->memberID = szTmp;
	lCount += strlen(szTmp);

	//2取旧密码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 32;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取旧密码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "旧密码为空");
		return -1;
	}

	//需要解密密码
	if (0 > Singleton_InFunc<CCrypticDES>::Instance().rl_DecryptPwd(szTmp, szTmpD, sizeof(szTmpD)))
	{
		strcpy(m_szErrMsg, "解密失败");
	}


	pstUsrData->oldPassword = szTmpD;
	lCount += strlen(szTmp);

	//3取新密码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 32;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取新密码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "新密码为空");
		return -1;
	}

	//需要解密密码
	if (0 > Singleton_InFunc<CCrypticDES>::Instance().rl_DecryptPwd(szTmp, szTmpD, sizeof(szTmpD)))
	{
		strcpy(m_szErrMsg, "解密失败");
	}


	pstUsrData->newPassword = szTmp;

	lCount += strlen(szTmp);


	//输入日志MY_D2，MY_D3
	//成功返回转化后的字节数，失败返回-1
	lRet = sizeof(TReqMemberUrgentPasswordUpdate);
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_RspMemberUrgentPwdUpdate
* 功能描述：OutAPI-将会员修改应急密码响应回调API报文转化为平台报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- inout_plOutMsgPos         输入输出 --保存在outMsg报文当前保存位置
*- inout_ppszOutFileName         输入输出 --存文件的文件名
*- out_pblFollowupFlag         输出 --是否有后续报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wupy   于 20070903 创建
************************************************************/

long CGTAMsgConvert::rl_RspMemberUrgentPwdUpdate(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag)
{
	long	lRet = -1;
	TTRspMsg * pstTRspMsg;
	TReqMemberUrgentPasswordUpdate *pstUsrData;
	char	szTmp[512];
	char    szPwd[512];
	long	lPos = *inout_plOutMsgPos, lLen = 0;
	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));

	//输出日志MY_NORMAL，MY_D3

	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	//API回调返回结构是按TTRspMsg、TReqMemberUrgentPasswordUpdate顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TReqMemberUrgentPasswordUpdate*)((char*)in_pszInMsg + sizeof(TTRspMsg));
	//缺省为没有后续报文
	*out_pblFollowupFlag = false;

	//1、将API回调返回的结构TTRspMsg和TReqMemberUrgentPasswordUpdate转化为平台下传的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//    (2)inout_plOutMsgPos为保存转化后内容的开始配置，转化完成后需要修改位置为当前写入内容的最位置后一字节
	//    以下项只在特定的函数中才有，如查询之类
	//    (3)out_pblFollowupFlag当有后续报文时，需要设置为true，否则设置为false
	//    (4)如果inout_plOutMsgPos不是0表示当前为后续报文，前面的报文存放在out_pszOutMsg中，转化后需要重新修改记录数
	//1.1转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//1.2转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.1转换会员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->memberID, strlen(pstUsrData->memberID));
	lLen = 6;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换会员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.2转换旧密码
	memset(szTmp, 0, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->oldPassword, strlen(pstUsrData->oldPassword));
	//加密密码
	if (0 > Singleton_InFunc<CCrypticDES>::Instance().rl_EncryptPwd(szTmp, szPwd, sizeof(szPwd)))
	{
		strcpy(m_szErrMsg, "加密失败");
	}


	lLen = 32;
	memset(szTmp, 32, sizeof(szTmp));
	//保存加密码

	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换旧密码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.3转换新密码
	memset(szTmp, 0, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->newPassword, strlen(pstUsrData->newPassword));
	//加密密码
	if (0 > Singleton_InFunc<CCrypticDES>::Instance().rl_EncryptPwd(szTmp, szPwd, sizeof(szPwd)))
	{
		strcpy(m_szErrMsg, "加密失败");
	}


	lLen = 32;
	memset(szTmp, 32, sizeof(szTmp));
	//保存加密码

	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换新密码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}
	



	//成功返回转化后的字节数，失败返回-1
	lRet = lPos - (*inout_plOutMsgPos);
	*inout_plOutMsgPos = lPos;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_ReqQryMemberCapital
* 功能描述：InAPI-将平台会员资金查询报文转化为调用API输入报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --输出空间的大小
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wupy   于 20070903 创建
************************************************************/

long CGTAMsgConvert::rl_ReqQryMemberCapital(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize)
{
	long	lRet = -1, lCount = 0;
	long lPos = 0, lLen = 0;
	TReqQryMemberCapital *pstUsrData;
	char 	szTmp[512];
	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));

	//输入日志MY_NORMAL，MY_D3

	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (sizeof(TReqQryMemberCapital) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "转化后的报文存放空间不足");
		return -1;
	}
	//1、将平台上传的结构转化为API所需要的结构TReqQryMemberCapital
	pstUsrData = (TReqQryMemberCapital*)out_pszOutMsg;


	//1.1取会员代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 6;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取会员代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "会员代码为空");
		return -1;
	}
	pstUsrData->memberID = szTmp;
	lCount += strlen(szTmp);

	//1.2取帐户类型
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 1;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取帐户类型错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	if (0 != strlen(szTmp))
	{
		pstUsrData->accountType = szTmp[0];
	}
	lCount += 1;

	//输入日志MY_D2，MY_D3
	//成功返回转化后的字节数，失败返回-1
	lRet = lCount;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_RspQryMemberCapital
* 功能描述：OutAPI-将会员资金查询响应回调API报文转化为平台报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- inout_plOutMsgPos         输入输出 --保存在outMsg报文当前保存位置
*- inout_ppszOutFileName         输入输出 --存文件的文件名
*- out_pblFollowupFlag         输出 --是否有后续报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wupy   于 20070903 创建
************************************************************/

long CGTAMsgConvert::rl_RspQryMemberCapital(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag)
{
	long	lRet = -1;
	TTRspMsg * pstTRspMsg;
	TMemberCapital *pstUsrData;
	char	szTmp[512];
	long	lPos = *inout_plOutMsgPos, lLen = 0;
	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输入日志MY_NORMAL，MY_D3
	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	//API回调返回结构是按TTRspMsg、TMemberCapital顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TMemberCapital*)((char*)in_pszInMsg + sizeof(TTRspMsg));

	//缺省为没有后续报文
	*out_pblFollowupFlag = false;

	//1、将API回调返回的结构TTRspMsg和TMemberCapital转化为平台下传的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//    (2)inout_plOutMsgPos为保存转化后内容的开始配置，转化完成后需要修改位置为当前写入内容的最位置后一字节
	//    以下项只在特定的函数中才有，如查询之类
	//    (3)out_pblFollowupFlag当有后续报文时，需要设置为true，否则设置为false
	//    (4)如果inout_plOutMsgPos不是0表示当前为后续报文，前面的报文存放在out_pszOutMsg中，转化后需要重新修改记录数
	//1.1转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//1.2转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.1转换会员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->memberID, strlen(pstUsrData->memberID));
	lLen = 6;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换会员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.2转换帐户类型
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->accountType;
	lLen = 1;
	szTmp[lLen] = 0;

	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换帐户类型错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.3转换可提资金
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-17.2f",pstUsrData->available);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换可提资金错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.4转换上日结算准备金余额
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-17.2f",pstUsrData->lastBalance);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换上日结算准备金余额错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.5转换上日占用保证金
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-17.2f",pstUsrData->lastOccupied);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换上日占用保证金错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.6转换当日结算准备金余额
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-17.2f",pstUsrData->todayBalance);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换当日结算准备金余额错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.7转换当日占用保证金
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-17.2f",pstUsrData->todayOccupied);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换当日占用保证金错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.8转换当日盈亏
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-17.2f",pstUsrData->profit);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换当日盈亏错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.9转换当日入金
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-17.2f",pstUsrData->todayIn);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换当日入金错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.10转换当日出金
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-17.2f",pstUsrData->todayOut);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换当日出金错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.11转换支付的货款额
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-17.2f",pstUsrData->Payment);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换支付的货款额错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.12转换收到的货款额
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-17.2f",pstUsrData->Received);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换收到的货款额错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.13转换费用
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-17.2f",pstUsrData->miscFee);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换费用错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.14转换冻结保证金
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-17.2f",pstUsrData->frozen);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换冻结保证金错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.15转换基础保证金
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-17.2f",pstUsrData->basefund);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换基础保证金错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//输入日志MY_D2，MY_D3
	//成功返回转化后的字节数，失败返回-1
	lRet = lPos - (*inout_plOutMsgPos);
	*inout_plOutMsgPos = lPos;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_ReqQryClientStorage
* 功能描述：InAPI-将平台客户库存查询报文转化为调用API输入报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --输出空间的大小
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wupy   于 20070903 创建
************************************************************/

long CGTAMsgConvert::rl_ReqQryClientStorage(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize)
{
	long	lRet = -1, lCount = 0;
	long lPos = 0, lLen = 0;
	TReqQryClientStorage *pstUsrData;
	char 	szTmp[512];

	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));

	//输入日志MY_NORMAL，MY_D3
	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (sizeof(TReqQryClientStorage) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "转化后的报文存放空间不足");
		return -1;
	}
	//1、将平台上传的结构转化为API所需要的结构TReqQryClientStorage
	pstUsrData = (TReqQryClientStorage*)out_pszOutMsg;


	//1.1取客户代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 12;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取客户代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "客户代码为空");
		return -1;
	}
	pstUsrData->clientID = szTmp;
	lCount += strlen(szTmp);

	//1.2取会员代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 6;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取会员代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "会员代码为空");
		return -1;
	}
	pstUsrData->memberID = szTmp;
	lCount += strlen(szTmp);


	//输入日志MY_D2，MY_D3
	//成功返回转化后的字节数，失败返回-1
	lRet = sizeof(TReqQryClientStorage);
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_RspQryClientStorage
* 功能描述：OutAPI-将客户库存查询响应回调API报文转化为平台报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- inout_plOutMsgPos         输入输出 --保存在outMsg报文当前保存位置
*- inout_ppszOutFileName         输入输出 --存文件的文件名
*- out_pblFollowupFlag         输出 --是否有后续报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wupy   于 20070903 创建
************************************************************/

long CGTAMsgConvert::rl_RspQryClientStorage(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag)
{
	long	lRet = -1;
	TTRspMsg * pstTRspMsg;
	TClientStorage *pstUsrData;
	char	szTmp[512];
	long	lPos = 0, lLen = 0;

	lPos = *inout_plOutMsgPos;
	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输入日志MY_NORMAL，MY_D3
	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}

	//API回调返回结构是按TTRspMsg、TClientStorage顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TClientStorage*)((char*)in_pszInMsg + sizeof(TTRspMsg));

	//缺省为没有后续报文
	*out_pblFollowupFlag = false;

	//1、将API回调返回的结构TTRspMsg和TClientStorage转化为平台下传的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//    (2)inout_plOutMsgPos为保存转化后内容的开始配置，转化完成后需要修改位置为当前写入内容的最位置后一字节
	//    (3)out_pblFollowupFlag当有后续报文时，需要设置为true，否则设置为false
	//    (4)如果inout_plOutMsgPos不是0表示当前为后续报文，前面的报文存放在out_pszOutMsg中，转化后需要重新修改记录数

	//判断是否为第一次记录  lPos为0则是 否则不是
	if (0 == lPos)
	{
		//1.1转换响应代码
		memset(szTmp,  32, sizeof(szTmp));
		memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
		lLen = 20;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//1.2转换响应消息
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
		lLen = 40;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//1.3记录数量
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp, "%-4ld", 1);
		lLen = 4;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "写入记录数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}

	}
	else
	{
		//修改记录数量的值 增加1
		if (strlen((out_pszOutMsg)) < 64)
		{
			sprintf(m_szErrMsg, "修改记录数量错误-报文长度%ld不够", in_lOutMsgSize);
			return -1;
		}
		memset(szTmp, 0, sizeof(szTmp));
		memcpy(szTmp, &out_pszOutMsg[60], 4);
		long lNum = atol(szTmp);
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp, "%-4ld", ++lNum);
		memcpy(&out_pszOutMsg[60], szTmp, 4);

	}
	// CONTINUE_PACKAGE：有后续纪录 LAST_PACKAGE：最后一条记录，记录内容非空 OVER_PACKAGE：最后一条记录，记录内容空
	switch (pstTRspMsg->Flag)
	{
	case OVER_PACKAGE:
		break;
	case CONTINUE_PACKAGE:
		*out_pblFollowupFlag = true;
	case LAST_PACKAGE:
		//2.1转换客户代码
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->clientID, strlen(pstUsrData->clientID));
		lLen = 12;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换客户代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}

		//2.2转换会员代码
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->memberID, strlen(pstUsrData->memberID));
		lLen = 6;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换会员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}

		//2.3转换交割品种代码
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->varietyID, strlen(pstUsrData->varietyID));
		lLen = 3;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换交割品种代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}

		//2.4转换库存总量
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-17.2f",pstUsrData->totalStorage);
		lLen = 17;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换库存总量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}

		//2.5转换可用库存
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-17.2f",pstUsrData->availableStorage);
		lLen = 17;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换可用库存错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}

		//2.6转换现货冻结库存
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-17.2f",pstUsrData->frozenStorage);
		lLen = 17;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换现货冻结库存错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}

		//2.7转换待提库存
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-17.2f",pstUsrData->pendStorage);
		lLen = 17;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换待提库存错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}

		//2.8转换质押库存
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-17.2f",pstUsrData->impawnStorage);
		lLen = 17;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换质押库存错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}

		//2.9转换法律冻结库存
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-17.2f",pstUsrData->lawFrozen);
		lLen = 17;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换法律冻结库存错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}

		//2.10转换当日买入
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-17.2f",pstUsrData->todayBuy);
		lLen = 17;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换当日买入错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}

		//2.11转换当日卖出
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-17.2f",pstUsrData->todaySell);
		lLen = 17;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换当日卖出错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}

		//2.12转换当日存入
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-17.2f",pstUsrData->todayDeposit);
		lLen = 17;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换当日存入错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}

		//2.13转换当日提出
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-17.2f",pstUsrData->todayDraw);
		lLen = 17;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换当日提出错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}

		//2.14转换当日借出
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-17.2f",pstUsrData->todayLend);
		lLen = 17;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换当日借出错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}

		//2.15转换当日借入
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-17.2f",pstUsrData->todayBorrow);
		lLen = 17;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换当日借入错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}

		//2.16转换当日转出
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-17.2f",pstUsrData->todayShiftOut);
		lLen = 17;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换当日转出错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}

		//2.17转换当日转入
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-17.2f",pstUsrData->todayShiftIn);
		lLen = 17;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换当日转入错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}

		break;
	default:
		break;
	}

	//输入日志MY_D2，MY_D3
	//成功返回转化后的字节数，失败返回-1
	lRet = lPos - (*inout_plOutMsgPos);
	*inout_plOutMsgPos = lPos;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_ReqQryBulletinBoard
* 功能描述：InAPI-将平台交易所公告查询报文转化为调用API输入报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --输出空间的大小
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wupy   于 20070903 创建
************************************************************/

long CGTAMsgConvert::rl_ReqQryBulletinBoard(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize)
{
	long	lRet = -1, lCount = 0;
	long lPos = 0, lLen = 0;
	TReqQryBulletinBoard *pstUsrData;
	char 	szTmp[512];
	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));

	//输入日志MY_NORMAL，MY_D3
	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}

	if (sizeof(TReqQryBulletinBoard) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "转化后的报文存放空间不足");
		return -1;
	}
	//1、将平台上传的结构转化为API所需要的结构TReqQryBulletinBoard
	pstUsrData = (TReqQryBulletinBoard*)out_pszOutMsg;


	//1.1取会员代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 6;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取会员代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->memberID = szTmp;
	lCount += strlen(szTmp);

	//1.2取发布日期
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 8;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取发布日期错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->announceDate = szTmp;
	lCount += strlen(szTmp);

	//输入日志MY_D2，MY_D3
	//成功返回转化后的字节数，失败返回-1
	lRet = sizeof(TReqQryBulletinBoard);
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_RspQryBulletinBoard
* 功能描述：OutAPI-将交易所公告查询响应回调API报文转化为平台报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- inout_plOutMsgPos         输入输出 --保存在outMsg报文当前保存位置
*- inout_pszOutFileName         输入输出 --存文件的文件名
*- out_pblFollowupFlag         输出 --是否有后续报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wupy   于 20070904 创建
************************************************************/

long CGTAMsgConvert::rl_RspQryBulletinBoard(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_pszOutFileName, bool * out_pblFollowupFlag)
{
	long	lRet = -1, lCount = 0;
	TTRspMsg * pstTRspMsg;
	TBulletinBoard *pstUsrData;
	char	szTmp[512];
	char szBuffer[1024] = "\0";
	long	lPos = 0, lLen = 0;

	lPos = *inout_plOutMsgPos;
	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}

	if (NULL == inout_pszOutFileName)
	{
		strcpy(m_szErrMsg, "没有为存文件的文件名分配空间");
		return -1;
	}
	//API回调返回结构是按TTRspMsg、TBulletinBoard顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TBulletinBoard*)((char*)in_pszInMsg + sizeof(TTRspMsg));
	//缺省为没有后续报文
	*out_pblFollowupFlag = false;
	//1、将API回调返回的结构TTRspMsg和TBulletinBoard转化为平台下传的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//    (2)inout_plOutMsgPos为保存转化后内容的开始配置，转化完成后需要修改位置为当前写入内容的最位置后一字节
	//    (3)out_pblFollowupFlag当有后续报文时，需要设置为true，否则设置为false
	//    (4)如果inout_plOutMsgPos不是0表示当前为后续报文，前面的报文存放在out_pszOutMsg中，转化后需要重新修改记录数
	//注意当前记录内容大，记录多，因此需要存入文件，如果文件名是NULL，则需要建立文件名
	if (0 == strlen(inout_pszOutFileName))
	{
		sprintf(inout_pszOutFileName, "TrGdBulletinB%0ld", getpid());
	}
	//判断是否为第一次记录  lPos为0则是 否则不是
	if (0 == lPos)
	{
		//1.1转换响应代码
		memset(szTmp,  32, sizeof(szTmp));
		memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
		lLen = 20;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//1.2转换响应消息
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
		lLen = 40;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//1.3记录数量
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp, "%-4ld", 1);
		lLen = 4;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "写入记录数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//1.4接收的记录文件
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, inout_pszOutFileName, strlen(inout_pszOutFileName));
		lLen = 32;
		szTmp[lLen] = 0;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "写入接收的记录文件错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		lCount += lPos - (*inout_plOutMsgPos);
		*inout_plOutMsgPos = lPos;
	}
	else
	{
		//修改记录数量的值 增加1
		if (strlen((out_pszOutMsg)) < 64)
		{
			sprintf(m_szErrMsg, "修改记录数量错误-报文长度%ld不够", in_lOutMsgSize);
			return -1;
		}
		memset(szTmp, 0, sizeof(szTmp));
		memcpy(szTmp, &out_pszOutMsg[60], 4);
		long lNum = atol(szTmp);
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp, "%-4ld", ++lNum);
		memcpy(&out_pszOutMsg[60], szTmp, 4);

	}
	// CONTINUE_PACKAGE：有后续纪录 LAST_PACKAGE：最后一条记录，记录内容非空 OVER_PACKAGE：最后一条记录，记录内容空
	switch (pstTRspMsg->Flag)
	{
	case OVER_PACKAGE:
		break;
	case CONTINUE_PACKAGE:
		*out_pblFollowupFlag = true;
	case LAST_PACKAGE:
	{
		lPos = 0;
		memset(szBuffer, 0, sizeof(szBuffer));
		//2.1转换会员代码
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->memberID, strlen(pstUsrData->memberID));
		lLen = 6;
		memcpy(&szBuffer[lPos], szTmp, lLen);
		lPos += lLen;

		//2.2转换序号
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-10d",pstUsrData->seqNo);
		lLen = 10;
		memcpy(&szBuffer[lPos], szTmp, lLen);
		lPos += lLen;

		//2.3转换发布日期
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->announceDate, strlen(pstUsrData->announceDate));
		lLen = 8;
		memcpy(&szBuffer[lPos], szTmp, lLen);
		lPos += lLen;

		//2.4转换发布时间
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->announceTime, strlen(pstUsrData->announceTime));
		lLen = 8;
		memcpy(&szBuffer[lPos], szTmp, lLen);
		lPos += lLen;

		//2.5转换部门代码
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->deptID, strlen(pstUsrData->deptID));
		lLen = 4;
		memcpy(&szBuffer[lPos], szTmp, lLen);
		lPos += lLen;

		//2.6转换标题
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->title, strlen(pstUsrData->title));
		lLen = 40;
		memcpy(&szBuffer[lPos], szTmp, lLen);
		lPos += lLen;

		//2.7转换内容
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->content, strlen(pstUsrData->content));
		lLen = 254;
		memcpy(&szBuffer[lPos], szTmp, lLen);
		lPos += lLen;

		//2.8转换发布人
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->announcer, strlen(pstUsrData->announcer));
		lLen = 10;
		memcpy(&szBuffer[lPos], szTmp, lLen);
		lPos += lLen;


		//将记录写入文件
		szBuffer[lPos] = '\n';
		FILE *fp = NULL;
		fp = fopen(inout_pszOutFileName, "a");
		if (NULL == fp)
		{
			sprintf(m_szErrMsg, "打开接收的记录文件[%s]失败", inout_pszOutFileName);
			return -1;
		}
		fprintf(fp, "%s", szBuffer);
		fclose(fp);
		lCount += lPos;
	}
	break;
	default:
		break;
	}
	//成功返回转化后的字节数，失败返回-1
	lRet = lCount;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_ReqQryInterQuotation
* 功能描述：InAPI-将平台国际行情查询报文转化为调用API输入报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --输出空间的大小
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wupy   于 20070904 创建
************************************************************/

long CGTAMsgConvert::rl_ReqQryInterQuotation(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize)
{
	long	lRet = -1, lCount = 0;
	long lPos = 0, lLen = 0;
	TInterQuotation *pstUsrData;
	char 	szTmp[512];
	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));

	//输入日志MY_NORMAL，MY_D3

	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}

	if (sizeof(TInterQuotation) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "转化后的报文存放空间不足");
		return -1;
	}

	//1、将平台上传的结构转化为API所需要的结构TInterQuotation
	pstUsrData = (TInterQuotation*)out_pszOutMsg;


	//1.1取内容
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 254;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取内容错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->content = szTmp;
	lCount += strlen(szTmp);

	//1.2取发布人
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 10;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取发布人错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->announcer = szTmp;
	lCount += strlen(szTmp);

	//1.3取发布时间
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 8;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取发布时间错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->announceTime = szTmp;
	lCount += strlen(szTmp);

	//1.4取发布日期
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 8;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取发布日期错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->announceDate = szTmp;
	lCount += strlen(szTmp);

	//输入日志MY_D2，MY_D3
	//成功返回转化后的字节数，失败返回-1
	lRet = sizeof(TInterQuotation);
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_RspQryInterQuotation
* 功能描述：OutAPI-将国际行情查询响应回调API报文转化为平台报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- inout_plOutMsgPos         输入输出 --保存在outMsg报文当前保存位置
*- inout_pszOutFileName         输入输出 --存文件的文件名
*- out_pblFollowupFlag         输出 --是否有后续报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wupy   于 20070904 创建
************************************************************/

long CGTAMsgConvert::rl_RspQryInterQuotation(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_pszOutFileName, bool * out_pblFollowupFlag)
{
	long	lRet = -1, lCount = 0;
	TTRspMsg * pstTRspMsg;
	TInterQuotation *pstUsrData;
	char	szTmp[512];
	char szBuffer[1024] = "\0";
	long	lPos = 0, lLen = 0;
	lPos = *inout_plOutMsgPos;
	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}

	if (NULL == inout_pszOutFileName )
	{
		strcpy(m_szErrMsg, "没有为存文件的文件名分配空间");
		return -1;
	}
	//API回调返回结构是按TTRspMsg、TInterQuotation顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TInterQuotation*)((char*)in_pszInMsg + sizeof(TTRspMsg));
	//缺省为没有后续报文
	*out_pblFollowupFlag = false;
	//1、将API回调返回的结构TTRspMsg和TInterQuotation转化为平台下传的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//    (2)inout_plOutMsgPos为保存转化后内容的开始配置，转化完成后需要修改位置为当前写入内容的最位置后一字节
	//    (3)out_pblFollowupFlag当有后续报文时，需要设置为true，否则设置为false
	//    (4)如果inout_plOutMsgPos不是0表示当前为后续报文，前面的报文存放在out_pszOutMsg中，转化后需要重新修改记录数
	//注意当前记录内容大，记录多，因此需要存入文件，如果文件名是NULL，则需要建立文件名
	if (0 == strlen(inout_pszOutFileName))
	{
		sprintf(inout_pszOutFileName, "TrGdInterQuot%0ld", getpid());
	}
	//判断是否为第一次记录  lPos为0则是 否则不是
	if (0 == lPos)
	{
		//1.1转换响应代码
		memset(szTmp,  32, sizeof(szTmp));
		memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
		lLen = 20;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//1.2转换响应消息
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
		lLen = 40;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//1.3记录数量
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp, "%-4ld", 1);
		lLen = 4;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "写入记录数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//1.4接收的记录文件
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, inout_pszOutFileName, strlen(inout_pszOutFileName));
		lLen = 32;
		szTmp[lLen] = 0;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "写入接收的记录文件错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		lCount += lPos - (*inout_plOutMsgPos);
		*inout_plOutMsgPos = lPos;
	}
	else
	{
		//修改记录数量的值 增加1
		if (strlen((out_pszOutMsg)) < 64)
		{
			sprintf(m_szErrMsg, "修改记录数量错误-报文长度%ld不够", in_lOutMsgSize);
			return -1;
		}
		memset(szTmp, 0, sizeof(szTmp));
		memcpy(szTmp, &out_pszOutMsg[60], 4);
		long lNum = atol(szTmp);
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp, "%-4ld", ++lNum);
		memcpy(&out_pszOutMsg[60], szTmp, 4);

	}
	// CONTINUE_PACKAGE：有后续纪录 LAST_PACKAGE：最后一条记录，记录内容非空 OVER_PACKAGE：最后一条记录，记录内容空
	switch (pstTRspMsg->Flag)
	{
	case OVER_PACKAGE:
		break;
	case CONTINUE_PACKAGE:
		*out_pblFollowupFlag = true;
	case LAST_PACKAGE:
	{
		lPos = 0;
		memset(szBuffer, 0, sizeof(szBuffer));
		//2.1转换内容
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->content, strlen(pstUsrData->content));
		lLen = 254;
		memcpy(&szBuffer[lPos], szTmp, lLen);
		lPos += lLen;

		//2.2转换发布人
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->announcer, strlen(pstUsrData->announcer));
		lLen = 10;
		memcpy(&szBuffer[lPos], szTmp, lLen);
		lPos += lLen;

		//2.3转换发布时间
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->announceTime, strlen(pstUsrData->announceTime));
		lLen = 8;
		memcpy(&szBuffer[lPos], szTmp, lLen);
		lPos += lLen;

		//2.4转换发布日期
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->announceDate, strlen(pstUsrData->announceDate));
		lLen = 8;
		memcpy(&szBuffer[lPos], szTmp, lLen);
		lPos += lLen;


		//将记录写入文件
		szBuffer[lPos] = '\n';
		FILE *fp = NULL;
		fp = fopen(inout_pszOutFileName, "a");
		if (NULL == fp)
		{
			sprintf(m_szErrMsg, "打开接收的记录文件[%s]失败", inout_pszOutFileName);
			return -1;
		}
		fprintf(fp, "%s", szBuffer);
		fclose(fp);
		lCount += lPos;
	}
	break;
	default:
		break;
	}
	//成功返回转化后的字节数，失败返回-1
	lRet = lCount;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_ReqQryClient
* 功能描述：InAPI-将平台会员客户查询报文转化为调用API输入报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --输出空间的大小
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wupy   于 20070904 创建
************************************************************/

long CGTAMsgConvert::rl_ReqQryClient(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize)
{
	long	lRet = -1, lCount = 0;
	long lPos = 0, lLen = 0;
	TReqQryClient *pstUsrData;
	char 	szTmp[512];
	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));

	//输入日志MY_NORMAL，MY_D3
	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}

	if (sizeof(TReqQryClient) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "转化后的报文存放空间不足");
		return -1;
	}
	//1、将平台上传的结构转化为API所需要的结构TReqQryClient
	pstUsrData = (TReqQryClient*)out_pszOutMsg;

	//1.1取会员代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 6;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取会员代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "会员代码为空");
		return -1;
	}
	pstUsrData->memberID = szTmp;
	lCount += strlen(szTmp);

	//1.2取客户代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 12;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取客户代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->clientID = szTmp;
	lCount += strlen(szTmp);

	//输入日志MY_D2，MY_D3
	//成功返回转化后的字节数，失败返回-1
	lRet = sizeof(TReqQryClient);
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_ReqQryDeferFeeRate
* 功能描述：InAPI-将平台递延费率查询请求报文转化为调用API输入报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --输出空间的大小
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei于 20091109 创建
************************************************************/

long CGTAMsgConvert::rl_ReqQryDeferFeeRate(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize)
{
	long	lRet = -1, lCount = 0;
	long lPos = 0, lLen = 0;
	TReqQryDeferFeeRate *pstUsrData;
	char 	szTmp[512];
	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));

	//输入日志MY_NORMAL，MY_D3
	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}

	if (sizeof(TReqQryDeferFeeRate) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "转化后的报文存放空间不足");
		return -1;
	}
	//1、将平台上传的结构转化为API所需要的结构TReqQryDeferFeeRate
	pstUsrData = (TReqQryDeferFeeRate*)out_pszOutMsg;


	//1取起始日期
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 8;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取起始日期错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "起始日期为空");
		return -1;
	}
	pstUsrData->effectDateBegin = szTmp;
	lCount += strlen(szTmp);

	//2取截止日期
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 8;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取截止日期错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "起始日期为空");
		return -1;
	}
	pstUsrData->effectDateEnd = szTmp;
	lCount += strlen(szTmp);


	//输入日志MY_D2，MY_D3
	//成功返回转化后的字节数，失败返回-1
	lRet = sizeof(TReqQryDeferFeeRate);
	return lRet;
}


/**********************************************************
* 函 数 名： CTrMsgConvert::rl_RspQryClient
* 功能描述：OutAPI-将会员客户查询响应回调API报文转化为平台报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- inout_plOutMsgPos         输入输出 --保存在outMsg报文当前保存位置
*- inout_pszOutFileName         输入输出 --存文件的文件名
*- out_pblFollowupFlag         输出 --是否有后续报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wupy   于 20070904 创建
************************************************************/

long CGTAMsgConvert::rl_RspQryClient(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_pszOutFileName, bool * out_pblFollowupFlag)
{
	long	lRet = -1, lCount = 0;
	TTRspMsg * pstTRspMsg;
	TClient *pstUsrData;
	char	szTmp[512];
	char szBuffer[1024] = "\0";
	long	lPos = 0, lLen = 0;
	lPos = *inout_plOutMsgPos;
	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));

	//输入日志MY_NORMAL，MY_D3

	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}

	if (NULL == inout_pszOutFileName )
	{
		strcpy(m_szErrMsg, "没有为存文件的文件名分配空间");
		return -1;
	}
	//API回调返回结构是按TTRspMsg、TClient顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TClient*)((char*)in_pszInMsg + sizeof(TTRspMsg));
	//缺省为没有后续报文
	*out_pblFollowupFlag = false;
	//1、将API回调返回的结构TTRspMsg和TClient转化为平台下传的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//    (2)inout_plOutMsgPos为保存转化后内容的开始配置，转化完成后需要修改位置为当前写入内容的最位置后一字节
	//    (3)out_pblFollowupFlag当有后续报文时，需要设置为true，否则设置为false
	//    (4)如果inout_plOutMsgPos不是0表示当前为后续报文，前面的报文存放在out_pszOutMsg中，转化后需要重新修改记录数
	//注意当前记录内容大，记录多，因此需要存入文件，如果文件名是NULL，则需要建立文件名
	if (0 == strlen(inout_pszOutFileName))
	{
		sprintf(inout_pszOutFileName, "trgold%0ld", getpid());
	}
	//判断是否为第一次记录  lPos为0则是 否则不是
	if (0 == lPos)
	{
		//1.1转换响应代码
		memset(szTmp,  32, sizeof(szTmp));
		memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
		lLen = 20;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//1.2转换响应消息
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
		lLen = 40;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//1.3记录数量
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp, "%-4ld", 1);
		lLen = 4;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "写入记录数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//1.4接收的记录文件
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, inout_pszOutFileName, strlen(inout_pszOutFileName));
		lLen = 32;
		szTmp[lLen] = 0;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "写入接收的记录文件错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		lCount += lPos - (*inout_plOutMsgPos);
		*inout_plOutMsgPos = lPos;
	}
	else
	{
		//修改记录数量的值 增加1
		if (strlen((out_pszOutMsg)) < 64)
		{
			sprintf(m_szErrMsg, "修改记录数量错误-报文长度%ld不够", in_lOutMsgSize);
			return -1;
		}
		memset(szTmp, 0, sizeof(szTmp));
		memcpy(szTmp, &out_pszOutMsg[60], 4);
		long lNum = atol(szTmp);
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp, "%-4ld", ++lNum);
		memcpy(&out_pszOutMsg[60], szTmp, 4);

	}
	// CONTINUE_PACKAGE：有后续纪录 LAST_PACKAGE：最后一条记录，记录内容非空 OVER_PACKAGE：最后一条记录，记录内容空
	switch (pstTRspMsg->Flag)
	{
	case OVER_PACKAGE:
		break;
	case CONTINUE_PACKAGE:
		*out_pblFollowupFlag = true;
	case LAST_PACKAGE:
	{
		lPos = 0;
		memset(szBuffer, 0, sizeof(szBuffer));
		//2.1转换客户代码
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->clientID, strlen(pstUsrData->clientID));
		lLen = 12;
		memcpy(&szBuffer[lPos], szTmp, lLen);
		lPos += lLen;

		//2.2转换会员代码
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->memberID, strlen(pstUsrData->memberID));
		lLen = 6;
		memcpy(&szBuffer[lPos], szTmp, lLen);
		lPos += lLen;

		//2.3转换交易权限
		lLen = 1;
		szBuffer[lPos] = pstUsrData->tradeRight;
		lPos += lLen;

		//2.4转换客户类型
		lLen = 1;
		szBuffer[lPos] = pstUsrData->clientType;
		lPos += lLen;

		//2.5转换注销状态
		lLen = 1;
		szBuffer[lPos] = pstUsrData->destroyFlag;
		lPos += lLen;


		//将记录写入文件
		szBuffer[lPos] = '\n';
		FILE *fp = NULL;
		fp = fopen(inout_pszOutFileName, "a");
		if (NULL == fp)
		{
			sprintf(m_szErrMsg, "打开接收的记录文件[%s]失败", inout_pszOutFileName);
			return -1;
		}
		fprintf(fp, "%s", szBuffer);
		fclose(fp);
		lCount += lPos;
	}
	break;
	default:
		break;
	}
	//成功返回转化后的字节数，失败返回-1
	lRet = lCount;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_ReqSpotOrder
* 功能描述：InAPI-将平台现货报单请求报文转化为调用API输入报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --输出空间的大小
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：ukyo  于 20071014创建
************************************************************/

long CGTAMsgConvert::rl_ReqSpotOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize)
{
	long	lRet = -1, lCount = 0;
	long lPos = 0, lLen = 0;
	TSpotOrder *pstUsrData;
	char 	szTmp[512]={0};

	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));


	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "传入输出报文指针为空");
		return -1;
	}
	if (sizeof(TSpotOrder) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "转化后的报文存放空间不足");
		return -1;
	}
	//1、将平台上传的结构转化为API所需要的结构TSpotOrder
	pstUsrData = (TSpotOrder*)out_pszOutMsg;




	lPos+=6;
	//1.1取报单号
	//memset(szTmp, 0, sizeof(szTmp)); delete by zhugp 20110502
	lLen = 16;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取报单号错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}


	UStringHelper::Trim(szTmp);
	pstUsrData->orderNo = szTmp;
	lCount += strlen(szTmp);

	//1.2取合约代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 8;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取合约代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}


	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "合约代码为空");
		return -1;
	}
	pstUsrData->instID = szTmp;
	lCount += strlen(szTmp);

	//1.3取客户代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 12;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取客户代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}


	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "客户代码为空");
		return -1;
	}
	pstUsrData->clientID = szTmp;
	lCount += strlen(szTmp);

	//1.4取会员代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 6;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取会员代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}


	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "会员代码为空");
		return -1;
	}
	pstUsrData->memberID = szTmp;
	lCount += strlen(szTmp);

	//1.5取买卖方向
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 1;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取买卖方向错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}


	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "买卖方向为空");
		return -1;
	}
	pstUsrData->buyOrSell = szTmp[0];
	lCount += strlen(szTmp);

	//1.6取申报日期
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 8;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取申报日期错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}


	UStringHelper::Trim(szTmp);
	pstUsrData->applyDate = szTmp;
	lCount += strlen(szTmp);

	//1.7取申报时间
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 8;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取申报时间错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}


	UStringHelper::Trim(szTmp);
	pstUsrData->applyTime = szTmp;
	lCount += strlen(szTmp);

	//1.8取交易员代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 10;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取交易员代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}


	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "交易员代码为空");
		return -1;
	}
	pstUsrData->traderID = szTmp;
	lCount += strlen(szTmp);

	//1.9取价格
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 18;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取价格错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}


	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "价格为空");
		return -1;
	}
	pstUsrData->price = atof(szTmp);
	lCount += sizeof(pstUsrData->price);

	//1.10取数量
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 10;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取数量错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}


	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "数量为空");
		return -1;
	}
	pstUsrData->amount = atoi(szTmp);
	lCount += sizeof(pstUsrData->amount);

	//1.11取剩余数量
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 10;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取剩余数量错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}


	UStringHelper::Trim(szTmp);
	pstUsrData->remainAmount = atoi(szTmp);
	lCount += sizeof(pstUsrData->remainAmount);

	//1.12取撤销时间
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 8;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取撤销时间错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}


	UStringHelper::Trim(szTmp);
	pstUsrData->cancelTime = szTmp;
	lCount += strlen(szTmp);

	//1.13取撤销员代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 10;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取撤销员代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}


	UStringHelper::Trim(szTmp);
	pstUsrData->cancelID = szTmp;
	lCount += strlen(szTmp);

	//1.14取状态
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 1;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取状态错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}


	UStringHelper::Trim(szTmp);
	pstUsrData->status = szTmp[0];
	lCount += strlen(szTmp);

	//1.15取类型
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 1;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取类型错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}


	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "类型为空");
		return -1;
	}
	pstUsrData->matchType = szTmp[0];
	lCount += strlen(szTmp);

	//1.16取转让合约代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 8;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取转让合约代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}


	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "转让合约代码为空");
		return -1;
	}
	pstUsrData->endorseInstID = szTmp;
	lCount += strlen(szTmp);

	//1.17取本地报单号
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 14;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取本地报单号错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}


	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "本地报单号为空");
		return -1;
	}
	pstUsrData->localOrderNo = szTmp;
	lCount += strlen(szTmp);

	//成功返回转化后的字节数，失败返回-1
	lRet = sizeof(TSpotOrder);
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_ReqTrDeferOrder
* 功能描述：InAPI-将平台现货延期报单请求报文转化为调用API输入报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --输出空间的大小
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei  于 20091016创建
************************************************************/

long CGTAMsgConvert::rl_ReqDeferOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize)
{
	long	lRet = -1, lCount = 0;
	long lPos = 0, lLen = 0;
	TDeferOrder *pstUsrData=NULL;
	char 	szTmp[512] = {0};

	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));

	//输出日志MY_NORMAL，MY_D3


	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "传入输出报文指针为空");
		return -1;
	}
	if (sizeof(TDeferOrder) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "转化后的报文存放空间不足");
		return -1;
	}
	//1、将平台上传的结构转化为API所需要的结构TDeferOrder
	pstUsrData = (TDeferOrder*)out_pszOutMsg;

	//1.1取报单号
	//memset(szTmp, 0, sizeof(szTmp)); delete by zhugp 20110502
	lLen = 16;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取报单号错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->orderNo = szTmp;
	lCount += strlen(szTmp);

	//1.2取客户代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 12;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取客户代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "客户代码为空");
		return -1;
	}
	pstUsrData->clientID = szTmp;
	lCount += strlen(szTmp);


	//1.3取合约代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 8;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取合约代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "合约代码为空");
		return -1;
	}
	pstUsrData->instID = szTmp;
	lCount += strlen(szTmp);


	//1.4取会员代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 6;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取会员代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "会员代码为空");
		return -1;
	}
	pstUsrData->memberID = szTmp;
	lCount += strlen(szTmp);


	//1.5取买卖方向
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 1;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取买卖方向错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "买卖方向为空");
		return -1;
	}
	pstUsrData->buyOrSell = szTmp[0];
	lCount += strlen(szTmp);

	//1.6取申报日期
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 8;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取申报日期错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->applyDate = szTmp;
	lCount += strlen(szTmp);

	//1.7取申报时间
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 8;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取申报时间错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->applyTime = szTmp;
	lCount += strlen(szTmp);

	//1.8取开平仓标志
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 1;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取开平仓标志-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->offSetFlag = szTmp[0];
	lCount += strlen(szTmp);

	//1.9取价格
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 18;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取价格错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "价格为空");
		return -1;
	}
	pstUsrData->price = atof(szTmp);
	lCount += sizeof(pstUsrData->price);

	//1.10取数量
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 10;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取数量错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "数量为空");
		return -1;
	}
	pstUsrData->amount = atoi(szTmp);
	lCount += sizeof(pstUsrData->amount);

	//1.11取剩余数量
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 10;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取剩余数量错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->remainAmount = atoi(szTmp);
	lCount += sizeof(pstUsrData->remainAmount);

	//1.12取撤销时间
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 8;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取撤销时间错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->cancelTime = szTmp;
	lCount += strlen(szTmp);

	//1.13取状态
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 1;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取状态错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->status = szTmp[0];
	lCount += strlen(szTmp);

	//1.14取本地报单号
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 14;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取本地报单号错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "本地报单号为空");
		return -1;
	}
	pstUsrData->localOrderNo = szTmp;
	lCount += strlen(szTmp);

	//1.15取保证金率
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 18;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取保证金率错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "保证金率为空");
		return -1;
	}
	pstUsrData->margin = atof(szTmp);
	lCount += sizeof(pstUsrData->margin);

	//1.16取保证金类型
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 1;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取保证金类型错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->marginType = szTmp[0];
	lCount += strlen(szTmp);

	//1.17取交易员代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 10;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取交易员代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "交易员代码为空");
		return -1;
	}
	pstUsrData->traderID = szTmp;
	lCount += strlen(szTmp);

	//1.18取撤销员代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 10;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取撤销员代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->cancelID = szTmp;
	lCount += strlen(szTmp);

	//1.19取类型
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 1;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取类型错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "类型为空");
		return -1;
	}
	pstUsrData->matchType = szTmp[0];
	lCount += strlen(szTmp);


	

	//成功返回转化后的字节数，失败返回-1
	lRet = sizeof(TDeferOrder);
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_ReqMiddleAppOrder
* 功能描述：InAPI-将平台现货延期中立仓申报请求报文转化为调用API输入报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --输出空间的大小
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei  于 20091029创建
************************************************************/

long CGTAMsgConvert::rl_ReqMiddleAppOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize)
{
	long	lRet = -1, lCount = 0;
	long lPos = 0, lLen = 0;
	TMiddleAppOrder *pstUsrData;
	char 	szTmp[512];

	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));

	//输出日志MY_NORMAL，MY_D3


	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "传入输出报文指针为空");
		return -1;
	}
	if (sizeof(TMiddleAppOrder) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "转化后的报文存放空间不足");
		return -1;
	}
	//1、将平台上传的结构转化为API所需要的结构TMiddleAppOrder
	pstUsrData = (TMiddleAppOrder*)out_pszOutMsg;

	//1.1取报单号
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 16;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取报单号错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->orderNo = szTmp;
	lCount += strlen(szTmp);

	//1.2取客户代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 12;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取客户代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "客户代码为空");
		return -1;
	}
	pstUsrData->clientID = szTmp;
	lCount += strlen(szTmp);


	//1.3取合约代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 8;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取合约代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "合约代码为空");
		return -1;
	}
	pstUsrData->instID = szTmp;
	lCount += strlen(szTmp);


	//1.4取会员代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 6;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取会员代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "会员代码为空");
		return -1;
	}
	pstUsrData->memberID = szTmp;
	lCount += strlen(szTmp);


	//1.5取买卖方向
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 1;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取买卖方向错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "买卖方向为空");
		return -1;
	}
	pstUsrData->buyOrSell = szTmp[0];
	lCount += strlen(szTmp);

	//1.6取申报日期
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 8;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取申报日期错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->applyDate = szTmp;
	lCount += strlen(szTmp);

	//1.7取申报时间
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 8;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取申报时间错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->applyTime = szTmp;
	lCount += strlen(szTmp);


	//1.8取数量
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 10;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取数量错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "数量为空");
		return -1;
	}
	pstUsrData->amount = atoi(szTmp);
	lCount += sizeof(pstUsrData->amount);

	//1.9取撤销时间
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 8;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取撤销时间错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->cancelTime = szTmp;
	lCount += strlen(szTmp);

	//1.10取状态
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 1;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取状态错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->status = szTmp[0];
	lCount += strlen(szTmp);

	//1.11取本地报单号
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 14;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取本地报单号错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "本地报单号为空");
		return -1;
	}
	pstUsrData->localOrderID = szTmp;
	lCount += strlen(szTmp);

	//1.12取交易员代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 10;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取交易员代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "交易员代码为空");
		return -1;
	}
	pstUsrData->traderID = szTmp;
	lCount += strlen(szTmp);

	//1.13取撤销员代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 10;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取撤销员代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->cancelID = szTmp;
	lCount += strlen(szTmp);


	//1.14取剩余数量
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 10;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取剩余数量错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->remainAmount = atoi(szTmp);
	lCount += sizeof(pstUsrData->remainAmount);


	

	//成功返回转化后的字节数，失败返回-1
	lRet = sizeof(TMiddleAppOrder);
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_ReqMiddleAppOrderCancel
* 功能描述：InAPI-将平台现货延期中立仓申报撤销请求报文转化为调用API输入报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --输出空间的大小
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei  于 20091029创建
************************************************************/

long CGTAMsgConvert::rl_ReqMiddleAppOrderCancel(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize)
{
	long	lRet = -1, lCount = 0;
	long lPos = 0, lLen = 0;
	TMiddleAppOrderCancel *pstUsrData;
	char 	szTmp[512];

	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));

	//输出日志MY_NORMAL，MY_D3


	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "传入输出报文指针为空");
		return -1;
	}
	if (sizeof(TMiddleAppOrderCancel) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "转化后的报文存放空间不足");
		return -1;
	}
	//1、将平台上传的结构转化为API所需要的结构TMiddleAppOrder
	pstUsrData = (TMiddleAppOrderCancel*)out_pszOutMsg;

	//1.1取报单号
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 16;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取报单号错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->orderNo = szTmp;
	lCount += strlen(szTmp);

	//1.2取合约代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 8;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取合约代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "合约代码为空");
		return -1;
	}
	pstUsrData->instID = szTmp;
	lCount += strlen(szTmp);


	//1.3取客户代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 12;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取客户代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "客户代码为空");
		return -1;
	}
	pstUsrData->clientID = szTmp;
	lCount += strlen(szTmp);

	//1.4取会员代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 6;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取会员代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "会员代码为空");
		return -1;
	}
	pstUsrData->memberID = szTmp;
	lCount += strlen(szTmp);

	//1.5取交易员代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 10;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取交易员代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "交易员代码为空");
		return -1;
	}
	pstUsrData->traderID = szTmp;
	lCount += strlen(szTmp);



	//成功返回转化后的字节数，失败返回-1
	lRet = sizeof(TMiddleAppOrderCancel);
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_ReqDeferDeliveryAppOrderCancel
* 功能描述：InAPI-将平台现货延期交收交割撤销请求报文转化为调用API输入报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --输出空间的大小
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei  于 20091029创建
************************************************************/

long CGTAMsgConvert::rl_ReqDeferDeliveryAppOrderCancel(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize)
{
	long	lRet = -1, lCount = 0;
	long lPos = 0, lLen = 0;
	TDeferDeliveryAppOrderCancel *pstUsrData;
	char 	szTmp[512];

	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));

	//输出日志MY_NORMAL，MY_D3


	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "传入输出报文指针为空");
		return -1;
	}
	if (sizeof(TDeferDeliveryAppOrderCancel) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "转化后的报文存放空间不足");
		return -1;
	}
	//1、将平台上传的结构转化为API所需要的结构TMiddleAppOrder
	pstUsrData = (TDeferDeliveryAppOrderCancel*)out_pszOutMsg;

	//1.1取报单号
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 16;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取报单号错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->orderNo = szTmp;
	lCount += strlen(szTmp);

	//1.2取合约代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 8;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取合约代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "合约代码为空");
		return -1;
	}
	pstUsrData->instID = szTmp;
	lCount += strlen(szTmp);


	//1.3取客户代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 12;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取客户代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "客户代码为空");
		return -1;
	}
	pstUsrData->clientID = szTmp;
	lCount += strlen(szTmp);

	//1.4取会员代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 6;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取会员代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "会员代码为空");
		return -1;
	}
	pstUsrData->memberID = szTmp;
	lCount += strlen(szTmp);

	//1.5取交易员代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 10;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取交易员代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "交易员代码为空");
		return -1;
	}
	pstUsrData->traderID = szTmp;
	lCount += strlen(szTmp);


	

	//成功返回转化后的字节数，失败返回-1
	lRet = sizeof(TDeferDeliveryAppOrderCancel);
	return lRet;
}


/**********************************************************
* 函 数 名： CTrMsgConvert::rl_ReqDeferDeliveryAppOrder
* 功能描述：InAPI-将平台现货延期交收交割报单请求报文转化为调用API输入报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --输出空间的大小
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei  于 20091027创建
************************************************************/

long CGTAMsgConvert::rl_ReqDeferDeliveryAppOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize)
{
	long	lRet = -1, lCount = 0;
	long lPos = 0, lLen = 0;
	TDeferDeliveryAppOrder *pstUsrData;
	char 	szTmp[512];

	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));

	//输出日志MY_NORMAL，MY_D3


	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "传入输出报文指针为空");
		return -1;
	}
	if (sizeof(TDeferDeliveryAppOrder) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "转化后的报文存放空间不足");
		return -1;
	}
	//1、将平台上传的结构转化为API所需要的结构TDeferDeliveryAppOrder
	pstUsrData = (TDeferDeliveryAppOrder*)out_pszOutMsg;

	//1.1取报单号
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 16;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取报单号错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->orderNo = szTmp;
	lCount += strlen(szTmp);

	//1.2取客户代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 12;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取客户代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "客户代码为空");
		return -1;
	}
	pstUsrData->clientID = szTmp;
	lCount += strlen(szTmp);


	//1.3取合约代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 8;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取合约代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "合约代码为空");
		return -1;
	}
	pstUsrData->instID = szTmp;
	lCount += strlen(szTmp);

	//1.4取买卖方向
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 1;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取买卖方向错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "买卖方向为空");
		return -1;
	}
	pstUsrData->buyOrSell = szTmp[0];
	lCount += strlen(szTmp);

	//1.5取申报日期
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 8;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取申报日期错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->applyDate = szTmp;
	lCount += strlen(szTmp);

	//1.6取申报时间
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 8;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取申报时间错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->applyTime = szTmp;
	lCount += strlen(szTmp);

	//1.7取数量
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 10;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取数量错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "数量为空");
		return -1;
	}
	pstUsrData->amount = atoi(szTmp);
	lCount += sizeof(pstUsrData->amount);

	//1.8取撤销时间
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 8;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取撤销时间错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->cancelTime = szTmp;
	lCount += strlen(szTmp);

	//1.9取状态
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 1;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取状态错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->status = szTmp[0];
	lCount += strlen(szTmp);

	//1.10取本地报单号
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 14;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取本地报单号错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "本地报单号为空");
		return -1;
	}
	pstUsrData->localOrderID = szTmp;
	lCount += strlen(szTmp);

	//1.11取会员代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 6;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取会员代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "会员代码为空");
		return -1;
	}
	pstUsrData->memberID = szTmp;
	lCount += strlen(szTmp);

	//1.12取交易员代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 10;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取交易员代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "交易员代码为空");
		return -1;
	}
	pstUsrData->traderID = szTmp;
	lCount += strlen(szTmp);

	//1.13取撤销员代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 10;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取撤销员代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->cancelID = szTmp;
	lCount += strlen(szTmp);

	//1.14取剩余数量
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 10;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取剩余数量错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->remainAmount = atoi(szTmp);
	lCount += sizeof(pstUsrData->remainAmount);


	

	//成功返回转化后的字节数，失败返回-1
	lRet = sizeof(TDeferDeliveryAppOrder);
	return lRet;
}


/**********************************************************
* 函 数 名： CTrMsgConvert::rl_RspSpotOrder
* 功能描述：OutAPI-将现货报单和现货报单查询响应回调API报文转化为平台报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- inout_plOutMsgPos         输入输出 --保存在outMsg报文当前保存位置
*- inout_ppszOutFileName         输入输出 --存文件的文件名
*- out_pblFollowupFlag         输出 --是否有后续报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：ukyo  于 20071014创建
************************************************************/

long CGTAMsgConvert::rl_RspSpotOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag)
{
	long	lRet = -1;
	TTRspMsg * pstTRspMsg;
	TSpotOrder *pstUsrData;
	char szTmp[512];
	long lPos = *inout_plOutMsgPos, lLen = 0;
	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));

	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	//API回调返回结构是按TTRspMsg、TSpotOrder顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TSpotOrder*)((char*)in_pszInMsg + sizeof(TTRspMsg));
	//缺省为没有后续报文
	*out_pblFollowupFlag = false;
	//1、将API回调返回的结构TTRspMsg和TSpotOrder转化为平台下传的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//    (2)inout_plOutMsgPos为保存转化后内容的开始配置，转化完成后需要修改位置为当前写入内容的最位置后一字节
	//    以下项只在特定的函数中才有，如查询之类
	//    (3)out_pblFollowupFlag当有后续报文时，需要设置为true，否则设置为false
	//    (4)如果inout_plOutMsgPos不是0表示当前为后续报文，前面的报文存放在out_pszOutMsg中，转化后需要重新修改记录数

	//1.1转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//1.2转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.1转换报单号
	memset(szTmp, 32, sizeof(szTmp));

	memcpy(szTmp, pstUsrData->orderNo, strlen(pstUsrData->orderNo));
	lLen = 16;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.2转换合约代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.3转换客户代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->clientID, strlen(pstUsrData->clientID));
	lLen = 12;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换客户代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.4转换会员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->memberID, strlen(pstUsrData->memberID));
	lLen = 6;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换会员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.5转换买卖方向
	lLen = 1;
	out_pszOutMsg[lPos] = pstUsrData->buyOrSell;
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买卖方向错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.6转换申报日期
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->applyDate, strlen(pstUsrData->applyDate));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换申报日期错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.7转换申报时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->applyTime, strlen(pstUsrData->applyTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换申报时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.8转换交易员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->traderID, strlen(pstUsrData->traderID));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.9转换价格
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->price, 2);
	sprintf(szTmp, "%-18f", pstUsrData->price);
	lLen = 18;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换价格错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.10转换数量
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->amount);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.11转换剩余数量
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->remainAmount);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换剩余数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.12转换撤销时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->cancelTime, strlen(pstUsrData->cancelTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换撤销时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.13转换撤销员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->cancelID, strlen(pstUsrData->cancelID));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换撤销员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.14转换状态
	lLen = 1;
	out_pszOutMsg[lPos] = pstUsrData->status;
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换状态错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.15转换类型
	lLen = 1;
	out_pszOutMsg[lPos] = pstUsrData->matchType;
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换类型错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.16转换转让合约代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->endorseInstID, strlen(pstUsrData->endorseInstID));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换转让合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.17转换本地报单号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->localOrderNo, strlen(pstUsrData->localOrderNo));
	lLen = 14;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换本地报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//成功返回转化后的字节数，失败返回-1
	lRet = lPos - (*inout_plOutMsgPos);
	*inout_plOutMsgPos = lPos;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_RspQrySpotLocalOrder
* 功能描述：OutAPI-将现货查询响应回调API报文转化为平台报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- inout_plOutMsgPos         输入输出 --保存在outMsg报文当前保存位置
*- inout_ppszOutFileName         输入输出 --存文件的文件名
*- out_pblFollowupFlag         输出 --是否有后续报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：ukyo  于 20071014创建
************************************************************/

long CGTAMsgConvert::rl_RspQrySpotLocalOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag)
{
	long	lRet = -1;
	TTRspMsg * pstTRspMsg;
	TSpotOrder *pstUsrData;
	char szTmp[512];
	long lPos = *inout_plOutMsgPos, lLen = 0;
	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	//API回调返回结构是按TTRspMsg、TReqQryLocalOrder顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TSpotOrder*)((char*)in_pszInMsg + sizeof(TTRspMsg));
	//缺省为没有后续报文
	*out_pblFollowupFlag = false;
	//1、将API回调返回的结构TTRspMsg和TReqQryLocalOrder转化为平台下传的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//    (2)inout_plOutMsgPos为保存转化后内容的开始配置，转化完成后需要修改位置为当前写入内容的最位置后一字节
	//    以下项只在特定的函数中才有，如查询之类
	//    (3)out_pblFollowupFlag当有后续报文时，需要设置为true，否则设置为false
	//    (4)如果inout_plOutMsgPos不是0表示当前为后续报文，前面的报文存放在out_pszOutMsg中，转化后需要重新修改记录数

	// 		//1.1转换交易代码
	// 	memset(szTmp,  32, sizeof(szTmp));
	// 	memcpy(szTmp, in_pszTrCode, strlen(in_pszTrCode));
	// 	lLen = 10;
	// 	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	// 	lPos += lLen;
	// 	if (lPos > in_lOutMsgSize)
	// 	{
	// 		sprintf(m_szErrMsg, "转换交易代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
	// 		return -1;
	// 	}
	//
	//
	// 	{
	// 	}

	//1.2转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//1.3转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}
	//2.1转换保单号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->orderNo, strlen(pstUsrData->orderNo));
	lLen =16;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换保单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.2转换合约代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}
	//2.3转换客户代码
	memset(szTmp, 0, sizeof(szTmp));
	memcpy(szTmp,pstUsrData->clientID,strlen(pstUsrData->clientID));
	lLen = 12;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换客户代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.4转换会员代码
	memset(szTmp, 0, sizeof(szTmp));
	memcpy(szTmp,pstUsrData->memberID,strlen(pstUsrData->memberID));
	lLen = 6;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换会员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.5取买卖方向
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 1;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取买卖方向错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}


	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "买卖方向为空");
		return -1;
	}
	pstUsrData->buyOrSell = szTmp[0];

	//2.6转换申报日期
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->applyDate, strlen(pstUsrData->applyDate));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换申报日期错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	


	//2.7转换申报时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->applyTime, strlen(pstUsrData->applyTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换申报时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//



	//2.8转换交易员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->traderID, strlen(pstUsrData->traderID));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//



	//2.9转换价格
	memset(szTmp, 32, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->price, 2);
	sprintf(szTmp, "%-17f", pstUsrData->price);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换价格错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}
	//2.10转换数量
	memset(szTmp, 32, sizeof(szTmp));
	sprintf(szTmp, "%-10d", pstUsrData->amount);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}
	//2.11取剩余数量
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 10;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取剩余数量错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}


	UStringHelper::Trim(szTmp);
	pstUsrData->remainAmount = atoi(szTmp);

	//2.12转换撤销时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->cancelTime, strlen(pstUsrData->cancelTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换撤销时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.13转换撤消员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->cancelID, strlen(pstUsrData->cancelID));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换撤消员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	

	//2.14转换状态
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->status;
	//	szTmp[0] = '3';
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换状态错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.15转换类型
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->matchType;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换类型错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.16转换转让合约代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->endorseInstID, strlen(pstUsrData->endorseInstID));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换转让合约代码 错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}
	//2.17转换本地报单号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->localOrderNo, strlen(pstUsrData->localOrderNo));
	lLen = 14;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换本地报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//成功返回转化后的字节数，失败返回-1
	lRet = lPos - (*inout_plOutMsgPos);
	*inout_plOutMsgPos = lPos;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_RspQryDeferLocalOrder
* 功能描述：OutAPI-将现货延期交收本地保单号查询响应回调API报文转化为平台报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- inout_plOutMsgPos         输入输出 --保存在outMsg报文当前保存位置
*- inout_ppszOutFileName         输入输出 --存文件的文件名
*- out_pblFollowupFlag         输出 --是否有后续报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：ukyo  于 20071014创建
************************************************************/

long CGTAMsgConvert::rl_RspQryDeferLocalOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag)
{
	long	lRet = -1;
	TTRspMsg * pstTRspMsg;
	TDeferOrder *pstUsrData;
	char szTmp[512];
	long lPos = *inout_plOutMsgPos, lLen = 0;
	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));

	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	//API回调返回结构是按TTRspMsg、TReqQryLocalOrder顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TDeferOrder*)((char*)in_pszInMsg + sizeof(TTRspMsg));
	//缺省为没有后续报文
	*out_pblFollowupFlag = false;
	//1、将API回调返回的结构TTRspMsg和TReqQryLocalOrder转化为平台下传的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//    (2)inout_plOutMsgPos为保存转化后内容的开始配置，转化完成后需要修改位置为当前写入内容的最位置后一字节
	//    以下项只在特定的函数中才有，如查询之类
	//    (3)out_pblFollowupFlag当有后续报文时，需要设置为true，否则设置为false
	//    (4)如果inout_plOutMsgPos不是0表示当前为后续报文，前面的报文存放在out_pszOutMsg中，转化后需要重新修改记录数

	// 1.1转换交易代码
	// 	memset(szTmp,  32, sizeof(szTmp));
	// 	memcpy(szTmp, in_pszTrCode, strlen(in_pszTrCode));
	// 	lLen = 10;
	// 	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	// 	lPos += lLen;
	// 	if (lPos > in_lOutMsgSize)
	// 	{
	// 		sprintf(m_szErrMsg, "转换交易代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
	// 		return -1;
	// 	}
	//
	//
	// 	{
	// 	}

	//1.2转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//1.3转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//


	//2.1转换报单号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->orderNo, strlen(pstUsrData->orderNo));
	lLen = 16;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.2转换客户代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->clientID, strlen(pstUsrData->clientID));
	lLen = 12;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换客户代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.3转换合约代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.4转换会员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->memberID, strlen(pstUsrData->memberID));
	lLen = 6;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换会员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}
	//2.5转换买卖方向
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->buyOrSell;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买卖方向错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}
	//2.6转换申报日期
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->applyDate, strlen(pstUsrData->applyDate));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换申报日期错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}
	//2.7转换申报时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->applyTime, strlen(pstUsrData->applyTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换申报时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.8转换开平仓标志
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->offSetFlag;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换开平仓标志错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.9转换价格
	memset(szTmp, 32, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->price, 2);
	sprintf(szTmp, "%-17f", pstUsrData->price);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换价格错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.10转换数量
	memset(szTmp, 32, sizeof(szTmp));
	sprintf(szTmp, "%-10d", pstUsrData->amount);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}
	//2.11转换剩余数量
	memset(szTmp, 32, sizeof(szTmp));
	sprintf(szTmp, "%-10d", pstUsrData->remainAmount);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换剩余数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}
	//2.12转换撤销时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->cancelTime, strlen(pstUsrData->cancelTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换撤销时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//



	//2.13转换状态
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->status;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换状态错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//



	//2.14转换本地报单号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->localOrderNo, strlen(pstUsrData->localOrderNo));
	lLen = 14;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换本地报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//



	//2.15转换保证金率
	memset(szTmp, 32, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->margin, 2);
	sprintf(szTmp, "%-17f", pstUsrData->margin);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换保证金率错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//



	//2.16转换保证金类型
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->marginType;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换保证金类型错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//



	//2.17转换交易员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->traderID, strlen(pstUsrData->traderID));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//



	//2.18转换撤消员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->cancelID, strlen(pstUsrData->cancelID));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换撤消员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//



	//2.19转换类型
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->matchType;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换类型错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//成功返回转化后的字节数，失败返回-1
	lRet = lPos - (*inout_plOutMsgPos);
	*inout_plOutMsgPos = lPos;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_RspQryForwardLocalOrder
* 功能描述：OutAPI-将现货T+N查询响应回调API报文转化为平台报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- inout_plOutMsgPos         输入输出 --保存在outMsg报文当前保存位置
*- inout_ppszOutFileName         输入输出 --存文件的文件名
*- out_pblFollowupFlag         输出 --是否有后续报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：ukyo  于 20071014创建
************************************************************/

long CGTAMsgConvert::rl_RspQryForwardLocalOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag)
{
	long	lRet = -1;
	TTRspMsg * pstTRspMsg;
	TReqQryLocalOrder *pstUsrData;
	char szTmp[512];
	long lPos = *inout_plOutMsgPos, lLen = 0;
	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));

	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	//API回调返回结构是按TTRspMsg、TReqQryLocalOrder顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TReqQryLocalOrder*)((char*)in_pszInMsg + sizeof(TTRspMsg));
	//缺省为没有后续报文
	*out_pblFollowupFlag = false;
	//1、将API回调返回的结构TTRspMsg和TReqQryLocalOrder转化为平台下传的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//    (2)inout_plOutMsgPos为保存转化后内容的开始配置，转化完成后需要修改位置为当前写入内容的最位置后一字节
	//    以下项只在特定的函数中才有，如查询之类
	//    (3)out_pblFollowupFlag当有后续报文时，需要设置为true，否则设置为false
	//    (4)如果inout_plOutMsgPos不是0表示当前为后续报文，前面的报文存放在out_pszOutMsg中，转化后需要重新修改记录数

	//1.1转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//1.2转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.1转换会员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->memberID, strlen(pstUsrData->memberID));
	lLen = 6;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换会员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.2转换客户代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->clientID, strlen(pstUsrData->clientID));
	lLen = 12;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换客户代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.3转换本地报单号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->localOrderNo, strlen(pstUsrData->localOrderNo));
	lLen = 14;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换本地报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//成功返回转化后的字节数，失败返回-1
	lRet = lPos - (*inout_plOutMsgPos);
	*inout_plOutMsgPos = lPos;
	return lRet;
}


/**********************************************************
* 函 数 名： CTrMsgConvert::rl_RspTrDeferOrder
* 功能描述：OutAPI-将现货延期报单和现货延期报单查询响应回调API报文转化为平台报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- inout_plOutMsgPos         输入输出 --保存在outMsg报文当前保存位置
*- inout_ppszOutFileName         输入输出 --存文件的文件名
*- out_pblFollowupFlag         输出 --是否有后续报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei  于 20091016创建
************************************************************/

long CGTAMsgConvert::rl_RspDeferOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag)
{
	long	lRet = -1;
	TTRspMsg * pstTRspMsg;
	TDeferOrder *pstUsrData;
	char szTmp[512];
	long lPos = *inout_plOutMsgPos, lLen = 0;
	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));

	//输出日志MY_NORMAL，MY_D3

	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	//API回调返回结构是按TTRspMsg、TDeferOrder顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TDeferOrder*)((char*)in_pszInMsg + sizeof(TTRspMsg));
	//缺省为没有后续报文
	*out_pblFollowupFlag = false;
	//1、将API回调返回的结构TTRspMsg和TSpotOrder转化为平台下传的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//    (2)inout_plOutMsgPos为保存转化后内容的开始配置，转化完成后需要修改位置为当前写入内容的最位置后一字节
	//    以下项只在特定的函数中才有，如查询之类
	//    (3)out_pblFollowupFlag当有后续报文时，需要设置为true，否则设置为false
	//    (4)如果inout_plOutMsgPos不是0表示当前为后续报文，前面的报文存放在out_pszOutMsg中，转化后需要重新修改记录数
	//1.1转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//1.2转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.1转换报单号
	memset(szTmp, 32, sizeof(szTmp));

	memcpy(szTmp, pstUsrData->orderNo, strlen(pstUsrData->orderNo));
	lLen = 16;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.2转换客户代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->clientID, strlen(pstUsrData->clientID));
	lLen = 12;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换客户代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.3转换合约代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.4转换会员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->memberID, strlen(pstUsrData->memberID));
	lLen = 6;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换会员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.5转换买卖方向
	lLen = 1;
	out_pszOutMsg[lPos] = pstUsrData->buyOrSell;
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买卖方向错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.6转换申报日期
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->applyDate, strlen(pstUsrData->applyDate));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换申报日期错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.7转换申报时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->applyTime, strlen(pstUsrData->applyTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换申报时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.8转换开平仓标志
	lLen = 1;
	out_pszOutMsg[lPos] = pstUsrData->offSetFlag;
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换开平仓标志错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.9转换价格
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->price, 2);
	sprintf(szTmp, "%-18f", pstUsrData->price);
	lLen = 18;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换价格错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.10转换数量
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->amount);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.11转换剩余数量
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->remainAmount);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换剩余数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.12转换撤销时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->cancelTime, strlen(pstUsrData->cancelTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换撤销时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.13转换状态
	lLen = 1;
	out_pszOutMsg[lPos] = pstUsrData->status;
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换状态错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.14转换本地报单号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->localOrderNo, strlen(pstUsrData->localOrderNo));
	lLen = 14;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换本地报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.15转换保证金率
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->margin, 2);
	sprintf(szTmp, "%-18f", pstUsrData->margin);
	lLen = 18;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换保证金率错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.16转换保证金类型
	lLen = 1;
	out_pszOutMsg[lPos] = pstUsrData->marginType;
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换保证金类型错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.17转换交易员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->traderID, strlen(pstUsrData->traderID));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.18转换撤销员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->cancelID, strlen(pstUsrData->cancelID));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换撤销员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.19转换类型
	lLen = 1;
	out_pszOutMsg[lPos] = pstUsrData->matchType;
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换类型错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	

	//成功返回转化后的字节数，失败返回-1
	lRet = lPos - (*inout_plOutMsgPos);
	*inout_plOutMsgPos = lPos;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_RspMiddleAppOrder
* 功能描述：OutAPI-将现货延期中立仓申报响应回调API报文转化为平台报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- inout_plOutMsgPos         输入输出 --保存在outMsg报文当前保存位置
*- inout_ppszOutFileName         输入输出 --存文件的文件名
*- out_pblFollowupFlag         输出 --是否有后续报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei  于 20091029创建
************************************************************/

long CGTAMsgConvert::rl_RspMiddleAppOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag)
{
	long	lRet = -1;
	TTRspMsg * pstTRspMsg;
	TMiddleAppOrder *pstUsrData;
	char szTmp[512];
	long lPos = *inout_plOutMsgPos, lLen = 0;
	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));

	//输出日志MY_NORMAL，MY_D3

	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	//API回调返回结构是按TTRspMsg、TMiddleAppOrder顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TMiddleAppOrder*)((char*)in_pszInMsg + sizeof(TTRspMsg));
	//缺省为没有后续报文
	*out_pblFollowupFlag = false;
	//1、将API回调返回的结构TTRspMsg和TMiddleAppOrder转化为平台下传的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//    (2)inout_plOutMsgPos为保存转化后内容的开始配置，转化完成后需要修改位置为当前写入内容的最位置后一字节
	//    以下项只在特定的函数中才有，如查询之类
	//    (3)out_pblFollowupFlag当有后续报文时，需要设置为true，否则设置为false
	//    (4)如果inout_plOutMsgPos不是0表示当前为后续报文，前面的报文存放在out_pszOutMsg中，转化后需要重新修改记录数
	//1.1转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//1.2转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.1转换报单号
	memset(szTmp, 32, sizeof(szTmp));

	memcpy(szTmp, pstUsrData->orderNo, strlen(pstUsrData->orderNo));
	lLen = 16;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.2转换客户代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->clientID, strlen(pstUsrData->clientID));
	lLen = 12;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换客户代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.3转换合约代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.4转换会员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->memberID, strlen(pstUsrData->memberID));
	lLen = 6;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换会员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.5转换买卖方向
	lLen = 1;
	out_pszOutMsg[lPos] = pstUsrData->buyOrSell;
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买卖方向错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.6转换申报日期
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->applyDate, strlen(pstUsrData->applyDate));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换申报日期错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.7转换申报时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->applyTime, strlen(pstUsrData->applyTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换申报时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.8转换数量
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->amount);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.9转换撤销时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->cancelTime, strlen(pstUsrData->cancelTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换撤销时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.10转换状态
	lLen = 1;
	out_pszOutMsg[lPos] = pstUsrData->status;
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换状态错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.11转换本地报单号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->localOrderID, strlen(pstUsrData->localOrderID));
	lLen = 14;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换本地报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.12转换交易员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->traderID, strlen(pstUsrData->traderID));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.13转换撤销员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->cancelID, strlen(pstUsrData->cancelID));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换撤销员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.14转换剩余数量
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->remainAmount);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换剩余数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	

	//成功返回转化后的字节数，失败返回-1
	lRet = lPos - (*inout_plOutMsgPos);
	*inout_plOutMsgPos = lPos;
	return lRet;
}



/**********************************************************
* 函 数 名： CTrMsgConvert::rl_RspDeferDeliveryAppOrder
* 功能描述：OutAPI-将现货延期交收交割报单和现货延期交收交割报单查询响应回调API报文转化为平台报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- inout_plOutMsgPos         输入输出 --保存在outMsg报文当前保存位置
*- inout_ppszOutFileName         输入输出 --存文件的文件名
*- out_pblFollowupFlag         输出 --是否有后续报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei  于 20091027创建
************************************************************/

long CGTAMsgConvert::rl_RspDeferDeliveryAppOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag)
{
	long	lRet = -1;
	TTRspMsg * pstTRspMsg;
	TDeferDeliveryAppOrder *pstUsrData;
	char szTmp[512];
	long lPos = *inout_plOutMsgPos, lLen = 0;
	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));

	//输出日志MY_NORMAL，MY_D3

	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	//API回调返回结构是按TTRspMsg、rl_RspDeferDeliveryAppOrder顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TDeferDeliveryAppOrder*)((char*)in_pszInMsg + sizeof(TTRspMsg));
	//缺省为没有后续报文
	*out_pblFollowupFlag = false;
	//1、将API回调返回的结构TTRspMsg和TDeferDeliveryAppOrder转化为平台下传的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//    (2)inout_plOutMsgPos为保存转化后内容的开始配置，转化完成后需要修改位置为当前写入内容的最位置后一字节
	//    以下项只在特定的函数中才有，如查询之类
	//    (3)out_pblFollowupFlag当有后续报文时，需要设置为true，否则设置为false
	//    (4)如果inout_plOutMsgPos不是0表示当前为后续报文，前面的报文存放在out_pszOutMsg中，转化后需要重新修改记录数
	//1.1转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//1.2转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.1转换报单号
	memset(szTmp, 32, sizeof(szTmp));

	memcpy(szTmp, pstUsrData->orderNo, strlen(pstUsrData->orderNo));
	lLen = 16;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.2转换客户代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->clientID, strlen(pstUsrData->clientID));
	lLen = 12;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换客户代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.3转换合约代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.4转换买卖方向
	lLen = 1;
	out_pszOutMsg[lPos] = pstUsrData->buyOrSell;
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买卖方向错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.5转换申报日期
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->applyDate, strlen(pstUsrData->applyDate));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换申报日期错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.6转换申报时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->applyTime, strlen(pstUsrData->applyTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换申报时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.7转换数量
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->amount);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.8转换撤销时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->cancelTime, strlen(pstUsrData->cancelTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换撤销时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.9转换状态
	lLen = 1;
	out_pszOutMsg[lPos] = pstUsrData->status;
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换状态错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.10转换本地报单号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->localOrderID, strlen(pstUsrData->localOrderID));
	lLen = 14;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换本地报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.11转换会员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->memberID, strlen(pstUsrData->memberID));
	lLen = 6;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换会员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.12转换交易员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->traderID, strlen(pstUsrData->traderID));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.13转换撤销员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->cancelID, strlen(pstUsrData->cancelID));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换撤销员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.14转换剩余数量
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->remainAmount);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换剩余数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	

	//成功返回转化后的字节数，失败返回-1
	lRet = lPos - (*inout_plOutMsgPos);
	*inout_plOutMsgPos = lPos;
	return lRet;
}


/**********************************************************
* 函 数 名： CTrMsgConvert::rl_ReqSpotOrderCancel
* 功能描述：InAPI-将平台现货撤单请求报文转化为调用API输入报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --输出空间的大小
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：ukyo  于 20071014创建
************************************************************/

long CGTAMsgConvert::rl_ReqSpotOrderCancel(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize)
{
	long	lRet = -1, lCount = 0;
	long lPos = 0, lLen = 0;
	TSpotOrderCancel *pstUsrData;
	char 	szTmp[512];
	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3

	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "传入输出报文指针为空");
		return -1;
	}
	if (sizeof(TSpotOrderCancel) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "转化后的报文存放空间不足");
		return -1;
	}
	//1、将平台上传的结构转化为API所需要的结构TSpotOrderCancel
	pstUsrData = (TSpotOrderCancel*)out_pszOutMsg;

	//1.1取报单号
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 16;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取报单号错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "报单号为空");
		return -1;
	}
	pstUsrData->orderNo = szTmp;
	lCount += strlen(szTmp);

	//1.2取合约代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 8;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取合约代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "合约代码为空");
		return -1;
	}
	pstUsrData->instID = szTmp;
	lCount += strlen(szTmp);

	//1.3取客户代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 12;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取客户代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "客户代码为空");
		return -1;
	}
	pstUsrData->clientID = szTmp;
	lCount += strlen(szTmp);

	//1.4取会员代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 6;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取会员代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "会员代码为空");
		return -1;
	}
	pstUsrData->memberID = szTmp;
	lCount += strlen(szTmp);

	//1.5取交易员代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 10;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取交易员代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "交易员代码为空");
		return -1;
	}
	pstUsrData->traderID = szTmp;
	lCount += strlen(szTmp);


	


	//成功返回转化后的字节数，失败返回-1
	lRet = sizeof(TSpotOrderCancel);
	return lRet;
}


/**********************************************************
* 函 数 名： CTrMsgConvert::rl_ReqDeferOrderCancel
* 功能描述：InAPI-将平台现货延期交收撤单请求报文转化为调用API输入报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --输出空间的大小
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei  于 20091027创建
************************************************************/

long CGTAMsgConvert::rl_ReqDeferOrderCancel(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize)
{
	long	lRet = -1, lCount = 0;
	long lPos = 0, lLen = 0;
	TDeferOrderCancel *pstUsrData;
	char 	szTmp[512];
	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3

	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "传入输出报文指针为空");
		return -1;
	}
	if (sizeof(TDeferOrderCancel) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "转化后的报文存放空间不足");
		return -1;
	}
	//1、将平台上传的结构转化为API所需要的结构TDeferOrderCancel
	pstUsrData = (TDeferOrderCancel*)out_pszOutMsg;

	//1.1取报单号
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 16;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取报单号错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "报单号为空");
		return -1;
	}
	pstUsrData->orderNo = szTmp;
	lCount += strlen(szTmp);

	//1.2取合约代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 8;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取合约代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "合约代码为空");
		return -1;
	}
	pstUsrData->instID = szTmp;
	lCount += strlen(szTmp);

	//1.3取客户代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 12;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取客户代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "客户代码为空");
		return -1;
	}
	pstUsrData->clientID = szTmp;
	lCount += strlen(szTmp);

	//1.4取会员代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 6;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取会员代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "会员代码为空");
		return -1;
	}
	pstUsrData->memberID = szTmp;
	lCount += strlen(szTmp);

	//1.5取交易员代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 10;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取交易员代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "交易员代码为空");
		return -1;
	}
	pstUsrData->traderID = szTmp;
	lCount += strlen(szTmp);


	


	//成功返回转化后的字节数，失败返回-1
	lRet = sizeof(TDeferOrderCancel);
	return lRet;
}



/**********************************************************
* 函 数 名： CTrMsgConvert::rl_RspSpotOrderCancel
* 功能描述：OutAPI-将现货撤单响应回调API报文转化为平台报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- inout_plOutMsgPos         输入输出 --保存在outMsg报文当前保存位置
*- inout_ppszOutFileName         输入输出 --存文件的文件名
*- out_pblFollowupFlag         输出 --是否有后续报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：ukyo  于 20071014创建
************************************************************/

long CGTAMsgConvert::rl_RspSpotOrderCancel(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag)
{
	long	lRet = -1;
	TTRspMsg * pstTRspMsg;
	TSpotOrderCancel *pstUsrData;
	char szTmp[512];
	long lPos = *inout_plOutMsgPos, lLen = 0;

	//输出日志MY_NORMAL，MY_D3

	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));

	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}

	//API回调返回结构是按TTRspMsg、TSpotOrderCancel顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TSpotOrderCancel*)((char*)in_pszInMsg + sizeof(TTRspMsg));

	//缺省为没有后续报文
	*out_pblFollowupFlag = false;

	//1、将API回调返回的结构TTRspMsg和TSpotOrderCancel转化为平台下传的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	// (2)inout_plOutMsgPos为保存转化后内容的开始配置，转化完成后需要修改位置为当前写入内容的最位置后一字节
	//    以下项只在特定的函数中才有，如查询之类
	// (3)out_pblFollowupFlag当有后续报文时，需要设置为true，否则设置为false
	// (4)如果inout_plOutMsgPos不是0表示当前为后续报文，前面的报文存放在out_pszOutMsg中，转化后需要重新修改记录数


	//1.1转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//1.2转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.1转换报单号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->orderNo, strlen(pstUsrData->orderNo));
	lLen = 16;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.2转换合约代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.3转换客户代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->clientID, strlen(pstUsrData->clientID));
	lLen = 12;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换客户代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.4转换会员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->memberID, strlen(pstUsrData->memberID));
	lLen = 6;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换会员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.5转换交易员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->traderID, strlen(pstUsrData->traderID));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	

	//成功返回转化后的字节数，失败返回-1
	lRet = lPos - (*inout_plOutMsgPos);
	*inout_plOutMsgPos = lPos;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_RspDeferOrderCancel
* 功能描述：OutAPI-将现货延期交收撤单响应回调API报文转化为平台报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- inout_plOutMsgPos         输入输出 --保存在outMsg报文当前保存位置
*- inout_ppszOutFileName         输入输出 --存文件的文件名
*- out_pblFollowupFlag         输出 --是否有后续报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei  于 20091027创建
************************************************************/

long CGTAMsgConvert::rl_RspDeferOrderCancel(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag)
{
	long	lRet = -1;
	TTRspMsg * pstTRspMsg;
	TDeferOrderCancel *pstUsrData;
	char szTmp[512];
	long lPos = *inout_plOutMsgPos, lLen = 0;

	//输出日志MY_NORMAL，MY_D3

	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));

	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}

	//API回调返回结构是按TTRspMsg、TDeferOrderCancel顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TDeferOrderCancel*)((char*)in_pszInMsg + sizeof(TTRspMsg));

	//缺省为没有后续报文
	*out_pblFollowupFlag = false;

	//1、将API回调返回的结构TTRspMsg和TDeferOrderCancel转化为平台下传的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	// (2)inout_plOutMsgPos为保存转化后内容的开始配置，转化完成后需要修改位置为当前写入内容的最位置后一字节
	//    以下项只在特定的函数中才有，如查询之类
	// (3)out_pblFollowupFlag当有后续报文时，需要设置为true，否则设置为false
	// (4)如果inout_plOutMsgPos不是0表示当前为后续报文，前面的报文存放在out_pszOutMsg中，转化后需要重新修改记录数


	//1.1转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//1.2转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.1转换报单号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->orderNo, strlen(pstUsrData->orderNo));
	lLen = 16;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.2转换合约代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.3转换客户代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->clientID, strlen(pstUsrData->clientID));
	lLen = 12;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换客户代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.4转换会员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->memberID, strlen(pstUsrData->memberID));
	lLen = 6;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换会员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.5转换交易员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->traderID, strlen(pstUsrData->traderID));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	

	//成功返回转化后的字节数，失败返回-1
	lRet = lPos - (*inout_plOutMsgPos);
	*inout_plOutMsgPos = lPos;
	return lRet;
}


/**********************************************************
* 函 数 名： CTrMsgConvert::rl_RspDeferOrderCancel
* 功能描述：OutAPI-将现货延期中立仓申报撤单响应回调API报文转化为平台报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- inout_plOutMsgPos         输入输出 --保存在outMsg报文当前保存位置
*- inout_ppszOutFileName         输入输出 --存文件的文件名
*- out_pblFollowupFlag         输出 --是否有后续报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei  于 20091029创建
************************************************************/

long CGTAMsgConvert::rl_RspMiddleAppOrderCancel(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag)
{
	long	lRet = -1;
	TTRspMsg * pstTRspMsg;
	TMiddleAppOrderCancel *pstUsrData;
	char szTmp[512];
	long lPos = *inout_plOutMsgPos, lLen = 0;

	//输出日志MY_NORMAL，MY_D3

	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));

	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}

	//API回调返回结构是按TTRspMsg、TMiddleAppOrderCancel顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TMiddleAppOrderCancel*)((char*)in_pszInMsg + sizeof(TTRspMsg));

	//缺省为没有后续报文
	*out_pblFollowupFlag = false;

	//1、将API回调返回的结构TTRspMsg和TMiddleAppOrderCancel转化为平台下传的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	// (2)inout_plOutMsgPos为保存转化后内容的开始配置，转化完成后需要修改位置为当前写入内容的最位置后一字节
	//    以下项只在特定的函数中才有，如查询之类
	// (3)out_pblFollowupFlag当有后续报文时，需要设置为true，否则设置为false
	// (4)如果inout_plOutMsgPos不是0表示当前为后续报文，前面的报文存放在out_pszOutMsg中，转化后需要重新修改记录数


	//1.1转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//1.2转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.1转换报单号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->orderNo, strlen(pstUsrData->orderNo));
	lLen = 16;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.2转换合约代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.3转换客户代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->clientID, strlen(pstUsrData->clientID));
	lLen = 12;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换客户代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.4转换会员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->memberID, strlen(pstUsrData->memberID));
	lLen = 6;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换会员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.5转换交易员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->traderID, strlen(pstUsrData->traderID));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	

	//成功返回转化后的字节数，失败返回-1
	lRet = lPos - (*inout_plOutMsgPos);
	*inout_plOutMsgPos = lPos;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_RspDeferDeliveryAppOrderCancel
* 功能描述：OutAPI-将现货延期交收交割申报撤单响应回调API报文转化为平台报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- inout_plOutMsgPos         输入输出 --保存在outMsg报文当前保存位置
*- inout_ppszOutFileName         输入输出 --存文件的文件名
*- out_pblFollowupFlag         输出 --是否有后续报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei  于 20091029创建
************************************************************/

long CGTAMsgConvert::rl_RspDeferDeliveryAppOrderCancel(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag)
{
	long	lRet = -1;
	TTRspMsg * pstTRspMsg;
	TDeferDeliveryAppOrderCancel *pstUsrData;
	char szTmp[512];
	long lPos = *inout_plOutMsgPos, lLen = 0;

	//输出日志MY_NORMAL，MY_D3

	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));

	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}

	//API回调返回结构是按TTRspMsg、TDeferDeliveryAppOrderCancel顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TDeferDeliveryAppOrderCancel*)((char*)in_pszInMsg + sizeof(TTRspMsg));

	//缺省为没有后续报文
	*out_pblFollowupFlag = false;

	//1、将API回调返回的结构TTRspMsg和TDeferDeliveryAppOrderCancel转化为平台下传的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	// (2)inout_plOutMsgPos为保存转化后内容的开始配置，转化完成后需要修改位置为当前写入内容的最位置后一字节
	//    以下项只在特定的函数中才有，如查询之类
	// (3)out_pblFollowupFlag当有后续报文时，需要设置为true，否则设置为false
	// (4)如果inout_plOutMsgPos不是0表示当前为后续报文，前面的报文存放在out_pszOutMsg中，转化后需要重新修改记录数


	//1.1转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//1.2转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.1转换报单号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->orderNo, strlen(pstUsrData->orderNo));
	lLen = 16;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.2转换合约代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.3转换客户代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->clientID, strlen(pstUsrData->clientID));
	lLen = 12;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换客户代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.4转换会员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->memberID, strlen(pstUsrData->memberID));
	lLen = 6;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换会员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.5转换交易员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->traderID, strlen(pstUsrData->traderID));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	

	//成功返回转化后的字节数，失败返回-1
	lRet = lPos - (*inout_plOutMsgPos);
	*inout_plOutMsgPos = lPos;
	return lRet;
}


/**********************************************************
* 函 数 名： CTrMsgConvert::rl_ReqQrySpotOrder
* 功能描述：InAPI-将平台现货报单查询报文转化为调用API输入报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --输出空间的大小
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：ukyo  于 20071014创建
************************************************************/

long CGTAMsgConvert::rl_ReqQrySpotOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize)
{
	long	lRet = -1, lCount = 0;
	// 定义下一个开始取值的位置 和 下一个要取值的长度
	long lPos = 0, lLen = 0;
	// 交易员登录请求信息
	TReqQryOrder *pstUsrData;
	char 	szTmp[512];


	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));

	//输出日志MY_NORMAL，MY_D3

	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (sizeof(TReqQryOrder) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "转化后的报文存放空间不足");
		return -1;
	}

	//将平台上传的结构转化为API所需要的结构TReqQryOrder
	pstUsrData = (TReqQryOrder*)out_pszOutMsg;


	//1取合约代码

	memset(szTmp, 0, sizeof(szTmp));
	lLen = 8;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取合约代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->instID = szTmp;
	lCount += strlen(szTmp);

	//2取市场代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 2;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取市场代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "市场代码为空");
		return -1;
	}
	pstUsrData->marketID = szTmp;
	lCount += strlen(szTmp);

	//3取报单号
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 16;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取报单号错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->orderNo = szTmp;
	lCount += strlen(szTmp);

	//4取客户代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 12;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取客户代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->clientID = szTmp;
	lCount += strlen(szTmp);

	//5取会员代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 6;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取会员代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->memberID = szTmp;
	lCount += strlen(szTmp);

	//5取开平标志
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 1;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取开平标志错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->offsetFlag = szTmp[0];
	lCount += strlen(szTmp);

	


	//成功返回转化后的字节数，失败返回-1
	lRet = sizeof(TReqQryOrder);
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_ReqQryDeferOrder
* 功能描述：InAPI-将平台现货延期交收报单查询请求报文转化为调用API输入报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --输出空间的大小
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：ukyo  于 20071014创建
************************************************************/

long CGTAMsgConvert::rl_ReqQryDeferOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize)
{
	long	lRet = -1, lCount = 0;
	// 定义下一个开始取值的位置 和 下一个要取值的长度
	long lPos = 0, lLen = 0;
	// 交易员登录请求信息
	TReqQryOrder *pstUsrData;
	char 	szTmp[512];


	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));

	//输出日志MY_NORMAL，MY_D3

	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (sizeof(TReqQryOrder) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "转化后的报文存放空间不足");
		return -1;
	}

	//将平台上传的结构转化为API所需要的结构TReqQryOrder
	pstUsrData = (TReqQryOrder*)out_pszOutMsg;


	//1取合约代码

	memset(szTmp, 0, sizeof(szTmp));
	lLen = 8;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取合约代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->instID = szTmp;
	lCount += strlen(szTmp);

	//2取市场代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 2;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取市场代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "市场代码为空");
		return -1;
	}
	pstUsrData->marketID = szTmp;
	lCount += strlen(szTmp);

	//3取报单号
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 16;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取报单号错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->orderNo = szTmp;
	lCount += strlen(szTmp);

	//4取客户代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 12;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取客户代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->clientID = szTmp;
	lCount += strlen(szTmp);

	//5取会员代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 6;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取会员代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->memberID = szTmp;
	lCount += strlen(szTmp);

	//5取开平标志
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 1;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取开平标志错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->offsetFlag = szTmp[0];
	lCount += strlen(szTmp);

	


	//成功返回转化后的字节数，失败返回-1
	lRet = sizeof(TReqQryOrder);
	return lRet;
}


/**********************************************************
* 函 数 名： CTrMsgConvert::rl_ReqQrySpotLocalOrder
* 功能描述：InAPI-将平台现货本地保单号查询请求报文转化为调用API输入报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --输出空间的大小
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：ukyo  于 20071014创建
************************************************************/

long CGTAMsgConvert::rl_ReqQrySpotLocalOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize)
{
	long	lRet = -1, lCount = 0;
	// 定义下一个开始取值的位置 和 下一个要取值的长度
	long lPos = 0, lLen = 0;
	// 交易员登录请求信息
	TReqQryLocalOrder *pstUsrData;
	char 	szTmp[512];


	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));

	//输出日志MY_NORMAL，MY_D3

	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (sizeof(TReqQryLocalOrder) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "转化后的报文存放空间不足");
		return -1;
	}

	//将平台上传的结构转化为API所需要的结构TReqQryOrder
	pstUsrData = (TReqQryLocalOrder*)out_pszOutMsg;


	//1会员代码

	memset(szTmp, 0, sizeof(szTmp));
	lLen = 6;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取会员代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->memberID = szTmp;
	lCount += strlen(szTmp);

	//2取客户代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 12;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取客户代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "客户代码为空");
		return -1;
	}
	pstUsrData->clientID= szTmp;
	lCount += strlen(szTmp);

	//3取本地报单号
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 14;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取本地报单号错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->localOrderNo = szTmp;
	lCount += strlen(szTmp);

	

	//成功返回转化后的字节数，失败返回-1
	lRet = sizeof(TReqQryLocalOrder);
	return lRet;
}


/**********************************************************
* 函 数 名： CTrMsgConvert::rl_ReqQryDeferLocalOrder
* 功能描述：InAPI-将平台现货延期交收本地保单号查询请求报文转化为调用API输入报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --输出空间的大小
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：ukyo  于 20071014创建
************************************************************/

long CGTAMsgConvert::rl_ReqQryDeferLocalOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize)
{
	long	lRet = -1, lCount = 0;
	// 定义下一个开始取值的位置 和 下一个要取值的长度
	long lPos = 0, lLen = 0;
	// 交易员登录请求信息
	TReqQryLocalOrder *pstUsrData;
	char 	szTmp[512];


	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));

	//输出日志MY_NORMAL，MY_D3

	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (sizeof(TReqQryLocalOrder) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "转化后的报文存放空间不足");
		return -1;
	}

	//将平台上传的结构转化为API所需要的结构TReqQryOrder
	pstUsrData = (TReqQryLocalOrder*)out_pszOutMsg;


	//1会员代码

	memset(szTmp, 0, sizeof(szTmp));
	lLen = 6;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取会员代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->memberID = szTmp;
	lCount += strlen(szTmp);

	//2取客户代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 12;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取客户代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "客户代码为空");
		return -1;
	}
	pstUsrData->clientID= szTmp;
	lCount += strlen(szTmp);

	//3取本地报单号
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 14;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取本地报单号错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->localOrderNo = szTmp;
	lCount += strlen(szTmp);

	

	//成功返回转化后的字节数，失败返回-1
	lRet = sizeof(TReqQryLocalOrder);
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_ReqQryForwardLocalOrder
* 功能描述：InAPI-将平台现货延期交收本地保单号查询请求报文转化为调用API输入报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --输出空间的大小
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：ukyo  于 20071014创建
************************************************************/

long CGTAMsgConvert::rl_ReqQryForwardLocalOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize)
{
	long	lRet = -1, lCount = 0;
	// 定义下一个开始取值的位置 和 下一个要取值的长度
	long lPos = 0, lLen = 0;
	// 交易员登录请求信息
	TReqQryLocalOrder *pstUsrData;
	char 	szTmp[512];


	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));

	//输出日志MY_NORMAL，MY_D3

	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (sizeof(TReqQryLocalOrder) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "转化后的报文存放空间不足");
		return -1;
	}

	//将平台上传的结构转化为API所需要的结构TReqQryOrder
	pstUsrData = (TReqQryLocalOrder*)out_pszOutMsg;


	//1会员代码

	memset(szTmp, 0, sizeof(szTmp));
	lLen = 6;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取会员代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->memberID = szTmp;
	lCount += strlen(szTmp);

	//2取客户代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 12;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取客户代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "客户代码为空");
		return -1;
	}
	pstUsrData->clientID= szTmp;
	lCount += strlen(szTmp);

	//3取本地报单号
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 14;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取本地报单号错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->localOrderNo = szTmp;
	lCount += strlen(szTmp);

	

	//成功返回转化后的字节数，失败返回-1
	lRet = sizeof(TReqQryLocalOrder);
	return lRet;
}


/**********************************************************
* 函 数 名： CTrMsgConvert::rl_ReqQrySpotMatch
* 功能描述：InAPI-将平台现货成交单查询报文转化为调用API输入报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --输出空间的大小
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：ukyo  于 20071014创建
************************************************************/

long CGTAMsgConvert::rl_ReqQrySpotMatch(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize)
{
	// 定义返回值
	long	lRet = -1, lCount = 0;
	// 定义下一个开始取值的位置 和 下一个要取值的长度
	long lPos = 0, lLen = 0;
	// 交易员登录请求信息
	TReqQryMatch *pstUsrData;
	char 	szTmp[512];


	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3


	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (sizeof(TReqQryMatch) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "转化后的报文存放空间不足");
		return -1;
	}

	//将平台上传的结构转化为API所需要的结构TReqQryMatch
	pstUsrData = (TReqQryMatch*)out_pszOutMsg;


	//1取合约代码
	//rl_CutMsgReq(in_pszInMsg, in_lInMsgLength,&pstUsrData->traderID,"交易员代码", 10,lPos, 1, 1, 0);
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 8;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取合约代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	pstUsrData->instID = szTmp;
	lCount += strlen(szTmp);

	//2取市场代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 2;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取市场代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "市场代码为空");
		return -1;
	}
	pstUsrData->marketID = szTmp;
	lCount += strlen(szTmp);

	//3取报单号
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 16;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取报单号错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);

	pstUsrData->orderNo = szTmp;
	lCount += strlen(szTmp);

	//4取客户代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 12;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取客户代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->clientID = szTmp;
	lCount += strlen(szTmp);

	//6取会员代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 6;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取会员代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->memberID = szTmp;
	lCount += strlen(szTmp);

	//7取成交编号
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 16;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取成交编号错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->matchNo = szTmp;
	lCount += sizeof(szTmp);
	


	//成功返回转化后的字节数，失败返回-1
	lRet = sizeof(TReqQryMatch);
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_ReqQryDeferMatch
* 功能描述：InAPI-将平台现货延期交收成交单查询请求报文转化为调用API输入报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --输出空间的大小
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei于 20091109创建
************************************************************/

long CGTAMsgConvert::rl_ReqQryDeferMatch(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize)
{
	// 定义返回值
	long	lRet = -1, lCount = 0;
	// 定义下一个开始取值的位置 和 下一个要取值的长度
	long lPos = 0, lLen = 0;
	// 交易员登录请求信息
	TReqQryMatch *pstUsrData;
	char 	szTmp[512];


	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3


	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (sizeof(TReqQryMatch) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "转化后的报文存放空间不足");
		return -1;
	}

	//将平台上传的结构转化为API所需要的结构TReqQryMatch
	pstUsrData = (TReqQryMatch*)out_pszOutMsg;


	//1取合约代码
	//rl_CutMsgReq(in_pszInMsg, in_lInMsgLength,&pstUsrData->traderID,"交易员代码", 10,lPos, 1, 1, 0);
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 8;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取合约代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	pstUsrData->instID = szTmp;
	lCount += strlen(szTmp);

	//2取市场代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 2;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取市场代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "市场代码为空");
		return -1;
	}
	pstUsrData->marketID = szTmp;
	lCount += strlen(szTmp);

	//3取报单号
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 16;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取报单号错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);

	pstUsrData->orderNo = szTmp;
	lCount += strlen(szTmp);

	//4取客户代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 12;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取客户代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->clientID = szTmp;
	lCount += strlen(szTmp);

	//6取会员代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 6;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取会员代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->memberID = szTmp;
	lCount += strlen(szTmp);

	//7取成交编号
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 16;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取成交编号错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->matchNo = szTmp;
	lCount += sizeof(szTmp);
	


	//成功返回转化后的字节数，失败返回-1
	lRet = sizeof(TReqQryMatch);
	return lRet;
}


/**********************************************************
* 函 数 名： CTrMsgConvert::rl_ReqQryDeferClientPosi
* 功能描述：InAPI-将平台客户现货延期交收持仓查询报文转化为调用API输入报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --输出空间的大小
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei于 20091104创建
************************************************************/

long CGTAMsgConvert::rl_ReqQryDeferClientPosi(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize)
{
	// 定义返回值
	long	lRet = -1, lCount = 0;
	// 定义下一个开始取值的位置 和 下一个要取值的长度
	long lPos = 0, lLen = 0;
	// 交易员登录请求信息
	TReqQryClientPosi *pstUsrData;
	char 	szTmp[512];


	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3


	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (sizeof(TReqQryClientPosi) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "转化后的报文存放空间不足");
		return -1;
	}

	//将平台上传的结构转化为API所需要的结构TReqQryClientPosi
	pstUsrData = (TReqQryClientPosi*)out_pszOutMsg;


	//1取客户代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 12;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取客户代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->clientID = szTmp;
	lCount += strlen(szTmp);

	//2取会员代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 6;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取会员代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->memberID = szTmp;
	lCount += strlen(szTmp);

	//3取市场代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 2;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取市场代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "市场代码为空");
		return -1;
	}
	pstUsrData->marketID = szTmp;
	lCount += strlen(szTmp);

	//4取合约代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 8;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取合约代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	pstUsrData->instID = szTmp;
	lCount += strlen(szTmp);


	//成功返回转化后的字节数，失败返回-1
	lRet = sizeof(TReqQryClientPosi);
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_ReqQryDeferMemberPosi
* 功能描述：InAPI-将平台客户现货延期交收持仓查询报文转化为调用API输入报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --输出空间的大小
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei于 20091111创建
************************************************************/

long CGTAMsgConvert::rl_ReqQryDeferMemberPosi(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize)
{
	// 定义返回值
	long	lRet = -1, lCount = 0;
	// 定义下一个开始取值的位置 和 下一个要取值的长度
	long lPos = 0, lLen = 0;
	// 交易员登录请求信息
	TReqQryMemberPosi *pstUsrData;
	char 	szTmp[512];


	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3


	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (sizeof(TReqQryMemberPosi) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "转化后的报文存放空间不足");
		return -1;
	}

	//将平台上传的结构转化为API所需要的结构TReqQryMemberPosi
	pstUsrData = (TReqQryMemberPosi*)out_pszOutMsg;


	//1取会员代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 6;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取会员代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->memberID = szTmp;
	lCount += strlen(szTmp);

	//2取市场代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 2;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取市场代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "市场代码为空");
		return -1;
	}
	pstUsrData->marketID = szTmp;
	lCount += strlen(szTmp);

	//3取合约代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 8;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取合约代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	pstUsrData->instID = szTmp;
	lCount += strlen(szTmp);


	//成功返回转化后的字节数，失败返回-1
	lRet = sizeof(TReqQryMemberPosi);
	return lRet;
}


/**********************************************************
* 函 数 名： CTrMsgConvert::rl_ReqQryDeferClientPosi
* 功能描述：InAPI-将现货延期交收交割申报成交单查询报文转化为调用API输入报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --输出空间的大小
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei于 20091104创建
************************************************************/

long CGTAMsgConvert::rl_ReqQryDeferDeliveryAppMatch(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize)
{
	// 定义返回值
	long	lRet = -1, lCount = 0;
	// 定义下一个开始取值的位置 和 下一个要取值的长度
	long lPos = 0, lLen = 0;
	// 交易员登录请求信息
	TReqQryMatch *pstUsrData;
	char 	szTmp[512];


	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3


	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (sizeof(TReqQryMatch) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "转化后的报文存放空间不足");
		return -1;
	}

	//将平台上传的结构转化为API所需要的结构TReqQryClientPosi
	pstUsrData = (TReqQryMatch*)out_pszOutMsg;


	//1取合约代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 8;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取合约代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	pstUsrData->instID = szTmp;
	lCount += strlen(szTmp);

	//2取市场代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 2;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取市场代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "市场代码为空");
		return -1;
	}
	pstUsrData->marketID = szTmp;
	lCount += strlen(szTmp);

	//3取报单号
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 16;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取报单号错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);

	pstUsrData->orderNo = szTmp;
	lCount += strlen(szTmp);

	//4取客户代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 12;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取客户代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->clientID = szTmp;
	lCount += strlen(szTmp);

	//5取会员代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 6;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取会员代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->memberID = szTmp;
	lCount += strlen(szTmp);

	//6取成交编号
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 16;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取成交编号错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->matchNo = szTmp;
	lCount += sizeof(szTmp);


	//成功返回转化后的字节数，失败返回-1
	lRet = sizeof(TReqQryMatch);
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_ReqQryDeferDeliveryAppOrder
* 功能描述：InAPI-将现货延期交收交割申报报单查询请求报文转化为调用API输入报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --输出空间的大小
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei于 20091111创建
************************************************************/

long CGTAMsgConvert::rl_ReqQryDeferDeliveryAppOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize)
{
	// 定义返回值
	long	lRet = -1, lCount = 0;
	// 定义下一个开始取值的位置 和 下一个要取值的长度
	long lPos = 0, lLen = 0;
	// 交易员登录请求信息
	TReqQryOrder *pstUsrData;
	char 	szTmp[512];


	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3


	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (sizeof(TReqQryOrder) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "转化后的报文存放空间不足");
		return -1;
	}

	//将平台上传的结构转化为API所需要的结构TReqQryOrder
	pstUsrData = (TReqQryOrder*)out_pszOutMsg;


	//1取合约代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 8;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取合约代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	pstUsrData->instID = szTmp;
	lCount += strlen(szTmp);

	//2取市场代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 2;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取市场代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "市场代码为空");
		return -1;
	}
	pstUsrData->marketID = szTmp;
	lCount += strlen(szTmp);

	//3取报单号
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 16;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取报单号错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);

	pstUsrData->orderNo = szTmp;
	lCount += strlen(szTmp);

	//4取客户代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 12;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取客户代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->clientID = szTmp;
	lCount += strlen(szTmp);

	//5取会员代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 6;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取会员代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->memberID = szTmp;
	lCount += strlen(szTmp);

	//6取开平标志
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 1;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取开平标志错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->offsetFlag = szTmp[0];
	lCount += strlen(szTmp);


	//成功返回转化后的字节数，失败返回-1
	lRet = sizeof(TReqQryOrder);
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_ReqQryDeferMinsQuotation
* 功能描述：InAPI-将现货延期交收分钟行情查询请求报文转化为调用API输入报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --输出空间的大小
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei于 20091111创建
************************************************************/

long CGTAMsgConvert::rl_ReqQryDeferMinsQuotation(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize)
{
	// 定义返回值
	long	lRet = -1, lCount = 0;
	// 定义下一个开始取值的位置 和 下一个要取值的长度
	long lPos = 0, lLen = 0;
	// 交易员登录请求信息
	TReqQryMinsQuotation *pstUsrData;
	char 	szTmp[512];


	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3


	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (sizeof(TReqQryMinsQuotation) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "转化后的报文存放空间不足");
		return -1;
	}

	//将平台上传的结构转化为API所需要的结构TReqQryMinsQuotation
	pstUsrData = (TReqQryMinsQuotation*)out_pszOutMsg;


	//1取合约代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 8;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取合约代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	pstUsrData->instID = szTmp;
	lCount += strlen(szTmp);

	//2取市场代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 2;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取市场代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "市场代码为空");
		return -1;
	}
	pstUsrData->marketID = szTmp;
	lCount += strlen(szTmp);

	//3取上一次时间
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 8;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取上一次时间错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->lastTime = szTmp;
	lCount += strlen(szTmp);




	//成功返回转化后的字节数，失败返回-1
	lRet = sizeof(TReqQryMinsQuotation);
	return lRet;
}


/**********************************************************
* 函 数 名： CTrMsgConvert::rl_RspQrySpotMatch
* 功能描述：OutAPI-将现货成交单查询响应回调API报文转化为平台报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- inout_plOutMsgPos         输入输出 --保存在outMsg报文当前保存位置
*- inout_ppszOutFileName         输入输出 --存文件的文件名
*- out_pblFollowupFlag         输出 --是否有后续报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：ukyo  于 20071014创建
************************************************************/

long CGTAMsgConvert::rl_RspQrySpotMatch(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag)
{
	long	lRet = -1;
	TTRspMsg * pstTRspMsg;
	TSpotMatch *pstUsrData;
	char szTmp[512];
	long lPos = *inout_plOutMsgPos, lLen = 0;
	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));

	//输出日志MY_NORMAL，MY_D3

	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}

	//API回调返回结构是按TTRspMsg、TSpotMatch顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TSpotMatch*)((char*)in_pszInMsg + sizeof(TTRspMsg));

	//缺省为没有后续报文
	*out_pblFollowupFlag = false;

	//1、将API回调返回的结构TTRspMsg和TSpotMatch转化为平台下传的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//    (2)inout_plOutMsgPos为保存转化后内容的开始配置，转化完成后需要修改位置为当前写入内容的最位置后一字节
	//    (3)out_pblFollowupFlag当有后续报文时，需要设置为true，否则设置为false
	//    (4)如果inout_plOutMsgPos不是0表示当前为后续报文，前面的报文存放在out_pszOutMsg中，转化后需要重新修改记录数

	//判断是否为第一次记录  lPos为0则是 否则不是
	if (0 == lPos)
	{
		//1.1转换响应代码
		memset(szTmp,  32, sizeof(szTmp));
		memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
		lLen = 20;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//1.2转换响应消息
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
		lLen = 40;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//1.3记录数量
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp, "%-4ld", 1);
		lLen = 4;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "写入记录数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}

	}
	else
	{
		//修改记录数量的值 增加1
		if (strlen((out_pszOutMsg)) < 64)
		{
			sprintf(m_szErrMsg, "修改记录数量错误-报文长度%ld不够", in_lOutMsgSize);
			return -1;
		}
		memset(szTmp, 0, sizeof(szTmp));
		memcpy(szTmp, &out_pszOutMsg[60], 4);
		long lNum = atol(szTmp);
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp, "%-4ld", ++lNum);
		memcpy(&out_pszOutMsg[60], szTmp, 4);

	}
	// CONTINUE_PACKAGE：有后续纪录 LAST_PACKAGE：最后一条记录，记录内容非空 OVER_PACKAGE：最后一条记录，记录内容空
	switch (pstTRspMsg->Flag)
	{
	case OVER_PACKAGE:
		break;
	case CONTINUE_PACKAGE:
		*out_pblFollowupFlag = true;
	case LAST_PACKAGE:
		//2.1转换成交编号
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->matchNo, strlen(pstUsrData->matchNo));
		lLen = 16;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换成交编号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.2转换买卖方向
		lLen = 1;
		out_pszOutMsg[lPos] = pstUsrData->buyOrSell;
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换买卖方向错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.3转换客户代码
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->clientID, strlen(pstUsrData->clientID));
		lLen = 12;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换客户代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}



		//2.4转换客户类型
		lLen = 1;
		out_pszOutMsg[lPos] = pstUsrData->clientType;
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换客户类型错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.5转换会员代码
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->memberID, strlen(pstUsrData->memberID));
		lLen = 6;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换会员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.6转换合约代码
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
		lLen = 8;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.7转换成交日期
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->matchDate, strlen(pstUsrData->matchDate));
		lLen = 8;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换成交日期错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.8转换成交时间
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->matchTime, strlen(pstUsrData->matchTime));
		lLen = 8;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换成交时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.9转换价格
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp, "%-18f", pstUsrData->price);
		lLen = 18;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换价格错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.10转换数量
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-10d",pstUsrData->volume);
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.11转换系统报单号
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->orderNo, strlen(pstUsrData->orderNo));
		lLen = 14;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换系统报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.12转换报单本地编号
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->localOrderNo, strlen(pstUsrData->localOrderNo));
		lLen = 14;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换报单本地编号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.4转换类型
		lLen = 1;
		out_pszOutMsg[lPos] = pstUsrData->matchType;
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换类型错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}

		break;
	default:
		break;
	}

	

	//成功返回转化后的字节数，失败返回-1
	lRet = lPos - (*inout_plOutMsgPos);
	*inout_plOutMsgPos = lPos;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_RspQryDeferMatch
* 功能描述：OutAPI-将现货延期交收成交单查询应答回调API报文转化为平台报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- inout_plOutMsgPos         输入输出 --保存在outMsg报文当前保存位置
*- inout_ppszOutFileName         输入输出 --存文件的文件名
*- out_pblFollowupFlag         输出 --是否有后续报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei于 20091109创建
************************************************************/

long CGTAMsgConvert::rl_RspQryDeferMatch(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag)
{
	long	lRet = -1;
	TTRspMsg * pstTRspMsg;
	TDeferMatch *pstUsrData;
	char szTmp[512];
	long lPos = *inout_plOutMsgPos, lLen = 0;
	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));

	//输出日志MY_NORMAL，MY_D3

	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}

	//API回调返回结构是按TTRspMsg、TDeferMatch顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TDeferMatch*)((char*)in_pszInMsg + sizeof(TTRspMsg));

	//缺省为没有后续报文
	*out_pblFollowupFlag = false;

	//1、将API回调返回的结构TTRspMsg和TDeferMatch转化为平台下传的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//    (2)inout_plOutMsgPos为保存转化后内容的开始配置，转化完成后需要修改位置为当前写入内容的最位置后一字节
	//    (3)out_pblFollowupFlag当有后续报文时，需要设置为true，否则设置为false
	//    (4)如果inout_plOutMsgPos不是0表示当前为后续报文，前面的报文存放在out_pszOutMsg中，转化后需要重新修改记录数

	//判断是否为第一次记录  lPos为0则是 否则不是
	if (0 == lPos)
	{
		//1.1转换响应代码
		memset(szTmp,  32, sizeof(szTmp));
		memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
		lLen = 20;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//1.2转换响应消息
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
		lLen = 40;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//1.3记录数量
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp, "%-4ld", 1);
		lLen = 4;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "写入记录数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}

	}
	else
	{
		//修改记录数量的值 增加1
		if (strlen((out_pszOutMsg)) < 64)
		{
			sprintf(m_szErrMsg, "修改记录数量错误-报文长度%ld不够", in_lOutMsgSize);
			return -1;
		}
		memset(szTmp, 0, sizeof(szTmp));
		memcpy(szTmp, &out_pszOutMsg[60], 4);
		long lNum = atol(szTmp);
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp, "%-4ld", ++lNum);
		memcpy(&out_pszOutMsg[60], szTmp, 4);

	}
	// CONTINUE_PACKAGE：有后续纪录 LAST_PACKAGE：最后一条记录，记录内容非空 OVER_PACKAGE：最后一条记录，记录内容空
	switch (pstTRspMsg->Flag)
	{
	case OVER_PACKAGE:
		break;
	case CONTINUE_PACKAGE:
		*out_pblFollowupFlag = true;
	case LAST_PACKAGE:
		//2.1转换成交编号
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->matchNo, strlen(pstUsrData->matchNo));
		lLen = 16;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换成交编号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.2转换买卖方向
		lLen = 1;
		out_pszOutMsg[lPos] = pstUsrData->buyOrSell;
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换买卖方向错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.3转换客户代码
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->clientID, strlen(pstUsrData->clientID));
		lLen = 12;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换客户代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}



		//2.4转换客户类型
		lLen = 1;
		out_pszOutMsg[lPos] = pstUsrData->clientType;
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换客户类型错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.5转换合约代码
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
		lLen = 8;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}



		//2.6转换会员代码
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->memberID, strlen(pstUsrData->memberID));
		lLen = 6;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换会员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}



		//2.7转换成交日期
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->matchDate, strlen(pstUsrData->matchDate));
		lLen = 8;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换成交日期错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.8转换成交时间
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->matchTime, strlen(pstUsrData->matchTime));
		lLen = 8;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换成交时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.9转换价格
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp, "%-18f", pstUsrData->price);
		lLen = 18;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换价格错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.10转换数量
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-10d",pstUsrData->volume);
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.11转换开平标志
		lLen = 1;
		out_pszOutMsg[lPos] = pstUsrData->offsetFlag;
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换开平标志错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}



		//2.12转换系统报单号
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->orderNo, strlen(pstUsrData->orderNo));
		lLen = 14;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换系统报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.13转换报单本地编号
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->localOrderNo, strlen(pstUsrData->localOrderNo));
		lLen = 14;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换报单本地编号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		break;
	default:
		break;
	}

	

	//成功返回转化后的字节数，失败返回-1
	lRet = lPos - (*inout_plOutMsgPos);
	*inout_plOutMsgPos = lPos;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_RspQryDeferFeeRate
* 功能描述：OutAPI-将递延费率查询请求应答回调API报文转化为平台报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- inout_plOutMsgPos         输入输出 --保存在outMsg报文当前保存位置
*- inout_ppszOutFileName         输入输出 --存文件的文件名
*- out_pblFollowupFlag         输出 --是否有后续报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei于 20091109创建
************************************************************/

long CGTAMsgConvert::rl_RspQryDeferFeeRate(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag)
{
	long	lRet = -1;
	TTRspMsg * pstTRspMsg;
	TDeferFeeRate *pstUsrData;
	char szTmp[512];
	long lPos = *inout_plOutMsgPos, lLen = 0;
	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));

	//输出日志MY_NORMAL，MY_D3

	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}

	//API回调返回结构是按TTRspMsg、TDeferMatch顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TDeferFeeRate*)((char*)in_pszInMsg + sizeof(TTRspMsg));

	//缺省为没有后续报文
	*out_pblFollowupFlag = false;

	//1、将API回调返回的结构TTRspMsg和TDeferFeeRate转化为平台下传的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//    (2)inout_plOutMsgPos为保存转化后内容的开始配置，转化完成后需要修改位置为当前写入内容的最位置后一字节
	//    (3)out_pblFollowupFlag当有后续报文时，需要设置为true，否则设置为false
	//    (4)如果inout_plOutMsgPos不是0表示当前为后续报文，前面的报文存放在out_pszOutMsg中，转化后需要重新修改记录数

	//判断是否为第一次记录  lPos为0则是 否则不是
	if (0 == lPos)
	{
		//1.1转换响应代码
		memset(szTmp,  32, sizeof(szTmp));
		memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
		lLen = 20;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//1.2转换响应消息
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
		lLen = 40;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//1.3记录数量
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp, "%-4ld", 1);
		lLen = 4;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "写入记录数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}

	}
	else
	{
		//修改记录数量的值 增加1
		if (strlen((out_pszOutMsg)) < 64)
		{
			sprintf(m_szErrMsg, "修改记录数量错误-报文长度%ld不够", in_lOutMsgSize);
			return -1;
		}
		memset(szTmp, 0, sizeof(szTmp));
		memcpy(szTmp, &out_pszOutMsg[60], 4);
		long lNum = atol(szTmp);
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp, "%-4ld", ++lNum);
		memcpy(&out_pszOutMsg[60], szTmp, 4);

	}
	// CONTINUE_PACKAGE：有后续纪录 LAST_PACKAGE：最后一条记录，记录内容非空 OVER_PACKAGE：最后一条记录，记录内容空
	switch (pstTRspMsg->Flag)
	{
	case OVER_PACKAGE:
		break;
	case CONTINUE_PACKAGE:
		*out_pblFollowupFlag = true;
	case LAST_PACKAGE:

		//2.1转换日期
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->effectDate, strlen(pstUsrData->effectDate));
		lLen = 8;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换日期错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.2转换合约代码
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
		lLen = 8;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.3转换支付方向
		lLen = 1;
		out_pszOutMsg[lPos] = pstUsrData->payDirection;
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换支付方向错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.4转换递延费率
		memset(szTmp, 0, sizeof(szTmp));
		CGTABufferBuilder::rd_RoundFloat(pstUsrData->feeRate, 5);
		sprintf(szTmp, "%-17f", pstUsrData->feeRate);
		lLen = 17;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换递延费率错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}



		break;
	default:
		break;
	}

	

	//成功返回转化后的字节数，失败返回-1
	lRet = lPos - (*inout_plOutMsgPos);
	*inout_plOutMsgPos = lPos;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_RspQryDeferDeliveryAppMatch
* 功能描述：OutAPI-将现货延期交收交割申报成交单查询响应回调API报文转化为平台报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- inout_plOutMsgPos         输入输出 --保存在outMsg报文当前保存位置
*- inout_ppszOutFileName         输入输出 --存文件的文件名
*- out_pblFollowupFlag         输出 --是否有后续报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei于 20091105创建
************************************************************/

long CGTAMsgConvert::rl_RspQryDeferDeliveryAppMatch(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag)
{
	long	lRet = -1;
	TTRspMsg * pstTRspMsg;
	TDeferDeliveryAppMatch *pstUsrData;
	char szTmp[512];
	long lPos = *inout_plOutMsgPos, lLen = 0;
	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));

	//输出日志MY_NORMAL，MY_D3

	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}

	//API回调返回结构是按TTRspMsg、TSpotMatch顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TDeferDeliveryAppMatch*)((char*)in_pszInMsg + sizeof(TTRspMsg));

	//缺省为没有后续报文
	*out_pblFollowupFlag = false;

	//1、将API回调返回的结构TTRspMsg和TDeferDeliveryAppMatch转化为平台下传的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//    (2)inout_plOutMsgPos为保存转化后内容的开始配置，转化完成后需要修改位置为当前写入内容的最位置后一字节
	//    (3)out_pblFollowupFlag当有后续报文时，需要设置为true，否则设置为false
	//    (4)如果inout_plOutMsgPos不是0表示当前为后续报文，前面的报文存放在out_pszOutMsg中，转化后需要重新修改记录数

	//判断是否为第一次记录  lPos为0则是 否则不是
	if (0 == lPos)
	{
		//1.1转换响应代码
		memset(szTmp,  32, sizeof(szTmp));
		memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
		lLen = 20;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//1.2转换响应消息
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
		lLen = 40;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//1.3记录数量
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp, "%-4ld", 1);
		lLen = 4;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "写入记录数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}

	}
	else
	{
		//修改记录数量的值 增加1
		if (strlen((out_pszOutMsg)) < 64)
		{
			sprintf(m_szErrMsg, "修改记录数量错误-报文长度%ld不够", in_lOutMsgSize);
			return -1;
		}
		memset(szTmp, 0, sizeof(szTmp));
		memcpy(szTmp, &out_pszOutMsg[60], 4);
		long lNum = atol(szTmp);
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp, "%-4ld", ++lNum);
		memcpy(&out_pszOutMsg[60], szTmp, 4);

	}
	// CONTINUE_PACKAGE：有后续纪录 LAST_PACKAGE：最后一条记录，记录内容非空 OVER_PACKAGE：最后一条记录，记录内容空
	switch (pstTRspMsg->Flag)
	{
	case OVER_PACKAGE:
		break;
	case CONTINUE_PACKAGE:
		*out_pblFollowupFlag = true;
	case LAST_PACKAGE:
		//2.1转换成交编号
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->matchNo, strlen(pstUsrData->matchNo));
		lLen = 16;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换成交编号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.2转换客户代码
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->clientID, strlen(pstUsrData->clientID));
		lLen = 12;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换客户代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.3转换合约代码
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
		lLen = 8;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.4转换会员代码
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->memberID, strlen(pstUsrData->memberID));
		lLen = 6;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换会员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}



		//2.5转换买卖方向
		lLen = 1;
		out_pszOutMsg[lPos] = pstUsrData->buyOrSell;
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换买卖方向错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.6转换数量
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-10d",pstUsrData->volume);
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.7转换成交日期
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->matchDate, strlen(pstUsrData->matchDate));
		lLen = 8;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换成交日期错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.8转换成交时间
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->matchTime, strlen(pstUsrData->matchTime));
		lLen = 8;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换成交时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.9转换系统报单号
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->orderNo, strlen(pstUsrData->orderNo));
		lLen = 14;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换系统报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.10转换报单本地编号
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->LocalOrderNo, strlen(pstUsrData->LocalOrderNo));
		lLen = 14;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换报单本地编号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.11转换中立仓标志
		lLen = 1;
		out_pszOutMsg[lPos] = pstUsrData->middleFlag;
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换中立仓标志错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		break;

	default:
		break;
	}

	

	//成功返回转化后的字节数，失败返回-1
	lRet = lPos - (*inout_plOutMsgPos);
	*inout_plOutMsgPos = lPos;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_RspQryDeferDeliveryAppOrder
* 功能描述：OutAPI-将现货延期交收交割申报报单查询应答回调API报文转化为平台报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- inout_plOutMsgPos         输入输出 --保存在outMsg报文当前保存位置
*- inout_ppszOutFileName         输入输出 --存文件的文件名
*- out_pblFollowupFlag         输出 --是否有后续报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei于 20091111创建
************************************************************/

long CGTAMsgConvert::rl_RspQryDeferDeliveryAppOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag)
{
	long	lRet = -1;
	TTRspMsg * pstTRspMsg;
	TDeferDeliveryAppOrder *pstUsrData;
	char szTmp[512];
	long lPos = *inout_plOutMsgPos, lLen = 0;
	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));

	//输出日志MY_NORMAL，MY_D3

	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}

	//API回调返回结构是按TTRspMsg、TDeferDeliveryAppOrder顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TDeferDeliveryAppOrder*)((char*)in_pszInMsg + sizeof(TTRspMsg));

	//缺省为没有后续报文
	*out_pblFollowupFlag = false;

	//1、将API回调返回的结构TTRspMsg和TDeferDeliveryAppOrder转化为平台下传的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//    (2)inout_plOutMsgPos为保存转化后内容的开始配置，转化完成后需要修改位置为当前写入内容的最位置后一字节
	//    (3)out_pblFollowupFlag当有后续报文时，需要设置为true，否则设置为false
	//    (4)如果inout_plOutMsgPos不是0表示当前为后续报文，前面的报文存放在out_pszOutMsg中，转化后需要重新修改记录数

	//判断是否为第一次记录  lPos为0则是 否则不是
	if (0 == lPos)
	{
		//1.1转换响应代码
		memset(szTmp,  32, sizeof(szTmp));
		memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
		lLen = 20;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//1.2转换响应消息
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
		lLen = 40;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//1.3记录数量
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp, "%-4ld", 1);
		lLen = 4;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "写入记录数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}

	}
	else
	{
		//修改记录数量的值 增加1
		if (strlen((out_pszOutMsg)) < 64)
		{
			sprintf(m_szErrMsg, "修改记录数量错误-报文长度%ld不够", in_lOutMsgSize);
			return -1;
		}
		memset(szTmp, 0, sizeof(szTmp));
		memcpy(szTmp, &out_pszOutMsg[60], 4);
		long lNum = atol(szTmp);
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp, "%-4ld", ++lNum);
		memcpy(&out_pszOutMsg[60], szTmp, 4);

	}
	// CONTINUE_PACKAGE：有后续纪录 LAST_PACKAGE：最后一条记录，记录内容非空 OVER_PACKAGE：最后一条记录，记录内容空
	switch (pstTRspMsg->Flag)
	{
	case OVER_PACKAGE:
		break;
	case CONTINUE_PACKAGE:
		*out_pblFollowupFlag = true;
	case LAST_PACKAGE:

		//2.1转换系统报单号
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->orderNo, strlen(pstUsrData->orderNo));
		lLen = 14;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换系统报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.2转换客户代码
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->clientID, strlen(pstUsrData->clientID));
		lLen = 12;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换客户代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.3转换合约代码
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
		lLen = 8;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.4转换买卖方向
		lLen = 1;
		out_pszOutMsg[lPos] = pstUsrData->buyOrSell;
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换买卖方向错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.5转换申请日期
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->applyDate, strlen(pstUsrData->applyDate));
		lLen = 8;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换申请日期错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.6转换申请时间
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->applyTime, strlen(pstUsrData->applyTime));
		lLen = 8;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换申请时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.7转换数量
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-10d",pstUsrData->amount);
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.8转换撤销时间
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->cancelTime, strlen(pstUsrData->cancelTime));
		lLen = 8;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换撤销时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.9转换报单状态
		memset(szTmp, 32, sizeof(szTmp));
		szTmp[0] = pstUsrData->status;
		lLen = 1;
		szTmp[lLen] = 0;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换状态错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.10转换报单本地编号
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->localOrderID, strlen(pstUsrData->localOrderID));
		lLen = 14;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换报单本地编号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.11转换会员代码
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->memberID, strlen(pstUsrData->memberID));
		lLen = 6;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换会员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.12转换交易员代码
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->traderID, strlen(pstUsrData->traderID));
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换交易员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.13转换撤销员代码
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->cancelID, strlen(pstUsrData->cancelID));
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换撤销员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.14转换剩余数量
		memset(szTmp, 32, sizeof(szTmp));
		sprintf(szTmp, "%-10d", pstUsrData->remainAmount);
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换剩余数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		break;

	default:
		break;
	}

	

	//成功返回转化后的字节数，失败返回-1
	lRet = lPos - (*inout_plOutMsgPos);
	*inout_plOutMsgPos = lPos;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_RspQryDeferOrder
* 功能描述：OutAPI-将现货延期交收报单查询请求应答回调API报文转化为平台报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- inout_plOutMsgPos         输入输出 --保存在outMsg报文当前保存位置
*- inout_ppszOutFileName         输入输出 --存文件的文件名
*- out_pblFollowupFlag         输出 --是否有后续报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei于 20091111创建
************************************************************/

long CGTAMsgConvert::rl_RspQryDeferOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag)
{
	long	lRet = -1;
	TTRspMsg * pstTRspMsg;
	TDeferOrder *pstUsrData;
	char szTmp[512];
	long lPos = *inout_plOutMsgPos, lLen = 0;
	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));

	//输出日志MY_NORMAL，MY_D3

	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}

	//API回调返回结构是按TTRspMsg、TDeferOrder顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TDeferOrder*)((char*)in_pszInMsg + sizeof(TTRspMsg));

	//缺省为没有后续报文
	*out_pblFollowupFlag = false;

	//1、将API回调返回的结构TTRspMsg和TDeferOrder转化为平台下传的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//    (2)inout_plOutMsgPos为保存转化后内容的开始配置，转化完成后需要修改位置为当前写入内容的最位置后一字节
	//    (3)out_pblFollowupFlag当有后续报文时，需要设置为true，否则设置为false
	//    (4)如果inout_plOutMsgPos不是0表示当前为后续报文，前面的报文存放在out_pszOutMsg中，转化后需要重新修改记录数

	//判断是否为第一次记录  lPos为0则是 否则不是
	if (0 == lPos)
	{
		//1.1转换响应代码
		memset(szTmp,  32, sizeof(szTmp));
		memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
		lLen = 20;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//1.2转换响应消息
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
		lLen = 40;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//1.3记录数量
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp, "%-4ld", 1);
		lLen = 4;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "写入记录数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}

	}
	else
	{
		//修改记录数量的值 增加1
		if (strlen((out_pszOutMsg)) < 64)
		{
			sprintf(m_szErrMsg, "修改记录数量错误-报文长度%ld不够", in_lOutMsgSize);
			return -1;
		}
		memset(szTmp, 0, sizeof(szTmp));
		memcpy(szTmp, &out_pszOutMsg[60], 4);
		long lNum = atol(szTmp);
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp, "%-4ld", ++lNum);
		memcpy(&out_pszOutMsg[60], szTmp, 4);

	}
	// CONTINUE_PACKAGE：有后续纪录 LAST_PACKAGE：最后一条记录，记录内容非空 OVER_PACKAGE：最后一条记录，记录内容空
	switch (pstTRspMsg->Flag)
	{
	case OVER_PACKAGE:
		break;
	case CONTINUE_PACKAGE:
		*out_pblFollowupFlag = true;
	case LAST_PACKAGE:

		//2.1转换报单号
		memset(szTmp, 32, sizeof(szTmp));

		memcpy(szTmp, pstUsrData->orderNo, strlen(pstUsrData->orderNo));
		lLen = 14;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.2转换客户代码
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->clientID, strlen(pstUsrData->clientID));
		lLen = 12;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换客户代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}



		//2.3转换合约代码
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
		lLen = 8;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}



		//2.4转换会员代码
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->memberID, strlen(pstUsrData->memberID));
		lLen = 6;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换会员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.5转换买卖方向
		lLen = 1;
		out_pszOutMsg[lPos] = pstUsrData->buyOrSell;
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换买卖方向错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.6转换申报日期
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->applyDate, strlen(pstUsrData->applyDate));
		lLen = 8;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换申报日期错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.7转换申报时间
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->applyTime, strlen(pstUsrData->applyTime));
		lLen = 8;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换申报时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.8转换开平仓标志
		lLen = 1;
		out_pszOutMsg[lPos] = pstUsrData->offSetFlag;
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换开平仓标志错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.9转换价格
		memset(szTmp, 0, sizeof(szTmp));
		CGTABufferBuilder::rd_RoundFloat(pstUsrData->price, 2);
		sprintf(szTmp, "%-18f", pstUsrData->price);
		lLen = 18;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换价格错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.10转换数量
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-10d",pstUsrData->amount);
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.11转换剩余数量
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-10d",pstUsrData->remainAmount);
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换剩余数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.12转换撤销时间
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->cancelTime, strlen(pstUsrData->cancelTime));
		lLen = 8;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换撤销时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.13转换状态
		lLen = 1;
		out_pszOutMsg[lPos] = pstUsrData->status;
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换状态错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.14转换本地报单号
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->localOrderNo, strlen(pstUsrData->localOrderNo));
		lLen = 14;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换本地报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.15转换保证金率
		memset(szTmp, 0, sizeof(szTmp));
		CGTABufferBuilder::rd_RoundFloat(pstUsrData->margin, 2);
		sprintf(szTmp, "%-18f", pstUsrData->margin);
		lLen = 18;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换保证金率错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.16转换保证金类型
		lLen = 1;
		out_pszOutMsg[lPos] = pstUsrData->marginType;
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换保证金类型错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}



		//2.17转换交易员代码
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->traderID, strlen(pstUsrData->traderID));
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换交易员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}



		//2.18转换撤销员代码
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->cancelID, strlen(pstUsrData->cancelID));
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换撤销员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}



		//2.19转换类型
		lLen = 1;
		out_pszOutMsg[lPos] = pstUsrData->matchType;
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换类型错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		break;

	default:
		break;
	}

	

	//成功返回转化后的字节数，失败返回-1
	lRet = lPos - (*inout_plOutMsgPos);
	*inout_plOutMsgPos = lPos;
	return lRet;
}


/**********************************************************
* 函 数 名： CTrMsgConvert::rl_RspQryDeferClientPosi
* 功能描述：OutAPI-将客户现货延期交收持仓查询响应回调API报文转化为平台报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- inout_plOutMsgPos         输入输出 --保存在outMsg报文当前保存位置
*- inout_ppszOutFileName         输入输出 --存文件的文件名
*- out_pblFollowupFlag         输出 --是否有后续报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei于 20091104创建
************************************************************/

long CGTAMsgConvert::rl_RspQryDeferClientPosi(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag)
{
	long	lRet = -1;
	TTRspMsg * pstTRspMsg;
	TDeferClientPosi *pstUsrData;
	char szTmp[512];
	long lPos = *inout_plOutMsgPos, lLen = 0;
	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));

	//输出日志MY_NORMAL，MY_D3

	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}

	//API回调返回结构是按TTRspMsg、TDeferClientPosi顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TDeferClientPosi*)((char*)in_pszInMsg + sizeof(TTRspMsg));

	//缺省为没有后续报文
	*out_pblFollowupFlag = false;

	//1、将API回调返回的结构TTRspMsg和TDeferClientPosi转化为平台下传的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//    (2)inout_plOutMsgPos为保存转化后内容的开始配置，转化完成后需要修改位置为当前写入内容的最位置后一字节
	//    (3)out_pblFollowupFlag当有后续报文时，需要设置为true，否则设置为false
	//    (4)如果inout_plOutMsgPos不是0表示当前为后续报文，前面的报文存放在out_pszOutMsg中，转化后需要重新修改记录数

	//判断是否为第一次记录  lPos为0则是 否则不是
	if (0 == lPos)
	{
		//1.1转换响应代码
		memset(szTmp,  32, sizeof(szTmp));
		memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
		lLen = 20;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//1.2转换响应消息
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
		lLen = 40;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//1.3记录数量
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp, "%-4ld", 1);
		lLen = 4;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "写入记录数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}

	}
	else
	{
		//修改记录数量的值 增加1
		if (strlen((out_pszOutMsg)) < 64)
		{
			sprintf(m_szErrMsg, "修改记录数量错误-报文长度%ld不够", in_lOutMsgSize);
			return -1;
		}
		memset(szTmp, 0, sizeof(szTmp));
		memcpy(szTmp, &out_pszOutMsg[60], 4);
		long lNum = atol(szTmp);
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp, "%-4ld", ++lNum);
		memcpy(&out_pszOutMsg[60], szTmp, 4);

	}
	// CONTINUE_PACKAGE：有后续纪录 LAST_PACKAGE：最后一条记录，记录内容非空 OVER_PACKAGE：最后一条记录，记录内容空
	switch (pstTRspMsg->Flag)
	{
	case OVER_PACKAGE:
		break;
	case CONTINUE_PACKAGE:
		*out_pblFollowupFlag = true;
	case LAST_PACKAGE:

		//2.1转换合约代码
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
		lLen = 8;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.2转换客户代码
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->clientID, strlen(pstUsrData->clientID));
		lLen = 12;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换客户代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.3转换会员代码
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->memberID, strlen(pstUsrData->memberID));
		lLen = 6;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换会员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.4转换多头持仓量
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-10d",pstUsrData->longPosi);
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换多头持仓量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.5转换空头持仓量
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-10d",pstUsrData->shortPosi);
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换空头持仓量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.6转换上日多头持仓量
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-10d",pstUsrData->lastLong);
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换上日多头持仓量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.7转换上日空头持仓量
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-10d",pstUsrData->lastShort);
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换上日空头持仓量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.8转换今日多头开仓量
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-10d",pstUsrData->todayLong);
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换今日多头开仓量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.9转换今日空头开仓量
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-10d",pstUsrData->todayShort);
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换今日空头开仓量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.10转换今日多头平仓量
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-10d",pstUsrData->todayOffsetLong);
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换今日多头平仓量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.11转换今日空头平仓量
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-10d",pstUsrData->todayOffsetShort);
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换今日空头平仓量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.12转换多头持仓金额
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp, "%-18f", pstUsrData->longTurnOver);
		lLen = 18;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换多头持仓金额错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.13转换空头持仓金额
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp, "%-18f", pstUsrData->shortTurnOver);
		lLen = 18;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换空头持仓金额错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.14转换交割申报多头冻结
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-10d",pstUsrData->deliveryApplyLongFrozen);
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换交割申报多头冻结错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.15转换交割申报空头冻结
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-10d",pstUsrData->deliveryApplyShortFrozen);
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换交割申报空头冻结错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.16转换交割申报使用多头持仓
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-10d",pstUsrData->deliveryApplyLong);
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换交割申报使用多头持仓错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.17转换交割申报使用空头持仓
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-10d",pstUsrData->deliveryApplyShort);
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换交割申报使用空头持仓错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.18转换多头开仓冻结限额
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-10d",pstUsrData->longLimitFrozen);
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换多头开仓冻结限额错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.19转换空头开仓冻结限额
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-10d",pstUsrData->shortLimitFrozen);
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换空头开仓冻结限额错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.20转换多头平仓冻结
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-10d",pstUsrData->offsetLongFrozen);
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换多头平仓冻结错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.21转换空头平仓冻结
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-10d",pstUsrData->offsetShortFrozen);
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换空头平仓冻结错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		break;

	default:
		break;
	}

	

	//成功返回转化后的字节数，失败返回-1
	lRet = lPos - (*inout_plOutMsgPos);
	*inout_plOutMsgPos = lPos;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_RspQryDeferMemberPosi
* 功能描述：OutAPI-将会员现货延期交收持仓查询应答回调API报文转化为平台报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- inout_plOutMsgPos         输入输出 --保存在outMsg报文当前保存位置
*- inout_ppszOutFileName         输入输出 --存文件的文件名
*- out_pblFollowupFlag         输出 --是否有后续报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei于 20091104创建
************************************************************/

long CGTAMsgConvert::rl_RspQryDeferMemberPosi(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag)
{
	long	lRet = -1;
	TTRspMsg * pstTRspMsg;
	TDeferMemberPosi *pstUsrData;
	char szTmp[512];
	long lPos = *inout_plOutMsgPos, lLen = 0;
	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));

	//输出日志MY_NORMAL，MY_D3

	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}

	//API回调返回结构是按TTRspMsg、TDeferMemberPosi顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TDeferMemberPosi*)((char*)in_pszInMsg + sizeof(TTRspMsg));

	//缺省为没有后续报文
	*out_pblFollowupFlag = false;

	//1、将API回调返回的结构TTRspMsg和TDeferMemberPosi转化为平台下传的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//    (2)inout_plOutMsgPos为保存转化后内容的开始配置，转化完成后需要修改位置为当前写入内容的最位置后一字节
	//    (3)out_pblFollowupFlag当有后续报文时，需要设置为true，否则设置为false
	//    (4)如果inout_plOutMsgPos不是0表示当前为后续报文，前面的报文存放在out_pszOutMsg中，转化后需要重新修改记录数

	//判断是否为第一次记录  lPos为0则是 否则不是
	if (0 == lPos)
	{
		//1.1转换响应代码
		memset(szTmp,  32, sizeof(szTmp));
		memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
		lLen = 20;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//1.2转换响应消息
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
		lLen = 40;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//1.3记录数量
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp, "%-4ld", 1);
		lLen = 4;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "写入记录数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}

	}
	else
	{
		//修改记录数量的值 增加1
		if (strlen((out_pszOutMsg)) < 64)
		{
			sprintf(m_szErrMsg, "修改记录数量错误-报文长度%ld不够", in_lOutMsgSize);
			return -1;
		}
		memset(szTmp, 0, sizeof(szTmp));
		memcpy(szTmp, &out_pszOutMsg[60], 4);
		long lNum = atol(szTmp);
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp, "%-4ld", ++lNum);
		memcpy(&out_pszOutMsg[60], szTmp, 4);

	}
	// CONTINUE_PACKAGE：有后续纪录 LAST_PACKAGE：最后一条记录，记录内容非空 OVER_PACKAGE：最后一条记录，记录内容空
	switch (pstTRspMsg->Flag)
	{
	case OVER_PACKAGE:
		break;
	case CONTINUE_PACKAGE:
		*out_pblFollowupFlag = true;
	case LAST_PACKAGE:

		//2.1转换会员代码
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->memberID, strlen(pstUsrData->memberID));
		lLen = 6;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换会员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.2转换合约代码
		memset(szTmp, 32, sizeof(szTmp));
		memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
		lLen = 8;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.3转换多头持仓量
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-10d",pstUsrData->longPosi);
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换多头持仓量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.4转换空头持仓量
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-10d",pstUsrData->shortPosi);
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换空头持仓量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.5转换上日多头持仓量
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-10d",pstUsrData->lastLong);
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换上日多头持仓量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.6转换上日空头持仓量
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-10d",pstUsrData->lastShort);
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换上日空头持仓量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.7转换今日多头开仓量
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-10d",pstUsrData->todayLong);
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换今日多头开仓量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.8转换今日空头开仓量
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-10d",pstUsrData->todayShort);
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换今日空头开仓量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.9转换今日多头平仓量
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-10d",pstUsrData->todayOffsetLong);
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换今日多头平仓量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.10转换今日空头平仓量
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-10d",pstUsrData->todayOffsetShort);
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换今日空头平仓量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.11转换多头持仓金额
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp, "%-18f", pstUsrData->longTurnOver);
		lLen = 18;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换多头持仓金额错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.12转换空头持仓金额
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp, "%-18f", pstUsrData->shortTurnOver);
		lLen = 18;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换空头持仓金额错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.13转换交割申报多头冻结
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-10d",pstUsrData->deliveryApplyLongFrozen);
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换交割申报多头冻结错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.14转换交割申报空头冻结
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-10d",pstUsrData->deliveryApplyShortFrozen);
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换交割申报空头冻结错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.15转换交割申报使用多头持仓
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-10d",pstUsrData->deliveryApplyLong);
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换交割申报使用多头持仓错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.16转换交割申报使用空头持仓
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-10d",pstUsrData->deliveryApplyShort);
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换交割申报使用空头持仓错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.17转换多头开仓冻结限额
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-10d",pstUsrData->longLimitFrozen);
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换多头开仓冻结限额错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.18转换空头开仓冻结限额
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-10d",pstUsrData->shortLimitFrozen);
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换空头开仓冻结限额错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.19转换多头平仓冻结
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-10d",pstUsrData->offsetLongFrozen);
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换多头平仓冻结错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		//2.20转换空头平仓冻结
		memset(szTmp, 0, sizeof(szTmp));
		sprintf(szTmp,"%-10d",pstUsrData->offsetShortFrozen);
		lLen = 10;
		memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
		lPos += lLen;
		if (lPos > in_lOutMsgSize)
		{
			sprintf(m_szErrMsg, "转换空头平仓冻结错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
			return -1;
		}


		break;

	default:
		break;
	}

	

	//成功返回转化后的字节数，失败返回-1
	lRet = lPos - (*inout_plOutMsgPos);
	*inout_plOutMsgPos = lPos;
	return lRet;
}


/**********************************************************
* 函 数 名： CTrMsgConvert::rl_ReqQrySpotQuotation
* 功能描述：InAPI-将平台现货行情查询请求报文转化为调用API输入报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --输出空间的大小
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：ukyo  于 20071014创建
************************************************************/

long CGTAMsgConvert::rl_ReqQrySpotQuotation(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize)
{
	long	lRet = -1, lCount = 0;
	// 定义下一个开始取值的位置 和 下一个要取值的长度
	long lPos = 0, lLen = 0;
	// 交易员登录请求信息
	TReqQryQuotation *pstUsrData;
	char 	szTmp[512];


	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3


	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (sizeof(TReqQryQuotation) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "转化后的报文存放空间不足");
		return -1;
	}

	//将平台上传的结构转化为API所需要的结构TReqQryQuotation
	pstUsrData = (TReqQryQuotation*)out_pszOutMsg;


	//1取合约代码

	memset(szTmp, 0, sizeof(szTmp));
	lLen = 8;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取合约代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->instID = szTmp;
	lCount += strlen(szTmp);

	//2取市场代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 2;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取市场代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "市场代码为空");
		return -1;
	}
	pstUsrData->marketID = szTmp;
	lCount += strlen(szTmp);


	


	//成功返回转化后的字节数，失败返回-1
	lRet = sizeof(TReqQryQuotation);
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_ReqQryDeferQuotation
* 功能描述：InAPI-将平台现货延期交收行情查询请求报文转化为调用API输入报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --输出空间的大小
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei于 20091112创建
************************************************************/

long CGTAMsgConvert::rl_ReqQryDeferQuotation(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize)
{
	long	lRet = -1, lCount = 0;
	// 定义下一个开始取值的位置 和 下一个要取值的长度
	long lPos = 0, lLen = 0;
	// 交易员登录请求信息
	TReqQryQuotation *pstUsrData;
	char 	szTmp[512];


	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3


	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (sizeof(TReqQryQuotation) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "转化后的报文存放空间不足");
		return -1;
	}

	//将平台上传的结构转化为API所需要的结构TReqQryQuotation
	pstUsrData = (TReqQryQuotation*)out_pszOutMsg;


	//1取合约代码

	memset(szTmp, 0, sizeof(szTmp));
	lLen = 8;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取合约代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->instID = szTmp;
	lCount += strlen(szTmp);

	//2取市场代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 2;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取市场代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "市场代码为空");
		return -1;
	}
	pstUsrData->marketID = szTmp;



	


	//成功返回转化后的字节数，失败返回-1
	lRet = sizeof(TReqQryQuotation);
	return lRet;
}


/**********************************************************
* 函 数 名： CTrMsgConvert::rl_RspQrySpotQuotation
* 功能描述：OutAPI-将现货行情查询响应回调API报文转化为平台报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- inout_plOutMsgPos         输入输出 --保存在outMsg报文当前保存位置
*- inout_ppszOutFileName         输入输出 --存文件的文件名
*- out_pblFollowupFlag         输出 --是否有后续报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：ukyo  于 20071014创建
************************************************************/

long CGTAMsgConvert::rl_RspQrySpotQuotation(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag)
{
	long	lRet = -1;
	TTRspMsg * pstTRspMsg;
	TSpotQuotation *pstUsrData;
	char szTmp[512];
	long lPos = *inout_plOutMsgPos, lLen = 0;

	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));

	//输出日志MY_NORMAL，MY_D3

	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}

	//API回调返回结构是按TTRspMsg、TSpotQuotation顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TSpotQuotation*)((char*)in_pszInMsg + sizeof(TTRspMsg));

	//缺省为没有后续报文
	*out_pblFollowupFlag = false;

	//1、将API回调返回的结构TTRspMsg和TSpotQuotation转化为平台下传的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//    (2)inout_plOutMsgPos为保存转化后内容的开始配置，转化完成后需要修改位置为当前写入内容的最位置后一字节
	//    以下项只在特定的函数中才有，如查询之类
	//    (3)out_pblFollowupFlag当有后续报文时，需要设置为true，否则设置为false
	//    (4)如果inout_plOutMsgPos不是0表示当前为后续报文，前面的报文存放在out_pszOutMsg中，转化后需要重新修改记录数

	//1.1转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//1.2转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.1转换合约代码
	memset(szTmp, 32, sizeof(szTmp));

	memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.2转换合约名称
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->name, strlen(pstUsrData->name));
	lLen = 12;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约名称错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.3转换昨收盘
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->lastClose, 2);
	sprintf(szTmp, "%-17f", pstUsrData->lastClose);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换昨收盘错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.4转换开盘价
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->open, 2);
	sprintf(szTmp, "%-17f", pstUsrData->open);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换开盘价错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.5转换最高价
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->high, 2);
	sprintf(szTmp, "%-17f", pstUsrData->high);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换最高价错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.6转换最低价
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->low, 2);
	sprintf(szTmp, "%-17f", pstUsrData->low);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换最低价错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.7转换最新价
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->last, 2);
	sprintf(szTmp, "%-17f", pstUsrData->last);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换最新价错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.8转换收盘价
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->close, 2);
	sprintf(szTmp, "%-17f", pstUsrData->close);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换收盘价错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.9转换买价1
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->bid1, 2);
	sprintf(szTmp, "%-17f", pstUsrData->bid1);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买价1错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.10转换买量1
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->bidLot1);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买量1错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.11转换买价2
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->bid2, 2);
	sprintf(szTmp, "%-17f", pstUsrData->bid2);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买价2错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.12转换买量2
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->bidLot2);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买量2错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.13转换买价3
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->bid3, 2);
	sprintf(szTmp, "%-17f", pstUsrData->bid3);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买价3错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.14转换买量3
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->bidLot3);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买量3错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.15转换买价4
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->bid4, 2);
	sprintf(szTmp, "%-17f", pstUsrData->bid4);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买价4错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.16转换买量4
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->bidLot4);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买量4错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.17转换买价5
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->bid5, 2);
	sprintf(szTmp, "%-17f", pstUsrData->bid5);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买价5错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.17转换买量5
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->bidLot5);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买量5错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.18转换卖价1
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->ask1, 2);
	sprintf(szTmp, "%-17f", pstUsrData->ask1);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换卖价1错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.19转换卖量1
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->askLot1);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换卖量1错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.20转换卖价2
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->ask2, 2);
	sprintf(szTmp, "%-17f", pstUsrData->ask2);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换卖价2错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.21转换卖量2
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->askLot2);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换卖量2错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.22转换卖价3
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->ask3, 2);
	sprintf(szTmp, "%-17f", pstUsrData->ask3);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换卖价3错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.23转换卖量3
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->askLot3);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换卖量3错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.24转换卖价4
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->ask4, 2);
	sprintf(szTmp, "%-17f", pstUsrData->ask4);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换卖价4错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.25转换卖量4
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->askLot4);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换卖量4错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.26转换卖价5
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->ask5, 2);
	sprintf(szTmp, "%-17f", pstUsrData->ask5);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换卖价5错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.27转换卖量5
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->askLot5);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换卖量5错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.28转换成交量
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->volume);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换成交量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.29转换成交重量
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->weight, 2);
	sprintf(szTmp, "%-17f", pstUsrData->weight);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换成交重量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.30转换涨停板
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->highLimit, 2);
	sprintf(szTmp, "%-17f", pstUsrData->highLimit);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换涨停板错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.31转换跌停板
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->lowLimit, 2);
	sprintf(szTmp, "%-17f", pstUsrData->lowLimit);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换跌停板错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.32涨跌
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->upDown, 2);
	sprintf(szTmp, "%-17f", pstUsrData->upDown);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换涨跌错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.33涨跌幅度
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->upDownRate, 2);
	sprintf(szTmp, "%-17f", pstUsrData->upDownRate);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换涨跌幅度错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.34成交额
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->turnOver, 2);
	sprintf(szTmp, "%-17f", pstUsrData->turnOver);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换成交额错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.35均价
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->average, 2);
	sprintf(szTmp, "%-17f", pstUsrData->average);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换均价错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.36转换行情日期
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->quoteDate, strlen(pstUsrData->quoteDate));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换行情日期错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.37转换行情时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->quoteTime, strlen(pstUsrData->quoteTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换行情时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.38转换行情序号
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->sequenceNo);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换行情序号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	

	//成功返回转化后的字节数，失败返回-1
	lRet = lPos - (*inout_plOutMsgPos);
	*inout_plOutMsgPos = lPos;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_RspQryDeferQuotation
* 功能描述：OutAPI-将现货延期交收行情查询响应回调API报文转化为平台报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- inout_plOutMsgPos         输入输出 --保存在outMsg报文当前保存位置
*- inout_ppszOutFileName         输入输出 --存文件的文件名
*- out_pblFollowupFlag         输出 --是否有后续报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei于 20091112创建
************************************************************/

long CGTAMsgConvert::rl_RspQryDeferQuotation(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag)
{
	long	lRet = -1;
	TTRspMsg * pstTRspMsg;
	TDeferQuotation *pstUsrData;
	char szTmp[512];
	long lPos = *inout_plOutMsgPos, lLen = 0;

	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));

	//输出日志MY_NORMAL，MY_D3

	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}

	//API回调返回结构是按TTRspMsg、TDeferQuotation顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TDeferQuotation*)((char*)in_pszInMsg + sizeof(TTRspMsg));

	//缺省为没有后续报文
	*out_pblFollowupFlag = false;

	//1、将API回调返回的结构TTRspMsg和TDeferQuotation转化为平台下传的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//    (2)inout_plOutMsgPos为保存转化后内容的开始配置，转化完成后需要修改位置为当前写入内容的最位置后一字节
	//    以下项只在特定的函数中才有，如查询之类
	//    (3)out_pblFollowupFlag当有后续报文时，需要设置为true，否则设置为false
	//    (4)如果inout_plOutMsgPos不是0表示当前为后续报文，前面的报文存放在out_pszOutMsg中，转化后需要重新修改记录数

	//1.1转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//1.2转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.1转换合约代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.2转换合约名称
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->name, strlen(pstUsrData->name));
	lLen = 12;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约名称错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.3转换昨结算
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->lastSettle, 2);
	sprintf(szTmp, "%-17f", pstUsrData->lastSettle);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换昨结算错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.4转换昨收盘
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->lastClose, 2);
	sprintf(szTmp, "%-17f", pstUsrData->lastClose);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换昨收盘错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}
	//2.5转换开盘价
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->open, 2);
	sprintf(szTmp, "%-17f", pstUsrData->open);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换开盘价错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.6转换最高价
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->high, 2);
	sprintf(szTmp, "%-17f", pstUsrData->high);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换最高价错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.7转换最低价
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->low, 2);
	sprintf(szTmp, "%-17f", pstUsrData->low);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换最低价错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.8转换最新价
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->last, 2);
	sprintf(szTmp, "%-17f", pstUsrData->last);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换最新价错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.9转换收盘价
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->close, 2);
	sprintf(szTmp, "%-17f", pstUsrData->close);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换收盘价错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.10转换结算价
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->settle, 2);
	sprintf(szTmp, "%-17f", pstUsrData->settle);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换结算价错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.11转换买价1
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->bid1, 2);
	sprintf(szTmp, "%-17f", pstUsrData->bid1);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买价1错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.12转换买量1
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->bidLot1);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买量1错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.13转换买价2
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->bid2, 2);
	sprintf(szTmp, "%-17f", pstUsrData->bid2);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买价2错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.14转换买量2
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->bidLot2);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买量2错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.15转换买价3
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->bid3, 2);
	sprintf(szTmp, "%-17f", pstUsrData->bid3);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买价3错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.16转换买量3
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->bidLot3);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买量3错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.17转换买价4
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->bid4, 2);
	sprintf(szTmp, "%-17f", pstUsrData->bid4);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买价4错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.18转换买量4
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->bidLot4);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买量4错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.19转换买价5
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->bid5, 2);
	sprintf(szTmp, "%-17f", pstUsrData->bid5);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买价5错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.20转换买量5
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->bidLot5);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买量5错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.21转换卖价1
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->ask1, 2);
	sprintf(szTmp, "%-17f", pstUsrData->ask1);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换卖价1错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.22转换卖量1
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->askLot1);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换卖量1错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.23转换卖价2
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->ask2, 2);
	sprintf(szTmp, "%-17f", pstUsrData->ask2);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换卖价2错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.24转换卖量2
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->askLot2);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换卖量2错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.25转换卖价3
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->ask3, 2);
	sprintf(szTmp, "%-17f", pstUsrData->ask3);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换卖价3错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.26转换卖量3
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->askLot3);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换卖量3错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.27转换卖价4
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->ask4, 2);
	sprintf(szTmp, "%-17f", pstUsrData->ask4);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换卖价4错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.28转换卖量4
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->askLot4);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换卖量4错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.29转换卖价5
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->ask5, 2);
	sprintf(szTmp, "%-17f", pstUsrData->ask5);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换卖价5错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.30转换卖量5
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->askLot5);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换卖量5错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.31转换成交量
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->volume);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换成交量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.32转换成交重量
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->weight, 2);
	sprintf(szTmp, "%-17f", pstUsrData->weight);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换成交重量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.33转换涨停板
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->highLimit, 2);
	sprintf(szTmp, "%-17f", pstUsrData->highLimit);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换涨停板错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.34转换跌停板
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->lowLimit, 2);
	sprintf(szTmp, "%-17f", pstUsrData->lowLimit);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换跌停板错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.35转换持仓量
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->Posi);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换持仓量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.36涨跌
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->upDown, 2);
	sprintf(szTmp, "%-17f", pstUsrData->upDown);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换涨跌错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.37涨跌幅度
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->upDownRate, 2);
	sprintf(szTmp, "%-17f", pstUsrData->upDownRate);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换涨跌幅度错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.38成交额
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->turnOver, 2);
	sprintf(szTmp, "%-17f", pstUsrData->turnOver);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换成交额错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.39均价
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->average, 2);
	sprintf(szTmp, "%-17f", pstUsrData->average);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换均价错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.40转换行情日期
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->quoteDate, strlen(pstUsrData->quoteDate));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换行情日期错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.41转换行情时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->quoteTime, strlen(pstUsrData->quoteTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换行情时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.42转换行情序号
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->sequenceNo);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换行情序号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	

	//成功返回转化后的字节数，失败返回-1
	lRet = lPos - (*inout_plOutMsgPos);
	*inout_plOutMsgPos = lPos;
	return lRet;
}


/**********************************************************
* 函 数 名： CTrMsgConvert::rl_ReqQrySpotMinsQuotation
* 功能描述：InAPI-将平台现货分钟行情查询请求报文转化为调用API输入报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --输出空间的大小
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：ukyo  于 20071014创建
************************************************************/

long CGTAMsgConvert::rl_ReqQrySpotMinsQuotation(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize)
{
	long	lRet = -1, lCount = 0;
	// 定义下一个开始取值的位置 和 下一个要取值的长度
	long lPos = 0, lLen = 0;
	// 交易员登录请求信息
	TReqQryMinsQuotation *pstUsrData;
	char 	szTmp[512];


	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3


	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (sizeof(TReqQryMinsQuotation) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "转化后的报文存放空间不足");
		return -1;
	}

	//将平台上传的结构转化为API所需要的结构TReqQryMinsQuotation
	pstUsrData = (TReqQryMinsQuotation*)out_pszOutMsg;


	//1取合约代码

	memset(szTmp, 0, sizeof(szTmp));
	lLen = 8;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取合约代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "合约代码为空");
		return -1;
	}
	pstUsrData->instID = szTmp;
	lCount += strlen(szTmp);

	//2取市场代码
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 2;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取市场代码错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "市场代码为空");
		return -1;
	}
	pstUsrData->marketID = szTmp;
	lCount += strlen(szTmp);

	//3取上一次时间
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 8;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取上一次时间错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	UStringHelper::Trim(szTmp);
	pstUsrData->lastTime = szTmp;
	lCount += strlen(szTmp);


	


	//成功返回转化后的字节数，失败返回-1
	lRet = sizeof(TReqQryMinsQuotation);
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_RspQrySpotMinsQuotation
* 功能描述：OutAPI-将现货分钟行情查询响应回调API报文转化为平台报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- inout_plOutMsgPos         输入输出 --保存在outMsg报文当前保存位置
*- inout_ppszOutFileName         输入输出 --存文件的文件名
*- out_pblFollowupFlag         输出 --是否有后续报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：ukyo  于 20071014创建
************************************************************/

long CGTAMsgConvert::rl_RspQrySpotMinsQuotation(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag)
{
	long	lRet = -1;
	TTRspMsg * pstTRspMsg;
	TMinsSpotQuotation *pstUsrData;
	char szTmp[512];
	long lPos = *inout_plOutMsgPos, lLen = 0;

	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));

	//输出日志MY_NORMAL，MY_D3

	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}

	//API回调返回结构是按TTRspMsg、TMinsSpotQuotation顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TMinsSpotQuotation*)((char*)in_pszInMsg + sizeof(TTRspMsg));

	//缺省为没有后续报文
	*out_pblFollowupFlag = false;

	//1、将API回调返回的结构TTRspMsg和TMinsSpotQuotation转化为平台下传的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//    (2)inout_plOutMsgPos为保存转化后内容的开始配置，转化完成后需要修改位置为当前写入内容的最位置后一字节
	//    以下项只在特定的函数中才有，如查询之类
	//    (3)out_pblFollowupFlag当有后续报文时，需要设置为true，否则设置为false
	//    (4)如果inout_plOutMsgPos不是0表示当前为后续报文，前面的报文存放在out_pszOutMsg中，转化后需要重新修改记录数

	//1.1转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//1.2转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.1转换合约代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.2转换开盘价
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->open, 2);
	sprintf(szTmp, "%-17f", pstUsrData->open);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换开盘价错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.3转换最高价
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->high, 2);
	sprintf(szTmp, "%-17f", pstUsrData->high);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换最高价错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.4转换最低价
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->low, 2);
	sprintf(szTmp, "%-17f", pstUsrData->low);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换最低价错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.5转换收盘价
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->close, 2);
	sprintf(szTmp, "%-17f", pstUsrData->close);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换收盘价错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.6转换成交量
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->volume);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换成交量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.7转换行情日期
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->quoteDate, strlen(pstUsrData->quoteDate));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换行情日期错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.8转换行情时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->quoteTime, strlen(pstUsrData->quoteTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换行情时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	


	//成功返回转化后的字节数，失败返回-1
	lRet = lPos - (*inout_plOutMsgPos);
	*inout_plOutMsgPos = lPos;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_RspQryDeferMinsQuotation
* 功能描述：OutAPI-将现货延期交收分钟行情查询应答回调API报文转化为平台报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- inout_plOutMsgPos         输入输出 --保存在outMsg报文当前保存位置
*- inout_ppszOutFileName         输入输出 --存文件的文件名
*- out_pblFollowupFlag         输出 --是否有后续报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：ukyo  于 20071014创建
************************************************************/

long CGTAMsgConvert::rl_RspQryDeferMinsQuotation(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, long * inout_plOutMsgPos, char * inout_ppszOutFileName, bool * out_pblFollowupFlag)
{
	long	lRet = -1;
	TTRspMsg * pstTRspMsg;
	TMinsDeferQuotation *pstUsrData;
	char szTmp[512];
	long lPos = *inout_plOutMsgPos, lLen = 0;

	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));

	//输出日志MY_NORMAL，MY_D3

	//有效性检查
	if (NULL == in_pszInMsg)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}

	//API回调返回结构是按TTRspMsg、TMinsDeferQuotation顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TMinsDeferQuotation*)((char*)in_pszInMsg + sizeof(TTRspMsg));

	//缺省为没有后续报文
	*out_pblFollowupFlag = false;

	//1、将API回调返回的结构TTRspMsg和TMinsDeferQuotation转化为平台下传的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//    (2)inout_plOutMsgPos为保存转化后内容的开始配置，转化完成后需要修改位置为当前写入内容的最位置后一字节
	//    以下项只在特定的函数中才有，如查询之类
	//    (3)out_pblFollowupFlag当有后续报文时，需要设置为true，否则设置为false
	//    (4)如果inout_plOutMsgPos不是0表示当前为后续报文，前面的报文存放在out_pszOutMsg中，转化后需要重新修改记录数

	//1.1转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//1.2转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.1转换合约代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.2转换开盘价
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->open, 2);
	sprintf(szTmp, "%-17f", pstUsrData->open);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换开盘价错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.3转换最高价
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->high, 2);
	sprintf(szTmp, "%-17f", pstUsrData->high);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换最高价错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.4转换最低价
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->low, 2);
	sprintf(szTmp, "%-17f", pstUsrData->low);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换最低价错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.5转换收盘价
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->close, 2);
	sprintf(szTmp, "%-17f", pstUsrData->close);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换收盘价错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.6转换成交量
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->volume);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换成交量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.7转换持仓量
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->Posi);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换持仓量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.8转换行情日期
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->quoteDate, strlen(pstUsrData->quoteDate));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换行情日期错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.9转换行情时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->quoteTime, strlen(pstUsrData->quoteTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换行情时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	


	//成功返回转化后的字节数，失败返回-1
	lRet = lPos - (*inout_plOutMsgPos);
	*inout_plOutMsgPos = lPos;
	return lRet;
}


/**********************************************************
* 函 数 名： CTrMsgConvert::rl_OnRecvRtnSpotOrder
* 功能描述：RspInitiative将现货报单回报API报文转化为平台请求报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- in_pszTrCode      输入 --交易代码
*- in_lConvertFlag      输入 --需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：ukyo  于 20071014创建
************************************************************/

long CGTAMsgConvert::rl_OnRecvRtnSpotOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag)
{
	long	lRet = -1,	lLen;
	long lPos = 0;
	TTRspMsg *pstTRspMsg;
	TSpotOrder *pstUsrData;
	char szTmp[512];

	//	memcpy(out_pszOutMsg,"hello",5);
	//	return 5;      //wangfei test

	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3

	//有效性检查
	if (NULL == in_pszInMsg || NULL == in_pszTrCode)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (((long)sizeof(TInstState) + (long)strlen(in_pszTrCode)) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "传入输出报文空间不够");
		return -1;
	}
	//转化标志检查
	if (1 == in_lConvertFlag)
	{
		strcpy(m_szErrMsg, "不支持此格式报文");
		return -1;
	}


	//API回调返回结构是按TTRspMsg、TSpotOrder顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TSpotOrder*)((char*)in_pszInMsg + sizeof(TTRspMsg));

	//1、将API回报的结构TTRspMsg和TSpotOrder转化为向平台发送的请求的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//in_lConvertFlag需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文，现在只做0方式
	//1.1转换交易代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, in_pszTrCode, strlen(in_pszTrCode));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//1.2转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	//	memcpy(szTmp, "000000000", strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//1.3转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	

	//2.1转换报单号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->orderNo, strlen(pstUsrData->orderNo));
	lLen = 16;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.2转换合约代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.3转换客户代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->clientID, strlen(pstUsrData->clientID));
	lLen = 12;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换客户代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.4转换会员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->memberID, strlen(pstUsrData->memberID));
	lLen = 6;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换会员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.5转换买卖方向
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->buyOrSell;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买卖方向错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.5转换申报日期
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->applyDate, strlen(pstUsrData->applyDate));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换申报日期错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.6转换申报时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->applyTime, strlen(pstUsrData->applyTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换申报时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.7转换交易员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->traderID, strlen(pstUsrData->traderID));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.8转换价格
	memset(szTmp, 32, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->price, 2);
	sprintf(szTmp, "%-17f", pstUsrData->price);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换价格错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.9转换数量
	memset(szTmp, 32, sizeof(szTmp));
	sprintf(szTmp, "%-10d", pstUsrData->amount);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.10转换剩余数量
	memset(szTmp, 32, sizeof(szTmp));
	sprintf(szTmp, "%-10d", pstUsrData->remainAmount);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换剩余数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.11转换撤销时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->cancelTime, strlen(pstUsrData->cancelTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换撤销时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.12转换撤消员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->cancelID, strlen(pstUsrData->cancelID));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换撤消员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.13转换状态
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->status;
	//	szTmp[0] = '3';
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换状态错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.14转换类型
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->matchType;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换类型错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.15转换转让合约代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->endorseInstID, strlen(pstUsrData->endorseInstID));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换转让合约代码 错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.16转换本地报单号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->localOrderNo, strlen(pstUsrData->localOrderNo));
	lLen = 14;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换本地报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//成功返回转化后的字节数，失败返回-1
	lRet = lPos;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_OnRecvRtnDeferOrder
* 功能描述：RspInitiative将现货延期报单回报API报文转化为平台请求报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- in_pszTrCode      输入 --交易代码
*- in_lConvertFlag      输入 --需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei  于 20091016创建
************************************************************/

long CGTAMsgConvert::rl_OnRecvRtnDeferOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag)
{
	long	lRet = -1,	lLen;
	long lPos = 0;
	TTRspMsg *pstTRspMsg;
	TDeferOrder *pstUsrData;
	char szTmp[512];

	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3

	//有效性检查
	if (NULL == in_pszInMsg || NULL == in_pszTrCode)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (((long)sizeof(TInstState) + (long)strlen(in_pszTrCode)) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "传入输出报文空间不够");
		return -1;
	}
	//转化标志检查
	if (1 == in_lConvertFlag)
	{
		strcpy(m_szErrMsg, "不支持此格式报文");
		return -1;
	}
	//API回调返回结构是按TTRspMsg、TDeferOrder顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TDeferOrder*)((char*)in_pszInMsg + sizeof(TTRspMsg));


	//1、将API回报的结构TTRspMsg和TDeferOrder转化为向平台发送的请求的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//in_lConvertFlag需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文，现在只做0方式
	//1.1转换交易代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, in_pszTrCode, strlen(in_pszTrCode));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//1.2转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//1.3转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.1转换报单号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->orderNo, strlen(pstUsrData->orderNo));
	lLen = 16;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.2转换客户代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->clientID, strlen(pstUsrData->clientID));
	lLen = 12;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换客户代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.3转换合约代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.4转换会员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->memberID, strlen(pstUsrData->memberID));
	lLen = 6;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换会员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.5转换买卖方向
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->buyOrSell;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买卖方向错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.6转换申报日期
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->applyDate, strlen(pstUsrData->applyDate));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换申报日期错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.7转换申报时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->applyTime, strlen(pstUsrData->applyTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换申报时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.8转换开平仓标志
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->offSetFlag;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换开平仓标志错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.9转换价格
	memset(szTmp, 32, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->price, 2);
	sprintf(szTmp, "%-17f", pstUsrData->price);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换价格错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.10转换数量
	memset(szTmp, 32, sizeof(szTmp));
	sprintf(szTmp, "%-10d", pstUsrData->amount);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.11转换剩余数量
	memset(szTmp, 32, sizeof(szTmp));
	sprintf(szTmp, "%-10d", pstUsrData->remainAmount);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换剩余数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.12转换撤销时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->cancelTime, strlen(pstUsrData->cancelTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换撤销时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.13转换状态
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->status;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换状态错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.14转换本地报单号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->localOrderNo, strlen(pstUsrData->localOrderNo));
	lLen = 14;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换本地报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.15转换保证金率
	memset(szTmp, 32, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->margin, 2);
	sprintf(szTmp, "%-17f", pstUsrData->margin);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换保证金率错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.16转换保证金类型
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->marginType;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换保证金类型错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.17转换交易员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->traderID, strlen(pstUsrData->traderID));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.18转换撤消员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->cancelID, strlen(pstUsrData->cancelID));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换撤消员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.19转换类型
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->matchType;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换类型错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	
	//成功返回转化后的字节数，失败返回-1
	lRet = lPos;
	return lRet;
}



/**********************************************************
* 函 数 名： CTrMsgConvert::rl_onRecvRspQryDeferLocalOrder
* 功能描述：RspInitiative将现货延期交收本地报单查询回报API报文转化为平台请求报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- in_pszTrCode      输入 --交易代码
*- in_lConvertFlag      输入 --需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：sunwenna  于 20110718创建
************************************************************/

long CGTAMsgConvert::rl_onRecvRspQryDeferLocalOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag)
{
	long	lRet = -1,	lLen;
	long lPos = 0;
	TTRspMsg *pstTRspMsg;
	TDeferOrder *pstUsrData;
	char szTmp[512];

	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3

	//有效性检查
	if (NULL == in_pszInMsg || NULL == in_pszTrCode)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (((long)sizeof(TInstState) + (long)strlen(in_pszTrCode)) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "传入输出报文空间不够");
		return -1;
	}
	//转化标志检查
	if (1 == in_lConvertFlag)
	{
		strcpy(m_szErrMsg, "不支持此格式报文");
		return -1;
	}
	//API回调返回结构是按TTRspMsg、TDeferOrder顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TDeferOrder*)((char*)in_pszInMsg + sizeof(TTRspMsg));


	//1、将API回报的结构TTRspMsg和TDeferOrder转化为向平台发送的请求的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//in_lConvertFlag需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文，现在只做0方式
	//1.1转换交易代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, in_pszTrCode, strlen(in_pszTrCode));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//1.2转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//1.3转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.1转换报单号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->orderNo, strlen(pstUsrData->orderNo));
	lLen = 16;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.2转换客户代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->clientID, strlen(pstUsrData->clientID));
	lLen = 12;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换客户代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.3转换合约代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.4转换会员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->memberID, strlen(pstUsrData->memberID));
	lLen = 6;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换会员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.5转换买卖方向
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->buyOrSell;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买卖方向错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.6转换申报日期
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->applyDate, strlen(pstUsrData->applyDate));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换申报日期错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.7转换申报时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->applyTime, strlen(pstUsrData->applyTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换申报时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.8转换开平仓标志
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->offSetFlag;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换开平仓标志错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.9转换价格
	memset(szTmp, 32, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->price, 2);
	sprintf(szTmp, "%-17f", pstUsrData->price);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换价格错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.10转换数量
	memset(szTmp, 32, sizeof(szTmp));
	sprintf(szTmp, "%-10d", pstUsrData->amount);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.11转换剩余数量
	memset(szTmp, 32, sizeof(szTmp));
	sprintf(szTmp, "%-10d", pstUsrData->remainAmount);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换剩余数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.12转换撤销时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->cancelTime, strlen(pstUsrData->cancelTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换撤销时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.13转换状态
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->status;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换状态错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.14转换本地报单号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->localOrderNo, strlen(pstUsrData->localOrderNo));
	lLen = 14;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换本地报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.15转换保证金率
	memset(szTmp, 32, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->margin, 2);
	sprintf(szTmp, "%-17f", pstUsrData->margin);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换保证金率错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.16转换保证金类型
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->marginType;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换保证金类型错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.17转换交易员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->traderID, strlen(pstUsrData->traderID));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.18转换撤消员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->cancelID, strlen(pstUsrData->cancelID));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换撤消员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.19转换类型
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->matchType;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换类型错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//成功返回转化后的字节数，失败返回-1
	lRet = lPos;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_OnRecvRtnMiddleAppOrder
* 功能描述：RspInitiative将现货延期中立仓申报回报API报文转化为平台请求报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- in_pszTrCode      输入 --交易代码
*- in_lConvertFlag      输入 --需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei  于 20091029创建
************************************************************/

long CGTAMsgConvert::rl_OnRecvRtnMiddleAppOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag)
{
	long	lRet = -1,	lLen;
	long lPos = 0;
	TTRspMsg *pstTRspMsg;
	TMiddleAppOrder *pstUsrData;
	char szTmp[512];

	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3

	//有效性检查
	if (NULL == in_pszInMsg || NULL == in_pszTrCode)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (((long)sizeof(TInstState) + (long)strlen(in_pszTrCode)) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "传入输出报文空间不够");
		return -1;
	}
	//转化标志检查
	if (1 == in_lConvertFlag)
	{
		strcpy(m_szErrMsg, "不支持此格式报文");
		return -1;
	}
	//API回调返回结构是按TTRspMsg、TDeferOrder顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TMiddleAppOrder*)((char*)in_pszInMsg + sizeof(TTRspMsg));


	//1、将API回报的结构TTRspMsg和TMiddleAppOrder转化为向平台发送的请求的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//in_lConvertFlag需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文，现在只做0方式
	//1.1转换交易代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, in_pszTrCode, strlen(in_pszTrCode));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//1.2转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//1.3转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.1转换报单号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->orderNo, strlen(pstUsrData->orderNo));
	lLen = 16;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.2转换客户代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->clientID, strlen(pstUsrData->clientID));
	lLen = 12;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换客户代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.3转换合约代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.4转换会员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->memberID, strlen(pstUsrData->memberID));
	lLen = 6;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换会员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.5转换买卖方向
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->buyOrSell;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买卖方向错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.6转换申报日期
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->applyDate, strlen(pstUsrData->applyDate));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换申报日期错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.7转换申报时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->applyTime, strlen(pstUsrData->applyTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换申报时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.8转换数量
	memset(szTmp, 32, sizeof(szTmp));
	sprintf(szTmp, "%-10d", pstUsrData->amount);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//



	//2.9转换撤销时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->cancelTime, strlen(pstUsrData->cancelTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换撤销时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.10转换状态
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->status;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换状态错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.11转换本地报单号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->localOrderID, strlen(pstUsrData->localOrderID));
	lLen = 14;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换本地报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}






	//2.12转换交易员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->traderID, strlen(pstUsrData->traderID));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	


	//2.13转换撤消员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->cancelID, strlen(pstUsrData->cancelID));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换撤消员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	


	//2.14转换剩余数量
	memset(szTmp, 32, sizeof(szTmp));
	sprintf(szTmp, "%-10d", pstUsrData->remainAmount);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换剩余数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	


	

	

	//成功返回转化后的字节数，失败返回-1
	lRet = lPos;
	return lRet;
}


/**********************************************************
* 函 数 名： CTrMsgConvert::rl_OnRecvRtnDeferDeliveryAppOrder
* 功能描述：RspInitiative将现货延期交收交割报单回报API报文转化为平台请求报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- in_pszTrCode      输入 --交易代码
*- in_lConvertFlag      输入 --需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei  于 20091027创建
************************************************************/

long CGTAMsgConvert::rl_OnRecvRtnDeferDeliveryAppOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag)
{
	long	lRet = -1,	lLen;
	long lPos = 0;
	TTRspMsg *pstTRspMsg;
	TDeferDeliveryAppOrder *pstUsrData;
	char szTmp[512];

	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3

	


	//有效性检查
	if (NULL == in_pszInMsg || NULL == in_pszTrCode)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (((long)sizeof(TInstState) + (long)strlen(in_pszTrCode)) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "传入输出报文空间不够");
		return -1;
	}
	//转化标志检查
	if (1 == in_lConvertFlag)
	{
		strcpy(m_szErrMsg, "不支持此格式报文");
		return -1;
	}
	//API回调返回结构是按TTRspMsg、TDeferDeliveryAppOrder顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TDeferDeliveryAppOrder*)((char*)in_pszInMsg + sizeof(TTRspMsg));


	//1、将API回报的结构TTRspMsg和TDeferDeliveryAppOrder转化为向平台发送的请求的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//in_lConvertFlag需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文，现在只做0方式

	

	//1.1转换交易代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, in_pszTrCode, strlen(in_pszTrCode));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//1.2转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//1.3转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.1转换报单号
	memset(szTmp, 32, sizeof(szTmp));

	memcpy(szTmp, pstUsrData->orderNo, strlen(pstUsrData->orderNo));
	lLen = 16;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.2转换客户代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->clientID, strlen(pstUsrData->clientID));
	lLen = 12;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换客户代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}



	//2.3转换合约代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.4转换买卖方向
	lLen = 1;
	out_pszOutMsg[lPos] = pstUsrData->buyOrSell;
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买卖方向错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.5转换申报日期
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->applyDate, strlen(pstUsrData->applyDate));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换申报日期错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.6转换申报时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->applyTime, strlen(pstUsrData->applyTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换申报时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.7转换数量
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->amount);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.8转换撤销时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->cancelTime, strlen(pstUsrData->cancelTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换撤销时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.9转换状态
	lLen = 1;
	out_pszOutMsg[lPos] = pstUsrData->status;
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换状态错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.10转换本地报单号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->localOrderID, strlen(pstUsrData->localOrderID));
	lLen = 14;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换本地报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.11转换会员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->memberID, strlen(pstUsrData->memberID));
	lLen = 6;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换会员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.12转换交易员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->traderID, strlen(pstUsrData->traderID));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.13转换撤销员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->cancelID, strlen(pstUsrData->cancelID));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换撤销员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.14转换剩余数量
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->remainAmount);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换剩余数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	



	//成功返回转化后的字节数，失败返回-1
	lRet = lPos;
	return lRet;
}


/**********************************************************
* 函 数 名： CTrMsgConvert::rl_OnRecvRtnSpotOrderCancel
* 功能描述：RspInitiative将现货撤单回报API报文转化为平台请求报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- in_pszTrCode      输入 --交易代码
*- in_lConvertFlag      输入 --需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：ukyo  于 20071014创建
************************************************************/

long CGTAMsgConvert::rl_OnRecvRtnSpotOrderCancel(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag)
{
	long	lRet = -1,	lLen;
	long	lPos = 0;
	TTRspMsg *pstTRspMsg;
	TSpotOrder *pstUsrData;
	char szTmp[512];

	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3


	//有效性检查
	if (NULL == in_pszInMsg || NULL == in_pszTrCode)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (((long)sizeof(TInstState) + (long)strlen(in_pszTrCode)) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "传入输出报文空间不够");
		return -1;
	}
	//转化标志检查
	if (1 == in_lConvertFlag)
	{
		strcpy(m_szErrMsg, "不支持此格式报文");
		return -1;
	}
	//API回调返回结构是按TTRspMsg、TSpotOrder顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TSpotOrder*)((char*)in_pszInMsg + sizeof(TTRspMsg));


	//1、将API回报的结构TTRspMsg和TSpotOrder转化为向平台发送的请求的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//in_lConvertFlag需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文，现在只做0方式

	//1.1转换交易代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, in_pszTrCode, strlen(in_pszTrCode));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//1.2转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}
	//1.3转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.1转换报单号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->orderNo, strlen(pstUsrData->orderNo));
	lLen = 16;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.2转换合约代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}
	//2.3转换客户代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->clientID, strlen(pstUsrData->clientID));
	lLen = 12;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换客户代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}
	//2.4转换会员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->memberID, strlen(pstUsrData->memberID));
	lLen = 6;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换会员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.5转换买卖方向
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->buyOrSell;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买卖方向错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.5转换申报日期
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->applyDate, strlen(pstUsrData->applyDate));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换申报日期错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.6转换申报时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->applyTime, strlen(pstUsrData->applyTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换申报时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.7转换交易员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->traderID, strlen(pstUsrData->traderID));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.8转换价格
	memset(szTmp, 32, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->price, 2);
	sprintf(szTmp, "%-17f", pstUsrData->price);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换价格错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.9转换数量
	memset(szTmp, 32, sizeof(szTmp));
	sprintf(szTmp, "%-10d", pstUsrData->amount);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.10转换剩余数量
	memset(szTmp, 32, sizeof(szTmp));
	sprintf(szTmp, "%-10d", pstUsrData->remainAmount);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换剩余数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.11转换撤销时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->cancelTime, strlen(pstUsrData->cancelTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换撤销时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.12转换撤消员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->cancelID, strlen(pstUsrData->cancelID));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换撤消员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.13转换状态
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->status;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换状态错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.14转换类型
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->matchType;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换类型错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.15转换转让合约代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->endorseInstID, strlen(pstUsrData->endorseInstID));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换转让合约代码 错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.16转换本地报单号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->localOrderNo, strlen(pstUsrData->localOrderNo));
	lLen = 14;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换本地报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	



	//成功返回转化后的字节数，失败返回-1
	lRet = lPos;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_OnRecvRtnDeferOrderCancel
* 功能描述：RspInitiative将现货延期撤单回报API报文转化为平台请求报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- in_pszTrCode      输入 --交易代码
*- in_lConvertFlag      输入 --需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei  于 20091027创建
************************************************************/

long CGTAMsgConvert::rl_OnRecvRtnDeferOrderCancel(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag)
{
	long	lRet = -1,	lLen;
	long	lPos = 0;
	TTRspMsg *pstTRspMsg;
	TDeferOrder *pstUsrData;
	char szTmp[512];

	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3


	//有效性检查
	if (NULL == in_pszInMsg || NULL == in_pszTrCode)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (((long)sizeof(TInstState) + (long)strlen(in_pszTrCode)) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "传入输出报文空间不够");
		return -1;
	}
	//转化标志检查
	if (1 == in_lConvertFlag)
	{
		strcpy(m_szErrMsg, "不支持此格式报文");
		return -1;
	}
	//API回调返回结构是按TTRspMsg、TDeferOrder顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TDeferOrder*)((char*)in_pszInMsg + sizeof(TTRspMsg));


	//1、将API回报的结构TTRspMsg和TDeferOrder转化为向平台发送的请求的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//in_lConvertFlag需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文，现在只做0方式



	//1.1转换交易代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, in_pszTrCode, strlen(in_pszTrCode));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//1.2转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//1.3转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.1转换报单号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->orderNo, strlen(pstUsrData->orderNo));
	lLen = 16;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.2转换客户代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->clientID, strlen(pstUsrData->clientID));
	lLen = 12;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换客户代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.3转换合约代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.4转换会员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->memberID, strlen(pstUsrData->memberID));
	lLen = 6;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换会员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.5转换买卖方向
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->buyOrSell;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买卖方向错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.6转换申报日期
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->applyDate, strlen(pstUsrData->applyDate));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换申报日期错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.7转换申报时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->applyTime, strlen(pstUsrData->applyTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换申报时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.8转换开平仓标志
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->offSetFlag;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换开平仓标志错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.9转换价格
	memset(szTmp, 32, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->price, 2);
	sprintf(szTmp, "%-17f", pstUsrData->price);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换价格错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.10转换数量
	memset(szTmp, 32, sizeof(szTmp));
	sprintf(szTmp, "%-10d", pstUsrData->amount);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.11转换剩余数量
	memset(szTmp, 32, sizeof(szTmp));
	sprintf(szTmp, "%-10d", pstUsrData->remainAmount);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换剩余数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.12转换撤销时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->cancelTime, strlen(pstUsrData->cancelTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换撤销时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.13转换状态
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->status;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换状态错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.14转换本地报单号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->localOrderNo, strlen(pstUsrData->localOrderNo));
	lLen = 14;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换本地报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.15转换保证金率
	memset(szTmp, 32, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->margin, 2);
	sprintf(szTmp, "%-17f", pstUsrData->margin);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换保证金率错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.16转换保证金类型
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->marginType;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换保证金类型错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.17转换交易员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->traderID, strlen(pstUsrData->traderID));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.18转换撤消员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->cancelID, strlen(pstUsrData->cancelID));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换撤消员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.19转换类型
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->matchType;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换类型错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	



	//成功返回转化后的字节数，失败返回-1
	lRet = lPos;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_OnRecvRtnMiddleAppOrderCancel
* 功能描述：RspInitiative将现货延期中立仓申报撤单回报API报文转化为平台请求报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- in_pszTrCode      输入 --交易代码
*- in_lConvertFlag      输入 --需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei  于 20091027创建
************************************************************/

long CGTAMsgConvert::rl_OnRecvRtnMiddleAppOrderCancel(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag)
{
	long	lRet = -1,	lLen;
	long	lPos = 0;
	TTRspMsg *pstTRspMsg;
	TMiddleAppOrder *pstUsrData;
	char szTmp[512];

	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3

	


	//有效性检查
	if (NULL == in_pszInMsg || NULL == in_pszTrCode)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (((long)sizeof(TInstState) + (long)strlen(in_pszTrCode)) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "传入输出报文空间不够");
		return -1;
	}
	//转化标志检查
	if (1 == in_lConvertFlag)
	{
		strcpy(m_szErrMsg, "不支持此格式报文");
		return -1;
	}
	//API回调返回结构是按TTRspMsg、TMiddleAppOrder顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TMiddleAppOrder*)((char*)in_pszInMsg + sizeof(TTRspMsg));


	//1、将API回报的结构TTRspMsg和TMiddleAppOrder转化为向平台发送的请求的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//in_lConvertFlag需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文，现在只做0方式



	//1.1转换交易代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, in_pszTrCode, strlen(in_pszTrCode));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//1.2转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//1.3转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.1转换报单号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->orderNo, strlen(pstUsrData->orderNo));
	lLen = 16;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.2转换客户代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->clientID, strlen(pstUsrData->clientID));
	lLen = 12;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换客户代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.3转换合约代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.4转换会员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->memberID, strlen(pstUsrData->memberID));
	lLen = 6;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换会员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.5转换买卖方向
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->buyOrSell;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买卖方向错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.6转换申报日期
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->applyDate, strlen(pstUsrData->applyDate));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换申报日期错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.7转换申报时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->applyTime, strlen(pstUsrData->applyTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换申报时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.8转换数量
	memset(szTmp, 32, sizeof(szTmp));
	sprintf(szTmp, "%-10d", pstUsrData->amount);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.9转换撤销时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->cancelTime, strlen(pstUsrData->cancelTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换撤销时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.10转换状态
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->status;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换状态错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.11转换本地报单号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->localOrderID, strlen(pstUsrData->localOrderID));
	lLen = 14;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换本地报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}






	//2.12转换交易员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->traderID, strlen(pstUsrData->traderID));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.13转换撤消员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->cancelID, strlen(pstUsrData->cancelID));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换撤消员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.14转换剩余数量
	memset(szTmp, 32, sizeof(szTmp));
	sprintf(szTmp, "%-10d", pstUsrData->remainAmount);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换剩余数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	



	//成功返回转化后的字节数，失败返回-1
	lRet = lPos;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_OnRecvRtnDeferDeliveryAppOrderCancel
* 功能描述：RspInitiative将现货延期交收交割申报撤单回报API报文转化为平台请求报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- in_pszTrCode      输入 --交易代码
*- in_lConvertFlag      输入 --需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei  于 20091029创建
************************************************************/

long CGTAMsgConvert::rl_OnRecvRtnDeferDeliveryAppOrderCancel(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag)
{
	long	lRet = -1,	lLen;
	long	lPos = 0;
	TTRspMsg *pstTRspMsg;
	TDeferDeliveryAppOrder *pstUsrData;
	char szTmp[512];

	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3

	


	//有效性检查
	if (NULL == in_pszInMsg || NULL == in_pszTrCode)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (((long)sizeof(TInstState) + (long)strlen(in_pszTrCode)) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "传入输出报文空间不够");
		return -1;
	}
	//转化标志检查
	if (1 == in_lConvertFlag)
	{
		strcpy(m_szErrMsg, "不支持此格式报文");
		return -1;
	}
	//API回调返回结构是按TTRspMsg、TDeferDeliveryAppOrder顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TDeferDeliveryAppOrder*)((char*)in_pszInMsg + sizeof(TTRspMsg));


	//1、将API回报的结构TTRspMsg和TDeferDeliveryAppOrder转化为向平台发送的请求的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//in_lConvertFlag需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文，现在只做0方式



	//1.1转换交易代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, in_pszTrCode, strlen(in_pszTrCode));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//1.2转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//1.3转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.1转换报单号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->orderNo, strlen(pstUsrData->orderNo));
	lLen = 16;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.2转换客户代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->clientID, strlen(pstUsrData->clientID));
	lLen = 12;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换客户代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.3转换合约代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.4转换买卖方向
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->buyOrSell;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买卖方向错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.5转换申报日期
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->applyDate, strlen(pstUsrData->applyDate));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换申报日期错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.6转换申报时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->applyTime, strlen(pstUsrData->applyTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换申报时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.7转换数量
	memset(szTmp, 32, sizeof(szTmp));
	sprintf(szTmp, "%-10d", pstUsrData->amount);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.8转换撤销时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->cancelTime, strlen(pstUsrData->cancelTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换撤销时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.9转换状态
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->status;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换状态错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.10转换本地报单号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->localOrderID, strlen(pstUsrData->localOrderID));
	lLen = 14;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换本地报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	


	//2.11转换会员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->memberID, strlen(pstUsrData->memberID));
	lLen = 6;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换会员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	


	//2.12转换交易员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->traderID, strlen(pstUsrData->traderID));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.13转换撤消员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->cancelID, strlen(pstUsrData->cancelID));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换撤消员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.14转换剩余数量
	memset(szTmp, 32, sizeof(szTmp));
	sprintf(szTmp, "%-10d", pstUsrData->remainAmount);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换剩余数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	



	//成功返回转化后的字节数，失败返回-1
	lRet = lPos;
	return lRet;
}


/**********************************************************
* 函 数 名： CTrMsgConvert::rl_OnRecvRtnSpotMatch
* 功能描述：RspInitiative将现货成交回报API报文转化为平台请求报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- in_pszTrCode      输入 --交易代码
*- in_lConvertFlag      输入 --需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：ukyo  于 20071014创建
************************************************************/

long CGTAMsgConvert::rl_OnRecvRtnSpotMatch(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag)
{
	long		lRet = -1,	lLen;
	long		lPos = 0;
	TTRspMsg *pstTRspMsg;
	TSpotMatch *pstUsrData;
	char szTmp[512];

	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3

	


	//有效性检查
	if (NULL == in_pszInMsg || NULL == in_pszTrCode)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}

	if (((long)sizeof(TInstState) + (long)strlen(in_pszTrCode)) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "传入输出报文空间不够");
		return -1;
	}
	//转化标志检查
	if (1 == in_lConvertFlag)
	{
		strcpy(m_szErrMsg, "不支持此格式报文");
		return -1;
	}
	//API回调返回结构是按TTRspMsg、TSpotMatch顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TSpotMatch*)((char*)in_pszInMsg + sizeof(TTRspMsg));


	//1、将API回报的结构TTRspMsg和TSpotMatch转化为向平台发送的请求的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//in_lConvertFlag需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文，现在只做0方式



	//1.1转换交易代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, in_pszTrCode, strlen(in_pszTrCode));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//1.2转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//1.3转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.1转换成交编号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->matchNo, strlen(pstUsrData->matchNo));
	lLen = 16;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换成交编号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.2转换买卖方向
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->buyOrSell;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买卖方向错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.3转换客户代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->clientID, strlen(pstUsrData->clientID));
	lLen = 12;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换客户代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.4转换客户类型
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->clientType;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换客户类型错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.5转换会员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->memberID, strlen(pstUsrData->memberID));
	lLen = 6;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换会员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.6转换合约代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.6转换成交日期
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->matchDate, strlen(pstUsrData->matchDate));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换成交日期错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.7转换成交时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->matchTime, strlen(pstUsrData->matchTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换成交时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.8转换价格
	memset(szTmp, 32, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->price, 2);
	sprintf(szTmp, "%-17f", pstUsrData->price);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换价格错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.9转换数量
	memset(szTmp, 32, sizeof(szTmp));
	sprintf(szTmp, "%-10d", pstUsrData->volume);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.10转换系统报单号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->orderNo, strlen(pstUsrData->orderNo));
	lLen = 14;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换系统报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.11转换报单本地编号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->localOrderNo, strlen(pstUsrData->localOrderNo));
	lLen = 14;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换报单本地编号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.12转换类型
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->matchType;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换类型错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	



	//成功返回转化后的字节数，失败返回-1
	lRet = lPos;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_OnRecvRtnDeferMatch
* 功能描述：RspInitiative将现货延期成交回报API报文转化为平台请求报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- in_pszTrCode      输入 --交易代码
*- in_lConvertFlag      输入 --需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei  于 20091016创建
************************************************************/

long CGTAMsgConvert::rl_OnRecvRtnDeferMatch(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag)
{
	long		lRet = -1,	lLen;
	long		lPos = 0;
	TTRspMsg *pstTRspMsg;
	TDeferMatch *pstUsrData;
	char szTmp[512];

	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3

	


	//有效性检查
	if (NULL == in_pszInMsg || NULL == in_pszTrCode)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}

	if (((long)sizeof(TInstState) + (long)strlen(in_pszTrCode)) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "传入输出报文空间不够");
		return -1;
	}
	//转化标志检查
	if (1 == in_lConvertFlag)
	{
		strcpy(m_szErrMsg, "不支持此格式报文");
		return -1;
	}
	//API回调返回结构是按TTRspMsg、TDeferMatch顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TDeferMatch*)((char*)in_pszInMsg + sizeof(TTRspMsg));


	//1、将API回报的结构TTRspMsg和TDeferMatch转化为向平台发送的请求的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//in_lConvertFlag需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文，现在只做0方式

	

	//1.1转换交易代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, in_pszTrCode, strlen(in_pszTrCode));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//1.2转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//1.3转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.1转换成交编号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->matchNo, strlen(pstUsrData->matchNo));
	lLen = 16;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换成交编号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.2转换买卖方向
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->buyOrSell;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买卖方向错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.3转换客户代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->clientID, strlen(pstUsrData->clientID));
	lLen = 12;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换客户代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.4转换客户类型
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->clientType;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换客户类型错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.5转换合约代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.6转换会员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->memberID, strlen(pstUsrData->memberID));
	lLen = 6;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换会员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.7转换成交日期
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->matchDate, strlen(pstUsrData->matchDate));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换成交日期错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.8转换成交时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->matchTime, strlen(pstUsrData->matchTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换成交时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.9转换价格
	memset(szTmp, 32, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->price, 2);
	sprintf(szTmp, "%-17f", pstUsrData->price);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换价格错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.10转换数量
	memset(szTmp, 32, sizeof(szTmp));
	sprintf(szTmp, "%-10d", pstUsrData->volume);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.11转换开平标志
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->offsetFlag;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换开平标志错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.12转换系统报单号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->orderNo, strlen(pstUsrData->orderNo));
	lLen = 14;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换系统报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.13转换报单本地编号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->localOrderNo, strlen(pstUsrData->localOrderNo));
	lLen = 14;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换报单本地编号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	



	//成功返回转化后的字节数，失败返回-1
	lRet = lPos;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_OnRecvRtnDeferDeliveryAppOrderMatch
* 功能描述：RspInitiative将现货延期交收交割成交回报API报文转化为平台请求报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- in_pszTrCode      输入 --交易代码
*- in_lConvertFlag      输入 --需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei  于 20091029创建
************************************************************/

long CGTAMsgConvert::rl_OnRecvRtnDeferDeliveryAppOrderMatch(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag)
{
	long		lRet = -1,	lLen;
	long		lPos = 0;
	TTRspMsg *pstTRspMsg;
	TDeferDeliveryAppMatch *pstUsrData;
	char szTmp[512];

	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3

	


	//有效性检查
	if (NULL == in_pszInMsg || NULL == in_pszTrCode)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}

	if (((long)sizeof(TInstState) + (long)strlen(in_pszTrCode)) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "传入输出报文空间不够");
		return -1;
	}
	//转化标志检查
	if (1 == in_lConvertFlag)
	{
		strcpy(m_szErrMsg, "不支持此格式报文");
		return -1;
	}
	//API回调返回结构是按TTRspMsg、TDeferDeliveryAppMatch顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TDeferDeliveryAppMatch*)((char*)in_pszInMsg + sizeof(TTRspMsg));


	//1、将API回报的结构TTRspMsg和TDeferDeliveryAppMatch转化为向平台发送的请求的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//in_lConvertFlag需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文，现在只做0方式



	//1.1转换交易代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, in_pszTrCode, strlen(in_pszTrCode));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//1.2转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//1.3转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.1转换成交编号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->matchNo, strlen(pstUsrData->matchNo));
	lLen = 16;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换成交编号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.2转换客户代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->clientID, strlen(pstUsrData->clientID));
	lLen = 12;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换客户代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.3转换合约代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.4转换会员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->memberID, strlen(pstUsrData->memberID));
	lLen = 6;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换会员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.5转换买卖方向
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->buyOrSell;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买卖方向错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.6转换数量
	memset(szTmp, 32, sizeof(szTmp));
	sprintf(szTmp, "%-10d", pstUsrData->volume);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.7转换成交日期
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->matchDate, strlen(pstUsrData->matchDate));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换成交日期错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.8转换成交时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->matchTime, strlen(pstUsrData->matchTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换成交时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.9转换系统报单号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->orderNo, strlen(pstUsrData->orderNo));
	lLen = 14;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换系统报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.10转换报单本地编号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->LocalOrderNo, strlen(pstUsrData->LocalOrderNo));
	lLen = 14;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换报单本地编号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.11转换中立仓标志
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->middleFlag;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换中立仓标志错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}
	if (szTmp[0]=='1')
		m_lFlag = 1;





	



	//成功返回转化后的字节数，失败返回-1
	lRet = lPos;
	return lRet;
}


/**********************************************************
* 函 数 名： CTrMsgConvert::rl_OnRecvMarketInfo
* 功能描述：RspInitiative将市场信息回报API报文转化为平台请求报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- in_pszTrCode      输入 --交易代码
*- in_lConvertFlag      输入 --需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：ukyo  于 20071014创建
************************************************************/

long CGTAMsgConvert::rl_OnRecvMarketInfo(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag)
{
	long lRet = -1, lLen;
	long lPos=0;
	TTRspMsg * pstTRspMsg;
	TMarket *pstUsrData;

	char szTmp[512];


	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3

	


	//有效性检查
	if (NULL == in_pszInMsg || NULL == in_pszTrCode)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (((long)sizeof(TInstState) + (long)strlen(in_pszTrCode)) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "传入输出报文空间不够");
		return -1;
	}
	//转化标志检查
	if (1 == in_lConvertFlag)
	{
		strcpy(m_szErrMsg, "不支持此格式报文");
		return -1;
	}
	//API回调返回结构是按TTRspMsg、TMarket顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TMarket*)((char*)in_pszInMsg + sizeof(TTRspMsg));

	//1、将API回报的结构TTRspMsg和TMarket转化为向平台发送的请求的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//in_lConvertFlag需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文，现在只做0方式


	//成功返回转化后的字节数，失败返回-1



	//1.1转换交易代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, in_pszTrCode, strlen(in_pszTrCode));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}

	//1.2转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//	 {
	//	 }
	//1.3转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	

	//2.1转换市场代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->marketID, strlen(pstUsrData->marketID));
	lLen = 2;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换市场代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.2转换全称
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->name, strlen(pstUsrData->name));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换全称错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.3转换类型
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->type;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换类型错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.3转换活跃标志
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->openFlag;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换活跃标志错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.3转换市场交易状态
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->marketState;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换市场交易状态错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	


	



	//成功返回转化后的字节数，失败返回-1
	lRet = lPos;
	return lRet;
}


/**********************************************************
* 函 数 名： CTrMsgConvert::rl_OnRecvDeferInstInfo
* 功能描述：RspInitiative将递延合约信息回报API报文转化为平台请求报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- in_pszTrCode      输入 --交易代码
*- in_lConvertFlag      输入 --需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei于 20100118创建
************************************************************/

long CGTAMsgConvert::rl_OnRecvDeferInstInfo(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag)
{
	long	lRet = -1,	lLen;
	long	lPos = 0;
	TTRspMsg *pstTRspMsg;
	TDeferInst *pstUsrData;
	char szTmp[512];

	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3
	if (m_blNtfWriteLogFlag)
	{}

	//有效性检查
	if (NULL == in_pszInMsg || NULL == in_pszTrCode )
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (((long)sizeof(TInstState) + (long)strlen(in_pszTrCode)) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "传入输出报文空间不够");
		return -1;
	}
	//转化标志检查
	if (1 == in_lConvertFlag)
	{
		strcpy(m_szErrMsg, "不支持此格式报文");
		return -1;
	}
	//API回调返回结构是按TTRspMsg、TDeferInst顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TDeferInst*)((char*)in_pszInMsg + sizeof(TTRspMsg));


	//1、将API回报的结构TTRspMsg和TDeferInst转化为向平台发送的请求的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//in_lConvertFlag需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文，现在只做0方式
	if (m_blNtfWriteLogFlag)
	{}
	//1.1转换交易代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, in_pszTrCode, strlen(in_pszTrCode));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}

	//1.2转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}

	//1.3转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}
	//2.1转换合约代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}

	//2.2转换合约名称
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->name, strlen(pstUsrData->name));
	lLen = 12;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约名称错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}


	//2.3转换市场代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->marketID, strlen(pstUsrData->marketID));
	lLen = 2;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换市场代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}

	//2.4转换品种类型
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->varietyType;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换品种类型错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}

	//2.5转换交割品种代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->varietyID, strlen(pstUsrData->varietyID));
	lLen = 3;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交割品种代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}

	//2.6转换交易单位数量
	memset(szTmp, 32, sizeof(szTmp));
	sprintf(szTmp, "%-10d", pstUsrData->unit);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易单位数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}

	//2.7转换最小变动价位
	memset(szTmp, 32, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->tick, 2);
	sprintf(szTmp, "%-17f", pstUsrData->tick);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换最小变动价位错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}

	//2.8转换最大申报限量(单手)
	memset(szTmp, 32, sizeof(szTmp));
	sprintf(szTmp, "%-10d", pstUsrData->maxHand);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换最大申报限量(单手)错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}

	//2.9转换最小申报限量
	memset(szTmp, 32, sizeof(szTmp));
	sprintf(szTmp, "%-10d", pstUsrData->minHand);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换最小申报限量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}
	//2.10转换涨停板率
	memset(szTmp, 32, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->upperLimit, 2);
	sprintf(szTmp, "%-17f", pstUsrData->upperLimit);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换涨停板率错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}

	//2.11转换跌停板率
	memset(szTmp, 32, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->lowerLimit, 2);
	sprintf(szTmp, "%-17f", pstUsrData->lowerLimit);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换跌停板率错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}

	//2.12转换合约标志
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->openFlag;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约标志错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}

	//2.12转换合约交易状态
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->tradeState;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约交易状态标志错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}
	//2.13转换参考价
	memset(szTmp, 32, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->refPrice, 2);
	sprintf(szTmp, "%-17f", pstUsrData->refPrice);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换产考价错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}



	
	if (m_blNtfWriteLogFlag)
	{}
	//成功返回转化后的字节数，失败返回-1
	lRet = lPos;
	return lRet;

}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_OnRecvSpotInstInfo
* 功能描述：RspInitiative将现货合约信息回报API报文转化为平台请求报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- in_pszTrCode      输入 --交易代码
*- in_lConvertFlag      输入 --需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：ukyo  于 20071014创建
************************************************************/

long CGTAMsgConvert::rl_OnRecvSpotInstInfo(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag)
{
	long	lRet = -1,	lLen;
	long	lPos = 0;
	TTRspMsg *pstTRspMsg;
	TSpotInst *pstUsrData;
	char szTmp[512];

	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3

	


	//有效性检查
	if (NULL == in_pszInMsg || NULL == in_pszTrCode )
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (((long)sizeof(TInstState) + (long)strlen(in_pszTrCode)) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "传入输出报文空间不够");
		return -1;
	}
	//转化标志检查
	if (1 == in_lConvertFlag)
	{
		strcpy(m_szErrMsg, "不支持此格式报文");
		return -1;
	}
	//API回调返回结构是按TTRspMsg、TSpotInst顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TSpotInst*)((char*)in_pszInMsg + sizeof(TTRspMsg));


	//1、将API回报的结构TTRspMsg和TSpotInst转化为向平台发送的请求的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//in_lConvertFlag需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文，现在只做0方式



	//1.1转换交易代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, in_pszTrCode, strlen(in_pszTrCode));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//1.2转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	


	//1.3转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	

	//2.1转换合约代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.2转换合约名称
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->name, strlen(pstUsrData->name));
	lLen = 12;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约名称错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}






	//2.3转换市场代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->marketID, strlen(pstUsrData->marketID));
	lLen = 2;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换市场代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.4转换品种类型
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->varietyType;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换品种类型错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	


	//2.5转换交割品种代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->varietyID, strlen(pstUsrData->varietyID));
	lLen = 3;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交割品种代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.6转换交易单位数量
	memset(szTmp, 32, sizeof(szTmp));
	sprintf(szTmp, "%-10d", pstUsrData->unit);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易单位数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.7转换最小变动价位
	memset(szTmp, 32, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->tick, 2);
	sprintf(szTmp, "%-17f", pstUsrData->tick);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换最小变动价位错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.8转换最大申报限量(单手)
	memset(szTmp, 32, sizeof(szTmp));
	sprintf(szTmp, "%-10d", pstUsrData->maxHand);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换最大申报限量(单手)错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.9转换最小申报限量
	memset(szTmp, 32, sizeof(szTmp));
	sprintf(szTmp, "%-10d", pstUsrData->minHand);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换最小申报限量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.10转换涨停板率
	memset(szTmp, 32, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->upperLimit, 2);
	sprintf(szTmp, "%-17f", pstUsrData->upperLimit);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换涨停板率错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.11转换跌停板率
	memset(szTmp, 32, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->lowerLimit, 2);
	sprintf(szTmp, "%-17f", pstUsrData->lowerLimit);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换跌停板率错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.12转换合约标志
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->openFlag;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约标志错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.12转换合约交易状态
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->tradeState;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约交易状态标志错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.13转换产考价
	memset(szTmp, 32, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->refPrice, 2);
	sprintf(szTmp, "%-17f", pstUsrData->refPrice);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换产考价错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.14转换卖方收款比例
	memset(szTmp, 32, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->recvRate, 2);
	sprintf(szTmp, "%-17f", pstUsrData->recvRate);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换卖方收款比例错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	



	//成功返回转化后的字节数，失败返回-1
	lRet = lPos;
	return lRet;

}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_OnRecvRtnSpotInstStateUpdate
* 功能描述：RspInitiative将现货合约交易状态改变回报API报文转化为平台请求报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- in_pszTrCode      输入 --交易代码
*- in_lConvertFlag      输入 --需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：ukyo  于 20071014创建
************************************************************/

long CGTAMsgConvert::rl_OnRecvRtnSpotInstStateUpdate(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag)
{
	long	lRet = -1,	lLen;
	long	lPos = 0;
	TTRspMsg *pstTRspMsg;
	TInstState *pstUsrData;
	char szTmp[512];



	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3
	if (m_blNtfWriteLogFlag)
	{}

	//有效性检查
	if (NULL == in_pszInMsg || NULL == in_pszTrCode)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (((long)sizeof(TInstState) + (long)strlen(in_pszTrCode)) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "传入输出报文空间不够");
		return -1;
	}
	//转化标志检查
	if (1 == in_lConvertFlag)
	{
		strcpy(m_szErrMsg, "不支持此格式报文");
		return -1;
	}
	//API回调返回结构是按TTRspMsg、TInstState 顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TInstState *)((char*)in_pszInMsg + sizeof(TTRspMsg));


	//1、将API回报的结构TTRspMsg和TInstState 转化为向平台发送的请求的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//in_lConvertFlag需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文，现在只做0方式
	if (m_blNtfWriteLogFlag)
	{}
	//1.1转换交易代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, in_pszTrCode, strlen(in_pszTrCode));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}

	//1.2转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}
	//1.3转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}
	//2.1转换合约代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}
	//2.2转换合约交易状态
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->tradeState;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约交易状态错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}


	
	if (m_blNtfWriteLogFlag)
	{}
	//成功返回转化后的字节数，失败返回-1
	lRet = lPos;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_OnRecvRtnDeferInstStateUpdate
* 功能描述：RspInitiative将递延合约交易状态改变回报API报文转化为平台请求报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- in_pszTrCode      输入 --交易代码
*- in_lConvertFlag      输入 --需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei于 20090118创建
************************************************************/

long CGTAMsgConvert::rl_OnRecvRtnDeferInstStateUpdate(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag)
{
	long	lRet = -1,	lLen;
	long	lPos = 0;
	TTRspMsg *pstTRspMsg;
	TInstState *pstUsrData;
	char szTmp[512];

	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3
	if (m_blNtfWriteLogFlag)
	{}

	//有效性检查
	if (NULL == in_pszInMsg || NULL == in_pszTrCode)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (((long)sizeof(TInstState) + (long)strlen(in_pszTrCode)) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "传入输出报文空间不够");
		return -1;
	}
	//转化标志检查
	if (1 == in_lConvertFlag)
	{
		strcpy(m_szErrMsg, "不支持此格式报文");
		return -1;
	}
	//API回调返回结构是按TTRspMsg、TInstState 顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TInstState *)((char*)in_pszInMsg + sizeof(TTRspMsg));


	//1、将API回报的结构TTRspMsg和TInstState 转化为向平台发送的请求的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//in_lConvertFlag需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文，现在只做0方式
	if (m_blNtfWriteLogFlag)
	{}
	//1.1转换交易代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, in_pszTrCode, strlen(in_pszTrCode));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}

	//1.2转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}
	//1.3转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}
	//2.1转换合约代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}
	//2.2转换合约交易状态
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->tradeState;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约交易状态错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}

	
	if (m_blNtfWriteLogFlag)
	{}
	//成功返回转化后的字节数，失败返回-1
	lRet = lPos;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_OnRecvRtnSpotMarketStateUpdate
* 功能描述：RspInitiative将现货市场交易状态改变回报API报文转化为平台请求报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- in_pszTrCode      输入 --交易代码
*- in_lConvertFlag      输入 --需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：ukyo  于 20071014创建
************************************************************/

long CGTAMsgConvert::rl_OnRecvRtnSpotMarketStateUpdate(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag)
{
	long	lRet = -1,	lLen;
	long	lPos = 0;
	TTRspMsg *pstTRspMsg;
	TMarketState *pstUsrData;
	char szTmp[512];



	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3

	


	//有效性检查
	if (NULL == in_pszInMsg || NULL == in_pszTrCode)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (((long)sizeof(TInstState) + (long)strlen(in_pszTrCode)) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "传入输出报文空间不够");
		return -1;
	}
	//转化标志检查
	if (1 == in_lConvertFlag)
	{
		strcpy(m_szErrMsg, "不支持此格式报文");
		return -1;
	}
	//API回调返回结构是按TTRspMsg、TMarketState 顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TMarketState *)((char*)in_pszInMsg + sizeof(TTRspMsg));


	//1、将API回报的结构TTRspMsg和TMarketState 转化为向平台发送的请求的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//in_lConvertFlag需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文，现在只做0方式



	//1.1转换交易代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, in_pszTrCode, strlen(in_pszTrCode));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//1.2转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//1.3转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.1转换市场代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->marketID, strlen(pstUsrData->marketID));
	lLen = 2;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换市场代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.2转换市场交易状态
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->marketState;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换市场交易状态错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}






	



	//成功返回转化后的字节数，失败返回-1
	lRet = lPos;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_OnRecvRtnSpotMarketStateUpdate
* 功能描述：RspInitiative将发布现货延期交收补偿费率通知API报文转化为平台请求报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- in_pszTrCode      输入 --交易代码
*- in_lConvertFlag      输入 --需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei于 20100119创建
************************************************************/

long CGTAMsgConvert::rl_OnRecvDeferFeeRateUpdate(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag)
{
	long	lRet = -1,	lLen;
	long	lPos = 0;
	TTRspMsg *pstTRspMsg;
	TDeferFeeRate *pstUsrData;
	char szTmp[512];

	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3
	if (m_blNtfWriteLogFlag)
	{}

	//有效性检查
	if (NULL == in_pszInMsg || NULL == in_pszTrCode)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (((long)sizeof(TDeferFeeRate) + (long)strlen(in_pszTrCode)) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "传入输出报文空间不够");
		return -1;
	}
	//转化标志检查
	if (1 == in_lConvertFlag)
	{
		strcpy(m_szErrMsg, "不支持此格式报文");
		return -1;
	}
	//API回调返回结构是按TTRspMsg、TMarketState 顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TDeferFeeRate *)((char*)in_pszInMsg + sizeof(TTRspMsg));


	//1、将API回报的结构TTRspMsg和TDeferFeeRate 转化为向平台发送的请求的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//in_lConvertFlag需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文，现在只做0方式



	//1.1转换交易代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, in_pszTrCode, strlen(in_pszTrCode));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)



		//1.2转换响应代码
		memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)


		//1.3转换响应消息
		memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)


		//2.1转换日期
		memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->effectDate, strlen(pstUsrData->effectDate));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换日期错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	

	//2.2转换合约代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	
	//2.3转换支付方向
	lLen = 1;
	out_pszOutMsg[lPos] = pstUsrData->payDirection;
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换支付方向错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	

	//2.4转换递延费率
	memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->feeRate, 5);
	sprintf(szTmp, "%-17f", pstUsrData->feeRate);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换递延费率错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	
//成功返回转化后的字节数，失败返回-1
	lRet = lPos;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_OnRecvRtnDeferMarketStateUpdate
* 功能描述：RspInitiative将递延市场交易状态改变回报API报文转化为平台请求报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- in_pszTrCode      输入 --交易代码
*- in_lConvertFlag      输入 --需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei  于 20100118创建
************************************************************/

long CGTAMsgConvert::rl_OnRecvRtnDeferMarketStateUpdate(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag)
{
	long	lRet = -1,	lLen;
	long	lPos = 0;
	TTRspMsg *pstTRspMsg;
	TMarketState *pstUsrData;
	char szTmp[512];

	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3

	//有效性检查
	if (NULL == in_pszInMsg || NULL == in_pszTrCode)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (((long)sizeof(TInstState) + (long)strlen(in_pszTrCode)) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "传入输出报文空间不够");
		return -1;
	}
	//转化标志检查
	if (1 == in_lConvertFlag)
	{
		strcpy(m_szErrMsg, "不支持此格式报文");
		return -1;
	}
	//API回调返回结构是按TTRspMsg、TMarketState 顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TMarketState *)((char*)in_pszInMsg + sizeof(TTRspMsg));


	//1、将API回报的结构TTRspMsg和TMarketState 转化为向平台发送的请求的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//in_lConvertFlag需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文，现在只做0方式



	//1.1转换交易代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, in_pszTrCode, strlen(in_pszTrCode));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//1.2转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//1.3转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.1转换市场代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->marketID, strlen(pstUsrData->marketID));
	lLen = 2;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换市场代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.2转换市场交易状态
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->marketState;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换市场交易状态错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	



	



	//成功返回转化后的字节数，失败返回-1
	lRet = lPos;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_OnRecvRtnBulletinBoard
* 功能描述：RspInitiative将公告回报API报文转化为平台请求报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- in_pszTrCode      输入 --交易代码
*- in_lConvertFlag      输入 --需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：ukyo  于 20071014创建
************************************************************/

long CGTAMsgConvert::rl_OnRecvRtnBulletinBoard(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag)
{
	long	lRet = -1,	lLen;
	long	lPos = 0;
	TTRspMsg * pstTRspMsg;
	TBulletinBoard  *pstUsrData;
	char szTmp[512];

	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3
	if (m_blNtfWriteLogFlag)
	{}

	//有效性检查
	if (NULL == in_pszInMsg || NULL == in_pszTrCode)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (((long)sizeof(TInstState) + (long)strlen(in_pszTrCode)) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "传入输出报文空间不够");
		return -1;
	}
	//转化标志检查
	if (1 == in_lConvertFlag)
	{
		strcpy(m_szErrMsg, "不支持此格式报文");
		return -1;
	}
	//API回调返回结构是按TTRspMsg、TBulletinBoard 顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TBulletinBoard *)((char*)in_pszInMsg + sizeof(TTRspMsg));

	//1、将API回报的结构TTRspMsg和TBulletinBoard 转化为向平台发送的请求的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//in_lConvertFlag需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文，
	if (m_blNtfWriteLogFlag)
	{}
	//1.1转换交易代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, in_pszTrCode, strlen(in_pszTrCode));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	
	//1.2转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}
	//1.3转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}
	//2.1转换会员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->memberID, strlen(pstUsrData->memberID));
	lLen = 6;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换会员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}
//2.2转换序号
	memset(szTmp, 32, sizeof(szTmp));
	sprintf(szTmp, "%-10d", pstUsrData->seqNo);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换序号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	
	//2.3转换发布日期
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->announceDate, strlen(pstUsrData->announceDate));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换发布日期错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	
	//2.4转换发布时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->announceTime, strlen(pstUsrData->announceTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换发布时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	
	//2.5转换部门代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->deptID, strlen(pstUsrData->deptID));
	lLen = 4;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换部门代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	
	//2.6转换标题
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->title, strlen(pstUsrData->title));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换标题错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	
	//2.7转换内容
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->content, strlen(pstUsrData->content));
	lLen = 254;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换内容错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	
	//2.8转换发布人
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->announcer, strlen(pstUsrData->announcer));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换发布人错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	

	
	
	//成功返回转化后的字节数，失败返回-1
	lRet = lPos;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_OnRecvRtnInterQuotation
* 功能描述：RspInitiative将国际行情回报API报文转化为平台请求报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- in_pszTrCode      输入 --交易代码
*- in_lConvertFlag      输入 --需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：ukyo  于 20071014创建
************************************************************/

long CGTAMsgConvert::rl_OnRecvRtnInterQuotation(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag)
{
	long	lRet = -1,	lLen;
	long	lPos = 0;
	TTRspMsg *pstTRspMsg;
	TInterQuotation  *pstUsrData;
	char szTmp[512];



	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3
	

	//有效性检查
	if (NULL == in_pszInMsg || NULL == in_pszTrCode)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (((long)sizeof(TInstState) + (long)strlen(in_pszTrCode)) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "传入输出报文空间不够");
		return -1;
	}
	//转化标志检查
	if (1 == in_lConvertFlag)
	{
		strcpy(m_szErrMsg, "不支持此格式报文");
		return -1;
	}
	//API回调返回结构是按TTRspMsg、TInterQuotation 顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TInterQuotation *)((char*)in_pszInMsg + sizeof(TTRspMsg));


	//1、将API回报的结构TTRspMsg和TInterQuotation 转化为向平台发送的请求的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//in_lConvertFlag需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文，现在只做0方式
	
	//1.1转换交易代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, in_pszTrCode, strlen(in_pszTrCode));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	

	//1.2转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	
	//1.3转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	
	//2.1转换内容
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->content, strlen(pstUsrData->content));
	lLen = 254;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换内容错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	
	//2.2转换发布人
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->announcer, strlen(pstUsrData->announcer));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换发布人错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	
	//2.3转换发布时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->announceTime, strlen(pstUsrData->announceTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换发布时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	
	//2.4转换发布日期
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->announceDate, strlen(pstUsrData->announceDate));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换发布日期错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}


	
	if (m_blNtfWriteLogFlag)
	{}
	//成功返回转化后的字节数，失败返回-1
	lRet = lPos;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_OnRecvSpotQuotation
* 功能描述：RspInitiative将现货行情回报API报文转化为平台请求报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- in_pszTrCode      输入 --交易代码
*- in_lConvertFlag      输入 --需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：ukyo  于 20071014创建
************************************************************/

long CGTAMsgConvert::rl_OnRecvSpotQuotation(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag)
{
	long	lRet = -1,	lLen;
	long lPos = 0;
	TTRspMsg *pstTRspMsg;
	TSpotQuotation *pstUsrData;

	char szTmp[512];


	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3
	if (m_blNtfWriteLogFlag)
	{}

	//有效性检查
	if (NULL == in_pszInMsg || NULL == in_pszTrCode)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (((long)sizeof(TInstState) + (long)strlen(in_pszTrCode)) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "传入输出报文空间不够");
		return -1;
	}
	//转化标志检查
	if (1 == in_lConvertFlag)
	{
		strcpy(m_szErrMsg, "不支持此格式报文");
		return -1;
	}
	//API回调返回结构是按TTRspMsg、TSpotQuotation 顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TSpotQuotation *)((char*)in_pszInMsg + sizeof(TTRspMsg));


	//1、将API回报的结构TTRspMsg和TSpotQuotation 转化为向平台发送的请求的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//in_lConvertFlag需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文，现在只做0方式
	if (m_blNtfWriteLogFlag)
	{}
	//1.1转换交易代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, in_pszTrCode, strlen(in_pszTrCode));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}

	//1.2转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}

	//1.3转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}
	//2.1转换合约代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}


	//2.2转换合约名称
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->name, strlen(pstUsrData->name));
	lLen = 12;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约名称错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.3转换昨收盘
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->lastClose, 2);
	sprintf(szTmp, "%-17f", pstUsrData->lastClose);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换昨收盘错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.4转换开盘价
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->open, 2);
	sprintf(szTmp, "%-17f", pstUsrData->open);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换开盘价错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.5转换最高价
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->high, 2);
	sprintf(szTmp, "%-17f", pstUsrData->high);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换最高价错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.6转换最低价
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->low, 2);
	sprintf(szTmp, "%-17f", pstUsrData->low);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换最低价错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.7转换最新价
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->last, 2);
	sprintf(szTmp, "%-17f", pstUsrData->last);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换最新价错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.8转换收盘价
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->close, 2);
	sprintf(szTmp, "%-17f", pstUsrData->close);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换收盘价错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.9转换买价1
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->bid1, 2);
	sprintf(szTmp, "%-17f", pstUsrData->bid1);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买价1错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.10转换买量1
		memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->bidLot1);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买量1错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.11转换买价2
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->bid2, 2);
	sprintf(szTmp, "%-17f", pstUsrData->bid2);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买价2错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.12转换买量2
		memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->bidLot2);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买量2错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.13转换买价3
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->bid3, 2);
	sprintf(szTmp, "%-17f", pstUsrData->bid3);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买价3错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}
	//2.14转换买量3
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->bidLot3);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买量3错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.15转换买价4
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->bid4, 2);
	sprintf(szTmp, "%-17f", pstUsrData->bid4);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买价4错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.16转换买量4
		memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->bidLot4);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买量4错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.17转换买价5
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->bid5, 2);
	sprintf(szTmp, "%-17f", pstUsrData->bid5);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买价5错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.17转换买量5
		memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->bidLot5);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买量5错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.18转换卖价1
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->ask1, 2);
	sprintf(szTmp, "%-17f", pstUsrData->ask1);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换卖价1错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.19转换卖量1
		memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->askLot1);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换卖量1错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.20转换卖价2
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->ask2, 2);
	sprintf(szTmp, "%-17f", pstUsrData->ask2);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换卖价2错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.21转换卖量2
		memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->askLot2);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换卖量2错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.22转换卖价3
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->ask3, 2);
	sprintf(szTmp, "%-17f", pstUsrData->ask3);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换卖价3错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.23转换卖量3
		memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->askLot3);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换卖量3错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.24转换卖价4
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->ask4, 2);
	sprintf(szTmp, "%-17f", pstUsrData->ask4);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换卖价4错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.25转换卖量4
		memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->askLot4);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换卖量4错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.26转换卖价5
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->ask5, 2);
	sprintf(szTmp, "%-17f", pstUsrData->ask5);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换卖价5错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.27转换卖量5
		memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->askLot5);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换卖量5错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.28转换成交量
		memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->volume);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换成交量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.29转换成交重量
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->weight, 2);
	sprintf(szTmp, "%-17f", pstUsrData->weight);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换成交重量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.30转换涨停板
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->highLimit, 2);
	sprintf(szTmp, "%-17f", pstUsrData->highLimit);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换涨停板错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.31转换跌停板
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->lowLimit, 2);
	sprintf(szTmp, "%-17f", pstUsrData->lowLimit);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换跌停板错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.32涨跌
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->upDown, 2);
	sprintf(szTmp, "%-17f", pstUsrData->upDown);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换涨跌错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.33涨跌幅度
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->upDownRate, 2);
	sprintf(szTmp, "%-17f", pstUsrData->upDownRate);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换涨跌幅度错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.34成交额
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->turnOver, 2);
	sprintf(szTmp, "%-17f", pstUsrData->turnOver);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换成交额错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.35均价
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->average, 2);
	sprintf(szTmp, "%-17f", pstUsrData->average);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换均价错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.36转换行情日期
		memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->quoteDate, strlen(pstUsrData->quoteDate));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换行情日期错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.37转换行情时间
		memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->quoteTime, strlen(pstUsrData->quoteTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换行情时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.38转换行情序号
		memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->sequenceNo);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换行情序号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		
		if (m_blNtfWriteLogFlag)
		{}
	//成功返回转化后的字节数，失败返回-1
	lRet = lPos;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_OnRecvDeferQuotation
* 功能描述：RspInitiative将现货延期交收行情回报API报文转化为平台请求报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- in_pszTrCode      输入 --交易代码
*- in_lConvertFlag      输入 --需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei  于 20091009创建
************************************************************/

long CGTAMsgConvert::rl_OnRecvDeferQuotation(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag)
{
	long	lRet = -1,	lLen;
	long lPos = 0;
	TTRspMsg *pstTRspMsg;
	TDeferQuotation *pstUsrData;

	char szTmp[512];


	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3
	if (m_blNtfWriteLogFlag)
	{}

	//有效性检查
	if (NULL == in_pszInMsg || NULL == in_pszTrCode)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (((long)sizeof(TInstState) + (long)strlen(in_pszTrCode)) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "传入输出报文空间不够");
		return -1;
	}
	//转化标志检查
	if (1 == in_lConvertFlag)
	{
		strcpy(m_szErrMsg, "不支持此格式报文");
		return -1;
	}
	//API回调返回结构是按TTRspMsg、TDeferQuotation 顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TDeferQuotation *)((char*)in_pszInMsg + sizeof(TTRspMsg));


	//1、将API回报的结构TTRspMsg和TDeferQuotation 转化为向平台发送的请求的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//in_lConvertFlag需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文，现在只做0方式
	if (m_blNtfWriteLogFlag)
	{}
	//1.1转换交易代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, in_pszTrCode, strlen(in_pszTrCode));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}

	//1.2转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}

	//1.3转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}
	//2.1转换合约代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}


	//2.2转换合约名称
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->name, strlen(pstUsrData->name));
	lLen = 12;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约名称错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.3转换昨结算
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->lastSettle, 2);
	sprintf(szTmp, "%-17f", pstUsrData->lastSettle);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换昨结算错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)


		//2.4转换昨收盘
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->lastClose, 2);
	sprintf(szTmp, "%-17f", pstUsrData->lastClose);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换昨收盘错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.5转换开盘价
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->open, 2);
	sprintf(szTmp, "%-17f", pstUsrData->open);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换开盘价错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.6转换最高价
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->high, 2);
	sprintf(szTmp, "%-17f", pstUsrData->high);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换最高价错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.7转换最低价
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->low, 2);
	sprintf(szTmp, "%-17f", pstUsrData->low);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换最低价错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.8转换最新价
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->last, 2);
	sprintf(szTmp, "%-17f", pstUsrData->last);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换最新价错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.9转换收盘价
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->close, 2);
	sprintf(szTmp, "%-17f", pstUsrData->close);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换收盘价错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.10转换结算价
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->settle, 2);
	sprintf(szTmp, "%-17f", pstUsrData->settle);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换结算价错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.11转换买价1
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->bid1, 2);
	sprintf(szTmp, "%-17f", pstUsrData->bid1);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买价1错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.12转换买量1
		memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->bidLot1);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买量1错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.13转换买价2
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->bid2, 2);
	sprintf(szTmp, "%-17f", pstUsrData->bid2);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买价2错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.14转换买量2
		memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->bidLot2);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买量2错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.15转换买价3
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->bid3, 2);
	sprintf(szTmp, "%-17f", pstUsrData->bid3);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买价3错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}
	//2.16转换买量3
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->bidLot3);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买量3错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.17转换买价4
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->bid4, 2);
	sprintf(szTmp, "%-17f", pstUsrData->bid4);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买价4错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.18转换买量4
		memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->bidLot4);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买量4错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.19转换买价5
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->bid5, 2);
	sprintf(szTmp, "%-17f", pstUsrData->bid5);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买价5错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.20转换买量5
		memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->bidLot5);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买量5错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.21转换卖价1
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->ask1, 2);
	sprintf(szTmp, "%-17f", pstUsrData->ask1);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换卖价1错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.22转换卖量1
		memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->askLot1);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换卖量1错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.23转换卖价2
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->ask2, 2);
	sprintf(szTmp, "%-17f", pstUsrData->ask2);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换卖价2错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.24转换卖量2
		memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->askLot2);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换卖量2错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.25转换卖价3
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->ask3, 2);
	sprintf(szTmp, "%-17f", pstUsrData->ask3);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换卖价3错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.26转换卖量3
		memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->askLot3);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换卖量3错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.27转换卖价4
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->ask4, 2);
	sprintf(szTmp, "%-17f", pstUsrData->ask4);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换卖价4错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.28转换卖量4
		memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->askLot4);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换卖量4错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.29转换卖价5
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->ask5, 2);
	sprintf(szTmp, "%-17f", pstUsrData->ask5);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换卖价5错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.30转换卖量5
		memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->askLot5);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换卖量5错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.31转换成交量
		memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->volume);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换成交量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.32转换成交重量
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->weight, 2);
	sprintf(szTmp, "%-17f", pstUsrData->weight);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换成交重量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.33转换涨停板
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->highLimit, 2);
	sprintf(szTmp, "%-17f", pstUsrData->highLimit);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换涨停板错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.34转换跌停板
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->lowLimit, 2);
	sprintf(szTmp, "%-17f", pstUsrData->lowLimit);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换跌停板错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.35转换持仓量
		memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->Posi);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换持仓量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)


		//2.36涨跌
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->upDown, 2);
	sprintf(szTmp, "%-17f", pstUsrData->upDown);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换涨跌错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.37涨跌幅度
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->upDownRate, 2);
	sprintf(szTmp, "%-17f", pstUsrData->upDownRate);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换涨跌幅度错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.38成交额
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->turnOver, 2);
	sprintf(szTmp, "%-17f", pstUsrData->turnOver);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换成交额错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.39均价
		memset(szTmp, 0, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->average, 2);
	sprintf(szTmp, "%-17f", pstUsrData->average);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换均价错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.40转换行情日期
		memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->quoteDate, strlen(pstUsrData->quoteDate));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换行情日期错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.41转换行情时间
		memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->quoteTime, strlen(pstUsrData->quoteTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换行情时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.42转换行情序号
		memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->sequenceNo);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换行情序号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		
		if (m_blNtfWriteLogFlag)
		{}
	//成功返回转化后的字节数，失败返回-1
	lRet = lPos;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_OnRecvDeferDeliveryQuotation
* 功能描述：RspInitiative将现货延期交收交割行情回报API报文转化为平台请求报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- in_pszTrCode      输入 --交易代码
*- in_lConvertFlag      输入 --需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei  于 20100114创建
************************************************************/
long CGTAMsgConvert::rl_OnRecvDeferDeliveryQuotation(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag)
{
	long	lRet = -1,	lLen;
	long lPos = 0;
	TTRspMsg *pstTRspMsg;
	TDeferDeliveryQuotation  *pstUsrData;

	char szTmp[512];


	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3
	if (m_blNtfWriteLogFlag)
	{}

	//有效性检查
	if (NULL == in_pszInMsg || NULL == in_pszTrCode)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (((long)sizeof(TInstState) + (long)strlen(in_pszTrCode)) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "传入输出报文空间不够");
		return -1;
	}
	//转化标志检查
	if (1 == in_lConvertFlag)
	{
		strcpy(m_szErrMsg, "不支持此格式报文");
		return -1;
	}
	//API回调返回结构是按TTRspMsg、TDeferDeliveryQuotation 顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TDeferDeliveryQuotation *)((char*)in_pszInMsg + sizeof(TTRspMsg));


	//1、将API回报的结构TTRspMsg和TDeferDeliveryQuotation 转化为向平台发送的请求的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//in_lConvertFlag需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文，现在只做0方式
	if (m_blNtfWriteLogFlag)
	{}
	//1.1转换交易代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, in_pszTrCode, strlen(in_pszTrCode));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}

	//1.2转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}

	//1.3转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}
	//2.1转换合约代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}

	//2.2转换买量
	memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->bidLot);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换买量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.3转换卖量
		memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->askLot);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换卖量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.4转换中立仓买量
		memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->midBidLot);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换中立仓买量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.5转换中立仓卖量
		memset(szTmp, 0, sizeof(szTmp));
	sprintf(szTmp,"%-10d",pstUsrData->midAskLot);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换中立仓卖量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)


		
		if (m_blNtfWriteLogFlag)
		{}
	//成功返回转化后的字节数，失败返回-1
	lRet = lPos;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_onRecvRspQrySpotLocalOrder
* 功能描述：RspInitiative将现货本地保单号应答回报API报文转化为平台请求报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- in_pszTrCode      输入 --交易代码
*- in_lConvertFlag      输入 --需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei  于 20100114创建
************************************************************/
long CGTAMsgConvert::rl_onRecvRspQrySpotLocalOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag)
{
	long	lRet = -1,	lLen;
	long lPos = 0;
	TTRspMsg *pstTRspMsg;
	TSpotOrder  *pstUsrData;

	char szTmp[512];


	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3
	if (m_blNtfWriteLogFlag)
	{}

	//有效性检查
	if (NULL == in_pszInMsg || NULL == in_pszTrCode)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (((long)sizeof(TInstState) + (long)strlen(in_pszTrCode)) > in_lOutMsgSize)//空间判断为什么要以TInstState为基准
	{
		strcpy(m_szErrMsg, "传入输出报文空间不够");
		return -1;
	}
	//转化标志检查
	if (1 == in_lConvertFlag)
	{
		strcpy(m_szErrMsg, "不支持此格式报文");
		return -1;
	}
	//API回调返回结构是按TTRspMsg、TSpotOrder顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TSpotOrder *)((char*)in_pszInMsg + sizeof(TTRspMsg));


	//1、将API回报的结构TTRspMsg和TDeferDeliveryQuotation 转化为向平台发送的请求的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//in_lConvertFlag需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文，现在只做0方式
	if (m_blNtfWriteLogFlag)
	{}
	//1.1转换交易代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, in_pszTrCode, strlen(in_pszTrCode));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}

	//1.2转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}

	//1.3转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}
	//2.1转换保单号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->orderNo, strlen(pstUsrData->orderNo));
	lLen =16;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换保单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}

	//2.2转换合约代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}
	//2.3转换客户代码
	memset(szTmp, 0, sizeof(szTmp));
	memcpy(szTmp,pstUsrData->clientID,strlen(pstUsrData->clientID));
	lLen = 12;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换客户代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.4转换会员代码
		memset(szTmp, 0, sizeof(szTmp));
	memcpy(szTmp,pstUsrData->memberID,strlen(pstUsrData->memberID));
	lLen = 6;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换会员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.5取买卖方向
		memset(szTmp, 0, sizeof(szTmp));
	lLen = 1;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取买卖方向错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
		UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "买卖方向为空");
		return -1;
	}
	pstUsrData->buyOrSell = szTmp[0];

	//2.6转换申报日期
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->applyDate, strlen(pstUsrData->applyDate));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换申报日期错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.7转换申报时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->applyTime, strlen(pstUsrData->applyTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换申报时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.8转换交易员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->traderID, strlen(pstUsrData->traderID));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.9转换价格
	memset(szTmp, 32, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->price, 2);
	sprintf(szTmp, "%-17f", pstUsrData->price);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换价格错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.10转换数量
	memset(szTmp, 32, sizeof(szTmp));
	sprintf(szTmp, "%-10d", pstUsrData->amount);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.11取剩余数量
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 10;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取剩余数量错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
		UStringHelper::Trim(szTmp);
	pstUsrData->remainAmount = atoi(szTmp);

	//2.12转换撤销时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->cancelTime, strlen(pstUsrData->cancelTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换撤销时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)


		//2.13转换撤消员代码
		memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->cancelID, strlen(pstUsrData->cancelID));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换撤消员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}
	//2.14转换状态
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->status;
	//	szTmp[0] = '3';
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换状态错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}
	//2.15转换类型
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->matchType;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换类型错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}
	//2.16转换转让合约代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->endorseInstID, strlen(pstUsrData->endorseInstID));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换转让合约代码 错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}
	//2.17转换本地报单号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->localOrderNo, strlen(pstUsrData->localOrderNo));
	lLen = 14;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换本地报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}
	//成功返回转化后的字节数，失败返回-1
	lRet = lPos;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_onRecvRspQryForwardLocalOrder
* 功能描述：RspInitiative将现货本地保单号应答回报API报文转化为平台请求报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- in_pszTrCode      输入 --交易代码
*- in_lConvertFlag      输入 --需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：wangfei  于 20100114创建
************************************************************/
long CGTAMsgConvert::rl_onRecvRspQryForwardLocalOrder(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag)
{
	long	lRet = -1,	lLen;
	long lPos = 0;
	TTRspMsg *pstTRspMsg;
	TForwardOrder  *pstUsrData;

	char szTmp[512];


	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3


	//有效性检查
	if (NULL == in_pszInMsg || NULL == in_pszTrCode)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (((long)sizeof(TForwardOrder) + (long)strlen(in_pszTrCode)) > in_lOutMsgSize)//空间判断为什么要以TInstState为基准
	{
		strcpy(m_szErrMsg, "传入输出报文空间不够");
		return -1;
	}
	//转化标志检查
	if (1 == in_lConvertFlag)
	{
		strcpy(m_szErrMsg, "不支持此格式报文");
		return -1;
	}
	//API回调返回结构是按TTRspMsg、TSpotOrder顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TForwardOrder *)((char*)in_pszInMsg + sizeof(TTRspMsg));


	//1、将API回报的结构TTRspMsg和TDeferDeliveryQuotation 转化为向平台发送的请求的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//in_lConvertFlag需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文，现在只做0方式

	//1.1转换交易代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, in_pszTrCode, strlen(in_pszTrCode));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//1.2转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//1.3转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}
	//2.1转换报单号
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->orderNo, strlen(pstUsrData->orderNo));
	lLen =16;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	//2.2转换合约代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}
	//2.3转换客户代码
	memset(szTmp, 0, sizeof(szTmp));
	memcpy(szTmp,pstUsrData->clientID,strlen(pstUsrData->clientID));
	lLen = 12;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换客户代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.4转换会员代码
		memset(szTmp, 0, sizeof(szTmp));
	memcpy(szTmp,pstUsrData->memberID,strlen(pstUsrData->memberID));
	lLen = 6;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换会员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)

		//2.5取买卖方向
		memset(szTmp, 0, sizeof(szTmp));
	lLen = 1;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取买卖方向错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
		UStringHelper::Trim(szTmp);
	//判断是否为空
	if (0 == strlen(szTmp))
	{
		strcpy(m_szErrMsg, "买卖方向为空");
		return -1;
	}
	pstUsrData->buyOrSell = szTmp[0];

	//2.6转换申报日期
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->applyDate, strlen(pstUsrData->applyDate));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换申报日期错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.7转换申报时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->applyTime, strlen(pstUsrData->applyTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换申报时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.8转换交易员代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->traderID, strlen(pstUsrData->traderID));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}





	//2.9转换价格
	memset(szTmp, 32, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->price, 2);
	sprintf(szTmp, "%-17f", pstUsrData->price);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换价格错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.10转换数量
	memset(szTmp, 32, sizeof(szTmp));
	sprintf(szTmp, "%-10d", pstUsrData->amount);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}




	//2.11取剩余数量
	memset(szTmp, 0, sizeof(szTmp));
	lLen = 10;
	memcpy(szTmp, &in_pszInMsg[lPos], lLen);
	lPos += lLen;
	//判断是否为有效数据
	if (lPos > in_lInMsgLength)
	{
		sprintf(m_szErrMsg, "取剩余数量错误-报文长度%ld不够", in_lInMsgLength);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
		UStringHelper::Trim(szTmp);
	pstUsrData->remainAmount = atoi(szTmp);

	//2.12转换撤销时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->cancelTime, strlen(pstUsrData->cancelTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换撤销时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)


		//2.13转换撤消员代码
		memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->cancelID, strlen(pstUsrData->cancelID));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换撤消员代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
		

		//2.14转换状态
		memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->status;
	//	szTmp[0] = '3';
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换状态错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)



		//2.15转换本地报单号
		memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->localOrderNo, strlen(pstUsrData->localOrderNo));
	lLen = 14;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换本地报单号错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)



		//2.16转换类型
		memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->matchType;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换类型错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)





		
		if (m_blNtfWriteLogFlag)
		{}
	//成功返回转化后的字节数，失败返回-1
	lRet = lPos;
	return lRet;
}


/**********************************************************
* 函 数 名： CTrMsgConvert::rl_OnRecvMinsSpotQuotation
* 功能描述：RspInitiative将当天现货分钟行情回报API报文转化为平台请求报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- in_pszTrCode      输入 --交易代码
*- in_lConvertFlag      输入 --需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：ukyo  于 20071014创建
************************************************************/

long CGTAMsgConvert::rl_OnRecvMinsSpotQuotation(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag)
{
	long	lRet = -1,	lLen;
	long lPos = 0;
	TTRspMsg * pstTRspMsg;
	TMinsSpotQuotation  *pstUsrData;

	char szTmp[512];



	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3
	if (m_blNtfWriteLogFlag)
	{}

	//有效性检查
	if (NULL == in_pszInMsg || NULL == in_pszTrCode)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (((long)sizeof(TInstState) + (long)strlen(in_pszTrCode)) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "传入输出报文空间不够");
		return -1;
	}
	//转化标志检查
	if (1 == in_lConvertFlag)
	{
		strcpy(m_szErrMsg, "不支持此格式报文");
		return -1;
	}
	//API回调返回结构是按TTRspMsg、TMinsSpotQuotation 顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TMinsSpotQuotation *)((char*)in_pszInMsg + sizeof(TTRspMsg));

	//1、将API回报的结构TTRspMsg和TMinsSpotQuotation 转化为向平台发送的请求的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//in_lConvertFlag需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文，现在只做0方式


	//成功返回转化后的字节数，失败返回-1
	if (m_blNtfWriteLogFlag)
	{}
	//1.1转换交易代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, in_pszTrCode, strlen(in_pszTrCode));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}

	//1.2转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}
	//1.3转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}
	//2.1转换合约代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->instID, strlen(pstUsrData->instID));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换合约代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}
	//2.2转换开盘价
	memset(szTmp, 32, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->open, 2);
	sprintf(szTmp, "%-17f", pstUsrData->open);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换开盘价错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}
	//2.3最高价
	memset(szTmp, 32, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->high, 2);
	sprintf(szTmp, "%-17f", pstUsrData->high);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换最高价错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}
	//2.4转换最低价
	memset(szTmp, 32, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->low, 2);
	sprintf(szTmp, "%-17f", pstUsrData->low);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换最低价错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}
	//2.5转换收盘价
	memset(szTmp, 32, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->close, 2);
	sprintf(szTmp, "%-17f", pstUsrData->close);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换收盘价错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}
	//2.6转换成交量
	memset(szTmp, 32, sizeof(szTmp));
	sprintf(szTmp, "%-10d", pstUsrData->volume);
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换成交量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}
	//2.7转换行情日期
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->quoteDate, strlen(pstUsrData->quoteDate));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换行情日期错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}
	//2.8转换行情时间
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->quoteTime, strlen(pstUsrData->quoteTime));
	lLen = 8;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换行情时间错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}

	
	if (m_blNtfWriteLogFlag)
	{}
	//成功返回转化后的字节数，失败返回-1
	lRet = lPos;
	return lRet;
}
/**********************************************************
* 函 数 名： CTrMsgConvert::rl_OnRecvVarietyInfo
* 功能描述：RspInitiative将交割品种代码信息回报API报文转化为平台请求报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- in_pszTrCode      输入 --交易代码
*- in_lConvertFlag      输入 --需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：ukyo  于 20071014创建
************************************************************/

long CGTAMsgConvert::rl_OnRecvVarietyInfo(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag)
{
	long	lRet = -1,	lLen;
	long	lPos = 0;
	TTRspMsg *pstTRspMsg;
	TVariety *pstUsrData;
	char szTmp[512];

	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3
	if (m_blNtfWriteLogFlag)
	{}

	//有效性检查
	if (NULL == in_pszInMsg || NULL == in_pszTrCode)
	{
		strcpy(m_szErrMsg, "没有传入待转化报文");
		return -1;
	}
	if (NULL == out_pszOutMsg)
	{
		strcpy(m_szErrMsg, "没有传入输出报文指针");
		return -1;
	}
	if (((long)sizeof(TInstState) + (long)strlen(in_pszTrCode)) > in_lOutMsgSize)
	{
		strcpy(m_szErrMsg, "传入输出报文空间不够");
		return -1;
	}
	//转化标志检查
	if (1 == in_lConvertFlag)
	{
		strcpy(m_szErrMsg, "不支持此格式报文");
		return -1;
	}
	//API回调返回结构是按TTRspMsg、TVariety顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (TVariety *)((char*)in_pszInMsg + sizeof(TTRspMsg));


	//1、将API回报的结构TTRspMsg和TVariety 转化为向平台发送的请求的结构
	//注意：（1）转化后的内容长度不能超过空间存可保存的in_lOutMsgSize限制
	//in_lConvertFlag需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文，现在只做0方式
	if (m_blNtfWriteLogFlag)
	{}

	//1.1转换交易代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, in_pszTrCode, strlen(in_pszTrCode));
	lLen = 10;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交易代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}

	//1.2转换响应代码
	memset(szTmp,  32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspCode, strlen(pstTRspMsg->RspCode));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}

	//1.3转换响应消息
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstTRspMsg->RspMsg, strlen(pstTRspMsg->RspMsg));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换响应消息错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}

	//2.1转换交割品种代码
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->varietyID, strlen(pstUsrData->varietyID));
	lLen = 3;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交割品种代码错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}

	//2.2转换交割品种全称
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->name, strlen(pstUsrData->name));
	lLen = 40;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换交割品种全称错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}


	//2.3转换简称
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->abbr, strlen(pstUsrData->abbr));
	lLen = 20;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换简称错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}

	//2.4转换品种类型
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->varietyType;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换品种类型错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}

	//2.5转换最小提货数量
	memset(szTmp, 32, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->minPickup, 6);
	sprintf(szTmp, "%-17f", pstUsrData->minPickup);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换最小提货数量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}

	//2.6转换默认标准重量
	memset(szTmp, 32, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->defaultStdWeight, 6);
	sprintf(szTmp, "%-17f", pstUsrData->defaultStdWeight);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换默认标准重量错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}


	//2.7转换提货步长
	memset(szTmp, 32, sizeof(szTmp));
	CGTABufferBuilder::rd_RoundFloat(pstUsrData->pickupBase, 6);
	sprintf(szTmp, "%-17f", pstUsrData->pickupBase);
	lLen = 17;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换提货步长错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}

	//2.8转换重量单位
	memset(szTmp, 32, sizeof(szTmp));
	memcpy(szTmp, pstUsrData->weightUnit, strlen(pstUsrData->weightUnit));
	lLen = 2;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换重量单位错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}

	if (m_blNtfWriteLogFlag)
	{}

	//2.9转换注销标志
	memset(szTmp, 32, sizeof(szTmp));
	szTmp[0] = pstUsrData->destroyFlag;
	lLen = 1;
	szTmp[lLen] = 0;
	memcpy(&out_pszOutMsg[lPos], szTmp, lLen);
	lPos += lLen;
	if (lPos > in_lOutMsgSize)
	{
		sprintf(m_szErrMsg, "转换注销标志错误-转化后报文存放空间大小%ld不够", in_lOutMsgSize);
		return -1;
	}
//成功返回转化后的字节数，失败返回-1
	lRet = lPos;
	return lRet;
}

/**********************************************************
* 函 数 名： CTrMsgConvert::rl_OnChannelLost
* 功能描述：RspInitiative将路线丢失的回报API报文转化为平台请求报文
*参数：
*- in_pszInMsg      输入 --待转化报文
*- in_lInMsgLength      输入 --待转化报文长度
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
*- in_pszTrCode      输入 --交易代码
*- in_lConvertFlag      输入 --需要将报文转化为指定格式，0-为交行黄金交易报文，1-行情服务器报文
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：ukyo  于 20071014创建
************************************************************/

long CGTAMsgConvert::rl_OnChannelLost(char * in_pszInMsg, long in_lInMsgLength, char * out_pszOutMsg, long in_lOutMsgSize, char * in_pszTrCode, long in_lConvertFlag)
{
	long	lRet = -1;
	long	lPos = 0;
	TTRspMsg *pstTRspMsg;
	char *pstUsrData;

	//清除错误信息
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3



	//API回调返回结构是按TTRspMsg、TInstState 顺序存放在in_pszInMsg中
	pstTRspMsg = (TTRspMsg*)in_pszInMsg;
	pstUsrData = (char *)((char*)in_pszInMsg + sizeof(TTRspMsg));

	CGTABufferBuilder bf(out_pszOutMsg,0);
	bf.AppendBlock(in_pszTrCode,strlen(in_pszTrCode),10);

	lRet = bf.GetIndex();
	return lRet;
}


#if _MSC_VER <= 1200
#define  VC6CMPATBLE(x) x
#else
#define  VC6CMPATBLE(x) &CGTAMsgConvert::x
#endif

void CGTAMsgConvert::InitInAPIAFunc()
{
	//注：vc6对成员函数指针的语法支持不正确

	m_stInAPIAFunc["ReqMemberUrgentPwdUpdate"]= VC6CMPATBLE(rl_ReqMemberUrgentPwdUpdate);
	m_stInAPIAFunc["ReqQryBulletinBoard"]=VC6CMPATBLE( rl_ReqQryBulletinBoard);
	m_stInAPIAFunc["ReqQryClient"]=VC6CMPATBLE( rl_ReqQryClient);
	m_stInAPIAFunc["ReqQryClientStorage"]=VC6CMPATBLE( rl_ReqQryClientStorage);
	m_stInAPIAFunc["ReqQryInterQuotation"]=VC6CMPATBLE( rl_ReqQryInterQuotation);
	m_stInAPIAFunc["ReqQryMemberCapital"]=VC6CMPATBLE( rl_ReqQryMemberCapital);
	m_stInAPIAFunc["ReqQrySpotMatch"]=VC6CMPATBLE( rl_ReqQrySpotMatch);
	m_stInAPIAFunc["ReqQrySpotMinsQuotation"]=VC6CMPATBLE( rl_ReqQrySpotMinsQuotation);
	m_stInAPIAFunc["ReqQrySpotOrder"]=VC6CMPATBLE( rl_ReqQrySpotOrder);
	m_stInAPIAFunc["ReqQrySpotQuotation"]=VC6CMPATBLE( rl_ReqQrySpotQuotation);
	m_stInAPIAFunc["ReqSpotOrder"]=VC6CMPATBLE( rl_ReqSpotOrder);
	m_stInAPIAFunc["ReqSpotOrderCancel"]=VC6CMPATBLE( rl_ReqSpotOrderCancel);
	m_stInAPIAFunc["ReqTrLogin"]=VC6CMPATBLE( rl_ReqTrLogin);
	m_stInAPIAFunc["TLOGIN"]=VC6CMPATBLE( rl_ReqTrLogin);   ///TODO,chh login  call  do not use the upper name from GTA server.
	m_stInAPIAFunc["ReqTrLogout"]=VC6CMPATBLE( rl_ReqTrLogout);
	m_stInAPIAFunc["ReqTrPwdUpdate"]=VC6CMPATBLE( rl_ReqTrPwdUpdate);
	m_stInAPIAFunc["ReqDeferOrder"]=VC6CMPATBLE(rl_ReqDeferOrder);
	m_stInAPIAFunc["ReqDeferOrderCancel"]=VC6CMPATBLE(rl_ReqDeferOrderCancel);
	m_stInAPIAFunc["ReqDeferDeliveryAppOrder"]=VC6CMPATBLE(rl_ReqDeferDeliveryAppOrder);
	m_stInAPIAFunc["ReqMiddleAppOrder"]=VC6CMPATBLE(rl_ReqMiddleAppOrder);
	m_stInAPIAFunc["ReqMiddleAppOrderCancel"]=VC6CMPATBLE(rl_ReqMiddleAppOrderCancel);
	m_stInAPIAFunc["ReqDeferDeliveryAppOrderCancel"]=VC6CMPATBLE(rl_ReqDeferDeliveryAppOrderCancel);
	m_stInAPIAFunc["ReqQryDeferClientPosi"]=VC6CMPATBLE( rl_ReqQryDeferClientPosi);
	m_stInAPIAFunc["ReqQryDeferDeliveryAppMatch"]= VC6CMPATBLE(rl_ReqQryDeferDeliveryAppMatch);
	m_stInAPIAFunc["ReqQryDeferMatch"]= VC6CMPATBLE(rl_ReqQryDeferMatch);
	m_stInAPIAFunc["ReqQryDeferFeeRate"]= VC6CMPATBLE(rl_ReqQryDeferFeeRate);
	m_stInAPIAFunc["ReqQryDeferDeliveryAppOrder"]= VC6CMPATBLE(rl_ReqQryDeferDeliveryAppOrder);
	m_stInAPIAFunc["ReqQryDeferMinsQuotation"]= VC6CMPATBLE(rl_ReqQryDeferMinsQuotation);
	m_stInAPIAFunc["ReqQryDeferMemberPosi"]= VC6CMPATBLE(rl_ReqQryDeferMemberPosi);
	m_stInAPIAFunc["ReqQryDeferQuotation"]= VC6CMPATBLE(rl_ReqQryDeferQuotation);
	m_stInAPIAFunc["ReqQryDeferOrder"]= VC6CMPATBLE(rl_ReqQryDeferOrder);
	m_stInAPIAFunc["ReqQrySpotLocalOrder"]=VC6CMPATBLE(rl_ReqQrySpotLocalOrder);//现货本地保单号查询请求
	m_stInAPIAFunc["ReqQryDeferLocalOrder"]=VC6CMPATBLE(rl_ReqQryDeferLocalOrder);//现货延期交收本地保单号查询请求
	m_stInAPIAFunc["ReqQryForwardLocalOrder"]=VC6CMPATBLE(rl_ReqQryForwardLocalOrder);//现货T+N本地保单号查询请求


}



/**********************************************************
* 函 数 名： rl_BuildRspErrMsg
* 功能描述：   将传入的ErrMsg生成返回平台报文
* 参数：
*- in_pszErrNo      输入 --错误代码
*- in_pszErrMsg      输入 --错误信息
*- out_pszOutMsg         输出 --转化后的报文存放空间指针，注意该空间必须事先分配内存
*- in_lOutMsgSize      输入 --转化后的报文存放空间的大小
* 返回说明： 成功--转化报文长度，失败--小于0
* 开发历史：liuqy   于 20070913 创建
************************************************************/
long CGTAMsgConvert::rl_BuildRspErrMsg(char * in_pszErrNo, char * in_pszErrMsg, char * out_pszOutMsg, long in_lOutMsgSize)
{
	char szErrNo[30];
	long lRet = 0;

	//清除错误信息和错误代码
	memset(m_szErrMsg, 0, sizeof(m_szErrMsg));
	//输出日志MY_NORMAL，MY_D3

	memset(szErrNo, 0, sizeof(szErrNo));

	//如果返回码不存在，则使用缺席的
	if (NULL == in_pszErrNo)
		strcpy(szErrNo, ERR_OTHER);
	else
	{
		if (0 == strlen(in_pszErrNo))
			strcpy(szErrNo, ERR_OTHER);
		else
			strncpy(szErrNo, in_pszErrNo, 20); //最多复制20个字符
	}
	//如果没有错误信息,则只输出错误码
	if (NULL == in_pszErrMsg)
	{
		sprintf(out_pszOutMsg, "%-20s%-40s", szErrNo, "");
	}
	else
	{
		sprintf(out_pszOutMsg, "%-20s%-40s", szErrNo, in_pszErrMsg);
	}
	lRet = strlen(out_pszOutMsg);

	
	//成功返回转化后的字节数，失败返回-1
	return lRet;

}
}