#ifndef _THREAD_TASK_H_
#define _THREAD_TASK_H_

//
//其基于类模型的thead，开发起来非常方便
//
typedef unsigned long DWORD;
typedef void* HANDLE;
namespace utils
{
	class ThreadTask
	{
	public:
		virtual int svc()=0;
	public:
		int activate();
		void close(DWORD existcode);
	public:
		bool IsRunning() const;
		DWORD GetExistCode() const;
	private:
		static DWORD WorkThread(void* p);
	protected:
		HANDLE m_hthread;
		DWORD m_existcode;
	};
}
#endif