#include "GTAReciever.h"
#include "GTARecieverMgr.h"
#include "AnyCast.h"

namespace gta
{
	void gta::CGTARecieverMgr::AddReviever( std::shared_ptr<CGTAReciever>& rec )
	{
		threadlock::autoguard<threadlock::multi_threaded_local> a(&m_lock);
		m_recievers[rec->GetKey()] = rec;
	}


	
	void CGTARecieverMgr::RemoveReciever(std::string key)
	{
		threadlock::autoguard<threadlock::multi_threaded_local > guard(&m_lock);
		auto it = m_recievers.find(key);
		if(it != m_recievers.end())
		{
			m_recievers.erase(it);
		}
	}
	CGTARecieverMgr::~CGTARecieverMgr()
	{
		//m_recievers.clear();
	}

	std::pair<bool,std::shared_ptr<CGTAReciever> > CGTARecieverMgr::FindReciever( std::string key )
	{
		threadlock::autoguard<threadlock::multi_threaded_local > guard(&m_lock);

		auto it = m_recievers.find(key);
		if(it != m_recievers.end())
		{	
			return std::make_pair<bool,std::shared_ptr<CGTAReciever> > (true,it->second);
		}
		else
		{
			std::shared_ptr<CGTAReciever> dummy;
			return std::make_pair<bool,std::shared_ptr<CGTAReciever> >(false,dummy);
		}
	}

	std::string CGTARecieverMgr::GetQueryInfo()
	{
		threadlock::autoguard<threadlock::multi_threaded_local > guard(&m_lock);
		int count = m_recievers.size();
		std::string info = std::string("RecieveMgr目前管理着") + AnyCastString<int>(count) + "个Reciever";
		return info;
	}

}