#ifndef _TRADE_CODE_MAP_H_
#define _TRADE_CODE_MAP_H_
#include <string>
#include <map>

class TiXmlNode;
namespace gta
{
	class CGTATradeCodeMap
	{
	public:
		struct CodeInfo
		{
			std::string tradecode;
			std::string name;
			std::string InString2Struct;
			std::string OutMsgFunct;
			std::string Apifunction;
			std::string OutAPIFunct;
			int TimeOut;
		};

		struct RspInfo
		{
			std::string tradecode;
			std::string gtaaddr;
			int port;
		};
		

		bool InitializeTradecodeByXMLNode(TiXmlNode *p);
		bool InitializeReplycodeByXMLNode(TiXmlNode * p);
		static CGTATradeCodeMap& Instance();

		std::string GetTradeCodeByOutFuncName(std::string outfunc);


		RspInfo* GetRspInfo(const char* tradecode);

		bool ContainsKey(std::string key);

		std::map<std::string,CodeInfo> m_map;
		std::map<std::string,RspInfo> m_gtareplymap;
	private:
		CGTATradeCodeMap();


	};

}
#endif