/***********************************************************************
 * Module:  CCrypticDES.h
 * Author:  刘青勇
 * Modified: 2007年9月10日 
 * Purpose: Declaration of the class CCrypticDES
 * Comment: 对明文数据进行加密工作，密文数据进行解密工作，
 ***********************************************************************/


#ifndef __CCRYPTICDES_H__
#define __CCRYPTICDES_H__

//#include "ICCrypticDES.h"

namespace utils
{



/***
  加解密处理
*/

class CCrypticDES
{
public:
	/**********************************************************
	* 函 数 名： rv_Release
	* 功能描述：释放资源,删除自己
	*参数：
	* 返回说明： 无
	* 开发历史：liuqy   于 20070917 创建
	************************************************************/
	
	CCrypticDES();
	~CCrypticDES();
     /**********************************************************
     * 函 数 名： rpsz_GetErrMsg
     * 功能描述：   返回错误信息
   
     * 返回说明： 返回错误信息
     ************************************************************/
	char * rpsz_GetErrMsg(void);
	/**********************************************************
	* 函 数 名： rl_SetKey
	* 功能描述：设置系统当前工作密钥
	*参数：
	*- in_pszCryticKey      输入 --是经过保存方式加过密的钥密，是ASCII字符串
	* 返回说明： 成功--0，失败--小于0
	* 开发历史：liuqy   于 20070910 创建
	************************************************************/
	long	rl_SetKey(const char * in_pszCryticKey);
	/**********************************************************
	* 函 数 名： rl_EncryptPwd
	* 功能描述：对明文数据进行加密，加密使用rl_SetKey设置的密钥进行加密
	*参数：
	*- in_pszGram      输入 --待加密的明文数据最长不能超过64，是ASCII字符串
	*- out_pszRes      输出 --加密后的返回密文数据的指针，空间需要分配，系统将BYTE转换为ASCII字符串		
	*- in_loutResSize	输入 --返回密文数据指向的空间大小
	* 返回说明： 成功--加密后数据长度，失败--小于0
	* 开发历史：liuqy   于 20070910 创建
	************************************************************/
	long rl_EncryptPwd(char *in_pszGram, char *out_pszRes, long in_loutResSize);
	/**********************************************************
	* 函 数 名： rl_DecryptPwd
	* 功能描述：对密文的ASCII数据进行解密，加密使用rl_SetKey设置的密钥进行解密
	*参数：
	*- in_pszGram      输入 --待解密的密文数据最长不能超过128，是ASCII字符串
	*- out_pszRes      输出 --解密后的返回明文数据的指针，空间需要分配，系统将返回原加密状态的数据		
	*- in_loutResSize	输入 --返回明文数据指向的空间大小
	* 返回说明： 成功--0，失败--小于0
	* 开发历史：liuqy   于 20070910 创建
	************************************************************/
	long rl_DecryptPwd(char *in_pszGram, char *out_pszRes, long in_loutResSize);
	/**********************************************************
	* 函 数 名： rl_GetDataForSave
	* 功能描述：对需要保存到参数文件中的明文ASCII数据进行加密，加密使用保存所有的密钥进行加密
	*参数：
	*- in_pszGram      输入 --待保存的明文数据最长不能超过64，是ASCII字符串
	*- out_pszRes      输出 --加密后的返回保存的密文数据的指针，空间需要分配
	*- in_loutResSize	输入 --返回密文数据指向的空间大小
	* 返回说明： 成功--0，失败--小于0
	* 开发历史：liuqy   于 20070910 创建
	************************************************************/
	long rl_GetDataForSave(char *in_pszGram, char *out_pszRes, long in_loutResSize);
	/**********************************************************
	* 函 数 名： CCrypticDES::rl_GetDataFromSave
	* 功能描述：对保存的密文的ASCII数据进行解密
	* 注意：加密数据为：xxMMM,其中xx表示MMM的长度，MMM的原始数据数据，解密后只返回MMM
	*参数：
	*- in_pszGram      输入 --待解密的密文数据最长不能超过128，是ASCII字符串
	*- out_pszRes      输出 --解密后的返回明文数据的指针，空间需要分配，系统将返回原加密状态的数据		
	*- in_loutResSize	输入 --返回明文数据指向的空间大小
	* 返回说明： 成功--0，失败--小于0
	* 开发历史：liuqy   于 20070910 创建
	************************************************************/
	long rl_GetDataFromSave(char *in_pszGram, char *out_pszRes, long in_loutResSize);

private:
	long m_lKeyLen;

	/*工作密钥 */
	char	m_szKey[64]; 
	/*保存密文数据使用的密钥*/
	char	m_szSaveKey[64];
   /* 存放错误信息 */
	char	m_szErrMsg[512];


};
}
#endif