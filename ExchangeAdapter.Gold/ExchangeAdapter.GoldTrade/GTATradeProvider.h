#if !defined(_GTA_PROCESSOR_H_)
#define _GTA_PROCESSOR_H_
#pragma warning(disable :4786)
#include <string>
#include <map>
#include <memory>
#include <vector>
#include "threadlock.h"
#include "BufferPool.h"
using namespace utils;


namespace gta
{
	class CGTAListener;
	class CGTAReciever;
	class CGTARecieverMgr;
	class CGTASenderMgr;
	class CGTATradeApiHandler;




	class CGTATradeProvider 
	{
	public:
		enum RetType
		{			
			RESPONSE,
			RETURN,
		};

		struct InitParam
		{
			int ilistenport;
		
			std::string commkey;

			std::string GTAServerAddr;
			int GTAServerPort;
		};

		bool SetAPIHandler(CGTATradeApiHandler* handler);
		
		bool Initialize(InitParam* p);
		bool InitializeByXML(std::string fname);
		void Stop();
		CGTATradeProvider();
		~CGTATradeProvider();

		

		void ProcessData(char* buff,int bufflen,const char* rootkey,RetType TYPE,const char* tradecode );	
		
		void OldProcessData(char* buff,int bufflen,const char* rootkey,RetType TYPE,const char* tradecode );	
		
		
		std::shared_ptr<CGTARecieverMgr>  GetCGTARecieverMgr();
		std::shared_ptr<CGTAListener> GetCGTAListener();
		CGTATradeApiHandler* GetCGTATradeApiHandler();
		std::shared_ptr<CGTASenderMgr> GetCGTASenderMgr();
		std::shared_ptr<CThreadSafeBuffPool> GetShareDataPool();

		
	private:
		CGTATradeApiHandler* m_phandler;
		std::shared_ptr<CGTAListener> m_listener;
		std::shared_ptr<CGTARecieverMgr> m_revievermgr;
		std::shared_ptr<CGTASenderMgr> m_sendermgr;
		std::shared_ptr<CThreadSafeBuffPool> m_bufferpool;
		
	};
}
#endif