#ifndef _I_QUERY_ABLE_H_
#define _I_QUERY_ABLE_H_ 
#include <string>
//方便程序运行时查询对象运行状态
class IQueryable
{
	virtual std::string GetQueryInfo()=0;	
};
#endif