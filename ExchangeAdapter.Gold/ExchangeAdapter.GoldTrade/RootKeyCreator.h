﻿
#ifndef UNIQKEY_CREATOR_H
#define UNIQKEY_CREATOR_H

#include <string>
namespace gta
{
	/**
	*对于发往金交所的每个交易，都有一个唯一的key记录(金交异步所返回时，会含带这个key，以标志对应的交易)
	*/
	class CRootKeyCreator
	{
	public:
		static std::string Create();
	};

}
#endif