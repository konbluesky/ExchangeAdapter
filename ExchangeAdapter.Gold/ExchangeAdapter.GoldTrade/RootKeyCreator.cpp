#include <windows.h>
#include "util.h"
#include "RootKeyCreator.h"

namespace gta
{	
	std::string CRootKeyCreator::Create()
	{
		static long serial= utils::UTimeHelper::GetUniqLong();
		::InterlockedIncrement(&serial);
		char tmp[32];
		sprintf(tmp,"%d",serial);
		return tmp;
	}
}