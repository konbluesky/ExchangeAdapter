#ifndef __GTA_LISTENER_H__
#define __GTA_LISTENER_H__

#include <memory>
#include"ThreadTask.h"

class ACE_SOCK_Acceptor;

namespace gta
{	
	class CGTATradeProvider;

	class CGTAListener : utils::ThreadTask//ACE_Task<ACE_MT_SYNCH>
	{
	public:
		CGTAListener(CGTATradeProvider*  provider);
		virtual ~CGTAListener();
		bool Start(int port);
		void Stop();
		virtual int svc();

	private:
		std::shared_ptr<ACE_SOCK_Acceptor> m_acceptor;
		CGTATradeProvider* m_provider;
		int m_port;
	};
}
#endif