#ifndef _GTA_RECIEVER_MGR_H_
#define _GTA_RECIEVER_MGR_H_
#include <map>
#include <string>
#include <memory>
#include "threadlock.h"
#include "IQueryable.h"

namespace gta
{
	class CGTAReciever;

	class CGTARecieverMgr : IQueryable
	{
	public:
		virtual ~CGTARecieverMgr();

		void AddReviever(std::shared_ptr<CGTAReciever>& rec);
		std::pair<bool,std::shared_ptr<CGTAReciever> > FindReciever(std::string key);
		void RemoveReciever(std::string key);

		virtual std::string GetQueryInfo();


	private:
		std::map<std::string ,std::shared_ptr<CGTAReciever> > m_recievers;
		threadlock::multi_threaded_local m_lock;
	};
}
#endif
