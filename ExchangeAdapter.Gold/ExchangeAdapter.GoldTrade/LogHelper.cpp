#include <assert.h>
#include <stdarg.h>
#include "LogHelper.h"
#include "BufferPool.h"
using namespace utils ;

void WriteLog(const char* fname,ILog::LogType type,	const char* fmt,... )
{
	ILog& log = CLogMgr::Instance().GetLog(fname);
	//由于是变参类型，提高效率，在进行拼string时，就先检查是否要进行输出
	if(log.GetFavorite() & type)
	{
		char* data = CLogMgr::Instance().GetBufferPool()->Pop();		

		va_list pArg;
		va_start(pArg, fmt);
		vsprintf(data, fmt, pArg);
		va_end(pArg);

		int datalen = strlen(data);
		data[datalen] = '\n';
		data[datalen+1] = '\0';


		log.WriteLog(type,data,datalen+1);
	}
}



//WriteBlockLog(const char* fname,ILog::LogType type,char* block ,int blocklen);

void WriteBlockLog(const char* fname,ILog::LogType type,char* block ,int blocklen)
{
	ILog& log = CLogMgr::Instance().GetLog(fname);
	//由于是变参类型，提高效率，在进行拼string时，就先检查是否要进行输出
	if(log.GetFavorite() & type)
	{
		char* data = CLogMgr::Instance().GetBufferPool()->Pop();
		assert(blocklen <= CLogMgr::Instance().GetBufferPool()->GetSecSize());
		memcpy(data,block,blocklen);		
		log.WriteLog(type,data,blocklen);
	}
}

void UninitializeLog()
{
	CLogMgr::Instance().Close();
}

bool InitializeLog( const char* fname )
{
	bool ret = utils::CLogMgr::Instance().InitializeByXML(fname);
	return ret;
}

_privateCallStackLog::_privateCallStackLog( const char* fname,const char* msg )
{
	_thread = ::GetCurrentThreadId();
	_fname = fname;
	_msg = msg;
	WriteLog("CallStack",ILog::TALL,"T:%-5d run in  %-36s at %-16s",_thread,_msg.c_str(),UFileSystemHelper::GetName(_fname).c_str());
}

_privateCallStackLog::~_privateCallStackLog()
{
	WriteLog("CallStack",ILog::TALL,"T:%-5d run out %-36s at %-16s",_thread,_msg.c_str(),UFileSystemHelper::GetName(_fname).c_str());
}
