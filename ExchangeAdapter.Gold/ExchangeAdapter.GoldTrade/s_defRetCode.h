#ifndef S_DEFRETCODE__H___
#define S_DEFRETCODE__H___

//调用API之前报文转化错误编码
#define ERR_BEFOREAPI_CONVER	"C_0001"
//调用API以后报文转化错误编码
#define ERR_AFTERAPI_CONVER	"C_1001"

//系统繁忙
#define ERR_BUSY	"C_0004"

//API调用之前分配内存错误
#define ERR_BEFOREAPIMEM	"C_0005"

//API调用以后分配内存错误
#define ERR_AFTERAPIMEM	"C_1005"

//创建API服务器错
#define ERR_CRTAPI	"C_1000"

//接收数据超时
#define ERR_TIMEOUT	"C_9999"

//线路连接中断
#define ERR_LOSTCOM	"C_9998"

//发送数据失败,调API时返回
#define ERR_SNDCOM	"C_9997"

//内部通信失败,调API之前
#define ERR_INNER_COMM	"C_9996"


//其它错
#define ERR_OTHER  "C_9001"



#endif