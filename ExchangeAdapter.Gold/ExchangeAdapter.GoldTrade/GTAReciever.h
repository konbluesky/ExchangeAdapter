#ifndef _GTA_RECIEVER_H_
#define _GTA_RECIEVER_H_

#include <string>
#include <memory>
#include <ace/Task.h>
#include "ThreadTask.h"
#include "LogHelper.h"
class ACE_SOCK_Stream;
//class CTraderApi;
namespace gta
{
	class CGTATradeApiHandler;
	class CGTARecieverMgr;
	class CGTATradeProvider;
	
	class CGTAReciever : public utils::ThreadTask//ACE_Task<ACE_MT_SYNCH>
	{
	public:
		CGTAReciever(std::string key, std::shared_ptr<ACE_SOCK_Stream> p,CGTATradeProvider* pprovider);
		~CGTAReciever();
		
		bool Start();
		void Stop();
		virtual int svc();

		//注2011。10.新修改的gta报文形式
		int RecieveGTAMessage();

		int OldRecieveGTAMessage();
		
		std::string GetKey() const;

		int OldSendGTAMessage(char* buff,int len);

		
		


		void Dispose();
		

		
		

	private:
		std::string m_key;
		//char m_key[32];
		std::shared_ptr<ACE_SOCK_Stream> m_stream;
		
		
		
		CGTATradeProvider* m_pProvider;

		HANDLE m_oldhwaitreplay;
		char* m_oldreplydata;
		int m_oldreplaydatalen;
	};
}
#endif
