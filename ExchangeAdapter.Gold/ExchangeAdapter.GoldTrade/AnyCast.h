#ifndef  __ANY_CAST_H__
#define  __ANY_CAST_H__
#include <sstream>

template<typename T,typename I>
T AnyCast(I input)
{
	T ret;
	std::stringstream interpreter;

	if(interpreter << input)
	{		
		if (interpreter >> ret)
			return ret;
	}
	return ret;
}

template<typename I>
std::string AnyCastString(I input)
{
	return AnyCast<std::string,I>(input);
}

#endif