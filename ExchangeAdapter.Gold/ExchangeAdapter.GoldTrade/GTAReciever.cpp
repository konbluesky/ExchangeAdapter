#include <ace/SOCK_Stream.h>
#include "GTAReciever.h"
#include "GTATradeCodeMap.h"

#include "GTATradeApiHandler.h"
#include "GTARecieverMgr.h"
#include "GTAMsgConvert.h"
#include "LogHelper.h"
#include "GTATradeProvider.h"
#include "BufferPool.h"
namespace gta
{
	static ACE_Time_Value TIMEOUT = ACE_Time_Value(30);


	CGTAReciever::CGTAReciever( std::string key,std::shared_ptr<ACE_SOCK_Stream> p,CGTATradeProvider* pProvider)
	{
		m_pProvider = pProvider;
		m_key = key;
		m_stream = p;
		
		m_oldreplydata = NULL;
	}

	std::string CGTAReciever::GetKey() const
	{
		return m_key;
	}

	
	void CGTAReciever::Stop()
	{
		this->close(0);
	}

	int CGTAReciever::svc()
	{
		int ret = OldRecieveGTAMessage();//
		//int ret = RecieveGTAMessage();
		return ret;

	}
	
	void CGTAReciever::Dispose()
	{
		WriteLog("stdout",LTALL,"CGTAReciever::Dispose()");
		this->m_pProvider->GetCGTARecieverMgr()->RemoveReciever(this->GetKey());
	}

	bool CGTAReciever::Start()
	{
		if( !this->activate() )
			return true;
		else
			return false;
	}



	CGTAReciever::~CGTAReciever()
	{
		WriteLog("stdout",LTALL,"CGTAReciever::~CGTAReciever");
	}

	int CGTAReciever::RecieveGTAMessage()
	{
		try
		{
			char _buff[1024*8] = {0};
			//数据格式 【4位长度】【6位交易码】【数据区】
			char datalen[4] = {0};
			char tradecode[6+1] = {0};


			memset(_buff,0,sizeof(char)*1024*8);
			if ( m_stream->recv_n(datalen,4,&TIMEOUT) != -1)
			{
				int len = std::atoi(datalen);					
				int bufflen = len;

				//接收数据tradecode - data
				m_stream->recv_n(_buff,bufflen,&TIMEOUT);
				memcpy(tradecode,_buff,sizeof(char)*6);
				char  tmp[1024*32] = {0};

				memcpy(tmp,datalen,4);
				sprintf(&tmp[4],"%20s","RSP000000");
				sprintf(&tmp[24],"%40s","0");					

				memcpy(&tmp[64],_buff,bufflen-6);
				char param1[1024*8] = {0};


				std::string cv = CGTATradeCodeMap::Instance().m_map[tradecode].InString2Struct;



				int replylen = m_stream->send(tmp,bufflen+64-6); //TODO CHH 注：这里仅是临时测试
				CGTAMsgConvert::Instance().ConvertString2Struct(cv.c_str(),_buff,bufflen,param1,1024*8);
				int ret = this->m_pProvider->GetCGTATradeApiHandler()->Request(tradecode,param1,this->GetKey());

				m_stream->close();
			}
		}
		catch(std::exception& e)
		{
			WriteDetailLog("error",ILog::TALL,("GTAVReciever Throw exception: %s" ,e.what()) );
		}
		catch (...)
		{

		}
		Dispose();

		return 0;
	}

	
	//
	//注：对于老的的通讯模型，从这里接收数据，然后调用API，如果在限定时间内(60秒)response，则从此通道返回结果。
	//
	int CGTAReciever::OldRecieveGTAMessage()
	{
		char data[1024*64] = {0};
		try
		{
			
			
			m_stream->send("AGR",3);
			m_stream->recv_n(data,8,&TIMEOUT);//8 位版本信息
			m_stream->send("AGR",3);
			m_stream->recv_n(data,15); //9位终端号 + 6位交易码
			char buff_tradecode[7] = {0};
			memcpy(buff_tradecode,&data[9],6);
			if(!CGTATradeCodeMap::Instance().ContainsKey(buff_tradecode))
			{
				WriteLog("error",LTALL,"not found trade code map,the code is %s", buff_tradecode);
				WriteLog("stdout",LTALL,"not found trade code map,the code is %s", buff_tradecode);
				m_stream->send("REJ",3);
			}
			else
			{
				m_stream->send("AGR",3);
				char buff_datalen[4] = {0};
				
				m_stream->recv_n(buff_datalen,4,&TIMEOUT);
				int ilen = atoi(buff_datalen);
				m_stream->recv_n(data,ilen,&TIMEOUT);

				m_stream->send("AGR",3);
				m_stream->send("AGR",3); // for file sent confirm

				char param1[1024*8] = {0};
				CGTAMsgConvert::Instance().ConvertString2Struct(buff_tradecode,data	,ilen,param1,1024*8);
				int ret = this->m_pProvider->GetCGTATradeApiHandler()->Request(buff_tradecode,param1,this->GetKey());
			}	
			
		}
		catch(std::exception& e)
		{
			WriteDetailLog("error",ILog::TALL,("GTAVReciever Throw exception: %s" ,e.what()) );
		}
		catch (...)
		{

		}

		
		m_oldhwaitreplay  = CreateSemaphore(NULL,0,LONG_MAX,NULL);
		::WaitForSingleObject(m_oldhwaitreplay,60*1000);
		
		if(m_oldreplydata != NULL)	
		{
			WriteLog("stdout",LTALL,"得到交易所返回信息，反推从原路返回,");
			m_stream->send(m_oldreplydata,m_oldreplaydatalen);
			this->m_pProvider->GetShareDataPool()->Push(m_oldreplydata);
			m_stream->recv_n(data,3,&TIMEOUT);
		}
		else
		{
			WriteLog("stdout",LTALL,"没有得到反推信息，超时终结reciever");
		}
		
		::CloseHandle(m_oldhwaitreplay);

		m_oldhwaitreplay = NULL;
		m_stream->close();
		this->Dispose();		
		return 0;
	}

	int CGTAReciever::OldSendGTAMessage( char* buff,int len )
	{		
		m_oldreplaydatalen = len;
		m_oldreplydata = buff;
		if(m_oldhwaitreplay != NULL)
		{
			::ReleaseSemaphore(m_oldhwaitreplay,1,NULL);		
		}
		return 0 ;
	}

}