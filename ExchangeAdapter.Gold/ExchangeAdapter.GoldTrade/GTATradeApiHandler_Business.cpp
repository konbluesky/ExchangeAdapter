#include <algorithm>
#include "GTASender.h"
#include "GTASenderMgr.h"
#include "GTATradeProvider.h"
#include <windows.h>
#include "GTAMsgConvert.h"
#include "GTATradeApiHandler.h"
//#include "mybase.h"
#include "s_defRetCode.h"
#include "BufferPool.h"
#include "LogHelper.h"
#include "GTATradeCodeMap.h"
#include <time.h>
namespace gta
{
/**********************************************************
* 函 数 名： CTrMyTrader::onRecvRspDeferOrder
* 功能描述：现货延期交收报单应答
*参数：
*- in_pstReqData      输入 --现货延期交收报单信息
*- in_pstRspMsg      输入 --请求执行结果和连续包标志
*- in_pszRootID      输入 --与请求包对应的唯一标示
* 返回说明： 无
* 开发历史：liuqy   于 20070903 创建
************************************************************/

void CGTATradeApiHandler::onRecvRspSpotOrder(TSpotOrder  * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID)
{
	WriteLog("stdout",LTALL,"CGTATradeApiHandler::onRecvRspSpotOrder");
	char*  szBuffer = this->pProvider->GetShareDataPool()->Pop();
	memset(szBuffer,32,1024*4);
	long len;
	CGTAMsgConvert::onRecvRspSpotOrder(in_pstReqData,in_pstRspMsg,szBuffer,&len);
	auto tradecode = gta::CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvRspSpotOrder");
	pProvider->ProcessData(szBuffer,len,in_pszRootID,CGTATradeProvider::RESPONSE,tradecode.c_str());
	//完成
}


void CGTATradeApiHandler::onRecvRtnSpotOrder(TSpotOrder * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID)
{
	WriteLog("stdout",LTALL,"CGTATradeApiHandler::onRecvRtnSpotOrder");
	char*  szBuffer = this->pProvider->GetShareDataPool()->Pop();
	memset(szBuffer,32,1024*4);
	long len;
	CGTAMsgConvert::onRecvRtnSpotOrder(in_pstReqData,in_pstRspMsg,szBuffer,&len);
	auto tradecode = gta::CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvRspSpotOrder");
	pProvider->ProcessData(szBuffer,len,in_pszRootID,CGTATradeProvider::RETURN,tradecode.c_str());

}

void CGTATradeApiHandler::onRecvRtnSpotMatch(TSpotMatch * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID)
{
	WriteLog("stdout",LTALL,"CGTATradeApiHandler::onRecvRtnSpotMatch");
	char*  szBuffer = this->pProvider->GetShareDataPool()->Pop();
	memset(szBuffer,32,1024*4);
	long len;
	CGTAMsgConvert::onRecvRtnSpotMatch(in_pstRspData,in_pstRspMsg,szBuffer,&len);
	auto tradecode = gta::CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvRtnSpotMatch");
	pProvider->ProcessData(szBuffer,len,in_pszRootID,CGTATradeProvider::RETURN,tradecode.c_str());
}


//现货撤单
void CGTATradeApiHandler::onRecvRspSpotOrderCancel(TSpotOrderCancel * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID)
{
	WriteLog("stdout",LTALL,"CGTATradeApiHandler::onRecvRspSpotOrderCancel");
	char*  szBuffer = this->pProvider->GetShareDataPool()->Pop();
	memset(szBuffer,32,1024*4);
	long len;
	CGTAMsgConvert::onRecvRspSpotOrderCancel(in_pstRspData,in_pstRspMsg,szBuffer,&len);
		auto tradecode = gta::CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvRspSpotOrderCancel");
	pProvider->ProcessData(szBuffer,len,in_pszRootID,CGTATradeProvider::RESPONSE,tradecode.c_str());
}

//现货保单查询
void CGTATradeApiHandler::onRecvRspQrySpotOrder(TSpotOrder * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID)
{
	WriteLog("stdout",LTALL,"CGTATradeApiHandler::onRecvRspQrySpotOrder");
	char*  szBuffer = this->pProvider->GetShareDataPool()->Pop();
	memset(szBuffer,32,1024*4);
	long len;
	CGTAMsgConvert::onRecvRspQrySpotOrder(in_pstRspData,in_pstRspMsg,szBuffer,&len);
	auto tradecode = gta::CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvRspQrySpotOrder");
	pProvider->ProcessData(szBuffer,len,in_pszRootID,CGTATradeProvider::RESPONSE,tradecode.c_str());

}
//现货成交查询
void CGTATradeApiHandler::onRecvRspQrySpotMatch(TSpotMatch * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID)
{
	WriteLog("stdout",LTALL,"CGTATradeApiHandler::onRecvRspQrySpotMatch");
	char*  szBuffer = this->pProvider->GetShareDataPool()->Pop();
	memset(szBuffer,32,1024*4);
	long len;
	CGTAMsgConvert::onRecvRspQrySpotMatch(in_pstRspData,in_pstRspMsg,szBuffer,&len);
	auto tradecode = gta::CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvRspQrySpotMatch");
	pProvider->ProcessData(szBuffer,len,in_pszRootID,CGTATradeProvider::RESPONSE,tradecode.c_str());

}

//现货撤单回报
void CGTATradeApiHandler::onRecvRtnSpotOrderCancel(TSpotOrder * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID)
{
	WriteLog("stdout",LTALL,"CGTATradeApiHandler::onRecvRtnSpotOrderCancel");
	char*  szBuffer = this->pProvider->GetShareDataPool()->Pop();
	memset(szBuffer,32,1024*4);
	long len;
	CGTAMsgConvert::onRecvRtnSpotOrderCancel(in_pstRspData,in_pstRspMsg,szBuffer,&len);
	auto tradecode = gta::CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvRtnSpotOrderCancel");
	pProvider->ProcessData(szBuffer,len,in_pszRootID,CGTATradeProvider::RETURN,tradecode.c_str());

}
//交易员登陆响应
void CGTATradeApiHandler::onRecvRspTraderLogin(TReqTraderLogin * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID)
{
	WriteLog("stdout",LTALL,"CGTATradeApiHandler:: onRecvRspTraderLogin been called");
	char*  szBuffer = this->pProvider->GetShareDataPool()->Pop();
	memset(szBuffer,32,1024*4);
	long len;
	CGTAMsgConvert::onRecvRspTraderLogin(in_pstReqData,in_pstRspMsg,szBuffer,&len);
	auto tradecode = gta::CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvRspTraderLogin");
	pProvider->ProcessData(szBuffer,len,in_pszRootID,CGTATradeProvider::RESPONSE,tradecode.c_str());

}
//交易员登出响应
void CGTATradeApiHandler::onRecvRspTraderLogout(TReqTraderLogout * in_pstReqData, TTRspMsg * in_pstRspMsg, char* in_pszRootID)
{
	WriteLog("stdout",LTALL,"CGTATradeApiHandler::onRecvRspTraderLogout");
	char*  szBuffer = this->pProvider->GetShareDataPool()->Pop();
	memset(szBuffer,32,1024*4);
	long len;
	CGTAMsgConvert::onRecvRspTraderLogout(in_pstReqData,in_pstRspMsg,szBuffer,&len);
	auto tradecode = gta::CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvRspTraderLogout");
	pProvider->ProcessData(szBuffer,len,in_pszRootID,CGTATradeProvider::RESPONSE,tradecode.c_str());

}

//市场信息回报
void CGTATradeApiHandler::onRecvMarketInfo(TMarket * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID)
{
	WriteLog("stdout",LTALL,"CGTATradeApiHandler::onRecvMarketInfo");
	char*  szBuffer = this->pProvider->GetShareDataPool()->Pop();
	memset(szBuffer,32,1024*4);
	long len;
	CGTAMsgConvert::onRecvMarketInfo(in_pstRspData,in_pstRspMsg,szBuffer,&len);
	auto tradecode = gta::CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvMarketInfo");
	pProvider->ProcessData(szBuffer,len,in_pszRootID,CGTATradeProvider::RETURN,tradecode.c_str());

}

//现货合约信息回报
void CGTATradeApiHandler::onRecvSpotInstInfo(TSpotInst * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID)
{
	WriteLog("stdout",LTALL,"CGTATradeApiHandler::onRecvSpotInstInfo");
	char*  szBuffer = this->pProvider->GetShareDataPool()->Pop();
	memset(szBuffer,32,1024*4);
	long len;
	CGTAMsgConvert::onRecvSpotInstInfo(in_pstRspData,in_pstRspMsg,szBuffer,&len);
	//CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName()
	auto tradecode = gta::CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvSpotInstInfo");
	pProvider->ProcessData(szBuffer,len,in_pszRootID,CGTATradeProvider::RETURN,tradecode.c_str());

}

//现货合约状态改变

void CGTATradeApiHandler::onRecvRtnSpotInstStateUpdate(TInstState * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID)
{
	WriteLog("stdout",LTALL,"CGTATradeApiHandler::onRecvRtnSpotInstStateUpdate");
	char*  szBuffer = this->pProvider->GetShareDataPool()->Pop();
	memset(szBuffer,32,1024*4);
	long len;
	CGTAMsgConvert::onRecvRtnSpotInstStateUpdate(in_pstRspData,in_pstRspMsg,szBuffer,&len);
	auto tradecode = gta::CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvRtnSpotInstStateUpdate");
	pProvider->ProcessData(szBuffer,len,in_pszRootID,CGTATradeProvider::RETURN,tradecode.c_str());

}

//现货市场状态改变
void CGTATradeApiHandler::onRecvRtnSpotMarketStateUpdate(TMarketState * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID)
{
	static int i = 0;//TODO
	i++;
	if (i>1)
		return ;
	WriteLog("stdout",LTALL,"CGTATradeApiHandler::onRecvRtnSpotMarketStateUpdate");
	char*  szBuffer = this->pProvider->GetShareDataPool()->Pop();
	memset(szBuffer,32,1024*4);
	long len;
	CGTAMsgConvert::onRecvRtnSpotMarketStateUpdate(in_pstRspData,in_pstRspMsg,szBuffer,&len);
	auto tradecode = gta::CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvRtnSpotMarketStateUpdate");
	pProvider->ProcessData(szBuffer,len,in_pszRootID,CGTATradeProvider::RETURN,tradecode.c_str());

}

//现货行情

void CGTATradeApiHandler::onRecvSpotQuotation(TSpotQuotation * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID)
{
	WriteLog("stdout",LTALL,"CGTATradeApiHandler::onRecvSpotQuotation");
	static int i = 0 ;   //TODO
	i++;
	if (i>1)
	{
		return;
	}
	bool blNeedProcess = false;
	char*  szBuffer = this->pProvider->GetShareDataPool()->Pop();
	memset(szBuffer,32,1024*4);
	long len;
	std::vector<TNtfQuotation>& vSpotQuotation = this->GetSpotQuotation();

	char sNowTime[128];
	/*std::vector<TNtfQuotation>& vDeferQuotation = this->GetDeferQuotation();	*/


	for (std::vector<TNtfQuotation>::size_type it = 0;it!=vSpotQuotation.size();it++ )
	{
		if (strcmp(vSpotQuotation[it].szInstID,in_pstRspData->instID) == 0)
		{
			if (0<strcmp(in_pstRspData->quoteTime , this ->m_szBeginUnlimited1.c_str())&& strcmp(in_pstRspData->quoteTime ,this->m_szEndUnlimited1.c_str())< 0 )
			{
				blNeedProcess = true;
			}
			else
			{
				if (0<strcmp(in_pstRspData->quoteTime, this->m_szBeginUnlimited2.c_str()) && strcmp(in_pstRspData->quoteTime , this->m_szEndUnlimited2.c_str())<0)
				{
					blNeedProcess = true;
				}
				else
				{
					if (0<strcmp(in_pstRspData->quoteTime, this->m_szBeginUnlimited3.c_str()) && strcmp(in_pstRspData->quoteTime ,this->m_szEndUnlimited3.c_str())<0)
					{
						blNeedProcess = true;
					}
					else
					{

						::_strtime(sNowTime);
						if (atol(sNowTime) - vSpotQuotation[it].lLastNtfTimeSecond>=this->m_lNtfQuoationWaitSecond)
						{
							blNeedProcess = true;
							vSpotQuotation[it].lLastNtfTimeSecond = atol(sNowTime);
						}
					}
				}

			}
			break;
		}
	}

	if (!blNeedProcess)
	{
		WriteLog("stdout",ILog::TALL,"现货行情[%s]不处理",(char*)in_pstRspData->instID);
		return;
	}
	CGTAMsgConvert::onRecvSpotQuotation(in_pstRspData,in_pstRspMsg,szBuffer,&len);
	auto tradecode = gta::CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvSpotQuotation");
	pProvider->ProcessData(szBuffer,len,in_pszRootID,CGTATradeProvider::RETURN,tradecode.c_str());

}
//交割品种代码信息

void CGTATradeApiHandler::onRecvVarietyInfo(TVariety * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID)
{
	WriteLog("stdout",LTALL,"CGTATradeApiHandler::onRecvVarietyInfo");
	char*  szBuffer = this->pProvider->GetShareDataPool()->Pop();
	memset(szBuffer,32,1024*4);
	long len;
	CGTAMsgConvert::onRecvVarietyInfo(in_pstRspData,in_pstRspMsg,szBuffer,&len);
	auto tradecode = gta::CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvVarietyInfo");
	pProvider->ProcessData(szBuffer,len,in_pszRootID,CGTATradeProvider::RETURN,tradecode.c_str());

}

//延期委托报单

void CGTATradeApiHandler::onRecvRspDeferOrder(TDeferOrder  * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID)
{
	WriteLog("stdout",LTALL,"CGTATradeApiHandler::onRecvRspDeferOrder");
	char*  szBuffer = this->pProvider->GetShareDataPool()->Pop();
	memset(szBuffer,32,1024*4);
	long len;
	CGTAMsgConvert::onRecvRspDeferOrder(in_pstRspData,in_pstRspMsg,szBuffer,&len);
	auto tradecode = gta::CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvRspDeferOrder");
	pProvider->ProcessData(szBuffer,len,in_pszRootID,CGTATradeProvider::RESPONSE,tradecode.c_str());

}

/*
//延期客户持仓查询

void CGTATradeApiHandler::onRecvRspQryDeferClientPosi(TDeferClientPosi * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID)
{
char*  szBuffer = this->pProvider->GetShareDataPool()->Pop();
memset(szBuffer,32,1024*4);
long len;
CGTAMsgConvert::onRecvRspQryDeferClientPosi(in_pstRspData,in_pstRspMsg,szBuffer,&len);

pProvider->ProcessData(szBuffer,len,in_pszRootID);	

}
*/

//延期行情
void CGTATradeApiHandler::onRecvDeferQuotation(TDeferQuotation * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID)
{
	WriteLog("stdout",LTALL,"%s",(char*)in_pstRspData->instID);
	int i = 0 ;
	i++;
	if (i>1)
	{
		return;
	}
	char*  szBuffer = this->pProvider->GetShareDataPool()->Pop();
	memset(szBuffer,32,1024*4);
	char sNowTime[128];
	bool blNeedProcess = false;
	long len;
	std::vector<TNtfQuotation>& vDeferQuotation = this->GetDeferQuotation();

	for (std::vector<TNtfQuotation>::size_type it = 0;it!=vDeferQuotation.size();it++ )
	{
		if (strcmp(vDeferQuotation[it].szInstID ,in_pstRspData->instID))
		{
			if (0<strcmp(in_pstRspData->quoteTime , this->m_szBeginUnlimited1.c_str())&& strcmp(in_pstRspData->quoteTime ,this->m_szEndUnlimited1.c_str())< 0 )
			{
				blNeedProcess = true;
			}
			else
			{
				if (0<strcmp(in_pstRspData->quoteTime, this->m_szBeginUnlimited2.c_str()) && strcmp(in_pstRspData->quoteTime , this->m_szEndUnlimited2.c_str())<0)
				{
					blNeedProcess = true;
				}
				else
				{
					if (0<strcmp(in_pstRspData->quoteTime, this->m_szBeginUnlimited3.c_str()) && strcmp(in_pstRspData->quoteTime ,this->m_szEndUnlimited3.c_str())<0)
					{
						blNeedProcess = true;
					}
					else
					{
						::_strtime(sNowTime);
						if (atol(sNowTime) - vDeferQuotation[it].lLastNtfTimeSecond>=this->m_lNtfQuoationWaitSecond)
						{
							blNeedProcess = true;
							vDeferQuotation[it].lLastNtfTimeSecond = atol(sNowTime);
						}
					}
				}

			}
			break;
		}
	}

	if (!blNeedProcess)
	{
		WriteLog("stdout",LTALL,"延期行情[%s]不处理",(char*)in_pstRspData->instID);
		return;
	}
	CGTAMsgConvert::onRecvDeferQuotation(in_pstRspData,in_pstRspMsg,szBuffer,&len);
	auto tradecode = gta::CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvDeferQuotation");
	pProvider->ProcessData(szBuffer,len,in_pszRootID,CGTATradeProvider::RETURN,tradecode.c_str());

}

//延期报单反推

void CGTATradeApiHandler::onRecvRtnDeferOrder(TDeferOrder * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID)
{
	char*  szBuffer = this->pProvider->GetShareDataPool()->Pop();
	memset(szBuffer,32,1024*4);
	long len;
	CGTAMsgConvert::onRecvRtnDeferOrder(in_pstRspData,in_pstRspMsg,szBuffer,&len);
	auto tradecode = gta::CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvRtnDeferOrder");
	pProvider->ProcessData(szBuffer,len,in_pszRootID,CGTATradeProvider::RETURN,tradecode.c_str());

}
//延期成交反推
void CGTATradeApiHandler::onRecvRtnDeferMatch(TDeferMatch * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID)
{
	char*  szBuffer = this->pProvider->GetShareDataPool()->Pop();
	memset(szBuffer,32,1024*4);
	long len;
	CGTAMsgConvert::onRecvRtnDeferMatch(in_pstRspData,in_pstRspMsg,szBuffer,&len);
	auto tradecode = gta::CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvRtnDeferMatch");
	pProvider->ProcessData(szBuffer,len,in_pszRootID,CGTATradeProvider::RETURN,tradecode.c_str());

}

//延期撤单反推
void CGTATradeApiHandler::onRecvRtnDeferOrderCancel(TDeferOrder * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID)
{
	char*  szBuffer = this->pProvider->GetShareDataPool()->Pop();
	memset(szBuffer,32,1024*4);
	long len;
	CGTAMsgConvert::onRecvRtnDeferOrderCancel(in_pstRspData,in_pstRspMsg,szBuffer,&len);
	auto tradecode = gta::CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvRtnDeferOrderCancel");
	pProvider->ProcessData(szBuffer,len,in_pszRootID,CGTATradeProvider::RETURN,tradecode.c_str());

}
//延期市场状态改变
void CGTATradeApiHandler::onRecvRtnDeferMarketStateUpdate(TMarketState * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID)
{
	char*  szBuffer = this->pProvider->GetShareDataPool()->Pop();
	memset(szBuffer,32,1024*4);
	long len;
	CGTAMsgConvert::onRecvRtnDeferMarketStateUpdate(in_pstRspData,in_pstRspMsg,szBuffer,&len);
	auto tradecode = gta::CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvRtnDeferMarketStateUpdate");
	pProvider->ProcessData(szBuffer,len,in_pszRootID,CGTATradeProvider::RETURN,tradecode.c_str());
}

//延期合约信息
void CGTATradeApiHandler::onRecvDeferInstInfo(TDeferInst * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID)
{
	char*  szBuffer = this->pProvider->GetShareDataPool()->Pop();
	memset(szBuffer,32,1024*4);
	long len;
	CGTAMsgConvert::onRecvDeferInstInfo(in_pstRspData,in_pstRspMsg,szBuffer,&len);
	auto tradecode = gta::CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvDeferInstInfo");
	pProvider->ProcessData(szBuffer,len,in_pszRootID,CGTATradeProvider::RETURN,tradecode.c_str());
}

//延期合约状态改变
void CGTATradeApiHandler::onRecvRtnDeferInstStateUpdate(TInstState * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID)
{
	char*  szBuffer = this->pProvider->GetShareDataPool()->Pop();
	memset(szBuffer,32,1024*4);
	long len;
	CGTAMsgConvert::onRecvRtnDeferInstStateUpdate(in_pstRspData,in_pstRspMsg,szBuffer,&len);
	auto tradecode = gta::CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvRtnDeferInstStateUpdate");
	pProvider->ProcessData(szBuffer,len,in_pszRootID,CGTATradeProvider::RETURN,tradecode.c_str());
}

//延期费率
void CGTATradeApiHandler::onRecvDeferFeeRateUpdate(TDeferFeeRate * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID)
{
	char*  szBuffer = this->pProvider->GetShareDataPool()->Pop();
	memset(szBuffer,32,1024*4);
	long len;
	CGTAMsgConvert::onRecvDeferFeeRateUpdate(in_pstRspData,in_pstRspMsg,szBuffer,&len);
	auto tradecode = gta::CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvDeferFeeRateUpdate");
	pProvider->ProcessData(szBuffer,len,in_pszRootID,CGTATradeProvider::RETURN,tradecode.c_str());

}

//延期交割申报反推

void CGTATradeApiHandler::onRecvRtnDeferDeliveryAppOrder(TDeferDeliveryAppOrder * in_pstRspData, TTRspMsg * in_pstRspMsg, char* in_pszRootID)
{
	char*  szBuffer = this->pProvider->GetShareDataPool()->Pop();
	memset(szBuffer,32,1024*4);
	long len;
	CGTAMsgConvert::onRecvRtnDeferDeliveryAppOrder(in_pstRspData,in_pstRspMsg,szBuffer,&len);
	auto tradecode = gta::CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onRecvRtnDeferDeliveryAppOrder");
	pProvider->ProcessData(szBuffer,len,in_pszRootID,CGTATradeProvider::RETURN,tradecode.c_str());

}

void CGTATradeApiHandler::onChannelLost( char *ErrMsg )
{
	static time_t  lNowTime;
	static bool first = true;

	WriteLog("stdout",LTALL,"CGTATradeApiHandler::onChannelLost been called");

	::time(&lNowTime);
	if (first)
	{
		//第一次断开
		first = false;
		return;
	}
	else
	{
		//将产生线路中断时间,在2秒之内产生的OnChannelLost，将不处理
		if (abs(lNowTime - m_lastLostChannelTime) <2 )
		{
			m_lastLostChannelTime = lNowTime;
			WriteLog("MY_ERR", LTALL,"线路产生非真实的中断，不报告给二级系统");
			WriteLog("MY_NORMAL",LTALL, "线路产生非真实的中断，不报告给二级系统");
			return;
		}
		else
		{
			char  szBuffer[sizeof(TTRspMsg) + 32];
			memset(szBuffer, 0, sizeof(szBuffer));
			TTRspMsg*	pstTRspMsg = (TTRspMsg *)szBuffer;
			pstTRspMsg->Flag = 'L';
			pstTRspMsg->RspCode = ERR_LOSTCOM;
			pstTRspMsg->RspMsg=ErrMsg;
			long len = sizeof(szBuffer);
			CGTAMsgConvert::OnChannelLost(ErrMsg,szBuffer,&len);
			auto tradecode = gta::CGTATradeCodeMap::Instance().GetTradeCodeByOutFuncName("onChannelLost");
			pProvider->ProcessData(szBuffer,len,"",CGTATradeProvider::RETURN,tradecode.c_str());
		}
	}
}

void CGTATradeApiHandler::onException( char *ExceptionMsg )
{
	WriteLog("stdout",LTALL,"CGTATradeApiHandler::onException been called: %s",ExceptionMsg);
}

void CGTATradeApiHandler::onRecvSysErrorNtf( char * in_pszErrMsg )
{
}
}