#if !defined(__DSTRUCT___)
#define __DSTRUCT___

#include <WINSOCK.h>
#include <sys\timeb.h>
#include "CycleQueue.h"////zhugp #include "ICCycleQueue.h"
///////////////////////////
//系统宏定义
//////////////////////////
//最大允许的通道,即并发数量
#define	MAXCHANNEL		500
//最大允许的交易码
#define	MAXTRADE		500
//最大允许通信缓冲区大小
#define	MAXCOMMBUFSIZE	102400	
//最小限度通信缓冲区大小
#define MINCOMMBUFSIZE	4096
//最大允许队列大小2M
#define	MAXQUEUESIZE	2097152	
//最小限度队列大小1K
#define MINQUEUESIZE	1024
//最小等待时间1秒
#define MINWAITMILLISECOND	1000 
//最大等待时间30秒
#define MAXWAITMILLISECOND	30000 
//监控队列大小 20K
#define	MAXMONITORQUEUESIZE	20480
//监控队列名称 GoldMonitor
#define MONITORNAME	"Gold_Monitor"


//信道状态
typedef enum E_RUN_STATE
{
	None = 0,	//未使用
	Idle,		//空闲
	Ready,			//准备
	Busy,			//运行中
	TimeOut,		//超时
	
};
//IP地址Port端口号
typedef struct _tIPPort
{
	char	szIPAddr[21];	//IP地址
	long	lPort;			//端口号
	_tIPPort()
	{
		szIPAddr[21]= '\0';
		lPort		= 0;
	}
}TIPPORT;

//IP地址Port端口号
typedef struct _tIntIPPort
{
	long	nIPAddr;	//IP地址
	long	nPort;			//端口号
	_tIntIPPort()
	{
		nIPAddr		= 0;
		nPort		= 0;
	}
}INTIPPORT;

//交易客户端交易信息
typedef struct	_tTrClient
{
	char	szTrCode[8];	//交易码，最大7个字符
	char	szDeptNo[16];	//部门编号
	TIPPORT	stIpPort;	//IP地址Port端口号
	long	lTimeOut;		//超时时间
	long	lResendFlag;	//发送失败，重新发送标志
	_tTrClient()
	{
		szTrCode[8]	= '\0';	//交易码，最大7个字符
		szDeptNo[16]= '\0';	//部门编号
		lTimeOut	= 0;	//超时时间
		lResendFlag	= 0;	//发送失败，重新发送标志
	}
}TTRCLIENT;

//交易信息
typedef struct _tTrCode
{
	char	szTrCode[8];				// 交易码，最大7个字符
	char	szTrName[21];				// 交易名称，最大10个汉字
	char	szInMsgFunctionName[41];	// 调用的报文转换函数名称,名称最大40个字符
	char	szOutMsgFunctionName[41];	// 回调的报文转换函数名称,名称最大40个字符
	char	szInAPIFunctionName[41];	// 调用API函数名称，名称最大40个字符
	char	szOutAPIFunctionName[41];	// 回调API函数名称，名称最大40个字符
	long	lTimeOut;					// 超时时间
	_tTrCode()
	{
		szTrCode[8]					= '\0';
		szTrName[21]				= '\0';
		szInMsgFunctionName[41]		= '\0';
		szOutMsgFunctionName[41]	= '\0';
		szInAPIFunctionName[41]		= '\0';
		szOutAPIFunctionName[41]	= '\0';
		lTimeOut					= 0;
	}
}TTRCODE;

//RxOnline服务通道信息
typedef struct _TCHANNELSTATUS
{
	TTRCODE	*		pstTrCode;		//指向交易码列表
	SOCKET			HLSockCliConn;	//客户端套接字对象连接
	char			szTermNo[10];	//终端号
	char 			szProcSeqNo[10];	//唯一系统序列号，最大不能超过9位数，API调用时转入的ID号
	char	*		pszCommBuf;	//通信缓冲区
	char			szCommRcvFileName[33];			//通信接收文件名，不含路径，注意文件保存在$(HOME)/spool/下
	char			szCommSndFileName[33];			//通信Snd文件名，不含路径，注意文件保存在$(HOME)/spool/下
	char			szExtendFlag[11];	//扩展标志，字符0表示无，第0位表示返回报文是否转换，字符0表示可以转换，字符1表示pszCommBuf中直接为可发送数据，即可能是错误信息
	bool			blConnFlag;		//客户端连接标志，当为false时需调用套接字对象关闭函数
	bool			blFollowupCommPacket;	//还有后续通信包标志，当为false表示没有了，当为true表示系统还在等待后续包
	bool			blThreadRunFlag;		//线程运行标记	//add by liuqy 20080312 for 为保证所有的线程句柄都释放，即使线程正常退出也需要关闭线程
	long			lCommBufSize;	//通信缓冲区大小
	long			lCommBufDataLen;		//通信缓冲区数据长度，有后续通信包时，指定为后续数据保存缓冲区位置，第一包保存位置为0
	HANDLE			HLThreadHandle;	//运行线程句柄
	HANDLE			HLWaitResponeEvent;		//等待返回事件，注意发送数据等待之前必须清除事件
	struct timeb	stStartTime;	//开始时间
	struct timeb	stEndTime;		//结束时间
	E_RUN_STATE		eRunState;		//运行状态，idle
	CCycleQueue*    poRcvQueue;////	ICCycleQueue *	poRcvQueue;		//接收队列指针，用于接收API上送数据，因为API可能连续上传数据
	struct sockaddr_in stConnAddr;	//对方连接地址
	_TCHANNELSTATUS()
	{
		pstTrCode		= NULL;		//指向交易码列表
		HLSockCliConn	= 0;	//客户端套接字对象连接
		szTermNo[10]	= '\0';	//终端号
		szProcSeqNo[10]	= '\0';	//唯一系统序列号，最大不能超过9位数，API调用时转入的ID号
		pszCommBuf		= NULL;	//通信缓冲区
		szCommRcvFileName[33]= '\0';			//通信接收文件名，不含路径，注意文件保存在$(HOME)/spool/下
		szCommSndFileName[33]= '\0';			//通信Snd文件名，不含路径，注意文件保存在$(HOME)/spool/下
		szExtendFlag[11]= '\0';	//扩展标志，字符0表示无，第0位表示返回报文是否转换，字符0表示可以转换，字符1表示pszCommBuf中直接为可发送数据，即可能是错误信息
		blConnFlag		= false;		//客户端连接标志，当为false时需调用套接字对象关闭函数
		blFollowupCommPacket= false;	//还有后续通信包标志，当为false表示没有了，当为true表示系统还在等待后续包
		blThreadRunFlag	= false;		//线程运行标记	//add by liuqy 20080312 for 为保证所有的线程句柄都释放，即使线程正常退出也需要关闭线程
		lCommBufSize	= 0;	//通信缓冲区大小
		lCommBufDataLen	= 0;		//通信缓冲区数据长度，有后续通信包时，指定为后续数据保存缓冲区位置，第一包保存位置为0
		HLThreadHandle	= NULL;	//运行线程句柄
		HLWaitResponeEvent=NULL;		//等待返回事件，注意发送数据等待之前必须清除事件
		poRcvQueue		= NULL;		//接收队列指针，用于接收API上送数据，因为API可能连续上传数据
	}
}TCHANNELSTATUS;

//通知服务通道信息
typedef struct _TNOTIFYCHANNEL
{
	TTRCODE	*		pstTrCode;		//指向交易码列表
	char	*		pszCommBuf;	//通信缓冲区
	char			szCommRcvFileName[33];			//通信接收文件名，不含路径，注意文件保存在$(HOME)/spool/下
	char			szCommSndFileName[33];			//通信Snd文件名，不含路径，注意文件保存在$(HOME)/spool/下
	long			lCommBufSize;	//通信缓冲区大小
	long			lCommBufDataLen;		//通信缓冲区数据长度
	long			lSendTimes;		//当前交易发送次数
	long			lSuccessCount;		//成功发送次数add by liuqy 20080401 for统计成功发送数量
	bool			blThreadRunFlag;		//线程运行标记	//add by liuqy 20080312 for 为保证所有的线程句柄都释放，即使线程正常退出也需要关闭线程
	HANDLE			HLThreadHandle;	//运行线程句柄
	HANDLE			HLWaitWorkEvent;	//等待工作事件，因很多信息有顺序关系，顺序不正确会造成系统问题，线程工作顺序由系统决定的，为让线程工作顺序化，用此事件
	struct timeb	stStartTime;	//开始时间
	struct timeb	stEndTime;		//结束时间
	E_RUN_STATE		eRunState;		//运行状态，idle
	_TNOTIFYCHANNEL()
	{
		pstTrCode		= NULL;		//指向交易码列表
		pszCommBuf		= NULL;	//通信缓冲区
		szCommRcvFileName[33]= '\0';			//通信接收文件名，不含路径，注意文件保存在$(HOME)/spool/下
		szCommSndFileName[33]= '\0';			//通信Snd文件名，不含路径，注意文件保存在$(HOME)/spool/下
		lCommBufSize	= 0;	//通信缓冲区大小
		lCommBufDataLen	= 0;		//通信缓冲区数据长度
		lSendTimes		= 0;		//当前交易发送次数
		lSuccessCount	= 0;		//成功发送次数add by liuqy 20080401 for统计成功发送数量
		blThreadRunFlag	= false;		//线程运行标记	//add by liuqy 20080312 for 为保证所有的线程句柄都释放，即使线程正常退出也需要关闭线程
		HLThreadHandle	= NULL;	//运行线程句柄
		HLWaitWorkEvent	= NULL;	//等待工作事件，因很多信息有顺序关系，顺序不正确会造成系统问题，线程工作顺序由系统决定的，为让线程工作顺序化，用此事件
	}
}TNOTIFYCHANNEL;

#endif