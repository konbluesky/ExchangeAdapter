#include <windows.h>
#include "ThreadTask.h"

namespace utils
{
	int ThreadTask::activate()
	{
		m_hthread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)WorkThread, this, 0, NULL);
		return m_hthread != NULL ? 0 : -1;
	}

	DWORD ThreadTask::WorkThread(void* p)
	{
		ThreadTask* task = (ThreadTask*)p;
		::InterlockedExchange(&(task->m_existcode),STILL_ACTIVE);
		int ret =  task->svc();
		//::InterlockedExchange(&(task->m_existcode),ret);
		return ret;
	}
	
	//TODO ,ACE 在这里好像接口不是很好。
	void ThreadTask::close(DWORD existcode)
	{
		DWORD threadstate;
		::GetExitCodeThread(this->m_hthread,&threadstate);
		if(threadstate == STILL_ACTIVE)
		{
			::TerminateThread(this->m_hthread,existcode);
		}
	}

	bool ThreadTask::IsRunning() const
	{
		DWORD existcode;
		::GetExitCodeThread(this->m_hthread,&existcode);
		return existcode == STILL_ACTIVE;
	}

	DWORD ThreadTask::GetExistCode() const
	{
		DWORD existcode;
		::GetExitCodeThread(this->m_hthread,&existcode);
		return existcode;
	}


}