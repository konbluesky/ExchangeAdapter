#include "RootKeyCreator.h"
#include "GTASender.h"
#include "GTASenderMgr.h"

namespace gta
{
	std::shared_ptr<CGTASender> CGTASenderMgr::GetSender()
	{

		auto sender =  std::shared_ptr<CGTASender> (new CGTASender(CRootKeyCreator::Create(),m_addr,m_port,m_pProvider));
		threadlock::autoguard<threadlock::multi_threaded_local> _autogurad(&m_lock);
		m_map[sender->GetKey()] = sender;
		return sender;
	}
	std::shared_ptr<CGTASender> CGTASenderMgr::GetSpecialSender(std::string addr,int port)
	{

		auto sender =  std::shared_ptr<CGTASender> (new CGTASender(CRootKeyCreator::Create(),addr,port,m_pProvider));
		threadlock::autoguard<threadlock::multi_threaded_local> _autogurad(&m_lock);
		m_map[sender->GetKey()] = sender;
		return sender;
	}
	
	CGTASenderMgr::CGTASenderMgr(std::string addr,int port,CGTATradeProvider* pProvider)
	{
		m_addr = addr;
		m_port = port;
		m_pProvider = pProvider;
	}
	void CGTASenderMgr::RemoveSender(std::string key)
	{
		threadlock::autoguard<threadlock::multi_threaded_local> _autogurad(&m_lock);
		auto it = m_map.find(key);
		if(it != m_map.end())
		{
			m_map.erase(it);
		}
	}

	ACE_Stream* CGTASenderMgr::GetGTASharedStream()
	{
		return this->m_gtasharedstream;
	}

	void CGTASenderMgr::SetGTASharedStream( ACE_Stream* pstream )
	{
		m_gtasharedstream = pstream;
	}
}