//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ExchangeAdapter.GoldTrade.Test.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDD_MAINDLG                     129
#define IDC_IPADDRESS_DEST              1000
#define IDC_EDIT_PORT                   1001
#define IDC_BUTTON_SEND                 1002
#define IDC_EDIT_DATA                   1003
#define IDC_BUTTON_SEND2                1004
#define IDC_EDIT_ADDR                   1005
#define IDC_EDIT_THREADS                1006
#define IDC_EDIT_ROUNDS                 1007
#define IDC_EDIT1                       1008
#define IDC_BUTTON1                     1009

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        201
#define _APS_NEXT_COMMAND_VALUE         32775
#define _APS_NEXT_CONTROL_VALUE         1010
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
