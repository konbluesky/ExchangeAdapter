#ifndef _SHARED_QUEUE_H_
#define _SHARED_QUEUE_H_
#include <list>
#include <windows.h>
#include "threadlock.h"

class BlockableQueueClosedException : public std::exception
{
public:
	BlockableQueueClosedException(const char * const & data) : std::exception(data)
	{
	}
};

///
///注:这里的代码实现使用两个信号量(windows里没办法，linux的ptread里有cond函数)，容易引起互锁，所以某些函数不是足够的线程安全
///注:这个类的特征是：如果没有数据，则Pop时会block
///
template<typename T, class mt_policy = threadlock::multi_threaded_local>
class BlockableQueue
{	
public:
	
	BlockableQueue()
	{
		m_hhasData = CreateSemaphore(NULL,0,LONG_MAX,NULL);
		m_opened = true;
	}

	~BlockableQueue()
	{
		Close();
	}

	void Enqueue(T v)
	{
		threadlock::autoguard<mt_policy> a(&m_lock);
			
		if (!m_opened)
			return;
		m_datas.push_back(v);	
		
		ReleaseSemaphore(m_hhasData,1,NULL);	
	}

	///注：在某些情况，如程序需要安全退出，可以往队列最前面插入一个脏数据，然后consumer线程立即得到一个脏数据，即可安全实现break, consumer线程不要用TerminateThread这种不安全的方式结束)
	void InsertFirst(T v)
	{
		threadlock::autoguard<mt_policy> a(&m_lock);

		if (!m_opened)
			return;

		_datas.push_front(v);

		ReleaseSemaphore(_hhasData,1,NULL);		
	}

	///查看数据，并不pop数据出来,当数据池内没有数据，返回NULL,
	T* Peek()
	{
		threadlock::autoguard<mt_policy> a(&m_lock);
			
		if (_datas.size() == 0)
		{
			return NULL;
		}
		else
		{
			return &_datas.front();//不能用用*_datas.begin(),这个里面并不返回链表里对象的真实地址			
		}		
	}

	int Count()
	{
		threadlock::autoguard<mt_policy> a(&m_lock);
		return  m_datas.size();
	}	
	
	T Dequeue()
	{		
		WaitForSingleObject(m_hhasData,INFINITE);
		threadlock::autoguard<mt_policy> a(&m_lock);

		if (!m_opened )
		{		
			throw BlockableQueueClosedException("the BlockableQueue Has Closed");
		}
		T v = m_datas.front();
		m_datas.pop_front();
		

		return v;		
	}


	void Close()
	{
		threadlock::autoguard<mt_policy> a(&m_lock);
		if (m_opened)
		{
			m_datas.clear();
			::CloseHandle(m_hhasData);
			m_opened = false;
			
		}		
	}

	
private:
	mt_policy m_lock;
	std::list<T> m_datas;
	HANDLE m_hhasData;	
	bool m_opened;
	
};



#endif
