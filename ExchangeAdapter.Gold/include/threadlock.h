#ifndef THREAD_LOCK_H__
#define THREAD_LOCK_H__
//		THREADING MODES
//
//			single_threaded				- Your program is assumed to be single threaded from the point of view
//										  of signal/slot usage (i.e. all objects using signals and slots are
//										  created and destroyed from a single thread). Behaviour if objects are
//										  destroyed concurrently is undefined (i.e. you'll get the occasional
//										  segmentation fault/memory exception).
//
//			multi_threaded_global		- Your program is assumed to be multi threaded. Objects using signals and
//										  slots can be safely created and destroyed from any thread, even when
//										  connections exist. In multi_threaded_global mode, this is achieved by a
//										  single global mutex (actually a critical section on Windows because they
//										  are faster). This option uses less OS resources, but results in more
//										  opportunities for contention, possibly resulting in more context switches
//										  than are strictly necessary.
//
//			multi_threaded_local		- Behaviour in this mode is essentially the same as multi_threaded_global,
//										  except that each signal, and each object that inherits has_slots, all
//										  have their own mutex/critical section. In practice, this means that
//										  mutex collisions (and hence context switches) only happen if they are
//										  absolutely essential. However, on some platforms, creating a lot of
//										  mutexes can slow down the whole OS, so use this option with care.



#if defined(SIGSLOT_PURE_ISO) || (!defined(WIN32) && !defined(__GNUG__) && !defined(SIGSLOT_USE_POSIX_THREADS))
#	define _SIGSLOT_SINGLE_THREADED
#elif defined(WIN32)
#	define _SIGSLOT_HAS_WIN32_THREADS
#	include <windows.h>
#elif defined(__GNUG__) || defined(SIGSLOT_USE_POSIX_THREADS)
#	define _SIGSLOT_HAS_POSIX_THREADS
#	include <pthread.h>
#else
#	define _SIGSLOT_SINGLE_THREADED
#endif

#ifndef SIGSLOT_DEFAULT_MT_POLICY
#	ifdef _SIGSLOT_SINGLE_THREADED
#		define SIGSLOT_DEFAULT_MT_POLICY single_threaded
#	else
#		define SIGSLOT_DEFAULT_MT_POLICY multi_threaded_local
#	endif
#endif
namespace threadlock
{
	class single_threaded
	{

	public:
		single_threaded()
		{
			;
		}

		virtual ~single_threaded()
		{
			;
		}

		void lock ()
		{
			;
		}

		void unlock()

		{
			;
		}
	};

#ifdef _SIGSLOT_HAS_WIN32_THREADS
	// The multi threading policies only get compiled in if they are enabled.

	class multi_threaded_global
	{

	public:
		multi_threaded_global()
		{
			static bool isinitialised = false;

			if (!isinitialised)
			{
				InitializeCriticalSection(get_critsec());
				isinitialised = true;
			}
		}

		multi_threaded_global(const multi_threaded_global&)
		{
			;
		}

		virtual ~multi_threaded_global()

		{
			;
		}

		void lock ()
		{
			EnterCriticalSection(get_critsec());
		}

		void unlock()

		{
			LeaveCriticalSection(get_critsec());
		}

	private:
		CRITICAL_SECTION* get_critsec()
		{
			static CRITICAL_SECTION g_critsec;

			return &g_critsec;
		}
	};

	class multi_threaded_local
	{

	public:
		multi_threaded_local()
		{
			::InitializeCriticalSection(&m_critsec);
		}

		multi_threaded_local(const multi_threaded_local&)
		{
			::InitializeCriticalSection(&m_critsec);
		}

		virtual ~multi_threaded_local()

		{
			::DeleteCriticalSection(&m_critsec);
		}

		void lock ()
		{
			::EnterCriticalSection(&m_critsec);
		}

		void unlock()

		{
			::LeaveCriticalSection(&m_critsec);
		}

	private:
		CRITICAL_SECTION m_critsec;
	};

#endif // _SIGSLOT_HAS_WIN32_THREADS

#ifdef _SIGSLOT_HAS_POSIX_THREADS
	// The multi threading policies only get compiled in if they are enabled.

	class multi_threaded_global
	{

	public:
		multi_threaded_global()
		{
			pthread_mutex_init(get_mutex(), NULL);
		}

		multi_threaded_global(const multi_threaded_global&)
		{
			;
		}

		virtual ~multi_threaded_global()

		{
			;
		}

		void lock ()
		{
			pthread_mutex_lock(get_mutex());
		}

		void unlock()

		{
			pthread_mutex_unlock(get_mutex());
		}

	private:
		pthread_mutex_t* get_mutex()
		{
			static pthread_mutex_t g_mutex;

			return &g_mutex;
		}
	};

	class multi_threaded_local
	{

	public:
		multi_threaded_local()
		{
			pthread_mutex_init(&m_mutex, NULL);
		}

		multi_threaded_local(const multi_threaded_local&)
		{
			pthread_mutex_init(&m_mutex, NULL);
		}

		virtual ~multi_threaded_local()
		{
			pthread_mutex_destroy(&m_mutex);
		}

		void lock ()
		{
			pthread_mutex_lock(&m_mutex);
		}

		void unlock()
		{
			pthread_mutex_unlock(&m_mutex);
		}

	private:
		pthread_mutex_t m_mutex;
	};


#ifdef WIN32

	///注，这个使用Mutex(进程间全局有效)
	class multi_processed
	{		
	public:		
		multi_processed(HANDLE handler)
		{
			_handler = handler;
		}
		void lock ()
		{
			::WaitForSingleObject(_handler)
		}
		
		void unlock()
		{
			::ReleaseMutex(_handler);
		}		
	private:
		HANDLE _handler;
	};

#endif

#endif // _SIGSLOT_HAS_POSIX_THREADS

	

	template<class mt_policy>
	class autoguard
	{

	public:
		mt_policy *m_mutex;

		autoguard(mt_policy *mtx)
			: m_mutex(mtx)
		{
			m_mutex->lock ();
		}

		~autoguard()
		{
			m_mutex->unlock();
		}
	};

};//namespace of threadlock
#endif