 //==================================================
 /**
 *	@file	Singleton.h
 *
 *	purpose:Pattern:Singleton
 *	 
 *	@author	enigma
 *
 *	@date	2006/10/16		 
 */
 //==================================================
#ifndef SINGLETON_H
#define SINGLETON_H

namespace simpattern
{
	////////////////////////////////////////////////////////
	/**
	 对象实例定义在类中,注：此基本线程安全
	**/
	template<class T>class Singleton_InClass
	{
		private:
			static T _obj;
		public:
			static T& Instance()
			{
				return _obj;
			}
	};

	template<class T>	T Singleton_InClass<T>::_obj;





	////////////////////////////////////////////////////////
	/**
	对象实例定义在静态函数中,注,此并不线程安全
	**/
	template<class T>
	class Singleton_InFunc
	{
	public:
		static T& Instance()
		{
			static T _obj;
			return _obj;
		}
	};
}


#endif