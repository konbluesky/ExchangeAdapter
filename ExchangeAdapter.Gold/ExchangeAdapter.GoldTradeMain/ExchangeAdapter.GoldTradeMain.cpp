// ExchangeAdapter.GoldTradeMain.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
int _tmain(int argc ,char* argv)                     
{
	
	HMODULE h= ::LoadLibraryA("ExchangeAdapter.GoldTrade.dll");
	if (h != NULL)
	{
		typedef int ( *FUNC)();

		FUNC p = (FUNC)GetProcAddress(h,"exportmain");
		if(p != NULL)
			p();
	}
	return 0;
}

