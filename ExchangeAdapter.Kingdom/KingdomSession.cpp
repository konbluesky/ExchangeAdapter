#include "stdafx.h"
#include "ucodehelper.h"
#include "ustringutils.h"

#include "usystemutils.h"
#include "unetutils.h"
#include "umaths.h"
#include "ucomhelper.h"

#include "ExchangeAdapterDef.h"
#include "KingdomSession.h"
#include "KingdomHelper.h"

#include "tdataset.h"
#include "tdatasets.h"
#include "KingdomTradeAdapter.h"
#pragma warning(disable: 4482)
USING_LIBSPACE

namespace tradeadapter
{
	namespace kingdom
	{
		CKingdomSession::CKingdomSession(CKingdomTradeAdapter* parent, KCBPCLIHANDLE handler ,const KingdomParamA& param)
		{
			m_parant = parent;
			m_hHandle = handler;
			m_param = shared_ptr<KingdomParamA>(new KingdomParamA);
			memcpy(m_param.get() ,& param,sizeof(KingdomParamA));
		}

		CKingdomSession::~CKingdomSession(void)
		{
		}

		bool CKingdomSession::SetFieldValue( const char* key, int val )
		{
			char buff [32] ={0};
			sprintf(buff,"%d",val);
			return SetFieldValue(key,buff);
		}

		bool CKingdomSession::SetFieldValue( const char* key, double val )
		{
			char buff [32] ={0};
			sprintf(buff,"%9.3f",val);
			return SetFieldValue(key,buff);
		}

		bool CKingdomSession::SetFieldValue( const char* key, const char* val )
		{
			int ret = KCBPCLI_SetValue(m_hHandle,(char*)key,(char*)val);
			return ret==0 ? true : false;
		}

		bool CKingdomSession::SetFieldValue( const char* key, char val )
		{
			char buff [32] ={0};
			sprintf(buff,"%c",val);
			return SetFieldValue(key,buff);
		}

		bool CKingdomSession::SetFieldValue( const char* key, const std::string& val )
		{
			return SetFieldValue(key,val.c_str());
		}

		std::string CKingdomSession::GetFieldValue( const char* key )
		{
			char szResult[256] = {0};
			int nRet = KCBPCLI_GetValue(m_hHandle,(char*)key, szResult,255);
			return szResult;
		}

		

		

		bool CKingdomSession::BeginJob()
		{
			int nReturnCode = KCBPCLI_BeginWrite(m_hHandle);

			//this function must be called, the third param defined as orgid, but actually it's can be set any num.
			if(	KCBPCLI_SetSystemParam(m_hHandle, KCBP_PARAM_RESERVED, "0") )
			{
				//Log("KCBPCLI_SetSystemParam error\n");
				return false;
			}
			else
			{
				//cout<<"Set SystemParam OK\n";			
			}
			return true;
		}

		bool CKingdomSession::CommitJob( const char* funcid )
		{
			SetFieldValue("funcid",funcid);
			int ret = KCBPCLI_CallProgramAndCommit(m_hHandle,(char*)funcid);//提交采用同步方式
			return ret ==0;
		}

		bool CKingdomSession::_getRowDatas( libutils::TDataSet* ret )
		{
			INT ncol = 0;
			unsigned char* buff = NULL;// = new unsigned char[256];
			long bufflen = 0;
			KCBPCLI_RsGetColNum(m_hHandle,&ncol);

			for (int i=1; i<=ncol ; ++i)
			{
				char name[1024];
				KCBPCLI_RsGetColName(m_hHandle,i,name,1024);

				KCBPCLI_RsGetVal(m_hHandle,i,&buff,&bufflen);
				(*ret)[name] = (char*)buff;			
			}
			return true;
		}

		bool CKingdomSession::GetDataResultS( libutils::TDataSets* res )
		{
			//这个函数返回0表示有后继结果集
			
			while(!KCBPCLI_RsMore(m_hHandle))
			{
				while(!KCBPCLI_RsFetchRow(m_hHandle))
				{
					TDataSet* set  = new TDataSet;
					_getRowDatas(& (*set));
					res->m_bags.push_back(shared_ptr<TDataSet>(set));
				}		
			}
			return true;
		}

		bool CKingdomSession::SetBasicInfo(UseType ty )
		{
				char buff[1024] = {0};
				unsigned char password[1024] = {0};
				unsigned char * prig = (unsigned char*)m_param->trdpwd.c_str();

				int pwdlen = 0;
				switch(ty)
				{
				case UseType::Login:
					pwdlen = kingdom::UPasswordHelper::GetLoginPassword(prig,strlen((const char *)prig),password,1024);
					break;
				case UseType::Cust:
					pwdlen = kingdom::UPasswordHelper::GetCustPassword(prig,strlen((const char *)prig),password,1024,m_param->accountid.c_str());
					break;
				case UseType::Fund:
					pwdlen = kingdom::UPasswordHelper::GetfundPassword(prig,strlen((const char *)prig),password,1024,m_param->fundid.c_str());
					break;
				}

				unsigned char* trdpwd = password;
				char* ext = "0";	//this is must be 0，							
				SetFieldValue("custid",m_param->accountid.c_str());
				SetFieldValue("custorgid",m_param->orgid.c_str());
				SetFieldValue("trdpwd",(char *)trdpwd);
				SetFieldValue("orgid",m_param->orgid.c_str());
				SetFieldValue("operway",m_param->operway);

				
				SetFieldValue("ext",ext);
				

				static std::string addrip = GetNetAddr();
				SetFieldValue("cpusn",GetCPUID());
				SetFieldValue("loginip",addrip);
				SetFieldValue("autoflag",addrip);

				
				SetFieldValue("netaddr",addrip);
				static std::string mac = GetMac();
				SetFieldValue("mac",GetMac());
				//SetFieldValue("hddsn",GetHDID());  //comment this should chanage into below.
				static std::string netaddr2 = GetHDID();
				SetFieldValue("netaddr2",netaddr2);

				return true;			
		}

		std::string CKingdomSession::GetIP()
		{
			static std::string ip = TS2GB(UNetUtils::GetIpAddress().size() > 0 ? UNetUtils::GetIpAddress()[0] : _T("192.168.60.90"));
			return ip;
		}
		std::string CKingdomSession::GetNetAddr()
		{
			static std::string ip = GetIP() + ".";
			char ret[13] = "000000000000";
			
			for(int i=0; i<4; ++i)
			{
				int pos = ip.find(".");
				std::string data = ip.substr(0,pos);
				ip = ip.substr(pos+1);
				for(std::string::size_type j=0; j<data.size(); ++j)
				{
					ret[3*i+3-j-1] = data[data.size()-j-1];
				}
			}
			return ret;

		}

		std::string CKingdomSession::GetHDID()
		{
			static std::string hdid = USystemUtils::GetHDID();
			if(hdid == "")
			{
				//std::cout<<"无法获取硬盘id,使用自定义字符串S26XJ9FB834843" <<std::endl;
				hdid = "DISKIDS26XJ9FB834843";
			}
			return hdid;
		}

		std::string CKingdomSession::GetCPUID()
		{
			static std::string cpuid = USystemUtils::GetCPUID();
			return cpuid;

		}

		std::string CKingdomSession::GetMac()
		{
			tstring ret =  UNetUtils::GetMacAddress() ;
			ret = USOpeUtils::EraseAllChar(ret,_T(':'));
			std::string retx = TS2GB(ret);
			return retx;


			
		}

		bool CKingdomSession::GetStatusResult()
		{
			int ret = KCBPCLI_RsOpen(m_hHandle);
			KCBPCLI_RsFetchRow(m_hHandle);
			TDataSet res;
			_getRowDatas(&res);
			if(res["CODE"] == "0")
			{
				return true;
			}
			else
			{
				std::string first = res["CODE"];
				std::string second = res["MSG"];

				m_parant->SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter, UMaths::ToInt32(first), second.c_str()));
				std::cout<<"出错"<<res["CODE"]<<res["MSG"]<<std::endl;
				
				
				return false;
			}
		}
	}
};
