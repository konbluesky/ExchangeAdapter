#include "ustringutils.h"
#include "string.h"
#include "PasswordHelper.h"
#include "KDEncodeCli.h"
#include "KingdomDef.h"
namespace tradeadapter
{
	namespace kingdom
	{
		int UPasswordHelper::GetLoginPassword(const unsigned char *pSrcData, int nSrcDataLen, 
			unsigned char *pDestData, int nDestDataBufLen)
		{
			const char* pKey = FunctionID::KDF_LOGIN;
			int ret = _get_Password(pSrcData,  nSrcDataLen, pDestData,  nDestDataBufLen,pKey);
			return ret;
		}

		int UPasswordHelper::GetCustPassword( const unsigned char *pSrcData, int nSrcDataLen, unsigned char *pDestData, int nDestDataBufLen,const char* custid )
		{
			int ret = _get_Password(pSrcData,nSrcDataLen,pDestData,nDestDataBufLen,custid);
			return ret;
		}

		int UPasswordHelper::GetfundPassword( const unsigned char *pSrcData, int nSrcDataLen, unsigned char *pDestData, int nDestDataBufLen,const char* fundid )
		{
			int ret = _get_Password(pSrcData,  nSrcDataLen, pDestData,  nDestDataBufLen,fundid);
			return ret;
		}

		int UPasswordHelper::_get_Password( const unsigned char *pSrcData, int nSrcDataLen, unsigned char *pDestData, int nDestDataBufLen,const char* pKey )
		{
			int nKeyLen = strlen(pKey);
			unsigned char buff[1024];
			memcpy(buff,pSrcData,sizeof(char)*nSrcDataLen );

			int resultlen =KDEncode(KDCOMPLEX_ENCODE,
				buff, nSrcDataLen, 
				pDestData, nDestDataBufLen, 
				(void *)pKey,nKeyLen);
			return resultlen;

		}

		std::string UPasswordHelper::GetPwd( UPasswordHelper::UseType ty, std::string oripwd,std::string custid,std::string fundid )
		{
			char buff[1024] = {0};
			unsigned char password[1024] = {0};
			unsigned char * prig = (unsigned char*)oripwd.c_str();
			int pwdlen = 0;

			switch(ty)
			{
			case UPasswordHelper::Login:
				pwdlen = kingdom::UPasswordHelper::GetLoginPassword(prig,strlen((const char *)prig),password,1024);
				break;
			case UPasswordHelper::Cust:
				pwdlen = kingdom::UPasswordHelper::GetCustPassword(prig,strlen((const char *)prig),password,1024,custid.c_str());
				break;
			case UPasswordHelper::Fund:
				pwdlen = kingdom::UPasswordHelper::GetfundPassword(prig,strlen((const char *)prig),password,1024,fundid.c_str());
				break;
			}
			unsigned  char* trdpwd = password;
			return (char*)trdpwd;
		}
	}
}