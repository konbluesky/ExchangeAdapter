#ifndef __SCRITICAL_H__
#define __SCRITICAL_H__

//////////////////////////////////////////////////////////////////////////////
// �ٽ���
//////////////////////////////////////////////////////////////////////////////

//15801638238 - niu rui yao
class CCS
{
	CRITICAL_SECTION m_cs;
public:
	CCS()
	{
		InitializeCriticalSection( &m_cs );
	}

	~CCS()
	{
		DeleteCriticalSection( &m_cs );
	}

	void Enter()
	{
		EnterCriticalSection( &m_cs );
	}

	void Leave()
	{
		LeaveCriticalSection( &m_cs );
	}
private:
	operator = ();
	CCS(CCS&);

};


class CAutoGuard
{
public:
	CAutoGuard(CCS& cs):m_cs(cs)
	{
		m_cs.Enter();
	}
	~CAutoGuard()
	{
		m_cs.Leave();
	}

private:
	CCS& m_cs;
private:


};
#endif //__SCRITICAL_H__

