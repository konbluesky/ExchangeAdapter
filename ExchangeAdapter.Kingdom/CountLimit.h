
#include <list>
#include "ExchangeAdapterDef.h"
#include "threadlock.h"
class TAIAPI CountLimit
{
public:
	void SetLimit(int count);

	bool Enqueue();
	
private:
	#pragma warning(disable : 4251)
	int m_count;
	std::list<int> m_list;

	threadlock::multi_threaded_local m_lock;
};