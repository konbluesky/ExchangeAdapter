#ifndef _PASSWORD_HELPER_H_
#define _PASSWORD_HELPER_H_

#include <string>
/*
交易密码和资金密码需要加密送入，加密采用BlowFish加密
除用户登陆业务（410301）外其他业务涉及到密码加密采用的密钥是交易密码：密钥是客户代码资金密码：密钥是资金帐号
用户登陆业务（410301）加密采用密钥是功能号（410301）
具体加密函数调用参看“KDEncodeCli.h”，其中nSrcDataLen “需要加密源文的长度”、nKeyLen “密钥的长度”是不包含结束符的长度
*/
namespace tradeadapter
{
	namespace kingdom
	{
		class UPasswordHelper
		{
		public:
			enum UseType
			{
				Login,
				Cust,
				Fund,
			};
			//针对410301登录业务，需要加密用户密码, 客户账户密码密匙是客户号，资金账户密匙是资金账户
			static int GetLoginPassword(const unsigned char *pSrcData, int nSrcDataLen, 
				unsigned char *pDestData, int nDestDataBufLen);

			static int GetCustPassword(const unsigned char *pSrcData, int nSrcDataLen, 
				unsigned char *pDestData, int nDestDataBufLen,const char* custid);

			static int GetfundPassword(const unsigned char *pSrcData, int nSrcDataLen, 
				unsigned char *pDestData, int nDestDataBufLen,const char* fundid);

			static int _get_Password(const unsigned char *pSrcData, int nSrcDataLen, 
				unsigned char *pDestData, int nDestDataBufLen,const char* key);

			static std::string GetPwd(UPasswordHelper::UseType ty, std::string oripwd,std::string custid,std::string fundid);

		};
	};
}
#endif



