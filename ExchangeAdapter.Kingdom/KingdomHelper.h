#ifndef _KINGDOM_HELPER_H_
#define _KINGDOM_HELPER_H_
#include <iostream>
#include <string>
#include <map>
#include "IOrderService.h"
#include ".\lib\KCBPCli.h"
#include "KingdomDef.h"


namespace tradeadapter
{

	namespace kingdom
	{

		std::ostream& operator << (std::ostream& os,const tagKCBPConnectOption& info);

		

#define zhengquanmairu "0B"
#define zhengquanmaichu "0S"
		class UKingdomHelper
		{
		public:
			static std::string				BSToKDString(EBuySell ope);

			static KDMarketDef::MarketType	GetMarketType(const TCHAR* code);

			static std::string				GetGDH(const TCHAR* code , std::string sh,std::string sz);

			static EBuySell					KDStringToBS(const std::string& str);
		};




		class UPasswordHelper
		{
		public:
			
			//针对410301登录业务，需要加密用户密码, 客户账户密码密匙是客户号，资金账户密匙是资金账户
			static int GetLoginPassword(const unsigned char *pSrcData, int nSrcDataLen, 
				unsigned char *pDestData, int nDestDataBufLen);

			static int GetCustPassword(const unsigned char *pSrcData, int nSrcDataLen, 
				unsigned char *pDestData, int nDestDataBufLen,const char* custid);

			static int GetfundPassword(const unsigned char *pSrcData, int nSrcDataLen, 
				unsigned char *pDestData, int nDestDataBufLen,const char* fundid);

			static int _get_Password(const unsigned char *pSrcData, int nSrcDataLen, 
				unsigned char *pDestData, int nDestDataBufLen,const char* key);

			static std::string GetPwd(UPasswordHelper::UseType ty, std::string oripwd,std::string custid,std::string fundid);

		};
	};
};
#endif

