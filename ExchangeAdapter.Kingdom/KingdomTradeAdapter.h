#ifndef __KINGDOM_API_H__
#define __KINGDOM_API_H__
#include <algorithm>
#include <string>
#include <map>

#include "iorderservice.h"
#include "ITradeAdapter.h"
#include "threadlock.h"
#include "CountLimit.h"
//#include "KingdomDef.h"

/*
1. 资金账户登录
2. 资金账户退出
3. 持仓查询
4. 委托下单
5. 撤单
6. 查询下单成交
7. 每分钟激活包
*/
namespace tradeadapter
{
	


	enum SystemState
	{

		正常运行 = '0',//正常运行			
		正在初始化='1',//正在初始化
		清算准备 = '2',//清算准备
		清算完成 = '3',//清算完成
		交收准备 = '4' ,//交收准备
		正在交收 = '5',//正在交收
		交收完成没有初始化 = '9',//交收完成没有初始化
	};
	/*
	enum EntrustStatus
	{
		未报 = 0,
		已报 = 2,
		已报待撤 = 3,
		部成待撤 = 4,
		部撤 = 5,
		已撤 = 6,
		部成 = 7,
		已成 = 8,
		废单 = 9
	};*/

	/*
	备注信息	送证券代码必须送交易市场
	'0','未报' 在lbm中委托成功写入未发标志 
	'1','正报' 在落地方式中,报盘扫描未发委托,并将委托置为正报状态 
	'2','已报' 报盘成功后,报盘机回写发送标志为已报 
	'3','已报待撤' 已报委托撤单 
	'4','部成待撤' 部分成交后委托撤单 
	'5','部撤' 部分成交后撤单成交 
	'6','已撤' 全部撤单成交 
	'7','部成' 已报委托部分成交 
	'8','已成' 委托或撤单全部成交 
	'9','废单' 委托确认为费单 
	'A','待报' 
	'B','正报' 国泰模式中，已报道接口库尚未处理返回库的阶段，标准模式不用。

	*/





	namespace kingdom
	{

		//获取字典信息
		struct StockInfo
		{
			std::string market;
			std::string moneytype;
			std::string stkname;
			std::string stkcode;
			std::string stktype;
			std::string priceunit;
			std::string maxrisevalue;
			std::string maxdownvalue;
			std::string stopflag;
			std::string mtkcalflag;
			std::string bondintr;
			std::string maxqty;
			std::string minqty;
			std::string buyunit;
			std::string saleunit;
			std::string stkstatus;
		};

		//注：kingdom的api以返回值0为ok的。

		/*
		交易密码和资金密码需要加密送入，加密采用BlowFish加密
		除用户登陆业务（410301）外其他业务涉及到密码加密采用的密钥是
		交易密码：密钥是客户代码
		资金密码：密钥是资金帐号
		用户登陆业务（410301）加密采用密钥是功能号（410301）
		具体加密函数调用参看“KDEncodeCli.h”，其中nSrcDataLen “需要加密源文的长度”、nKeyLen “密钥的长度”是不包含结束符的长度
		*/


		///注：Kingdom的API返回值非0的为错误情况
		//由于整体的线程模型不是很明确(是否需要多线程、多线程效率如何等),因此在这里的所有的api实现，都是以单线程模型定义
		//多线程模型由调用方去负责







		struct KingdomParam
		{
			tstring addrname;
			tstring addraddress;
			int addrport;
			tstring kduser;
			tstring kdpwd;		//
			tstring sendname;
			tstring recname;
			bool logable;		//是否输出日志，注：这个是kingdom接口具有的log功能
			int timeout;		//超时

			tstring trdpwd;		//交易密码
			tstring accountid;	//


			//
			//tstring ipaddr;
			//注这个在登录后会获取得到
			//tstring secuid;
			tstring gdsh;//上海股东号
			tstring gdsz;//深圳股东号
			tstring name;
			char accounttype;

			tstring custprop; //keh
			tstring fundid;
			tstring custname;
			tstring orgid;
			tstring bankcode;	//
			tstring operway;	//委托方式 一般“1”
			int canceltimes;
			int countlimit;

		};

		struct KingdomParamA
		{


			std::string addrname;
			std::string addraddress;
			int			 addrport;
			std::string kduser;
			std::string kdpwd;
			std::string sendname;
			std::string recname;

			bool logable; //是否输出日志，注：这个是kingdom接口具有的log功能

			int timeout;


			//std::string custidlogin;//这个字段登录正确后，后台也会返回
			std::string trdpwd;//must hold this for all trade need it.

			//
			//std::string ipaddr;
			//注这个在登录后会获取得到
			//std::string secuid;
			std::string gdsh;//上海股东号
			std::string gdsz;//深圳股东号
			std::string name;
			char accounttype;
			std::string accountid;
			std::string custprop; //
			std::string fundid;		//资金账户
			std::string custname;	//客户名称
			std::string orgid;
			std::string bankcode;	//
			std::string operway;	//委托方式 一般“1”
			std::string ext;		// 取值0
			std::string netaddr;	//网络ip地址，这个取值可以随意
			int canceltimes;
			int countlimit;

			static KingdomParamA FromKingdomParam(KingdomParam param);

		};


		class TAIAPI CKingdomTradeAdapter : public ITradeAdapter
		{
		public:
			CKingdomTradeAdapter();
			virtual ~CKingdomTradeAdapter();

			virtual bool Initialize(void* pParam = NULL);
			virtual bool InitializeByXML( const TCHAR* fpath );
			virtual bool IsInitialized();

			

			virtual bool EntrustOrder(Order order, OrderID* pOutID);

			

			virtual bool QueryOrder(OrderID id, int* pCount);

			virtual bool CancelOrder(OrderID orderid);
			virtual void UnInitialize();			
			virtual bool QueryDealedOrders( std::list<DealedOrder>* pres );			
			virtual bool QueryAccountInfo( BasicAccountInfo* info );
			virtual bool QueryPostions( std::list<PositionInfo>* pPoss,const TCHAR* pstock = NULL );
			

			

			virtual bool QueryEntrustInfos(std::list<EntrustInfo>* pinfo);


			void _querydealedorders( std::list<DealedOrder>* pres ,char* pPos);
			bool _queryEntrustInfos(char* poststr,std::list<EntrustInfo>* pinfo);
			
			std::list<StockInfo> GetStockinfo(const TCHAR* stockcode);

			bool _cancelOrder(OrderID orderid);
			/*
			起始日期	strdate	int	Y	
			终止日期	enddate	int	Y	
			资金帐户	fundid	int	N	不送查询全部
			交易市场	market	char(1)	N	不送查询全部
			股东代码	secuid	char(10)	N	不送查询全部
			证券代码	stkcode	char(8)	N	不送查询全部
			外部银行	bankcode	char(4)	N	
			查询方向	qryflag	char(1) 	Y	向下/向上查询方向
			请求行数	count	int 	Y	每次取的行数
			定位串  	poststr	char(32)	Y	第一次填空
			*/
			bool	QueryAccountMoney(BasicAccountInfo* pInfo);
			//注：默认使用今天
			void	QueryServerStauts(int sysdate = -1);
			void	QueryHistoryDealedOrder(int strdate,int enddate);

			bool	_Connect( const char* szServerName,const char* szServerAddr,int nPort, const char* szUser = "KCXP00",const char* szPassword = "888888", const char* szSendName = "req1",const char* szRecName = "ans1");
			bool	_Login(char accountType ,/* */ const char* custid, /*登录标志 */ const char* pwd);
			bool	SetLogAble(bool Enable);
			bool	GetLogAble();

			virtual tstring GetName();


		private:
#pragma warning(disable : 4251)
			typedef void *KCBPCLIHANDLE;
			KCBPCLIHANDLE m_hHandle;
			KingdomParamA m_param; 
			
			bool m_initialized;
			threadlock::multi_threaded_local m_lock;
			CountLimit m_countlimit;
		};
	};
}
#endif

