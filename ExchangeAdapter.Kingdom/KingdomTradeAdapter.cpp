#include "StdAfx.h"
#include "ExchangeAdapterDef.h"
#pragma warning(disable : 4482)
#include <windows.h>
#include <iostream>
#include "texceptionex.h"
#include "ucomhelper.h"
#include "tinyxml.h"
#include "tconfigsetting.h"
#include "utimeutils.h"
#include "ustringutils.h"
#include "umaths.h"
#include "KCBPCli.h"
#include "KingdomDef.h"
#include "KingdomSession.h"
#include "KingdomHelper.h"
#include "KingdomTradeAdapter.h"

#include "uconsole.h"
#include "tdataset.h"
#include "tdatasets.h"

USING_LIBSPACE

#define AUTOGARD()   threadlock::autoguard<threadlock::multi_threaded_local>  _guard_(&m_lock)
namespace tradeadapter
{
	namespace kingdom
	{

		CKingdomTradeAdapter::CKingdomTradeAdapter( ) :m_hHandle(NULL),m_initialized(false)
		{
		}

		bool CKingdomTradeAdapter::EntrustOrder(Order order, OrderID* pOutID)
		{
			*pOutID = tradeadapter::InvalidOrderId;
			m_countlimit.Enqueue();
			order.Amount = (int ((order.Amount +50)/100))*100;
			KDMarketDef::MarketType markettype = UKingdomHelper::GetMarketType(order.ContractCode.c_str());			
			std::string pwd = UPasswordHelper::GetPwd(UseType::Cust,m_param.trdpwd,m_param.accountid,m_param.fundid);
			std::string secuid = UKingdomHelper::GetGDH(order.ContractCode.c_str(),m_param.gdsh,m_param.gdsz);
			std::string bsflag = UKingdomHelper::BSToKDString(order.OpeDir);

			AUTOGARD();//业务加锁

			CKingdomSession sess(this,m_hHandle,m_param);
			sess.BeginJob();
			sess.SetBasicInfo();
			sess.SetFieldValue("custid",m_param.accountid);
			sess.SetFieldValue("custorgid",m_param.orgid);			
			sess.SetFieldValue("trdpwd",pwd);			
			sess.SetFieldValue("ext",m_param.ext);
			sess.SetFieldValue("orgid",m_param.orgid);
			sess.SetFieldValue("operway",m_param.operway);
			sess.SetFieldValue("market",markettype);			
			sess.SetFieldValue("secuid",secuid);
			sess.SetFieldValue("fundid",m_param.fundid);
			sess.SetFieldValue("stkcode", TS2GB(order.ContractCode));
			sess.SetFieldValue("bsflag",bsflag);
			sess.SetFieldValue("price",order.Price);
			sess.SetFieldValue("qty",order.Amount);
			sess.SetFieldValue("ordergroup","");
			sess.SetFieldValue("bankcode","");
			sess.SetFieldValue("remark","");

			bool ret = sess.CommitJob(FunctionID::KDF_ENTRUSTORDER);
			/**
			返回数据	域名称	标识	类型及长度	描述
			委托序号	ordersno	int	返回给股民
			合同序号	orderid	char(10)	返回给股民
			委托批号	ordergroup	int	返回给股民	
			**/
			if(!sess.GetStatusResult())
			{
				return false;
			}
			if(ret)
			{
				TDataSets rs;
				sess.GetDataResultS(&rs);
				std::string orderno = rs[0]["ordersno"];
				std::string orderid = rs[0]["orderid"];
				std::string ordergroup= rs[0]["ordergroup"];
				int norderno = UMaths::ToInt32(orderno);

				*pOutID =  norderno;
				return true;
			}
			return false;
		}

		

		/*
		inputtype
		‘B’表示以银行帐户登陆  -登陆标识为银行帐户
		‘Z’表示以资金帐户登陆  -登陆标识为资金帐户
		‘C’表示以客户代码登陆  -登陆标识为客户代码（含代理人代码）
		‘K’表示以磁卡登陆      -登陆标识为磁卡号 
		‘X’表示代理人登陆      -登陆标识为代理人
		‘N’表示用股东内码登陆  -登陆标识为股东内码
		*/
		//交易密码需加密送入，加密采用BlowFish加密
		bool CKingdomTradeAdapter::_Login( char accountType ,/* */ const char* custid, /*登录标志 */ const char* pwd )
		{
			m_countlimit.Enqueue();
			AUTOGARD();
			CKingdomSession sess(this,m_hHandle,m_param);
			sess.SetBasicInfo(UseType::Login);

			sess.SetFieldValue("inputtype",accountType);
			sess.SetFieldValue("inputid",custid);

			sess.CommitJob(FunctionID::KDF_LOGIN);			
			
			if(! sess.GetStatusResult() )
			{
				return false;
			}			

			TDataSets rs;
			sess.GetDataResultS(&rs);		

			for(unsigned int i=0; i<rs.GetCount(); ++i)
			{
				if(rs[i]["market"][0] == KDMarketDef::ShenA)
				{
					m_param.gdsz = rs[i]["secuid"];
				}

				if(rs[i]["market"][0] == KDMarketDef::HuA)
				{
					m_param.gdsh = rs[i]["secuid"];
				}			
			}
			m_param.name = rs[0]["name"];
			m_param.accountid = rs[0]["custid"];
			m_param.custprop = rs[0]["custprop"];
			m_param.fundid = rs[0]["fundid"];
			m_param.custname = rs[0]["custname"];
			m_param.orgid = rs[0]["orgid"];
			m_param.bankcode = rs[0]["bankcode"];

			return true;
		}

		bool CKingdomTradeAdapter::CancelOrder(OrderID orderid)
		{
			
			bool canceled = false;
			int count = 0;
			if(orderid == tradeadapter::InvalidOrderId)
			{
				return true;
			}
			while (true)
			{
				count ++;
				bool canceled = _cancelOrder(orderid);
				if(count > this->m_param.canceltimes)
				{
					tstring error =  UStrConvUtils::Format(_T("对委托单号为%d,进行连续撤单%d次都不成功"),orderid, this->m_param.canceltimes) ;
					
					SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter, -1, error.c_str()));
					UConsole::Print(UConsole::TEXT_RED, TS2GB(error));
					//canceled = false;
					break;

				}

				if (canceled)
					break;
			}
			return canceled;
		}
		bool CKingdomTradeAdapter::_cancelOrder(OrderID orderid)
		{
			int date = UTimeUtils::GetNowDate();
			m_countlimit.Enqueue();
			AUTOGARD();
			CKingdomSession sess(this,m_hHandle,m_param);
			sess.BeginJob();
			sess.SetBasicInfo();
			sess.SetFieldValue("orderdate",date);
			sess.SetFieldValue("fundid",m_param.fundid.c_str());
			sess.SetFieldValue("ordersno",orderid);
			bool ret = sess.CommitJob(FunctionID::KDF_CANCELORDER);

			/*
			msgok	成功信息	char(32)	
			cancel_status	内部撤单标志	char(1)	1:内部撤单
			非1:普通撤单
			*/
			if(!sess.GetStatusResult())
			{
				ErrorInfo m_info;
				GetLastError(&m_info);
				if(m_info.Msg.find(_T("该笔委托不存在")) != tstring::npos)
				{
					return true;
				}
				else if (m_info.Msg.find(_T("该笔委托正在发送，请稍候再撤")) != tstring::npos)//还没有撤成功
				{
					return false;
				}			
			}
			TDataSets rs;
			sess.GetDataResultS(&rs);
			
			std::string msgok = sess.GetFieldValue("msgok");
			std::string cancel_status = sess.GetFieldValue("cancel_status");
			return true;
			
			

		}

		bool CKingdomTradeAdapter::SetLogAble( bool Enable )
		{
			
			int nval = Enable ? 1 : 0;
			int ret = KCBPCLI_SetOption(m_hHandle,KCBP_OPTION_TRACE,&nval);
			return ret==0 ? true : false;
		}

		bool CKingdomTradeAdapter::GetLogAble()
		{
			int nval = 0;
			KCBPCLI_GetOption(m_hHandle,KCBP_OPTION_TRACE,&nval);
			return nval ==0 ? false :true;
		}



		bool CKingdomTradeAdapter::Initialize( void* pParam )
		{
			bool ret =false;		
			KingdomParam v = *(KingdomParam*) pParam;
			m_param = KingdomParamA::FromKingdomParam(v);
			m_countlimit.SetLimit(m_param.countlimit);

			/*新建KCBP实例*/
			if( KCBPCLI_Init( &m_hHandle ) != 0 )
			{		
				SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,-1,_T("call KCBPCLI_Init failed")));
				m_initialized = false;
				return false;
			}
			m_initialized = SetLogAble(m_param.logable);

			if(!m_initialized)
				return m_initialized;

			m_initialized = _Connect(m_param.addrname.c_str(),m_param.addraddress.c_str(),m_param.addrport,m_param.kduser.c_str(),m_param.kdpwd.c_str(),m_param.sendname.c_str(),m_param.recname.c_str());


			if(!m_initialized)
				return m_initialized;

			//注：这里必须先调用login，获取股东账户信息(注：以后再改为多账户的模型时，这里的内容需要修改下).
			m_initialized = _Login(m_param.accounttype,m_param.accountid.c_str(),m_param.trdpwd.c_str());

			return m_initialized;
		}



		bool CKingdomTradeAdapter::_Connect( const char* szServerName,const char* szServerAddr,int nPort, const char* szUser /*= "KCXP00"*/,const char* szPassword /*= "888888"*/, const char* szSendName /*= "req1"*/,const char* szRecName /*= "ans1"*/ )
		{
			tagKCBPConnectOption stKCBPConnection = {0};

			strcpy(stKCBPConnection.szServerName,szServerName);		
			stKCBPConnection.nProtocal = 0;

			strcpy(stKCBPConnection.szAddress, szServerAddr);
			stKCBPConnection.nPort = nPort;

			strcpy(stKCBPConnection.szSendQName, szSendName);
			strcpy(stKCBPConnection.szReceiveQName, szRecName);	


			int nRet = 0;


			/*连接KCBP服务器*/
			nRet  = KCBPCLI_SetOption(m_hHandle, KCBP_OPTION_CONNECT, &stKCBPConnection);
			if( nRet )
			{
				printf( "KCBPCLI_SetConnectOption error\n" );
				KCBPCLI_Exit( m_hHandle );
				return false;
			}

			/**设置客户端超时**/
			nRet = KCBPCLI_SetCliTimeOut( m_hHandle, m_param.timeout );
			if( nRet )
			{
				SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,nRet, _T("KCBPCLI_SetCliTimeOut")));
				KCBPCLI_Exit( m_hHandle );
				return false;
			}

			/*连接KCBP SERVER*/			
			nRet = KCBPCLI_ConnectServer(m_hHandle, (char*)szServerName, (char*)szUser, (char*)szPassword);
			if(nRet )
			{
				SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter, nRet, _T("无法连接服务器")));
				KCBPCLI_Exit( m_hHandle );
				return false;
			}

			return true;
		}






		void CKingdomTradeAdapter::UnInitialize()
		{
			if(m_initialized)
			{				
				/*断开连接*/
				int nRet = KCBPCLI_DisConnect(m_hHandle);
				/*释放内存*/
				KCBPCLI_Exit(m_hHandle);
			}
		}



		//BOOL CKingdomTradeAdapter::_getRowDatas(std::map<std::string,std::string>* ret)
		//{
		//	//std::map<std::string,std::string>  ret;

		//	INT ncol = 0;
		//	unsigned char* buff = NULL;// = new unsigned char[256];
		//	long bufflen = 0;
		//	KCBPCLI_RsGetColNum(m_hHandle,&ncol);

		//	for (int i=1; i<=ncol ; ++i)
		//	{
		//		char name[1024];
		//		KCBPCLI_RsGetColName(m_hHandle,i,name,1024);

		//		KCBPCLI_RsGetVal(m_hHandle,i,&buff,&bufflen);
		//		(*ret)[name] = (char*)buff;			
		//	}
		//	return TRUE;
		//}

		

		





		//这个函数很古怪。需要输入两个字段sysdate和orderdate
		void CKingdomTradeAdapter::QueryServerStauts( int sysdate )
		{
			AUTOGARD();
			int orderdate  = sysdate;
			CKingdomSession sess(this,m_hHandle,m_param);
			sess.BeginJob();
			sess.SetBasicInfo();			
			sess.SetFieldValue("sysdate",sysdate);		
			sess.SetFieldValue("orderdate",orderdate);		
			sess.CommitJob(FunctionID::KDF_QUERYSERVERSTATUS);

			if(!sess.GetStatusResult())
			{
				return;
			}
			else
			{			
				TDataSets sets;
				sess.GetDataResultS(&sets);
				//std::cout<<sets.front();
			}		
		}




		//std::string CKingdomTradeAdapter::_getrowdata( const char* key )
		//{
		//	long ndatalen = 0;
		//	unsigned char* pdata;
		//	int ret = KCBPCLI_RsGetValByName(m_hHandle,(char*)key,&pdata,&ndatalen);
		//	return (char* )pdata;
		//}

		//
		

		////这个适合只有一行的结果。
		//BOOL CKingdomTradeAdapter::_getdataResults(TDataSets* res)
		//{
		//	KCBPCLI_RsMore(m_hHandle);

		//	while(!KCBPCLI_RsFetchRow(m_hHandle))
		//	{
		//		EXDataSet set;
		//		_getRowDatas(&set);
		//		res->push_back(set);
		//	}		
		//	return TRUE;
		//}

		CKingdomTradeAdapter::~CKingdomTradeAdapter()
		{
		}

		bool CKingdomTradeAdapter::InitializeByXML( const TCHAR* fpath )
		{
			TConfigSetting setting;
			bool ret = setting.LoadConfig(fpath);
			if(!ret)
			{
				throw TException(_T("加载配置文件失败"));
			}
			std::string addrname= setting["configuration"]["addrname"].EleVal();
			std::string addraddress= setting["configuration"]["addraddress"].EleVal();
			std::string addrport= setting["configuration"]["addrport"].EleVal();
			std::string kduser= setting["configuration"]["kduser"].EleVal();
			std::string kdpwd= setting["configuration"]["kdpwd"].EleVal();
			std::string sendname= setting["configuration"]["sendname"].EleVal();
			std::string recname= setting["configuration"]["recname"].EleVal();

			std::string timeout = setting["configuration"]["timeout"].EleVal();
			std::string logable = setting["configuration"]["logable"].EleVal();
			std::string accounttype = setting["configuration"]["accountype"].EleVal();
			std::string custid = setting["configuration"]["accountid"].EleVal();
			std::string password = setting["configuration"]["password"].EleVal();
			std::string operway =  setting["configuration"]["operway"].EleVal();
			std::string canceltimes =  setting["configuration"]["canceltimes"].EleVal();
			std::string countlimit =   setting["configuration"]["countlimit"].EleVal();


			KingdomParam param;
			param.addrname = U82TS(addrname.c_str());
			param.addraddress = U82TS(addraddress.c_str());
			param.addrport = UMaths::ToInt32(addrport);
			param.kduser = U82TS(kduser.c_str());
			param.kdpwd = U82TS(kdpwd.c_str());
			param.sendname = U82TS(sendname.c_str());
			param.recname = U82TS(recname.c_str());
			param.timeout = UMaths::ToInt32(timeout);
			param.logable = logable == "true";
			param.accounttype = accounttype[0];
			param.accountid = U82TS(custid.c_str());
			param.trdpwd = U82TS(password.c_str());
			param.operway = U82TS(operway.c_str());
			param.canceltimes = UMaths::ToInt32(canceltimes);
			param.countlimit = UMaths::ToInt32(countlimit);

			ret = Initialize(&param);
			
			return ret;
		}



		//注：kingdom的查询order的和
		bool CKingdomTradeAdapter::QueryOrder(OrderID id, int* pCount)
		{
			*pCount = 0;
			m_countlimit.Enqueue();
			AUTOGARD();
			CKingdomSession sess(this,m_hHandle,m_param);
			sess.BeginJob();
			sess.SetBasicInfo();
			sess.SetFieldValue("fundid",m_param.fundid);
			sess.SetFieldValue("market","");
			sess.SetFieldValue("secuid","");
			sess.SetFieldValue("stkcode","");
			sess.SetFieldValue("ordersno",id);
			sess.SetFieldValue("bankcode",m_param.bankcode);
			sess.SetFieldValue("qryflag","1");
			sess.SetFieldValue("count",3000);
			sess.SetFieldValue("poststr","");

			bool ret = sess.CommitJob(FunctionID::KDF_QUERYTODAYTRADE);
			if(!sess.GetStatusResult() )
			{
				return false;
			}

			TDataSets sets;
			sess.GetDataResultS(&sets);

			int sum=0;
			for (unsigned int i=0; i<sets.GetCount(); ++i)
			{
				sum += UMaths::ToInt32(sets[i]["matchqty"]);				
			}
			*pCount =  sum;			
			return true;
		}

		

		bool CKingdomTradeAdapter::IsInitialized()
		{
			return m_initialized;
		}

		bool CKingdomTradeAdapter::QueryDealedOrders( std::list<DealedOrder>* pres )
		{
			m_countlimit.Enqueue();
			pres->clear();
			char pos[32] = {0};
			while(true)
			{
				std::list<DealedOrder> tmp;

				_querydealedorders(&tmp,pos);
				if(tmp.size() == 0 )
				{
					return true;
				}
				else
				{
					for(std::list<DealedOrder>::iterator it = tmp.begin(); it != tmp.end(); ++it)
					{
						pres->push_back(*it);
					}
				}
			}

			return true;

		}

		bool CKingdomTradeAdapter::QueryAccountInfo( BasicAccountInfo* pinfo )
		{
			bool ret = QueryAccountMoney(pinfo);
			pinfo->ExProvider = _T("kingdom");
			return ret;
		}


		//注： 查询头寸的功能，应该结果集合是不可能操作1000条。
		bool CKingdomTradeAdapter::QueryPostions( std::list<PositionInfo>* pPoss,const TCHAR* pstock /*= NULL */ )
		{
			m_countlimit.Enqueue();
			AUTOGARD();
			CKingdomSession sess(this,m_hHandle,m_param);
			sess.BeginJob();
			sess.SetBasicInfo();
			sess.SetFieldValue("qryflag","1");
			sess.SetFieldValue("count",1000);
			sess.SetFieldValue("poststr","");
			sess.SetFieldValue("fundid","");
			sess.SetFieldValue("market","");
			sess.SetFieldValue("secuid","");
			sess.SetFieldValue("stkcode","");

			sess.CommitJob(FunctionID::KDF_QUERYPOSITION);
			if(!sess.GetStatusResult())
			{
				return false;
			}
			TDataSets sets;
			sess.GetDataResultS(&sets);
			for (unsigned int i = 0; i<sets.GetCount(); ++i)
			{
				PositionInfo info;
				info.code = GB2TS(sets[i]["stkcode"]);
				info.cost_price = UMaths::ToDouble(sets[i]["costprice"].c_str());
				info.current_amount = UMaths::ToInt32(sets[i]["stkqty"]);
				info.enable_amount = UMaths::ToInt32(sets[i]["stkavl"]);
//				info.exchange_type = GB2TS(sets[i]["market"]) ; //0,1;
				info.income_balance = UMaths::ToDouble(sets[i]["income"].c_str());
				info.last_price = UMaths::ToDouble(sets[i]["lastprice"].c_str());
				info.market_value = UMaths::ToDouble(sets[i]["mktval"].c_str());
				info.stock_account = GB2TS(sets[i]["secuid"]);
				info.stock_name = GB2TS(sets[i]["stkname"]);
				info.total_amount = UMaths::ToInt32(sets[i]["stkqty"]);

				pPoss->push_back(info);				
			}

			return true;
		}


		bool CKingdomTradeAdapter::QueryAccountMoney( BasicAccountInfo* pInfo )
		{
			m_countlimit.Enqueue();
			AUTOGARD();
			CKingdomSession sess(this,m_hHandle,m_param);
			sess.BeginJob();
			sess.SetBasicInfo();			
			sess.SetFieldValue("fundid","");
			sess.SetFieldValue("moneytype","");
			sess.CommitJob(FunctionID::KDF_QUERYMONEY);

			if(!sess.GetStatusResult())
			{
				return false;
			}
			TDataSets res;
			sess.GetDataResultS(&res);

			if(res.GetCount() >= 1)
			{
				//pInfo->AssetBalance;
				pInfo->ClientID = GB2TS(res[0]["custid"]);
				//pInfo->ClientName = UStrConvUtils
				pInfo->CurrentBalance = UMaths::ToDouble(res[0]["fundbal"]);
				pInfo->EnableBalance = UMaths::ToDouble(res[0]["fundavl"]);
				//pInfo->FetchCash
				pInfo->FundAccount = GB2TS(res[0]["fundid"]);
				//pInfo->MoneyType 
				//pInfo->SHStockAccount
				//pInfo->SZStockAccount
			}
			return true;
		}

		void CKingdomTradeAdapter::QueryHistoryDealedOrder( int strdate,int enddate )
		{	
			AUTOGARD();
			CKingdomSession sess(this,m_hHandle,m_param);
			sess.BeginJob();
			sess.SetBasicInfo();
			sess.SetFieldValue("strdate",strdate);
			sess.SetFieldValue("enddate",enddate);
			sess.SetFieldValue("fundid",m_param.fundid.c_str());
			sess.SetFieldValue("market","");
			sess.SetFieldValue("secuid","");
			sess.SetFieldValue("stkcode","");
			sess.SetFieldValue("count",3000);
			sess.SetFieldValue("poststr","");
			bool ret = sess.CommitJob(FunctionID::KDF_QUERY_HISTORY_ORDERS);

		}



		

		void CKingdomTradeAdapter::_querydealedorders( std::list<DealedOrder>* pres ,char* pPos )
		{
			m_countlimit.Enqueue();
			AUTOGARD();
			CKingdomSession sess(this,m_hHandle,m_param);
			sess.BeginJob();
			sess.SetBasicInfo();
			sess.SetFieldValue("fundid",m_param.fundid);
			sess.SetFieldValue("market","");
			sess.SetFieldValue("secuid","");
			sess.SetFieldValue("stkcode","");
			sess.SetFieldValue("ordersno","");
			sess.SetFieldValue("bankcode",m_param.bankcode);
			sess.SetFieldValue("qryflag","1");
			sess.SetFieldValue("count",3000);
			sess.SetFieldValue("poststr",pPos);

			bool ret = sess.CommitJob(FunctionID::KDF_QUERYTODAYTRADE);
			if( !sess.GetStatusResult() )
			{
				return;
			}

			TDataSets sets;
			sess.GetDataResultS(&sets);

			for (unsigned int i = 0; i<sets.GetCount(); ++i)
			{
				DealedOrder order;
				//order.bs_name

				order.business_amount = UMaths::ToInt32(sets[i]["matchqty"]);
				

				order.business_price = UMaths::ToDouble(sets[i]["matchprice"].c_str());

				order.business_balance = UMaths::ToDouble(sets[i]["matchamt"].c_str());

				//comment the kingdom does'nt calculte this val
				if(order.business_balance <=0.1)
				{
					order.business_balance = order.business_price * order.business_amount;
				}
				//order.business_status
				order.business_time = UMaths::ToInt32(sets[i]["matchtime"]) ;//注： 文档错误,返回时间格式是：hhmmss/100; //注：服务器返回的结果是hhmmsszz
				//order.business_times
				//order.business_type
				order.date = UMaths::ToInt32(sets[i]["trddate"]);
				order.entrust_bs = UKingdomHelper::KDStringToBS(sets[i]["bsflag"]);
				order.entrust_no = UMaths::ToInt32(sets[i]["ordersno"]);
				//order.exchange_type
				order.report_no = UMaths::ToInt32(sets[i]["orderid"]);
				order.serial_no = UMaths::ToInt32(sets[i]["matchsno"]);
			
				//order.status_name;
				//order.stock_account = 
				order.stock_code = GB2TS(sets[i]["stkcode"]);
				order.stock_name = GB2TS(sets[i]["stkname"]);
				//order.type_name;
				pres->push_back(order);

			}
			if (sets.GetCount() > 0)
			{
				sprintf(pPos, "%s", sets[sets.GetCount() - 1]["poststr"].c_str());
			}
		}







		std::list<StockInfo> CKingdomTradeAdapter::GetStockinfo( const TCHAR* stockcode )
		{
			std::list<StockInfo> infos;

			CKingdomSession sess(this,m_hHandle,m_param);
			sess.BeginJob();
			sess.SetBasicInfo();
			sess.SetFieldValue("market","");
			sess.SetFieldValue("stkcode", TS2GB(stockcode));

			sess.CommitJob(FunctionID::KDF_QUERYSTOCKINFO);

			if(!sess.GetStatusResult())
			{
				return infos;
			}

			TDataSets sets;
			sess.GetDataResultS(&sets);
			
			for (unsigned int i = 0; i<sets.GetCount(); ++i)
			{
				StockInfo info;
				info.market = sets[i]["market"];
				info.stkname = sets[i]["stkname"];
				info.moneytype = sets[i]["moneytype"];
				info.stkcode = sets[i]["stkcode"];
				info.stktype = sets[i]["stktype"];
				info.priceunit=sets[i]["priceunit"];
				info.maxrisevalue = sets[i]["maxrisevalue"];
				info.stopflag = sets[i]["stopflag"];
				info.mtkcalflag = sets[i]["mtkcalflag"];
				info.bondintr = sets[i]["bondintr"];
				info.maxqty = sets[i]["maxqty"];
				info.minqty = sets[i]["minqty"];
				info.buyunit = sets[i]["buyunit"];
				info.saleunit = sets[i]["saleunit"];
				info.stkstatus = sets[i]["stkstatus"];
				infos.push_back(info);			
			
			}
			return infos;
		}


		
		bool CKingdomTradeAdapter::_queryEntrustInfos( char* poststr,std::list<EntrustInfo>* pesinfo )
		{
			AUTOGARD();
			CKingdomSession sess(this,m_hHandle,m_param);
			sess.BeginJob();
			sess.SetBasicInfo(UseType::Cust);
			sess.SetFieldValue("market","");
			sess.SetFieldValue("fundid",m_param.fundid);
			sess.SetFieldValue("secuid","");
			sess.SetFieldValue("stkcode","");
			sess.SetFieldValue("ordersno","");
			sess.SetFieldValue("ordergroup","");
			sess.SetFieldValue("bankcode","");
			sess.SetFieldValue("qryflag","1");
			sess.SetFieldValue("count",3000);
			sess.SetFieldValue("poststr",poststr);
			sess.CommitJob(FunctionID::KDF_QUERYTODAYENTRUST);
			if( !sess.GetStatusResult() )
			{
				return false;
			}
			
			TDataSets sets;
			sess.GetDataResultS(&sets);
			for (unsigned int i = 0; i<sets.GetCount(); ++i)
			{
				EntrustInfo info;
				//info.cflag = 
				info.matchamt = UMaths::ToDouble(sets[i]["matchamt"]);
				info.matchqty = UMaths::ToInt32(sets[i]["matchqty"]);
				info.Ope = UKingdomHelper::KDStringToBS( sets[i]["bsflag"] );
				info.operdate = UMaths::ToInt32(sets[i]["operdate"] );
				info.opertime = UMaths::ToInt32(sets[i]["opertime"] );
				info.orderdate= UMaths::ToInt32(sets[i]["orderdate"] );
				info.orderfrzamt =UMaths::ToDouble(sets[i]["orderfrzamt"] );
				info.orderid = UMaths::ToInt32(sets[i]["ordersno"] );   //be carefu
				info.orderqty = UMaths::ToInt32(sets[i]["orderqty"] );
				//info.orderstatus;
				info.ordertime = UMaths::ToInt32(sets[i]["ordertime"] );
				info.stkcode = GB2TS(sets[i]["stkcode"]);
				info.stkname = GB2TS(sets[i]["stkname"]);
				info.orderprice = UMaths::ToDouble(sets[i]["orderprice"]);
				pesinfo->push_back(info);
					
			}
			if (sets.GetCount() >0)
			{			
				sprintf(poststr,"%s",sets[sets.GetCount()-1]["poststr"].c_str());
			}
			return true;
		}

		bool CKingdomTradeAdapter::QueryEntrustInfos( std::list<EntrustInfo>* pinfo )
		{
			m_countlimit.Enqueue();
			char poststr[32] = {0};
			pinfo->clear();
			int len = pinfo->size();

			while(true)
			{
				_queryEntrustInfos(poststr,pinfo);
				if(pinfo->size() == len)
				{
					break;
				}
				else
				{
					len = pinfo->size();
				}
			}
			return true;
		}

		tstring CKingdomTradeAdapter::GetName()
		{
			
			return _T("Kingdom Trade Adapter");
		}


		tradeadapter::kingdom::KingdomParamA KingdomParamA::FromKingdomParam(KingdomParam param)
		{
			KingdomParamA ret;
			ret.trdpwd = TS2GB(param.trdpwd);
			ret.addrname = TS2GB(param.addrname);
			ret.addraddress = TS2GB(param.addraddress);
			ret.addrport = param.addrport;
			ret.kduser = TS2GB(param.kduser);
			ret.kdpwd = TS2GB(param.kdpwd);
			ret.sendname = TS2GB(param.sendname);
			ret.recname = TS2GB(param.recname);

			ret.accounttype = param.accounttype;
			ret.accountid = TS2GB(param.accountid);

			ret.operway = TS2GB(param.operway);

			ret.timeout = param.timeout;

			ret.logable = param.logable;

			//ret.secuid = TS2GB(param.secuid);
			ret.canceltimes = param.canceltimes;
			ret.countlimit = param.countlimit;
			return ret;
		}

	};
};