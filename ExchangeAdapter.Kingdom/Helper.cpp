
#include "Helper.h"
namespace kingdom
{

	std::ostream& operator <<(std::ostream& os,const tagKCBPConnectOption& info)		
	{
		os<<"tagKCBPConnectOption 结构信息:"<<std::endl;//()
		os<<"info.nPort          "<<info.nPort<<std::endl;
		os<<"info.nProtocal      "<<info.nProtocal<<std::endl;
		os<<"info.szAddress      "<<info.szAddress<<std::endl;
		os<<"info.szReceiveQName "<<info.szReceiveQName<<std::endl;
		os<<"info.szReserved     "<<info.szReserved<<std::endl;
		os<<"info.szSendQName    "<<info.szSendQName<<std::endl;
		os<<"info.szServerName   "<<info.szServerName<<std::endl;
		os<<"---------------------------"<<std::endl;//()
		return os;

	}

	std::ostream& operator << (std::ostream& os,const KDDataSet& info)
	{
		
		os<<"-----------map信息-------------:"<<std::endl;//()
		for (std::map<std::string,std::string>::const_iterator it = info.begin(); it != info.end(); ++it)
		{
			os<<it->first<<"       :"<<it->second<<std::endl;
		}
		os<<"---------------------------"<<std::endl;//()
		return os;

	}


	std::ostream& operator << (std::ostream& os,const KDDataSetS& info)
	{
		os<<"==========================="<<std::endl;
		for (KDDataSetS::const_iterator it = info.begin(); it != info.end(); ++it)
		{
			os<<*it;
		}
		os<<"==========================="<<std::endl;
		return os;

	}
};
