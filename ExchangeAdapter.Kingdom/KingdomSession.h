#pragma once
#include <string>
#include <memory>
#include "IOrderService.h"
#include "KingdomDef.h"
#include "KCBPCli.h"
#include "tdataset.h"
#include "tdatasets.h"


namespace tradeadapter
{
	namespace kingdom
	{
		class  CKingdomTradeAdapter;
		struct KingdomParamA;

		class CKingdomSession
		{
		public:
			CKingdomSession(CKingdomTradeAdapter* pParent,KCBPCLIHANDLE handler, const KingdomParamA& param);
			virtual ~CKingdomSession(void);

			bool SetFieldValue( const char* key, const char* val );

			bool SetFieldValue( const char* key, const std::string& val );

			bool SetFieldValue( const char* key, char val );

			bool SetFieldValue( const char* key, int val );			

			bool SetFieldValue( const char* key, double val );

#pragma warning(disable : 4482)
			bool SetBasicInfo(UseType ty  = UseType::Cust);
			


			std::string GetFieldValue(const char* key);


			bool GetStatusResult();


			bool _getRowDatas(libutils::TDataSet* ret);


			bool GetDataResultS(libutils::TDataSets* res);

			bool BeginJob();
			bool CommitJob(const char* funcid);
		private:
			KCBPCLIHANDLE m_hHandle;
			std::shared_ptr<KingdomParamA> m_param;
			CKingdomTradeAdapter* m_parant;
			static std::string GetIP();
			static std::string GetNetAddr();
			static std::string GetHDID();
			static std::string GetCPUID();
			static std::string GetMac();
		};

	};
};