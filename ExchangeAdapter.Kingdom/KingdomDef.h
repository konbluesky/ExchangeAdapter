#ifndef _KINGDOM_DEF_H_
#define  _KINGDOM_DEF_H_


#include <string>
#include <vector>
#include <map>
#include "IOrderService.h"
namespace tradeadapter
{


	namespace kingdom
	{


		//#define  KDF_ENTRUSTORDER "410411"


		/*
		代码	交易市场
		0	深A
		1	沪A
		2	深B
		3	沪B
		6	三板A
		7	三板B
		A	非交易所债券
		J	开放式基金
		*/
		struct KDMarketDef
		{
			typedef char MarketType;

			static const MarketType ShenA = '0';
			static const MarketType HuA = '1';
			static const MarketType ShenB = '2';
			static const MarketType HuB = '3';
			static const MarketType SanBanA = '6';
			static const MarketType SanBanB = '7';
			static const MarketType FeiJiaosuozaiquan = 'A';
			static const MarketType Feikaifangshijijin = 'J';	

			//下两个字段是自己添加的
			static const MarketType Unkonw = '-';	
			static const MarketType ALL = '+';	

		};

		struct FunctionID
		{
			static const char* KDF_LOGIN;			//登陆
			static const char* KDF_ENTRUSTORDER;	//委托
			static const char* KDF_CANCELORDER;		//取消单
			static const char* KDF_QUERYMONEY;		//"410502";查询账户资金
			static const char* KDF_QUERYTODAYTRADE; // 410512查询今日成交
			static const char* KDF_QUERYPOSITION;	//410503 查询头寸
			static const char* KDF_QUERYSERVERSTATUS; //410232 查询服务器状态

			static const char* KDF_QUERYSTOCKINFO; //410203//证券信息(410203)
			static const char* KDF_QUERYTODAYENTRUST;//当日委托明细查询(410510)

			static const char* KDF_QUERY_HISTORY_ORDERS ;//411513 查询历史成交
		};
		enum UseType
		{
			Login,
			Cust,
			Fund,
		};
		/*

		买卖类别查询用bsflag
		代码	买卖类别	交易所代码	交易所买卖类别
		0B	证券买入		
		0S	证券卖出		
		0R	融资		
		0Q	融券		
		0G	转股		
		0H	回售		
		0Y	预受要约		
		0E	解除预受		
		0Z	转托管		
		0A	全转托管		
		0P	配售申购		
		0F	竞价申购		
		0D	定价申购		
		0J	配股缴款		
		0U	指定交易		
		0V	解除指定		
		0X	回购解除		
		0W	回购指定		
		0K	配债		
		0T	配售缴款		
		0L	配售放弃		
		0a	对方买入	YB	对手方最优价格买委托
		0b	本方买入	XB	本方最优价格买委托
		0c	即时买入	2B	即时成交剩余撤销买委托
		0d	五档买入(深沪)	VS	最优五档即时成交剩余撤销卖委托
		0e	全额买入	WB	全额成交或撤销买委托
		0f	对方卖出	YS	对手方最优价格卖委托
		0g	本方卖出	XS	本方最优价格卖委托
		0h	即时卖出	2S	即时成交剩余撤销卖委托
		0i	五档卖出(深沪)	VS	最优五档即时成交剩余撤销卖委托
		0j	全额卖出	WS	全额成交或撤销卖委托
		0q	转限买入(沪)		
		0r	转限卖入(沪)		
		1E	意向买		
		1F	意向卖		
		1G	定价买		
		1H	定价卖		
		1I	确认买		
		1J	确认卖		
		82	ETF实物申购		
		84	ETF实物赎回		
		85	基金拆分		
		86	基金合并		
		89	ETF实物冲账		
		90	跨境ETF申购		
		91	跨境ETF赎回		
		1K	报价回购买入		
		1N	报价回购入质		
		1O	报价回购出质		
		1P	约定购回初始交易		
		1R	柜台意向买		
		1S	柜台意向卖		
		1T	柜台大额做市买		
		1U	柜台大额做市卖		
		1V	柜台小额做市买		
		1W	柜台小额做市卖		
		1X	柜台成交确认买		
		1Y	柜台成交确认卖		

		*/
		/*
		3.	买卖类别统一升级成两位来标识（以前的一位需要在前面增加数字0，如买入应该送0B）目前送一位依然是兼容的

		*/


		/*
		struct BSFlag
		{
		static  char* zhengquanmairu = "0B";//	证券买入		
		static  char* zhengquanmaichu="0S";//	证券卖出

		};
		*/

		

		

	};


}
#endif