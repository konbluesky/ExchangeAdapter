#pragma once

#define OK			TRUE
#define ERR			FALSE

enum AccountType
{
    AccountType_Stock = 0,
    AccountType_Futrue = 1
};

enum Side
{
    Side_Buy = 0,
    Side_Sell = 1
};

enum PosSide
{
    PosSide_More = 0,
    PosSide_Short = 1,
    PosSide_Clear = 2
};

enum OpenClose
{
    OpenClose_Open = 0,
    OpenClose_Close = 1,
    OpenClose_CloseToday = 2,
    OpenClose_CloseYestoday = 3,
    OpenClose_ForceClose = 4,
    OpenClose_None = 5
};

enum PriceMode
{
    PriceMode_LimitPrice = 0,
    PriceMode_LastPrice = 1,
    PriceMode_MarketPrice = 2,
    PriceMode_MarketPriceThisSide = 3,
    PriceMode_MarketPriceThatSide = 4,
    PriceMode_MarketPriceCancelRest = 5,
    PriceMode_MarketPriceLimitedRest = 6
};

enum HedgeFlag
{
    HedgeFlag_Speculation = 1,
    HedgeFlag_Hedge = 3
};

enum OrderState
{
    OrderState_Unknow = 0,			//未知
    OrderState_UnAck = 1,			//未报
    OrderState_ToBeConfirmed = 2,	//已在队列，状态待确认
    OrderState_ToBeReported = 3,	//待报
    OrderState_Reporting = 4,		//正在申报
    OrderState_Reported = 5,		//已报
    OrderState_ReportedCancel = 6,	//已报待撤
    OrderState_PartFillAndCancelling = 7,//部成待撤
    OrderState_PartFilledAndCancelled = 8,//部成已撤
    OrderState_Canceled = 9,		//已撤
    OrderState_PartFilled = 10,		//部成
    OrderState_Filled = 11,			//已成
    OrderState_PlaceRejected = 12,	//下单拒绝
    OrderState_CancelRejected = 13	//撤单拒绝
};

enum Unit
{
    Unit_Hand = 0,
    Unit_Section = 1,
    Unit_Piece = 2
};

struct LoginInfo
{
    char UserAccount[20];
    BOOL IsLogined;
};

struct PositionInformation
{
    char UserAccount[20];		//资金账号
    char Symbol[8];				//证券代码
    enum PosSide PosSide;		//持仓方向
    enum Unit Unit;				//计量单位
    enum PositionDateType PositionDate;//持仓日期
    double YdPosition;			//上日持仓
    double TodayPosition;		//今日持仓
    double MoreFrozen;			//多头冻结
    double ShortFrozen;			//空头冻结
    double OpenFrozenAmount;	//开仓冻结金额
    double CloseFrozenAmount;	//平仓冻结金额
    double OpenVolume;			//开仓量
    double CloseVolume;			//平仓量
    double PositionCose;		//持仓成本
    double PreMargin;			//占用的保证金
    double UseMargin;			//冻结的保证金
    enum HedgeFlag HedgeFlag;	//投机套保标记
    float StockVolumn;			//当前持股数量
    float StockCanSoldVolumn;	//可卖股票数量
    float StockNewDealPrise;	//最新价
    float StockCost;			//成本价
    float StockTradingProfitLoss;//买卖盈亏
};

struct OrderStatus
{
    char TimeStamp[20];		//时间戳
    char account[20];		//资金账号
    char OrderID[20];		//合同号
    char OrderRef[20];		//原始单号
    enum OrderState OrderState;//委托状态
    char Message[256];		
};

struct CancelOrderInfo
{
    char OrderID[20];
    char ExchangeID[20];
    char OrderRef[20];
    long OrderActionRef;
};

struct TradingAccount
{
    char UserAccount[20];
    char Password[20];
    enum AccountType AccountType;
    char BranchNO[20];
    char Trader[20];
    char SeatNO[20];
    long IsLogined;
};

struct Order
{
    char UserAccount[20];
    char Code[8];
    char OrderID[20];
    char ExchangeID[20];
    enum Side Side;
    enum OpenClose OpenClose;
    int Amount;
    enum Unit Unit;
    double Price;
    enum PriceMode PriceMode;
    enum OrderState OrderState;
    char OrderRef[20];
    char UpdateTimeStamp[20];
    double FilledAmount;
    enum HedgeFlag HedgeFlag;
    char Info[256];
};

struct ReturnInfo
{
    BOOL IsSuccess;
    long ErrorCode;
    LPSTR ErrorMessage;
};

