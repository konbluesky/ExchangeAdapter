#include "stdafx.h"
#include <windows.h>
#include "ExchangeAdapterDef.h"
#include "UProductDefHelper.h"
#include <tchar.h>
#include "KDEncodeCli.h"
#include "texceptionex.h"
#include "KingdomHelper.h"
#include "UConsole.h"

#pragma warning(disable : 4482)

USING_LIBSPACE
namespace tradeadapter
{
	namespace kingdom
	{
		std::string UKingdomHelper::BSToKDString( EBuySell ope )
		{
			if(ope == EBuySell::eBuy)
				return zhengquanmairu;
			else if (ope == EBuySell::eSell)
				return zhengquanmaichu;
			else
				UConsole::Print(UConsole::TEXT_RED,_T("未定以的操作类型"));
				throw TException(_T("未定义的买卖操作符号"));
		}


		tradeadapter::EBuySell UKingdomHelper::KDStringToBS( const std::string& str )
		{
			if(str == "B" || str == "0B")
				return EBuySell::eBuy;
			else if(str == "S" || str == "0S")
				return EBuySell::eSell;
			else
				throw TException(_T("未定义的买卖操作符号"));

		}

		KDMarketDef::MarketType UKingdomHelper::GetMarketType( const TCHAR* code )
		{
			std::pair<EMarketDef,EProductType> info = UProductDefHelper::ParseCode(code);		
			if(info.first == EMarketDef::ESH)
			{
				return KDMarketDef::HuA;
			}
			if(info.first == EMarketDef::ESZ)
			{
				return KDMarketDef::ShenA;
			}
			else
			{
				UConsole::Print(UConsole::TEXT_RED, tstring(_T("未知的code")) + code);
				return KDMarketDef::Unkonw;
			}
		}

		std::string UKingdomHelper::GetGDH( const TCHAR* code , std::string sh,std::string sz )
		{
			
			if(code[0] == _T('0'))
				return sz;
			else if(code[0] == _T('6') || code[0] == _T('5'))
				return sh;
			else 
			{
				throw TException(_T("未定义的产品类型"));
			}
		}



		
		const char* FunctionID::KDF_ENTRUSTORDER = "410411";
		const char* FunctionID::KDF_CANCELORDER = "410413";
		const char* FunctionID::KDF_QUERYMONEY = "410502";
		const char* FunctionID::KDF_LOGIN	   = "410301";
		const char* FunctionID::KDF_QUERYTODAYTRADE = "410512";
		const char* FunctionID::KDF_QUERYPOSITION = "410503";
		const char* FunctionID::KDF_QUERYSERVERSTATUS = "410232";
		const char* FunctionID::KDF_QUERYSTOCKINFO = "410203";
		const char* FunctionID::KDF_QUERYTODAYENTRUST = "410510";
		const char* FunctionID::KDF_QUERY_HISTORY_ORDERS = "411513";

		


		int UPasswordHelper::GetLoginPassword(const unsigned char *pSrcData, int nSrcDataLen, 
			unsigned char *pDestData, int nDestDataBufLen)
		{
			const char* pKey = FunctionID::KDF_LOGIN;
			int ret = _get_Password(pSrcData,  nSrcDataLen, pDestData,  nDestDataBufLen,pKey);
			return ret;
		}

		int UPasswordHelper::GetCustPassword( const unsigned char *pSrcData, int nSrcDataLen, unsigned char *pDestData, int nDestDataBufLen,const char* custid )
		{
			int ret = _get_Password(pSrcData,nSrcDataLen,pDestData,nDestDataBufLen,custid);
			return ret;
		}

		int UPasswordHelper::GetfundPassword( const unsigned char *pSrcData, int nSrcDataLen, unsigned char *pDestData, int nDestDataBufLen,const char* fundid )
		{
			int ret = _get_Password(pSrcData,  nSrcDataLen, pDestData,  nDestDataBufLen,fundid);
			return ret;
		}

		int UPasswordHelper::_get_Password( const unsigned char *pSrcData, int nSrcDataLen, unsigned char *pDestData, int nDestDataBufLen,const char* pKey )
		{
			int nKeyLen = strlen(pKey);
			unsigned char buff[1024];
			memcpy(buff,pSrcData,sizeof(char)*nSrcDataLen );

			int resultlen =KDEncode(KDCOMPLEX_ENCODE,
				buff, nSrcDataLen, 
				pDestData, nDestDataBufLen, 
				(void *)pKey,nKeyLen);
			return resultlen;

		}

		std::string UPasswordHelper::GetPwd( UseType ty, std::string oripwd,std::string custid,std::string fundid )
		{
			char buff[1024] = {0};
			unsigned char password[1024] = {0};
			unsigned char * prig = (unsigned char*)oripwd.c_str();
			int pwdlen = 0;

			switch(ty)
			{
			case UseType::Login:
				pwdlen = kingdom::UPasswordHelper::GetLoginPassword(prig,strlen((const char *)prig),password,1024);
				break;
			case UseType::Cust:
				pwdlen = kingdom::UPasswordHelper::GetCustPassword(prig,strlen((const char *)prig),password,1024,custid.c_str());
				break;
			case UseType::Fund:
				pwdlen = kingdom::UPasswordHelper::GetfundPassword(prig,strlen((const char *)prig),password,1024,fundid.c_str());
				break;
			}
			unsigned  char* trdpwd = password;
			return (char*)trdpwd;
		}

	}
}