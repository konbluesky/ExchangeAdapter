#pragma once
#include "IOrderService.h"
#include <string>
#include <map>
#include "UProductDefHelper.h"


struct IF2UnPacker;

namespace tradeadapter
{
	namespace hst2
	{
		enum FunctionID
		{
			func_am_user_login			= 769000, //登录

			func_am_user_logout			= 769001, //退出登录

			func_am_entrust				= 769100, //普通委托

			func_am_entrust_withdraw	= 769101, //委托撤销			

			func_am_entrust_qry			= 769153, //委托查询

			func_am_realtime_qry		= 769155, //成交查询

			func_am_combostock_qry		= 769150,//组合持仓查询

			func_am_combofund_qry	    =769151,//组合层现货可用资金查询

			func_am_futu_combofund_qry = 769152,//组合层期货可用资金查询

			



	



 

		};

		/*

		amprice_type	价格类型
		'0'	限价，[上交所、深交所]
		'a'	五档即成剩撤，[上交所]
		'b'	五档即成剩转，[上交所]
		'A'	五档即成剩撤，[深交所]
		'C'	即成剩撤，[深交所]
		'D'	对手方最优，[深交所]
		'E'	本方最优，[深交所]
		'F'	全额成或撤，[深交所]
		'1'	任意价，[中金所、上期所、郑商所，大商所]
		'2'	限价，[中金所、上期所、郑商所，大商所]


		*/


		class UHST2Helper
		{
		public:
			static std::string GetLanguagePreferString(ELanguagePref prefer);

			static std::string OpeToHST2String(  Order order );//增加支持期货的功能
			static EBuySell HST2ToBuySell(const std::string& str);
			static EOpenClose HST2ToOpenClose(const std::string& str);
			static std::string GetFixedPriceCode(tstring code);


			static std::string GetSeatNo(const HST2IniParamA& input, const TCHAR* code );

			/*
			exchange_type	交易市场
				'1'	上交所
				'2'	深交所
				'3'	上期所
				'4'	郑商所
				'7'	中金所
				'9'	大商所
				*/

			static std::string GetExchangetype(tstring contractcode);

			static void DumpIF2Unpack(IF2UnPacker* lpUnPacker);
			
		};


		

		
	}
}