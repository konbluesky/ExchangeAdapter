#include <Include/mcapi_interface.h>

void PrintUnPack(IF2UnPacker* lpUnPack)
{
	printf("记录行数：           %d\n",lpUnPack->GetRowCount());
	printf("列行数：			 %d\n",lpUnPack->GetColCount());
	while (!lpUnPack->IsEOF())
	{
		for (int i=0;i<lpUnPack->GetColCount();i++)
		{
			char* colName = (char*)lpUnPack->GetColName(i);
			char colType = lpUnPack->GetColType(i);
			if (colType!='R')
			{
				char* colValue = (char*)lpUnPack->GetStrByIndex(i);
				printf("%s:			[%s]\n",colName,colValue);
			}
			else
			{
				int colLength = 0;
				char* colValue = (char*)lpUnPack->GetRawByIndex(i,&colLength);
				printf("%s:			[%s](%d)\n",colName,colValue,colLength);
			}
		}
		lpUnPack->Next();
	}
	
}

void ParseMsg(IESBMessage* lpMsg)
{
	char* topicName = (char*)lpMsg->GetItem(TAG_MC_TOPICNAME)->GetString();
	int topicNo = lpMsg->GetItem(TAG_MC_TOPICNO)->GetInt();
	char* publisherName = (char*)lpMsg->GetItem(TAG_MCAPPLICATION_NAME)->GetString();
	int msgNo = lpMsg->GetItem(TAG_MCMSG_ID)->GetInt();
	int RebulidFlag = lpMsg->GetItem(TAG_MCNOTIFY_STATUS)->GetInt();

	int appLength = 0;
	char* appdata = (char*)lpMsg->GetItem(TAG_MCADDITION_DATA)->GetRawData(&appLength);
	printf("***************************\n");
	printf("----------消息头部分-------\n");
	printf("主题名字：           %s\n",topicName);
	printf("主题编号：           %d\n",topicNo);
	printf("发布者名字：         %s\n",publisherName);
	printf("发布序号：           %d\n",msgNo);
	printf("补缺标识：           %d\n",RebulidFlag);
	printf("附加数据长度：       %d\n",appLength);
	if (appLength>0)
	{
		printf("附加数据：           %s\n",appdata);
	}
	printf("---------------------------\n");
	printf("----------业务体部分-------\n");
	int packLen = 0;
	char* packData = (char*)lpMsg->GetItem(TAG_MESSAGE_BODY)->GetRawData(&packLen);
	if (packData && packLen>0)
	{
		IF2UnPacker* lpUnPack = NewUnPacker(packData,packLen);
		if (lpUnPack)
		{
			PrintUnPack(lpUnPack);
			lpUnPack->Release();
		}
	}

	printf("---------------------------\n");
	printf("***************************\n");
}

class SubCallBak : public CSubCallbackInterface
{
public:
	SubCallBak()
	{

	}
	~SubCallBak()
	{

	}
	void FUNCTION_CALL_MODE OnReceived(CSubscribeInterface *lpSub,char* topicName, const void *lpData, int nLength)
	{
		IESBMessage* lpMsg = NewESBMessage();
		lpMsg->AddRef();
		if(!lpMsg->SetBuffer(lpData,nLength))
		{
			ParseMsg(lpMsg);
		}
		else
		{
			printf("不是esbmsg : topicName %s,nLength: %d\n",topicName,nLength);
		}	
		lpMsg->Release();
	}

	unsigned long  FUNCTION_CALL_MODE QueryInterface( HS_SID iid, IKnown **ppv ) 
	{
		return 0;
	}
    unsigned long  FUNCTION_CALL_MODE AddRef() 
	{
		return 0;
	}
    unsigned long  FUNCTION_CALL_MODE Release()
	{
		return 0;
	}
	virtual void FUNCTION_CALL_MODE OnRecvTickMsg(CSubscribeInterface *lpSub,CSubscribeParamInterface *lpSubParam,const char* TickMsgInfo) 
	{

	}
};


int main(int argc, char** argv )
{
	char* fileName = argv[1];
	CConfigInterface * lpT2sdk = NewConfig();
	lpT2sdk->Load(fileName);

	//获取业务名
	char* bizName = (char*)lpT2sdk->GetString("subcribe","biz_name","");
	printf("初始化\n");
	//初始化
	int iRet = MCInit(lpT2sdk,bizName,5000,TYPE_SUBCRIBER);
	if (iRet!=0)
	{
		printf("MCInit Error: %d,%s\n",iRet,GetErrorMsg(iRet));
		return -1;
	}

	//创建订阅接口指针
	SubCallBak SubCall ;
	printf("建会话\n");
	CSubscribeInterface* lpSub = NewSubscribe(&SubCall, 5000);
	lpSub->AddRef();
	if (!lpSub)
	{
		printf("NewSubscribe Error: %s\n",GetMCLastError());
		return -1;
	}

	//订阅参数获取
	CSubscribeParamInterface* lpSubscribeParam = NewSubscribeParam();
	lpSubscribeParam->AddRef();
	char* topicName = (char*)lpT2sdk->GetString("subcribe","topic_name","");//主题名字
	lpSubscribeParam->SetTopicName(topicName);
	char* isFromNow = (char*)lpT2sdk->GetString("subcribe","is_rebulid","");//是否补缺
	if (strcmp(isFromNow,"true")==0)
	{
		lpSubscribeParam->SetFromNow(true);
	}
	else
	{
		lpSubscribeParam->SetFromNow(false);
	}

	char* isReplace = (char*)lpT2sdk->GetString("subcribe","is_replace","");//是否覆盖
	if (strcmp(isReplace,"true")==0)
	{
		lpSubscribeParam->SetReplace(true);
	}
	else
	{
		lpSubscribeParam->SetReplace(false);
	}
	
	char* lpApp = "xuxinpeng";
	lpSubscribeParam->SetAppData(lpApp,9);//添加附加数据

	//添加过滤字段
	int nCount = lpT2sdk->GetInt("subcribe","filter_count",0);
	for (int i=1;i<=nCount;i++)
	{
		char lName[128]={0};
		sprintf(lName,"filter_name%d",i);
		char* filterName = (char*)lpT2sdk->GetString("subcribe",lName,"");
		char lValue[128]={0};
		sprintf(lValue,"filter_value%d",i);
		char* filterValue = (char*)lpT2sdk->GetString("subcribe",lValue,"");
		lpSubscribeParam->SetFilter(filterName,filterValue);
	}
	//添加发送频率
	lpSubscribeParam->SetSendInterval(lpT2sdk->GetInt("subcribe","send_interval",0));
	//添加返回字段
	nCount = lpT2sdk->GetInt("subcribe","return_count",0);
	for (int k=1;k<=nCount;k++)
	{
		char lName[128]={0};
		sprintf(lName,"return_filed%d",k);
		char* filedName = (char*)lpT2sdk->GetString("subcribe",lName,"");
		lpSubscribeParam->SetReturnFiled(filedName);
	}

	
	
	printf("开始订阅\n");
	iRet = lpSub->SubscribeTopic(lpSubscribeParam,5000,NULL);
	printf("SubscribeTopic info:[%d] %s\n",iRet,GetErrorMsg(iRet));

	//打印已经订阅的主题信息
	printf("**********************************************\n");
	IF2Packer* lpPack = NewPacker(2);
	lpPack->AddRef();
	lpSub->GetSubcribeTopic(lpPack);
	if (lpPack)
	{
		PrintUnPack(lpPack->UnPack());
	}
	lpPack->FreeMem(lpPack->GetPackBuf());
	lpPack->Release();
	printf("**********************************************\n");
	printf("输入任意字符取消订阅\n");
	getchar();


	//添加过滤字段
	CFilterInterface* lpFilter = NewFilter();
	lpFilter->AddRef();
	nCount = lpT2sdk->GetInt("subcribe","filter_count",0);
	for (int j=1;j<=nCount;j++)
	{
		char lName[128]={0};
		sprintf(lName,"filter_name%d",j);
		char* filterName = (char*)lpT2sdk->GetString("subcribe",lName,"");
		char lValue[128]={0};
		sprintf(lValue,"filter_value%d",j);
		char* filterValue = (char*)lpT2sdk->GetString("subcribe",lValue,"");
		lpFilter->SetFilter(filterName,filterValue);
	}
	//取消订阅
	iRet = lpSub->CancelSubscribeTopic(topicName,lpFilter);
	printf("CancelSubscribeTopic:%d %s\n",iRet,GetErrorMsg(iRet));
	printf("输入任意字符退出\n");
	getchar();

	//释放订阅端
	lpSub->Release();
	printf("退出\n");
	return 0;
}
