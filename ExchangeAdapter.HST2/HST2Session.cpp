#include "StdAfx.h"

#include "tdataset.h"
#include "tdatasets.h"
#include "ExchangeAdapterDef.h"

#include "t2sdk_interface.h"
#include "HST2Helper.h"
#include "HST2AutoPtr.h"
#include "HST2Session.h"
#include "HST2TradeAdapter.h"



USING_LIBSPACE


namespace tradeadapter
{
	namespace hst2
	{
		CHST2Session::CHST2Session( CHST2TradeAdapter* pinstance )
		{
			m_instance = pinstance;
		}

		CHST2Session::~CHST2Session(void)
		{
			m_instance = NULL;
		}

		void CHST2Session::AddField(const char* key, const char* val)
		{
			m_datas.push_back(std::make_pair<std::string,std::string>(key,val));
		}

		void CHST2Session::AddField( const char* key, const std::string& val )
		{
			std::string tmp = val;
			m_datas.push_back(std::make_pair<std::string, std::string>(PAIR_WRAP(key), PAIR_WRAP(tmp)));
		}



		void CHST2Session::BeginJob()
		{
		}

		

		


		void DumpPacket(IF2UnPacker *lpUnPacker)
		{
			int i = 0, t = 0, j = 0, k = 0;

			for (i = 0; i < lpUnPacker->GetDatasetCount(); ++i)
			{
				// 设置当前结果集
				lpUnPacker->SetCurrentDatasetByIndex(i);

				// 打印字段
				for (t = 0; t < lpUnPacker->GetColCount(); ++t)
				{
					printf("%20s", lpUnPacker->GetColName(t));
				}

				putchar('\n');

				// 打印所有记录
				for (j = 0; j < (int)lpUnPacker->GetRowCount(); ++j)
				{
					// 打印每条记录
					for (k = 0; k < lpUnPacker->GetColCount(); ++k)
					{
						switch (lpUnPacker->GetColType(k))
						{
						case 'I':
							printf("%20d", lpUnPacker->GetIntByIndex(k));
							break;

						case 'C':
							printf("%20c", lpUnPacker->GetCharByIndex(k));
							break;

						case 'S':
							printf("%20s", lpUnPacker->GetStrByIndex(k));
							break;

						case 'F':
							printf("%20f", lpUnPacker->GetDoubleByIndex(k));
							break;

						case 'R':
							{
								int nLength = 0;
								void *lpData = lpUnPacker->GetRawByIndex(k, &nLength);

								// 对2进制数据进行处理
								break;
							}

						default:
							// 未知数据类型
							printf("未知数据类型。\n");
							break;
						}
					}

					putchar('\n');

					lpUnPacker->Next();
				}

				putchar('\n');
			}
		}



		bool CHST2Session::CommitJob(FunctionID jobid, TDataSets* pResult)
		{
			IF2PackerPtr lpPacker = PtrFactory::GetNewPacker();

			lpPacker->BeginPack();
			for(std::vector<std::pair<std::string,std::string> >::size_type j=0; j<m_datas.size(); ++j)
			{
				lpPacker->AddField(m_datas[j].first.c_str());
			}
			for(std::vector<std::pair<std::string,std::string> >::size_type j=0; j<m_datas.size(); ++j)
			{
				lpPacker->AddStr(m_datas[j].second.c_str());
			}
			lpPacker->EndPack();

			int hsend=m_instance->m_ConnectionPtr->SendBiz(jobid, IF2PackerPtr::GetPointer(lpPacker), 0);
			lpPacker->FreeMem(lpPacker->GetPackBuf());

			if(hsend <0)
			{				
				m_instance->_updateLastError(hsend);
				return false;
			}

			void * lpUnpack1 = NULL;
			int iRcv=m_instance->m_ConnectionPtr->RecvBiz(hsend,&lpUnpack1,10000);

			//按照文档中对RecvBiz函数的说明，iRcv的值可能会有0,1,2,3四种结果
			//只有iRcv为0或者1时，lpUnpack1才会指向一个IF2UnPacker结构
			if( iRcv == 0 || iRcv == 1 )
			{
				bool ret = GetDataResult( (IF2UnPacker*)lpUnpack1, pResult );

				return ret;
			}
			else//TO DO iRcv是0和1之外的其它值时，如何处理，待定...
			{
				m_instance->_updateLastError(iRcv);
				return false;
			}
		}

		bool CHST2Session::GetDataResult( IF2UnPacker* lpUnpack, TDataSets* pResult)
		{
//			DumpPacket(lpUnpack);
			int colcount = lpUnpack->GetColCount();
			int rowcount = lpUnpack->GetRowCount();
			for(int i=0; i<rowcount; ++i)
			{	
				shared_ptr< TDataSet>  res = shared_ptr< TDataSet>(new TDataSet);
				for(int j=0; j<colcount; ++j)
				{
					std::string key = lpUnpack->GetColName(j);
					std::string val = lpUnpack->GetStrByIndex(j);

					(*res.get())[key] = val;					
				}
				//UEXDebugHelper::Dump(res);
				pResult->m_bags.push_back(res);
				lpUnpack->Next();
			}
			return true;
		}

		

	};
};