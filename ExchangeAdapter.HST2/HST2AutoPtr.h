#ifndef _EXCHANGE_ADAPTER_HST2_
#define _EXCHANGE_ADAPTER_HST2_

#include "loki/SmartPtr.h"
/*
template<typename T>
Loki::SmartPtr<T,Loki::COMRefCounted>;
*/
typedef Loki::SmartPtr<CConfigInterface,Loki::COMRefCounted>   CConfigInterfacePtr;
typedef Loki::SmartPtr<CConnectionInterface,Loki::COMRefCounted>   CConnectionInterfacePtr;
typedef Loki::SmartPtr<IF2Packer,Loki::COMRefCounted>   IF2PackerPtr;


class PtrFactory
{
public:
	static CConfigInterfacePtr GetNewConfig()
	{
		CConfigInterface* pinterface = NewConfig();
		pinterface->AddRef();
		CConfigInterfacePtr ret = pinterface;
		return ret;
	}
	static CConnectionInterfacePtr GetNewConnection(CConfigInterface* pPtr)
	{
		CConnectionInterface* pinterface = NewConnection(pPtr);
		pinterface->AddRef();
		CConnectionInterfacePtr ret = pinterface;
		return ret;

	}

	static  IF2PackerPtr GetNewPacker()
	{
		IF2Packer* pinterface = NewPacker(2);
		pinterface->AddRef();
		IF2PackerPtr ret = pinterface;
		return ret;
	}

};





#endif
