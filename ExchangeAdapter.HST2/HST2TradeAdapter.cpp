#include "StdAfx.h"
#include "ucodehelper.h"
#include "ExchangeAdapterDef.h"
#include <windows.h>
#include <algorithm>

#include "umaths.h"
#include "unetutils.h"
#include "ustringutils.h"
#include "threadtask.h"
#include "ucomhelper.h"
#include "texceptionex.h"
#include "uconsole.h"

#include "tinyxml.h"
#include "tconfigsetting.h"

#include "t2sdk_interface.h"
#include "HST2Helper.h"
#include "HST2AutoPtr.h"
#include "HST2Session.h"
#include "HST2TradeAdapter.h"
#include "tdatasetS.h"



#pragma warning(disable : 4482)

USING_LIBSPACE

namespace tradeadapter
{

	namespace hst2
	{

		CHST2TradeAdapter::CHST2TradeAdapter(void)
		{
			m_imp.SetAdapter(this);			
		}

		CHST2TradeAdapter::~CHST2TradeAdapter(void)
		{
		}

		bool CHST2TradeAdapter::Initialize( void* param  )
		{		
			int ret = -1;
			HST2IniParam* input = (HST2IniParam*)param;
			m_pParam = HST2IniParamA::FromHST2IniParam(*input);
			

			CConfigInterfacePtr cfgptr = PtrFactory::GetNewConfig();			
			//[t2sdk] servers指定需要连接的IP地址及端口，可配置多个，中间以“;”间隔			
			cfgptr->SetString("t2sdk", "servers",m_pParam. addrandport.c_str());

			//[t2sdk] license_file指定许可证文件路径			
			cfgptr->SetString("t2sdk", "license_file",m_pParam.licensefile.c_str());

			//[t2sdk] lang指定错误信息的语言号（缺省为简体中文2052），1033为英文
			std::string language = UHST2Helper::GetLanguagePreferString(m_pParam.pref);
			cfgptr->SetString("t2sdk", "lang",language.c_str());

			//[t2sdk] send_queue_size指定T2_SDK的发送队列大小
			cfgptr->SetString("t2sdk", "send_queue_size", "100");

			//[safe] safe_level指定连接的安全模式，需要和T2通道的安全模式一致，否则连接失败
			cfgptr->SetString("safe", "safe_level", "none");

			//通过T2SDK的引出函数，来获取一个新的CConnection对象指针

			m_ConnectionPtr =PtrFactory::GetNewConnection(CConfigInterfacePtr::GetPointer(cfgptr));
			

		
			
			 //初始化连接对象，返回0表示初始化成功，注意此时并没开始连接服务器
			ret = m_ConnectionPtr->Create(&m_imp);
			if(ret != 0)
			{
				_updateLastError(ret);
				return false;
			
			}

			
			ret = m_ConnectionPtr->Connect(m_pParam.timeout);
			if(ret !=0 )
			{
				_updateLastError(ret);
				return false;
			}
			m_initialized = _Login();
			return m_initialized;		
		}

		bool CHST2TradeAdapter::IsInitialized()
		{
			return m_initialized;
		}

		bool CHST2TradeAdapter::InitializeByXML( const TCHAR* fpath )
		{
			TConfigSetting cfgsetting ;
			bool ret = cfgsetting.LoadConfig(fpath);
			if(!ret)
			{
				std::cout<<"无法加载配置文件，是否是当前目录"<<std::endl;
				return ret;
			}
			HST2IniParam input;
			

			std::string ipaddr = cfgsetting["configuration"]["saddressip"].EleVal();
			std::string ipport = cfgsetting["configuration"]["iport"].EleVal();
			input.addrandport = UCodeHelper::ToTS(ipaddr + ":" + ipport);


			std::string licensefile = cfgsetting["configuration"]["licensefile"].EleVal();
			input.licensefile = UCodeHelper::ToTS(licensefile);

			std::string languagepref = cfgsetting["configuration"]["languagepref"].EleVal();
			input.pref = languagepref == "chinese" ? ELanguagePref::eChinese : ELanguagePref::eEnglish;

			std::string operator_no = cfgsetting["configuration"]["operator_no"].EleVal();
			input.operator_no = UCodeHelper::ToTS(operator_no);

			

			std::string password = cfgsetting["configuration"]["password"].EleVal();
			input.password = UCodeHelper::ToTS(password);

			std::string timeout =  cfgsetting["configuration"]["timeout"].EleVal();
			input.timeout = UMaths::ToInt32(timeout);

			std::string rectimes = cfgsetting["configuration"]["reconnecttimes"].EleVal();
			input.reconnecttimes = UMaths::ToInt32(rectimes);

			std::string fund_account = cfgsetting["configuration"]["fund_account"].EleVal();
			input.fund_account = UCodeHelper::ToTS(fund_account);


			std::string combine_id = cfgsetting["configuration"]["combine_id"].EleVal();
			input.combine_id = UCodeHelper::ToTS(combine_id);




			std::string shseat_no = cfgsetting["configuration"]["shseat_no"].EleVal();
			input.shseat_no = UCodeHelper::ToTS(shseat_no);
			
			ret = this->Initialize(&input);


			return ret;
		}

		bool CHST2TradeAdapter::EntrustOrder(Order order, OrderID* pOutID)
		{
			*pOutID = tradeadapter::InvalidOrderId;
			std::string exchangetype = UHST2Helper::GetExchangetype(order.ContractCode);
			std::string code = UCodeHelper::ToAS(order.ContractCode);
			std::string opedir = UHST2Helper::OpeToHST2String(order);
			std::string fixedcode = UHST2Helper::GetFixedPriceCode(order.ContractCode);
			std::string seat_no = UHST2Helper::GetSeatNo(m_pParam,order.ContractCode.c_str());

			CHST2Session sess(this);
			sess.BeginJob();
			sess.AddField("user_token",m_pParam.m_user_token.c_str());			
			sess.AddField("extsystem_id",m_pParam.extsystem_id.c_str());
			sess.AddField("batch_no","");
			sess.AddField("fund_account",m_pParam.fund_account.c_str());
			sess.AddField("combine_id",m_pParam.combine_id.c_str());
			sess.AddField("stock_account","");
			sess.AddField("seat_no",seat_no.c_str());			
			sess.AddField("exchange_type",exchangetype.c_str());
			sess.AddField("stock_code",code.c_str());			
			sess.AddField("entrust_direction",opedir.c_str());			
			sess.AddField("amprice_type",fixedcode.c_str());
			sess.AddField("entrust_amount",UCodeHelper::ToAS( UStrConvUtils::ToString(order.Amount)).c_str() );
			sess.AddField("entrust_price", UCodeHelper::ToAS(UStrConvUtils::ToString(order.Price)).c_str());
			sess.AddField("invest_way","1");
			sess.AddField("close_direction","");  //only for future.

			//sess.ad

			TDataSets result;
			bool ret = sess.CommitJob(FunctionID::func_am_entrust,&result);
			if(!ret)
			{
				return false;
			}
			if(result[0].GetMap().find("entrust_no") != result[0].GetMap().end())
			{
				int entrust_no = UMaths::ToInt32(result[0]["entrust_no"]);
				*pOutID =  entrust_no;
				return true;
			}
			return false;

		}

		
		bool CHST2TradeAdapter::CancelOrder( OrderID orderid )
		{
			CHST2Session sess(this);
			sess.BeginJob();
			sess.AddField("user_token",m_pParam.m_user_token.c_str());			
			sess.AddField("extsystem_id",m_pParam.extsystem_id.c_str());			
			sess.AddField("entrust_no",UCodeHelper::ToAS(UStrConvUtils::ToString(orderid)).c_str());
			sess.AddField("fund_account",m_pParam.fund_account.c_str());
			//sess.AddField("stock_account","D890776152");
			//sess.AddField("seat_no","13588");

			
			
			TDataSets result;
			bool ret = sess.CommitJob(FunctionID::func_am_entrust_withdraw,&result);
			
			if(!ret)
			{
				return false;
			}
			//已撤单
			if(result.GetCount()>0 && result[0].GetMap().find("error_no") != result[0].GetMap().end() && result[0]["error_no"] == "201")
			{
				return true;
			}

			return ret;

		}


		std::pair<bool,int> CHST2TradeAdapter::QueryDealedOrderInfo( OrderID id )
		{
			CHST2Session sess(this);
			sess.BeginJob();
			std::string strorderid = UCodeHelper::ToAS(UStrConvUtils::ToString(id));
			sess.AddField("user_token",m_pParam.m_user_token.c_str());
			sess.AddField("begin_entrust_no",strorderid.c_str());
			sess.AddField("end_entrust_no",strorderid.c_str());
			TDataSets result;
			bool ret = sess.CommitJob(FunctionID::func_am_realtime_qry,&result);
			if(!ret || result.GetCount() == 0)
			{
				return std::make_pair<bool,int>(false,0);
			}
			

			if(result[0].GetMap().find("business_amount") != result[0].GetMap().end())
			{
				double res = UMaths::ToDouble( result[0]["business_amount"].c_str());
				return std::make_pair<bool,int>(true,(int)res);
			}
			else
			{
				return std::make_pair<bool,int>(false,0);
			}
		}

		bool CHST2TradeAdapter::QueryOrder(OrderID id, int* pCount)
		{
			*pCount = 0;
			//comment: hst2, if input =0 ,then will output all orders info.
			if(id == 0 || id == tradeadapter::InvalidOrderId)
			{
				return true;
			}

			std::pair<bool,int> ret1 = QueryDealedOrderInfo(id);
			if(ret1.first )
			{
				*pCount = ret1.second;
				return true;
			}
			//////////////////////////////
			//query online order

			TDataSets result;
			
			_QueryEntrustInfo(&result,id);
			double amount = 0;
			for(unsigned int i=0; i<result.GetCount(); ++i)
			{
				amount += UMaths::ToDouble( result[i]["business_amount"] );

			}
			int data = (int)amount;			
			*pCount =  data;
			return true;
		}

		
		



		bool CHST2TradeAdapter::_QueryEntrustInfo( TDataSets* psets,OrderID id )
		{
			CHST2Session sess(this);
			sess.BeginJob();
			std::string strorderid = UCodeHelper::ToAS(UStrConvUtils::ToString(id));
			if(id == InvalidOrderId)
			{
				strorderid = "";
			}

			sess.AddField("user_token",m_pParam.m_user_token.c_str());
			sess.AddField("begin_entrust_no",strorderid.c_str());
			sess.AddField("end_entrust_no",strorderid.c_str());
			
			bool ret = sess.CommitJob(FunctionID::func_am_entrust_qry,psets);
			if(!ret)
			{
				return false;
			}
			

			return true;
		}

		
		int compareBusinessid(shared_ptr<TDataSet> left, shared_ptr<TDataSet> right)
		{
			int leftval = UMaths::ToInt32((* left.get())["business_no"]);
			int rightval = UMaths::ToInt32((*right.get())["business_no"]);
			return leftval - rightval;
			
		}
		

		//注：这里直接支持期货
		bool CHST2TradeAdapter::QueryDealedOrders( std::list<DealedOrder>* pres )
		{
			pres->clear();

			CHST2Session sess(this);
			sess.BeginJob();
			
			sess.AddField("user_token",m_pParam.m_user_token.c_str());
			sess.AddField("begin_entrust_no","");
			sess.AddField("end_entrust_no","");
			TDataSets result;
			bool ret = sess.CommitJob(FunctionID::func_am_realtime_qry,&result);
			if(!ret)
			{
				return false;
			}

			std::sort(result.m_bags.begin(),result.m_bags.end(),compareBusinessid);
			for (unsigned  i = 0; i<result.GetCount(); ++i)
			{
				DealedOrder info;
				info.business_amount = UMaths::ToDouble(result[i]["business_amount"]);
				info.business_balance = UMaths::ToDouble(result[i]["business_balance"]);

				info.business_price = UMaths::ToDouble(result[i]["business_price"]);

				//info.business_status = UMaths::ToDouble(result[i]["business_amount"]);
				info.business_time = UMaths::ToInt32(result[i]["business_time"]);
				info.entrust_bs = UHST2Helper::HST2ToBuySell(result[i]["entrust_direction"]);
				info.entrust_no = UMaths::ToInt32(result[i]["entrust_no"]);
				info.serial_no = UMaths::ToInt32(result[i]["business_no"]);

				info.stock_account = UCodeHelper::ToTS(result[i]["stock_account"]);
				info.stock_code = UCodeHelper::ToTS(result[i]["stock_code"]);

				//info.posside

				pres->push_back(info);
				
			}
			return true;

			
		}

		void CHST2TradeAdapter::UnInitialize()
		{
		
			_Logout();
		
			if(m_ConnectionPtr)
			{
				m_ConnectionPtr->Close();
				m_ConnectionPtr = NULL;
			}

		}

		bool CHST2TradeAdapter::QueryAccountInfo( BasicAccountInfo* info )
		{

			info->ClientID = UCodeHelper::ToTS( m_pParam.operator_no );
			info->ClientName = UCodeHelper::ToTS(m_pParam.operator_no);
			
			CHST2Session sess(this);
			sess.BeginJob();

			sess.AddField("user_token",m_pParam.m_user_token.c_str());
			sess.AddField("fund_account","");
			sess.AddField("combine_id",m_pParam.combine_id.c_str());

			TDataSets result;
			bool ret = sess.CommitJob(FunctionID::func_am_combofund_qry,&result);
			if(!ret)
			{
				return false;
			}
			if(result.GetCount() <= 0 || result.GetCount() >1)
			{
				throw TException(_T("查询账户信息时，返回的结果暂时无法处理"));
			}
			for(unsigned int i=0; i<result.GetCount(); ++i)
			{
				if(result.GetCount()== 1  || m_pParam.fund_account == result[i]["fund_account"])
				{				
					info->FundAccount = UCodeHelper::ToTS(result[i]["fund_account"]);
					info->EnableBalance = UMaths::ToDouble(result[i]["enable_balance"]);
					info->CurrentBalance = UMaths::ToDouble(result[i]["enable_balance_t1"]);
					m_pParam.fund_account = result[i]["fund_account"];
				}
				//else
			}
			return true;
		}

		//组合持仓查询
		bool CHST2TradeAdapter::QueryPostions( std::list<PositionInfo>* pPoss,const TCHAR* pstock )
		{

			pPoss->clear();

			pstock = pstock == NULL ? _T("") :pstock;
			std::string stkcode = UCodeHelper::ToAS(pstock);
			CHST2Session sess(this);
			sess.BeginJob();
			sess.AddField("user_token",m_pParam.m_user_token.c_str());
			sess.AddField("init_date","");
			sess.AddField("fund_account","");
			sess.AddField("combine_id",m_pParam.combine_id.c_str());
			sess.AddField("exchange_type","");
			sess.AddField("stock_code",stkcode.c_str());


			TDataSets result;
			sess.CommitJob(FunctionID::func_am_combostock_qry,&result);

			//todo，这里会出现多个数据股票号码相同的情况，
			for(unsigned int i = 0; i< result.GetCount(); ++i)
			{
				PositionInfo info;
				info.code = UCodeHelper::ToTS(result[i]["stock_code"]);
				info.cost_price = UMaths::ToDouble(result[i]["cost_balance"]);
				info.current_amount = UMaths::ToDouble(result[i]["current_amount"]);
				info.enable_amount = UMaths::ToDouble(result[i]["enable_amount"]);
				//info.datetype = 
				info.market_value = UMaths::ToDouble(result[i]["market_value"]);
				info.stock_account = UCodeHelper::ToTS(result[i]["stock_account"]);
				info.posside = result[i]["pupil_flag"] == "1" ? EPosSide ::eMore : EPosSide::eShort;

				//info.stock_name
				pPoss->push_back(info);
			}


			/*
			init_date
				fund_account
				combine_id
				stock_account
				seat_no
				exchange_type
				stock_code
				invest_way
				pupil_flag
				begin_amount
				current_amount
				enable_amount
				cost_balance
				market_value
				buy_amount
				sell_amount
				*/

			return true;
		}


		void CHST2TradeAdapter::_updateLastError(int errcode)
		{
			std::string msg =  	m_ConnectionPtr->GetErrorMsg(errcode);
			SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter, errcode, msg.c_str()));
			
		}



		

		

		DWORD WINAPI  CHST2TradeAdapter::_reconnectImp( void* param )
		{
			CHST2TradeAdapter* pInstance = (CHST2TradeAdapter*)param;			
			for(int i=0; i<pInstance->m_pParam.reconnecttimes; ++i)
			{
				int ret = 0;
				if(ret = pInstance->m_ConnectionPtr->Connect(30000))
				{
					::Sleep(100);
				}
				else
				{				
					return true;
				}
			}
			return false;
		}

		bool CHST2TradeAdapter::_reconnect(void* )
		{

			DWORD dwthreadid;
			HANDLE hthread = CreateThread(NULL,0,CHST2TradeAdapter::_reconnectImp,this,0,&dwthreadid);
			::CloseHandle(hthread);

			return true;			
		}

		

		bool CHST2TradeAdapter::_Login()
		{
			UConsole::Print(UConsole::TEXT_GREEN,"登录");


			std::string mac = UCodeHelper::ToAS( UNetUtils::GetMacAddress() ) ;
			mac = mac.size() == 0 ? "DD-AA-BB-CC-EE-77" : mac;

			tstrings addrsss = UNetUtils::GetIpAddress();
			std::string inputaddr = addrsss.size() >0 ? UCodeHelper::ToAS( addrsss[0] ) : "192.168.185.93";


			CHST2Session sess(this);
			sess.BeginJob();
			sess.AddField("operator_no",m_pParam.operator_no.c_str());
			sess.AddField("password",m_pParam.password.c_str());

			sess.AddField("mac_address",mac.c_str());

			sess.AddField("station_add",inputaddr.c_str());			
			sess.AddField("ip_address",inputaddr.c_str());

			

			

			TDataSets result;
			bool ret = sess.CommitJob(FunctionID::func_am_user_login,&result);

			if(ret)
			{
				m_pParam.m_user_token = result[0]["user_token"];
				UConsole::Print(UConsole::TEXT_GREEN,m_pParam.m_user_token.c_str());
				if(m_pParam.m_user_token == "")
				{
					UConsole::Print(UConsole::TEXT_RED,"登录失败，获取用户token失败");
					return false;				
				}
			}
			else
			{
				UConsole::Print(UConsole::TEXT_RED,"登录失败，获取用户token失败");
			}

			BasicAccountInfo info;
			QueryAccountInfo(&info);//get account fund_account;
			return ret;
			
		}	

		bool CHST2TradeAdapter::QueryEntrustInfos( std::list<EntrustInfo>* pinfos )
		{
			pinfos->clear();

			TDataSets res;
			bool ret = _QueryEntrustInfo( &res ,InvalidOrderId);

			
			if(ret && res.GetCount()>0 )
			{
				for(unsigned int i=0; i<res.GetCount() ; ++i)
				{
					EntrustInfo info;
					info.matchqty = UMaths::ToInt32(res[i]["business_amount"]);					
					info.matchamt = UMaths::ToDouble(res[i]["business_balance"]);
					info.Ope  = UHST2Helper::HST2ToBuySell(res[i]["entrust_direction"]); // todo.
					info.orderprice = UMaths::ToDouble(res[i]["entrust_price"]);
					info.ordertime = UMaths::ToInt32(res[i]["entrust_time"]);
					info.orderid = UMaths::ToInt32(res[i]["entrust_no"]);
					info.orderqty = UMaths::ToInt32(res[i]["entrust_amount"]);
					info.stkcode = UCodeHelper::ToTS(res[i]["stock_code"]);
					info.futureOC = UHST2Helper::HST2ToOpenClose(res[i]["entrust_direction"]);
					pinfos->push_back(info);

				}
			}
				/*

				("amentrust_status","1")	
+		[1]	("batch_no","787071")	
+		[2]	("business_amount","0")	
+		[3]	("business_balance","0")	
+		[4]	("cancel_info","")	
+		[5]	("combine_id","4100")	
+		[6]	("entrust_amount","100")	
+		[7]	("entrust_direction","1")	
+		[8]	("entrust_no","867312")	
+		[9]	("entrust_price","8.22")	
+		[10]	("entrust_time","103601")	
+		[11]	("exchange_type","1")	

+		[17]	("position_str","1")	
+		[18]	("report_time","0")	
+		[19]	("schema_no","0")	
+		[20]	("seat_no","13588")	
+		[21]	("stock_account","D890776152")	
+		[22]	("stock_code","600016")	
+		[23]	("withdraw_amount","0")	

				*/

			
			return ret;			
		}

		void CHST2TradeAdapter::_Logout()
		{
			if(m_pParam.m_user_token != "")
			{			
				CHST2Session sess(this);
				sess.BeginJob();
				sess.AddField("user_token",m_pParam.m_user_token.c_str());
				sess.AddField("operator_no",m_pParam.operator_no.c_str());
				TDataSets res;
				sess.CommitJob(FunctionID::func_am_user_logout,&res);
			}
			m_pParam.m_user_token = "";

		}

		bool CHST2TradeAdapter::_QueryFutureAccountInfo(double* pfutu_enable_balance)
		{
			CHST2Session sess(this);
			sess.AddField("user_token",m_pParam.m_user_token.c_str());
			sess.AddField("fund_account",m_pParam.fund_account.c_str());
			sess.AddField("combine_id",m_pParam.combine_id.c_str());

			TDataSets res;
			bool ret = sess.CommitJob(FunctionID::func_am_futu_combofund_qry,&res);
			if (!ret)
			{
				return ret;
			}
			
			*pfutu_enable_balance = UMaths::ToDouble(res[0]["futu_enable_balance"]);

			return true;


		}

		tstring CHST2TradeAdapter::GetName()
		{
			throw std::logic_error("The method or operation is not implemented.");
		}

	}
}

