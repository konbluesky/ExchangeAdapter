#ifndef __HST2_TRADEADAPTER_H__
#define __HST2_TRADEADAPTER_H__




#include "tdataset.h"
#include "tdatasets.h"


#include "IOrderService.h"
#include "ITradeAdapter.h"
#include "t2sdk_interface.h"
#include "HST2AutoPtr.h"
#include "HST2Helper.h"
#include "CallBackImp.h"
#include "threadlock.h"
#include <vector>
#include <string>
#include <algorithm>
#include <map>
#include "threadlock.h"


USING_LIBSPACE

//注：恒生2的柜台，使用的客户比较多，因此扩展成为多账户模型的必要比较大。
namespace tradeadapter
{
	namespace hst2
	{


		struct HST2IniParam
		{
			tstring addrandport; // format: "192.168.94.30:9999"  or "192.168.94.30:9999;192.168.94.31:9992"
			int timeout;
			int reconnecttimes;

			tstring licensefile;
			ELanguagePref pref;		//msg show prefer.
			tstring operator_no;
			tstring password;

			tstring combine_id;

			tstring extsystem_id;



			tstring fund_account;

			//上海席位
			tstring shseat_no;
			tstring szseat_no;
			tstring zjseat_no;



		};


		struct HST2IniParamA
		{
			std::string addrandport; // format: "192.168.94.30:9999"  or "192.168.94.30:9999;192.168.94.31:9992"
			int timeout;
			int reconnecttimes;

			std::string licensefile;
			ELanguagePref pref;		//msg show prefer.
			std::string operator_no;
			std::string password;

			std::string combine_id;

			std::string extsystem_id;


			std::string fund_account;


			std::string m_user_token;//this get from login.

			std::string shseat_no;

			std::string szseat_no;
			std::string zjseat_no;

			static HST2IniParamA FromHST2IniParam(HST2IniParam input)
			{
				HST2IniParamA ret;
				ret.addrandport = TS2GB(input.addrandport);
				ret.licensefile = TS2GB(input.licensefile);
				ret.pref = input.pref;
				ret.operator_no = TS2GB(input.operator_no);
				ret.password = TS2GB(input.password);
				ret.extsystem_id = TS2GB(input.extsystem_id);
				ret.fund_account = TS2GB(input.fund_account);
				ret.timeout = input.timeout;
				ret.reconnecttimes = input.reconnecttimes;

				ret.combine_id = TS2GB(input.combine_id);

				ret.shseat_no = TS2GB(input.shseat_no);
				ret.szseat_no = TS2GB(input.szseat_no);
				ret.zjseat_no = TS2GB(input.zjseat_no);



				return ret;
			}

		};

		//恒生2的柜台的线程模型比较恶心。
		class TAIAPI CHST2TradeAdapter : public ITradeAdapter
		{
		public:
			CHST2TradeAdapter(void);
			virtual	~CHST2TradeAdapter(void);

			virtual bool Initialize( void* param = NULL );

			virtual bool IsInitialized();

			virtual bool InitializeByXML( const TCHAR* fpath );

			

			virtual bool EntrustOrder(Order order, OrderID* pOutID);

			virtual bool CancelOrder( OrderID orderid );


			std::pair<bool,int> QueryDealedOrderInfo(OrderID id);

			

			virtual bool QueryOrder(OrderID id, int* pCount);

			virtual bool QueryDealedOrders( std::list<DealedOrder>* pres );



			virtual void UnInitialize();

			virtual bool QueryAccountInfo( BasicAccountInfo* info );

			virtual bool QueryPostions( std::list<PositionInfo>* pPoss,const TCHAR* pstock = NULL );

			

			

			void _updateLastError(int errcode);

			bool _reconnect(void* );
			void _Logout();

			//期货可用保证金

			bool _QueryFutureAccountInfo(double* futu_enable_balance);


			bool _QueryEntrustInfo(TDataSets* psets, OrderID id);
			
			
			static DWORD WINAPI  _reconnectImp(void* param);



			bool _Login();

			virtual bool QueryEntrustInfos( std::list<EntrustInfo>* pinfo );

			virtual tstring GetName();



			friend class CHST2Session;


		private:
#pragma warning(disable : 4251)

			CConnectionInterfacePtr m_ConnectionPtr;
			CCallBackImp m_imp;			
			HST2IniParamA m_pParam;
			
			bool m_initialized;
			static threadlock::multi_threaded_local m_lock;	

			

		};

	};
};



#endif // !__HST2_TRADEADAPTER_H__