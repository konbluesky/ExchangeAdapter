#include "StdAfx.h"
#include "UProductDefHelper.h"
#include "ExchangeAdapterDef.h"
#include "texceptionex.h"
#include "ustringutils.h"
#include "HST2Helper.h"
#include "t2sdk_interface.h"
USING_LIBSPACE
namespace tradeadapter
{
	namespace hst2
	{
#pragma warning(disable : 4482)

		std::string UHST2Helper::GetLanguagePreferString( ELanguagePref prefer )
		{
			if(prefer == ELanguagePref::eEnglish)
				return "1033";
			else
				return "2052";
		}

		/*
		entrust_direction	委托方向
		'1'	股票买入
		'2'	股票卖出
		'3'	债券买入
		'4'	债券卖出
		'5'	融资回购
		'6'	融券回购
		'p'	开基申购
		'q'	开基赎回
		'V'	买入开仓
		'W'	卖出平仓
		'X'	卖出开仓
		'Y'	买入平仓
		'U'	提交质押
		'T'	转回质押


		*/

		EOpenClose UHST2Helper::HST2ToOpenClose( const std::string& str )
		{
			if(str=="V" || str == "X")
				return EOpenClose::eOpen;
			else if(str == "W" || str == "Y")
				return EOpenClose::eClose;
			else if(str == "1")
				return EOpenClose::eOpen;//
			else if(str == "2")
				return EOpenClose::eClose;
			throw TException(_T("未定义发方向"));
		}
		std::string UHST2Helper::OpeToHST2String( Order order )
		{
			if(order.ContractCode[0] == _T('I')   || order.ContractCode[0] == _T('i')  )//股指期货
			{
				if(order.OpeDir == EBuySell::eBuy && order.openclose == EOpenClose::eOpen )
				{
					return "V";
				}
				else if(order.OpeDir == EBuySell::eBuy && order.openclose == EOpenClose::eClose )
				{
					return "Y";
				}
				else if(order.OpeDir == EBuySell::eSell && order.openclose == EOpenClose::eOpen )
				{
					return "X";
				}
				else if(order.OpeDir == EBuySell::eSell && order.openclose == EOpenClose::eClose )
				{
					return "W";
				}
				else
					throw TException(_T("未定义的方向"));
			}
			else
			{		
			
				if(order.OpeDir == eBuy)
				{
					return "1";
				}
				else if(order.OpeDir == eSell)
				{
					return "2";
				}
			}
			throw TException(_T("not definition IOrderService::BuySell types"));
		}

		std::string UHST2Helper::GetFixedPriceCode( tstring code )
		{
			if(code[0] == _T('I') || code[1] == _T('i'))//for future ,use 2, and for stock use 0
				return "2";
			else
				return "0";
		}


//		'1'	上交所			'2'	深交所			'3'	上期所			'4'	郑商所			'7'	中金所			'9'	大商所
		std::string UHST2Helper::GetExchangetype( tstring contractcode )
		{
			if(contractcode[0] == _T('6') || contractcode[0] == _T('5')) //5:sh基金
				return "1";
			else if(contractcode[0] == _T('0') || contractcode[0] == _T('1')) //1:sz 基金
				return "2";
			else if(contractcode[0] == _T('i') || contractcode[0] == _T('I'))
				return "7";
			else
				throw TException(_T("未定义的交易所类型"));
		}


		void UHST2Helper::DumpIF2Unpack(IF2UnPacker* lpUnPacker)		
		{
			int i = 0, t = 0, j = 0, k = 0;

			for (i = 0; i < lpUnPacker->GetDatasetCount(); ++i)
			{
				// 设置当前结果集
				lpUnPacker->SetCurrentDatasetByIndex(i);

				// 打印字段
				for (t = 0; t < lpUnPacker->GetColCount(); ++t)
				{
					printf("%20s", lpUnPacker->GetColName(t));
				}

				putchar('\n');

				// 打印所有记录
				for (j = 0; j < (int)lpUnPacker->GetRowCount(); ++j)
				{
					// 打印每条记录
					for (k = 0; k < lpUnPacker->GetColCount(); ++k)
					{
						switch (lpUnPacker->GetColType(k))
						{
						case 'I':
							printf("%20d", lpUnPacker->GetIntByIndex(k));
							break;

						case 'C':
							printf("%20c", lpUnPacker->GetCharByIndex(k));
							break;

						case 'S':
							printf("%20s", lpUnPacker->GetStrByIndex(k));
							break;

						case 'F':
							printf("%20f", lpUnPacker->GetDoubleByIndex(k));
							break;

						case 'R':
							{
								int nLength = 0;
								void *lpData = lpUnPacker->GetRawByIndex(k, &nLength);

								// 对2进制数据进行处理
								break;
							}

						default:
							// 未知数据类型
							printf("未知数据类型。\n");
							break;
						}
					}

					putchar('\n');

					lpUnPacker->Next();
				}

				putchar('\n');
			}
		}

		tradeadapter::EBuySell UHST2Helper::HST2ToBuySell( const std::string& str )
		{
			/*
			'1'	股票买入
			'2'	股票卖出
			'3'	债券买入
			'4'	债券卖出
			'5'	融资回购
			'6'	融券回购
			'p'	开基申购
			'q'	开基赎回
			'V'	买入开仓
			'W'	卖出平仓
			'X'	卖出开仓
			'Y'	买入平仓
			'U'	提交质押
			'T'	转回质押
			*/
			if(str == "1")
				return EBuySell::eBuy;
			else if(str == "2")
				return EBuySell::eSell;
			else if(str == "V" || str == "Y")			
				return EBuySell::eBuy;
			else if(str == "W" || str == "X")
				return EBuySell::eSell;
			
			throw TException(_T("未定义的买卖类型"));
		}

		std::string UHST2Helper::GetSeatNo( const tradeadapter::HST2IniParamA& input, const TCHAR* code )
		{
			std::pair<EMarketDef,EProductType> info = UProductDefHelper::ParseCode(code);
			if(info.first == EMarketDef::ESH)
			{
				return input.shseat_no;
			}
			else if(info.first == EMarketDef::ESZ)
			{
				return input.szseat_no;
			}
			else if(info.first == EMarketDef::EZJ)
			{
				return input.zjseat_no;
			}
			else
			{
				throw TException(_T("错误的产品代码"));
			}
		}

		

	}

};