#ifndef __HST2SESSION_H__
#define __HST2SESSION_H__


#include "tdataset.h"
#include "tdatasets.h"

USING_LIBSPACE

namespace tradeadapter
{
	namespace hst2
	{
		class CHST2TradeAdapter;

		class CHST2Session
		{
		public:
			CHST2Session(CHST2TradeAdapter* pinstance);
			virtual ~CHST2Session(void);

			void BeginJob();

			void AddField(const char* key, const char* val);
			void AddField(const char* key, const std::string& val);


			bool CommitJob(FunctionID jobid, TDataSets* pResultS);

			bool GetDataResult(IF2UnPacker* lpUnpack, TDataSets* pResult);



		private:
			std::vector<std::pair<std::string,std::string> > m_datas;			
			CHST2TradeAdapter* m_instance;
		

		};
	};
};

#endif // !__HST2SESSION_H__