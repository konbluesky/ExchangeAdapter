#include "StdAfx.h"
#include "ExchangeAdapterDef.h"
#include "uconsole.h"
#include "ustringutils.h"
#include "CallBackImp.h"
#include "HST2TradeAdapter.h"
#include <iostream>
USING_LIBSPACE
namespace tradeadapter
{
	namespace hst2
	{
		CCallBackImp::CCallBackImp()
		{			
		}

		CCallBackImp::~CCallBackImp(void)
		{
		}

		void FUNCTION_CALL_MODE CCallBackImp::OnConnect( CConnectionInterface *lpConnection )
		{
			std::cout<<"OnConnect"<<std::endl;
		}

		void FUNCTION_CALL_MODE CCallBackImp::OnSafeConnect( CConnectionInterface *lpConnection )
		{
			std::cout<<"OnSafeConnect"<<std::endl;
		}

		void FUNCTION_CALL_MODE CCallBackImp::OnRegister( CConnectionInterface *lpConnection )
		{
			std::cout<<"OnRegister"<<std::endl;
		}



		void FUNCTION_CALL_MODE CCallBackImp::OnClose( CConnectionInterface *lpConnection )
		{
			UConsole::Print(UConsole::TEXT_RED,"���ӶϿ�");
			
			if( m_pAdapter != NULL)
			{
			}

		}

		void FUNCTION_CALL_MODE CCallBackImp::OnSent( CConnectionInterface *lpConnection, int hSend, void *reserved1, void *reserved2, int nQueuingData )
		{

		}

		void FUNCTION_CALL_MODE CCallBackImp::Reserved1( void *a, void *b, void *c, void *d )
		{

		}

		void FUNCTION_CALL_MODE CCallBackImp::Reserved2( void *a, void *b, void *c, void *d )
		{

		}

		int FUNCTION_CALL_MODE CCallBackImp::Reserved3()
		{
			return 0;
		}

		void FUNCTION_CALL_MODE CCallBackImp::Reserved4()
		{

		}

		void FUNCTION_CALL_MODE CCallBackImp::Reserved5()
		{

		}

		void FUNCTION_CALL_MODE CCallBackImp::Reserved6()
		{

		}

		void FUNCTION_CALL_MODE CCallBackImp::Reserved7()
		{

		}

		void FUNCTION_CALL_MODE CCallBackImp::OnReceivedBiz( CConnectionInterface *lpConnection, int hSend, const void *lpUnPackerOrStr, int nResult )
		{

		}

		void FUNCTION_CALL_MODE CCallBackImp::OnReceivedBizEx( CConnectionInterface *lpConnection, int hSend, LPRET_DATA lpRetData, const void *lpUnpackerOrStr, int nResult )
		{

		}

		unsigned long FUNCTION_CALL_MODE CCallBackImp::QueryInterface( const char *iid, IKnown **ppv )
		{
			return 0;	
		}

		unsigned long FUNCTION_CALL_MODE CCallBackImp::AddRef()
		{
			return 0;
		}

		unsigned long FUNCTION_CALL_MODE CCallBackImp::Release()
		{
			return 0;

		}

		void CCallBackImp::SetAdapter( void* pAdapter )
		{
			m_pAdapter = pAdapter;
		}

	}
}