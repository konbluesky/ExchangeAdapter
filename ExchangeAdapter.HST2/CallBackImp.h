#pragma once
#include "t2sdk_interface.h"
namespace tradeadapter
{

	namespace hst2
	{


		class CCallBackImp :public CCallbackInterface
		{
		public:
			CCallBackImp();
			~CCallBackImp(void);

			void SetAdapter(void* pAdapter);

			virtual void FUNCTION_CALL_MODE OnConnect( CConnectionInterface *lpConnection );

			virtual void FUNCTION_CALL_MODE OnSafeConnect( CConnectionInterface *lpConnection );

			virtual void FUNCTION_CALL_MODE OnRegister( CConnectionInterface *lpConnection );

			virtual void FUNCTION_CALL_MODE OnClose( CConnectionInterface *lpConnection );

			virtual void FUNCTION_CALL_MODE OnSent( CConnectionInterface *lpConnection, int hSend, void *reserved1, void *reserved2, int nQueuingData );

			virtual void FUNCTION_CALL_MODE Reserved1( void *a, void *b, void *c, void *d );

			virtual void FUNCTION_CALL_MODE Reserved2( void *a, void *b, void *c, void *d );

			virtual int FUNCTION_CALL_MODE Reserved3();

			virtual void FUNCTION_CALL_MODE Reserved4();

			virtual void FUNCTION_CALL_MODE Reserved5();

			virtual void FUNCTION_CALL_MODE Reserved6();

			virtual void FUNCTION_CALL_MODE Reserved7();

			virtual void FUNCTION_CALL_MODE OnReceivedBiz( CConnectionInterface *lpConnection, int hSend, const void *lpUnPackerOrStr, int nResult );

			virtual void FUNCTION_CALL_MODE OnReceivedBizEx( CConnectionInterface *lpConnection, int hSend, LPRET_DATA lpRetData, const void *lpUnpackerOrStr, int nResult );

			virtual unsigned long FUNCTION_CALL_MODE QueryInterface( const char *iid, IKnown **ppv );

			virtual unsigned long FUNCTION_CALL_MODE AddRef();

			virtual unsigned long FUNCTION_CALL_MODE Release();

		private:
			void* m_pAdapter;

		};
	}
}