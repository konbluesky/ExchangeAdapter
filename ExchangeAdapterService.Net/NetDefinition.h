#pragma once


using namespace System;
namespace ExchangeServiceNet
{
	 enum class EErrSystem
	{	
		Unknow,
		ConfigFile,
		RiskLimit,
		MQNet,
		Net,
		TradeAdapter,
		QuoteAdapter,
		TDFQUOTE,
	};

	 enum class EErrLevel
	 {
		 Normal,//default is 
		 Warning,
		 Error,
		 //....
	 };

	public ref class  ErrorInfo
	{
	public :
		int Code;
		EErrSystem System;
		System::String^ Msg;
		int Date;
		int Time;//时间格式：如果HHMMSSmm，如果是六位，则认为毫秒没有，否则认为存在毫秒。
		EErrLevel Level;

	};


	enum class TradeUnit
	{
		/// <summary>
		/// 手
		/// </summary>
		Hand,
		/// <summary>
		/// 股
		/// </summary>
		Section,
		/// <summary>
		/// 张
		/// </summary>
		Piece
	};

	enum  class PositionDateType
	{
		UseHistory,
		NoUseHistory
	};


	/// <summary>
	/// 持仓方向
	/// </summary>
	enum class PosSide
	{
		/// <summary>
		/// 多
		/// </summary>
		More,
		/// <summary>
		/// 空
		/// </summary>
		Short,
		/// <summary>
		/// 清
		/// </summary>
		Clear
	};
	

typedef int OrderID; // 订单号

public ref struct PositionInfo
{
public:


	/// <summary>
	/// 交易类别 
	/// 没有用
	/// </summary>
	///tstring exchange_type; 
	/// <summary>
	/// 证券账号 
	/// </summary>
	System::String^ stock_account;
	/// <summary>
	/// 证券/期货代码 
	/// </summary>
	System::String^ code;
	/// <summary>
	/// 证券名称 
	/// </summary>
	System::String^ stock_name;
	/// <summary>
	/// 当前数量 
	/// </summary>
	double current_amount;
	/// <summary>
	/// 可卖股票 
	/// </summary>
	double enable_amount;
	/// <summary>
	/// 最新价 
	/// </summary>
	double total_amount;
	/// <summary>
	/// 总数量（期货专用） 
	/// </summary>
	double total_diaableamount;
	/// <summary>
	/// 已平数量（期货专用） 
	/// </summary>
	double last_price;
	/// <summary>
	/// 成本价 
	/// </summary>
	double cost_price;
	/// <summary>
	/// 买卖盈亏 
	/// </summary>
	double income_balance;
	/// <summary>
	/// 股手标志，'0'--股   '1' -- 手。 
	/// </summary>
	TradeUnit hand_flag;
	/// <summary>
	/// 证券市值，在明细下为每个股东帐号的市值，而在汇总下则为fund_account的，汇总下无stock_account。 
	/// </summary>
	double market_value;
	PositionDateType datetype;
	PosSide posside;
public:
	
};
//In Different systems,the definitions are defferent. so don't define values.
//no zero,which should cause business orror easily.
enum class BuySell
{
	None = 0,
	Buy =1,
	Sell =2,

};

/// <summary>
/// 价格模式
/// </summary>
enum class PriceMode
{
	/// <summary>
	/// 限价
	/// </summary>
	LimitPrice,
	/// <summary>
	/// 市价委托--最新价(默认)
	/// </summary>
	LastPrice,
	/// <summary>
	/// 市价委托
	/// </summary>
	MarketPrice,
	/// <summary>
	/// 市价委托--本方最优价
	/// </summary>
	MarketPriceThisSide,
	/// <summary>
	/// 市价委托--对手方最优价
	/// </summary>
	MarketPriceThatSide,
	/// <summary>
	/// 市价委托--即时成交剩余撤销
	/// </summary>
	MarketPriceCancelRest,
	/// <summary>
	/// 市价委托--最优五档即时成交剩余转限价
	/// </summary>
	MarketPriceLimitedRest
};

/// <summary>
/// 开平标志
/// </summary>
enum class OpenClose
{
	/// <summary>
	/// 开仓
	/// </summary>
	Open,
	/// <summary>
	/// 平仓
	/// </summary>
	Close,
	/// <summary>
	/// 平今
	/// </summary>
	CloseToday,
	/// <summary>
	/// 平昨
	/// </summary>
	CloseYestoday,
	/// <summary>
	/// 强平
	/// </summary>
	ForceClose
};

enum class HedgeFlag
{
	/// <summary>
	/// 投机
	/// </summary>
	Speculation =1,
	/// <summary>
	/// 套保
	/// </summary>
	Hedge = 3
};



public ref struct Order
{
	System::String^ clientID;		//From which client(this is not only for account.
	System::String^ ContractCode;	//注股票里是stock_code，期货里是期货合约id
	BuySell OpeDir;			//买卖方向
	double	Price;			//note: we only put fixed price order.
	int		Amount;
	int		Day;			//20131010 = 2013.10.10
	int		Time;			//121120 = 12:11:20	

	PriceMode pricemode;	//comment: default value is fixed price, and havn't used in stock systems.
	OpenClose openclose;	//only used on ctp/future,
	HedgeFlag hedge;		//ctp only
	OrderID	orderid;
};
};
