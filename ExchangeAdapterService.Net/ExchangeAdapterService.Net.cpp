// This is the main DLL file.

#include "stdafx.h"
#include "ErrorDefinition.h"
#include "IOrderService.h"

#include "ExchangeAdapterService.Net.h"

namespace ExchangeServiceNet
{



	bool NetOrderService::Initialize(System::String^ fname)
	{
		pInstance = tradeadapter::IOrderService::CreateInstance(NULL);

		bool ret = pInstance->InitializeByXML(_T(""));
		return ret;
	}

	bool NetOrderService::IsInitialized()
	{
		bool ret = pInstance->IsInitialized();
		return ret;
	}

	OrderID NetOrderService::EntrustOrder(System::String^ clientID, Order^ order)
	{
		tradeadapter::Order input;
		input.Amount = order->Amount;
		input.Price = order->Price;
#pragma chMSG(TODO)
		tradeadapter::OrderID orderid;
		bool ret = this->pInstance->EntrustOrder(_T(""), &input,&orderid);
		return orderid;

	}

	bool NetOrderService::CancelOrder(System::String^ clinetID, OrderID id)
	{
		throw gcnew System::NotImplementedException();
	}

	int NetOrderService::QueryOrder(System::String^ clientID, OrderID id)
	{
		throw gcnew System::NotImplementedException();
	}

	bool NetOrderService::QueryPostions(List<PositionInfo^>^ pPoss, System::String^ pstock /*= NULL */)
	{
		std::list<tradeadapter::PositionInfo> info;
		bool ret = pInstance->QueryPostions(&info);

		return ret;
	}

	bool NetOrderService::QueryPostions(List<PositionInfo^>^ pPoss)
	{
		Boolean ret = QueryPostions(pPoss, System::String::Empty);
		return ret;
	}

	bool NetOrderService::GetLastError(ErrorInfo^ perr)
	{
		::ErrorInfo erros;
		bool ret = pInstance->GetLastError(&erros);
		perr->Code = erros.Code;
		//perr->Msg;// = gcnew System::String ^ (erros.Msg.c_str());
		return ret;
	}

	void NetOrderService::UnInitialize()
	{
		if (pInstance != NULL)
		{
			pInstance->UnInitialize();
			delete pInstance;
			pInstance = NULL;
		}
	}

}