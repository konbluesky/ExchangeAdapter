#pragma once
//#using  System;
using namespace System;
namespace ExchangeAdapter.Net
{
	ref enum  EErrSystem
	{	
		Unknow,
		ConfigFile,
		RiskLimit,
		MQNet,
		Net,
		TradeAdapter,
		QuoteAdapter,
		TDFQUOTE,
	};



	ref struct  ErrorInfo
	{	

		int Code;
		EErrSystem System;
		System::String^ Msg;
		int Date;
		int Time;//时间格式：如果HHMMSSmm，如果是六位，则认为毫秒没有，否则认为存在毫秒。
		EErrLevel Level;

	};

	enum  EErrLevel
	{
		Normal,//default is 
		Warning,
		Error,
		//....
	};



ref struct PositionInfo
{
public:


	/// <summary>
	/// 交易类别 
	/// 没有用
	/// </summary>
	///tstring exchange_type; 
	/// <summary>
	/// 证券账号 
	/// </summary>
	System::String^ stock_account;
	/// <summary>
	/// 证券/期货代码 
	/// </summary>
	System::String^ code;
	/// <summary>
	/// 证券名称 
	/// </summary>
	System::String^ stock_name;
	/// <summary>
	/// 当前数量 
	/// </summary>
	double current_amount;
	/// <summary>
	/// 可卖股票 
	/// </summary>
	double enable_amount;
	/// <summary>
	/// 最新价 
	/// </summary>
	double total_amount;
	/// <summary>
	/// 总数量（期货专用） 
	/// </summary>
	double total_diaableamount;
	/// <summary>
	/// 已平数量（期货专用） 
	/// </summary>
	double last_price;
	/// <summary>
	/// 成本价 
	/// </summary>
	double cost_price;
	/// <summary>
	/// 买卖盈亏 
	/// </summary>
	double income_balance;
	/// <summary>
	/// 股手标志，'0'--股   '1' -- 手。 
	/// </summary>
	TradeUnit hand_flag;
	/// <summary>
	/// 证券市值，在明细下为每个股东帐号的市值，而在汇总下则为fund_account的，汇总下无stock_account。 
	/// </summary>
	double market_value;
	PositionDateType datetype;
	PosSide posside;
public:
	
};
}