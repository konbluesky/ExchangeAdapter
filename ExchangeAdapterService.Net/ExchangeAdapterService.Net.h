// ExchangeAdapter.Net.h

#pragma once

using namespace System;
using namespace System::Collections::Generic;
#include "NetDefinition.h"

namespace tradeadapter
{
	class IOrderService;
};

namespace ExchangeServiceNet 
{
	
	public ref class NetOrderService //: public IOrderService
	{
		// TODO: Add your methods for this class here.
	public:
		bool QueryPostions(  List<PositionInfo^>^ pPoss);

		bool QueryPostions(List<PositionInfo^>^ pPoss, System::String^ pstock);

		//virtual bool QueryPostions( std::list<PositionInfo>* pPoss,const TCHAR* pstock = NULL );

		bool Initialize(System::String^ fname);

		virtual bool IsInitialized();

		virtual OrderID EntrustOrder( System::String^ clientID,Order^ order );

		virtual bool CancelOrder( System::String^ clinetID, OrderID id );

		virtual int QueryOrder( System::String^ clientID, OrderID id );

		virtual bool GetLastError( ErrorInfo^ perr );

		virtual void UnInitialize();

	private:

		tradeadapter::IOrderService* pInstance;

	};
}
/*


class COrderServiceImp : public IOrderService
	{
	public:
		COrderServiceImp(void);
		virtual ~COrderServiceImp(void);


		virtual bool Initialize(void* p = NULL);
		virtual void UnInitialize();


		virtual ITradeAdapter* GetAdapter();
		//************************************
		// Method:    EntrustOrder
		// FullName:  COrderServie::EntrustOrder
		// Access:    public 
		// Returns:   如果返回-1，则系统错误，所有业务停止，调用GetLastError
		// Qualifier:
		// Parameter: const TCHAR * clientID
		// Parameter: Order
		//************************************
		virtual OrderID EntrustOrder(const TCHAR* clientID,Order order);
		//************************************
		// Method:    CancelOrder
		// FullName:  COrderServie::CancelOrder
		// Access:    public 
		// Returns:   撤单是否成功，在自动交易的情况下，应该都是true。
		// Qualifier:
		// Parameter: const TCHAR * clinetID
		// Parameter: int OrderID
		//************************************
		virtual  bool CancelOrder(const TCHAR* clinetID, OrderID id);


		//************************************
		// Method:    QueryOrder
		// FullName:  COrderServie::QueryOrder
		// Access:    public 
		// Returns:   返回成交量，注：这个值如果order是无效单，则为0，如果order部分成，则不一定被100整除，
		// Qualifier:
		// Parameter: const TCHAR * clientID
		// Parameter: OrderID id
		//************************************
		virtual int QueryOrder(const TCHAR* clientID, OrderID id);



		std::vector<int> QueryOrders(const TCHAR* clientID);

		

		virtual bool IsInitialized() ;

		virtual bool QueryPostions( std::list<PositionInfo>* pPoss,const TCHAR* pstock = NULL );

		//virtual bool InitializeByXML( const TCHAR* fpane );


		bool InitializeTradelogDB();

		virtual bool GetLastError( ErrorInfo* perr );


		std::string m_id;

	private:
		ITradeAdapter* m_adapter;
		threadlock::multi_threaded_local m_lock;	

		std::vector<OrderDetail> m_orderdetails;
		
	};
	*/