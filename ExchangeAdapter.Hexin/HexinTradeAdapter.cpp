#include "StdAfx.h"
#include "ustringutils.h"
#include "HexinTradeAdapter.h"
namespace tradeadapter
{

	namespace hexin
	{


		CHexinTradeAdapter::CHexinTradeAdapter(void)
		{
		}

		CHexinTradeAdapter::~CHexinTradeAdapter(void)
		{
		}

		bool CHexinTradeAdapter::Initialize( void* param /*= NULL */ )
		{
			
			return false;
		}

		bool CHexinTradeAdapter::IsInitialized()
		{
			throw std::logic_error("The method or operation is not implemented.");
		}

		bool CHexinTradeAdapter::InitializeByXML( const TCHAR* fpath )
		{
			throw std::logic_error("The method or operation is not implemented.");
		}

		int CHexinTradeAdapter::EntrustOrder( Order order )
		{
			throw std::logic_error("The method or operation is not implemented.");
		}

		bool CHexinTradeAdapter::CancelOrder( OrderID orderid )
		{
			throw std::logic_error("The method or operation is not implemented.");
		}

		int CHexinTradeAdapter::QueryOrder( OrderID id )
		{
			throw std::logic_error("The method or operation is not implemented.");
		}

		bool CHexinTradeAdapter::QueryDealedOrders( std::list<DealedOrder>* pres )
		{
			throw std::logic_error("The method or operation is not implemented.");
		}

		void CHexinTradeAdapter::UnInitialize()
		{
			throw std::logic_error("The method or operation is not implemented.");
		}

		bool CHexinTradeAdapter::QueryAccountInfo( BasicAccountInfo* info )
		{
			throw std::logic_error("The method or operation is not implemented.");
		}

		bool CHexinTradeAdapter::QueryPostions( std::list<PositionInfo>* pPoss,const TCHAR* pstock /*= NULL */ )
		{
			throw std::logic_error("The method or operation is not implemented.");
		}

		bool CHexinTradeAdapter::GetLastError( ErrorInfo* pError )
		{
			throw std::logic_error("The method or operation is not implemented.");
		}

		bool CHexinTradeAdapter::QueryEntrustInfos( std::list<EntrustInfo>* pinfo )
		{
			throw std::logic_error("The method or operation is not implemented.");
		}
	};
};