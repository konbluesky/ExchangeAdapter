#ifndef __ZZ_TRADERADAPTER_H__
#define __ZZ_TRADERADAPTER_H__

#include "IFixTrader.h"
#include "ChinaOrderDefinition.h"
#include "ZZCommonAPI.h"

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4251)
#pragma warning(disable:4275)
#else
#error "不应该出现,以待其他"
#endif

namespace exchangeadapter_zhengzhou
{
	struct DataConvInfo;

	class EXAPI CZZTraderAdapter : public IFixTraderProvider
	{
	public:
		CZZTraderAdapter(void);
		virtual ~CZZTraderAdapter(void);

	public:
		struct InitParam
		{
			tstring sFlowPath;
			tstring sAddressIp;
			uint32 uiPort;
			tstring sParticipantID;
			tstring sUserID;
			tstring sPassword;
			tstring sLogPath;
			bool bEncrypt;
			bool bCompress;
			uint32 uiMonitorPort;
			tstring sLocalIP;
			uint32 uiMarketId;
			uint32 uiHeartBeatInterval;
			uint32 uiHeartBeatTimeout;
			uint32 uiConnWait;
			tstring sProVersion;
			tstring sInitPasswd;
			tstring sAuthSerNo;
			tstring sAuthCode;
			uint32 uiloginWait;
			uint32 uiSecquenceNo;

			EXAPI static std::pair<bool,InitParam> LoadConfig(
				const tstring& fname);
		};
		virtual bool Initialize(void* param);
		virtual bool InitializeByXML(const tchar* configfile);
		virtual void UnInitialize();
		virtual bool Login(void*);
		virtual bool Logout(void*);
		virtual tstring GetName();

		virtual void RequestFix(FIX::Message &msg);

		/* 内部使用 */
	public:
		void OnAdapterRsp(int32 pid,CZZPackage &package);
		void OnAdmRsp(int32 pid,CZZPackage &package,FlowType flowtype);
		/* 在函数内部实现自动重连 */
		int32 OnNetErrDialog();
		int32 OnNetErrPrivate();

	private:
		void ReqeustOrderAdd(const Order&  order);
		void RequestOrderCancel(const OrderCancelRequest& order);
		void RequestOrderModify(const OrderCancelReplaceRequest& order);
		
		bool InitPara(InitParam &param);
		bool InitAdapter();
		void OrderAddProcess(int32 pid,CZZPackage &package);
		void OrderCancelProcess(int32 pid,CZZPackage &package);
		void OrderFillProcess(int32 pid,CZZPackage &package);

	private:
		ApiAttr m_stApiAttr;
		ConnAttr m_stConnAttr;
		LoginAttr m_stLoginAttr;
		boost::shared_ptr<CZZPackage> m_package;
		boost::shared_ptr<DataConvInfo> m_dataconv;
		int32 m_maxDlgSeq;
		int32 m_maxPriSeq;
	};
}

#ifdef _MSC_VER
#pragma warning(pop)
#else
#error "仅支持VC,以待修改"
#endif

#endif