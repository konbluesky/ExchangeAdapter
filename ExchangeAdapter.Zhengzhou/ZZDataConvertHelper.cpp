
#include "ZZDataConvertHelper.h"
#include "debugutils.h"
#include "StringUtils.h"
#include "PID.h"
#include "FieldID.h"
#include "LocalCompile.h"

namespace exchangeadapter_zhengzhou
{
	static inline int GetInt(char x)
	{
		return x-'0';
	}

	/* 需要考虑如何加快消息的转换速度,这个过程中,就是如何利用该API */
	bool CDataConvert::TranslateQuoteToPublic(CZZPackage &package,Quote* pquote)
	{
		Date_Time tmptime;
		uint32 ihour,imin,isec;
		DBG_ASSERT(PID_MarketMatchDataRsp == package.GetPID());
		package.GetField(FID_InstrumentId,pquote->ID);
		package.GetField(FID_LastPrice,pquote->Last_Price);
		package.GetField(FID_LastLot,pquote->Last_Volume);
		package.GetField(FID_BidPrice,pquote->BidPrice);
		package.GetField(FID_BidLot,pquote->BidQty);
		package.GetField(FID_AskPrice,pquote->AskPrice);
		package.GetField(FID_AskLot,pquote->AskQty);
		package.GetField(FID_LowPrice,pquote->LowPrice);
		package.GetField(FID_HighPrice,pquote->HighPrice);
		package.GetField(FID_TotalVolume,pquote->Total_Volume);
		package.GetField(FID_PreSettle,pquote->SettlementPrice);

		API_Now(&tmptime);
		package.GetField(FID_UpdateTime,tmptime);
		pquote->RecieveTime.SetHMS((int)tmptime.hour,(int)tmptime.minute,
			(int)tmptime.second,tmptime.microsec);
		pquote->RecieveTime.SetYMD(tmptime.year,tmptime.month,tmptime.day);

		return true;
	}

	int CDataConvert::TranslatePublicToOrder(const Order &order,
		CZZPackage &package,const DataConvInfo *data)
	{
		short int iOrderType = 0;
		package.SetPID(PID_OrderInsertReq);

		package.SetField(FID_OrderLocalId,order.zzIDLocalOrder,
			strlen(order.zzIDLocalOrder));
		package.SetField(FID_Direction,order.Side);
		package.SetField(FID_VolumeTotalOrginal,order.Quantity);
		iOrderType = OrderTypePublicToPrivate(order.OrderType);
		package.SetField(FID_OrderType,iOrderType);
		package.SetField(FID_MatchCondition,order.zzpri.TimeCondition);
		package.SetField(FID_LimitPrice,order.LimitPrice);
		switch(order.OrderType)
		{
		case Market:
			package.SetField(FID_StopPrice,0.0);
			break;
		case Limit:
		case Stop:
			package.SetField(FID_StopPrice,order.StopPrice);
			break;		
		}
		package.SetField(FID_OffsetFlag,order.OpenClose);
		package.SetField(FID_InstrumentId,order.SecurityDesc);
		package.SetField(FID_ParticipantId,*(data->pparticipantid));
		package.SetField(FID_UserId,*(data->puserid));
#if defined(RMMS_TEST_CLIENT)
		package.SetField(FID_ClientId,RMMS_ZZ_CLIENT);
#else
		package.SetField(FID_ClientId,order.IDClient);
#endif
		package.SetField(FID_OrderSysId,"");
		package.SetField(FID_HedgeFlag,1);
		package.SetField(FID_AutoSuspend,1);
		package.SetField(FID_InsertTime,"");
		package.SetField(FID_MinimalVolume,0);

		return 0;
	}

	int CDataConvert::TranslatePublicToCancelOrder(
		const OrderCancelRequest & cancelOrder,CZZPackage &package,
		const DataConvInfo *data)
	{
		short int iOrderType = 0;
		package.SetPID(PID_OrderActionReq);

		package.SetField(FID_OrderActionCode,0);
#if defined(RMMS_TEST_CLIENT)
		package.SetField(FID_ClientId,RMMS_ZZ_CLIENT);
#else
		package.SetField(FID_ClientId,order.IDClient);
#endif
		package.SetField(FID_OrderSysId,cancelOrder.IDSysOrder);
		package.SetField(FID_ActionLocalId,cancelOrder.zzIDLocalOrder,
			strlen(cancelOrder.zzIDLocalOrder));
		package.SetField(FID_InstrumentId,cancelOrder.IDInstrument);
		package.SetField(FID_InstrumentVersion,0);
		package.SetField(FID_UserId,*(data->puserid));
		package.SetField(FID_OrderType,0);

		return 0;
	}

	int CDataConvert::TranslateOrderExcutionToPublic(
		CZZPackage &package,ExecutionReport * preport)
	{
		switch(package.GetPID()){
		case PID_OrderInsertRsp:
			return OrderInsertToPublic(package,preport);
			break;
		case PID_OrderActionRsp:
			return OrderActionToPublic(package,preport);
			break;
		}		
		return 0;
	}

	void CDataConvert::TranslateOrderInsertErrorToPublic(CZZPackage &package,
		BusinessReject *reject)
	{
		tchar acErrCode[10] = {0};
#if defined(_DEBUG)
		tchar acErrText[100] = {0};
#endif
		package.GetField(FID_ErrorCode,acErrCode,10);
#if defined(_DEBUG)
		package.GetField(FID_ErrorText,acErrText,100);
		DBG_TRACE(("错误:%s:%s",acErrCode,acErrText));
#endif
		reject->ErrorCode = acErrCode;
		reject->ErrorSource = "ZZ";
		package.GetField(FID_OrderLocalId,reject->zzIDLocalOrder,
			sizeof(reject->zzIDLocalOrder));
		package.GetField(FID_SequenceNo,reject->LastMsgSeqNumProcessed);
		reject->RefMsgType = "D";
		reject->BusinessRejectReason = BRR_Other;
	}

	int CDataConvert::OrderInsertToPublic(
		CZZPackage &package,ExecutionReport * preport)
	{
		int8 tempValue;

		package.GetField(FID_OrderSysId,preport->IDSysOrder);
		package.GetField(FID_OrderLocalId,preport->zzIDLocalOrder,
			sizeof(preport->zzIDLocalOrder));
		package.GetField(FID_InstrumentId,preport->SecurityDesc);
		package.GetField(FID_Direction,tempValue);
		preport->Side = (TradeSide)tempValue;
		package.GetField(FID_LimitPrice,preport->PriceLimit);
		package.GetField(FID_StopPrice,preport->PriceStop);
		preport->OrderStatus = New;
		preport->ExecType = ExecType_New;
		package.GetField(FID_VolumeTotalOrginal,preport->QtyOrder);
		return 0;
	}

	int CDataConvert::TradeInsertSingleToPublic(CZZPackage &package,
		FillReport *preport)
	{
		int8 tempValue;

#if defined(RMMS_TEST_CLIENT)
		preport->IDClient = RMMS_ZZ_GLOTRADER;
#else
		package.GetField(FID_ClientId,preport->IDClient);
#endif
		package.GetField(FID_OrderSysId,preport->IDSysOrder);
		package.GetField(FID_OrderLocalId,preport->zzIDLocalOrder,
			sizeof(preport->zzIDLocalOrder));
		package.GetField(FID_InstrumentId,preport->SecurityDesc);
		package.GetField(FID_TradeId,preport->IDExec);
		package.GetField(FID_Direction,tempValue);
		preport->Side = (TradeSide)tempValue;
		package.GetField(FID_Price,preport->LastPx);
		package.GetField(FID_Volume,preport->QtyLastShares);
		Date_Time datetime;
		package.GetField(FID_MatchDate,datetime);
		preport->TradeTime.SetYMD(datetime.year,datetime.month,datetime.day);
		preport->TradeTime.SetHMS(datetime.hour,datetime.minute,
			datetime.second,datetime.microsec);
		return 0;
	}

	int CDataConvert::OrderActionToPublic(CZZPackage &package,
		ExecutionReport * preport)
	{
		int8 tempValue;
		// Client ID 需要由数据库恢复
		package.GetField(FID_OrderSysId,preport->IDSysOrder);
		package.GetField(FID_ActionLocalId,preport->zzIDLocalOrder,
			sizeof(preport->zzIDLocalOrder));
		package.GetField(FID_InstrumentId,preport->SecurityDesc);
		// Side需要由数据库恢复
		// 原始申报总数量
		preport->PriceLimit = 0;
		preport->PriceStop = 0;
		preport->OrderStatus = Canceled;
		preport->ExecType = ExecType_Canceled;
		return 0;
	}

	void CDataConvert::TranslateOrderActionErrorToPublic(
		CZZPackage &package,OrderCancelReject *reject)
	{
		tchar acErrCode[10] = {0};
#if defined(_DEBUG)
		tchar acErrText[100] = {0};
#endif
		package.GetField(FID_ErrorCode,acErrCode,10);
#if defined(_DEBUG)
		package.GetField(FID_ErrorText,acErrText,100);
		DBG_TRACE(("错误:%s:%s",acErrCode,acErrText));
#endif
		reject->ErrorCode = acErrCode;
		reject->ErrorSource = "ZCE";
		package.GetField(FID_OrderSysId,reject->IDSysOrder);
		package.GetField(FID_OrderLocalId,reject->zzIDLocalOrder,
			sizeof(reject->zzIDLocalOrder));
	}

	int CDataConvert::OrderTypePublicToPrivate(OrderTypeFlag type)
	{
		int iPriType = -1;
		switch(type){
		case Market:
			iPriType = 1;break;
		case Limit:
			iPriType = 0;break;
		case Stop:
			iPriType = 3;break;
		default:
			break;
		}
		return iPriType;
	}

	tstring CDataConvert::IDConvert( const char* ori )
	{
		tstring SID = ori;
		char buff[64];
		sprintf(buff,"%s.F.1%s00",
			LIB_NS::UStrConvUtils::ToUpper(SID.substr(0,SID.size()-3)).c_str(),
			SID.substr(SID.size()-3).c_str());
		return buff;
	}
}