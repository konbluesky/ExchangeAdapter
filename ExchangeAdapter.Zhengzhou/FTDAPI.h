#ifndef FTDAPI_H_
#define FTDAPI_H_

/*FTDAPI.h
 *global functions declared here
 */
#include "Config.h"

#if defined (ACE_HAS_TIUSER_H_BROKEN_EXTERN_C)
extern "C" {
#endif

#ifndef NULL
#define NULL 0
#endif

#pragma pack(push)
#pragma pack(1)

	typedef struct tagDateTime
	{
		API_UINT16 year;
		API_BYTE  month,day,hour,minute,second;
		API_UINT16 microsec;
	}Date_Time;

#pragma pack(pop)

typedef int API_BOOL;
#define API_TRUE	1
#define API_FALSE	0

/*线程模型*/
typedef int THREAD_MODE;
#define SINGLE_THREAD_MODE 0 
#define MULTI_THREAD_MODE 1

/*建立连接时是否阻塞*/
#define CONNECT_WAIT 1
#define CONNECT_NO_WAIT 0

/*交易所*/
typedef int MARKET_ID;
#define MARKET_ZCE 1
#define MARKET_DCE 2
#define MARKET_SFE 3

/*初始化*/
FTDDLL_API API_BOOL FTDAPI_CALL ftd_api_init(THREAD_MODE mode,const char *LogFilePath,int MonitorPort);
/*停止Api*/
FTDDLL_API void FTDAPI_CALL ftd_api_stop();
/*单线程事件处理驱动，*/
FTDDLL_API int FTDAPI_CALL ftd_api_event_loop(double wait=0.001);

/*创建数据包对象*/
FTDDLL_API MsgPackageHandle FTDAPI_CALL API_AllocPackage();
/*回收数据包对象*/
FTDDLL_API void FTDAPI_CALL API_FreePackage(MsgPackageHandle pkg);
/*报文数据操作*/
/*报文ID*/
FTDDLL_API int FTDAPI_CALL API_GetPID(MsgPackageHandle pkg);
FTDDLL_API void FTDAPI_CALL API_SetPID(MsgPackageHandle pkg,int pid);
//域类型
#define	FTNULL			0 
#define	FTCHAR			1
#define	FTLONG			2
#define	FTSTRING		3
#define	FTDOUBLE		4
#define	FTDATETIME		5
//取得某字段的当前数据类型
FTDDLL_API int FTDAPI_CALL API_GetFieldType(MsgPackageHandle pkg,int FieldID);
//字段操作
//字符
FTDDLL_API char FTDAPI_CALL API_GFChar(MsgPackageHandle pkg,int fid,char def=' ');
FTDDLL_API void FTDAPI_CALL API_SFChar(MsgPackageHandle pkg,int fid,char val);
//整数
FTDDLL_API int FTDAPI_CALL API_GFInt(MsgPackageHandle pkg,int fid,int def=0);
FTDDLL_API void FTDAPI_CALL API_SFInt(MsgPackageHandle pkg,int fid,int val);
//双精度小数
FTDDLL_API double FTDAPI_CALL API_GFDouble(MsgPackageHandle pkg,int fid,double def=0);
FTDDLL_API void FTDAPI_CALL API_SFDouble(MsgPackageHandle pkg,int fid,double val,int precision=4);
//串
FTDDLL_API int  FTDAPI_CALL API_GFString(MsgPackageHandle pkg,int fid, char* buf,int bufsize);
FTDDLL_API void FTDAPI_CALL API_SFString(MsgPackageHandle pkg,int fid,const char* buf,int bufsize);
//日期时间
FTDDLL_API API_BOOL FTDAPI_CALL API_GFDateTime(MsgPackageHandle pkg,int fid,Date_Time*val);
FTDDLL_API void FTDAPI_CALL API_SFDateTime(MsgPackageHandle pkg,int fid,  Date_Time*val);

// 判断某个字段是否为空
FTDDLL_API API_BOOL FTDAPI_CALL API_FieldIsNull(MsgPackageHandle pkg,int FieldID);
// 清除某个特定的字段的值
FTDDLL_API void FTDAPI_CALL API_ClearField(MsgPackageHandle pkg,int FieldID);
// 清除数据报里的所有字段
FTDDLL_API void FTDAPI_CALL API_ClearAll(MsgPackageHandle pkg);
// 把source的数据全部复制到本数据包
FTDDLL_API void FTDAPI_CALL API_Copy(MsgPackageHandle pkg,MsgPackageHandle source);

//数据遍历
FTDDLL_API int FTDAPI_CALL API_FirstField(MsgPackageHandle pkg);
FTDDLL_API int FTDAPI_CALL API_NextField(MsgPackageHandle pkg);
FTDDLL_API int FTDAPI_CALL API_CFType(MsgPackageHandle pkg);
FTDDLL_API API_BOOL FTDAPI_CALL API_CFDT(MsgPackageHandle pkg,Date_Time *dt);
FTDDLL_API char FTDAPI_CALL API_CFChar(MsgPackageHandle pkg,char def=' ');
FTDDLL_API int FTDAPI_CALL API_CFInt(MsgPackageHandle pkg,int def=0);
FTDDLL_API double FTDAPI_CALL API_CFDouble(MsgPackageHandle pkg,int *prec=0,double def=0);
FTDDLL_API int  FTDAPI_CALL API_CFString(MsgPackageHandle pkg,char* buf,int bufsize);

FTDDLL_API void FTDAPI_CALL API_Now(Date_Time*dt);


/*创建交易所连接对象*/
FTDDLL_API ExchgConnectionHandle FTDAPI_CALL API_CreateExchgConnection(API_BOOL Encrypt,API_BOOL Commpress,MARKET_ID Market
											,int heart_beat_interval,int heart_beat_timeout);
/*发起与交易所连接*/
FTDDLL_API API_BOOL FTDAPI_CALL API_Connect(ExchgConnectionHandle conn,const char* HostIpAddr,int Port,double Wait);
/*是否已经连接*/
FTDDLL_API API_BOOL API_Connected(ExchgConnectionHandle conn);

/*登录到交易所,需要在reqPkg填写认证、流标志、流编号等信息*/
FTDDLL_API API_BOOL FTDAPI_CALL API_Login(ExchgConnectionHandle conn,MsgPackageHandle reqPkg,double Wait);
/*退出登录,需要在reqPkg填写认证、流标志、流编号等信息*/
FTDDLL_API API_BOOL FTDAPI_CALL API_Logout(ExchgConnectionHandle conn,MsgPackageHandle reqPkg,double Wait);
/*发送一个数据包*/
FTDDLL_API API_BOOL FTDAPI_CALL API_Send(ExchgConnectionHandle conn,MsgPackageHandle pkg);
/*接收一个数据包
 *wait:等待秒数，wait=0:没有数据立即返回
 */
FTDDLL_API MsgPackageHandle FTDAPI_CALL API_Recv(ExchgConnectionHandle conn,double wait=0);
/*断开与交易所的连接*/
FTDDLL_API void FTDAPI_CALL API_DisConnect(ExchgConnectionHandle);
/*关闭并释放交易所连接对象*/
FTDDLL_API void FTDAPI_CALL API_FreeExchgConnection(ExchgConnectionHandle);

//数据流状态常数
#define DFS_CLOSED 0//连接断开
#define DFS_OPENED 1//刚刚建立连接
#define DFS_NEGOTIATEKEY 2//刚刚协商密钥，还没有登录
#define DFS_LOGIN_OK     3//已经登录成功
/*取得数据流状态*/
FTDDLL_API int FTDAPI_CALL API_GetDataFlowStatus(ExchgConnectionHandle,int DataFlowFlag);

/*包含数据的回调函数,客户程序的返回值决定API对此数据包的后续处理方式
 *返回0:API将此数据包放入接收数据包队列，客户可以调用Recv再次取得此包
 *返回非0:API将此数据包销毁
 */
typedef int (FTDAPI_CALL*ExchgPackageCallBack)(void * CallBackArg,ExchgConnectionHandle,MsgPackageHandle);
/*连接状态的回调函数*/
typedef void (FTDAPI_CALL*ExchgConnectionCallBack)(void * CallBackArg,ExchgConnectionHandle,int error_code,const char* error_text);
/*设置回调函数*/
	/*尝试连接通知，error_code=0连接成功，非0连接失败*/
FTDDLL_API void FTDAPI_CALL API_SetOpenCallBack(ExchgConnectionHandle,ExchgConnectionCallBack,void * CallBackArg);
	/*连接断开通知，error_code=0本机主动断开，非0：远方主机断开*/
FTDDLL_API void FTDAPI_CALL API_SetCloseCallBack(ExchgConnectionHandle,ExchgConnectionCallBack,void * CallBackArg);
	/*出错通知*/
FTDDLL_API void FTDAPI_CALL API_SetErrorCallBack(ExchgConnectionHandle,ExchgConnectionCallBack,void * CallBackArg);

	/*数据通知*/
FTDDLL_API void FTDAPI_CALL API_SetRecvCallBack(ExchgConnectionHandle,ExchgPackageCallBack,void * CallBackArg);
	/*登录通知，登陆是否成功，请检查其ErrorCode域*/
FTDDLL_API void FTDAPI_CALL API_SetLoginCallBack(ExchgConnectionHandle,ExchgPackageCallBack,void * CallBackArg);
	/*登录通知，是否成功，请检查其ErrorCode域*/
FTDDLL_API void FTDAPI_CALL API_SetLogoutCallBack(ExchgConnectionHandle,ExchgPackageCallBack,void * CallBackArg);

#if defined(ACE_HAS_TIUSER_H_BROKEN_EXTERN_C)
}
#endif

#endif

