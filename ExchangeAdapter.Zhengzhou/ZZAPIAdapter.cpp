#include "ZZAPIAdapter.h"
#include "ZZCommonAPI.h"
#include "debugutils.h"
#include "FieldID.h"
#include "LocalCompile.h"

#if defined(ZZ_TEMP_TEST)
extern void APIpkgTofile (MsgPackageHandle ,int ,int);
#endif

namespace exchangeadapter_zhengzhou
{
	bool CZZAPIAdapter::InitAPI(const ApiAttr api)
	{
		if(API_TRUE != ftd_api_init(api.modeThread,api.strLogPath.c_str(),
			api.dwMonitorPort)) {
			DBG_TRACE(("初始化ZCEAPI失败"));
			return false;
		}
		return true;
	}

	/* 各个连接独立 */
	ExchgConnectionHandle CZZAPIAdapter::Connect(
		const ConnAttr conn,void *pTrader,FlowType enFlowType)
	{
		ExchgConnectionHandle hConn = NULL;
		hConn = API_CreateExchgConnection(
			conn.bEncrypt,conn.bCommpress,conn.idMarket,
			conn.dwHeartBeatInterval,conn.dwHeartBeatTimeout);
		if (NULL == hConn){
			DBG_TRACE(("创建交易所连接失败!"));
			return hConn;
		}

		switch(enFlowType){
		case DIALOG:
			/* 目前设想不接受对话流,因为私有流数据比对话流更详细,而对话流仅仅是
			 * 重复数据.但是在下单时,必须使用对话流,所以采取此种方法
			 */
			/*
			API_SetOpenCallBack(hConn,OnOpen,pTrader);
			API_SetErrorCallBack(hConn,OnError,pTrader);
			API_SetCloseCallBack(hConn,OnClose,pTrader);
			API_SetLoginCallBack(hConn,OnLogin,pTrader);
			API_SetLogoutCallBack(hConn,OnLogout,pTrader);
			API_SetRecvCallBack(hConn,OnRecv,pTrader);
			*/
			break;
		case PRIVATE:
			API_SetOpenCallBack(hConn,OnOpenPri,pTrader);
			API_SetErrorCallBack(hConn,OnErrorPri,pTrader);
			API_SetCloseCallBack(hConn,OnClosePri,pTrader);
			API_SetLoginCallBack(hConn,OnLoginPri,pTrader);
			API_SetLogoutCallBack(hConn,OnLogoutPri,pTrader);
			API_SetRecvCallBack(hConn,OnRecvPri,pTrader);
			break;
		case BROADCAST:
			API_SetOpenCallBack(hConn,OnOpenBrd,pTrader);
			API_SetErrorCallBack(hConn,OnErrorBrd,pTrader);
			API_SetCloseCallBack(hConn,OnCloseBrd,pTrader);
			API_SetLoginCallBack(hConn,OnLoginBrd,pTrader);
			API_SetLogoutCallBack(hConn,OnLogoutBrd,pTrader);
			API_SetRecvCallBack(hConn,OnRecvBrd,pTrader);
			break;
		}		

		if(API_TRUE != API_Connect(hConn,conn.strIPAddress.c_str(),
			conn.dwPort,conn.fdWait)){
			API_FreeExchgConnection(hConn);
			return NULL;
		}
		return hConn;
	}

	bool CZZAPIAdapter::Login(
		const ExchgConnectionHandle hConn,
		const LoginAttr login,FlowType enFlowType)
	{
		CZZPackage package;
		API_BOOL	bRet;
		
		package.SetPID(0x00016);
		package.SetField(FID_DataFlowFlag ,enFlowType);
		package.SetField(FID_ParticipantId ,login.strMemberID);
		package.SetField(FID_UserId ,login.strTraderID);
		package.SetField(FID_Password ,login.strPasswd);
		package.SetField(FID_ProtocolVersion,login.strProtocolVersion);
		package.SetField(FID_IpAddr ,login.strLoginIP);
		package.SetField(FID_SequenceNo ,login.dwSeNo);
		package.SetField(FID_AUTH_SERIAL_NO,login.strAuthSerialNo);
		package.SetField(FID_AUTH_CODE,login.strAuthCode);
		package.SetField(FID_INIT_PASSWORD,login.strInitPassword);
		bRet = API_Login (hConn,&package,login.fdWait);
		if (API_TRUE != bRet){
			DBG_TRACE(("登陆失败"));
			return false;
		}

		return true;
	}

	bool CZZAPIAdapter::Logout(
		const ExchgConnectionHandle hConn,
		const LogoutAttr logout,
		FlowType enFlowType)
	{
		CZZPackage package;
		API_BOOL	bRet;

		package.SetPID(0x00017);
		package.SetField(FID_DataFlowFlag ,enFlowType);
		package.SetField(FID_ParticipantId ,logout.strMemberID);
		package.SetField(FID_UserId ,logout.strTraderID);
		bRet = API_Logout (hConn,&package,10);
		if (API_TRUE != bRet){
			DBG_TRACE(("退出登陆失败"));
			return false;
		}
		
		return true;
	}

	void CZZAPIAdapter::Disconnect(ExchgConnectionHandle &hConn)
	{
		API_DisConnect(hConn);
		API_FreeExchgConnection(hConn);
	}

	void CZZAPIAdapter::UninitAPI()
	{
		ftd_api_stop();		
	}
	
	/* 对话流数据处理,先保留,实际上没有使用 */
	void CZZAPIAdapter::OnOpenDlg(void * CallBackArg,ExchgConnectionHandle conn,
		int error_code,const char* error_text)
	{
		CZZCommonAPI *pTrader = (CZZCommonAPI*)CallBackArg;
		pTrader->OnErrorRecv(error_code,error_text,DIALOG);
	}

	void CZZAPIAdapter::OnErrorDlg(void * CallBackArg,
		ExchgConnectionHandle conn,int error_code,const char* error_text)
	{
		CZZCommonAPI *pTrader = (CZZCommonAPI*)CallBackArg;
		pTrader->OnErrorRecv(error_code,error_text,DIALOG);
	}

	void CZZAPIAdapter::OnCloseDlg(void * CallBackArg,
		ExchgConnectionHandle conn,int error_code,const char* error_text)
	{
		CZZCommonAPI *pTrader = (CZZCommonAPI*)CallBackArg;
		pTrader->OnErrorRecv(error_code,error_text,DIALOG);
	}

	int CZZAPIAdapter::OnLoginDlg(void * CallBackArg,
		ExchgConnectionHandle conn,MsgPackageHandle pkg)
	{
		CZZCommonAPI *pTrader = (CZZCommonAPI*)CallBackArg;
		return pTrader->OnLoginRecv(pkg,DIALOG);
	}

	int CZZAPIAdapter::OnRecvDlg(void * CallBackArg,
		ExchgConnectionHandle conn,MsgPackageHandle pkg)
	{
		CZZCommonAPI *pTrader = (CZZCommonAPI*)CallBackArg;
#if defined(ZZ_TEMP_TEST)
		APIpkgTofile(pkg,0,0);
#endif
		return pTrader->OnAppRecv(pkg,DIALOG);
	}

	int CZZAPIAdapter::OnLogoutDlg(void * CallBackArg,
		ExchgConnectionHandle conn,MsgPackageHandle pkg)
	{
		CZZCommonAPI *pTrader = (CZZCommonAPI*)CallBackArg;
		return pTrader->OnLogoutRecv(pkg,DIALOG);
	}

	/* 私有流数据 */
	void CZZAPIAdapter::OnOpenPri(void * CallBackArg,
		ExchgConnectionHandle conn,int error_code,const char* error_text)
	{
		CZZCommonAPI *pTrader = (CZZCommonAPI*)CallBackArg;
		pTrader->OnOpenRecv(error_code,error_text,PRIVATE);
	}

	void CZZAPIAdapter::OnErrorPri(void * CallBackArg,
		ExchgConnectionHandle conn,int error_code,const char* error_text)
	{
		CZZCommonAPI *pTrader = (CZZCommonAPI*)CallBackArg;
		pTrader->OnErrorRecv(error_code,error_text,PRIVATE);
	}

	void CZZAPIAdapter::OnClosePri(void * CallBackArg,
		ExchgConnectionHandle conn,int error_code,const char* error_text)
	{
		CZZCommonAPI *pTrader = (CZZCommonAPI*)CallBackArg;
		pTrader->OnCloseRecv(error_code,error_text,PRIVATE);
	}

	int CZZAPIAdapter::OnLoginPri(void * CallBackArg,
		ExchgConnectionHandle conn,MsgPackageHandle pkg)
	{
		CZZCommonAPI *pTrader = (CZZCommonAPI*)CallBackArg;
		return pTrader->OnLoginRecv(pkg,PRIVATE);
	}

	int CZZAPIAdapter::OnRecvPri(void * CallBackArg,
		ExchgConnectionHandle conn,MsgPackageHandle pkg)
	{
		CZZCommonAPI *pTrader = (CZZCommonAPI*)CallBackArg;
#if defined(ZZ_TEMP_TEST)
		APIpkgTofile(pkg,0,0);
#endif
		return pTrader->OnAppRecv(pkg,PRIVATE);
	}

	int CZZAPIAdapter::OnLogoutPri(void * CallBackArg,
		ExchgConnectionHandle conn,MsgPackageHandle pkg)
	{
		CZZCommonAPI *pTrader = (CZZCommonAPI*)CallBackArg;
		return pTrader->OnLogoutRecv(pkg,PRIVATE);
	}

	/* 公共流数据 */
	void CZZAPIAdapter::OnOpenBrd(void * CallBackArg,
		ExchgConnectionHandle conn,int error_code,const char* error_text)
	{
		CZZCommonAPI *pTrader = (CZZCommonAPI*)CallBackArg;
		pTrader->OnOpenRecv(error_code,error_text,BROADCAST);
	}

	void CZZAPIAdapter::OnErrorBrd(void * CallBackArg,
		ExchgConnectionHandle conn,int error_code,const char* error_text)
	{
		CZZCommonAPI *pTrader = (CZZCommonAPI*)CallBackArg;
		pTrader->OnErrorRecv(error_code,error_text,BROADCAST);
	}

	void CZZAPIAdapter::OnCloseBrd(void * CallBackArg,
		ExchgConnectionHandle conn,int error_code,const char* error_text)
	{
		CZZCommonAPI *pTrader = (CZZCommonAPI*)CallBackArg;
		pTrader->OnCloseRecv(error_code,error_text,BROADCAST);
	}

	int CZZAPIAdapter::OnLoginBrd(void * CallBackArg,
		ExchgConnectionHandle conn,MsgPackageHandle pkg)
	{
		CZZCommonAPI *pTrader = (CZZCommonAPI*)CallBackArg;
		return pTrader->OnLoginRecv(pkg,BROADCAST);
	}

	int CZZAPIAdapter::OnRecvBrd(void * CallBackArg,
		ExchgConnectionHandle conn,MsgPackageHandle pkg)
	{
		CZZCommonAPI *pTrader = (CZZCommonAPI*)CallBackArg;
#if defined(ZZ_TEMP_TEST)
		APIpkgTofile(pkg,0,0);
#endif
		return pTrader->OnAppRecv(pkg,BROADCAST);
	}

	int CZZAPIAdapter::OnLogoutBrd(void * CallBackArg,
		ExchgConnectionHandle conn,MsgPackageHandle pkg)
	{
		CZZCommonAPI *pTrader = (CZZCommonAPI*)CallBackArg;
		return pTrader->OnLogoutRecv(pkg,BROADCAST);
	}
}