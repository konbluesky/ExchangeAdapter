#ifndef __ZZ_MARKET_DATA_PROVIDER_H__
#define __ZZ_MARKET_DATA_PROVIDER_H__

#include "ExchangeAdapterBase.h"
#include "IFixMarket.h"
#include "ChinaQuoteDefinition.h"
#include "ZZCommonAPI.h"

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4275)
#else
#error "不应该出现,以待其他"
#endif

namespace exchangeadapter_zhengzhou
{
	class EXAPI CZZMarketDataProvider : public IFixMarketProvider
	{
	public:
		CZZMarketDataProvider();
		virtual tstring GetName();
		virtual ~CZZMarketDataProvider(void);

		/* 暂时先放在外面,作为单独的一个使用.这样避免重复
		 * 后面再考虑是否需要Market和Trade各使用一套
		 */
		struct InitParam
		{
			tstring sFlowPath;
			tstring sAddressIp;
			uint32 uiPort;
			tstring sParticipantID;
			tstring sUserID;
			tstring sPassword;
			tstring sLogPath;
			bool bEncrypt;
			bool bCompress;
			uint32 uiMonitorPort;
			tstring sLocalIP;
			uint32 uiMarketId;
			uint32 uiHeartBeatInterval;
			uint32 uiHeartBeatTimeout;
			uint32 uiConnWait;
			tstring sProVersion;
			tstring sInitPasswd;
			tstring sAuthSerNo;
			tstring sAuthCode;
			uint32 uiloginWait;
			uint32 uiSecquenceNo;

			EXAPI static std::pair<bool,InitParam> LoadConfig(
				const tstring& fname);
		};

		virtual bool Initialize(void* param);
		virtual bool InitializeByXML(const tchar* configfile);
		virtual bool Login(void*);
		virtual bool Logout(void*);
		virtual bool SupportDoM();
		virtual void UnInitialize();
		
		void OnAdapterQuoteRecieve(CZZPackage &package);
		void OnAdapterDoMRecieve(CZZPackage &package){package;}
		void OnAdmRsp(int32 pid,CZZPackage &package,FlowType flowtype);
		/* 在函数内部实现自动重连 */
		int32 OnNetErr();

	protected:
		bool InitPara(InitParam &param);
		bool InitAdapter();

	private:
		ApiAttr m_stApiAttr;
		ConnAttr m_stConnAttr;
		LoginAttr m_stLoginAttr;
	};

}

#ifdef _MSC_VER
#pragma warning(pop)
#else
#error "仅支持VC,以待修改"
#endif

#endif