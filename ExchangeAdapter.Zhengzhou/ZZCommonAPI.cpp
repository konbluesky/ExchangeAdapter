#include "debugutils.h"
#include "PID.h"
#include "FieldID.h"
#include "ZZTraderAdapter.h"
#include "ZZMarketDataProvider.h"
#include "LocalCompile.h"

namespace exchangeadapter_zhengzhou
{
	CZZCommonAPI::CZZCommonAPI(void)
	{
		m_udwClient = 0;
		m_package = boost::shared_ptr<CZZPackage>(new CZZPackage);
	}

	CZZCommonAPI::~CZZCommonAPI(void)
	{
		m_udwClient = 0;
	}

	bool CZZCommonAPI::Initialize(const ApiAttr api)
	{
		LIB_NS::Locker locker(m_csInitMutex);
		
		if(0 == m_udwClient){
			if(!CZZAPIAdapter::InitAPI(api))
				return false;
		}
		m_udwClient++;
		return true;
	}

	/* 暂时不检查是否已经退出登陆了 */
	void CZZCommonAPI::Uninitialize(void)
	{
		LIB_NS::Locker locker(m_csInitMutex);
		m_udwClient--;
		if(0 == m_udwClient)
			CZZAPIAdapter::UninitAPI();
	}

	bool CZZCommonAPI::Start(FlowType enFlowType)
	{
		LIB_NS::Locker locker(m_csStartMutex);
		if(m_astFlowAttr[enFlowType].bstart)
			return true;
		return CZZAPIAdapter::Login(m_astFlowAttr[enFlowType].hConn,
			m_astFlowAttr[enFlowType].stLogin,enFlowType);
	}

	bool CZZCommonAPI::Stop(FlowType enFlowType)
	{
		LIB_NS::Locker locker(m_csStartMutex);
		if (!m_astFlowAttr[enFlowType].bstart)
			return false;

		LogoutAttr logout;
		logout.strMemberID = 
			m_astFlowAttr[enFlowType].stLogin.strMemberID;
		logout.strTraderID = 
			m_astFlowAttr[enFlowType].stLogin.strTraderID;
		return CZZAPIAdapter::Logout(m_astFlowAttr[enFlowType].hConn,logout,
			enFlowType);
	}

	bool CZZCommonAPI::RegisterAction(
		RegAction enAction,FlowType enFlowType,
		void* pAdapter)
	{
		UNREF_VAR(pAdapter);
		DBG_ASSERT(NULL != pAdapter);
		ExchgConnectionHandle hConn = NULL;
		
		switch(enAction)
		{
		case REGISTER:
			{
				hConn = CZZAPIAdapter::Connect(
					m_astFlowAttr[enFlowType].stConn,this,enFlowType);
				if (NULL == hConn)
					return false;

				m_astFlowAttr[enFlowType].hConn = hConn;				
			}
			break;
		case UNREGISTER:
			{
				hConn = m_astFlowAttr[enFlowType].hConn;
				CZZAPIAdapter::Disconnect(hConn);
				m_astFlowAttr[enFlowType].hConn = NULL;
			}			
			break;
		default:
			DBG_ASSERT(NULL);
			return false;
			break;
		}

		return true;
	}

	bool CZZCommonAPI::Register(
		FlowType enFlowType,const ConnAttr &conn,const LoginAttr &login,
		ClientType enCliType,void* pAdapter)
	{
		DBG_ASSERT(0 != m_udwClient);
		LIB_NS::Locker locker(m_csRegisterMutex);

		if(m_astFlowAttr[enFlowType].bstart)
			return false;
		m_astFlowAttr[enFlowType].stConn = conn;
		m_astFlowAttr[enFlowType].stLogin = login;	
		
		switch (enCliType)
		{
		case MARKET_DATA:
			if(NULL != m_astFlowAttr[enFlowType].pQuotAdapter)
				return false;
			m_astFlowAttr[enFlowType].pQuotAdapter = 
				(CZZMarketDataProvider*)pAdapter;
			break;
		case TRADER:
			if (NULL != m_astFlowAttr[enFlowType].pTraderAdapter)
				return false;
			m_astFlowAttr[enFlowType].pTraderAdapter = 
				(CZZTraderAdapter*)pAdapter;
			break;
		default:
			return false;
			break;
		}

		return RegisterAction(REGISTER,enFlowType,pAdapter);
	}

	bool CZZCommonAPI::Unregister(FlowType enFlowType,ClientType enCliType,
		void *pAdapter)
	{
		DBG_ASSERT(0 != m_udwClient);
		LIB_NS::Locker locker(m_csRegisterMutex);

		if(m_astFlowAttr[enFlowType].bstart)
			return false;

		switch (enCliType)
		{
		case MARKET_DATA:
			if(m_astFlowAttr[enFlowType].pQuotAdapter != 
				(CZZMarketDataProvider*)pAdapter)
				return false;
			break;
		case TRADER:
			if(m_astFlowAttr[enFlowType].pTraderAdapter != 
				(CZZTraderAdapter*)pAdapter)
				return false;
			break;
		default:
			return false;
			break;
		}
		return RegisterAction(UNREGISTER,enFlowType,pAdapter);
	}
	
/**
 * 网络状态管理
 */
	void CZZCommonAPI::OnErrorRecv(int error_code,const char* error_text,
		FlowType flowtype)
	{
		switch(flowtype){
		case DIALOG:
			{
				CZZTraderAdapter *pTrader = (CZZTraderAdapter *)
					m_astFlowAttr[flowtype].pTraderAdapter;
				if(NULL != pTrader)
					pTrader->OnNetErrDialog();
			}
			break;
		case PRIVATE:
			{
				CZZTraderAdapter *pTrader = (CZZTraderAdapter *)
					m_astFlowAttr[flowtype].pTraderAdapter;
				if(NULL != pTrader)
					pTrader->OnNetErrPrivate();
			}
			break;
		case BROADCAST:
			{
				CZZMarketDataProvider * pQuote = (CZZMarketDataProvider *)
					m_astFlowAttr[flowtype].pQuotAdapter;
				if(NULL != pQuote)
					pQuote->OnNetErr();
			}
			break;
		}
		DBG_TRACE(("递归性错误显示%d:%s",error_code,error_text));
	}

/***
 * 业务消息管理
 */
	INT32 CZZCommonAPI::OnAppRecvBrod(int32 pid)
	{
		switch(pid)	{
		case PID_MarketMatchDataRsp:
			{
				/* 目前的设想是只有启动广播流,才会有该部分数据 */
				CZZMarketDataProvider * pQuote = (CZZMarketDataProvider*)
					m_astFlowAttr[BROADCAST].pQuotAdapter;
				pQuote->OnAdapterQuoteRecieve(*m_package);
			}
			break;
		default:
			break;
		}
		return 0;
	}

	INT32 CZZCommonAPI::OnAppRecvPriv(int32 pid)
	{
		switch(pid)
		{
		case PID_OrderInsertRsp:			
		case PID_OrderActionRsp:
		case PID_TradeInsertSingleRsp:
			{
				CZZTraderAdapter *pTrader = (CZZTraderAdapter *)
					m_astFlowAttr[PRIVATE].pTraderAdapter;
				pTrader->OnAdapterRsp(pid,*(m_package.get()));
			}
			break;
		default:
			break;
		}
		return 0;
	}
	INT32 CZZCommonAPI::OnAppRecv(MsgPackageHandle pkg,FlowType flowtype)
	{
		int pid;
		MsgPackageHandle old = m_package->TempRP(pkg);
		pid = m_package->GetPID();
		
		switch(flowtype){
		case PRIVATE:
			OnAppRecvPriv(pid);
			break;
		case BROADCAST:
			OnAppRecvBrod(pid);
			break;
		default:
			DBG_ASSERT(0);
		}
		m_package->RestorRP(old);
		return 0;
	}

/***
 * 管理消息
 */
	INT32 CZZCommonAPI::OnUserLoginRsp(FlowType flowtype) const
	{
		if(!m_package->GetFieldIsNULL(FID_ErrorCode)){
			DBG_TRACE(("登陆失败!"));
			return -1;
		}

		switch(flowtype){
		case PRIVATE:
			{
				CZZTraderAdapter *pTrader = (CZZTraderAdapter *)
					m_astFlowAttr[flowtype].pTraderAdapter;
				if(NULL != pTrader)
					pTrader->OnAdmRsp(PID_UserLoginRsp,*(m_package.get()),
					flowtype);
			}
			break;
		case BROADCAST:
			{
				CZZMarketDataProvider * pQuote = (CZZMarketDataProvider *)
					m_astFlowAttr[flowtype].pQuotAdapter;
				if(NULL != pQuote){
					pQuote->OnAdmRsp(PID_UserLoginRsp,*(m_package.get()),
						flowtype);
				}
			}	
			break;
		}
		return 0;
	}

	INT32 CZZCommonAPI::OnLoginRecv(MsgPackageHandle pkg,FlowType flowtype)
	{
		MsgPackageHandle old = m_package->TempRP(pkg);
		OnUserLoginRsp(flowtype);
		m_package->RestorRP(old);
		return 0;
	}

	INT32 CZZCommonAPI::OnLogoutRecv(MsgPackageHandle /*pkg*/,
		FlowType /*flowtype*/)
	{
		// 暂时不出该中报文
		return 0;
	}

	void CZZCommonAPI::OnOpenRecv(int error_code,const char* error_text,
		FlowType flowtype)
	{
		if(0 == error_code){
			switch(flowtype){
			case PRIVATE:
				{
					CZZTraderAdapter *pTrader = (CZZTraderAdapter *)
						m_astFlowAttr[flowtype].pTraderAdapter;
					if(NULL != pTrader)
						pTrader->PriOnNetConnected();
				}
				break;
			case BROADCAST:
				{
					CZZMarketDataProvider * pQuote = (CZZMarketDataProvider *)
						m_astFlowAttr[flowtype].pQuotAdapter;
					if(NULL != pQuote)
						pQuote->PriOnNetConnected();
				}	
				break;
			}
		} else {
			// 是否会存在递归
			DBG_TRACE(("错误%d:%s",error_code,error_text));
		}
	}

	void CZZCommonAPI::OnCloseRecv(int error_code,const char* error_text,
		FlowType flowtype)
	{
		if(0 == error_code){
			switch(flowtype){
			case PRIVATE:
				{
					CZZTraderAdapter *pTrader = (CZZTraderAdapter *)
						m_astFlowAttr[flowtype].pTraderAdapter;
					if(NULL != pTrader)
						pTrader->PriOnNetDisconnected();
				}
				break;
			case BROADCAST:
				{
					CZZMarketDataProvider * pQuote = (CZZMarketDataProvider *)
						m_astFlowAttr[flowtype].pQuotAdapter;
					if(NULL != pQuote)
						pQuote->PriOnNetDisconnected();
				}
				break;
			}
		} else {
			// 如果直接重连是否会存在递归
			DBG_TRACE(("错误%d:%s",error_code,error_text));
		}
	}

/***
 * 发送消息
 */
	bool CZZCommonAPI::SendMsg(CZZPackage &package)
	{
		return API_Send(m_astFlowAttr[DIALOG].hConn,&package)?true:false;
	}
}
