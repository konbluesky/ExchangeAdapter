#ifndef __ZZ_PACKAGE_H__
#define __ZZ_PACKAGE_H__

#include "ExchangeAdapterBase.h"
#include "FTDAPI.h"

class EXAPI CZZPackage
{
public:

	enum ZZPackageFieldType
	{
		EFTNULL = 0,
		EFTCHAR = 1,
		EFTLONG = 2,
		EFTSTRING = 3,
		EFTDOUBLE = 4,
		EFTDATETIME =5,
	};


	CZZPackage(void);
	~CZZPackage(void);
	
	//���������bug
	static CZZPackage& Attach(MsgPackageHandle Handler);
	MsgPackageHandle Detatch();
	

	MsgPackageHandle TempRP(MsgPackageHandle newhandler)
	{
		MsgPackageHandle temp = _handler;	
			_handler = newhandler;	
			return temp;
	}

	MsgPackageHandle RestorRP(MsgPackageHandle oldhanler)
	{
		MsgPackageHandle temp = _handler;	
			_handler = oldhanler;	
			return temp;
	}
public:
	void Dump();
	CZZPackage* Clone();

	
	const MsgPackageHandle& operator &() const {return _handler;}

public:
	//opeation
	int GetPID();
	void SetPID(int pid);


	CZZPackage::ZZPackageFieldType GetFieldType(int FieldID);
	bool GetFieldIsNULL(int FieldID);
	bool ClearFiled(int FieldID);
	bool ClearAllFields();
	
	bool GetField(int FieldID,char& value);
	bool SetField(int FieldID,char value);
	bool GetField(int FieldID,short int& value);
	bool SetField(int FieldID,short int value);
	bool GetField(int FieldID,int& value);
	bool SetField(int FieldID,int value);
	bool GetField(int FieldID,double& value);
	bool SetField(int FieldID,double value);
	bool GetField(int FieldID,char* pbuff,int buffsize);
	bool SetField(int FieldID,const char* pbuff,int buffsize);
	bool GetField(int FieldID,tstring &value);
	bool SetField(int FieldID,tstring value);
	bool SetField(int FieldID,Date_Time &dt);
	bool GetField(int FieldID,Date_Time &dt);

private:
	CZZPackage(MsgPackageHandle Handler);
	MsgPackageHandle  _handler;
	
	DESABLE_COPY_AND_ASSIGN(CZZPackage)
};

#endif