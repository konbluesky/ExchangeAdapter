#ifndef __MACROINSTANCEDEF_H__
#define __MACROINSTANCEDEF_H__

#include "zzTraderAdapter.h"
#include "ZZMarketDataProvider.h"

#define __MARKETCLASS exchangeadapter_zhengzhou::CZZMarketDataProvider

#define __TRADERCLASS exchangeadapter_zhengzhou::CZZTraderAdapter;

#endif