#ifndef CONFIG_H_
#define CONFIG_H_

#include <limits.h>
//以下定义根据编译器不同可能需要修改
#ifdef WIN32//windows平台定义 for msvc7-8

	//导出修饰符，
	#ifdef FTDDLL_EXPORTS
		#define FTDDLL_API __declspec(dllexport)
	#else
		#define FTDDLL_API __declspec(dllimport)
	#endif

	//调用约定
	#define FTDAPI_CALL __cdecl
#else//Linux平台定义
	#define FTDDLL_API 
	#define FTDAPI_CALL 
#endif 

#if (UINT_MAX!=0xFFFFFFFF)
//目前仅仅支持32位平台
    #error support 32bit platforms only!
#else
	typedef unsigned short int API_UINT16;
	typedef unsigned char API_BYTE;
#endif

class MsgPackage;
class ExchangeConnection;
/*数据包句柄*/
typedef MsgPackage* MsgPackageHandle;
/*交易所连接对象句柄*/
typedef ExchangeConnection* ExchgConnectionHandle;

//指针安全选项，定义此标记后，API使用者调用的指针将被检查有效性
//#define CHECK_POINTER

#endif

