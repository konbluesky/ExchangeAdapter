#ifndef __ZZ_COMMON_API_H__
#define __ZZ_COMMON_API_H__

#include "ExchangeAdapterBase.h"
#include "ZZPackage.h"
#include "ZZAPIAdapter.h"
#include "Mutex.h"
#include "autoptr.h"

/* 
 * 从目前测试的情况来看,郑州API不支持一个连接上建立多个流
 * 必须为每个流建立一个连接.同时服务器端不支持多个相同流的连接
 * 所以每个API上最多建立3个连接,支持3种类型流.目前没办法实验多个流的用户名
 * 和密码是否必须相同.因为只有一个用户名,暂时无法测试.为保留兼容这种情况,
 * 所有的注册必须都继续传递的连接和登录参数.
 *
 * 每个连接是一个线程.所以如果不同的流使用相同的回调函数时,
 * 必须注意多线程的情况.
 * 
 * 多个线程之间存在一个
 */

namespace exchangeadapter_zhengzhou
{
struct FlowAttr
{
	bool bstart;
	ExchgConnectionHandle hConn;
	LoginAttr stLogin;
	ConnAttr stConn;
	/* 因为客户类型有限,所以不采取动态链表管理 */
	void *pTraderAdapter;
	void *pQuotAdapter;
};

enum ClientType
{
	MARKET_DATA = 0,
	TRADER,
};

enum RegAction
{
	REGISTER = 0,
	UNREGISTER,
};

class CZZCommonAPI
{
public:
	CZZCommonAPI(void);
	virtual ~CZZCommonAPI(void);

public:
	bool Initialize(const ApiAttr api);
	void Uninitialize(void);

	bool Register(FlowType enFlowType,const ConnAttr &conn,
		const LoginAttr &login,ClientType enCliType,void* pAdapter);
	bool Unregister(FlowType enFlowType,ClientType enCliType,void *pAdapter);
	bool Start(FlowType enFlowType);
	bool Stop(FlowType enFlowType);

	bool SendMsg(CZZPackage &package);

	void OnErrorRecv(int error_code,const char* error_text,FlowType flowtype);
	int32 OnAppRecv(MsgPackageHandle pkg,FlowType flowtype);
	int32 OnAppRecvPriv(int32 pid);
	int32 OnAppRecvBrod(int32 pid);
	
	void OnOpenRecv(int error_code,const char* error_text,FlowType flowtype);
	void OnCloseRecv(int error_code,const char* error_text,FlowType flowtype);
	int OnLoginRecv(MsgPackageHandle pkg,FlowType flowtype);
	int OnLogoutRecv(MsgPackageHandle pkg,FlowType flowtype);

protected:
	bool RegisterAction(RegAction enAction,FlowType enFlowType,void* pAdapter);
	int32 OnUserLoginRsp(FlowType flowtype) const;

protected:
	FlowAttr m_astFlowAttr[MAX_FLOW];
	uint32 m_udwClient;
	LIB_NS::Mutex m_csInitMutex;
	LIB_NS::Mutex m_csRegisterMutex;
	LIB_NS::Mutex m_csStartMutex;
	boost::shared_ptr<CZZPackage> m_package;
};

}

#endif