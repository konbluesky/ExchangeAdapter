#include "ExchangeAdapterConfig.h"
#include "ZZPackage.h"
#define TIXML_USE_STL
#include "tinyXML.h"
#include "LocalCompile.h"

CZZPackage::CZZPackage( void )
{
	_handler = API_AllocPackage();
}

CZZPackage::CZZPackage( MsgPackageHandle Handler )
{
	_handler = Handler;
}


CZZPackage::~CZZPackage( void ) 
{
	if (_handler !=NULL)
	{
		API_FreePackage(_handler);
		_handler = NULL;
	}
}

void CZZPackage::Dump()
{
	int index = API_FirstField(_handler);
	while(index != -1)
	{
		int datatype = API_GetFieldType(_handler,index);
		switch (datatype)
		{
		case FTNULL:
			{
				std::cout<<"FTNULL"<<std::endl;
				break;
			}
		case FTCHAR:
			{
				char val;
				GetField(index,val);
				std::cout<<"FTCHAR:"<<val<<std::endl;
				break;
			}
		case FTLONG:
			{
				int val;
				GetField(index,val);
				std::cout<<"FTLONG"<<val<<std::endl;
				break;
			}
		case FTSTRING:
			{
				tstring val;
				GetField(index,val);
				std::cout<<"FTSTRING:"<<val.c_str()<<std::endl;
				break;
			}
		case FTDOUBLE:
			{
				double val;
				GetField(index,val);
				std::cout<<"FTDOUBLE"<<val<<std::endl;
				break;
			}
		case FTDATETIME:
			{
				Date_Time val;
				GetField(index,val);
				std::cout<<"FTDATETIME"<<
					"."<<(int)val.year<<
					"."<<(int)val.month<<
					"."<<(int)val.day<<
					" "<<(int)val.hour<<
					":"<<(int)val.minute<<
					":"<<(int)val.second<<
					"-"<<(int)val.microsec
					<<std::endl;
				break;
			}
			
		}
		index = API_NextField(_handler);

	}
}



void CZZPackage::SetPID( int pid )
{
	return API_SetPID(_handler,pid);
}

int CZZPackage::GetPID()
{
	return API_GetPID(_handler);
}

MsgPackageHandle CZZPackage::Detatch()
{
	MsgPackageHandle handler = _handler;
	_handler = NULL;
	return handler;
}

bool CZZPackage::SetField( int FieldID,char value )
{
	API_SFChar(_handler,FieldID,value);
	return true;
}

bool CZZPackage::SetField( int FieldID,int value )
{
	API_SFInt(_handler,FieldID,value);
	return true;
}

bool CZZPackage::SetField( int FieldID,short int value )
{
	API_SFInt(_handler,FieldID,value);
	return true;
}

bool CZZPackage::SetField( int FieldID,double value )
{
	API_SFDouble(_handler,FieldID,value);
	return true;
}
bool CZZPackage::GetField( int FieldID,int& value )
{
	value = API_GFInt(_handler,FieldID,-1);
	return value == -1 ? false : true;
}

bool CZZPackage::GetField( int FieldID,short int& value )
{
	value = API_GFInt(_handler,FieldID,-1);
	return value == -1 ? false : true;
}

bool CZZPackage::GetField( int FieldID,char& value )
{
	value = API_GFChar(_handler,FieldID,'`');
	return value == '`' ? false :true;
}

bool CZZPackage::GetField( int FieldID,double& value )
{
	value = API_GFDouble(_handler,FieldID,-1);
	return true;
}

bool CZZPackage::GetField( int FieldID,char* pbuff,int buffsize )
{
	int32 ret = 0;
	ret = API_GFString(_handler,FieldID,pbuff,buffsize);
	if(0 == ret)
		return false;
	pbuff[ret]='\0';
	return true;
}

bool CZZPackage::SetField( int FieldID,const char* pbuff,int buffsize )
{
	API_SFString(_handler,FieldID,pbuff,buffsize);
	return true;
}

bool CZZPackage::GetField(int FieldID,tstring &value)
{
#define TEMP_LEN 100
	char temp[TEMP_LEN];
	return GetField(FieldID,temp,TEMP_LEN)?	(value = temp,true):false;	
}

bool CZZPackage::SetField(int FieldID,tstring value)
{
	SetField(FieldID,value.c_str(),value.length());
	return true;
}

bool CZZPackage::SetField(int FieldID,Date_Time &dt)
{
	API_SFDateTime(_handler,FieldID,&dt);
	return true;
}

bool CZZPackage::GetField(int FieldID,Date_Time &dt)
{
	API_GFDateTime(_handler,FieldID,&dt);
	return true;
}

bool CZZPackage::ClearAllFields()
{
	API_ClearAll(_handler);
	return true;
}

bool CZZPackage::GetFieldIsNULL( int FieldID )
{
	if(API_FieldIsNull(_handler,FieldID) )
		return true;
	else
		return false;
}

CZZPackage* CZZPackage::Clone()
{
	MsgPackageHandle newhandler = API_AllocPackage();

	API_Copy(newhandler,_handler);
	return new CZZPackage(newhandler);
}

CZZPackage::ZZPackageFieldType CZZPackage::GetFieldType( int FieldID )
{
	return (ZZPackageFieldType)API_GetFieldType(_handler,FieldID);
}

bool CZZPackage::ClearFiled( int FieldID )
{
	API_ClearField(_handler,FieldID);
	return true;
}
