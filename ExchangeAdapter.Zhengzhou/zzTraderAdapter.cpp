#include "ExchangeAdapterConfig.h"
#include "zzTraderAdapter.h"
#include "ZZPackage.h"
#include "ZZDataConvertHelper.h"
#include "debugutils.h"
#include "directory.h"
#define TIXML_USE_STL
#include "tinyXML.h"
#include <cctype>
#include "PID.h"
#include "FieldID.h"
#include "FixDataConvertHelper.h"
#include "LocalCompile.h"

namespace exchangeadapter_zhengzhou
{
	CZZTraderAdapter::CZZTraderAdapter(void)
	{
		std::memset(&m_stApiAttr,0,sizeof(ApiAttr));
		std::memset(&m_stConnAttr,0,sizeof(ConnAttr));
		std::memset(&m_stLoginAttr,0,sizeof(LoginAttr));

		m_package = boost::shared_ptr<CZZPackage>(new CZZPackage);
		m_dataconv = boost::shared_ptr<DataConvInfo>(new DataConvInfo);

		m_maxDlgSeq = 0;
		m_maxPriSeq = 0;
	}

	CZZTraderAdapter::~CZZTraderAdapter(void)
	{

	}
	
	std::pair<bool,CZZTraderAdapter::InitParam> 
		CZZTraderAdapter::InitParam::LoadConfig(
		const tstring& fname)
	{
		InitParam param;
		TiXmlDocument doc(fname.c_str());
		bool loadOkay = doc.LoadFile();
		TiXmlNode* pNode = NULL;
		TiXmlNode* ele = NULL;
		tstring str;

		std::memset(&param,0,sizeof(param));
		if (!loadOkay)
			return std::make_pair(false,param);

		pNode = doc.FirstChild("configure");
		XML_GET_STRING(pNode,"saddressip",param.sAddressIp);
		XML_GET_INT(pNode,"uiport",param.uiPort);
		XML_GET_STRING(pNode,"sparticipantid",param.sParticipantID);
		XML_GET_STRING(pNode,"suserid",param.sUserID);
		XML_GET_STRING(pNode,"spassword",param.sPassword);
		XML_GET_STRING(pNode,"slogpath",param.sLogPath);
		XML_GET_STRING(pNode,"spassword",param.sPassword);
		XML_GET_BOOL(pNode,"bencrypt",param.bEncrypt);
		XML_GET_BOOL(pNode,"bcompress",param.bCompress);
		XML_GET_INT(pNode,"uimonitorport",param.uiMonitorPort);
		XML_GET_STRING(pNode,"slocalip",param.sLocalIP);
		XML_GET_INT(pNode,"uimarketid",param.uiMarketId);
		XML_GET_INT(pNode,"uiheartbeatinterval",param.uiHeartBeatInterval);
		XML_GET_INT(pNode,"uiheartbeattimeout",param.uiHeartBeatTimeout);
		XML_GET_INT(pNode,"uiconnwait",param.uiConnWait);
		XML_GET_STRING(pNode,"sprotocolversion",param.sProVersion);
		XML_GET_STRING(pNode,"sinitpass",param.sInitPasswd);
		XML_GET_STRING(pNode,"sauthserialno",param.sAuthSerNo);
		XML_GET_STRING(pNode,"sauthcode",param.sAuthCode);
		XML_GET_INT(pNode,"uiloginwait",param.uiloginWait);
		XML_GET_INT(pNode,"uisequenceno",param.uiSecquenceNo);

		return std::make_pair(true,param);
	}

	bool CZZTraderAdapter::InitializeByXML( const tchar* configfile )
	{
		std::pair<bool,InitParam> param = InitParam::LoadConfig(configfile);
		if (param.first)
			return Initialize(&param.second);
		else
			return false;
	}

	bool CZZTraderAdapter::Initialize(void* param)
	{
		if(!InitPara(*(InitParam*)param) || !InitAdapter())
			return false;
		return true;
	}
	
	void CZZTraderAdapter::UnInitialize()
	{
		CZZCommonAPI *pAdapter = 
			&(simpattern::Singleton_InClass<CZZCommonAPI>::Instance());

		pAdapter->Unregister(DIALOG,TRADER,this);
		pAdapter->Unregister(PRIVATE,TRADER,this);
		pAdapter->Uninitialize();

		/* 将所有的必要数据放入xml中 */
	}

	bool CZZTraderAdapter::Login(void*)
	{
		CZZCommonAPI *pAdapter = 
			&(simpattern::Singleton_InClass<CZZCommonAPI>::Instance());
		if(!pAdapter->Start(DIALOG) || !pAdapter->Start(PRIVATE))
			return false;
		return true;
	}
	
	bool CZZTraderAdapter::Logout(void*)
	{
		CZZCommonAPI *pAdapter = 
			&(simpattern::Singleton_InClass<CZZCommonAPI>::Instance());
		pAdapter->Stop(DIALOG);
		pAdapter->Stop(PRIVATE);
		return true;
	}

	bool CZZTraderAdapter::InitPara(InitParam &param)
	{
		m_stApiAttr.modeThread = MULTI_THREAD_MODE;
		m_stApiAttr.strLogPath = param.sLogPath;
		m_stApiAttr.dwMonitorPort = param.uiPort;

		m_stConnAttr.bEncrypt = param.bEncrypt;
		m_stConnAttr.bCommpress = param.bCompress;
		m_stConnAttr.idMarket = param.uiMarketId;
		m_stConnAttr.dwHeartBeatInterval = param.uiHeartBeatInterval;
		m_stConnAttr.dwHeartBeatTimeout = param.uiHeartBeatTimeout;
		m_stConnAttr.strIPAddress = param.sAddressIp;
		m_stConnAttr.dwPort = param.uiPort;
		m_stConnAttr.fdWait = param.uiConnWait;

		m_stLoginAttr.strMemberID = param.sParticipantID;
		m_stLoginAttr.strTraderID = param.sUserID;
		m_stLoginAttr.strPasswd = param.sPassword;
		m_stLoginAttr.strLoginIP = param.sLocalIP;
		m_stLoginAttr.strProtocolVersion = param.sProVersion;
		m_stLoginAttr.strInitPassword = param.sInitPasswd;
		m_stLoginAttr.strAuthSerialNo = param.sAuthSerNo;
		m_stLoginAttr.strAuthCode = param.sAuthCode;
		m_stLoginAttr.fdWait = param.uiloginWait;
		m_stLoginAttr.dwSeNo = param.uiSecquenceNo;

		m_dataconv->pparticipantid = &(m_stLoginAttr.strMemberID);
		m_dataconv->puserid = &(m_stLoginAttr.strTraderID);
		return true;
	}

	bool CZZTraderAdapter::InitAdapter()
	{
		LIB_NS::UDirectory::CreateDirectory(m_stApiAttr.strLogPath.c_str());

		CZZCommonAPI *pAdapter = 
			&(simpattern::Singleton_InClass<CZZCommonAPI>::Instance());
		if(!pAdapter->Initialize(m_stApiAttr))
			return false;
		if(pAdapter->Register(DIALOG,m_stConnAttr,m_stLoginAttr,TRADER,this) &&
			pAdapter->Register(PRIVATE,m_stConnAttr,m_stLoginAttr,TRADER,this))
			return true;
		return false;
	}

	void CZZTraderAdapter::RequestFix(FIX::Message &msg)
	{
		FIX::MsgType type;
		const FIX::Header &header = msg.getHeader();
		header.getField(type);
		tstring strtype = type.getValue();
		int32 errorcode = 0;

		if(FIX::MsgType_NewOrderSingle == strtype){
			Order order;
			if(0 > FixDataConvertHelper::FixToInternal(RMMS_NS::RMMS_CN_ZZ,msg,&order))
				return;
			ReqeustOrderAdd(order);
		} else if(FIX::MsgType_OrderCancelRequest == strtype) {
			OrderCancelRequest cancel;
			if(0 > FixDataConvertHelper::FixToInternal(RMMS_NS::RMMS_CN_ZZ,msg,&cancel))
				return;
			RequestOrderCancel(cancel);
		}
	}

	void CZZTraderAdapter::OnAdmRsp(int32 pid,CZZPackage &package,
		FlowType flowtype)
	{
		/* 可能出错 */
		switch(pid){
		case PID_UserLoginRsp:
			int32 dwSeSeType = 0;
			if(!package.GetFieldIsNULL(FID_MaxOrderLocalId))
				package.GetField(FID_MaxOrderLocalId,dwSeSeType);
			
			if(0 == flowtype)
				m_maxDlgSeq = dwSeSeType;
			else
				m_maxPriSeq = dwSeSeType;
			break;
		}
		PriOnLogin();
	}

	void CZZTraderAdapter::OrderAddProcess(int32 pid,CZZPackage &package)
	{
		if(package.GetFieldIsNULL(FID_ErrorCode)){
			ExecutionReport exeReport;
			FIX::Message msg;
			if(0 > CDataConvert::TranslateOrderExcutionToPublic(package,
				&exeReport))
				return;
			//OnOrderAddResponse(exeReport);
			FixDataConvertHelper::InternalToFix(RMMS_NS::RMMS_CN_ZZ,exeReport,&msg);
			OnResponseMsg(msg);
		} else {
			BusinessReject reject;
			FIX::Message msg;
			CDataConvert::TranslateOrderInsertErrorToPublic(package,&reject);
			// OnBussinessReject(reject);
			FixDataConvertHelper::InternalToFix(RMMS_NS::RMMS_CN_ZZ,reject,&msg);
			OnResponseMsg(msg);
		}
	}

	void CZZTraderAdapter::OrderCancelProcess(int32 pid,CZZPackage &package)
	{
		if(package.GetFieldIsNULL(FID_ErrorCode)){
			ExecutionReport exeReport;
			FIX::Message msg;
			CDataConvert::TranslateOrderExcutionToPublic(package,&exeReport);
			//OnOrderCancelResponse(exeReport);
			FixDataConvertHelper::InternalToFix(RMMS_NS::RMMS_CN_ZZ,exeReport,&msg);
			OnResponseMsg(msg);
		} else {
			OrderCancelReject cancel;
			FIX::Message msg;
			CDataConvert::TranslateOrderActionErrorToPublic(package,&cancel);
			//OnOrderCancelReject(cancel);
			FixDataConvertHelper::InternalToFix(RMMS_NS::RMMS_CN_ZZ,cancel,&msg);
			OnResponseMsg(msg);
		}
	}

	void CZZTraderAdapter::OrderFillProcess(int32 pid,CZZPackage &package)
	{
		FillReport fill;
		FIX::Message msg;
		if(0 > CDataConvert::TradeInsertSingleToPublic(package,&fill))
			return;
		//OnOrderFillResponse(fill);
		FixDataConvertHelper::InternalToFix(RMMS_NS::RMMS_CN_ZZ,fill,&msg);
		OnResponseMsg(msg);
	}

	void CZZTraderAdapter::OnAdapterRsp(int32 pid,CZZPackage &package)
	{
		switch(pid){
		case PID_OrderInsertRsp:
			OrderAddProcess(pid,package);
			break;
		case PID_TradeInsertSingleRsp:
			OrderFillProcess(pid,package);
			break;
		case PID_OrderActionRsp:
			OrderCancelProcess(pid,package);
			break;
		}
	}

	/* 下单请求 */
	void CZZTraderAdapter::ReqeustOrderAdd(const Order&  order)
	{
		CZZCommonAPI *pAdapter = 
			&(simpattern::Singleton_InClass<CZZCommonAPI>::Instance());
		m_package->ClearAllFields();
		CDataConvert::TranslatePublicToOrder(order,*m_package,m_dataconv.get());
		pAdapter->SendMsg(*m_package);
	}

	/* 下单取消 */
	void CZZTraderAdapter::RequestOrderCancel(
		const OrderCancelRequest& order)
	{
		CZZCommonAPI *pAdapter = 
			&(simpattern::Singleton_InClass<CZZCommonAPI>::Instance());
		m_package->ClearAllFields();
		CDataConvert::TranslatePublicToCancelOrder(order,*m_package,
			m_dataconv.get());
		pAdapter->SendMsg(*m_package);
	}

	/* 下单修改 */
	void CZZTraderAdapter::RequestOrderModify(
		const OrderCancelReplaceRequest& /*order*/)
	{
		/* 不支持 */		
		DBG_ASSERT(false);
	}

	tstring CZZTraderAdapter::GetName()
	{
		return "ZZTraderAdapter";
	}

	int32 CZZTraderAdapter::OnNetErrDialog()
	{
		CZZCommonAPI *pAdapter = 
			&(simpattern::Singleton_InClass<CZZCommonAPI>::Instance());
		PriOnNetDisconnected();
		int32 i = 0;
		while(3 > i){
			if(pAdapter->Register(DIALOG,m_stConnAttr,
				m_stLoginAttr,TRADER,this) && pAdapter->Start(DIALOG)){
				PriOnNetConnected();
				return 0;
			}
			i++;
		}
		return -1;
	}

	int32 CZZTraderAdapter::OnNetErrPrivate()
	{
		CZZCommonAPI *pAdapter = 
			&(simpattern::Singleton_InClass<CZZCommonAPI>::Instance());
		PriOnNetDisconnected();
		int32 i = 0;
		while(3 > i){
			if(pAdapter->Register(PRIVATE,m_stConnAttr,m_stLoginAttr,
				TRADER,this) && pAdapter->Start(PRIVATE)){
				PriOnNetConnected();
				return 0;
			}
			i++;
		}
		return -1;
	}
}