#ifndef __ZZ_API_ADAPTER_H__
#define __ZZ_API_ADAPTER_H__

#include "ZZPackage.h"

namespace exchangeadapter_zhengzhou
{
	enum FlowType {
		DIALOG = 0,
		PRIVATE = 1,
		BROADCAST = 2,
		MAX_FLOW,
	};

	struct ApiAttr {
		THREAD_MODE modeThread;
		tstring strLogPath;
		int dwMonitorPort;		
	};

	struct ConnAttr {
		API_BOOL bEncrypt;
		API_BOOL bCommpress;
		MARKET_ID idMarket;
		int dwHeartBeatInterval;
		int dwHeartBeatTimeout;
		tstring strIPAddress;
		int dwPort;
		double fdWait;
	};

	struct LoginAttr {
		tstring strMemberID;
		tstring strTraderID;
		tstring strPasswd;
		tstring strLoginIP;
		double fdWait;
		tstring strProtocolVersion;
		tstring strInitPassword;
		tstring strAuthSerialNo;
		tstring strAuthCode;
		int dwSeNo;
	};

	struct LogoutAttr {
		tstring strMemberID;
		tstring strTraderID;
	};

	class CZZAPIAdapter
	{
	public:
		static bool InitAPI(const ApiAttr api);
		static ExchgConnectionHandle Connect(
			const ConnAttr conn,void *pTrader,FlowType enFlowType);
		static bool Login(
			const ExchgConnectionHandle hConn,const LoginAttr login,
			FlowType enFlowType);
		static bool Logout(
			const ExchgConnectionHandle hConn,const LogoutAttr logout,
			FlowType enFlowType);
		static void Disconnect(ExchgConnectionHandle &hConn);
		static void UninitAPI();

	protected:
		/* 对话流,对话流数据将直接屏蔽 */
		static void OnOpenDlg(
			void * CallBackArg,
			ExchgConnectionHandle conn,
			int error_code,const char* error_text);
		static void OnErrorDlg(
			void * CallBackArg,
			ExchgConnectionHandle conn,
			int error_code,const char* error_text);
		static void OnCloseDlg(
			void * CallBackArg,
			ExchgConnectionHandle conn,
			int error_code,const char* error_text);
		
		static int OnLoginDlg(
			void * CallBackArg,
			ExchgConnectionHandle conn,
			MsgPackageHandle pkg);
		static int OnRecvDlg(
			void * CallBackArg,
			ExchgConnectionHandle conn,
			MsgPackageHandle pkg);
		static int OnLogoutDlg(
			void * CallBackArg,
			ExchgConnectionHandle conn,
			MsgPackageHandle pkg);

		/* 私有流数据 */
	protected:
		static void OnOpenPri(
			void * CallBackArg,
			ExchgConnectionHandle conn,
			int error_code,const char* error_text);
		static void OnErrorPri(
			void * CallBackArg,
			ExchgConnectionHandle conn,
			int error_code,const char* error_text);
		static void OnClosePri(
			void * CallBackArg,
			ExchgConnectionHandle conn,
			int error_code,const char* error_text);

		static int OnLoginPri(
			void * CallBackArg,
			ExchgConnectionHandle conn,
			MsgPackageHandle pkg);
		static int OnRecvPri(
			void * CallBackArg,
			ExchgConnectionHandle conn,
			MsgPackageHandle pkg);
		static int OnLogoutPri(
			void * CallBackArg,
			ExchgConnectionHandle conn,
			MsgPackageHandle pkg);

		/* 广播流数据 */
	protected:
		static void OnOpenBrd(
			void * CallBackArg,
			ExchgConnectionHandle conn,
			int error_code,const char* error_text);
		static void OnErrorBrd(
			void * CallBackArg,
			ExchgConnectionHandle conn,
			int error_code,const char* error_text);
		static void OnCloseBrd(
			void * CallBackArg,
			ExchgConnectionHandle conn,
			int error_code,const char* error_text);

		static int OnLoginBrd(
			void * CallBackArg,
			ExchgConnectionHandle conn,
			MsgPackageHandle pkg);
		static int OnRecvBrd(
			void * CallBackArg,
			ExchgConnectionHandle conn,
			MsgPackageHandle pkg);
		static int OnLogoutBrd(
			void * CallBackArg,
			ExchgConnectionHandle conn,
			MsgPackageHandle pkg);
	};
}

#endif