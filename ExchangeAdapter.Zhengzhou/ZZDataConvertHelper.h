#ifndef _ZZ_DATA_CONVERT_H_
#define _ZZ_DATA_CONVERT_H_

#include "ChinaQuoteDefinition.h"
#include "ChinaOrderDefinition.h"
#include "ZZPackage.h"

#define _XML_GET_VALUE(node,nodename)	\
	node->FirstChild(nodename)->FirstChild()->Value()

#define _STR_TO_LOWER(x)	\
	std::transform(x.begin(),x.end(),x.begin(),std::tolower);

#define XML_GET_STRING(node,nodename,nodevalue)	\
	nodevalue = _XML_GET_VALUE(node,nodename);

#define XML_GET_INT(node,nodename,nodevalue)	\
	std::sscanf(_XML_GET_VALUE(node,nodename),"%d",&nodevalue);

#define XML_GET_UINT(node,nodename,nodevalue)	\
	std::sscanf(_XML_GET_VALUE(node,nodename),"%u",&nodevalue);

#define XML_GET_BOOL(node,nodename,nodevalue)	\
{											\
	tstring str = _XML_GET_VALUE(node,nodename);	\
	_STR_TO_LOWER(str);						\
	nodevalue = "true" == str ? true:false;	\
}

namespace exchangeadapter_zhengzhou
{
	/* ��̬���� */
	typedef struct DataConvInfo {
		tstring *pparticipantid;
		tstring *puserid;
	}DataConvInfo;

	class CDataConvert
	{
	public:
		static bool TranslateQuoteToPublic(
			CZZPackage &package,Quote* pquote);
	
	public:
		static int TranslatePublicToOrder(
			const Order &order,CZZPackage &package,const DataConvInfo *data);

		static int TranslatePublicToCancelOrder(
			const OrderCancelRequest & cancelOrder,
			CZZPackage &package,const DataConvInfo *data);

		static int TranslateOrderExcutionToPublic(
			CZZPackage &package,ExecutionReport * preport);
		static int TradeInsertSingleToPublic(
			CZZPackage &package,FillReport *preport);
		static void TranslateOrderInsertErrorToPublic(
			CZZPackage &package,BusinessReject *reject);
		static void TranslateOrderActionErrorToPublic(
			CZZPackage &package,OrderCancelReject *reject);

	private:
		static int OrderTypePublicToPrivate(OrderTypeFlag type);
		static int OrderInsertToPublic(
			CZZPackage &package,ExecutionReport * preport);
		static int OrderActionToPublic(
			CZZPackage &package,ExecutionReport * preport);

		static tstring IDConvert(const tchar* ori);
	};
}

#endif