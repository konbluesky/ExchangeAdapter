#include "QuotAdapter.h"
#include "DLMarketDataProvider.h"
#include "DLDataConvertHelper.h"
#include "debugutils.h"
#include "FixDataConvertHelper.h"
#include "LocalCompile.h"

namespace exchangeadapter_dalian
{
	CQuotAdapter::CQuotAdapter(CDLMarketDataProvider *pTraderAdapter)
	{
		DBG_ASSERT(NULL != pTraderAdapter);
		m_pTraderAdapter = pTraderAdapter;
	}

	CQuotAdapter::~CQuotAdapter(void)
	{
	}

	int CQuotAdapter::onRspTraderPwdUpd(uint32 nSeqNo,
		const _fldRspMsg & rspmsg,const _fldTraderPwdUpdReq & traderpwdupdreq,
		BYTE bChainFlag)
	{
		UNREF_VAR(nSeqNo);
		UNREF_VAR(rspmsg);
		UNREF_VAR(traderpwdupdreq);
		UNREF_VAR(bChainFlag);
		return 0;
	}

	int CQuotAdapter::onNtySnapShotQuot(uint32 nSeqNo,
		const _fldQuotQryReq & quotqryreq,BYTE bChainFlag)
	{
		UNREF_VAR(nSeqNo);
		UNREF_VAR(quotqryreq);
		UNREF_VAR(bChainFlag);
		return 0;
	}

	int CQuotAdapter::onRspQuotUserLogout(uint32 nSeqNo,
		const _fldRspMsg & rspmsg,const _fldTraderLogoutRsp & traderlogoutrsp,
		BYTE bChainFlag)
	{
		UNREF_VAR(nSeqNo);
		UNREF_VAR(rspmsg);
		UNREF_VAR(traderlogoutrsp);
		UNREF_VAR(bChainFlag);
		return 0;
	}

	int CQuotAdapter::onRspQryQuotRight(uint32 nSeqNo,
		const _fldRspMsg & rspmsg,
		CAPIVector<_fldQuotSubsRight> & lstQuotSubsRight,BYTE bChainFlag)
	{
		UNREF_VAR(nSeqNo);
		UNREF_VAR(rspmsg);
		UNREF_VAR(lstQuotSubsRight);
		UNREF_VAR(bChainFlag);
		return 0;
	}

	int CQuotAdapter::onRspUpdQuotRight(uint32 nSeqNo,
		const _fldRspMsg & rspmsg,const _fldQuotSubsRight & quotsubsright,
		BYTE bChainFlag)
	{
		UNREF_VAR(nSeqNo);
		UNREF_VAR(rspmsg);
		UNREF_VAR(quotsubsright);
		UNREF_VAR(bChainFlag);
		return 0;
	}

	int CQuotAdapter::onMarketDataMBLQuot(uint32 nSeqNo,
		const _fldBestQuot & bestquot,CAPIVector<_fldOptPara> & lstOptPara,
		CAPIVector<_fldMBLQuot> & lstMBLQuot,BYTE bChainFlag)
	{
		UNREF_VAR(nSeqNo);
		UNREF_VAR(lstOptPara);
		UNREF_VAR(lstMBLQuot);
		UNREF_VAR(bChainFlag);

		Quote quote;
		DoM dom;

		UDLDataConvert::TranslateQuoteDataToPublicData(
			bestquot,&quote);
		//UDLDataConvert::TranslateDomDataToPublicData(
		//	lstMBLQuot,bestquot,&dom);
		Quote2Dom(quote,&dom);
		
		//m_pTraderAdapter->OnQuoteRecieve(quote);
		//m_pTraderAdapter->OnDoMRecieve(dom);
		FIX::Message msg;
		FixDataConvertHelper::InternalToFix(RMMS_NS::RMMS_CN_DL,
			dom,&msg);
		m_pTraderAdapter->OnRspMsg(msg);
		return 0;

	}

	int CQuotAdapter::onMarketDataArbiMBLQuot(uint32 nSeqNo,
		const _fldArbiBestQuot & arbibestquot,
		CAPIVector<_fldMBLQuot> & lstMBLQuot,BYTE bChainFlag)
	{
		UNREF_VAR(nSeqNo);
		UNREF_VAR(arbibestquot);
		UNREF_VAR(lstMBLQuot);
		UNREF_VAR(bChainFlag);
		return 0;
	}

	int CQuotAdapter::onRspQuotTraderPwdUpd(uint32 nSeqNo,
		const _fldRspMsg & rspmsg,const _fldTraderPwdUpdReq & traderpwdupdreq,
		BYTE bChainFlag)
	{
		UNREF_VAR(nSeqNo);
		UNREF_VAR(rspmsg);
		UNREF_VAR(traderpwdupdreq);
		UNREF_VAR(bChainFlag);
		return 0;
	}

	int CQuotAdapter::onRspQueryHistoryQuot(uint32 nSeqNo,
		const _fldRspMsg & rspmsg,const _fldBestQuot & bestquot,
		CAPIVector<_fldOptPara> & lstOptPara,
		CAPIVector<_fldMBLQuot> & lstMBLQuot,BYTE bChainFlag)
	{
		UNREF_VAR(nSeqNo);
		UNREF_VAR(rspmsg);
		UNREF_VAR(bestquot);
		UNREF_VAR(lstOptPara);
		UNREF_VAR(lstMBLQuot);
		UNREF_VAR(bChainFlag);
		return 0;
	}

	int CQuotAdapter::onNtyCloseMktNotice(uint32 nSeqNo,
		const _fldMktDataNotice & mktdatanotice,BYTE bChainFlag)
	{
		UNREF_VAR(nSeqNo);
		UNREF_VAR(mktdatanotice);
		UNREF_VAR(bChainFlag);
		return 0;
	}

	int CQuotAdapter::onInvalidPackage(uint32 nTID,uint16 nSeries,
		uint32 nSequenceNo,uint16 nFieldCount,uint16 nFieldsLen,
		const tchar *pAddr)
	{
		UNREF_VAR(nTID);
		UNREF_VAR(nSeries);
		UNREF_VAR(nSequenceNo);
		UNREF_VAR(nFieldCount);
		UNREF_VAR(nFieldsLen);
		UNREF_VAR(pAddr);
		return -1;
	}

	void CQuotAdapter::onChannelLost(const tchar *szErrMsg)
	{
		m_pTraderAdapter->onChannelLost(szErrMsg);
		return;
	}
}