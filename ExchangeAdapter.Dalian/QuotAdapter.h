#ifndef QUOT_ADAPTER_H
#define QUOT_ADAPTER_H


#include "DLLib.h"

namespace exchangeadapter_dalian
{
class CDLMarketDataProvider;

class CQuotAdapter : public CQuotAPI
{
public:
	CQuotAdapter(CDLMarketDataProvider *pTraderAdapter);
	virtual ~CQuotAdapter(void);
public:
	virtual int onRspTraderPwdUpd(
		uint32 nSeqNo,
		const _fldRspMsg & rspmsg,
		const _fldTraderPwdUpdReq & traderpwdupdreq,
		uint8 bChainFlag=CHAIN_SINGLE);
	virtual int onNtySnapShotQuot(
		uint32 nSeqNo,
		const _fldQuotQryReq & quotqryreq,
		uint8 bChainFlag=CHAIN_SINGLE);
	virtual int onRspQuotUserLogout(
		uint32 nSeqNo,
		const _fldRspMsg & rspmsg,
		const _fldTraderLogoutRsp & traderlogoutrsp,
		uint8 bChainFlag=CHAIN_SINGLE);
	virtual int onRspQryQuotRight(
		uint32 nSeqNo,
		const _fldRspMsg & rspmsg,
		CAPIVector<_fldQuotSubsRight> & lstQuotSubsRight,
		uint8 bChainFlag=CHAIN_SINGLE);
	virtual int onRspUpdQuotRight(
		uint32 nSeqNo,
		const _fldRspMsg & rspmsg,
		const _fldQuotSubsRight & quotsubsright,
		uint8 bChainFlag=CHAIN_SINGLE);
	virtual int onMarketDataMBLQuot(
		uint32 nSeqNo,
		const _fldBestQuot & bestquot,
		CAPIVector<_fldOptPara> & lstOptPara,
		CAPIVector<_fldMBLQuot> & lstMBLQuot,
		uint8 bChainFlag=CHAIN_SINGLE);
	virtual int onMarketDataArbiMBLQuot(
		uint32 nSeqNo,
		const _fldArbiBestQuot & arbibestquot,
		CAPIVector<_fldMBLQuot> & lstMBLQuot,
		uint8 bChainFlag=CHAIN_SINGLE);
	virtual int onRspQuotTraderPwdUpd(
		uint32 nSeqNo,
		const _fldRspMsg & rspmsg,
		const _fldTraderPwdUpdReq & traderpwdupdreq,
		uint8 bChainFlag=CHAIN_SINGLE);
	virtual int onRspQueryHistoryQuot(
		uint32 nSeqNo,
		const _fldRspMsg & rspmsg,
		const _fldBestQuot & bestquot,
		CAPIVector<_fldOptPara> & lstOptPara,
		CAPIVector<_fldMBLQuot> & lstMBLQuot,
		uint8 bChainFlag=CHAIN_SINGLE);
	virtual int onNtyCloseMktNotice(
		uint32 nSeqNo,
		const _fldMktDataNotice & mktdatanotice,
		uint8 bChainFlag=CHAIN_SINGLE);
	virtual int onInvalidPackage(
		uint32 nTID,uint16 nSeries,
		uint32 nSequenceNo,
		uint16 nFieldCount,
		uint16 nFieldsLen,
		const tchar *pAddr);
	virtual void onChannelLost(const tchar *szErrMsg);
public:
	CDLMarketDataProvider *m_pTraderAdapter;
};

}

#endif
