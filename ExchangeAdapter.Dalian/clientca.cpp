/* ------------------------------------------------------------------
 * filename:    clientca.cpp
 * version:     1.0.0.0
 * created:     2005-9-22 13:51
 * author:		shigw
 * purpose:		CA操作的客户端封装
 * ------------------------------------------------------------------ 
 * Date
 * ------------------------------------------------------------------ 
*/ 
#include <stdio.h>
//#include <io.h>
#include "clientca.h"

CClientCA::CClientCA()
{
	f_init();
}

CClientCA::~CClientCA()
{
	f_done();
}

/* ------------------------------------------------------------------
 * 构造函数
 * ------------------------------------------------------------------
*/
void CClientCA::f_init(void)
{
	m_sCertInfo = NULL;
	m_sKeyInfo	= NULL;
	m_sRootInfo	= NULL;

	m_iCertInfo	= 0;
	m_iKeyInfo	= 0;
	m_iRootInfo	= 0;

	m_hSE		= NULL;
}
/* ------------------------------------------------------------------
 * 析构函数
 * ------------------------------------------------------------------
*/
void CClientCA::f_done(void)
{
	if (m_sCertInfo)
		delete [] m_sCertInfo;
	if (m_sKeyInfo)
		delete [] m_sKeyInfo;
	if (m_sRootInfo)
		delete [] m_sRootInfo;

	if (m_hSE)
		SEH_ClearSession(m_hSE);
	f_init();
}
/* ------------------------------------------------------------------
 * 装载客户端证书
 * ------------------------------------------------------------------
 * sCertFile:		证书文件
 * sKeyFile:		私钥文件
 * sRootFile:		根证书文件
 * sPasswd:			私钥密码
 * ------------------------------------------------------------------
*/
BOOL CClientCA::LoadCert(_IN_	int		const	iCertType,
						 _IN_	char	const	*sCertFile,
						 _IN_	char	const	*sKeyFile,
						 _IN_	char	const	*sRootFile,
						 _IN_	char	const	*sPasswd,
						 ___________________
						 _OUT_	int		*piErrCode,
						 _OUT_	char	*psErrMsg)
{
	int		m_iRet;
	BOOL	m_bRet;
	char	m_sRet[256];
	long	m_lRet;

	m_lRet = 0;
	m_iRet = 0;
	m_bRet = TRUE;
	strcpy(m_sRet,"");
	if (m_hSE)
	{
		sprintf(m_sRet,"证书已经初始化!");
		m_bRet = FALSE;
		m_iRet = E_CCERTINITED;
		goto L_RET;
	}

	//文件证书
	if(iCertType==T_FILECERT)
	{
		m_bRet = f_read_file(sCertFile,&m_sCertInfo,&m_iCertInfo,&m_iRet,m_sRet);
		if (m_bRet == FALSE)
			goto L_RET;
		m_bRet = f_read_file(sKeyFile,&m_sKeyInfo,&m_iKeyInfo,&m_iRet,m_sRet);
		if (m_bRet == FALSE)
			goto L_RET;
		m_bRet = f_read_file(sRootFile,&m_sRootInfo,&m_iRootInfo,&m_iRet,m_sRet);
		if (m_bRet == FALSE)
			goto L_RET;
		m_lRet = SEH_InitialSessionEx(&m_hSE,(BYTE*)m_sKeyInfo,m_iKeyInfo,(char*)sPasswd,(BYTE*)m_sRootInfo,m_iRootInfo);
	}
	else	//硬件证书--USB证书
	{
		m_lRet = SEH_InitialSession(&m_hSE, 0x0009, "com1", (char*)sPasswd, 0, 0x0009, "com1", "");
		if (m_lRet != SE_SUCCESS)
		{
			sprintf(m_sRet,"Initsession failed:%x\n",m_lRet);
			m_iRet = m_lRet;
			m_bRet = FALSE;
			goto L_RET;
		}
		m_iCertInfo=2048;
		m_sCertInfo=new BYTE[2048];
		m_lRet = SEH_GetSelfCertificate(&m_hSE, 0x0009,"com1", (char*)sPasswd,m_sCertInfo,(unsigned short *)&m_iCertInfo);
	}
	if (m_lRet != SE_SUCCESS)
	{
		sprintf(m_sRet,"Initsession failed:%x\n",m_lRet);
		m_iRet = m_lRet;
		m_bRet = FALSE;
		goto L_RET;
	}
L_RET:
	if (psErrMsg)
		strcpy(psErrMsg,m_sRet);
	if (piErrCode)
		*piErrCode = m_iRet;
	return m_bRet;
}
/* ------------------------------------------------------------------
 * 从文件中读取内容
 * ------------------------------------------------------------------
 * *sFileName:		证书文件路径
 * **pBuf:			读取出来内容.
 * *piBuf:			读取出来的内容长度
 * ------------------------------------------------------------------
*/
BOOL CClientCA::f_read_file(_IN_	char	const	*sFileName,
							___________________
							_OUT_	BYTE	**pBuf,
							_OUT_	int		*piBuf,
							___________________
							_OUT_	int		*piErrCode,
							_OUT_	char	*piErrMsg)
{
	FILE	*fh;
	int		 m_iRet;
	BOOL	 m_bRet;
	char	 m_sRet[256];
	BYTE	*pb;
	int		 ib;

	m_iRet = 0;
	m_bRet = TRUE;
	strcpy(m_sRet,"");
	fh = NULL;
	pb = NULL;

	fh = fopen(sFileName,"rb");
	if (fh == NULL)
	{
		f_done();
		sprintf(m_sRet,"不能打开证书文件!");
		m_bRet = FALSE;
		m_iRet = E_CFILEOPEN;
		goto L_RET;
	}
	fseek(fh,0,SEEK_END);
	ib = ftell(fh);
	fseek(fh,0,SEEK_SET);
	//ib = filelength(fh->_file);
	if (ib <=0)
	{
		f_done();
		sprintf(m_sRet,"证书文件长度不正确!");
		m_bRet = FALSE;
		m_iRet = E_CFILECORRUPT;
		goto L_RET;
	}
	pb = new BYTE[ib];
	m_iRet = (int)fread(pb,1,ib,fh);
	if (ib!= m_iRet)
	{
		f_done();
		sprintf(m_sRet,"文件读取错误!");
		m_bRet = FALSE;
		m_iRet = E_CREADFILE;
		goto L_RET;
	}
	if (pBuf)
		*pBuf = pb;
	if (piBuf)
		*piBuf = ib;
	m_iRet = 0;
	m_bRet = TRUE;

L_RET:
	if (m_bRet == FALSE)
	{
		if (pb != NULL)
			delete [] pb;
	}
	if (fh)
		fclose(fh);
	if (piErrMsg)
		strcpy(piErrMsg,m_sRet);
	if (piErrCode)
		*piErrCode = m_iRet;
	return m_bRet;
}
/* ------------------------------------------------------------------
 * 产生客户端端签名
 * ------------------------------------------------------------------
 * sOrgString:		被签名串
 * ------------------------------------------------------------------
 * sSignature:		输出的签名串
 * piSignatureLen:	输出的签名串长度
 * ------------------------------------------------------------------
*/
BOOL CClientCA::MakeSignature(_IN_	char	const		*sOrgString,
							  ___________________
							  _OUT_	BYTE				*sSignature,
							  _OUT_	int					*piSignatureLen,
							  ___________________
							  _OUT_	int					*piErrCode,
							  _OUT_	char				*psErrMsg)
{
	return f_MakeSignature(sOrgString,sSignature,piSignatureLen,piErrCode,psErrMsg);
}
BOOL CClientCA::f_MakeSignature(_IN_	char	const	*sOrgString,
								___________________
								_OUT_	BYTE			*sSignature,
								_OUT_	int				*piSignatureLen,
								___________________
								_OUT_	int		*piErrCode,
								_OUT_	char	*psErrMsg)
{
	BOOL	m_bRet;
	int		m_iRet;
	unsigned long	m_lRet;
	char	m_sRet[256];

	m_iRet = 0;
	m_bRet = TRUE;
	strcpy(m_sRet,"");

	if (m_hSE == NULL)
	{
		m_iRet = E_CCERTNOTINITED;
		m_bRet = FALSE;
		sprintf(m_sRet,"客户端证书没有初始化!");
		goto L_RET;
	}

	/* ------------------------------------------------------------------
	* 签名(2-MD5签名）
	* ------------------------------------------------------------------
	*/
	m_iRet = SEH_SignData(m_hSE,(BYTE*)sOrgString,(unsigned long)strlen(sOrgString),
		2,sSignature,&m_lRet);
	if (m_iRet != SE_SUCCESS)
	{
		sprintf(m_sRet,"签名失败[%x]!",m_iRet);
		m_iRet = E_CMAKESIGN;
		m_bRet = FALSE;
		goto L_RET;
	}
	*piSignatureLen = m_lRet;
L_RET:
	if (piErrCode)
		*piErrCode = m_iRet;
	if (psErrMsg)
		strcpy(psErrMsg,m_sRet);
	return m_bRet;
}

/* ------------------------------------------------------------------
 * 验证服务端的签名
 * ------------------------------------------------------------------
 * sServerCert:			服务端证书
 * iServerCert:			服务端证书长度
 * sSignature:			服务端签名
 * iSignatureLen:		服务端签名长度
 * sOrgString:			签名原始串
 * ------------------------------------------------------------------
*/
BOOL CClientCA::CheckSignature(_IN_	BYTE	const		*sServerCert,
							   _IN_	int					 iServerCert,
							   _IN_	BYTE	const		*sSignature,
							   _IN_	int					 iSignatureLen,
							   _IN_	char	const		*sOrgString,
							   ___________________
							   _OUT_	int			*piErrCode,
							   _OUT_	char		*psErrMsg)
{
	return f_CheckSignature(sServerCert,iServerCert,sSignature,iSignatureLen,sOrgString,piErrCode,psErrMsg);
}

BOOL CClientCA::f_CheckSignature(_IN_	BYTE	const		*sServerCert,
								 _IN_	int					 iServerCert,
								 _IN_	BYTE	const		*sSignature,
								 _IN_	int					 iSignatureLen,
								 _IN_	char	const		*sOrgString,
								 ___________________
								 _OUT_	int				*piErrCode,
								 _OUT_	char			*psErrMsg)
{
	BOOL	m_bRet;
	int		m_iRet;
	char	m_sRet[256];

	m_iRet = 0;
	m_bRet = TRUE;
	strcpy(m_sRet,"");


	if (m_hSE == NULL)
	{
		m_iRet = E_CCERTNOTINITED;
		m_bRet = FALSE;
		sprintf(m_sRet,"客户端证书没有初始化!");
		goto L_RET;
	}

	/* ------------------------------------------------------------------
	 * 验证签名(2-MD5签名）
	 * ------------------------------------------------------------------
	*/
	m_iRet = SEH_VerifySignData(m_hSE,(BYTE*)sOrgString,(unsigned long)strlen(sOrgString),
		2,(BYTE*)sSignature,iSignatureLen,(BYTE*)sServerCert,iServerCert);
	if (m_iRet != SE_SUCCESS)
	{
		sprintf(m_sRet,"验证签名失败[%x]!",m_iRet);
		m_iRet = E_CVERIFYSIGN;
		m_bRet = FALSE;
		goto L_RET;
	}
L_RET:
	if (piErrCode)
		*piErrCode = m_iRet;
	if (psErrMsg)
		strcpy(psErrMsg,m_sRet);
	return m_bRet;
}
/* ------------------------------------------------------------------
 * 取得客户端证书
 * ------------------------------------------------------------------
 * sCertInfo:		客户端证书
 * piCertInfoLen:	客户端证书长度
 * ------------------------------------------------------------------
*/
BOOL CClientCA::GetClientCert(_OUT_	BYTE		*sCertInfo,
							  _OUT_	int			*piCertInfoLen,
							  ___________________
							  _OUT_	int				*piErrCode,
							  _OUT_	char			*psErrMsg)
{
	return f_GetClientCert(sCertInfo,piCertInfoLen,piErrCode,psErrMsg);
}
BOOL CClientCA::f_GetClientCert(_OUT_	BYTE		*sCertInfo,
								_OUT_	int			*piCertInfoLen,
								___________________
								_OUT_	int				*piErrCode,
								_OUT_	char			*psErrMsg)
{
	BOOL	m_bRet;
	int		m_iRet;
	char	m_sRet[256];

	m_iRet = 0;
	m_bRet = TRUE;
	strcpy(m_sRet,"");


	if (m_sCertInfo == NULL)
	{
		m_iRet = E_CCERTNOTINITED;
		m_bRet = FALSE;
		sprintf(m_sRet,"客户端证书没有初始化!");
		goto L_RET;
	}
	if (sCertInfo)
		memcpy(sCertInfo,m_sCertInfo,m_iCertInfo);
	if (piCertInfoLen)
		*piCertInfoLen = m_iCertInfo;
L_RET:
	if (piErrCode)
		*piErrCode = m_iRet;
	if (psErrMsg)
		strcpy(psErrMsg,m_sRet);
	return m_bRet;
}
/* ------------------------------------------------------------------
 * 密码信封加密
 * ------------------------------------------------------------------
 * sOrgString:			被加密串
 * sServerCert:			服务器证书
 * iServerCertLen:		服务器证书长度
 * ------------------------------------------------------------------
 * sSecurData:			加密后的串
 * piSecurLen:			加密后的串长度
 * ------------------------------------------------------------------
*/
BOOL CClientCA::SecurityBox(_IN_	char	const	*sOrgString,
							_IN_	BYTE	const	*sServerCert,
							_IN_	int				 iServerCertLen,
							___________________
							_OUT_	BYTE			*sSecurData,
							_OUT_	int				*piSecurLen,
							___________________
							_OUT_	int				*piErrCode,
							_OUT_	char			*psErrMsg)
{
	return f_SecurityBox(sOrgString,sServerCert,iServerCertLen,sSecurData,piSecurLen,piErrCode,psErrMsg);
}
BOOL CClientCA::f_SecurityBox(_IN_	char	const	*sOrgString,
							  _IN_	BYTE	const	*sServerCert,
							  _IN_	int				iServerCertLen,
							  ___________________
							  _OUT_	BYTE			*sSecurData,
							  _OUT_	int				*piSecurLen,
							  ___________________
							  _OUT_	int				*piErrCode,
							  _OUT_	char			*psErrMsg)
{
	BOOL	m_bRet;
	int		m_iRet;
	unsigned long	m_lLen;
	unsigned long	m_lRet;
	char	m_sRet[256];

	m_iRet = 0;
	m_bRet = TRUE;
	strcpy(m_sRet,"");

	if (m_hSE == NULL)
	{
		m_iRet = E_CCERTNOTINITED;
		m_bRet = FALSE;
		sprintf(m_sRet,"客户端证书没有初始化!");
		goto L_RET;
	}

	/* ------------------------------------------------------------------
	 * 密码信封加密
	 * ------------------------------------------------------------------
	*/
	m_lRet = SEH_Envelope(m_hSE,1,(BYTE*)sOrgString,
		(unsigned long)strlen(sOrgString)+1,sSecurData,&m_lLen,
		(BYTE*)sServerCert,iServerCertLen);
	if(m_lRet != SE_SUCCESS)
	{
		m_bRet = FALSE;
		m_iRet = E_CENCODE;
		sprintf(m_sRet,"密码信封加密失败[%x]!",m_lRet);
		goto L_RET;
	}
	*piSecurLen = m_lLen;

L_RET:
	if (piErrCode)
		*piErrCode = m_iRet;
	if (psErrMsg)
		strcpy(psErrMsg,m_sRet);
	return m_bRet;
}
/* ------------------------------------------------------------------
 * 密码信封解密
 * ------------------------------------------------------------------
 * sSecurData:			被解密串
 * iSecurLen:			被解密串长度
 * ------------------------------------------------------------------
 * sOrgString:			解密后的字符串
 * ------------------------------------------------------------------
*/
BOOL CClientCA::DecodeSecurityBox(_IN_	BYTE	const	*sSecurData,
								  _IN_	int				 iSecurLen,
								  ___________________
								  _OUT_	char			*sOrgString,
								  ___________________
								  _OUT_	int		*piErrCode,
								  _OUT_	char	*psErrMsg)
{
	return f_DecodeSecurityBox(sSecurData,iSecurLen,sOrgString,piErrCode,psErrMsg);
}
BOOL CClientCA::f_DecodeSecurityBox(_IN_	BYTE	const	*sSecurData,
									_IN_	int				 iSecurLen,
									___________________
									_OUT_	char			*sOrgString,
									___________________
									_OUT_	int		*piErrCode,
									_OUT_	char	*psErrMsg)
{
	BOOL	m_bRet;
	int		m_iRet;
	unsigned long	m_lLen;
	unsigned long	m_lRet;
	char	m_sRet[256];

	m_iRet = 0;
	m_bRet = TRUE;
	strcpy(m_sRet,"");

	if (m_hSE == NULL)
	{
		m_iRet = E_CCERTNOTINITED;
		m_bRet = FALSE;
		sprintf(m_sRet,"客户端证书没有初始化!");
		goto L_RET;
	}

	/* ------------------------------------------------------------------
	* 密码信封加密
	* ------------------------------------------------------------------
	*/
	m_lRet = SEH_Envelope(m_hSE,2,(BYTE*)sSecurData,iSecurLen,(BYTE*)sOrgString,&m_lLen,
		NULL,0);
	if(m_lRet != SE_SUCCESS)
	{
		m_bRet = FALSE;
		m_iRet = E_CDECODE;
		sprintf(m_sRet,"密码信封解密失败[%x]!",m_lRet);
		goto L_RET;
	}
L_RET:
	if (piErrCode)
		*piErrCode = m_iRet;
	if (psErrMsg)
		strcpy(psErrMsg,m_sRet);
	return m_bRet;
}

/**函数名称:CClientCA::f_GetCertValidDate
***功能说明:获取证书有效期
* @return : BOOL 
*/
BOOL CClientCA::GetCertValidDate(
		_OUT_	char	*sStart,
		_OUT_	char    *sExpires,
		_OUT_	int				*piErrCode,
		_OUT_	char			*psErrMsg)
{
	return f_GetCertValidDate(sStart,sExpires,piErrCode,psErrMsg);
}

BOOL CClientCA::f_GetCertValidDate(
		_OUT_	char	*sStart,
		_OUT_	char    *sExpires,
		_OUT_	int		*piErrCode,
		_OUT_	char	*psErrMsg)
{
	BOOL	m_bRet;
	int		m_iRet;
	char	m_sRet[256];

	m_iRet = 0;
	m_bRet = TRUE;
	strcpy(m_sRet,"");

	unsigned short iStartLen=100;
	unsigned short iExpiresLen=100;

	if (m_sCertInfo == NULL)
	{
		m_iRet = E_CCERTNOTINITED;
		m_bRet = FALSE;
		sprintf(m_sRet,"客户端证书没有初始化!");
		goto L_RET;
	}


	//取得证书有效期起始
	SEH_GetCertDetail(m_hSE, (BYTE*)m_sCertInfo, m_iCertInfo, 11,(unsigned char *)sStart, &iStartLen);
	sStart[8]='\0';
	//取得证书有效期截止
	SEH_GetCertDetail(m_hSE, (BYTE*)m_sCertInfo, m_iCertInfo, 12,(unsigned char *)sExpires, &iExpiresLen);
	sExpires[8]='\0';

L_RET:
	if (piErrCode)
		*piErrCode = m_iRet;
	if (psErrMsg)
		strcpy(psErrMsg,m_sRet);
	return m_bRet;
}

void CClientCA::Done()
{
	f_done();
}
