#ifndef TRADE_API_H
#define TRADE_API_H

#ifdef WIN32
#ifdef API_EXPORTS
#define API_EXPORT __declspec(dllexport)
#else
#define API_EXPORT __declspec(dllimport)
#endif
#else
#define API_EXPORT
#endif

#include "APIStruct.h"
#include "APIVector.h"

#define API_VERSION "00.00.1234"//API版本号,主版本号(两个字节)+'.'+副版本号(两个字节)+'.'+内部build号(四个字节)
//TRADER_API_BASE_ERRORCODE 121010000 API错误码基值

class API_EXPORT CTradeAPI
{
public:
	/**
	*设置前置机的IP和端口
	* @param szIP 前置机的IP
	* @param nPort 前置机的端口
	*/
	void SetService(const char *szIP,int nPort);
	
	/**获取API线程实例句柄
	* @return	void* 
	*/
	void* GetApiThread();
	
	/**获取证书有效日期
	* @param	sStart		.
	* @param	sExpires		.
	* @return	int 
	*/
	int GetCertValidDate(char **sStart,char **sExpires);
	
	/**初始化API,创建消息驱动线程
	* @param isLogged 是否日志输出(true:是，false:否)
	* @param pApiThread Api线程实例
	* @return 返回0表示成功,返回-1表示失败
	*/
	int InitAPI(bool isLogged = true,void* pApiThread=NULL);

	/**初始化CA认证模块
	* @param	iCertType	证书类型
	* @param	sCertFile	证书文件路径
	* @param	sKeyFile	证书私钥文件路径
	* @param	sRootFile	根证书文件路径
	* @param	sPasswd		证书私钥密码
	* @param	bUseCAFlag是否使用证书验证
	* @return 返回0表示成功,返回-1表示失败
	*/
	int InitCA(int iCertType,const char *sCertFile,const char *sKeyFile,const	char *sRootFile,const char *sPasswd,bool bUseCAFlag=true);

	/**
	* 连接前置
	* @return 0表示成功,其它参见错误码
	*/
	int Connect();
	
	/**
	 * 交易员会话请求
	 * @param pTraderSessionReq 输入参数,交易会话请求域
	 * @param pTraderPwd 输入参数 交易席位密码
	 * @param pRspMsg 输出参数 响应域
	 * @param pTraderSessionRsp 输出参数 交易会话应答域
	 * @return 0表示成功
	 * @return 其它表示失败
	 */
	int TraderSession(const _fldTraderSessionReq * pTraderSessionReq,char *pTraderPwd,_fldRspMsg *pRspMsg,_fldTraderSessionRsp * pTraderSessionRsp);
	
	/**
	 * 交易员认证请求
	 * @param pTraderCertReq 输入参数,交易认证请求域
	 * @param pRspMsg 输出参数 响应域
	 * @param pTraderCertRsp 输出参数 交易认证应答域
	 * @return 0表示成功
	 * @return 其它表示失败
	 */
	int TraderCert(const _fldTraderCertReq * pTraderCertReq,_fldRspMsg *pRspMsg,_fldTraderCertRsp * pTraderCertRsp);
	
	/**
	* 登录前置
	* @param pReq 输入参数,登录请求包
	* @param pRspMsg 输出参数 响应域
	* @param pRsp 输出参数,认证反馈包
	* @return =0表示成功
	* @return <>0参见错误码
	*/
	int Login(const _fldTraderLoginReq * pReq,_fldRspMsg *pRspMsg,_fldTraderLoginRsp * pRsp);

	/**
	* 查询期货品种
	* @param vrtyqryreq 品种查询请求
	* @param pRspMsg 输出参数 响应域
	* @param pLstFtrVariety 输出参数,期货品种列表
	* @return =0表示成功
	* @return <>0参见错误码
	*/
	int QryFtrVariety(const _fldVarietyQryReq & vrtyqryreq,_fldRspMsg *pRspMsg,CAPIVector<_fldFtrVariety> **pLstFtrVariety);

	/**
	* 查询期权品种
	* @param vrtyqryreq 品种查询请求
	* @param pRspMsg 输出参数 响应域
	* @param pLstOptVariety 输出参数,期权品种列表
	* @return =0表示成功
	* @return <>0参见错误码
	*/
	int QryOptVariety(const _fldVarietyQryReq & vrtyqryreq,_fldRspMsg *pRspMsg,CAPIVector<_fldOptVariety> **pLstOptVariety);

	/**
	* 查询期货和约
	* @param contrqryreq 和约查询请求域
	* @param pRspMsg 输出参数,响应域
	* @param pLstFtrContr,期货和约列表
	* @return =0表示成功
	* @return <>0参见错误码
	*/
	int QryFtrContr(const _fldContractQryReq & contrqryreq,_fldRspMsg *pRspMsg,CAPIVector<_fldFtrContract> **pLstFtrContr);

	/**
	* 查询期权和约
	* @param contrqryreq 和约查询请求域
	* @param pRspMsg 输出参数,响应域
	* @param pLstOptContr,期权和约列表
	* @return =0表示成功
	* @return <>0参见错误码
	*/
	int QryOptContr(const _fldContractQryReq & contrqryreq,_fldRspMsg *pRspMsg,CAPIVector<_fldOptContract> **pLstOptContr);

	/**
	* 查询套利和约
	* @param contrqryreq 和约查询请求域
	* @param pRspMsg 输出参数,响应域
	* @param pLstArbiContr,套利和约列表
	* @param pLstArbiLeg,套利和约单腿列表
	* @return =0表示成功
	* @return <>0参见错误码
	*/
	int QryArbiContr(const _fldContractQryReq & contrqryreq,_fldRspMsg *pRspMsg,CAPIVector<_fldArbiContract> **pLstArbiContr,CAPIVector<_fldArbiLeg> **pLstArbiLeg);
	
	/**
	 * 查询套利策略规则
	 * @param arbirulereq 套利策略规则请求域
	 * @param rspmsg 应答域，如果应答域错误码表示成功,则该报文包含套利策略规则域;否则不包含套利策略规则
	 * @param pLstArbiRule,套利策略规则列表
	 * @return >=0表示成功
	 * @return <0表示失败
	 */
	int QryArbiRule(const _fldArbiRuleReq & arbirulereq,_fldRspMsg *pRspMsg,CAPIVector<_fldArbiRule> **pLstArbiRule);
	
	/**
	 * 查询客户请求
	 * @param clientqryreq 查询客户请求域
	 * @param rspmsg 应答域，如果应答域错误码表示成功,则该报文包含客户域;否则不包含客户域
	 * @param lstClient 客户域
	 * @return 0表示成功
	 * @return 其它表示失败
	 */
	 int QryTraderClient(const _fldClientQryReq & clientqryreq,_fldRspMsg *pRspMsg,CAPIVector<_fldClient> **pLstClient);
	 
	/**
	 * 发送就绪指令到前置
	 * @param	iPrivateFlow 私有流就绪标志
	 * @param	iMarketFlow	市场流就绪标志
	 * @param	bIsGetStart	是否从头开始获取数据标志
	 * @return 0表示成功
	 * @return 其它参见错误码
	 */
	int Ready(int iPrivateFlow = READY,int iMarketFlow = READY,bool bIsGetStart=false);
	
	/**
	 * 设置同步超时
	 * @param viSyncTimeOut同步超时，缺省120秒 
	 * @return 0表示成功
	 * @return 其它参见错误码
	 */
	int SetSyncTimeOut(int viSyncTimeOut = 120);
public:

	/**
	 * 发送交易员退出请求
	 * @param pSeqNo 输出参数,该报文的序列号
	 * @param traderlogoutreq 交易员退出请求域
	 * @return 0表示成功
	 * @return 其它表示失败
	 */
	int ReqTraderLogout(UINT4 *pSeqNo,const _fldTraderLogoutReq & traderlogoutreq);
	/**		
	 * 当收到交易员退出应答时回调该函数
	 * @param nSeqNo 输入参数,报文的序列号
	 * @param rspmsg 应答域,错误码为零表成功,否则表失败
	 * @param traderlogoutrsp 交易员退出请求域
	 * @param bChainFlag 输出参数,报文链标志
	 * @return >=0表示成功
	 * @return <0表示失败
	 */
	virtual int onRspTraderLogout(UINT4 nSeqNo,const _fldRspMsg & rspmsg,const _fldTraderLogoutRsp & traderlogoutrsp,BYTE bChainFlag=CHAIN_SINGLE){return 0;}
	/**
	 * 发送席位密码更新请求
	 * @param pSeqNo 输出参数,该报文的序列号
	 * @param traderpwdupdreq 席位密码更新请求域
	 * @return 0表示成功
	 * @return 其它表示失败
	 */
	int ReqTraderPwdUpd(UINT4 *pSeqNo,const _fldTraderPwdUpdReq & traderpwdupdreq);
	/**		
	 * 当收到席位密码更新应答时回调该函数
	 * @param nSeqNo 输入参数,报文的序列号
	 * @param rspmsg 应答域,错误码为零表成功,否则表失败
	 * @param traderpwdupdreq 更新后的席位密码域
	 * @param bChainFlag 输出参数,报文链标志
	 * @return >=0表示成功
	 * @return <0表示失败
	 */
	virtual int onRspTraderPwdUpd(UINT4 nSeqNo,const _fldRspMsg & rspmsg,const _fldTraderPwdUpdReq & traderpwdupdreq,BYTE bChainFlag=CHAIN_SINGLE){return 0;}
	/**
	 * 发送交易员定单请求
	 * @param pSeqNo 输出参数,该报文的序列号
	 * @param lstOrder 定单域,当为批量定单或组合定单时，可以为多个，否则为单个
	 * @return 0表示成功
	 * @return 其它表示失败
	 */
	int ReqTraderInsertOrders(UINT4 *pSeqNo,CAPIVector<_fldOrder> & lstOrder);
	/**		
	 * 当收到交易员定单应答时回调该函数
	 * @param nSeqNo 输入参数,报文的序列号
	 * @param rspmsg 应答域,错误码为零表成功,否则表失败
	 * @param lstOrder 定单域
	 * @param bChainFlag 输出参数,报文链标志
	 * @return >=0表示成功
	 * @return <0表示失败
	 */
	virtual int onRspTraderInsertOrders(UINT4 nSeqNo,const _fldRspMsg & rspmsg,CAPIVector<_fldOrder> & lstOrder,BYTE bChainFlag=CHAIN_SINGLE){return 0;}
	/**		
	 * 当收到交易员定单前置通讯应答时回调该函数
	 * @param nSeqNo 输入参数,报文的序列号
	 * @param rspmsg 应答域,错误码为零表成功,否则表失败
	 * @param lstOrder 定单域
	 * @param bChainFlag 输出参数,报文链标志
	 * @return >=0表示成功
	 * @return <0表示失败
	 */
	virtual int onRplTraderInsertOrders(UINT4 nSeqNo,const _fldRspMsg & rspmsg,CAPIVector<_fldOrder> & lstOrder,BYTE bChainFlag=CHAIN_SINGLE){return 0;}
	/**		
	 * 当收到交易员定单状态确认时回调该函数
	 * @param nSeqNo 输入参数,报文的序列号
	 * @param orderstatus 定单状态域
	 * @param bChainFlag 输出参数,报文链标志
	 * @return >=0表示成功
	 * @return <0表示失败
	 */
	virtual int onTraderOrdersConfirmation(UINT4 nSeqNo,const _fldOrderStatus & orderstatus,BYTE bChainFlag=CHAIN_SINGLE){return 0;}
	/**		
	 * 当收到交易员成交通知时回调该函数
	 * @param nSeqNo 输入参数,报文的序列号
	 * @param match 成交域
	 * @param bChainFlag 输出参数,报文链标志
	 * @return >=0表示成功
	 * @return <0表示失败
	 */
	virtual int onNtyTraderMatch(UINT4 nSeqNo,const _fldMatch & match,BYTE bChainFlag=CHAIN_SINGLE){return 0;}
	/**
	 * 发送交易员撤销定单
	 * @param pSeqNo 输出参数,该报文的序列号
	 * @param orderaction 定单操作域
	 * @return 0表示成功
	 * @return 其它表示失败
	 */
	int ReqTraderCancelOrder(UINT4 *pSeqNo,const _fldOrderAction & orderaction);
	/**		
	 * 当收到交易员撤销定单应答时回调该函数
	 * @param nSeqNo 输入参数,报文的序列号
	 * @param rspmsg 应答域,错误码为零表成功,否则表失败
	 * @param orderaction 定单操作域
	 * @param bChainFlag 输出参数,报文链标志
	 * @return >=0表示成功
	 * @return <0表示失败
	 */
	virtual int onRspTraderCancelOrder(UINT4 nSeqNo,const _fldRspMsg & rspmsg,const _fldOrderAction & orderaction,BYTE bChainFlag=CHAIN_SINGLE){return 0;}
	/**		
	 * 当收到交易员撤销定单前置通讯应答时回调该函数
	 * @param nSeqNo 输入参数,报文的序列号
	 * @param rspmsg 应答域,错误码为零表成功,否则表失败
	 * @param orderaction 定单操作域
	 * @param bChainFlag 输出参数,报文链标志
	 * @return >=0表示成功
	 * @return <0表示失败
	 */
	virtual int onRplTraderCancelOrder(UINT4 nSeqNo,const _fldRspMsg & rspmsg,const _fldOrderAction & orderaction,BYTE bChainFlag=CHAIN_SINGLE){return 0;}
	/**
	 * 发送期权执行申请
	 * @param pSeqNo 输出参数,该报文的序列号
	 * @param order 期权执行请求域
	 * @return 0表示成功
	 * @return 其它表示失败
	 */
	int ReqTraderOptExec(UINT4 *pSeqNo,const _fldOrder & order);
	/**		
	 * 当收到期权执行申请应答时回调该函数
	 * @param nSeqNo 输入参数,报文的序列号
	 * @param rspmsg 应答域,错误码为零表成功,否则表失败
	 * @param order 期权执行请求域
	 * @param bChainFlag 输出参数,报文链标志
	 * @return >=0表示成功
	 * @return <0表示失败
	 */
	virtual int onRspTraderOptExec(UINT4 nSeqNo,const _fldRspMsg & rspmsg,const _fldOrder & order,BYTE bChainFlag=CHAIN_SINGLE){return 0;}
	/**
	 * 发送撤销期权执行申请
	 * @param pSeqNo 输出参数,该报文的序列号
	 * @param order 期权执行请求域
	 * @return 0表示成功
	 * @return 其它表示失败
	 */
	int ReqTraderCancelOptExec(UINT4 *pSeqNo,const _fldOrder & order);
	/**		
	 * 当收到撤销期权执行申请应答时回调该函数
	 * @param nSeqNo 输入参数,报文的序列号
	 * @param rspmsg 应答域,错误码为零表成功,否则表失败
	 * @param order 期权执行请求域
	 * @param bChainFlag 输出参数,报文链标志
	 * @return >=0表示成功
	 * @return <0表示失败
	 */
	virtual int onRspTraderCancelOptExec(UINT4 nSeqNo,const _fldRspMsg & rspmsg,const _fldOrder & order,BYTE bChainFlag=CHAIN_SINGLE){return 0;}
	/**
	 * 发送查询期权执行申请
	 * @param pSeqNo 输出参数,该报文的序列号
	 * @param orderqryreq 委托查询请求域
	 * @return 0表示成功
	 * @return 其它表示失败
	 */
	int ReqTraderQryOptExec(UINT4 *pSeqNo,const _fldOrderQryReq & orderqryreq);
	/**		
	 * 当收到查询期权执行申请应答时回调该函数
	 * @param nSeqNo 输入参数,报文的序列号
	 * @param rspmsg 应答域,错误码为零表成功,否则表失败
	 * @param lstOrderStatus 定单状态域
	 * @param bChainFlag 输出参数,报文链标志
	 * @return >=0表示成功
	 * @return <0表示失败
	 */
	virtual int onRspTraderQryOptExec(UINT4 nSeqNo,const _fldRspMsg & rspmsg,CAPIVector<_fldOrderStatus> & lstOrderStatus,BYTE bChainFlag=CHAIN_SINGLE){return 0;}
	/**
	 * 发送查询委托请求
	 * @param pSeqNo 输出参数,该报文的序列号
	 * @param orderqryreq 委托查询请求域
	 * @return 0表示成功
	 * @return 其它表示失败
	 */
	int ReqTraderQryOrder(UINT4 *pSeqNo,const _fldOrderQryReq & orderqryreq);
	/**		
	 * 当收到查询委托应答时回调该函数
	 * @param nSeqNo 输入参数,报文的序列号
	 * @param rspmsg 应答域,错误码为零表成功,否则表失败
	 * @param lstOrderStatus 委托状态域
	 * @param bChainFlag 输出参数,报文链标志
	 * @return >=0表示成功
	 * @return <0表示失败
	 */
	virtual int onRspTraderQryOrder(UINT4 nSeqNo,const _fldRspMsg & rspmsg,CAPIVector<_fldOrderStatus> & lstOrderStatus,BYTE bChainFlag=CHAIN_SINGLE){return 0;}
	/**
	 * 发送查询成交请求
	 * @param pSeqNo 输出参数,该报文的序列号
	 * @param matchqryreq 成交查询请求域
	 * @return 0表示成功
	 * @return 其它表示失败
	 */
	int ReqTraderQryMatch(UINT4 *pSeqNo,const _fldMatchQryReq & matchqryreq);
	/**		
	 * 当收到查询成交应答时回调该函数
	 * @param nSeqNo 输入参数,报文的序列号
	 * @param rspmsg 应答域,错误码为零表成功,否则表失败
	 * @param lstMatch 成交域
	 * @param bChainFlag 输出参数,报文链标志
	 * @return >=0表示成功
	 * @return <0表示失败
	 */
	virtual int onRspTraderQryMatch(UINT4 nSeqNo,const _fldRspMsg & rspmsg,CAPIVector<_fldMatch> & lstMatch,BYTE bChainFlag=CHAIN_SINGLE){return 0;}
	/**
	 * 发送查询客户持仓请求
	 * @param pSeqNo 输出参数,该报文的序列号
	 * @param clientposiqryreq 客户持仓查询请求域
	 * @return 0表示成功
	 * @return 其它表示失败
	 */
	int ReqTraderQryClientPosi(UINT4 *pSeqNo,const _fldClientPosiQryReq & clientposiqryreq);
	/**		
	 * 当收到查询客户持仓应答时回调该函数
	 * @param nSeqNo 输入参数,报文的序列号
	 * @param rspmsg 应答域,错误码为零表成功,否则表失败
	 * @param lstPosi 持仓域
	 * @param bChainFlag 输出参数,报文链标志
	 * @return >=0表示成功
	 * @return <0表示失败
	 */
	virtual int onRspTraderQryClientPosi(UINT4 nSeqNo,const _fldRspMsg & rspmsg,CAPIVector<_fldPosi> & lstPosi,BYTE bChainFlag=CHAIN_SINGLE){return 0;}
	/**
	 * 发送查询会员持仓请求
	 * @param pSeqNo 输出参数,该报文的序列号
	 * @param memberposiqryreq 会员持仓查询请求域
	 * @return 0表示成功
	 * @return 其它表示失败
	 */
	int ReqTraderQryMemberPosi(UINT4 *pSeqNo,const _fldMemberPosiQryReq & memberposiqryreq);
	/**		
	 * 当收到查询会员持仓应答时回调该函数
	 * @param nSeqNo 输入参数,报文的序列号
	 * @param rspmsg 应答域,错误码为零表成功,否则表失败
	 * @param lstPosi 持仓域
	 * @param bChainFlag 输出参数,报文链标志
	 * @return >=0表示成功
	 * @return <0表示失败
	 */
	virtual int onRspTraderQryMemberPosi(UINT4 nSeqNo,const _fldRspMsg & rspmsg,CAPIVector<_fldPosi> & lstPosi,BYTE bChainFlag=CHAIN_SINGLE){return 0;}
	/**
	 * 发送查询会员资金请求
	 * @param pSeqNo 输出参数,该报文的序列号
	 * @param membercapqryreq 会员资金查询请求域
	 * @return 0表示成功
	 * @return 其它表示失败
	 */
	int ReqTraderQryMemberCap(UINT4 *pSeqNo,const _fldMemberCapQryReq & membercapqryreq);
	/**		
	 * 当收到查询会员资金应答时回调该函数
	 * @param nSeqNo 输入参数,报文的序列号
	 * @param rspmsg 应答域,错误码为零表成功,否则表失败
	 * @param membercap 会员资金域
	 * @param bChainFlag 输出参数,报文链标志
	 * @return >=0表示成功
	 * @return <0表示失败
	 */
	virtual int onRspTraderQryMemberCap(UINT4 nSeqNo,const _fldRspMsg & rspmsg,const _fldMemberCap & membercap,BYTE bChainFlag=CHAIN_SINGLE){return 0;}
	/**
	 * 发送交易参数查询请求
	 * @param pSeqNo 输出参数,该报文的序列号
	 * @param tradeparamqryreq 交易参数查询请求域
	 * @return 0表示成功
	 * @return 其它表示失败
	 */
	int ReqTraderQryParam(UINT4 *pSeqNo,const _fldTradeParamQryReq & tradeparamqryreq);
	/**		
	 * 当收到交易参数查询应答时回调该函数
	 * @param nSeqNo 输入参数,报文的序列号
	 * @param rspmsg 应答域,错误码为零表成功,否则表失败
	 * @param lstTradeParam 交易参数域
	 * @param bChainFlag 输出参数,报文链标志
	 * @return >=0表示成功
	 * @return <0表示失败
	 */
	virtual int onRspTraderQryParam(UINT4 nSeqNo,const _fldRspMsg & rspmsg,CAPIVector<_fldTradeParam> & lstTradeParam,BYTE bChainFlag=CHAIN_SINGLE){return 0;}
	/**		
	 * 当收到直接市场最优行情通知时回调该函数
	 * @param nSeqNo 输入参数,报文的序列号
	 * @param bestquot 直接市场最优行情域
	 * @param bChainFlag 输出参数,报文链标志
	 * @return >=0表示成功
	 * @return <0表示失败
	 */
	virtual int onBestQuot(UINT4 nSeqNo,const _fldBestQuot & bestquot,BYTE bChainFlag=CHAIN_SINGLE){return 0;}
	/**		
	 * 当收到套利最优行情通知时回调该函数
	 * @param nSeqNo 输入参数,报文的序列号
	 * @param arbibestquot 套利最优行情域
	 * @param bChainFlag 输出参数,报文链标志
	 * @return >=0表示成功
	 * @return <0表示失败
	 */
	virtual int onArbiBestQuot(UINT4 nSeqNo,const _fldArbiBestQuot & arbibestquot,BYTE bChainFlag=CHAIN_SINGLE){return 0;}
	/**		
	 * 当收到快照行情申请时回调该函数
	 * @param nSeqNo 输入参数,报文的序列号
	 * @param quotqryreq 行情查询请求域
	 * @param bChainFlag 输出参数,报文链标志
	 * @return >=0表示成功
	 * @return <0表示失败
	 */
	virtual int onNtySnapShotQuot(UINT4 nSeqNo,const _fldQuotQryReq & quotqryreq,BYTE bChainFlag=CHAIN_SINGLE){return 0;}
	/**
	 * 发送市场状态查询请求
	 * @param pSeqNo 输出参数,该报文的序列号
	 * @param user 操作员域
	 * @return 0表示成功
	 * @return 其它表示失败
	 */
	int ReqQryMktStatus(UINT4 *pSeqNo,const _fldUser & user);
	/**		
	 * 当收到市场状态查询应答时回调该函数
	 * @param nSeqNo 输入参数,报文的序列号
	 * @param rspmsg 应答域,错误码为零表成功,否则表失败
	 * @param mktstatus 定单状态域
	 * @param bChainFlag 输出参数,报文链标志
	 * @return >=0表示成功
	 * @return <0表示失败
	 */
	virtual int onRspQryMktStatus(UINT4 nSeqNo,const _fldRspMsg & rspmsg,const _fldMktStatus & mktstatus,BYTE bChainFlag=CHAIN_SINGLE){return 0;}
	/**		
	 * 当收到市场状态通知时回调该函数
	 * @param nSeqNo 输入参数,报文的序列号
	 * @param mktstatus 市场状态域
	 * @param bChainFlag 输出参数,报文链标志
	 * @return >=0表示成功
	 * @return <0表示失败
	 */
	virtual int onNtyMktStatus(UINT4 nSeqNo,const _fldMktStatus & mktstatus,BYTE bChainFlag=CHAIN_SINGLE){return 0;}
	/**		
	 * 当收到技术控制员向席位发送通知时回调该函数
	 * @param nSeqNo 输入参数,报文的序列号
	 * @param prompttrader 席位通知域
	 * @param bChainFlag 输出参数,报文链标志
	 * @return >=0表示成功
	 * @return <0表示失败
	 */
	virtual int onNtySendNotice(UINT4 nSeqNo,const _fldPromptTrader & prompttrader,BYTE bChainFlag=CHAIN_SINGLE){return 0;}
	/**		
	 * 当收到客户实时上场通知时回调该函数
	 * @param nSeqNo 输入参数,报文的序列号
	 * @param client 客户信息域
	 * @param bChainFlag 输出参数,报文链标志
	 * @return >=0表示成功
	 * @return <0表示失败
	 */
	virtual int onNtyClientReg(UINT4 nSeqNo,const _fldClient & client,BYTE bChainFlag=CHAIN_SINGLE){return 0;}
	/**		
	 * 当收到资金持仓通知时回调该函数
	 * @param nSeqNo 输入参数,报文的序列号
	 * @param membercap 会员资金域
	 * @param lstPosi 持仓域
	 * @param bChainFlag 输出参数,报文链标志
	 * @return >=0表示成功
	 * @return <0表示失败
	 */
	virtual int onNtyMemberCapPosi(UINT4 nSeqNo,const _fldMemberCap & membercap,CAPIVector<_fldPosi> & lstPosi,BYTE bChainFlag=CHAIN_SINGLE){return 0;}
	/**
	 * 发送常量行情查询请求
	 * @param pSeqNo 输出参数,该报文的序列号
	 * @param constquotreq 常量行情查询请求域
	 * @return 0表示成功
	 * @return 其它表示失败
	 */
	int ReqConstQuot(UINT4 *pSeqNo,const _fldConstQuotReq & constquotreq);
	/**		
	 * 当收到常量行情查询应答时回调该函数
	 * @param nSeqNo 输入参数,报文的序列号
	 * @param rspmsg 应答域,错误码为零表成功,否则表失败
	 * @param constquot 常量行情域
	 * @param bChainFlag 输出参数,报文链标志
	 * @return >=0表示成功
	 * @return <0表示失败
	 */
	virtual int onRspConstQuot(UINT4 nSeqNo,const _fldRspMsg & rspmsg,const _fldConstQuot & constquot,BYTE bChainFlag=CHAIN_SINGLE){return 0;}

public:
	/**
	* 当收到不能处理的FTCP包时回调该函数
	* @param nTID 报文类型
	* @param nSeries 数据流编号
	* @param nSequenceNo 序列号
	* @param nFieldCount 数据域个数
	* @param nFieldsLen 数据域总长度(不包括报体的16个字节)
	* @param pAddr 数据域起始地址
	* @return 处理成功返回0;处理失败返回错误码,则协议栈自动销毁
	*/
	virtual int onInvalidPackage(UINT4 nTID,WORD nSeries,UINT4 nSequenceNo,WORD nFieldCount,WORD nFieldsLen,const char *pAddr){return 0;}

	/** 
	* 返回API的版本号
	* @return API版本号
	*/
	const char * Version() const{return API_VERSION;}
	/**
	* 物理连接故障时的回调函数                                             
	* @param szErrMsg 错误说明信息
	*/
	virtual void onChannelLost(const char *szErrMsg){}
public:
	CTradeAPI(void);
	virtual ~CTradeAPI(void);
	void *m_pTradeImp;
};
#endif