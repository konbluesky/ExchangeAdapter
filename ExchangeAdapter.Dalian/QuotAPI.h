#ifndef QUOT_API_H
#define QUOT_API_H

#ifdef WIN32
#ifdef API_EXPORTS
#define API_EXPORT __declspec(dllexport)
#else
#define API_EXPORT __declspec(dllimport)
#endif
#else
#define API_EXPORT
#endif

#include "APIStruct.h"
#include "APIVector.h"

#define API_VERSION "00.00.1234"//API版本号,主版本号(两个字节)+'.'+副版本号(两个字节)+'.'+内部build号(四个字节)
//TRADER_API_BASE_ERRORCODE 121010000 API错误码基值

class API_EXPORT CQuotAPI
{
public:
	/**
	*设置前置机的IP和端口
	* @param szIP 前置机的IP
	* @param nPort 前置机的端口
	*/
	void SetService(const char *szIP,int nPort);
	
	/**获取API线程实例句柄
	* @return	void* 
	*/
	void* GetApiThread();
	
	/**获取证书有效日期
	* @param	sStart		.
	* @param	sExpires		.
	* @return	int 
	*/
	int GetCertValidDate(char **sStart,char **sExpires);
	
	/**初始化API,创建消息驱动线程
	* @param isLogged 是否日志输出(true:是，false:否)
	* @param pApiThread Api线程实例
	* @return 返回0表示成功,返回-1表示失败
	*/
	int InitAPI(bool isLogged = true,void* pApiThread=NULL);

	/**初始化CA认证模块
	* @param	iCertType	证书类型
	* @param	sCertFile	证书文件路径
	* @param	sKeyFile	证书私钥文件路径
	* @param	sRootFile	根证书文件路径
	* @param	sPasswd		证书私钥密码
	* @param	bUseCAFlag是否使用证书验证
	* @return 返回0表示成功,返回-1表示失败
	*/
	int InitCA(int iCertType,const char *sCertFile,const char *sKeyFile,const	char *sRootFile,const char *sPasswd,bool bUseCAFlag=true);

	/**
	* 连接前置
	* @return 0表示成功,其它参见错误码
	*/
	int Connect();
	
	/**
	 * 交易员会话请求
	 * @param pTraderSessionReq 输入参数,交易会话请求域
	 * @param pTraderPwd 输入参数 交易席位密码
	 * @param pRspMsg 输出参数 响应域
	 * @param pTraderSessionRsp 输出参数 交易会话应答域
	 * @return 0表示成功
	 * @return 其它表示失败
	 */
	int TraderSession(const _fldTraderSessionReq * pTraderSessionReq,char *pTraderPwd,_fldRspMsg *pRspMsg,_fldTraderSessionRsp * pTraderSessionRsp);
	
	/**
	 * 交易员认证请求
	 * @param pTraderCertReq 输入参数,交易认证请求域
	 * @param pRspMsg 输出参数 响应域
	 * @param pTraderCertRsp 输出参数 交易认证应答域
	 * @return 0表示成功
	 * @return 其它表示失败
	 */
	int TraderCert(const _fldTraderCertReq * pTraderCertReq,_fldRspMsg *pRspMsg,_fldTraderCertRsp * pTraderCertRsp);
	
	/**
	* 登录前置
	* @param pReq 输入参数,登录请求包
	* @param pRspMsg 输出参数 响应域
	* @param pRsp 输出参数,认证反馈包
	* @return =0表示成功
	* @return <>0参见错误码
	*/
	int Login(const _fldTraderLoginReq * pReq,_fldRspMsg *pRspMsg,_fldTraderLoginRsp * pRsp);
	/**
	 * 发送就绪指令到前置
	 * @param	iPrivateFlow 私有流就绪标志
	 * @param	iMarketFlow	市场流就绪标志
	 * @param	bIsGetStart	是否从头开始获取数据标志
	 * @return 0表示成功
	 * @return 其它参见错误码
	 */
	int Ready(int iPrivateFlow = READY,int iMarketFlow = READY,bool bIsGetStart=false);
	/**
	 * 设置同步超时
	 * @param viSyncTimeOut同步超时，缺省120秒 
	 * @return 0表示成功
	 * @return 其它参见错误码
	 */
	int SetSyncTimeOut(int viSyncTimeOut = 120);
	
public:

	/**
	 * 发送席位密码更新请求
	 * @param pSeqNo 输出参数,该报文的序列号
	 * @param traderpwdupdreq 席位密码更新请求域
	 * @return 0表示成功
	 * @return 其它表示失败
	 */
	int ReqTraderPwdUpd(UINT4 *pSeqNo,const _fldTraderPwdUpdReq & traderpwdupdreq);
	/**		
	 * 当收到席位密码更新应答时回调该函数
	 * @param nSeqNo 输入参数,报文的序列号
	 * @param rspmsg 应答域,错误码为零表成功,否则表失败
	 * @param traderpwdupdreq 更新后的席位密码域
	 * @param bChainFlag 输出参数,报文链标志
	 * @return >=0表示成功
	 * @return <0表示失败
	 */
	virtual int onRspTraderPwdUpd(UINT4 nSeqNo,const _fldRspMsg & rspmsg,const _fldTraderPwdUpdReq & traderpwdupdreq,BYTE bChainFlag=CHAIN_SINGLE){return 0;}
	/**		
	 * 当收到快照行情申请时回调该函数
	 * @param nSeqNo 输入参数,报文的序列号
	 * @param quotqryreq 行情查询请求域
	 * @param bChainFlag 输出参数,报文链标志
	 * @return >=0表示成功
	 * @return <0表示失败
	 */
	virtual int onNtySnapShotQuot(UINT4 nSeqNo,const _fldQuotQryReq & quotqryreq,BYTE bChainFlag=CHAIN_SINGLE){return 0;}
	/**
	 * 发送行情用户退出请求
	 * @param pSeqNo 输出参数,该报文的序列号
	 * @param traderlogoutreq 交易员退出请求域
	 * @return 0表示成功
	 * @return 其它表示失败
	 */
	int ReqQuotUserLogout(UINT4 *pSeqNo,const _fldTraderLogoutReq & traderlogoutreq);
	/**		
	 * 当收到行情用户退出应答时回调该函数
	 * @param nSeqNo 输入参数,报文的序列号
	 * @param rspmsg 应答域,错误码为零表成功,否则表失败
	 * @param traderlogoutrsp 交易员退出请求域
	 * @param bChainFlag 输出参数,报文链标志
	 * @return >=0表示成功
	 * @return <0表示失败
	 */
	virtual int onRspQuotUserLogout(UINT4 nSeqNo,const _fldRspMsg & rspmsg,const _fldTraderLogoutRsp & traderlogoutrsp,BYTE bChainFlag=CHAIN_SINGLE){return 0;}
	/**
	 * 发送查询行情订阅权限请求
	 * @param pSeqNo 输出参数,该报文的序列号
	 * @param quotsubsrightqryreq 行情订阅权限查询请求域
	 * @return 0表示成功
	 * @return 其它表示失败
	 */
	int ReqQryQuotRight(UINT4 *pSeqNo,const _fldQuotSubsRightQryReq & quotsubsrightqryreq);
	/**		
	 * 当收到查询行情订阅权限应答时回调该函数
	 * @param nSeqNo 输入参数,报文的序列号
	 * @param rspmsg 应答域,错误码为零表成功,否则表失败
	 * @param lstQuotSubsRight 行情订阅权限域
	 * @param bChainFlag 输出参数,报文链标志
	 * @return >=0表示成功
	 * @return <0表示失败
	 */
	virtual int onRspQryQuotRight(UINT4 nSeqNo,const _fldRspMsg & rspmsg,CAPIVector<_fldQuotSubsRight> & lstQuotSubsRight,BYTE bChainFlag=CHAIN_SINGLE){return 0;}
	/**
	 * 发送更新行情订阅权限请求
	 * @param pSeqNo 输出参数,该报文的序列号
	 * @param quotsubsright 行情订阅权限域
	 * @return 0表示成功
	 * @return 其它表示失败
	 */
	int ReqUpdQuotRight(UINT4 *pSeqNo,const _fldQuotSubsRight & quotsubsright);
	/**		
	 * 当收到更新行情订阅权限应答时回调该函数
	 * @param nSeqNo 输入参数,报文的序列号
	 * @param rspmsg 应答域,错误码为零表成功,否则表失败
	 * @param quotsubsright 更新后的行情订阅权限域
	 * @param bChainFlag 输出参数,报文链标志
	 * @return >=0表示成功
	 * @return <0表示失败
	 */
	virtual int onRspUpdQuotRight(UINT4 nSeqNo,const _fldRspMsg & rspmsg,const _fldQuotSubsRight & quotsubsright,BYTE bChainFlag=CHAIN_SINGLE){return 0;}
	/**		
	 * 当收到行情服务的深度行情通知时回调该函数
	 * @param nSeqNo 输入参数,报文的序列号
	 * @param bestquot 直接市场最优行情域
	 * @param lstOptPara 期权参数
	 * @param lstMBLQuot 直接市场深度行情域
	 * @param bChainFlag 输出参数,报文链标志
	 * @return >=0表示成功
	 * @return <0表示失败
	 */
	virtual int onMarketDataMBLQuot(UINT4 nSeqNo,const _fldBestQuot & bestquot,CAPIVector<_fldOptPara> & lstOptPara,CAPIVector<_fldMBLQuot> & lstMBLQuot,BYTE bChainFlag=CHAIN_SINGLE){return 0;}
	/**		
	 * 当收到行情服务的套利深度行情通知时回调该函数
	 * @param nSeqNo 输入参数,报文的序列号
	 * @param arbibestquot 套利最优行情域
	 * @param lstMBLQuot 套利深度行情域
	 * @param bChainFlag 输出参数,报文链标志
	 * @return >=0表示成功
	 * @return <0表示失败
	 */
	virtual int onMarketDataArbiMBLQuot(UINT4 nSeqNo,const _fldArbiBestQuot & arbibestquot,CAPIVector<_fldMBLQuot> & lstMBLQuot,BYTE bChainFlag=CHAIN_SINGLE){return 0;}
	/**
	 * 发送行情席位密码更新请求
	 * @param pSeqNo 输出参数,该报文的序列号
	 * @param traderpwdupdreq 行情席位密码更新请求域
	 * @return 0表示成功
	 * @return 其它表示失败
	 */
	int ReqQuotTraderPwdUpd(UINT4 *pSeqNo,const _fldTraderPwdUpdReq & traderpwdupdreq);
	/**		
	 * 当收到行情席位密码更新应答时回调该函数
	 * @param nSeqNo 输入参数,报文的序列号
	 * @param rspmsg 应答域,错误码为零表成功,否则表失败
	 * @param traderpwdupdreq 更新后的行情席位密码域
	 * @param bChainFlag 输出参数,报文链标志
	 * @return >=0表示成功
	 * @return <0表示失败
	 */
	virtual int onRspQuotTraderPwdUpd(UINT4 nSeqNo,const _fldRspMsg & rspmsg,const _fldTraderPwdUpdReq & traderpwdupdreq,BYTE bChainFlag=CHAIN_SINGLE){return 0;}
	/**
	 * 发送历史行情查询请求
	 * @param pSeqNo 输出参数,该报文的序列号
	 * @param qryhisquotreq 历史行情查询请求域
	 * @return 0表示成功
	 * @return 其它表示失败
	 */
	int ReqQueryHistoryQuot(UINT4 *pSeqNo,const _fldQryHisQuotReq & qryhisquotreq);
	/**		
	 * 当收到历史行情查询应答时回调该函数
	 * @param nSeqNo 输入参数,报文的序列号
	 * @param rspmsg 应答域,错误码为零表成功,否则表失败
	 * @param bestquot 直接市场最优行情域
	 * @param lstOptPara 期权参数
	 * @param lstMBLQuot 直接市场深度行情域
	 * @param bChainFlag 输出参数,报文链标志
	 * @return >=0表示成功
	 * @return <0表示失败
	 */
	virtual int onRspQueryHistoryQuot(UINT4 nSeqNo,const _fldRspMsg & rspmsg,const _fldBestQuot & bestquot,CAPIVector<_fldOptPara> & lstOptPara,CAPIVector<_fldMBLQuot> & lstMBLQuot,BYTE bChainFlag=CHAIN_SINGLE){return 0;}
	/**		
	 * 当收到数据服务盘后通知时回调该函数
	 * @param nSeqNo 输入参数,报文的序列号
	 * @param mktdatanotice 数据服务通知域
	 * @param bChainFlag 输出参数,报文链标志
	 * @return >=0表示成功
	 * @return <0表示失败
	 */
	virtual int onNtyCloseMktNotice(UINT4 nSeqNo,const _fldMktDataNotice & mktdatanotice,BYTE bChainFlag=CHAIN_SINGLE){return 0;}

public:
	/**
	* 当收到不能处理的FTCP包时回调该函数
	* @param nTID 报文类型
	* @param nSeries 数据流编号
	* @param nSequenceNo 序列号
	* @param nFieldCount 数据域个数
	* @param nFieldsLen 数据域总长度(不包括报体的16个字节)
	* @param pAddr 数据域起始地址
	* @return 处理成功返回0;处理失败返回错误码,则协议栈自动销毁
	*/
	virtual int onInvalidPackage(UINT4 nTID,WORD nSeries,UINT4 nSequenceNo,WORD nFieldCount,WORD nFieldsLen,const char *pAddr){return 0;}

	/** 
	* 返回API的版本号
	* @return API版本号
	*/
	const char * Version() const{return API_VERSION;}
	/**
	* 物理连接故障时的回调函数                                             
	* @param szErrMsg 错误说明信息
	*/
	virtual void onChannelLost(const char *szErrMsg){}
public:
	CQuotAPI(void);
	virtual ~CQuotAPI(void);
	void *m_pQuotImp;
};
#endif