#ifndef __FIX_DATA_CONVERT_HELPER_H__
#define __FIX_DATA_CONVERT_HELPER_H__

#include "ExchangeAdapterBase.h"
#include "RMMSCommon.h"
#include "ChinaQuoteDefinition.h"
#include "ChinaOrderDefinition.h"

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4100)
#pragma warning(disable:4512)
#pragma warning(disable:4125)
#pragma warning(disable:4251)
#else
#error "仅支持VC,以待修改"
#endif

#include "quickfix/Message.h"

class EXAPI FixHelper
{
public:
	static bool SetCurrentVer(RMMS_NS::RMMSFixVer ver);
	static RMMS_NS::RMMSFixVer GetCurrentVer(void);

	static FIX::MsgType Type(const FIX::Message &message)
	{
		FIX::MsgType msgType;
		message.getHeader().getField(msgType);
		return msgType;
	}

	static bool EqualCurVer(const FIX::Message &msg)
	{
		return curver == msg.getHeader().getField(FIX::FIELD::BeginString);
	}
	
private:
	static tstring curver;
	static RMMS_NS::RMMSFixVer rmmscurver;
};

class EXAPI FixDataConvertHelper
{
/* 错误报告 */
public:
	enum Error {
		ERROR_VERSION = -1,	/* 不支持的版本 */
		ERROR_NOFIELD = -2,	/* 字段不存在 */
		ERROR_VALUE = -3,	/* 字段属性不匹配 */
	};

/* 报价转换 */
public:
	static void InternalToFix(RMMS_NS::Exchange exch,const Quote &quote,
		FIX::Message *pmsg);
	static void InternalToFix(RMMS_NS::Exchange exch,const DoM &dom,
		FIX::Message *pmsg);

/* 交易转换 */
public:
	static void InternalToFix(RMMS_NS::Exchange exch,
		const ExecutionReport &exereport,FIX::Message *pmsg);
	static void InternalToFix(RMMS_NS::Exchange exch,
		const FillReport& fillreport,FIX::Message *pmsg);
	static void InternalToFix(RMMS_NS::Exchange exch,
		const BusinessReject &bizreject,FIX::Message *pmsg);
	static void InternalToFix(RMMS_NS::Exchange exch,
		const OrderCancelReject &cancelreject,FIX::Message *pmsg);
	static void InternalToFix(RMMS_NS::Exchange exch,
		const OrderCancelReplaceReject &cancelreplacereject,FIX::Message *pmsg);
	static int32 FixToInternal(RMMS_NS::Exchange exch,const FIX::Message &msg,
		Order *porder);
	static int32 FixToInternal(RMMS_NS::Exchange exch,const FIX::Message &msg,
		OrderCancelRequest *pcancel);
};

#ifdef _MSC_VER
#pragma warning(pop)
#else
#error "仅支持VC,以待修改"
#endif

#endif