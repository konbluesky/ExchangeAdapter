#ifndef __DL_LIB_H__
#define __DL_LIB_H__

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4100)
#pragma warning(disable:4512)
#else
#error "仅支持VC"
#endif

#include <cstdlib>
#include "QuotAPI.h"
#include "TradeAPI.h"



#ifdef _MSC_VER
#pragma warning(pop)
#else
#error "仅支持VC"
#endif

#endif