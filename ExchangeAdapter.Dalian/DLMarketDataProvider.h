#ifndef __DL_MARKET_DATA_PROVIDER_H__
#define __DL_MARKET_DATA_PROVIDER_H__

#include "ExchangeAdapterBase.h"
#include "DLLib.h"
#include "IFixMarket.h"
#include "QuotAdapter.h"
#include "autoptr.h"

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4251)
#pragma warning(disable:4275)
#else
#error "仅支持VC"
#endif

struct _fldOptPara;
struct _fldBestQuot;
struct _fldMBLQuot;
class CQuotAdapter;

namespace exchangeadapter_dalian
{
class EXAPI CDLMarketDataProvider : public IFixMarketProvider {
public:
	CDLMarketDataProvider(void);
	virtual ~CDLMarketDataProvider();

public:
	struct InitParam {
		tstring sFlowPath;
		tstring sAddressIp;
		int32 iPort;
		tstring sParticipantID;
		tstring sUserID;
		tstring sPassword;
		bool bWriteLog;
		static std::pair<bool,InitParam> LoadConfig(
			const tstring& fname);
	};
	virtual tstring GetName();
	virtual bool Initialize(void* param);
	virtual bool InitializeByXML(const tchar* configfile);
	virtual void UnInitialize();
	virtual bool Login(void*);
	virtual bool Logout(void*);
	virtual bool SupportDoM();
	int32 OnAutoConn();

	/* 内部使用 */
	void onChannelLost(const tchar *szErrMsg);
	void OnRspMsg(const FIX::Message &msg);

protected:
	bool InitPara();
	bool InitAPI();
	bool Connect();
	
private:
	void* m_pApiThread;
	int32 m_dwSeqNo;
	bool m_bActive;

	InitParam m_param;
	boost::shared_ptr<CQuotAdapter> m_pcsQutotAPI;
};
}

#ifdef _MSC_VER
#pragma warning(pop)
#else
#error "仅支持VC"
#endif

#endif