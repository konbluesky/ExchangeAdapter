#ifndef __DL_TRADER_ADAPTER_H
#define __DL_TRADER_ADAPTER_H

#include "ExchangeAdapterBase.h"
#include "DLLib.h"
#include "IFixTrader.h"
#include "DLDataConvertHelper.h"
#include "TradeAdapter.h"
#include "autoptr.h"

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4251)
#pragma warning(disable:4275)
#else
#error "仅支持VC"
#endif

namespace exchangeadapter_dalian
{

class EXAPI CDLTraderAdapter : public IFixTraderProvider
{
public:
	CDLTraderAdapter(void);
	virtual ~CDLTraderAdapter(void);

public:
	struct InitParam
	{
		tstring sFlowPath;
		tstring sAddressIp;
		int32 iPort;
		tstring sParticipantID;
		tstring sUserID;
		tstring sPassword;
		bool bWriteLog;
		static std::pair<bool,InitParam> LoadConfig(const tstring& fname);	
	};
	std::string GetName(){return "DLTraderAPI";}

	virtual bool Initialize(void* param);
	virtual bool InitializeByXML(const tchar* configfile);
	virtual void UnInitialize();
	virtual bool Login(void*);
	virtual bool Logout(void*);
	int32 OnAutoConn();
	virtual void RequestFix(FIX::Message &msg);

	void ReqeustOrderAdd(const Order&  order);
	void RequestOrderCancel(const OrderCancelRequest&  order);
	void RequestOrderModify(const OrderCancelReplaceRequest&  order);
	
	/* 收到前置应答,只有失败的时候翻译,其他丢掉 */
	virtual void OnRplTraderInsertOrders(
		uint32 nSeqNo,
		const _fldRspMsg & rspmsg,
		CAPIVector<_fldOrder> & lstOrder,
		uint8 bChainFlag/*=CHAIN_SINGLE*/);

	/* 交易员定单应答,只有失败的时候翻译,其他丢掉 */
	virtual void OnRspTraderInsertOrders(
		uint32 nSeqNo,
		const _fldRspMsg & rspmsg,
		CAPIVector<_fldOrder> & lstOrder,
		uint8 bChainFlag/*=CHAIN_SINGLE*/);

	/* 收到状态修改,对应Execreport */
	virtual void OnTraderOrdersConfirmation(
	   uint32 nSeqNo,
	   const _fldOrderStatus & orderstatus,
	   uint8 bChainFlag/*=CHAIN_SINGLE*/);

	/* 成交 */
	virtual void OnNtyTraderMatch(
		uint32 nSeqNo,
		const _fldMatch & match,
		uint8 bChainFlag/*=CHAIN_SINGLE*/);

	virtual void OnRspTraderCancelOrder(
		uint32 nSeqNo,
		const _fldRspMsg & rspmsg,
		const _fldOrderAction & orderaction,
		uint8 bChainFlag/*=CHAIN_SINGLE*/);

	virtual void OnRplTraderCancelOrder(
		uint32 nSeqNo,
		const _fldRspMsg & rspmsg,
		const _fldOrderAction & orderaction,
		uint8 bChainFlag/*=CHAIN_SINGLE*/);

	virtual void OnChannelLost(const tchar *szErrMsg);



	//------------------------用于查询------------------

	int32 ReqTraderQryOrder(uint32 *pSeqNo,const _fldOrderQryReq & orderqryreq);
	virtual int32 onRspTraderQryOrder(
		uint32 nSeqNo,const _fldRspMsg & rspmsg,
		CAPIVector<_fldOrderStatus> & lstOrderStatus,uint8 bChainFlag);
	
private:
	bool InitPara();
	bool InitAPI();
	bool Connect();
	
		
private:
	void* m_pApiThread;
	// 后面需要看看如何去掉,按理说,只要发送成功,就应该自动增加一次
	// 
	int32 m_dwSeqNo;
	bool m_bActive;
	bool m_bLogin;

	InitParam m_param;
	boost::shared_ptr<CTradeAdapter>  m_pcsTradeAPI;
};

}

#ifdef _MSC_VER
#pragma warning(pop)
#else
#error "仅支持VC"
#endif

#endif