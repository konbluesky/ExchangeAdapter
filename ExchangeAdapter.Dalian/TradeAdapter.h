#ifndef TRADER_ADAPTER_H
#define TRADER_ADAPTER_H

#include "DLLib.h"
#include "DLDataConvertHelper.h"

namespace exchangeadapter_dalian
{
class CDLTraderAdapter;

class CTradeAdapter : public CTradeAPI
{
public:
	CTradeAdapter(CDLTraderAdapter *pTraderAdapter);
	virtual ~CTradeAdapter(void);
public:
	virtual int onRspTraderLogout(
		uint32 nSeqNo,
		const _fldRspMsg & rspmsg,
		const _fldTraderLogoutRsp & traderlogoutrsp,
		uint8 bChainFlag=CHAIN_SINGLE);
	virtual int onRspTraderPwdUpd(
		uint32 nSeqNo,
		const _fldRspMsg & rspmsg,
		const _fldTraderPwdUpdReq & traderpwdupdreq,
		uint8 bChainFlag=CHAIN_SINGLE);
	virtual int onRspTraderInsertOrders(
		uint32 nSeqNo,
		const _fldRspMsg & rspmsg,
		CAPIVector<_fldOrder> & lstOrder,
		uint8 bChainFlag=CHAIN_SINGLE);
	virtual int onRplTraderInsertOrders(
		uint32 nSeqNo,
		const _fldRspMsg & rspmsg,
		CAPIVector<_fldOrder> & lstOrder,
		uint8 bChainFlag=CHAIN_SINGLE);
	virtual int onTraderOrdersConfirmation(
		uint32 nSeqNo,
		const _fldOrderStatus & orderstatus,
		uint8 bChainFlag=CHAIN_SINGLE);
	virtual int onNtyTraderMatch(
		uint32 nSeqNo,
		const _fldMatch & match,
		uint8 bChainFlag=CHAIN_SINGLE);
	virtual int onRspTraderCancelOrder(
		uint32 nSeqNo,
		const _fldRspMsg & rspmsg,
		const _fldOrderAction & orderaction,
		uint8 bChainFlag=CHAIN_SINGLE);
	virtual int onRplTraderCancelOrder(
		uint32 nSeqNo,
		const _fldRspMsg & rspmsg,
		const _fldOrderAction & orderaction,
		uint8 bChainFlag=CHAIN_SINGLE);
	virtual int onRspTraderOptExec(
		uint32 nSeqNo,
		const _fldRspMsg & rspmsg,
		const _fldOrder & order,
		uint8 bChainFlag=CHAIN_SINGLE);
	virtual int onRspTraderCancelOptExec(
		uint32 nSeqNo,
		const _fldRspMsg & rspmsg,
		const _fldOrder & order,
		uint8 bChainFlag=CHAIN_SINGLE);
	virtual int onRspTraderQryOptExec(
		uint32 nSeqNo,
		const _fldRspMsg & rspmsg,
		CAPIVector<_fldOrderStatus> & lstOrderStatus,
		uint8 bChainFlag=CHAIN_SINGLE);
	virtual int onRspTraderQryOrder(
		uint32 nSeqNo,
		const _fldRspMsg & rspmsg,
		CAPIVector<_fldOrderStatus> & lstOrderStatus,
		uint8 bChainFlag=CHAIN_SINGLE);
	virtual int onRspTraderQryMatch(
		uint32 nSeqNo,
		const _fldRspMsg & rspmsg,
		CAPIVector<_fldMatch> & lstMatch,
		uint8 bChainFlag=CHAIN_SINGLE);
	virtual int onRspTraderQryClientPosi(
		uint32 nSeqNo,
		const _fldRspMsg & rspmsg,
		CAPIVector<_fldPosi> & lstPosi,
		uint8 bChainFlag=CHAIN_SINGLE);
	virtual int onRspTraderQryMemberPosi(
		uint32 nSeqNo,
		const _fldRspMsg & rspmsg,
		CAPIVector<_fldPosi> & lstPosi,
		uint8 bChainFlag=CHAIN_SINGLE);
	virtual int onRspTraderQryMemberCap(
		uint32 nSeqNo,
		const _fldRspMsg & rspmsg,
		const _fldMemberCap & membercap,
		uint8 bChainFlag=CHAIN_SINGLE);
	virtual int onRspTraderQryParam(
		uint32 nSeqNo,
		const _fldRspMsg & rspmsg,
		CAPIVector<_fldTradeParam> & lstTradeParam,
		uint8 bChainFlag=CHAIN_SINGLE);
	virtual int onBestQuot(
		uint32 nSeqNo,
		const _fldBestQuot & bestquot,
		uint8 bChainFlag=CHAIN_SINGLE);
	virtual int onArbiBestQuot(
		uint32 nSeqNo,
		const _fldArbiBestQuot & arbibestquot,
		uint8 bChainFlag=CHAIN_SINGLE);
	virtual int onNtySnapShotQuot(
		uint32 nSeqNo,
		const _fldQuotQryReq & quotqryreq,
		uint8 bChainFlag=CHAIN_SINGLE);
	virtual int onRspQryMktStatus(
		uint32 nSeqNo,
		const _fldRspMsg & rspmsg,
		const _fldMktStatus & mktstatus,uint8 bChainFlag=CHAIN_SINGLE);
	virtual int onNtyMktStatus(
		uint32 nSeqNo,
		const _fldMktStatus & mktstatus,
		uint8 bChainFlag=CHAIN_SINGLE);
	virtual int onNtySendNotice(
		uint32 nSeqNo,
		const _fldPromptTrader & prompttrader,
		uint8 bChainFlag=CHAIN_SINGLE);
	virtual int onNtyClientReg(
		uint32 nSeqNo,
		const _fldClient & client,
		uint8 bChainFlag=CHAIN_SINGLE);
	virtual int onNtyMemberCapPosi(
		uint32 nSeqNo,
		const _fldMemberCap & membercap,
		CAPIVector<_fldPosi> & lstPosi,
		uint8 bChainFlag=CHAIN_SINGLE);
	virtual int onRspConstQuot(
		uint32 nSeqNo,
		const _fldRspMsg & rspmsg,
		const _fldConstQuot & constquot,
		uint8 bChainFlag=CHAIN_SINGLE);
	virtual int onInvalidPackage(
		uint32 nTID,uint16 nSeries,
		uint32 nSequenceNo,uint16 nFieldCount,
		uint16 nFieldsLen,const tchar *pAddr);
	virtual void onChannelLost(const tchar *szErrMsg);
private:
	CDLTraderAdapter *m_pTraderAdapter;
};
}

#endif