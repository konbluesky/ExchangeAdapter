#ifndef __UDLPRICE_HELPER_H__
#define __UDLPRICE_HELPER_H__

namespace exchangeadapter_dalian
{
	class UDLPriceHelper
	{
	
	public:
#define MAXDOUBLE 1000000000000
		static double Price(double price)
		{
			//注：在中金的交易系统里，如果没有报价信息，它会使用一个最大的double的值来表示，
			//但double在不同平台下，它的精度是不一样的(double在不同体系结构里的内存表示方式是不一样的)
			//dom.AskLevel[3] = marketData->AskPrice4 == std::numeric_limits<double>::max() ? 0 : marketData->AskPrice4;
			return price > MAXDOUBLE ? 0 : price;
		}


	private:
		UDLPriceHelper(void);
		~UDLPriceHelper(void);
	};
}

#endif