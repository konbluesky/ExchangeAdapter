/* ------------------------------------------------------------------
 * filename:    _s_types.h
 * version:     1.0.0.0
 * created:     2005-9-12 9:37
 * author:		shigw
 * purpose:		用于内部的beauty类型定义
 * ------------------------------------------------------------------ 
 * Date:
 * ------------------------------------------------------------------ 
*/

#ifndef __S_TYPES_H__
#define __S_TYPES_H__


#define _IN_
#define _OUT_
#define _INOUT_
#define ___________________

/* -----------------------------------------------------------------------
* 调试开关
* -----------------------------------------------------------------------
*/
#define S_DEBUG

#ifdef S_DEBUG
#define dbgprintf printf
#else
#define dbgprintf (void)
#endif

#define _PUTS(c,d,a,b)	a##_##b##_put(c,d->b,(unsigned int)strlen(d->b));
#define _PUTB(c,d,a,b,e)	a##_##b##_put(c,d->b,e);
#define _PUTD(c,d,a,b)	a##_##b##_put(c,d->b);

#define _GETS(c,d,a,b) a##_##b##_get(c,d->b,sizeof(d->b))
#define _GETB(c,d,a,b,e) a##_##b##_get(c,0,d->b,sizeof(d->b),(uint4*)&e)
#define _GETD(c,d,a,b) a##_##b##_get(c,&d->b);


#endif
