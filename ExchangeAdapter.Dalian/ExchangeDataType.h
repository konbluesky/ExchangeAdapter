//**********************************************************************
//系统名：大连期货交易所v60信息系统
//公司名：
//文件名： ExchangeDateType.h
//主要功能:定义了基本数据类型和衍生数据类型
//修改历史：
//20051213	何  荣		创建文件
//**********************************************************************
#ifndef EXCHANGE_DATA_TYPE_H
#define EXCHANGE_DATA_TYPE_H
#include "ByteArray.h"
						  
#ifndef __FTCP__STDTYPE__
#define __FTCP__STDTYPE__
typedef unsigned char     BYTE;
typedef unsigned short    WORD;
typedef unsigned long     DWORD;
typedef char              INT1;
typedef short             INT2;
typedef int               INT4;
typedef unsigned int      UINT4;
typedef float             REAL4;
typedef double            REAL8;
#endif

typedef INT1                 TActionFlag;              /*报单操作类型码*/
typedef INT1                 TBoolFlag;                /*布尔类型*/
typedef INT1                 TClientProperty;          /*客户性质*/
typedef BYTEARRAY<20>        TComeFrom;                /*消息来源*/
typedef BYTEARRAY<3>         TCurrency;                /*交易用的货币*/
typedef INT2                 TDataFlowFlag;            /*数据流名称*/
typedef BYTEARRAY<19>        TDateTime;                /*yyyy-mm-dd hh:mi:ss*/
typedef BYTEARRAY<6>         TFusePoint;               /*在该时间点后不可熔断*/
typedef BYTEARRAY<100>       TForceExitReason;         /*强制退出原因*/
typedef INT1                 TContractType;            /*合约类型,TBC*/
typedef INT1                 TContractVersion;         /*合约版本号,TBC*/
typedef BYTEARRAY<10>        TMktID;                   /*市场编码,TBC*/
typedef INT1                 TMatchSession;            /*报单成交时间,TBC*/
typedef BYTEARRAY<6>         TMsgRef;                  /*用户自定义数据,TBC*/
typedef BYTEARRAY<2>         TNewsType;                /*消息类型*/
typedef INT1                 TNewsUrgency;             /*紧急程度*/
typedef INT1                 TMemberType;              /*会员类型*/
typedef INT1                 TPubStyle;                /*竞价阶段合约行情发布方式*/
typedef INT1                 TStopCode;                /*停止交易原因*/
typedef INT1                 TTradeRight;              /*交易权限*/
typedef BYTEARRAY<200>       TURLLink;                 /*此消息的WEB链结*/
typedef INT1                 TQtyType;                 /*数量类型*/
typedef REAL8                TAmt;                     /*金额*/
typedef BYTEARRAY<20>        TAppName;                 /*应用名称*/
typedef BYTEARRAY<20>        TAppSerial;               /*应用序列号*/
typedef BYTEARRAY<10>        TAppVersion;              /*应用版本号*/
typedef BYTEARRAY<10>        TArbiCode;                /*策略代码*/
typedef BYTE                 TBsFlag;                  /*买卖标志*/
typedef INT1                 TChar;                    /*单字节整数,TBC*/
typedef BYTEARRAY<14>        TClearAccountNo;          /*结算帐号*/
typedef BYTEARRAY<12>        TClearMemberID;           /*结算会员代码*/
typedef BYTEARRAY<16>        TClientID;                /*客户号*/
typedef BYTEARRAY<80>        TClientName;              /*客户名称*/
typedef BYTEARRAY<4000>      TContent;                 /*内容*/
typedef BYTE                 TContractAddType;         /*合约增加方式*/
typedef BYTEARRAY<40>        TContractName;            /*合约名称*/
typedef BYTE                 TContractStatus;          /*合约状态*/
typedef BYTE                 TContractTradeState;      /*合约交易状态*/
typedef BYTE                 TCpFlag;                  /*看涨看跌标志*/
typedef BYTEARRAY<8>         TDate;                    /*日期,yyyymmdd*/
typedef BYTE                 TDirection;               /*多空方向*/
typedef BYTE                 TEoFlag;                  /*开平标志*/
typedef BYTEARRAY<6>         TExchCode;                /*交易所代码*/
typedef BYTE                 TExchState;               /*交易所状态*/
typedef BYTE                 TFlag;                    /*标志,TBC*/
typedef BYTE                 TForceOffsetReason;       /*强平原因*/
typedef BYTEARRAY<40>        TFtrArbiContractID;       /*期货套利合约号*/
typedef BYTEARRAY<8>         TFtrContractID;           /*期货合约号*/
typedef BYTE                 TGender;                  /*性别*/
typedef UINT4                TInt;                     /*整型*/
typedef BYTEARRAY<15>        TIP;                      /*IP地址,***.***.***.****/
typedef BYTE                 TLogoutFlag;              /*注销标志*/
typedef UINT4                TMatchNo;                 /*成交号*/
typedef BYTEARRAY<8>         TMemberID;                /*交易会员代码*/
typedef BYTEARRAY<80>        TMemberName;              /*会员名称*/
typedef REAL8                TMoney;                   /*货币类型*/
typedef BYTEARRAY<6>         TMonth;                   /*年月,yyyymm*/
typedef BYTEARRAY<80>        TName;                    /*名称*/
typedef BYTEARRAY<300>       TNote;                    /*备注*/
typedef UINT4                TNumber;                  /*数字型*/
typedef BYTEARRAY<10>        TOperatorCode;            /*操作员代码*/
typedef BYTEARRAY<80>        TOptArbiContractID;       /*期权套利合约号*/
typedef BYTEARRAY<20>        TOptContractID;           /*期权合约号*/
typedef UINT4                TOrderNo;                 /*委托号*/
typedef BYTE                 TOrderAttr;               /*定单属性*/
typedef BYTE                 TOrderSort;               /*定单类别*/
typedef BYTE                 TOrderSrc;                /*定单来源*/
typedef BYTE                 TOrderStatus;             /*委托状态*/
typedef BYTE                 TOrderType;               /*定单类型*/
typedef BYTEARRAY<20>        TPersonName;              /*姓名*/
typedef REAL8                TPrice;                   /*价格*/
typedef BYTEARRAY<15>        TPromObject;              /*发布对象*/
typedef BYTEARRAY<40>        TPwd;                     /*密码*/
typedef UINT4                TQty;                     /*数量*/
typedef BYTE                 TQuotFlag;                /*行情类型*/
typedef REAL8                TRate;                    /*比率*/
typedef BYTEARRAY<40>        TRspMsg;                  /*响应信息*/
typedef BYTEARRAY<2>         TTraderFuncType;          /*席位的功能类型*/
typedef BYTEARRAY<2>         TTraderPosType;           /*席位的位置类型*/
typedef BYTE                 TShFlag;                  /*投保标志*/
typedef INT2                 TShort;                   /*短整形*/
typedef BYTEARRAY<2>         TShortCut;                /*快捷输入*/
typedef BYTEARRAY<3000>      TSign;                    /*CA签名*/
typedef BYTEARRAY<1000>      TSignRsp;                 /*签名反馈*/
typedef BYTE                 TStatus;                  /*状态,TBC*/
typedef BYTEARRAY<256>       TSymmetricalKey;          /*对称密钥*/
typedef UINT4                TTID;                     /*ID*/
typedef BYTEARRAY<12>        TTime;                    /*时间,HH24:MI:SS:MMM*/
typedef BYTEARRAY<16>        TTraderNo;                /*交易员号*/
typedef BYTE                 TTradeType;               /*交易类型*/
typedef BYTEARRAY<2>         TType;                    /*类型,TBC*/
typedef BYTEARRAY<80>        TVarchar;                 /*字符型(长)*/
typedef BYTEARRAY<4>         TVarietyID;               /*品种代码*/
typedef BYTEARRAY<20>        TVarietyName;             /*品种名称*/
typedef BYTE                 TVarietyState;            /*品种状态*/
typedef INT4                 TChgQty;                  /*变化数量*/
typedef INT4                 TINT4;                    /*整型*/
typedef BYTEARRAY<2048>      TCACert;                  /*CA证书*/
typedef BYTEARRAY<128>       TSignature;               /*签名*/
typedef BYTEARRAY<2>         TMktStatus;               /*市场状态*/

//总共100个
#endif