#ifndef __DL_DATACONVERT_H__
#define __DL_DATACONVERT_H__

#include "DLLib.h"
#include "ChinaOrderDefinition.h"
#include "ChinaQuoteDefinition.h"

#define _XML_GET_VALUE(node,nodename)	\
	node->FirstChild(nodename)->FirstChild()->Value()

#define _STR_TO_LOWER(x)	\
	std::transform(x.begin(),x.end(),x.begin(),std::tolower);

#define XML_GET_STRING(node,nodename,nodevalue)	\
	nodevalue = _XML_GET_VALUE(node,nodename);

#define XML_GET_INT(node,nodename,nodevalue)	\
	std::sscanf(_XML_GET_VALUE(node,nodename),"%d",&nodevalue);

#define XML_GET_UINT(node,nodename,nodevalue)	\
	std::sscanf(_XML_GET_VALUE(node,nodename),"%u",&nodevalue);

#define XML_GET_BOOL(node,nodename,nodevalue)	\
{											\
	tstring str = _XML_GET_VALUE(node,nodename);	\
	_STR_TO_LOWER(str);						\
	nodevalue = "true" == str ? true:false;	\
}

namespace exchangeadapter_dalian
{
class UDLDataConvert
{

/*****************************************************************************/
/* 行情数据转换
/*****************************************************************************/
public:
	/* 报价转换 */	
	static int32 TranslateQuoteDataToPublicData(
		const _fldBestQuot & bestquot,Quote* pquote);

	/* 深度市场数据 */
	static int32 TranslateDomDataToPublicData(
		CAPIVector<_fldMBLQuot> & lstMBLQuot,
		const _fldBestQuot &bestquot,DoM* pdom);

/*****************************************************************************/
/* 交易数据转换
/*****************************************************************************/
public:

	/* 下单请求 */
	static int32 TranslatePublicDataToOrderData(
		const Order & clientOrder,_fldOrder* pApiOrder,
		const tstring &sParticipantID,const tstring &sUserID);

	/* 下单拒绝 */
	static int32 TraslateRejectExecutionReportToPublicData(
		const _fldRspMsg &rspmsg,_fldOrder & lstOrder,
		BusinessReject *pClientReject,int32 requestID);

	/* 下单成功 */
	static int32 TranRspNewOrderSingleToPublicData(const _fldOrder &orderInfo,
		ExecutionReport * pRspNewOrderSig);

	/* 成交成功 */
	static int32 TranslateOrderExcutionToPublicData(
		const _fldMatch &orderExcution,FillReport * pfillreport);

	/* 撤销单请求 */
	static int32 TranslatePublicDataToCancelOrderData(
		const OrderCancelRequest & clientOrderCancel,
		_fldOrderAction* pApiOrderCancel,const tstring &sParticipantID,
		const tstring &sUserID);

	/* 撤销单拒绝 */
	static int32 TraslateRejectExecutionReportToPublicData(
		const _fldRspMsg &rspmsg,const _fldOrderAction & lstOrder,
		OrderCancelReject *pCancelReject,int32 requestID);

	/* 撤单成功 */
	static int32 TranCancelOrderSuccToPublicData(
		const _fldOrderAction & orderaction,
		ExecutionReport* pclientOrderCancel);
	
	static bool GetUTCTime(const tchar *cYMD,const tchar *cHMSm,
		LIB_NS::DateTime *dt);
	static void IDConvet(uint32 num,tstring *pstr);
	static void IDConvet(int32 num,tstring *pstr);
	static void SysIDConvert(uint32 id,tstring *pstr);
	static void SysIDConvert(const tchar* pstr,uint32 *pid);
};
}
#endif