#include "ExchangeAdapterConfig.h"
#include "DLTraderAdapter.h"
#include "debugutils.h"
#include <cctype>
#include "StringUtils.h"
#define TIXML_USE_STL
#include "tinyxml.h"
#include "FixDataConvertHelper.h"
#include "LocalCompile.h"

namespace exchangeadapter_dalian
{
	CDLTraderAdapter::CDLTraderAdapter(void)
	{
		m_dwSeqNo = 0;
		m_bLogin = false;
	}

	CDLTraderAdapter::~CDLTraderAdapter(void)
	{
	}

	std::pair<bool,CDLTraderAdapter::InitParam> 
		CDLTraderAdapter::InitParam::LoadConfig(const std::string& fname)
	{
		InitParam param;
		TiXmlDocument doc(fname.c_str());
		bool loadOkay = doc.LoadFile();
		TiXmlNode* pNode = NULL;
		std::string str;

		std::memset(&param,0,sizeof(param));
		if (!loadOkay)
			return std::make_pair(false,param);

		pNode = doc.FirstChild("configure");
		XML_GET_STRING(pNode,"saddressip",param.sAddressIp);
		XML_GET_INT(pNode,"iport",param.iPort);
		XML_GET_STRING(pNode,"sparticipantid",param.sParticipantID);
		XML_GET_STRING(pNode,"suserid",param.sUserID);
		XML_GET_STRING(pNode,"spassword",param.sPassword);
		XML_GET_BOOL(pNode,"blog",param.bWriteLog);

		return std::make_pair(true,param);
	}


	bool CDLTraderAdapter::InitializeByXML( const tchar* configfile )
	{
		std::pair<bool,InitParam> param = 
			InitParam::LoadConfig(configfile);
		if (param.first)
			return Initialize(&param.second);
		else
			return false;
	}


	bool CDLTraderAdapter::Initialize(void* param)

	{
		m_param = *(CDLTraderAdapter::InitParam*)param;
		if(!InitPara() || !InitAPI() || !Connect())
			return false;

		return true;
	}

	void CDLTraderAdapter::UnInitialize()
	{}

	bool CDLTraderAdapter::InitPara()
	{
		m_pcsTradeAPI = 
			boost::shared_ptr<CTradeAdapter>(new CTradeAdapter(this));
		if(NULL == m_pcsTradeAPI){
			DBG_TRACE(("内存不足\n"));
			return false;
		}

		return true;
	}

	bool CDLTraderAdapter::InitAPI()
	{
		if(0 != m_pcsTradeAPI->InitAPI(m_param.bWriteLog))
			return false;

		if(0 != m_pcsTradeAPI->InitCA(0,"","","","",false))
			return false;

		//该处可设置多个前置机服务
		m_pcsTradeAPI->SetService(m_param.sAddressIp.c_str(),m_param.iPort);

		return true;
	}

	bool CDLTraderAdapter::Connect()
	{
		if(0 != m_pcsTradeAPI->Connect()){
			DBG_TRACE(("连接错误\n"));
			return false;
		}

		return true;
	}

	bool CDLTraderAdapter::Login(void*)
	{
		_fldTraderSessionReq pTraderSessionReq;
		_fldRspMsg pRspMsg;
		_fldTraderSessionRsp pTraderSessionRsp;
		_fldTraderLoginReq loginreq;
		_fldTraderLoginRsp loginrsp;
		int32 iRet;

		LIB_NS::UString::Copy(pTraderSessionReq.MemberID.buf,
			m_param.sParticipantID);
		LIB_NS::UString::Copy(pTraderSessionReq.TraderNo.buf,m_param.sUserID);
		pTraderSessionReq.IsShortCert = _T('0');
		LIB_NS::UString::Copy(loginreq.TraderNo.buf,m_param.sUserID);
		LIB_NS::UString::Copy(loginreq.Pwd.buf,m_param.sPassword);

		iRet = m_pcsTradeAPI->TraderSession(
			&pTraderSessionReq,
			const_cast<tchar*>(m_param.sPassword.c_str()),
			&pRspMsg,&pTraderSessionRsp);
		if(0 != iRet){
			DBG_TRACE(("TraderSession error. id:%d\n",iRet));
			return false;
		}
		if(0 != pRspMsg.ErrCode)
		{
			DBG_TRACE(("TraderSession error. id:%d %s\n",pRspMsg.ErrCode,pRspMsg.RspMsg.buf));
			return false;
		}
		if(0 != (iRet = m_pcsTradeAPI->Login(&loginreq,&pRspMsg,&loginrsp))){
			DBG_TRACE(("Trader login error!id:%d\n", iRet));
			return false;
		}
		if(0 != pRspMsg.ErrCode)
		{
			DBG_TRACE(("Trader login error. id:%d %s\n",pRspMsg.ErrCode,pRspMsg.RspMsg.buf));
			return false;
		}
		if(0 != (iRet=m_pcsTradeAPI->Ready(READY,READY,false))){
			DBG_TRACE(("交易数据准备失败!\n"));
			return false;
		}

		m_bLogin = true;

		/* 上报连接 */
		PriOnLogin();
		return true;
	}

	bool CDLTraderAdapter::Logout(void*)
	{
		DBG_ASSERT(m_bLogin);
		_fldTraderLogoutReq pLogout;

		LIB_NS::UString::Copy(pLogout.MemberID.buf,m_param.sParticipantID);
		LIB_NS::UString::Copy(pLogout.TraderNo.buf,m_param.sUserID);

		if(0 > m_pcsTradeAPI->ReqTraderLogout((uint32*)(&m_dwSeqNo),pLogout))
			return false;

		PriOnLogout();
		return true;
	}

	void CDLTraderAdapter::ReqeustOrderAdd(const Order&  order)
	{
		DBG_TRACE(("开始下单\n"));
		_fldOrder fldOrder;
		CAPIVector<_fldOrder> csVector;

		std::memset(&fldOrder,0,sizeof(_fldOrder));
		UDLDataConvert::TranslatePublicDataToOrderData(order,&fldOrder,
			m_param.sParticipantID,m_param.sUserID);
		csVector.PushBack(&fldOrder);

		int32 iRet = m_pcsTradeAPI->ReqTraderInsertOrders(
			(uint32*)(&m_dwSeqNo),csVector);
		if(0 != iRet){
			DBG_TRACE(("发送订单失败:%d\n",iRet));
			// 增加发送失败处理
		}
	}

	void CDLTraderAdapter::RequestOrderCancel(const OrderCancelRequest&  order)
	{
		DBG_TRACE(("开始下撤销单...\n"));
		_fldOrderAction cancelOrder;
		std::memset(&cancelOrder,0,sizeof(_fldOrderAction));
		UDLDataConvert::TranslatePublicDataToCancelOrderData(
			order,&cancelOrder,m_param.sParticipantID,m_param.sUserID);
		int32 iret = m_pcsTradeAPI->ReqTraderCancelOrder((uint32*)(&m_dwSeqNo),
			cancelOrder);
		if(0 != iret){
			DBG_TRACE(("发送撤单失败:%d\n",iret));
			// 增加发送失败处理
		}
	}

	void CDLTraderAdapter::RequestOrderModify(
		const OrderCancelReplaceRequest & /*order*/)
	{
		DBG_TRACE(("不支持该消息类型,发送失败处理......\n"));
	}

	int32 CDLTraderAdapter::ReqTraderQryOrder(uint32 *pSeqNo,
		const _fldOrderQryReq & orderqryreq)
	{
		return m_pcsTradeAPI->ReqTraderQryOrder(pSeqNo,orderqryreq);
	}

/*****************************************************************************/
/* 响应函数处理
/*****************************************************************************/
	void CDLTraderAdapter::OnChannelLost(const tchar *szErrMsg)
	{
		DBG_TRACE(("错误:%s\n自动重连...\n防止递归\n",szErrMsg));
		PriOnNetDisconnected();

		int32 i = 0;
		while(3 > i && 0 > OnAutoConn()){
			ZThread::Thread::sleep(3000);
			i++;
		}
		if(3 > i)
			PriOnNetConnected();
	}

	int32 CDLTraderAdapter::OnAutoConn()
	{
		if(!Connect() || !Login(NULL)){
			DBG_TRACE(("重连失败错误"));
			return -1;
		}
		return 0;
	}

	void CDLTraderAdapter::OnRspTraderInsertOrders(uint32 nSeqNo,
		const _fldRspMsg & rspmsg,CAPIVector<_fldOrder> & lstOrder,
		BYTE /* bChainFlag*/)
	{
		if(0 == rspmsg.ErrCode){
			DBG_ASSERT(1 == lstOrder.GetCount());
			ExecutionReport pClientReport;
			_fldOrder &curOrderExc = lstOrder.Get(0);
			UDLDataConvert::TranRspNewOrderSingleToPublicData(
				curOrderExc,&pClientReport);
			//this->OnOrderAddResponse(pClientReport);
			FIX::Message msg;
			FixDataConvertHelper::InternalToFix(RMMS_NS::RMMS_CN_DL,
				pClientReport,&msg);
			OnResponseMsg(msg);
		}

		BusinessReject pClientRport;
		int orderCount = lstOrder.GetCount();
		for(int id=0;id<orderCount;id++){
			_fldOrder & curOrderExc = lstOrder.Get(id);
			UDLDataConvert::TraslateRejectExecutionReportToPublicData(
				rspmsg, curOrderExc,&pClientRport, nSeqNo);
			//OnBussinessReject(pClientRport);
			FIX::Message msg;
			FixDataConvertHelper::InternalToFix(RMMS_NS::RMMS_CN_DL,
				pClientRport,&msg);
			OnResponseMsg(msg);
		}
	}

	void CDLTraderAdapter::OnRplTraderInsertOrders(
		uint32 nSeqNo,const _fldRspMsg & rspmsg,
		CAPIVector<_fldOrder> & lstOrder,BYTE /*bChainFlag*/)
	{
		if(0 == rspmsg.ErrCode)
			return;
		
		BusinessReject pBusinessReject;
		int orderCount = lstOrder.GetCount();
		for(int id=0;id<orderCount;id++){
			_fldOrder & curOrderExc = lstOrder.Get(id);
			UDLDataConvert::TraslateRejectExecutionReportToPublicData(
				rspmsg, curOrderExc,&pBusinessReject, nSeqNo);
			//OnBussinessReject(pBusinessReject);
			FIX::Message msg;
			FixDataConvertHelper::InternalToFix(RMMS_NS::RMMS_CN_DL,
				pBusinessReject,&msg);
			OnResponseMsg(msg);
		}
	}

	void CDLTraderAdapter::OnTraderOrdersConfirmation(
		uint32 nSeqNo,const _fldOrderStatus & orderstatus,BYTE /*bChainFlag*/)
	{
		UNREF_VAR(nSeqNo);
		DBG_TRACE(("删除......\n"));
		orderstatus;
	}

	void CDLTraderAdapter::OnNtyTraderMatch(
		uint32 /*nSeqNo*/,const _fldMatch & match,BYTE /*bChainFlag */)
	{
		DBG_TRACE(("收到成交单......\n"));
		FillReport pFillReport;
		std::memset(&pFillReport,0,sizeof(FillReport));
		UDLDataConvert::TranslateOrderExcutionToPublicData(match,&pFillReport);
		//this->OnOrderFillResponse(pFillReport);
		FIX::Message msg;
		FixDataConvertHelper::InternalToFix(RMMS_NS::RMMS_CN_DL,
			pFillReport,&msg);
		OnResponseMsg(msg);
	}

	void CDLTraderAdapter::OnRspTraderCancelOrder(
		uint32 nSeqNo,const _fldRspMsg & rspmsg,
		const _fldOrderAction & orderaction,BYTE /*bChainFlag*/)
	{	
		if(0 == rspmsg.ErrCode){
			DBG_TRACE(("撤单成功......\n"));	
			ExecutionReport pcancelReport;
			UDLDataConvert::TranCancelOrderSuccToPublicData(
				orderaction,&pcancelReport);
			//this->OnOrderCancelResponse(pcancelReport);
			FIX::Message msg;
			FixDataConvertHelper::InternalToFix(RMMS_NS::RMMS_CN_DL,
				pcancelReport,&msg);
			OnResponseMsg(msg);
		} else {
			DBG_TRACE(("撤单被拒...\n"));
			DBG_TRACE(("错误号:%u",rspmsg.ErrCode));
			OrderCancelReject cancelReject;
			std::memset(&cancelReject,0,sizeof(OrderCancelReject));
			UDLDataConvert::TraslateRejectExecutionReportToPublicData(
				rspmsg,orderaction,&cancelReject,nSeqNo);
			//this->OnOrderCancelReject(cancelReject);
			FIX::Message msg;
			FixDataConvertHelper::InternalToFix(RMMS_NS::RMMS_CN_DL,
				cancelReject,&msg);
			OnResponseMsg(msg);
		}
	}

	void CDLTraderAdapter::OnRplTraderCancelOrder(
		uint32 nSeqNo,const _fldRspMsg & rspmsg,
		const _fldOrderAction & orderaction,BYTE bChainFlag)
	{
		UNREF_VAR(nSeqNo);
		UNREF_VAR(rspmsg);
		UNREF_VAR(orderaction);
		UNREF_VAR(bChainFlag);
		if(0 == rspmsg.ErrCode){
			DBG_TRACE(("撤单请求发送成功......\n"));	
			return;
		}

		DBG_TRACE(("撤单请求发送失败......\n"));
		// 增加撤单失败处理
	}

	int32 CDLTraderAdapter::onRspTraderQryOrder(uint32 nSeqNo,
		const _fldRspMsg & rspmsg,CAPIVector<_fldOrderStatus> & lstOrderStatus,
		BYTE bChainFlag)
	{
		UNREF_VAR(nSeqNo);
		UNREF_VAR(rspmsg);
		UNREF_VAR(lstOrderStatus);
		UNREF_VAR(bChainFlag);
		return 0;
	}

	void CDLTraderAdapter::RequestFix(FIX::Message &msg)
	{
		FIX::MsgType type;
		const FIX::Header &header = msg.getHeader();
		header.getField(type);
		tstring strtype = type.getValue();

		if(FIX::MsgType_NewOrderSingle == strtype){
			Order order;
			if(0 > FixDataConvertHelper::FixToInternal(RMMS_NS::RMMS_CN_ZZ,msg,&order))
				return;
			ReqeustOrderAdd(order);
		} else if(FIX::MsgType_OrderCancelRequest == strtype) {
			OrderCancelRequest cancel;
			if(0 > FixDataConvertHelper::FixToInternal(RMMS_NS::RMMS_CN_ZZ,msg,&cancel))
				return;
			RequestOrderCancel(cancel);
		}
	}
}


	



