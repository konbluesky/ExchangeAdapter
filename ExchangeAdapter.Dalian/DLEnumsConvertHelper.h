#ifndef __DLENUMCONVERTHELPER_H__
#define __DLENUMCONVERTHELPER_H__


#include "ChinaOrderDefinition.h"

namespace exchangeadapter_dalian
{
	class DLEnumsConvertHelper
	{
	public:	
		static uint8 Convert2ExchOpenClose(OpenCloseFlag rmmOpenClose);
		static uint8 Convert2ExchSide(TradeSide rmmTradeSide);
		static uint8 Convert2ExchOrderType(OrderTypeFlag rmmOrderType);
		static void Convert2RmmOpenClose(uint8 eoFlag,
			OpenCloseFlag rmmOpenClose);
		static void Convert2RmmSide(uint8 bsFlag,TradeSide rmmTradeSide);
		static void Convert2RmmOrderType(uint8 orderType,
			OrderTypeFlag rmmOrderType);
	};
}

#endif