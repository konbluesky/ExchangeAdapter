#ifndef _APIVECTOR
#define _APIVECTOR

#define INITSIZE 1000

template <typename _T>
class CAPIVector  
{
public:
	/**CAPIVector
	* @return	
	*/
	CAPIVector()
	{
		_M_Buffer = NULL;
		m_nCount = 0;
		m_nCapacity = 0;
	}

	/**~CAPIVector
	* @return	virtual 
	*/
	virtual ~CAPIVector()
	{
		//printf("~CAPIVector:%p",_M_Buffer);
		delete _M_Buffer;
	}
	
	/**向容器中接入对象
	* @param	value		.
	* @return	int 
	*/
	int PushBack(_T value)
	{
		EnsureSize(m_nCount + 1);
		Get(m_nCount)=value;
		m_nCount++;
		return m_nCount - 1;
	}
	
	/**向容器中加入对象指针
	* @param	pValue		.
	* @return	int 
	*/
	int PushBack(_T *pValue)
	{
		EnsureSize(m_nCount + 1);
		Get(m_nCount) = *pValue;
		m_nCount++;
		return m_nCount - 1;
	}

	/**从容器中移除对象
	* @param	value		.
	* @return	void 
	*/
	void Remove(_T value )
	{
		int i=0;
		
		_T * pBuffer;
		pBuffer=new _T[m_nCount];
		int j=0;
		for (i=0;i<m_nCount;i++)
		{
			if (Get(i)!=value)
			{
				memcpy(pBuffer+j,_M_Buffer+i,sizeof(_T));
				j++;
			}
		}
		memcpy(_M_Buffer,pBuffer,sizeof(_T)*j);
		delete pBuffer;
		m_nCount=j;
	}

	/**获取容器中元素个数
	* @return	int 
	*/
	int GetCount()
	{
		return m_nCount;
	}

	/**获取指定索引的元素
	* @param	index		.
	* @return	_T & 
	*/
	_T & Get(int index)
	{
		return _M_Buffer[index];
	}

	_T & operator [](int index)
	{
		return Get(index);
	}

	/**清除容器中的元素
	* @return	void 
	*/
	void Clear()
	{
		delete _M_Buffer;
		_M_Buffer = NULL;
		m_nCount = 0;
		m_nCapacity = 0;
	}
	
protected:
	void EnsureSize(int nNewSize)
	{
		if(m_nCapacity < nNewSize){
			m_nCapacity = (nNewSize + INITSIZE -1)/INITSIZE*INITSIZE;
			_T *pTemp = new _T[m_nCapacity];
			memcpy(pTemp, _M_Buffer, m_nCount*sizeof(_T));
			delete _M_Buffer;
			_M_Buffer = pTemp;
		}
	}
private:
	int m_nCapacity;
	int m_nCount;
	_T * _M_Buffer;
};

#endif
