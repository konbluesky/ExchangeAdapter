#ifndef __PASS_H
#define __PASS_H

#ifdef WIN32
#ifdef GM_MAIN_PROCESS
#define GM_FUNC_EXPORT __declspec(dllimport)
#else
#define GM_FUNC_EXPORT __declspec(dllexport)
#endif
#else
#define GM_FUNC_EXPORT
#endif

GM_FUNC_EXPORT 	int 	  getUserName(char *FileName,char *dbUser,char *dbPassword);
GM_FUNC_EXPORT	int   passwd_new(char *password,int len,char *crit);
GM_FUNC_EXPORT	void  setPassLevel(int level,char *keyword);
GM_FUNC_EXPORT	void  getDqrq(char *d);
GM_FUNC_EXPORT	int   passwd_old(char *password,int len,char *crit);
GM_FUNC_EXPORT	int   WriteSystemDatFile(char *filename,char *UserName,char *UserPasswd);
GM_FUNC_EXPORT	int	  Sencrypt_init();
GM_FUNC_EXPORT	void  Sencrypt_done();
GM_FUNC_EXPORT	int   endes(char *block);
GM_FUNC_EXPORT	int   dedes(char *block);
GM_FUNC_EXPORT	void  DesDeCode(char *sInputStr,char *sOut);
GM_FUNC_EXPORT	void  DesEnCode(char *sInputStr,char *sOut);
GM_FUNC_EXPORT  void  DesPutHex(char *sBuf,char *sOut);
GM_FUNC_EXPORT	void  EncBuf(char const* sInBuf,char* sOutBuf,char const* sIdentify,int iInBufMaxLen);
GM_FUNC_EXPORT	void  DesBuf(char* sInBuf,char* sOutBuf);
#endif
