#include "ExchangeAdapterConfig.h"
#include "ExchangeAdapterBase.h"
#include "macroinstancedef.h"

//这个先简单的实现，到时候考虑实现多接口(类似COM框架).
EXCAPI 	IFixMarketProvider* CreateMarketDataProvider()
{
	return new __MARKETCLASS;
}

EXCAPI 	void DestoryMarketDataProvider(IFixMarketProvider* adapter)
{
	if (adapter !=NULL)
	{
		delete adapter;
	}
}

EXCAPI	IFixTraderProvider* CreateTraderProvider()
{
	return new __TRADERCLASS;
}

EXCAPI	void DestoryTraderProvider(IFixTraderProvider* adapter)
{
	if (adapter !=NULL)
		delete adapter;
}