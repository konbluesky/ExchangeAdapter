#include "InternalHeader.h"
#include "DLMarketDataAdapter.h"
#include "debugutils.h"
#include "QuotAdapter.h"
#include "DLDataConvertHelper.h"
namespace exchangeadapter_dalian
{

CDLMarketDataAdapter::CDLMarketDataAdapter(void)
{
	m_dwSeqNo = 0;
	m_bLogin = false;
	m_bIsLog = false;
	std::memset(&m_stUserInfo,0,sizeof(UserInfo));
}

CDLMarketDataAdapter::~CDLMarketDataAdapter(void)
{
}


bool CDLMarketDataAdapter::Initialize(void* pInitParam)

{
	if(!InitPara() || !InitAPI() || !Connect() || !Login())
	{
		return false;
	}

	return true;
}

void CDLMarketDataAdapter::UnInitialize()
{
	Logout();
}

/* 
 * 暂时直接配置.后期可更改多种方式,如从ini,xml等文件中
 * 读取.通过配置文件来屏蔽交易所API的细节差异.
 */
bool CDLMarketDataAdapter::InitPara()
{
	m_pcsQutotAPI = 
		boost::shared_ptr<CQuotAdapter>(new CQuotAdapter(this));
	if(NULL == m_pcsQutotAPI)
	{
		DBG_TRACE(("内存不足"));
		return false;
	}
	m_stUserInfo.tsrMemberID = _T("0002");
	m_stUserInfo.tsrTradeID = _T("20090002");
	m_stUserInfo.tsrPasswd = _T("12457802");

	NetInfo netinfo;
	netinfo.tsrIP = _T("218.25.154.83");
	netinfo.dwPort = 15555;
	m_csNetInfoList.push_back(netinfo);

	m_bIsLog = false;

	return true;
}

bool CDLMarketDataAdapter::InitAPI()
{
	m_pcsQutotAPI->InitAPI(m_bIsLog);

	if(0 != m_pcsQutotAPI->InitCA(0,"","","","",false))
	{
		return false;
	}

	return true;
}

bool CDLMarketDataAdapter::Connect()
{
	NetInfoList::const_iterator listIter = m_csNetInfoList.begin();
	for (;listIter != m_csNetInfoList.end();listIter++)
	{
		m_pcsQutotAPI->SetService(
			(*listIter).tsrIP.c_str(),
			(*listIter).dwPort);
	}

	if(0 != m_pcsQutotAPI->Connect())
	{
		DBG_TRACE(("连接错误"));
		return false;
	}

	return true;
}

bool CDLMarketDataAdapter::Login()
{
	_fldTraderSessionReq pTraderSessionReq;
	_fldRspMsg pRspMsg;
	_fldTraderSessionRsp pTraderSessionRsp;
	_fldTraderLoginReq loginreq;
	_fldTraderLoginRsp loginrsp;
	INT32 iRet;

	std::strcpy(
		pTraderSessionReq.MemberID.buf,m_stUserInfo.tsrMemberID.c_str());
	std::strcpy(
		pTraderSessionReq.TraderNo.buf,m_stUserInfo.tsrTradeID.c_str());
	pTraderSessionReq.IsShortCert = _T('0');

	std::strcpy(
		loginreq.TraderNo.buf,m_stUserInfo.tsrTradeID.c_str());
	std::strcpy(
		loginreq.Pwd.buf,m_stUserInfo.tsrPasswd.c_str());

	iRet = m_pcsQutotAPI->TraderSession(
		&pTraderSessionReq,
		const_cast<char*>(m_stUserInfo.tsrPasswd.c_str()),
		&pRspMsg,&pTraderSessionRsp);
	if(iRet)
	{
		DBG_TRACE(("错误号:%d %s",iRet,pRspMsg.RspMsg.buf));
		return false;
	}

	if((iRet = m_pcsQutotAPI->Login(&loginreq,&pRspMsg,&loginrsp)))
	{
		DBG_TRACE(("报价登陆失败!"));
		return false;
	}

	if((iRet=m_pcsQutotAPI->Ready(READY,READY,true)))
	{
		DBG_TRACE(("准备失败!"));
		return false;
	}

	m_bLogin = true;

	return true;
}

void CDLMarketDataAdapter::Logout()
{
	DBG_ASSERT(m_bLogin);
	_fldTraderLogoutReq pLogout;

	std::strcpy(
		pLogout.MemberID.buf,m_stUserInfo.tsrMemberID.c_str());
	std::strcpy(
		pLogout.TraderNo.buf,m_stUserInfo.tsrTradeID.c_str());

	DBG_ASSERT(
		0 ==m_pcsQutotAPI->ReqQuotUserLogout((UINT4*)(&m_dwSeqNo),pLogout));
}

int CDLMarketDataAdapter::OnQuoteRecieve(
	UINT4 nSeqNo,
	const _fldBestQuot &bestquot,
	CAPIVector<_fldOptPara> & lstOptPara,
	CAPIVector<_fldMBLQuot> &lstMBLQuot,
	BYTE bChainFlag)
{
	Quote quote;
	DoM dom;

	std::memset(&quote,0,sizeof(quote));
	std::memset(&dom,0,sizeof(dom));

	/* 处理顺序号 */
	

	UDLDataConvert::TranslateQuoteDataToPublicData(
		bestquot,&quote);
	UDLDataConvert::TranslateDomDataToPublicData(
		lstMBLQuot,bestquot,&dom);

	OnQuoteRecieve(quote);
	OnDomRecieve(dom);

	return 0;
}

void CDLMarketDataAdapter::onChannelLost(const char *szErrMsg)
{
	DBG_TRACE((const_cast<char*>(szErrMsg)));
	m_bLogin = false;	
}

INT32 CDLMarketDataAdapter::onInvalidPackage(
	UINT4 nTID,WORD nSeries,
	UINT4 nSequenceNo,
	WORD nFieldCount,
	WORD nFieldsLen,
	const char *pAddr)
{
	DBG_TRACE(("包格式错误"));
	return 0;
}

}