#include "DLDataConvertHelper.h"
#include "DLEnumsConvertHelper.h"
#include "DLPriceHelper.h"
#include "debugutils.h"
#include "stringutils.h"
#define TIXML_USE_STL
#include "tinyxml.h"
#include "localcompile.h"

using namespace std;

namespace exchangeadapter_dalian
{
	/* 下单 */
	int32 UDLDataConvert::TranslatePublicDataToOrderData(
		const Order &clientOrder,_fldOrder *pApiOrder,
		const tstring &sParticipantID,const tstring &sUserID)
	{
		DBG_ASSERT(NULL != pApiOrder);
		
		std::strcpy(pApiOrder->MemberID.buf,sParticipantID.c_str());
		std::strcpy(pApiOrder->TraderNo.buf,sUserID.c_str());
		pApiOrder->LocalOrderNo = clientOrder.dlIDLocalOrder;
		pApiOrder->BsFlag = DLEnumsConvertHelper::Convert2ExchSide(
			clientOrder.Side);
		pApiOrder->Qty = clientOrder.Quantity;
		pApiOrder->OrderType = DLEnumsConvertHelper::Convert2ExchOrderType(
			clientOrder.OrderType);
		pApiOrder->EoFlag = DLEnumsConvertHelper::Convert2ExchOpenClose(
			clientOrder.OpenClose);
		std::strcpy(pApiOrder->ContractID.buf,clientOrder.SecurityDesc.c_str());
#if defined(RMMS_TEST_CLIENT)
		std::strcpy(pApiOrder->ClientID.buf,RMMS_DL_CLIENT);
#else
		std::strcpy(pApiOrder->ClientID.buf,clientOrder.IDClient.c_str());
#endif

		/* 中间数据结构不提供 */
		pApiOrder->OrderAttr = (BYTE)('0');
		pApiOrder->OrderSort = (BYTE)('0');
		pApiOrder->OrderSrc =(BYTE)('0');
		pApiOrder->IsMktMk = (BYTE)('0');
		pApiOrder->ShFlag = '1';

		/* 下面是有条件必带,所以需要判断条件 */
		pApiOrder->Price = clientOrder.LimitPrice;
		pApiOrder->StopPrice = clientOrder.StopPrice;

		return 0;
	}

	/* 下单拒绝 */
	int32 UDLDataConvert::TraslateRejectExecutionReportToPublicData(
		const _fldRspMsg &rspmsg,_fldOrder & orderExcution,
		BusinessReject *pClientReject, int requestID)
	{
		DBG_ASSERT(NULL != pClientReject);
		pClientReject->LastMsgSeqNumProcessed = requestID;
		pClientReject->dlIDLocalOrder = orderExcution.LocalOrderNo;
		IDConvet(rspmsg.ErrCode,&(pClientReject->ErrorCode));
		pClientReject->ErrorSource = "DCE";
		pClientReject->RefMsgType = "D";

		return 0;
	}

	/* 下单成功 */
	int32 UDLDataConvert::TranRspNewOrderSingleToPublicData(
		const _fldOrder &orderInfo,ExecutionReport * pRspNewOrderSig)
	{
		/*
#if defined(RMMS_TEST_CLIENT)
		pRspNewOrderSig->IDClient = RMMS_DL_GLOTRADER;
#else
		pRspNewOrderSig->IDClient = orderInfo.ClientID.buf;
#endif
		*/
		IDConvet(orderInfo.SysOrderNo,	&(pRspNewOrderSig->IDSysOrder));
		pRspNewOrderSig->dlIDLocalOrder = orderInfo.LocalOrderNo;
		pRspNewOrderSig->SecurityDesc = orderInfo.ContractID.buf;
		DLEnumsConvertHelper::Convert2RmmSide(orderInfo.BsFlag,
			pRspNewOrderSig->Side);
		pRspNewOrderSig->OrderStatus = New;
		pRspNewOrderSig->ExecType = ExecType_New;
		pRspNewOrderSig->PriceLimit = orderInfo.Price;
		pRspNewOrderSig->QtyOrder = orderInfo.Qty;

		pRspNewOrderSig->datetime = LIB_NS::DateTime::NowUtc();

		return 0;
	}

	/* 下单成交 */
	int32 UDLDataConvert::TranslateOrderExcutionToPublicData(
		const _fldMatch &orderExcution,FillReport * pfillreport)
	{
		DBG_ASSERT(NULL != pfillreport);
#if defined(RMMS_TEST_CLIENT)
		pfillreport->IDClient = RMMS_DL_GLOTRADER;
#else
		pfillreport->IDClient = orderExcution.ClientID.buf;
#endif
		SysIDConvert(orderExcution.SysOrderNo,&(pfillreport->IDSysOrder));
		tchar buff[11] = {0};
		std::sprintf(buff,"%010u",orderExcution.MatchNo);
		pfillreport->IDExec = buff;
		pfillreport->dlIDLocalOrder = orderExcution.LocalID;
		pfillreport->SecurityDesc = orderExcution.ContractID.buf;
		DLEnumsConvertHelper::Convert2RmmSide(orderExcution.BsFlag,
			pfillreport->Side);
		pfillreport->LastPx = orderExcution.Price;
		pfillreport->QtyLastShares = orderExcution.Qty;
		//pfillreport->TradeTime = orderExcution.MatchTime;
		return 0;
	}

	/* 取消单 */
	int32 UDLDataConvert::TranslatePublicDataToCancelOrderData(
		const OrderCancelRequest & clientOrderCancel,
		_fldOrderAction* pApiOrderCancel,const tstring &sParticipantID,
		const tstring &sUserID)
	{
		DBG_ASSERT(NULL != pApiOrderCancel);

		std::strcpy(pApiOrderCancel->MemberID.buf,sParticipantID.c_str());
		std::strcpy(pApiOrderCancel->TraderNo.buf,sUserID.c_str());
		std::strcpy(pApiOrderCancel->CancelTraderNo.buf,sUserID.c_str());
		SysIDConvert(clientOrderCancel.IDSysOrder.c_str(),
			&(pApiOrderCancel->SysOrderNo));
		pApiOrderCancel->OrderBatchNo = pApiOrderCancel->SysOrderNo;
		pApiOrderCancel->LocalOrderNo = clientOrderCancel.dlIDLocalOrder;
		pApiOrderCancel->OrderSrc = '0';
		std::strcpy(pApiOrderCancel->ContractID.buf,
			clientOrderCancel.IDInstrument.c_str());
		return 0;
	}

	/* 取消拒绝 */
	int32 UDLDataConvert::TraslateRejectExecutionReportToPublicData(
		const _fldRspMsg& /*rspmsg*/,const _fldOrderAction & /*orderExcution*/,
		OrderCancelReject * /*pCancelReject*/, int /*requestID*/)
	{
		DBG_TRACE(("不支持"));
		return -1;
	}

	/* 取消成功 */
	int32 UDLDataConvert::TranCancelOrderSuccToPublicData(
		const _fldOrderAction & orderaction,
		ExecutionReport* pClientOrderCancel)
	{
		DBG_ASSERT(NULL != pClientOrderCancel);
		/*
#if defined(RMMS_TEST_CLIENT)
		pClientOrderCancel->IDClient = RMMS_DL_GLOTRADER;
#else
		pClientOrderCancel->IDClient = orderaction.ClientID.buf;
#endif
		*/
		SysIDConvert(orderaction.SysOrderNo,&(pClientOrderCancel->IDSysOrder));
		pClientOrderCancel->dlIDLocalOrder = orderaction.LocalOrderNo;
		pClientOrderCancel->SecurityDesc = orderaction.ContractID.buf;
		// Side 
		pClientOrderCancel->OrderStatus = Canceled;
		pClientOrderCancel->ExecType = ExecType_Canceled;
		pClientOrderCancel->PriceLimit = 0;				
		return 0;
	}
	
	/* 报价 */
	int32 UDLDataConvert::TranslateQuoteDataToPublicData(
		const _fldBestQuot &bestquot, Quote *pquote)
	{

		DBG_ASSERT(NULL != pquote);
		pquote->ID= bestquot.ContractID.buf;
		DBG_TRACE(("%s",pquote->ID.c_str()));
		pquote->Last_Price= UDLPriceHelper::Price(bestquot.LastPrice);
		pquote->Last_Volume=bestquot.LastMatchQty;
		pquote->BidPrice=UDLPriceHelper::Price(bestquot.BidPrice);
		pquote->BidQty=bestquot.BidQty;
		pquote->AskPrice=UDLPriceHelper::Price(bestquot.AskPrice);
		pquote->AskQty=bestquot.AskQty;
		pquote->LowPrice=UDLPriceHelper::Price(bestquot.LowPrice);
		pquote->HighPrice=UDLPriceHelper::Price(bestquot.HighPrice);
		pquote->Total_Volume=bestquot.MatchTotQty;
		pquote->Open_Price=UDLPriceHelper::Price(bestquot.OpenPrice);
		pquote->SettlementPrice=UDLPriceHelper::Price(bestquot.LastClearPrice);
		DBG_TRACE(("%f",pquote->SettlementPrice));
		GetUTCTime(((_fldBestQuot)bestquot).TradeDate.getValue(),
			((_fldBestQuot)bestquot).GenTime.getValue(),&pquote->RecieveTime);
		return 0;
	}

	/* 深度市场数据 */
	int32 UDLDataConvert::TranslateDomDataToPublicData(
		CAPIVector<_fldMBLQuot> & lstMBLQuot,const _fldBestQuot &bestquot,
		DoM* pdom)
	{
		UNREF_VAR(lstMBLQuot);
		UNREF_VAR(bestquot);
		UNREF_VAR(pdom);
		DBG_ASSERT(NULL != pdom);
		return 0;
	}

	static inline int32 GetInt(int8 x)
	{
		return x-'0';
	}

	bool UDLDataConvert::GetUTCTime(const tchar *cYMD,const tchar *cHMSm,
		LIB_NS::DateTime* dt)
	{
		unsigned int iYear=0,iMon=0,iDay=0,iHour=0,iMin=0,iSec=0,iMil=0;
		
		std::sscanf(cYMD,"%4d%2d%2d",&iYear,&iMon,&iDay);
		std::sscanf(cHMSm,"%2d:%2d:%2d",&iHour,&iMin,&iSec,&iMil);
		dt->SetYMD(iYear,iMon,iDay);
		dt->SetHMS(iHour,iMin,iSec,iMil);

		return true;
	}

	void UDLDataConvert::IDConvet(uint32 num,tstring *pstr)
	{
		tchar convertID[16] = {'\0'};
		std::sprintf(convertID,"%u",num);
		*pstr = convertID;
	}

	void UDLDataConvert::IDConvet(int32 num,tstring *pstr)
	{
		tchar convertID[16] = {'\0'};
		std::sprintf(convertID,"%d",num);
		*pstr = convertID;
	}

	void UDLDataConvert::SysIDConvert(uint32 id,tstring *pstr)
	{
		tchar convertID[16] = {'\0'};
		std::sprintf(convertID,"%010u",id);
		*pstr = convertID;
	}
	
	void UDLDataConvert::SysIDConvert(const tchar* pstr,uint32 *pid)
	{
		std::sscanf(pstr,"%010u",pid);
	}
}