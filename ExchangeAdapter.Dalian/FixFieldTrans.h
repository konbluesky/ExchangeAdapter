#ifndef __FIX_FIELD_TRANS_H__
#define __FIX_FIELD_TRANS_H__

#include "ChinaQuoteDefinition.h"
#include "ChinaOrderDefinition.h"
#include "RMMSCommon.h"
#include "StringUtils.h"

class FixFieldTrans
{
public:
	static inline void OrderType_I2F(OrderTypeFlag flag,FIX::Message *pmsg)
	{
		switch(flag)
		{
		case Limit:
			pmsg->setField(FIX::OrdType(FIX::OrdType_LIMIT));
			break;
		case Market:
			pmsg->setField(FIX::OrdType(FIX::OrdType_MARKET));
			break;
		case Stop:
			pmsg->setField(FIX::OrdType(FIX::OrdType_STOP));
			break;
		case Stop_Limit:
			pmsg->setField(FIX::OrdType(FIX::OrdType_STOP_LIMIT));
			break;
		default:break;
		}
	}

	static inline void OrderType_F2I(FIX::OrdType &type,
		OrderTypeFlag *pflag)
	{
		switch(type.getValue())
		{
		case FIX::OrdType_LIMIT:
			*pflag = Limit;
			break;
		case FIX::OrdType_MARKET:
			*pflag = Market;
			break;
		case FIX::OrdType_STOP:
			*pflag = Stop;
			break;
		case FIX::OrdType_STOP_LIMIT:
			*pflag = Stop_Limit;
			break;
		default:break;
		}
	}

	static inline void Side_F2I(FIX::Side &fixside,
		TradeSide *pside)
	{
		switch(fixside.getValue())
		{
		case FIX::Side_BUY:
			*pside = BUY;
			break;
		case FIX::Side_SELL:
			*pside = SELL;
			break;
		}
	}

	static inline void Side_I2F(TradeSide innside,FIX::Message *pmsg)		
	{
		switch(innside)
		{
		case BUY:
			pmsg->setField(FIX::Side(FIX::Side_BUY));
			break;
		case SELL:
			pmsg->setField(FIX::Side(FIX::Side_SELL));
			break;
		}
	}

	static inline void OrderQty_F2I(FIX::OrderQty &fixquantity,
		int32 *pquantity)
	{
		*pquantity = (int32)(fixquantity.getValue());
	}

	static inline void OrderQty_I2F(int32 innquantity,FIX::Message *pmsg)		
	{
		pmsg->setField(FIX::OrderQty((double)innquantity));
	}

	static inline void OpenClose_F2I(FIX::OpenClose &fixopenclose,
		OpenCloseFlag *pinnopenclose)
	{
		switch(fixopenclose.getValue())
		{
		case FIX::OpenClose_OPEN:
			*pinnopenclose = Open;
			break;
		case FIX::OpenClose_CLOSE:
			*pinnopenclose = Close;
			break;
		}
	}

	static inline void OpenClose_I2F(OpenCloseFlag innopenclose,
		FIX::Message *pmsg)
	{
		switch(innopenclose)
		{
		case Open:
			pmsg->setField(FIX::OpenClose(FIX::OpenClose_OPEN));
			break;
		case Close:
			pmsg->setField(FIX::OpenClose(FIX::OpenClose_CLOSE));
			break;
		}
	}

	static inline void TimeInForce_F2I(FIX::TimeInForce &fixtimeinforce,
		TimeConditionFlag *pinntimecondition)
	{
		switch(fixtimeinforce.getValue())
		{
		case FIX::TimeInForce_DAY:
			*pinntimecondition = Day;
			break;
		case FIX::TimeInForce_GOOD_TILL_CANCEL:
			*pinntimecondition = GTC;
			break;
		case FIX::TimeInForce_IMMEDIATE_OR_CANCEL:
			*pinntimecondition = IOC;
			break;
		}
	}

	static inline void TimeInForce_I2F(TimeConditionFlag inntimecondition,
		FIX::Message *pmsg)		
	{
		switch(inntimecondition)
		{
		case Day:
			pmsg->setField(FIX::TimeInForce(FIX::TimeInForce_DAY));
			break;
		case GTC:
			pmsg->setField(FIX::TimeInForce(FIX::TimeInForce_GOOD_TILL_CANCEL));
			break;
		case IOC:
			pmsg->setField(FIX::TimeInForce(
				FIX::TimeInForce_IMMEDIATE_OR_CANCEL));
			break;
		}
	}

	/*
	static inline void OrdStatus_F2I(FIX::OrdStatus &fixordstatus,
		OrderStatusFlag *pinnorderstatusflag)
	{
		switch(fixordstatus.getValue())
		{
		case FIX::OrdStatus_NEW:
			*pinnorderstatusflag = Completed;
			break;
		case FIX::OrdStatus_CANCELED:
			*pinnorderstatusflag = Cancelled;
			break;
		case FIX::OrdStatus_REJECTED:
			*pinnorderstatusflag = Rejected;
			break;
		}
	}
	*/

	static inline void OrdStatus_I2F(OrderStatusFlag inntimecondition,
		FIX::Message *pmsg)
	{
		switch(inntimecondition)
		{
		case New:
			pmsg->setField(FIX::OrdStatus(FIX::OrdStatus_NEW));
			break;
		case Canceled:
			pmsg->setField(FIX::OrdStatus(FIX::OrdStatus_CANCELED));
			break;
		case Rejected:
			pmsg->setField(FIX::OrdStatus(FIX::OrdStatus_REJECTED));
			break;
		case Filled:
			pmsg->setField(FIX::OrdStatus(FIX::OrdStatus_FILLED));
			break;
		case PartiallyFilled:
			pmsg->setField(FIX::OrdStatus(FIX::OrdStatus_PARTIALLY_FILLED));
			break;
		case Replaced:
			pmsg->setField(FIX::OrdStatus(FIX::OrdStatus_REPLACED));
			break;
		}
	}

	static void FixMonthToExchange(tchar cmonth,tstring *pstr)
	{
		switch(cmonth){
		case 'F': *pstr = "01";break;
		case 'G': *pstr = "02";break;
		case 'H': *pstr = "03";break;
		case 'J': *pstr = "04";break;
		case 'K': *pstr = "05";break;
		case 'M': *pstr = "06";break;
		case 'N': *pstr = "07";break;
		case 'Q': *pstr = "08";break;
		case 'U': *pstr = "09";break;
		case 'V': *pstr = "10";break;
		case 'X': *pstr = "11";break;
		case 'Z': *pstr = "12";break;
		default:*pstr = "";break;
		}
	}
	static inline void ExchangeMonthToFix(tstring &cmonth,tchar *pmon)
	{
		if("01" == cmonth)
			*pmon = 'F';
		else if("02" == cmonth)
			*pmon = 'G';
		else if("03" == cmonth)
			*pmon = 'H';
		else if("04" == cmonth)
			*pmon = 'J';
		else if("05" == cmonth) 
			*pmon = 'K';
		else if("06" == cmonth) 
			*pmon = 'M';
		else if("07" == cmonth)
			*pmon = 'N';
		else if("08" == cmonth)
			*pmon = 'Q';
		else if("09" == cmonth)
			*pmon = 'U';
		else if("10" == cmonth)
			*pmon = 'V';
		else if("11" == cmonth) 
			*pmon = 'X';
		else
			*pmon = 'Z';
	}

	/* 转到Globatal */
	static inline void _ZZ_Fix(const tstring &str,tstring *pdst)
	{
		if("CF007" == str) *pdst = "CF101";
		else if("CF009" == str) *pdst = "CF103";
		else if("CF011" == str) *pdst = "CF105";
		else if("CF101" == str) *pdst = "CF107";
		else if("CF103" == str) *pdst = "CF109";
		else if("CF105" == str) *pdst = "CF111";
		else if("ER007" == str) *pdst = "ER101";
		else if("ER009" == str) *pdst = "ER103";
		else if("ER011" == str) *pdst = "ER105";
		else if("ER101" == str) *pdst = "ER107";
		else if("ER103" == str) *pdst = "ER109";
		else if("ER105" == str) *pdst = "ER111";
		else if("RO007" == str) *pdst = "RO001";
		else if("RO009" == str) *pdst = "RO003";
		else if("RO011" == str) *pdst = "RO005";
		else if("RO101" == str) *pdst = "RO007";
		else if("RO103" == str) *pdst = "RO009";
		else if("RO105" == str) *pdst = "RO011";
		else if("SR007" == str) *pdst = "SR101";
		else if("SR009" == str) *pdst = "SR103";
		else if("SR011" == str) *pdst = "SR105";
		else if("SR101" == str) *pdst = "SR107";
		else if("SR103" == str) *pdst = "SR109";
		else if("SR105" == str) *pdst = "SR111";
		else if("SR107" == str) *pdst = "SR201";
		else if("SR109" == str) *pdst = "SR203";
		else if("SR111" == str) *pdst = "SR205";
		else if("TA007" == str) *pdst = "TA101";
		else if("TA008" == str) *pdst = "TA102";
		else if("TA009" == str) *pdst = "TA103";
		else if("TA010" == str) *pdst = "TA104";
		else if("TA011" == str) *pdst = "TA105";
		else if("TA012" == str) *pdst = "TA106";
		else if("TA101" == str) *pdst = "TA107";
		else if("TA102" == str) *pdst = "TA108";
		else if("TA103" == str) *pdst = "TA109";
		else if("TA104" == str) *pdst = "TA110";
		else if("TA105" == str) *pdst = "TA111";
		else if("TA106" == str) *pdst = "TA112";
		else if("WS007" == str) *pdst = "WS101";
		else if("WS009" == str) *pdst = "WS103";
		else if("WS011" == str) *pdst = "WS105";
		else if("WS101" == str) *pdst = "WS107";
		else if("WS103" == str) *pdst = "WS109";
		else if("WS105" == str) *pdst = "WS111";
		else if("WS107" == str) *pdst = "WS201";
		else if("WS109" == str) *pdst = "WS203";
		else if("WS111" == str) *pdst = "WS205";
		else if("WT007" == str) *pdst = "WT101";
		else if("WT009" == str) *pdst = "WT103";
		else if("WT011" == str) *pdst = "WT105";
		else if("WT101" == str) *pdst = "WT107";
		else if("WT103" == str) *pdst = "WT109";
		else if("WT105" == str) *pdst = "WT111";
	}

	static void _ZJ_Fix(const tstring &str,tstring *pdst)
	{
		if("IF1012" == str)  *pdst = "IF1101";
		else if("IF1005" == str) *pdst = "IF1102";
		else if("IF1006" == str) *pdst = "IF1103";
		else if("IF1009" == str) *pdst = "IF1106";
		else
			*pdst = str;
	}

	static void _DL_Fix(const tstring &str,tstring *pdst)
	{
		if("a1007" == str)  *pdst = "A1103";
		else if("a1009" == str) *pdst = "A1105";
		else if("a1011" == str) *pdst = "A1107";
		else if("a1101" == str) *pdst = "A1109";
		else if("a1103" == str) *pdst = "A1111";
		else if("a1105" == str) *pdst = "A1201";
		else if("a1107" == str) *pdst = "A1203";
		else if("a1109" == str) *pdst = "A1205";
		else if("a1111" == str) *pdst = "A1207";
		else
			*pdst = LIB_NS::UStrConvUtils::ToUpper(str);
	}

	/* 转到交易所 */
	static inline void _ZZ_Exc(const tstring &str,tstring *pdst)
	{
		if("CF101" == str) *pdst = "CF007";
		else if("CF103" == str) *pdst = "CF009";
		else if("CF105" == str) *pdst = "CF011";
		else if("CF107" == str) *pdst = "CF101";
		else if("CF109" == str) *pdst = "CF103";
		else if("CF111" == str) *pdst = "CF105";
		else if("ER101" == str) *pdst = "ER007";
		else if("ER103" == str) *pdst = "ER009";
		else if("ER105" == str) *pdst = "ER011";
		else if("ER107" == str) *pdst = "ER101";
		else if("ER109" == str) *pdst = "ER103";
		else if("ER111" == str) *pdst = "ER105";
		else if("RO001" == str) *pdst = "RO007";
		else if("RO003" == str) *pdst = "RO009";
		else if("RO005" == str) *pdst = "RO011";
		else if("RO007" == str) *pdst = "RO101";
		else if("RO009" == str) *pdst = "RO103";
		else if("RO011" == str) *pdst = "RO105";
		else if("SR101" == str) *pdst = "SR007";
		else if("SR103" == str) *pdst = "SR009";
		else if("SR105" == str) *pdst = "SR011";
		else if("SR107" == str) *pdst = "SR101";
		else if("SR109" == str) *pdst = "SR103";
		else if("SR111" == str) *pdst = "SR105";
		else if("SR201" == str) *pdst = "SR107";
		else if("SR203" == str) *pdst = "SR109";
		else if("SR205" == str) *pdst = "SR111";
		else if("TA101" == str) *pdst = "TA007";
		else if("TA102" == str) *pdst = "TA008";
		else if("TA103" == str) *pdst = "TA009";
		else if("TA104" == str) *pdst = "TA010";
		else if("TA105" == str) *pdst = "TA011";
		else if("TA106" == str) *pdst = "TA012";
		else if("TA107" == str) *pdst = "TA101";
		else if("TA108" == str) *pdst = "TA102";
		else if("TA109" == str) *pdst = "TA103";
		else if("TA110" == str) *pdst = "TA104";
		else if("TA111" == str) *pdst = "TA105";
		else if("TA112" == str) *pdst = "TA106";
		else if("WS101" == str) *pdst = "WS007";
		else if("WS103" == str) *pdst = "WS009";
		else if("WS105" == str) *pdst = "WS011";
		else if("WS107" == str) *pdst = "WS101";
		else if("WS109" == str) *pdst = "WS103";
		else if("WS111" == str) *pdst = "WS105";
		else if("WS201" == str) *pdst = "WS107";
		else if("WS203" == str) *pdst = "WS109";
		else if("WS205" == str) *pdst = "WS111";
		else if("WT101" == str) *pdst = "WT007";
		else if("WT103" == str) *pdst = "WT009";
		else if("WT105" == str) *pdst = "WT011";
		else if("WT107" == str) *pdst = "WT101";
		else if("WT109" == str) *pdst = "WT103";
		else if("WT111" == str) *pdst = "WT105";
		else
			*pdst = str;
	}
	static void _ZJ_Exc(const tstring &str,tstring *pdst)
	{
		if("IF1101" == str) *pdst = "IF1012";
		else if("IF1103" == str) *pdst = "IF1005";
		else if("IF1102" == str) *pdst = "IF1006";
		else if("IF1106" == str) *pdst = "IF1009";
		else
			*pdst = str;
	}

	static void _DL_Exc(const tstring &str,tstring *pdst)
	{
		if("A1103" == str)  *pdst = "a1007";
		else if("A1105" == str) *pdst = "a1009";
		else if("A1107" == str) *pdst = "a1011";
		else if("A1109" == str) *pdst = "a1101";
		else if("A1111" == str) *pdst = "a1103";
		else if("A1201" == str) *pdst = "a1105";
		else if("A1203" == str) *pdst = "a1107";
		else if("A1205" == str) *pdst = "a1109";
		else if("A1207" == str) *pdst = "a1111";
			*pdst = LIB_NS::UStrConvUtils::ToLower(str);
	}

	static inline void SecDesc_I2F(RMMS_NS::Exchange exch,const tstring &idstr,
		tstring *psymbol,tstring *pdesc)
	{
		switch(exch){
		case RMMS_NS::RMMS_CN_ZZ:
			{
				tstring strtmp;
				_ZZ_Fix(idstr,&strtmp);
				std::size_t ilen = strtmp.length();
				tchar tmp;

				/* Month */
				*pdesc = strtmp.substr(ilen-2,2);
				ExchangeMonthToFix(*pdesc,&tmp);
				*pdesc = tmp;

				/* Year */
				tmp = strtmp.at(ilen-3);
				*pdesc += tmp;

				/* Return */
				*psymbol = strtmp.substr(0,ilen-3);
				*pdesc = *psymbol + *pdesc;
			}
			break;
		case RMMS_NS::RMMS_CN_DL:
			{
				tstring strtmp;
				_DL_Fix(idstr,&strtmp);
				std::size_t ilen = strtmp.length();
				tchar tmp;

				/* Month */
				*pdesc = strtmp.substr(ilen-2,2);
				ExchangeMonthToFix(*pdesc,&tmp);
				*pdesc = tmp;

				/* Year */
				tmp = strtmp.at(ilen-3);
				*pdesc += tmp;

				/* Return */
				*psymbol = strtmp.substr(0,ilen-4);
				*pdesc = *psymbol + *pdesc;

				//tstring strtmp;
				//_DL_Fix(idstr,&strtmp);
				//std::size_t ilen = strtmp.length();

				///* Exchange */
				//*pdesc = "DCE.";

				///* Symbol */
				//tstring symbol = strtmp.substr(0,ilen-4);
				//
				//*pdesc += symbol;

				///* Future */
				//*pdesc += ".F.";

				///* Year */
				//*pdesc = *pdesc + "1" + strtmp.at(ilen-3);;

				///* Month */
				//*pdesc += strtmp.substr(ilen-2,2);

				//*pdesc += "00";

				///* Return */
				//*psymbol = symbol;
			}
			break;
		case RMMS_NS::RMMS_CN_ZJ:
			{
				tstring strtmp;
				_ZJ_Fix(idstr,&strtmp);
				std::size_t ilen = strtmp.length();
				tchar tmp;

				/* Month */
				*pdesc = strtmp.substr(ilen-2,2);
				ExchangeMonthToFix(*pdesc,&tmp);
				*pdesc = tmp;

				/* Year */
				tmp = strtmp.at(ilen-3);
				*pdesc += tmp;

				*psymbol = strtmp.substr(0,ilen-4);
				*pdesc = *psymbol + *pdesc;
				
				//tstring strtmp;
				//_ZJ_Fix(idstr,&strtmp);
				//std::size_t ilen = strtmp.length();

				///* Exchange */
				//*pdesc = "CFFEX.";

				///* Symbol */
				//tstring symbol = strtmp.substr(0,ilen-4);
				//*pdesc += symbol;

				///* Future */
				//*pdesc += ".F.";

				///* Year */
				//*pdesc = *pdesc + "1" + strtmp.at(ilen-3);;

				///* Month */
				//*pdesc += strtmp.substr(ilen-2,2);

				//*pdesc += "00";

				///* Return */
				//*psymbol = symbol;
			}
			break;
		case RMMS_NS::RMMS_CN_SH:
			{
				std::size_t ilen = idstr.length();
				tchar tmp;

				/* Month */
				*pdesc = idstr.substr(ilen-2,2);
				ExchangeMonthToFix(*pdesc,&tmp);
				*pdesc = tmp;

				/* Year */
				tmp = idstr.at(ilen-3);
				*pdesc += tmp;

				/* Return */
				*psymbol = idstr.substr(0,ilen-4);
				*pdesc = *psymbol + *pdesc;
			}
			break;
		}
	}

	static inline void SecDesc_F2I(RMMS_NS::Exchange exch,const tstring &desc,
		tstring *pdesc)
	{
		switch(exch){
		case RMMS_NS::RMMS_CN_ZZ:
			{
				std::size_t ilen = desc.length();
				tchar tmp;
				tstring tmpstr;

				*pdesc = desc.substr(0,ilen-2);
				tmp = desc.at(ilen-1);
				if('2' == tmp) tmp = '0';
				else tmp = '1';
				*pdesc += tmp;

				tmp = desc.at(ilen-2);
				FixMonthToExchange(tmp,&tmpstr);
				*pdesc += tmpstr;		
				_ZZ_Exc(*pdesc,pdesc);
			}
			break;
		case RMMS_NS::RMMS_CN_DL:
			{
				std::size_t ilen = desc.length();
				tchar tmp;
				tstring tmpstr;

				*pdesc = desc.substr(0,ilen-2);
				tmp = desc.at(ilen-1);
				*pdesc += '1';
				*pdesc += tmp;

				tmp = desc.at(ilen-2);
				FixMonthToExchange(tmp,&tmpstr);
				*pdesc += tmpstr;		
				_DL_Exc(*pdesc,pdesc);
			}
			break;
		case RMMS_NS::RMMS_CN_ZJ:
			{
				std::size_t ilen = desc.length();
				tchar tmp;
				tstring tmpstr;

				*pdesc = desc.substr(0,ilen-2);
				tmp = desc.at(ilen-1);
				*pdesc += '1';
				*pdesc += tmp;

				tmp = desc.at(ilen-2);
				FixMonthToExchange(tmp,&tmpstr);
				*pdesc += tmpstr;
				_ZJ_Exc(*pdesc,pdesc);
			}
			break;
		}
	}
};

#endif