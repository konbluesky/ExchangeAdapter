#include "DlTraderAdapter.h"
#include "TradeAdapter.h"
#include "debugutils.h"
#include "LocalCompile.h"

namespace exchangeadapter_dalian
{
	CTradeAdapter::CTradeAdapter(CDLTraderAdapter *pTraderAdapter)
	{
		DBG_ASSERT(NULL != pTraderAdapter);
		m_pTraderAdapter = pTraderAdapter;
	}

	CTradeAdapter::~CTradeAdapter(void)
	{
	}

	int CTradeAdapter::onRspTraderLogout(uint32 nSeqNo,
		const _fldRspMsg & rspmsg,const _fldTraderLogoutRsp & traderlogoutrsp,
		BYTE bChainFlag)
	{
		UNREF_VAR(nSeqNo);
		UNREF_VAR(rspmsg);
		UNREF_VAR(traderlogoutrsp);
		UNREF_VAR(bChainFlag);
		return 0;
	}

	int CTradeAdapter::onRspTraderPwdUpd(uint32 nSeqNo,
		const _fldRspMsg & rspmsg,const _fldTraderPwdUpdReq & traderpwdupdreq,
		BYTE bChainFlag)
	{
		UNREF_VAR(nSeqNo);
		UNREF_VAR(rspmsg);
		UNREF_VAR(traderpwdupdreq);
		UNREF_VAR(bChainFlag);
		DBG_TRACE(("�յ���Ϣ\n"));
		return 0;
	}

	int32 CTradeAdapter::onRspTraderInsertOrders(uint32 nSeqNo,
		const _fldRspMsg & rspmsg,CAPIVector<_fldOrder> & lstOrder,
		BYTE bChainFlag)
	{
		m_pTraderAdapter->OnRspTraderInsertOrders(nSeqNo,rspmsg,
			lstOrder,bChainFlag);
		return 0;
	}

	int CTradeAdapter::onRplTraderInsertOrders(uint32 nSeqNo,
		const _fldRspMsg & rspmsg,CAPIVector<_fldOrder> & lstOrder,
		BYTE bChainFlag)
	{
		m_pTraderAdapter->OnRplTraderInsertOrders(nSeqNo,rspmsg,lstOrder,
			bChainFlag);

		return 0;
	}

	int CTradeAdapter::onTraderOrdersConfirmation(uint32 nSeqNo,
		const _fldOrderStatus & orderstatus,BYTE bChainFlag)
	{
		m_pTraderAdapter->OnTraderOrdersConfirmation(nSeqNo,orderstatus,
			bChainFlag);
		return 0;
	}

	int CTradeAdapter::onNtyTraderMatch(uint32 nSeqNo,const _fldMatch & match,
		BYTE bChainFlag)
	{
		m_pTraderAdapter->OnNtyTraderMatch(nSeqNo,match,bChainFlag);
		return 0;
	}

	int CTradeAdapter::onRspTraderCancelOrder(uint32 nSeqNo,
		const _fldRspMsg & rspmsg,const _fldOrderAction & orderaction,
		BYTE bChainFlag)
	{
		m_pTraderAdapter->OnRspTraderCancelOrder(nSeqNo,rspmsg,orderaction,
			bChainFlag);
		return 0;
	}

	int CTradeAdapter::onRplTraderCancelOrder(uint32 nSeqNo,
		const _fldRspMsg & rspmsg,const _fldOrderAction & orderaction,
		BYTE bChainFlag)
	{
		m_pTraderAdapter->OnRplTraderCancelOrder(nSeqNo,rspmsg,orderaction,
			bChainFlag);
		return 0;
	}

	int CTradeAdapter::onRspTraderOptExec(uint32 nSeqNo,
		const _fldRspMsg & rspmsg,const _fldOrder & order,
		BYTE bChainFlag)
	{
		UNREF_VAR(nSeqNo);
		UNREF_VAR(rspmsg);
		UNREF_VAR(order);
		UNREF_VAR(bChainFlag);
		return 0;
	}

	int CTradeAdapter::onRspTraderCancelOptExec(uint32 nSeqNo,
		const _fldRspMsg & rspmsg,const _fldOrder & order,BYTE bChainFlag)
	{
		UNREF_VAR(nSeqNo);
		UNREF_VAR(rspmsg);
		UNREF_VAR(order);
		UNREF_VAR(bChainFlag);
		return 0;
	}

	int CTradeAdapter::onRspTraderQryOptExec(uint32 nSeqNo,\
		const _fldRspMsg & rspmsg,CAPIVector<_fldOrderStatus> & lstOrderStatus,
		BYTE bChainFlag)
	{
		UNREF_VAR(nSeqNo);
		UNREF_VAR(rspmsg);
		UNREF_VAR(lstOrderStatus);
		UNREF_VAR(bChainFlag);
		return 0;
	}

	int CTradeAdapter::onRspTraderQryOrder(uint32 nSeqNo,
		const _fldRspMsg & rspmsg,CAPIVector<_fldOrderStatus> & lstOrderStatus,
		BYTE bChainFlag)
	{
		return m_pTraderAdapter->onRspTraderQryOrder(nSeqNo,rspmsg,
			lstOrderStatus,bChainFlag);
	}

	int CTradeAdapter::onRspTraderQryMatch(uint32 nSeqNo,
		const _fldRspMsg & rspmsg,CAPIVector<_fldMatch> & lstMatch,
		BYTE bChainFlag)
	{
		UNREF_VAR(nSeqNo);
		UNREF_VAR(rspmsg);
		UNREF_VAR(lstMatch);
		UNREF_VAR(bChainFlag);
		return 0;
	}

	int CTradeAdapter::onRspTraderQryClientPosi(uint32 nSeqNo,
		const _fldRspMsg & rspmsg,CAPIVector<_fldPosi> & lstPosi,
		BYTE bChainFlag)
	{
		UNREF_VAR(nSeqNo);
		UNREF_VAR(rspmsg);
		UNREF_VAR(lstPosi);
		UNREF_VAR(bChainFlag);
		return 0;
	}

	int CTradeAdapter::onRspTraderQryMemberPosi(uint32 nSeqNo,
		const _fldRspMsg & rspmsg,CAPIVector<_fldPosi> & lstPosi,
		BYTE bChainFlag)
	{
		UNREF_VAR(nSeqNo);
		UNREF_VAR(rspmsg);
		UNREF_VAR(lstPosi);
		UNREF_VAR(bChainFlag);
		return 0;
	}

	int CTradeAdapter::onRspTraderQryMemberCap(uint32 nSeqNo,
		const _fldRspMsg & rspmsg,const _fldMemberCap & membercap,
		BYTE bChainFlag)
	{
		UNREF_VAR(nSeqNo);
		UNREF_VAR(rspmsg);
		UNREF_VAR(membercap);
		UNREF_VAR(bChainFlag);
		return 0;
	}

	int CTradeAdapter::onRspTraderQryParam(uint32 nSeqNo,
		const _fldRspMsg & rspmsg,CAPIVector<_fldTradeParam> & lstTradeParam,
		BYTE bChainFlag)
	{
		UNREF_VAR(nSeqNo);
		UNREF_VAR(rspmsg);
		UNREF_VAR(lstTradeParam);
		UNREF_VAR(bChainFlag);
		return 0;
	}

	int CTradeAdapter::onBestQuot(uint32 nSeqNo,const _fldBestQuot & bestquot,
		BYTE bChainFlag)
	{
		UNREF_VAR(nSeqNo);
		UNREF_VAR(bestquot);
		UNREF_VAR(bChainFlag);
		return 0;
	}

	int CTradeAdapter::onArbiBestQuot(uint32 nSeqNo,
		const _fldArbiBestQuot & arbibestquot,BYTE bChainFlag)
	{
		UNREF_VAR(nSeqNo);
		UNREF_VAR(arbibestquot);
		UNREF_VAR(bChainFlag);
		return 0;
	}

	int CTradeAdapter::onNtySnapShotQuot(uint32 nSeqNo,
		const _fldQuotQryReq & quotqryreq,BYTE bChainFlag)
	{
		UNREF_VAR(nSeqNo);
		UNREF_VAR(quotqryreq);
		UNREF_VAR(bChainFlag);
		return 0;
	}

	int CTradeAdapter::onRspQryMktStatus(uint32 nSeqNo,
		const _fldRspMsg & rspmsg,const _fldMktStatus & mktstatus,
		BYTE bChainFlag)
	{
		UNREF_VAR(nSeqNo);
		UNREF_VAR(rspmsg);
		UNREF_VAR(mktstatus);
		UNREF_VAR(bChainFlag);
		return 0;
	}

	int CTradeAdapter::onNtyMktStatus(uint32 nSeqNo,
		const _fldMktStatus & mktstatus,BYTE bChainFlag)
	{
		UNREF_VAR(nSeqNo);
		UNREF_VAR(mktstatus);
		UNREF_VAR(bChainFlag);
		return 0;
	}

	int CTradeAdapter::onNtySendNotice(uint32 nSeqNo,
		const _fldPromptTrader & prompttrader,BYTE bChainFlag)
	{
		UNREF_VAR(nSeqNo);
		UNREF_VAR(prompttrader);
		UNREF_VAR(bChainFlag);
		return 0;
	}

	int CTradeAdapter::onNtyClientReg(uint32 nSeqNo,const _fldClient & client,
		BYTE bChainFlag)
	{
		UNREF_VAR(nSeqNo);
		UNREF_VAR(client);
		UNREF_VAR(bChainFlag);
		return 0;
	}

	int CTradeAdapter::onNtyMemberCapPosi(uint32 nSeqNo,
		const _fldMemberCap & membercap,CAPIVector<_fldPosi> & lstPosi,
		BYTE bChainFlag)
	{
		UNREF_VAR(nSeqNo);
		UNREF_VAR(membercap);
		UNREF_VAR(lstPosi);
		UNREF_VAR(bChainFlag);
		return 0;
	}

	int CTradeAdapter::onRspConstQuot(uint32 nSeqNo,const _fldRspMsg & rspmsg,
		const _fldConstQuot & constquot,BYTE bChainFlag)
	{
		UNREF_VAR(nSeqNo);
		UNREF_VAR(rspmsg);
		UNREF_VAR(constquot);
		UNREF_VAR(bChainFlag);
		return 0;
	}

	int CTradeAdapter::onInvalidPackage(uint32 nTID,uint16 nSeries,
		uint32 nSequenceNo,uint16 nFieldCount,uint16 nFieldsLen,
		const tchar *pAddr)
	{
		UNREF_VAR(nTID);
		UNREF_VAR(nSeries);
		UNREF_VAR(nSequenceNo);
		UNREF_VAR(nFieldCount);
		UNREF_VAR(nFieldsLen);
		UNREF_VAR(pAddr);
		return 0;
	}

	void CTradeAdapter::onChannelLost(const tchar *szErrMsg)
	{
		m_pTraderAdapter->OnChannelLost(szErrMsg);
	}
}

