#include "DLEnumsConvertHelper.h"
#include "localcompile.h"

namespace exchangeadapter_dalian
{
	uint8 DLEnumsConvertHelper::Convert2ExchOpenClose(
		OpenCloseFlag rmmOpenClose)
	{
		switch(rmmOpenClose){
		case Open: 
			return uint8('1');
		case Close:
			return uint8('2');
		case ForceClose:
			return uint8('3');
		default:
			return uint8('1');
		}
	}

	uint8 DLEnumsConvertHelper::Convert2ExchOrderType(
		OrderTypeFlag rmmOrderType)
	{
		switch(rmmOrderType){
		case Market: 
			return uint8('1');
		case Limit: 
			return uint8('0');
		case Stop:
			return uint8('2');
		case Stop_Limit:
			return uint8('4');
		default:
			return uint8('1');
		}
	}	

	uint8 DLEnumsConvertHelper::Convert2ExchSide(TradeSide rmmTradeSide)
	{
		switch(rmmTradeSide){
		case BUY:
			return uint8('1');
		case SELL: 
			return uint8('3');
		default: 
			return uint8('1');
		}
	}

	void DLEnumsConvertHelper::Convert2RmmOpenClose(uint8 eoFlag,
		OpenCloseFlag rmmOpenClose)
	{
		switch(eoFlag){
		case '1':
			rmmOpenClose = Open;
			break;
		case '2':
			rmmOpenClose = Close;
			break;
		case '3':
			rmmOpenClose = ForceClose;
			break;
		default:
			rmmOpenClose = Open;
			break;
		}		
	}

	void DLEnumsConvertHelper::Convert2RmmOrderType(uint8 orderType,
		OrderTypeFlag rmmOrderType)
	{
		switch(orderType){
		case '1':
			rmmOrderType = Market;
			break;
		case '0':
			rmmOrderType = Limit;
			break;
		case '2':
			rmmOrderType = Stop;
			break;
		case '4':
			rmmOrderType = Stop_Limit;
			break;
		default:
			break;
		}		
	}

	void DLEnumsConvertHelper::Convert2RmmSide(uint8 bsFlag,
		TradeSide rmmTradeSide)
	{
		switch(bsFlag){
		case '1':
			rmmTradeSide = BUY;
			break;
		case '3':
			rmmTradeSide = SELL;
			break;
		default:
			break;
		}		
	}	
}
