/* ------------------------------------------------------------------
 * filename:    kscrypt.h
 * version:     1.0.0.0
 * created:     2005-10-28 11:14
 * author:		shigw
 * purpose:		加密函数的封装
 * ------------------------------------------------------------------ 
 * Date
 * ------------------------------------------------------------------ 
*/
#ifndef __S_CRYPT_H
#define __S_CRYPT_H
#include "mytypes.h"
#include "_s_types.h"
/* ------------------------------------------------------------------
 * 支持的加密算法和摘要算法
 * ------------------------------------------------------------------
*/
#define KCS_CODE_PLAIN		0
#define KCS_CODE_BLOWFISH	1
#define KCS_CODE_RC4		2
#define KCS_CODE_DES3		3
#define KCS_CODE_RC2		4
#define KCS_CODE_IDEA		5
//#define KCS_CODE_RC5		6		not support rc5
#define KCS_CODE_CAST		7
#define KCS_CODE_DES		8

/* Client/Server Communication Encrypt/Decrypt
*/
#define KCS_MD_MD5	1
#define KCS_MD_SHA	2
#define KCS_MD_SHA1	3
#define KCS_MD_MD2	4

class CCryptApi
{
public:
	CCryptApi();
	~CCryptApi();
	/* ------------------------------------------------------------------
	 * 设定算法类型
	 * ------------------------------------------------------------------
	 * iCipher:					算法类型
	 *	 KCS_CODE_PLAIN			不加密
	 *	 KCS_CODE_BLOWFISH		BLOWFISH算法
	 *	 KCS_CODE_RC4			RC4算法
	 *	 KCS_CODE_DES3			DES3算法
	 *	 KCS_CODE_RC2			RC2算法
	 *	 KCS_CODE_IDEA			IDEA算法
	 *	 KCS_CODE_CAST			CAST算法
	 *	 KCS_CODE_DES			DES算法
	 * pKey:					密钥
	 *	 KCS_CODE_PLAIN			无
	 *	 KCS_CODE_BLOWFISH		16位（16x8二进制位）
	 *	 KCS_CODE_RC4			16位（16x8二进制位）
	 *	 KCS_CODE_DES3			24位（24x8二进制位）
	 *	 KCS_CODE_RC2			16位（16x8二进制位）
	 *	 KCS_CODE_IDEA			16位（16x8二进制位）
	 *	 KCS_CODE_CAST			16位（16x8二进制位）
	 *	 KCS_CODE_DES			8位 （8x8二进制位）
	 * ------------------------------------------------------------------
	*/
	BOOL		SetCipher(
		_IN_	int				 iCipher,
		_IN_	char	const	*pKey,
		_OUT_	int				*piErrCode,
		_OUT_	char			*psErrMsg);
	/* ------------------------------------------------------------------
	 * 加密
	 * ------------------------------------------------------------------
	 * pIn:		被加密串
	 * iLen:	被加密串长度
	 * pOut:	输出加密串
	 * ------------------------------------------------------------------
	*/
	void		Encrypt(
		_IN_	void	const	*pIn,
		_IN_	int				 iLen,
		_OUT_	void			*pOut);
	/* ------------------------------------------------------------------
	 * 解密
	 * ------------------------------------------------------------------
	 * pIn:		被解密串
	 * iLen:	被解密串长度
	 * pOut:	输出解密串
	 * ------------------------------------------------------------------
	*/
	void		Decrypt(
		_IN_	void	const	*pIn,
		_IN_	int				 iLen,
		_OUT_	void			*pOut);
	/* ------------------------------------------------------------------
	 * 摘要
	 * ------------------------------------------------------------------
	 * iMode:	摘要模式
	 *	KCS_MD_MD5	
	 *	KCS_MD_SHA	
	 *	KCS_MD_SHA1	
	 *	KCS_MD_MD2	
	 * pBuff:	被摘要串
	 * iLen:	被摘要串长度
	 * pOut:	输出摘要串
	 * ------------------------------------------------------------------
	*/
	void		MD(
		_IN_	int				 iMode,
		_IN_	void	const	*pBuff,
		_IN_	int				 iLen,
		_IN_	char			*pMd);
	/* ------------------------------------------------------------------
	 * 产生随机密钥
	 * ------------------------------------------------------------------
	 * pBuff:	输出buff
	 * iLen:	需要产生的长度
	 * ------------------------------------------------------------------
	*/
	void		GenKey(
		_OUT_	void			*pBuff,
		_IN_	int				 iLen);
private:
	int			 m_iCipherMode;
#ifdef SSLINCLUDED
	union{
		struct st_noname{
			BF_KEY		 m_bf_ks;
			RC4_KEY		 m_rc4_ks_enc;
			RC4_KEY		 m_rc4_ks_dec;
			des_key_schedule	m_sch1,m_sch2,m_sch3;
			RC2_KEY		 m_rc2_ks;
			IDEA_KEY_SCHEDULE	m_idea_e_ks;
			IDEA_KEY_SCHEDULE	m_idea_d_ks;
			CAST_KEY	 m_cast_ks;
			unsigned	char	m_en_iv[32];
			unsigned	char	m_de_iv[32];
		}m_st;
		char		 m_dummy[20480];
	}m_noname;
#else
	char		 m_dummy[20480];
#endif

#ifdef	unix
	char		 randf_path[256];
	char const	*randpath;
#endif
	char		 m_key[1024];

	void		 f_init();
	void		 f_done();
};
#endif
