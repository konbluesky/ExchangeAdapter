#ifndef API_STRUCT_H
#define API_STRUCT_H
/********************************************************
 * 版权所有 (C)2004－2008, 
 * 
 * 文件名称：APIStruct.h
 * 文件标识：定义了数据域所对应的结构
 * 内容摘要：
 * 其它说明：
 * 当前版本：
 * 作    者：何 荣
 * 完成日期：2005-12-13 16:04:11
 * 
 * 修改记录：
 *			修改日期		修 改 人		修改内容
 *			2005-12-13		何  荣		创建文件
 *********************************************************/
#include "ExchangeDataType.h"

#define	READY		1//就绪
#define	NOREADY		0//未就绪

#define CHAIN_SINGLE	'S'		//单个报文
#define CHAIN_CENTRIC	'C'		//中间报文
#define CHAIN_LAST 		'L'		//最后一个报文
#define CHAIN_FIRST		'F'		//第一个报文

#define D_FILECERT	0			//文件证书
#define D_USBCERT	1			//USB证书


/* 响应域 */
struct _fldRspMsg
{
	INT4           ErrCode;             /*错误码*/
	BYTEARRAY<40>  RspMsg;              /*错误描述信息*/
	BYTEARRAY<19>  TimeStamp;           /*时间戳*/
};

/* 客户信息域 */
struct _fldClient
{
	BYTEARRAY<16>  ClientID;            /*客户号*/
	BYTEARRAY<8>   MemberID;            /*会员号*/
	BYTEARRAY<80>  ClientName;          /*客户名称*/
	BYTE           ClientSort;          /*客户类别*/
	BYTE           ClientProperty;      /*客户性质*/
};

/* 定单信息域 */
struct _fldOrder
{
	UINT4          SysOrderNo;          /*系统委托号*/
	UINT4          LocalOrderNo;        /*本地委托号*/
	UINT4          TID;                 /*事务编号*/
	BYTEARRAY<16>  TraderNo;            /*委托席位号*/
	BYTEARRAY<8>   MemberID;            /*会员编号*/
	BYTEARRAY<16>  ClientID;            /*客户编号*/
	BYTEARRAY<80>  ContractID;          /*合约号*/
	BYTE           BsFlag;              /*买卖标志*/
	BYTE           EoFlag;              /*开平标志*/
	BYTE           ShFlag;              /*投保标志*/
	REAL8          StopPrice;           /*止损(盈)价*/
	REAL8          Price;               /*价格*/
	UINT4          Qty;                 /*数量*/
	BYTE           OrderType;           /*定单类型*/
	BYTE           OrderAttr;           /*定单属性*/
	INT1           MatchSession;        /*报单成交时间(空)*/
	BYTEARRAY<8>   ValidThrough;        /*有效时间约束(空)*/
	UINT4          MinQty;              /*最小成交量(空)*/
	INT1           AutoSuspend;         /*自动挂起标志(空)*/
	BYTEARRAY<12>  OrderTime;           /*委托时间*/
	BYTEARRAY<6>   MsgRef;              /*用户自定义数据。可打印ASCII字符(空)*/
	BYTE           OrderSort;           /*定单类别*/
	BYTE           OrderSrc;            /*定单来源*/
	BYTE           ForceOffsetReason;   /*强平原因*/
	BYTEARRAY<80>  ArbiContractID;      /*套利合约号*/
	UINT4          OrderBatchNo;        /*委托批次号*/
	INT1           IsMktMk;             /*是否做市商下单*/
	BYTEARRAY<14>  ClearAccountNo;      /*结算帐号*/
	BYTE           TradeType;           /*交易类型*/
};

/* 定单状态域 */
struct _fldOrderStatus
{
	UINT4          SysOrderNo;          /*系统委托号*/
	UINT4          LocalOrderNo;        /*本地委托号*/
	BYTEARRAY<16>  TraderNo;            /*委托席位号*/
	BYTEARRAY<8>   MemberID;            /*会员编号*/
	BYTEARRAY<16>  ClientID;            /*客户编号*/
	BYTEARRAY<80>  ContractID;          /*合约号*/
	BYTE           BsFlag;              /*买卖标志*/
	BYTE           EoFlag;              /*开平标志*/
	BYTE           ShFlag;              /*投保标志*/
	REAL8          StopPrice;           /*止损(盈)价*/
	REAL8          Price;               /*价格*/
	BYTE           OrderType;           /*定单类型*/
	BYTE           OrderAttr;           /*定单属性*/
	INT1           MatchSession;        /*报单成交时间(空)*/
	BYTEARRAY<8>   ValidThrough;        /*有效时间约束（空）*/
	UINT4          MinQty;              /*最小成交量（空）*/
	INT1           AutoSuspend;         /*自动挂起标志（空）*/
	BYTEARRAY<12>  OrderTime;           /*委托时间*/
	BYTEARRAY<6>   MsgRef;              /*用户自定义数据。可打印ASCII字符（空）*/
	BYTE           OrderSort;           /*定单类别*/
	BYTE           OrderSrc;            /*定单来源*/
	BYTE           OrderStatus;         /*定单状态*/
	UINT4          MatchQty;            /*成交量*/
	BYTEARRAY<12>  TrigTime;            /*触发时间*/
	BYTEARRAY<12>  CancelTime;          /*撤单时间*/
	BYTEARRAY<16>  CancelTraderNo;      /*撤单席位号*/
	BYTE           ForceOffsetReason;   /*强平原因*/
	BYTEARRAY<80>  ArbiContractID;      /*套利合约号*/
	UINT4          OrderBatchNo;        /*委托批次号*/
	INT1           IsMktMk;             /*是否做市商下单*/
	BYTEARRAY<14>  ClearAccountNo;      /*结算帐号*/
	UINT4          QtyTotOrginal;       /*原始总申报数量（以手为单位）*/
	UINT4          QtyTot;              /*剩余总申报数量（以手为单位）*/
	BYTEARRAY<12>  SuspendTime;         /*挂起时间（空）*/
	BYTEARRAY<12>  UpdTime;             /*最后修改时间*/
	REAL8          Margin;              /*保证金*/
	UINT4          ForceGroupID;        /*强平组号（空）*/
	REAL8          LastMatchPrice;      /*最新成交价格*/
	BYTE           TradeType;           /*交易类型*/
};

/* 成交单信息域 */
struct _fldMatch
{
	BYTEARRAY<80>  ContractID;          /*合约号*/
	INT1           ContractVersion;     /*合约版本号（空）*/
	UINT4          TID;                 /*事务编号*/
	INT1           CancelFlag;          /*成交是否被取消（空）*/
	BYTEARRAY<8>   CancelDate;          /*取消日期（空）*/
	BYTEARRAY<12>  CancelTime;          /*取消时间（空）*/
	UINT4          MatchNo;             /*成交号*/
	BYTEARRAY<8>   MatchDate;           /*成交日期*/
	BYTEARRAY<12>  MatchTime;           /*成交时间*/
	BYTEARRAY<8>   ClearDate;           /*清算日期*/
	REAL8          Price;               /*成交价格*/
	UINT4          Qty;                 /*成交数量*/
	UINT4          ArbiQty;             /*套利成交手数*/
	UINT4          SysOrderNo;          /*系统委托号*/
	BYTEARRAY<16>  TraderNo;            /*席位号*/
	BYTE           BsFlag;              /*买卖标志*/
	BYTE           EoFlag;              /*开平标志*/
	BYTE           ShFlag;              /*投保标志*/
	BYTEARRAY<8>   MemberID;            /*会员号*/
	BYTEARRAY<16>  ClientID;            /*客户号*/
	UINT4          LocalID;             /*报单本地编码*/
	BYTEARRAY<80>  ArbiContractID;      /*套利合约号*/
	UINT4          OrderBatchNo;        /*委托批次号*/
	UINT4          MatchBatchNo;        /*成交批次号*/
	BYTE           OrderSrc;            /*定单来源*/
};

/* 持仓域 */
struct _fldPosi
{
	BYTEARRAY<8>   MemberID;            /*会员号*/
	BYTEARRAY<16>  ClientID;            /*客户号*/
	BYTEARRAY<20>  ContractID;          /*合约号*/
	INT1           ContractVersion;     /*合约版本号(空)*/
	BYTEARRAY<8>   TradeDate;           /*交易日期*/
	UINT4          YdBuyHedgeQty;       /*前买持仓量(保)*/
	UINT4          YdBuySpecQty;        /*前买持仓量(投)*/
	UINT4          YdSellHedgeQty;      /*前卖持仓量(保)*/
	UINT4          YdSellSpecQty;       /*前卖持仓量(投)*/
	UINT4          BuyMatchHedgeQty;    /*买成交量(保)*/
	UINT4          BuyMatchSpecQty;     /*买成交量(投)*/
	UINT4          SellMatchHedgeQty;   /*卖成交量(保)*/
	UINT4          SellMatchSpecQty;    /*卖成交量(投)*/
	UINT4          BuyOpenHedgeQty;     /*买开仓量(保)*/
	UINT4          BuyOpenSpecQty;      /*买开仓量(投)*/
	UINT4          SellOpenHedgeQty;    /*卖开仓量(保)*/
	UINT4          SellOpenSpecQty;     /*卖开仓量(投)*/
	UINT4          BuyOffsetHedgeQty;   /*买平今量(保)*/
	UINT4          BuyOffsetSpecQty;    /*买平今量(投)*/
	UINT4          SellOffsetHedgeQty;  /*卖平今量(保)*/
	UINT4          SellOffsetSpecQty;   /*卖平今量(投)*/
	UINT4          YdBuyOffsetHedgeQty; /*买平昨量(保)*/
	UINT4          YdBuyOffsetSpecQty;  /*买平昨量(投)*/
	UINT4          YdSellOffsetHedgeQty;/*卖平昨量(保)*/
	UINT4          YdSellOffsetSpecQty; /*卖平昨量(投)*/
	UINT4          BuyHedgeQty;         /*买持仓量(保)*/
	UINT4          BuySpecQty;          /*买持仓量(投)*/
	UINT4          SellHedgeQty;        /*卖持仓量(保)*/
	UINT4          SellSpecQty;         /*卖持仓量(投)*/
	REAL8          BuyHedgeAmt;         /*买持仓金额(保)*/
	REAL8          BuySpecAmt;          /*买持仓金额(投)*/
	REAL8          SellHedgeAmt;        /*卖持仓金额(保)*/
	REAL8          SellSpecAmt;         /*卖持仓金额(投)*/
	UINT4          BuyOpenFrozenHedgeQty;/*多头开仓冻结持仓手（保）*/
	UINT4          BuyOpenFrozenSpecQty;/*多头开仓冻结持仓手（投）*/
	UINT4          SellOpenFrozenHedgeQty;/*空头开仓冻结持仓手（保）*/
	UINT4          SellOpenFrozenSpecQty;/*空头开仓冻结持仓手（投）*/
	UINT4          BuyOffsetFrozenHedgeQty;/*多头平仓冻结持仓手(保)*/
	UINT4          BuyOffsetFrozenSpecQty;/*多头平仓冻结持仓手(投)*/
	UINT4          SellOffsetFrozenHedgeQty;/*空头平仓冻结持仓手(保)*/
	UINT4          SellOffsetFrozenSpecQty;/*空头平仓冻结持仓手(投)*/
	UINT4          YdBuyOffsetFrozenHedgeQty;/*上日多头平仓冻结持仓手(保)*/
	UINT4          YdBuyOffsetFrozenSpecQty;/*上日多头平仓冻结持仓手(投)*/
	UINT4          YdSellOffsetFrozenHedgeQty;/*上日空头平仓冻结持仓手(保)*/
	UINT4          YdSellOffsetFrozenSpecQty;/*上日空头平仓冻结持仓手(投)*/
};

/* 直接市场最优行情域 */
struct _fldBestQuot
{
	BYTEARRAY<8>   TradeDate;           /*交易日期*/
	BYTEARRAY<20>  ContractID;          /*合约号*/
	UINT4          TID;                 /*事务编号*/
	BYTEARRAY<40>  ContractName;        /*合约名称*/
	REAL8          LastPrice;           /*最新价*/
	REAL8          HighPrice;           /*最高价*/
	REAL8          LowPrice;            /*最低价*/
	UINT4          LastMatchQty;        /*最新成交量*/
	UINT4          MatchTotQty;         /*成交量*/
	REAL8          Turnover;            /*成交额*/
	UINT4          InitOpenInterest;    /*初始持仓量*/
	UINT4          OpenInterest;        /*持仓量*/
	INT4           InterestChg;         /*持仓量变化*/
	REAL8          ClearPrice;          /*今结算价*/
	REAL8          LifeLow;             /*历史最低价*/
	REAL8          LifeHigh;            /*历史最高价*/
	REAL8          RiseLimit;           /*涨停板*/
	REAL8          FallLimit;           /*跌停板*/
	REAL8          LastClearPrice;      /*上日结算价*/
	REAL8          LastClose;           /*上日收盘价*/
	REAL8          BidPrice;            /*最高买*/
	UINT4          BidQty;              /*申买量*/
	UINT4          BidImplyQty;         /*申买推导量*/
	REAL8          AskPrice;            /*最低卖*/
	UINT4          AskQty;              /*申卖量*/
	UINT4          AskImplyQty;         /*申卖推导量*/
	REAL8          AvgPrice;            /*当日均价*/
	BYTEARRAY<12>  GenTime;             /*生成时间*/
	REAL8          OpenPrice;           /*开盘价*/
	REAL8          ClosePrice;          /*收盘价*/
};

/* 套利最优行情域 */
struct _fldArbiBestQuot
{
	BYTEARRAY<8>   TradeDate;           /*交易日期*/
	BYTEARRAY<80>  ArbiContractID;      /*套利合约号*/
	UINT4          TID;                 /*事务编号*/
	REAL8          LastPrice;           /*最新价*/
	REAL8          LowPrice;            /*最低价*/
	REAL8          HighPrice;           /*最高价*/
	REAL8          LifeLow;             /*历史最低价*/
	REAL8          LifeHigh;            /*历史最高价*/
	REAL8          RiseLimit;           /*涨停板*/
	REAL8          FallLimit;           /*跌停板*/
	REAL8          BidPrice;            /*最高买*/
	UINT4          BidQty;              /*申买量*/
	REAL8          AskPrice;            /*最低卖*/
	UINT4          AskQty;              /*申卖量*/
	BYTEARRAY<8>   GenTime;             /*生成时间*/
};

/* 深度行情域 */
struct _fldMBLQuot
{
	REAL8          OrderPrice;          /*价格*/
	INT4           OrderQty;            /*委托量*/
	INT4           ImplyQty;            /*推导量*/
	BYTE           BsFlag;              /*买卖标志*/
	BYTEARRAY<12>  GenTime;             /*生成时间*/
};

/* 期权参数域 */
struct _fldOptPara
{
	BYTEARRAY<8>   TradeDate;           /*交易日期*/
	BYTEARRAY<20>  ContractID;          /*期权合约号*/
	REAL8          Delta;               /*delta*/
	REAL8          Gamma;               /*gama*/
	REAL8          Rho;                 /*rho*/
	REAL8          Theta;               /*theta*/
	REAL8          Vega;                /*vega*/
};

/* 结算价域 */
struct _fldClearPrice
{
	BYTEARRAY<20>  ContractID;          /*合约号*/
	REAL8          LastClearPrice;      /*上日结算价*/
	REAL8          ClearPrice;          /*今结算价*/
	BYTE           Status;              /*交易所状态*/
};

/* 品种查询请求域 */
struct _fldVarietyQryReq
{
	BYTEARRAY<4>   VarietyID;           /*品种代码*/
	BYTE           TradeType;           /*交易类型*/
};

/* 期货品种域 */
struct _fldFtrVariety
{
	BYTEARRAY<4>   VarietyID;           /*品种代码*/
	BYTEARRAY<20>  Name;                /*品种名称*/
	BYTEARRAY<2>   Shortcut;            /*品种快捷输入*/
	UINT4          Unit;                /*交易单位数量*/
	REAL8          Tick;                /*最小变动价位*/
	UINT4          MaxHand;             /*最大下单手数*/
	UINT4          MinHand;             /*最小下单手数*/
	UINT4          LastPos;             /*期货合约最后交易日在交割月的位置*/
	REAL8          NoRiseRate;          /*无成交涨停板率*/
	REAL8          NoFallRate;          /*无成交跌停板率*/
	REAL8          RiseRate;            /*已成交涨停板率*/
	REAL8          FallRate;            /*已成交跌停板率*/
	REAL8          DeliveryRiseRate;    /*交割月涨停板率*/
	REAL8          DeliveryFallRate;    /*交割月跌停板率*/
	BYTE           Status;              /*品种状态*/
	BYTEARRAY<8>   LogoutDate;          /*摘牌日期*/
};

/* 期权品种域 */
struct _fldOptVariety
{
	BYTEARRAY<4>   VarietyID;           /*品种代码*/
	BYTEARRAY<20>  Name;                /*品种名称*/
	REAL8          Tick;                /*最小变动价位*/
	UINT4          MaxHand;             /*最大下单手数*/
	UINT4          MinHand;             /*最小下单手数*/
	UINT4          BeforeDeliveryPos;   /*最后交易日在期货合约交割月的前几个月*/
	UINT4          LastPos;             /*期货合约最后交易日在交割月的位置*/
	UINT4          ExpirationPos;       /*合约到期日距最后交易日的位置*/
	BYTE           AddType;             /*期权合约增加方式*/
	REAL8          Space;               /*执行价格间距*/
	UINT4          CoverQty;            /*执行价格覆盖涨跌停板的个数*/
	BYTE           Status;              /*品种状态*/
	BYTEARRAY<8>   LogoutDate;          /*摘牌日期*/
};

/* 合约查询请求域 */
struct _fldContractQryReq
{
	BYTEARRAY<80>  ContractID;          /*合约号*/
};

/* 期货合约域 */
struct _fldFtrContract
{
	BYTEARRAY<10>  MktID;               /*市场编码（空）*/
	BYTEARRAY<8>   ContractID;          /*合约编码*/
	INT1           ContractVersion;     /*合约版本号（空）*/
	BYTEARRAY<40>  ContractName;        /*合约名称*/
	INT1           ShortCutKey;         /*输入快捷键（空）*/
	BYTEARRAY<8>   StartTradeDate;      /*开始交易日*/
	BYTEARRAY<8>   EndTradeDate;        /*最后交易日*/
	INT1           ContractType;        /*合约类型(空)*/
	BYTEARRAY<8>   EndDeliveryDate;     /*最后交割日（空）*/
	BYTEARRAY<8>   StartDeliveryDate;   /*开始交割日*/
	INT1           ContractStopCode;    /*停止交易原因(空)*/
	BYTEARRAY<3>   Currency;            /*交易用的货币（空）*/
	INT1           FuseRights;          /*可否熔断（空）*/
	UINT4          Unit;                /*每手乘数*/
	REAL8          Tick;                /*最小价位*/
	REAL8          RiseLimit;           /*涨停板*/
	REAL8          FallLimit;           /*跌停板*/
	UINT4          MaxHand;             /*最大可下单手数*/
	UINT4          MinHand;             /*最小可下单手数*/
	INT1           PubStyle;            /*竞价阶段合约行情发布方式*/
	REAL8          FuseLimit;           /*熔断限额（空）*/
	UINT4          FusePersistTime;     /*熔断最大持续时间（空）*/
	BYTEARRAY<6>   FusePoint;           /*在该时间点后不可熔断（空）*/
	INT1           FuseFlag;            /*熔断标记（空）*/
	UINT4          Forcedistime;        /*强平的撮合间隔（空）*/
	BYTE           Status;              /*合约状态*/
	BYTEARRAY<6>   DeliveryMonth;       /*交割月*/
	BYTEARRAY<4>   VarietyId;           /*品种代码*/
	REAL8          RefPrice;            /*挂牌基准价*/
};

/* 期权合约域 */
struct _fldOptContract
{
	BYTEARRAY<20>  ContractID;          /*合约号*/
	BYTEARRAY<8>   SeriesID;            /*期权系列号*/
	BYTE           CpFlag;              /*看涨看跌标志*/
	REAL8          StrikePrice;         /*执行价格*/
	BYTEARRAY<6>   DeliveryMonth;       /*交割月*/
	UINT4          Unit;                /*交易单位数量*/
	REAL8          Tick;                /*最小变动价位*/
	UINT4          MaxHand;             /*最大下单手数*/
	UINT4          MinHand;             /*最小下单手数*/
	BYTEARRAY<8>   StartTradeDate;      /*开始交易日*/
	BYTEARRAY<8>   EndTradeDate;        /*最后交易日*/
	BYTEARRAY<8>   ExpirationDate;      /*合约到期日*/
	REAL8          RiseLimit;           /*涨停板*/
	REAL8          FallLimit;           /*跌停板*/
	REAL8          RefPrice;            /*挂牌基准价*/
	BYTE           Status;              /*合约状态*/
	BYTEARRAY<4>   VarietyId;           /*品种代码*/
};

/* 套利合约域 */
struct _fldArbiContract
{
	BYTEARRAY<80>  ArbiContractID;      /*套利合约号*/
	BYTEARRAY<10>  ArbiCode;            /*策略代码*/
	UINT4          LegQty;              /*腿数量*/
	REAL8          RiseLimit;           /*报价上限*/
	REAL8          FallLimit;           /*报价下限*/
	REAL8          Tick;                /*最小变动价位*/
	UINT4          MaxHand;             /*最大下单手数*/
	UINT4          MinHand;             /*最小下单手数*/
};

/* 套利合约单腿域 */
struct _fldArbiLeg
{
	BYTEARRAY<20>  ContractID;          /*合约号*/
	BYTE           BsFlag;              /*买卖标志*/
	UINT4          Ratio;               /*数量比例*/
	UINT4          LegNo;               /*腿号*/
	INT4           CalcWay;             /*计算方式*/
};

/* 数据流起始分发域 */
struct _fldDissemination
{
	INT2           SeqSeries;           /*标示FTCP包的序列类别号*/
	UINT4          StorageSeqNo;        /*表示FTCP包的序列号(Flow中的消息编号)*/
};

/* 交易员会话请求域 */
struct _fldTraderSessionReq
{
	BYTEARRAY<8>   MemberID;            /*会员号*/
	BYTEARRAY<16>  TraderNo;            /*席位编号*/
	INT1           IsShortCert;         /*是否快捷认证*/
	BYTEARRAY<40>  ShortCode;           /*快捷码*/
	BYTEARRAY<40>  ProgramID;           /*程序代号*/
	BYTEARRAY<20>  ProgramVer;          /*程序版本号*/
};

/* 交易员会话应答域 */
struct _fldTraderSessionRsp
{
	BYTEARRAY<40>  SessionID;           /*会话ID*/
	INT1           CertOrNot;           /*如果需要认证,则SRandomString有效，否则SymmetricKey有效(用席位号DES加密)*/
	BYTEARRAY<20>  SRandomString;       /*服务端产生的随机串*/
	BYTEARRAY<256> SymmetricKey;        /*对称密钥*/
	INT4           SymKeyLength;        /*对称密钥的长度*/
};

/* 交易员认证请求域 */
struct _fldTraderCertReq
{
	BYTEARRAY<40>  SessionID;           /*会话ID*/
	BYTEARRAY<8>   MemberID;            /*会员号*/
	BYTEARRAY<16>  TraderNo;            /*席位编号*/
	BYTEARRAY<20>  SRandomString;       /*服务端产生的随机串*/
	BYTEARRAY<128> SRandomSignature;    /*服务端随机串的签名*/
	INT4           SignatureLength;     /*服务端随机串签名长度*/
	BYTEARRAY<20>  CRandomString;       /*交易员端产生的随机字符串*/
	BYTEARRAY<2048>ClientCA;            /*客户CA证书*/
	INT4           CALength;            /*CA证书长度*/
};

/* 交易员认证应答域 */
struct _fldTraderCertRsp
{
	BYTEARRAY<20>  CRandomString;       /*客户端产生的随机串*/
	BYTEARRAY<128> CRandomSignature;    /*客户端随机串的签名*/
	INT4           SignatureLength;     /*客户端随机串的签名长度*/
	BYTEARRAY<256> SymmetricKey;        /*对称密钥*/
	INT4           SymKeyLength;        /*对称密钥的长度*/
	BYTEARRAY<2048>ServerCA;            /*服务端CA证书*/
	INT4           CALength;            /*CA证书长度*/
};

/* 交易员登录请求域 */
struct _fldTraderLoginReq
{
	BYTEARRAY<8>   MemberID;            /*会员号*/
	BYTEARRAY<16>  TraderNo;            /*席位编号*/
	BYTEARRAY<40>  Pwd;                 /*席位密码*/
	BYTEARRAY<2>   FuncType;            /*席位的功能类型*/
	BYTEARRAY<2>   PosType;             /*席位的位置类型*/
	BYTEARRAY<15>  IP;                  /*席位IP*/
	BYTEARRAY<10>  AppVersion;          /*应用版本号*/
	BYTEARRAY<20>  AppSerial;           /*应用序列号*/
	BYTEARRAY<20>  AppName;             /*应用名*/
};

/* 交易员登录应答域 */
struct _fldTraderLoginRsp
{
	BYTEARRAY<8>   MemberID;            /*会员号*/
	BYTEARRAY<16>  TraderNo;            /*席位编号*/
	BYTEARRAY<8>   Date;                /*交易核心的日期*/
	BYTEARRAY<12>  Time;                /*交易核心的时间*/
	UINT4          LatestOrderNo;       /*最近成功提交的报单编号*/
	BYTE           IsFirstLogin;        /*是否首次登录*/
	BYTE           IsPwdExpirePrompt;   /*密码是否到期提示*/
	INT4           ExpireDays;          /*距离过期天数*/
	BYTE           LastLoginFlag;       /*上次登录是否成功*/
	BYTEARRAY<8>   LastDate;            /*上次登录日期*/
	BYTEARRAY<12>  LastTime;            /*上次登录时间*/
	BYTE           CAStyle;             /*上次CA认证方法*/
	BYTEARRAY<15>  LastIP;              /*上次登录位置*/
	INT4           FailedTimes;         /*上次登录失败次数*/
};

/* 交易员退出请求域 */
struct _fldTraderLogoutReq
{
	BYTEARRAY<16>  TraderNo;            /*席位编号*/
	BYTEARRAY<8>   MemberID;            /*会员号*/
};

/* 交易员退出应答域 */
struct _fldTraderLogoutRsp
{
	BYTEARRAY<16>  TraderNo;            /*席位编号*/
	BYTEARRAY<8>   MemberID;            /*会员号*/
};

/* 席位密码更改请求 */
struct _fldTraderPwdUpdReq
{
	BYTEARRAY<16>  TraderNo;            /*席位编号*/
	BYTEARRAY<8>   MemberID;            /*会员号*/
	BYTEARRAY<40>  OldPwd;              /*旧密码*/
	BYTEARRAY<40>  NewPwd;              /*新密码*/
};

/* 会员资金查询请求 */
struct _fldMemberCapQryReq
{
	BYTEARRAY<8>   MemberID;            /*会员号*/
	BYTEARRAY<16>  TraderNo;            /*席位号*/
};

/* 会员资金域 */
struct _fldMemberCap
{
	BYTEARRAY<8>   MemberID;            /*会员号*/
	INT1           MemberType;          /*会员类型*/
	BYTEARRAY<8>   TradeDate;           /*交易日期*/
	REAL8          InitMargin;          /*初始保证金*/
	REAL8          LastRemain;          /*上日结存*/
	REAL8          BuySpecOpenOccupiedMargin;/*买投开仓占用保证金(单腿)*/
	REAL8          BuyHedgeOpenOccupiedMargin;/*买保开仓占用保证金(单腿)*/
	REAL8          SellSpecOpenOccupiedMargin;/*卖投开仓占用保证金(单腿)*/
	REAL8          SellHedgeOpenOccupiedMargin;/*卖保开仓占用保证金(单腿)*/
	REAL8          BuySpecOffsetProfit; /*买投平仓盈亏(全)*/
	REAL8          BuyHedgeOffsetProfit;/*买保平仓盈亏(全)*/
	REAL8          SellSpecOffsetProfit;/*卖投平仓盈亏(全)*/
	REAL8          SellHedgeOffsetProfit;/*卖保平仓盈亏(全)*/
	REAL8          BuySpecOpenFrozenMargin;/*买投开仓冻结保证金(单腿)*/
	REAL8          BuyHedgeOpenFrozenMargin;/*买保开仓冻结保证金(单腿)*/
	REAL8          SellSpecOpenFrozenMargin;/*卖投开仓冻结保证金(单腿)*/
	REAL8          SellHedgeOpenFrozenMargin;/*卖保开仓冻结保证金(单腿)*/
	REAL8          BuySpecReturnMargin; /*买投平昨仓返回保证金(单腿)*/
	REAL8          BuyHedgeReturnMargin;/*买保平昨仓返回保证金(单腿)*/
	REAL8          SellSpecReturnMargin;/*卖投平昨仓返回保证金(单腿)*/
	REAL8          SellHedgeReturnMargin;/*卖保平昨仓返回保证金(单腿)*/
	REAL8          BuySpecTurnOver;     /*买投成交额(全)*/
	REAL8          BuyHedgeTurnOver;    /*买保成交额(全)*/
	REAL8          SellSpecTurnOver;    /*卖投成交额(全)*/
	REAL8          SellHedgeTurnOver;   /*卖保成交额(全)*/
	REAL8          MoneyIn;             /*本交易日新增资金*/
	REAL8          MoneyOut;            /*本交易日提出资金*/
	REAL8          Avail;               /*可用保证金余额*/
	REAL8          FrozenPremium;       /*冻结权利金*/
	REAL8          BuyPremium;          /*权利金支出*/
	REAL8          SellPremium;         /*权利金收入*/
	BYTEARRAY<14>  ClearAccountNo;      /*结算帐号*/
	REAL8          TraderCreditAmt;     /*席位信用额度*/
	REAL8          BuyAddMargin;        /*组合平仓后买加收保证金(单腿)*/
	REAL8          SellAddMargin;       /*组合平仓后卖加收保证金(单腿)*/
	REAL8          PtflOpenOccupiedMargin;/*开仓占用保证金(组合)*/
	REAL8          PtflOpenFrozenMargin;/*开仓冻结保证金(组合)*/
	REAL8          PtflReturnMargin;    /*平昨仓返回保证金(组合)*/
};

/* 会员持仓查询请求 */
struct _fldMemberPosiQryReq
{
	BYTEARRAY<8>   MemberID;            /*会员号*/
	BYTEARRAY<20>  ContractID;          /*合约号*/
};

/* 客户查询域 */
struct _fldClientQryReq
{
	BYTEARRAY<16>  ClientID;            /*客户号*/
	BYTEARRAY<8>   MemberID;            /*会员号*/
};

/* 客户持仓查询域 */
struct _fldClientPosiQryReq
{
	BYTEARRAY<8>   MemberID;            /*会员号*/
	BYTEARRAY<16>  ClientID;            /*客户号*/
	BYTEARRAY<20>  ContractID;          /*合约号*/
};

/* 客户均价查询请求域 */
struct _fldClientAvgQryReq
{
	BYTEARRAY<16>  ClientID;            /*客户号*/
	BYTEARRAY<8>   MemberID;            /*会员号*/
	BYTEARRAY<80>  ContractID;          /*合约号*/
};

/* 客户均价查询应答域 */
struct _fldClientAvg
{
	BYTEARRAY<16>  ClientID;            /*客户号*/
	BYTEARRAY<80>  ContractID;          /*合约号*/
	REAL8          AvgPrice;            /*客户均价*/
};

/* 定单查询请求域 */
struct _fldOrderQryReq
{
	BYTEARRAY<8>   MemberID;            /*会员号*/
	BYTEARRAY<16>  ClientID;            /*客户号*/
	BYTEARRAY<16>  TraderNo;            /*交易员编码*/
	UINT4          SysOrderNo;          /*系统委托号*/
	BYTEARRAY<80>  ContractID;          /*合约号*/
};

/* 成交查询请求域 */
struct _fldMatchQryReq
{
	BYTEARRAY<8>   MemberID;            /*会员号*/
	BYTEARRAY<16>  ClientID;            /*客户号*/
	BYTEARRAY<16>  TraderNo;            /*席位编号*/
	UINT4          MatchNo;             /*成交号*/
	BYTEARRAY<80>  ContractID;          /*合约号*/
};

/* 行情查询请求域 */
struct _fldQuotQryReq
{
	BYTEARRAY<80>  ContractID;          /*合约号*/
	BYTE           TradeType;           /*交易类型*/
	BYTE           Flag;                /*行情查询类型*/
};

/* 合约交易状态域 */
struct _fldContractStatus
{
	BYTEARRAY<10>  MktID;               /*市场编码(空)*/
	BYTEARRAY<80>  ContractID;          /*合约号*/
	BYTE           Status;              /*合约交易状态*/
};

/* 交易所公告信息域 */
struct _fldBulletinBoard
{
	BYTEARRAY<2>   Type;                /*消息类型*/
	INT1           UrgencyDegree;       /*紧急程度*/
	BYTEARRAY<12>  PromTime;            /*发布时间*/
	BYTEARRAY<80>  Title;               /*标题*/
	BYTEARRAY<20>  ComeFrom;            /*消息来源*/
	BYTEARRAY<10>  MktID;               /*市场编码(空)*/
	BYTEARRAY<4000>Content;             /*内容*/
	BYTEARRAY<200> URLLink;             /*此消息的WEB联结*/
	UINT4          No;                  /*序号*/
	BYTEARRAY<8>   PromDate;            /*发布日期*/
};

/* 行情订阅权限查询请求域 */
struct _fldQuotSubsRightQryReq
{
	BYTE           TradeType;           /*交易类型*/
	BYTEARRAY<4>   VarietyID;           /*品种代码*/
	BYTEARRAY<80>  ContractID;          /*合约号*/
};

/* 行情订阅权限域 */
struct _fldQuotSubsRight
{
	BYTE           TradeType;           /*交易类型*/
	BYTEARRAY<4>   VarietyID;           /*品种代码*/
	BYTEARRAY<80>  ContractID;          /*合约号*/
	INT1           IfBest;              /*是否最优行情*/
	UINT4          MBLLevel;            /*MBL行情级别*/
	INT1           IfOptPara;           /*是否期权参数*/
	INT1           IfClearPrice;        /*是否结算价*/
	UINT4          SendInterval;        /*发送间隔*/
	UINT4          SendTimeout;         /*发送时延*/
};

/* 交易管理员强制席位退出域 */
struct _fldAdminForceTraderLogout
{
	BYTEARRAY<16>  TraderNo;            /*席位编号*/
	BYTEARRAY<8>   MemberID;            /*会员号*/
	BYTEARRAY<100> ForceExitReason;     /*强制退出原因*/
};

/* 套利策略规则查询请求域 */
struct _fldArbiRuleReq
{
	BYTEARRAY<10>  ArbiCode;            /*策略代码*/
};

/* 套利策略规则域 */
struct _fldArbiRule
{
	BYTEARRAY<10>  ArbiCode;            /*策略代码*/
	UINT4          LegNo;               /*腿号*/
	BYTEARRAY<2>   TradeType;           /*各腿交易类型*/
	INT4           VarietyRel;          /*品种关系*/
	BYTEARRAY<10>  MonthRel;            /*月份关系*/
	BYTE           BsFlag;              /*买卖标志*/
	BYTE           CpFlag;              /*看涨看跌标志*/
	BYTEARRAY<10>  StrikePriceRel;      /*执行价格关系*/
	UINT4          Ratio;               /*数量比例*/
	INT4           CalcWay;             /*计算方式*/
};

/* 交易控制员域 */
struct _fldTradeCtrl
{
	BYTEARRAY<20>  TradeCtrlName;       /*交易控制员姓名*/
	BYTEARRAY<8>   LoginDate;           /*登录日期*/
	BYTEARRAY<12>  LoginTime;           /*登录时间*/
	BYTEARRAY<10>  TradeCtrlID;         /*交易控制员ID*/
	BYTEARRAY<40>  Pwd;                 /*交易员登录密码*/
	BYTEARRAY<40>  CAPwd;               /*CA密码 */
	BYTEARRAY<15>  Ip;                  /*登录终端IP*/
	BYTEARRAY<16>  TraderNo;            /*席位编号*/
};

/* 交易控制员请求域 */
struct _fldTradeCtrlReq
{
	BYTEARRAY<10>  TradeCtrlID;         /*交易控制员ID*/
	BYTEARRAY<12>  QryTime;             /*请求时间*/
};

/* 交易系统品种状态域(删除) */
struct _fldVarietyStatus
{
	BYTE           MktStatus;           /*所有品种状态*/
	BYTE           IsMatch;             /*开竞状态*/
	BYTEARRAY<12>  OpTime;              /*操作时间*/
	BYTE           OpStyle;             /*操作类型*/
	BYTEARRAY<6>   ExchCode;            /*交易所代码*/
	BYTEARRAY<20>  TradeCtrlName;       /*交易控制员姓名*/
	BYTEARRAY<10>  TradeCtrlID;         /*交易控制员ID*/
	BYTEARRAY<12>  QryTime;             /*请求时间*/
};

/* 强平操作状态域 */
struct _fldForceOrderOp
{
	INT1           IsShiftFO;           /*当日是否手动强平过*/
	INT2           CounterFO;           /*手动强平的次数*/
	BYTEARRAY<10>  TradeCtrlID;         /*交易控制员ID*/
	BYTEARRAY<12>  QryTime;             /*请求时间*/
};

/* 席位状态域 */
struct _fldTraderStatus
{
	BYTEARRAY<8>   MemberID;            /*会员号*/
	BYTEARRAY<80>  MemberName;          /*会员名称*/
	BYTEARRAY<16>  TraderNo;            /*席位编号*/
	BYTEARRAY<2>   FuncType;            /*席位的功能类型*/
	BYTEARRAY<2>   PosType;             /*席位的位置类型*/
	BYTE           Status;              /*席位状态*/
	BYTEARRAY<10>  TradeCtrlID;         /*交易控制员ID*/
	BYTEARRAY<12>  QryTime;             /*请求时间*/
	BYTEARRAY<15>  IP;                  /*席位IP*/
	UINT4          GroupId;             /*组号*/
	UINT4          DeviceId;            /*设备号*/
	UINT4          ChannelId;           /*通道号*/
};

/* 通知提示信息域 */
struct _fldPromptBoard
{
	UINT4          NoticeNo;            /*消息序号*/
	INT4           NoticeType;          /*消息类别*/
	BYTEARRAY<80>  NoticeTitle;         /*标题*/
	BYTEARRAY<12>  NoticeTime;          /*发布时间*/
	BYTEARRAY<300> Content;             /*内容*/
	BYTEARRAY<16>  TraderNo;            /*发布对象*/
	BYTEARRAY<10>  TradeCtrlID;         /*交易控制员ID*/
	BYTEARRAY<12>  QryTime;             /*请求时间*/
};

/* 强平单查询请求域 */
struct _fldForceOffsetOrderQryReq
{
	BYTEARRAY<10>  TradeCtrlID;         /*交易控制员ID*/
	UINT4          SysOrderNo;          /*系统委托号*/
	BYTEARRAY<8>   MemberID;            /*会员号*/
	BYTEARRAY<16>  ClientID;            /*客户号*/
	BYTEARRAY<80>  ContractID;          /*合约号*/
	BYTE           BsFlag;              /*买卖标志*/
	BYTE           ShFlag;              /*投保标志*/
	BYTE           OrderSrc;            /*定单类别：自动、手动、手动输入*/
};

/* 手动录入强平单请求域 */
struct _fldManualForceOffsetReq
{
	BYTEARRAY<80>  ContractID;          /*合约号*/
	BYTEARRAY<8>   MemberID;            /*会员号*/
	BYTEARRAY<16>  ClientID;            /*客户号*/
	BYTE           BsFlag;              /*买卖标志*/
	UINT4          MatchQty;            /*数量*/
	BYTE           ShFlag;              /*投保标志*/
	BYTEARRAY<10>  TradeCtrlID;         /*交易控制员ID*/
	BYTEARRAY<12>  QryTime;             /*请求时间*/
};

/* 强平单成交查询请求域 */
struct _fldForceOffsetMatchQryReq
{
	UINT4          MatchNo;             /*成交号*/
	BYTEARRAY<16>  ClientID;            /*客户号*/
	BYTEARRAY<8>   MemberID;            /*会员号*/
	BYTEARRAY<4>   VarietyID;           /*品种编号*/
	BYTEARRAY<80>  ContractID;          /*合约号*/
	BYTE           EoFlag;              /*开平标志*/
	BYTE           BsFlag;              /*买卖标志*/
	BYTE           ShFlag;              /*投保标志*/
	BYTE           OrderSrc;            /*定单类别：自动、手动、手动输入*/
	BYTEARRAY<10>  TradeCtrlID;         /*交易控制员ID*/
	BYTEARRAY<12>  QryTime;             /*请求时间*/
};

/* 报单顺序表域 */
struct _fldNewOrderSeq
{
	UINT4          OrderNo;             /*报单号*/
	UINT4          SeqNo;               /*顺序号*/
	BYTEARRAY<10>  TradeCtrlID;         /*交易控制员ID*/
	BYTEARRAY<12>  QryTime;             /*请求时间*/
};

/* 场上技术控制员域 */
struct _fldTechCtrl
{
	BYTEARRAY<20>  TechCtrlName;        /*技术控制员姓名*/
	BYTEARRAY<8>   LoginDate;           /*登录日期*/
	BYTEARRAY<12>  LoginTime;           /*登录时间*/
	BYTEARRAY<10>  TechCtrlID;          /*技术控制员ID*/
	BYTEARRAY<40>  Pwd;                 /*技术控制员登录密码*/
	BYTEARRAY<10>  TechCtrlIDAffirm;    /*技术控制员ID*/
	BYTEARRAY<40>  PwdAffirm;           /*技术控制员登录密码*/
	BYTEARRAY<15>  Ip;                  /*登录终端IP*/
	BYTEARRAY<16>  TraderNo;            /*席位编号*/
};

/* 技术员请求域 */
struct _fldTechCtrlReq
{
	BYTEARRAY<10>  TechCtrlID;          /*场上技术控制员ID*/
	BYTEARRAY<12>  QryTime;             /*请求时间*/
	BYTEARRAY<16>  TraderNo;            /*席位编号*/
};

/* 场上入金状态域 */
struct _fldMoney
{
	BYTEARRAY<12>  ClearMemerID;        /*结算会员代码*/
	BYTEARRAY<14>  ClearAccountNo;      /*结算帐号*/
	BYTEARRAY<80>  ClearMemberName;     /*结算会员名称*/
	REAL8          MoneyAmt;            /*入金数量*/
	BYTEARRAY<20>  OperatorName;        /*交易控制员名*/
	BYTEARRAY<12>  Tradetime;           /*交易时间*/
	BYTEARRAY<8>   TradeDate;           /*交易日期*/
	INT1           IsOpSuccess;         /*操作结果*/
	BYTEARRAY<12>  OpTime;              /*入金操作时间*/
	REAL8          OpFee;               /*入金手续费*/
	REAL8          MoneyAmtAffirm;      /*入金数量*/
	BYTEARRAY<40>  TechCtrlID;          /*场上技术控制员ID*/
	BYTEARRAY<12>  QryTime;             /*请求时间*/
	BYTEARRAY<40>  ErrorReason;         /*错误描述信息*/
};

/* 踢除席位信息域 */
struct _fldKickTrader
{
	BYTEARRAY<8>   MemberID;            /*会员号*/
	BYTEARRAY<80>  MemberName;          /*会员名称*/
	BYTEARRAY<16>  TraderNo;            /*席位号*/
	BYTE           ConnectStatus;       /*连接状态*/
	BYTEARRAY<2>   PosType;             /*席位位置类型*/
	BYTEARRAY<2>   FuncType;            /*席位功能类型*/
	INT1           IsDirectConnect;     /*是否直接接入席位*/
	BYTEARRAY<8>   ApplyDate;           /*席位申请日期*/
	BYTEARRAY<15>  Ip;                  /*踢除席位的IP地址*/
};

/* 席位通知域 */
struct _fldPromptTrader
{
	BYTEARRAY<300> Content;             /*内容*/
	UINT4          NoticeNo;            /*消息序号*/
	BYTEARRAY<10>  TechCtrlID;          /*场上技术控制员ID*/
	BYTEARRAY<12>  NoticeTime;          /*发布时间*/
	BYTEARRAY<80>  Title;               /*标题*/
	BYTEARRAY<16>  TraderNo;            /*席位号 */
	BYTEARRAY<12>  QryTime;             /*请求时间*/
};

/* 会员域 */
struct _fldMember
{
	BYTEARRAY<8>   MemberID;            /*会员编码*/
	BYTEARRAY<80>  MemberName;          /*会员名称*/
	INT1           MemberType;          /*会员类型*/
	INT1           TradeRight;          /*会员权限*/
};

/* 市场交易状态域 */
struct _fldMktStatus
{
	BYTEARRAY<2>   MktStatus;           /*市场状态*/
	BYTEARRAY<6>   ExchCode;            /*交易所代码*/
	BYTEARRAY<12>  MktChgTime;          /*状态触发时间*/
};

/* 定单操作域 */
struct _fldOrderAction
{
	BYTEARRAY<8>   MemberID;            /*会员号*/
	INT1           OrderActionCode;     /*定单操作类型码*/
	UINT4          SysOrderNo;          /*系统委托号*/
	UINT4          OrderBatchNo;        /*委托批次号*/
	UINT4          LocalOrderNo;        /*本地定单号*/
	BYTEARRAY<80>  ContractID;          /*合约号*/
	INT1           ContractVersion;     /*合约版本号（空）*/
	BYTEARRAY<16>  TraderNo;            /*委托席位号*/
	BYTEARRAY<16>  CancelTraderNo;      /*撤单席位号*/
	BYTE           OrderType;           /*定单类型*/
	BYTE           OrderSrc;            /*定单来源*/
	BYTEARRAY<12>  ActionTime;          /*操作时间*/
	UINT4          OrderCancelQty;      /*撤单数量*/
};

/* 市场查询请求域 */
struct _fldMktQryReq
{
	BYTEARRAY<10>  MktID;               /*市场编码(空)*/
};

/* 市场状态查询请求域 */
struct _fldMktStatusQryReq
{
	BYTEARRAY<10>  MktID;               /*市场编码(空)*/
};

/* 会员查询请求域 */
struct _fldMemberQryReq
{
	BYTEARRAY<8>   MemberID;            /*会员编码*/
};

/* 交易员查询域 */
struct _fldTraderQryReq
{
	BYTEARRAY<8>   MemberID;            /*会员编码*/
	BYTEARRAY<16>  TraderNo;            /*交易员编码*/
};

/* 交易员在线查询域 */
struct _fldLoginedTraderQryReq
{
	BYTEARRAY<8>   MemberID;            /*会员编码*/
	BYTEARRAY<16>  TraderNo;            /*交易员编码*/
};

/* 市场域 */
struct _fldMkt
{
	BYTEARRAY<10>  MktID;               /*市场编码(空)*/
	BYTEARRAY<80>  MktName;             /*市场名称(空)*/
	INT2           BroadcastSeqSeries;  /*广播模式序列类别号*/
	BYTE           TradeType;           /*市场交易类型*/
};

/* 席位信用额度查询请求域 */
struct _fldTraderCreditQryReq
{
	BYTEARRAY<16>  TraderNo;            /*席位号*/
	BYTEARRAY<8>   MemberID;            /*会员号*/
};

/* 席位信用额度 */
struct _fldTraderCredit
{
	BYTEARRAY<16>  TraderNo;            /*席位号*/
	BYTEARRAY<8>   MemberID;            /*会员号*/
	REAL8          CreditAmt;           /*信用额度*/
};

/* 场上资金域 */
struct _fldAccount
{
	BYTEARRAY<14>  ClearAccountNo;      /*结算帐号*/
	BYTEARRAY<20>  BillNo;              /*票号*/
	REAL8          Margin;              /*交易保证金变化资金*/
	REAL8          OffsetProfit;        /*平仓盈亏*/
	REAL8          Premium;             /*权利金收支*/
	REAL8          UsedCap;             /*定单占用资金*/
	REAL8          AvailCap;            /*可用资金*/
	REAL8          Currency;            /*实时货币资金*/
};

/* CA会话请求域 */
struct _fldCASessionReq
{
	BYTEARRAY<40>  UserID;              /*用户名*/
	BYTEARRAY<2>   LoginType;           /*登录类型*/
	BYTEARRAY<40>  ProgramID;           /*程序代号*/
	BYTEARRAY<20>  ProgramVer;          /*程序版本号*/
};

/* CA会话应答域 */
struct _fldCASessionRsp
{
	BYTEARRAY<40>  SessionID;           /*会话标识*/
	BYTEARRAY<20>  SRandomString;       /*服务端产生的随机串*/
	BYTE           ServerStatus;        /*CA服务器状态*/
	BYTEARRAY<256> SymmetricKey;        /*对称密钥*/
	INT4           SymKeyLength;        /*对称密钥的长度*/
};

/* CA认证请求域 */
struct _fldCACertReq
{
	BYTEARRAY<40>  SessionID;           /*会话标识*/
	BYTEARRAY<40>  UserID;              /*用户名*/
	BYTEARRAY<2>   LoginType;           /*登录类型*/
	BYTEARRAY<2048>ClientCA;            /*客户CA证书*/
	INT4           CALength;            /*CA证书长度*/
	BYTEARRAY<20>  SRandomString;       /*服务端产生的随机串*/
	BYTEARRAY<128> SRandomSignature;    /*对随机串的签名*/
	INT4           SignatureLength;     /*签名长度*/
	BYTEARRAY<20>  CRandomString;       /*客户端产生的随机串*/
};

/* CA认证应答域 */
struct _fldCACertRsp
{
	BYTEARRAY<20>  CRandomString;       /*客户端产生的随机串*/
	BYTEARRAY<128> CRandomSignature;    /*对随机串的签名*/
	INT4           SignatureLength;     /*签名长度*/
	BYTEARRAY<256> SymmetricKey;        /*对称密钥*/
	INT4           SymKeyLength;        /*对称密钥的长度*/
	BYTEARRAY<256> SpecSymmetricKey;    /*特别对称密钥*/
	INT4           SpecSymKeyLength;    /*特别对称密钥的长度*/
	BYTEARRAY<2048>ServerCA;            /*服务端CA证书*/
	INT4           CALength;            /*CA证书长度*/
};

/* 授权码认证请求域 */
struct _fldAccreditReq
{
	BYTEARRAY<40>  UserID;              /*用户名*/
	BYTEARRAY<2>   LoginType;           /*登录类型*/
	BYTEARRAY<40>  ProgramID;           /*程序代号*/
	BYTEARRAY<20>  ProgramVer;          /*程序版本号*/
	BYTEARRAY<40>  AccreditCode;        /*授权码*/
};

/* 授权码认证应答域 */
struct _fldAccreditRsp
{
	BYTEARRAY<40>  SessionID;           /*会话标识*/
	BYTEARRAY<256> SymmetricKey;        /*对称密钥*/
	INT4           SymKeyLength;        /*对称密钥的长度*/
};

/* 交易参数请求域 */
struct _fldTradeParamQryReq
{
	BYTEARRAY<8>   MemberID;            /*会员号*/
	BYTEARRAY<16>  TraderNo;            /*席位号*/
};

/* 交易参数信息域 */
struct _fldTradeParam
{
	BYTEARRAY<8>   TradeDate;           /*交易日期*/
	BYTEARRAY<20>  ContractID;          /*合约号*/
	REAL8          LastClearPrice;      /*昨日结算价*/
	REAL8          SpecNewBuyRate;      /*投机新开仓买率*/
	REAL8          SpecNewSellRate;     /*投机新开仓卖率*/
	REAL8          HedgeNewBuyRate;     /*套保新开仓买率*/
	REAL8          HedgeNewSellRate;    /*套保新开仓卖率*/
	REAL8          SpecBuyRate;         /*投机买率*/
	REAL8          SpecSellRate;        /*投机卖率*/
	REAL8          HedgeBuyRate;        /*套保买率*/
	REAL8          HedgeSellRate;       /*套保卖率*/
	REAL8          SpecNewBuy;          /*投机新开仓买*/
	REAL8          SpecNewSell;         /*投机新开仓卖*/
	REAL8          HedgeNewBuy;         /*套保新开仓买*/
	REAL8          HedgeNewSell;        /*套保新开仓卖*/
	REAL8          SpecBuy;             /*投机买*/
	REAL8          SpecSell;            /*投机卖*/
	REAL8          HedgeBuy;            /*套保买*/
	REAL8          HedgeSell;           /*套保卖*/
	REAL8          OpenFee;             /*开仓手续费*/
	REAL8          OffsetFee;           /*平仓手续费*/
	REAL8          ShortOpenFee;        /*短线开仓手续费*/
	REAL8          ShortOffsetFee;      /*短线平仓手续费*/
	REAL8          ExecFee;             /*执行手续费*/
	REAL8          PerformFee;          /*履约手续费*/
	REAL8          DeliveryFee;         /*交割手续费率*/
	REAL8          RiseLimit;           /*涨停板*/
	REAL8          FallLimit;           /*跌停板*/
	REAL8          RiseLimitRate;       /*涨停板率*/
	REAL8          FallLimitRate;       /*跌停板率*/
	REAL8          RiseRange;           /*涨停板(幅度)*/
	REAL8          FallRange;           /*跌停板(幅度)*/
	REAL8          RiseDiff;            /*涨停板与前收盘价差值*/
	REAL8          FallDiff;            /*跌停板与前收盘价差值*/
	REAL8          AllSelfSellPosiQuota;/*综合会员自营卖持仓限额*/
	REAL8          AllAgentSellPosiQuota;/*综合会员经纪卖持仓限额*/
	REAL8          AllTotSellPosiQuota; /*综合会员卖限仓总额*/
	REAL8          AllSelfBuyPosiQuota; /*综合会员自营买持仓限额*/
	REAL8          AllAgentBuyPosiQuota;/*综合会员经纪买持仓限额*/
	REAL8          AllTotBuyPosiQuota;  /*综合会员买限仓总额*/
	REAL8          AgentTotSellPosiQuota;/*经纪会员卖持仓限额*/
	REAL8          AgentTotBuyPosiQuota;/*经纪会员买持仓限额*/
	REAL8          SelfTotSellPosiQuota;/*自营会员卖持仓限额*/
	REAL8          SelfTotBuyPosiQuota; /*自营会员买持仓限额*/
	REAL8          ClientSellPosiQuota; /*客户卖持仓限额*/
	REAL8          ClientBuyPosiQuota;  /*客户买持仓限额*/
	REAL8          OutMoney;            /*虚值额*/
	REAL8          InMoney;             /*实值额*/
	INT4           BeforeDeliveryPos;   /*交割月的前几个月*/
	INT4           MonthDayNo;          /*当日在本月的交易日数*/
	REAL8          Tick;                /*最小变动价位*/
	UINT4          MaxHand;             /*最大下单手数*/
	UINT4          Unit;                /*交易单位数量*/
	BYTEARRAY<2>   Status;              /*交易状态*/
};

/* 套保额度域 */
struct _fldHedgeQuote
{
	BYTEARRAY<8>   MemberID;            /*会员号*/
	BYTEARRAY<16>  ClientID;            /*客户号*/
	BYTEARRAY<20>  ContractID;          /*合约代码*/
	BYTE           BsFlag;              /*买卖标志*/
	INT4           ConfirmQuota;        /*有效审批额度/批准额度*/
	INT4           Remain;              /*剩余额度/操作类型*/
	INT4           Used;                /*已用额度*/
};

/* 上场信息域 */
struct _fldBizInfo
{
	INT4           SerialNo;            /*上场序号*/
	BYTE           EndFlag;             /*结束标志*/
};

/* 品种合约权限域 */
struct _fldContractRight
{
	BYTEARRAY<16>  ID;                  /*席位号/客户号 */
	BYTEARRAY<8>   MemberID;            /*会员号*/
	BYTE           TradeType;           /*交易类型*/
	BYTEARRAY<4>   VarietyId;           /*品种代码*/
	BYTEARRAY<20>  ContractId;          /*合约号*/
	INT1           TradeRight;          /*交易权限*/
	INT1           OptType;             /*操作类型*/
};

/* 报单顺序表域 */
struct _fldOrderSeq
{
	UINT4          OrderQty;            /*强平单总数*/
	UINT4          CurQty;              /*当前发送数量*/
	UINT4          OldSeqNo;            /*原来顺序号*/
	UINT4          NewSeqNo;            /*新顺序号*/
};

/* 操作员域 */
struct _fldUser
{
	BYTEARRAY<2>   FuncType;            /*席位的功能类型*/
	BYTEARRAY<8>   DepartmentID;        /*部门号*/
	BYTEARRAY<16>  TraderNo;            /*席位号*/
	BYTE           Type;                /*用户类型*/
};

/* 手动强平操作状态域 */
struct _fldOprtStatus
{
	BYTE           Status;              /*操作状态*/
	BYTEARRAY<40>  RspMsg;              /*通知信息*/
};

/* 市场控制模式状态域 */
struct _fldCtrlModeStatus
{
	BYTE           OpStyle;             /*操作类型*/
	BYTEARRAY<6>   ExchCode;            /*交易所代码*/
};

/* 通知查询请求域 */
struct _fldQryPromptReq
{
	BYTEARRAY<300> QryKey;              /*查询关键字*/
};

/* 入金查询请求域 */
struct _fldQryInMoneyReq
{
	BYTEARRAY<12>  ClearMemberID;       /*会员号*/
	BYTEARRAY<14>  ClearAccountNo;      /*结算帐号*/
};

/* 是否禁用CA查询域 */
struct _fldDisableCaStatus
{
	BYTE           IsDisableCaStatus;   /*CA是否已经被禁用*/
};

/* 业务响应域 */
struct _fldBizAnswer
{
	UINT4          NoticeNo;            /*消息序号*/
	INT4           NoticeType;          /*消息类型*/
	INT4           AnsCode;             /*应答代码*/
	BYTEARRAY<40>  AnsMsg;              /*应答信息*/
};

/* 定单修改域 */
struct _fldModifyOrder
{
	BYTEARRAY<10>  TradeCtrlID;         /*交易控制员ID*/
	UINT4          NewQty;              /*修改后数量*/
	BYTE           ModifyReason;        /*修改原因*/
	BYTEARRAY<12>  ModifyTime;          /*修改时间*/
	BYTEARRAY<8>   MemberID;            /*会员号*/
	BYTEARRAY<16>  ClientID;            /*客户号*/
	BYTEARRAY<80>  ContractID;          /*合约号*/
	BYTE           BsFlag;              /*买卖标志*/
	BYTE           ShFlag;              /*投保标志*/
	UINT4          OrderQty;            /*委托量*/
	UINT4          MatchQty;            /*成交量*/
	UINT4          ModQty;              /*修改减少量*/
	UINT4          OrderBatchNo;        /*委托批次号*/
	UINT4          LocalOrderNo;        /*本地委托号*/
	BYTE           TradeType;           /*交易类型*/
};

/* 行情发送标志域 */
struct _fldSendQuotFlag
{
	UINT4          MachineID;           /*撮合机器号*/
	BYTEARRAY<4>   VarityID;            /*品种代码*/
	BYTE           SendFlag;            /*发送标志*/
	BYTEARRAY<12>  Time;                /*时间*/
};

/* 交易员内部登录请求域 */
struct _fldInTraderLoginReq
{
	BYTEARRAY<8>   MemberID;            /*会员号*/
	BYTEARRAY<16>  TraderNo;            /*席位编号*/
	BYTEARRAY<40>  Pwd;                 /*席位密码*/
	BYTEARRAY<2>   FuncType;            /*席位的功能类型*/
	BYTEARRAY<2>   PosType;             /*席位的位置类型*/
	BYTEARRAY<2>   LoginType;           /*登录类型*/
	BYTEARRAY<15>  IP;                  /*席位IP*/
	BYTEARRAY<10>  AppVersion;          /*应用版本号*/
	BYTEARRAY<20>  AppSerial;           /*应用序列号*/
	BYTEARRAY<20>  AppName;             /*应用名*/
	UINT4          GroupId;             /*组号*/
	UINT4          DeviceId;            /*设备号*/
	UINT4          ChannelId;           /*通道号*/
	BYTE           CAStyle;             /*CA认证方法*/
};

/* 交易员内部登录应答域 */
struct _fldInTraderLoginRsp
{
	BYTEARRAY<8>   MemberID;            /*会员号*/
	BYTEARRAY<16>  TraderNo;            /*席位编号*/
	BYTEARRAY<2>   FuncType;            /*席位的功能类型*/
	BYTEARRAY<2>   PosType;             /*席位的位置类型*/
	BYTEARRAY<2>   LoginType;           /*登录类型*/
	BYTEARRAY<15>  IP;                  /*席位IP*/
	BYTEARRAY<10>  AppVersion;          /*应用版本号*/
	BYTEARRAY<20>  AppSerial;           /*应用序列号*/
	BYTEARRAY<20>  AppName;             /*应用名*/
	UINT4          GroupId;             /*组号*/
	UINT4          DeviceId;            /*设备号*/
	UINT4          ChannelId;           /*通道号*/
	BYTEARRAY<8>   Date;                /*交易核心的日期类型*/
	BYTEARRAY<12>  Time;                /*交易核心的时间*/
	UINT4          LatestOrderNo;       /*最近成功提交的报单编号*/
	BYTE           IsFirstLogin;        /*是否首次登录*/
	BYTE           IsPwdExpirePrompt;   /*密码是否到期提示*/
	INT4           ExpireDays;          /*距离过期天数*/
	BYTE           LastLoginFlag;       /*上次登录是否成功*/
	BYTEARRAY<8>   LastDate;            /*上次登录日期*/
	BYTEARRAY<12>  LastTime;            /*上次登录时间*/
	BYTE           CAStyle;             /*上次CA认证方法*/
	BYTEARRAY<15>  LastIP;              /*上次登录位置*/
	INT4           FailedTimes;         /*上次登录失败次数*/
	BYTEARRAY<2>   RecvRange;           /*接收范围*/
};

/* 交易员内部退出请求域 */
struct _fldInTraderLogoutReq
{
	BYTEARRAY<8>   MemberID;            /*会员号*/
	BYTEARRAY<16>  TraderNo;            /*席位编号*/
	BYTEARRAY<40>  Pwd;                 /*席位密码*/
	BYTEARRAY<2>   FuncType;            /*席位的功能类型*/
	BYTEARRAY<2>   PosType;             /*席位的位置类型*/
	BYTEARRAY<2>   LoginType;           /*登录类型*/
	BYTEARRAY<15>  IP;                  /*席位IP*/
	BYTEARRAY<10>  AppVersion;          /*应用版本号*/
	BYTEARRAY<20>  AppSerial;           /*应用序列号*/
	BYTEARRAY<20>  AppName;             /*应用名*/
	UINT4          GroupId;             /*组号*/
	UINT4          DeviceId;            /*设备号*/
	UINT4          ChannelId;           /*通道号*/
};

/* 交易员内部退出应答域 */
struct _fldInTraderLogoutRsp
{
	BYTEARRAY<8>   MemberID;            /*会员号*/
	BYTEARRAY<16>  TraderNo;            /*席位编号*/
	BYTEARRAY<2>   FuncType;            /*席位的功能类型*/
	BYTEARRAY<2>   PosType;             /*席位的位置类型*/
	BYTEARRAY<2>   LoginType;           /*登录类型*/
	BYTEARRAY<15>  IP;                  /*席位IP*/
	BYTEARRAY<10>  AppVersion;          /*应用版本号*/
	BYTEARRAY<20>  AppSerial;           /*应用序列号*/
	BYTEARRAY<20>  AppName;             /*应用名*/
	UINT4          GroupId;             /*组号*/
	UINT4          DeviceId;            /*设备号*/
	UINT4          ChannelId;           /*通道号*/
};

/* 超级登录请求域 */
struct _fldTechCtrlSuperLoginReq
{
	BYTEARRAY<8>   MemberID;            /*会员号*/
	BYTEARRAY<16>  TraderNo;            /*席位编号*/
	BYTEARRAY<40>  Pwd;                 /*席位密码*/
	BYTEARRAY<16>  SecondTraderNo;      /*第二人席位编号*/
	BYTEARRAY<40>  SecondPwd;           /*第二人席位密码*/
	BYTEARRAY<2>   FuncType;            /*席位的功能类型*/
	BYTEARRAY<2>   PosType;             /*席位的位置类型*/
	BYTEARRAY<2>   LoginType;           /*登录类型*/
	BYTEARRAY<15>  IP;                  /*席位IP*/
	BYTEARRAY<10>  AppVersion;          /*应用版本号*/
	BYTEARRAY<20>  AppSerial;           /*应用序列号*/
	BYTEARRAY<20>  AppName;             /*应用名*/
	UINT4          GroupId;             /*组号*/
	UINT4          DeviceId;            /*设备号*/
	UINT4          ChannelId;           /*通道号*/
};

/* 超级登录应答域 */
struct _fldTechCtrlSuperLoginRsp
{
	BYTEARRAY<8>   MemberID;            /*会员号*/
	BYTEARRAY<16>  TraderNo;            /*席位编号*/
	BYTEARRAY<2>   FuncType;            /*席位的功能类型*/
	BYTEARRAY<2>   PosType;             /*席位的位置类型*/
	BYTEARRAY<2>   LoginType;           /*登录类型*/
	BYTEARRAY<15>  IP;                  /*席位IP*/
	BYTEARRAY<10>  AppVersion;          /*应用版本号*/
	BYTEARRAY<20>  AppSerial;           /*应用序列号*/
	BYTEARRAY<20>  AppName;             /*应用名*/
	UINT4          GroupId;             /*组号*/
	UINT4          DeviceId;            /*设备号*/
	UINT4          ChannelId;           /*通道号*/
	BYTEARRAY<8>   Date;                /*交易核心的日期类型*/
	UINT4          LatestOrderNo;       /*最近成功提交的报单编号*/
	BYTE           IsFirstLogin;        /*是否首次登录*/
	BYTE           IsPwdExpire;         /*密码是否过期*/
};

/* 审计信息域 */
struct _fldAudit
{
	BYTEARRAY<10>  Department;          /*部门*/
	BYTEARRAY<40>  Programid;           /*程序ID*/
	BYTEARRAY<20>  Programver;          /*程序版本*/
	BYTEARRAY<40>  Userid;              /*用户ID*/
	BYTEARRAY<15>  IP;                  /*席位IP*/
	BYTEARRAY<10>  Action;              /*操作*/
	BYTEARRAY<1000>Object;              /*操作原始数据*/
	BYTEARRAY<10>  Result;              /*操作结果(错误代码)*/
	BYTEARRAY<300> Resultmsg;           /*错误信息*/
	BYTEARRAY<8>   Begindate;           /*开始日期*/
	BYTEARRAY<12>  Begintime;           /*开始时间*/
	BYTEARRAY<8>   Enddate;             /*结束日期*/
	BYTEARRAY<12>  Endtime;             /*结束时间*/
	BYTEARRAY<300> Note;                /*附加信息*/
};

/* 内部密码更改请求域 */
struct _fldInTraderPwdUpdReq
{
	BYTEARRAY<16>  TraderNo;            /*席位编号*/
	BYTEARRAY<8>   MemberID;            /*会员号*/
	BYTEARRAY<40>  OldPwd;              /*旧密码*/
	BYTEARRAY<40>  NewPwd;              /*新密码*/
	BYTEARRAY<2>   LoginType;           /*登录类型*/
};

/* 席位列表域 */
struct _fldTraderList
{
	BYTEARRAY<8>   MemberID;            /*会员号*/
	BYTEARRAY<16>  TraderNo;            /*席位编号*/
	BYTEARRAY<40>  Password;            /*席位密码*/
	BYTEARRAY<2>   FuncType;            /*席位的功能类型*/
	BYTEARRAY<2>   RecvRange;           /*接收范围*/
};

/* 常量行情查询请求域 */
struct _fldConstQuotReq
{
	BYTEARRAY<20>  ContractID;          /*合约号*/
};

/* 常量行情域 */
struct _fldConstQuot
{
	BYTEARRAY<8>   TradeDate;           /*交易日期*/
	BYTEARRAY<20>  ContractID;          /*合约号*/
	BYTEARRAY<40>  ContractName;        /*合约名称*/
	UINT4          InitOpenInterest;    /*初始持仓量*/
	REAL8          LifeLow;             /*历史最低价*/
	REAL8          LifeHigh;            /*历史最高价*/
	REAL8          RiseLimit;           /*涨停板*/
	REAL8          FallLimit;           /*跌停板*/
	REAL8          LastClearPrice;      /*上日结算价*/
	REAL8          LastClose;           /*上日收盘价*/
};

/* 交易会员结算帐号域 */
struct _fldMemberClearNo
{
	BYTEARRAY<8>   MemberID;            /*会员号*/
	BYTEARRAY<16>  TraderNo;            /*席位编号*/
	BYTEARRAY<14>  ClearAccountNo;      /*结算帐号*/
};

/* 历史行情查询请求域 */
struct _fldQryHisQuotReq
{
	BYTEARRAY<16>  TraderNo;            /*席位编号*/
	BYTEARRAY<12>  StartTime;           /*开始时间*/
	BYTEARRAY<12>  EndTime;             /*结束时间*/
};

/* 撮合状态域 */
struct _fldKernelStatus
{
	UINT4          MachineID;           /*机器号*/
	BYTEARRAY<2>   Status;              /*状态*/
};

/* 数据服务通知域 */
struct _fldMktDataNotice
{
	BYTEARRAY<12>  Time;                /*时间*/
	BYTEARRAY<2>   Type;                /*类型*/
	BYTEARRAY<80>  Content;             /*内容*/
};

/* 冻结操作域 */
struct _fldFreeze
{
	BYTEARRAY<16>  ItemNo;              /*席位号或用户号*/
	BYTE           OpStyle;             /*0:解冻；1:冻结*/
};

/* 强平委托查询请求域 */
struct _fldForceOrderQryReq
{
	BYTEARRAY<8>   MemberID;            /*会员号*/
	BYTEARRAY<16>  ClientID;            /*客户号*/
	BYTEARRAY<16>  TraderNo;            /*交易员编码*/
	UINT4          SysOrderNo;          /*系统委托号*/
	BYTEARRAY<80>  ContractID;          /*合约号*/
	BYTE           BsFlag;              /*买卖标志*/
	BYTE           EoFlag;              /*开平标志*/
	BYTE           ShFlag;              /*投保标志*/
	BYTE           OrderSort;           /*定单类别*/
	BYTE           OrderSrc;            /*定单来源*/
	BYTE           OrderStatus;         /*定单状态*/
};

/* 启用前置发送行情通知域 */
struct _fldEnableFrontSendQuot
{
	BYTE           IsEnableStatus;      /*是否启用*/
};

/* 用户列表域 */
struct _fldUserList
{
	BYTEARRAY<16>  UserID;              /*席位编号*/
	BYTEARRAY<40>  PassWord;            /*席位密码*/
};

/* 流量控制阀值域 */
struct _fldCntrlValue
{
	INT4           iInterval;           /*频率*/
	INT4           iValue;              /*阀值*/
};

/* 数据服务与行情前置心跳域 */
struct _fldHeartBeatMarketFront
{
	INT4           BatchNo;             /*批次号*/
};

//总共115个
#endif