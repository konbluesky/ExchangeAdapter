#ifndef INTERNAL_HEADER_H
#define INTERNAL_HEADER_H

#include <string>
#include <vector>

namespace exchangeadapter_dalian
{
	struct InitParam
	{
		std::string sFlowPath;
		std::string sAddressIp;
		int iPort;
		std::string sParticipantID;
		std::string sUserID;
		std::string sPassword;
		bool bWriteLog;
		static std::pair<bool,InitParam> LoadConfig(
			const std::string& fname);	
	};
/*
	struct UserInfo
	{
		std::string tsrMemberID;
		std::string tsrTradeID;
		std::string tsrPasswd;
	};
	*/
}

#endif