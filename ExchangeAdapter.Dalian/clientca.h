/* ------------------------------------------------------------------
 * filename:    clientca.h
 * version:     1.0.0.0
 * created:     2005-9-22 15:56
 * author:		shigw
 * purpose:		客户端CA操作封装
 * ------------------------------------------------------------------ 
 * Date
 * ------------------------------------------------------------------ 
*/
#ifndef __CLIENTCA_H
#define __CLIENTCA_H
#include <string.h>
#include "SafeEngine.h"
#include "mytypes.h"
#include "_s_types.h"


class CClientCA
{
public:
	CClientCA();
	~CClientCA();
	enum{
		E_CCERTINITED=-1,		// 证书已经初始化
		E_CCERTINITFAILED=-2,	// 证书初始化失败
		E_CFILEOPEN=-3,			// 证书文件不能打开
		E_CFILECORRUPT=-4,		// 证书文件损坏
		E_CREADFILE=-5,			// 读取文件失败
		E_CCERTNOTINITED=-6,	// 证书没有初始化
		E_CMAKESIGN=-7,			// 签名失败
		E_CVERIFYSIGN=-8,		// 验证签名失败
		E_CENCODE=-9,			// 加密失败
		E_CDECODE=-10,			// 解密失败
	};
	enum{
		T_FILECERT	=0,			//文件证书
		T_USBCERT	=1,			//USB证书
	};
public:
	BOOL		LoadCert(
		_IN_	int		const	iCertType,
		_IN_	char	const	*sCertFile,
		_IN_	char	const	*sKeyFile,
		_IN_	char	const	*sRootFile,
		_IN_	char	const	*sPasswd,
		___________________
		_OUT_	int		*piErrCode,
		_OUT_	char	*psErrMsg);
	BOOL MakeSignature(
		_IN_	char	const		*sOrgString,
		___________________
		_OUT_	BYTE				*sSignature,
		_OUT_	int					*piSignatureLen,
		___________________
		_OUT_	int					*piErrCode,
		_OUT_	char				*psErrMsg);
	BOOL CheckSignature(
		_IN_	BYTE	const		*sServerCert,
		_IN_	int					 iServerCert,
		_IN_	BYTE	const		*sSignature,
		_IN_	int					 iSignatureLen,
		_IN_	char	const		*sOrgString,
		___________________
		_OUT_	int			*piErrCode,
		_OUT_	char		*psErrMsg);
	BOOL		 GetClientCert(
		_OUT_	BYTE		*sCertInfo,
		_OUT_	int			*piCertInfoLen,
		___________________
		_OUT_	int				*piErrCode,
		_OUT_	char			*psErrMsg);
	BOOL		 SecurityBox(
		_IN_	char	const	*sOrgString,
		_IN_	BYTE	const	*sServerCert,
		_IN_	int				 iServerCertLen,
		___________________
		_OUT_	BYTE			*sSecurData,
		_OUT_	int				*piSecurLen,
		___________________
		_OUT_	int				*piErrCode,
		_OUT_	char			*psErrMsg);
	BOOL		 DecodeSecurityBox(
		_IN_	BYTE	const	*sSecurData,
		_IN_	int				 iSecurLen,
		___________________
		_OUT_	char			*sOrgString,
		___________________
		_OUT_	int		*piErrCode,
		_OUT_	char	*psErrMsg);
	BOOL		GetCertValidDate(
		_OUT_	char	*sStart,
		_OUT_	char    *sExpires,
		_OUT_	int				*piErrCode,
		_OUT_	char			*psErrMsg);
	void Done();
private:
	/* ------------------------------------------------------------------
	 * m_sCertInfo/m_iCertInfo:	客户端证书
	 * m_sKeyInfo/m_iKeyInfo:	客户端私钥
	 * m_sRootInfo/m_iRootInfo:	客户端证书根
	 * ------------------------------------------------------------------
	*/
	BYTE		*m_sCertInfo;
	BYTE		*m_sKeyInfo;
	BYTE		*m_sRootInfo;
	int			 m_iCertInfo;
	int			 m_iKeyInfo;
	int			 m_iRootInfo;

	/* ------------------------------------------------------------------
	 * 客户端的CA句柄
	 * ------------------------------------------------------------------
	*/
	HSE			 m_hSE;
	void		 f_init(void);
	void		 f_done(void);
	BOOL		 f_read_file(
		_IN_	char	const *sFileName,
		___________________
		_OUT_	BYTE	**pBuf,
		_OUT_	int		*piBuf,
		___________________
		_OUT_	int		*piErrCode,
		_OUT_	char	*psErrMsg);
	BOOL		 f_MakeSignature(
		_IN_	char	const	*sOrgString,
		___________________
		_OUT_	BYTE			*sSignature,
		_OUT_	int				*piSignatureLen,
		___________________
		_OUT_	int		*piErrCode,
		_OUT_	char	*psErrMsg);
	BOOL		 f_CheckSignature(
		_IN_	BYTE	const		*sServerCert,
		_IN_	int					 iServerCert,
		_IN_	BYTE	const		*sSignature,
		_IN_	int					 iSignatureLen,
		_IN_	char	const		*sOrgString,
		___________________
		_OUT_	int				*piErrCode,
		_OUT_	char			*psErrMsg);
	BOOL		 f_GetClientCert(
		_OUT_	BYTE		*sCertInfo,
		_OUT_	int			*piCertInfoLen,
		___________________
		_OUT_	int				*piErrCode,
		_OUT_	char			*psErrMsg);
	BOOL		 f_SecurityBox(
		_IN_	char	const	*sOrgString,
		_IN_	BYTE	const	*sServerCert,
		_IN_	int				iServerCertLen,
		___________________
		_OUT_	BYTE			*sSecurData,
		_OUT_	int				*piSecurLen,
		___________________
		_OUT_	int				*piErrCode,
		_OUT_	char			*psErrMsg);
	BOOL f_DecodeSecurityBox(
		_IN_	BYTE	const	*sSecurData,
		_IN_	int				 iSecurLen,
		___________________
		_OUT_	char			*sOrgString,
		___________________
		_OUT_	int		*piErrCode,
		_OUT_	char	*psErrMsg);
	BOOL		f_GetCertValidDate(
		_OUT_	char	*sStart,
		_OUT_	char    *sExpires,
		_OUT_	int				*piErrCode,
		_OUT_	char			*psErrMsg);
};
#endif

