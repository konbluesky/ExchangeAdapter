#include "ExchangeAdapterConfig.h"
#include "DLMarketDataProvider.h"
#include "DLDataConvertHelper.h"
#include "debugutils.h"
#include "StringUtils.h"
#include <cctype>
#define TIXML_USE_STL
#include "tinyxml.h"
#include "LocalCompile.h"

namespace exchangeadapter_dalian
{
	std::pair<bool,CDLMarketDataProvider::InitParam> 
		CDLMarketDataProvider::InitParam::LoadConfig(const tstring& fname)
	{
		InitParam param;
		TiXmlDocument doc(fname.c_str());
		bool loadOkay = doc.LoadFile();
		TiXmlNode* pNode = NULL;
		std::string str;

		std::memset(&param,0,sizeof(param));
		if (!loadOkay)
			return std::make_pair(false,param);
		
		pNode = doc.FirstChild("configure");
		XML_GET_STRING(pNode,"saddressip",param.sAddressIp);
		XML_GET_INT(pNode,"iport",param.iPort);
		XML_GET_STRING(pNode,"sparticipantid",param.sParticipantID);
		XML_GET_STRING(pNode,"suserid",param.sUserID);
		XML_GET_STRING(pNode,"spassword",param.sPassword);
		XML_GET_BOOL(pNode,"blog",param.bWriteLog);
				
		return std::make_pair(true,param);
	}

	CDLMarketDataProvider::CDLMarketDataProvider(void)
	{
		m_dwSeqNo = 0;
	}

	bool CDLMarketDataProvider::Initialize(void* param)
	{
		m_param = *(InitParam*)param;
		if(!InitPara() || !InitAPI() || !Connect())
			return false;
		return true;
	}

	bool CDLMarketDataProvider::InitializeByXML( const tchar* configfile )
	{
		std::pair<bool,InitParam> param = 
			InitParam::LoadConfig(configfile);
		if (param.first)
			return Initialize(&param.second);
		else
			return false;
	}

	void CDLMarketDataProvider::UnInitialize(){}

	bool CDLMarketDataProvider::InitPara()
	{
		m_pcsQutotAPI = boost::shared_ptr<CQuotAdapter>(
			new CQuotAdapter(this));
		return true;
	}

	bool CDLMarketDataProvider::InitAPI()
	{
		m_pcsQutotAPI->InitAPI(m_param.bWriteLog);
		if(0 != m_pcsQutotAPI->InitCA(0,"","","","",false))
			return false;
		m_pcsQutotAPI->SetService(m_param.sAddressIp.c_str(),m_param.iPort);
		return true;
	}

	bool CDLMarketDataProvider::Connect()
	{
		int errorid = m_pcsQutotAPI->Connect();
		if(0 != errorid){
			DBG_TRACE(("���Ӵ���,����id��:%d",errorid));
			return false;
		}
		return true;
	}

	bool CDLMarketDataProvider::Login(void*)
	{
		_fldTraderSessionReq pTraderSessionReq;
		_fldRspMsg pRspMsg;
		_fldTraderSessionRsp pTraderSessionRsp;
		_fldTraderLoginReq loginreq;
		_fldTraderLoginRsp loginrsp;
		int32 iRet;

		LIB_NS::UString::Copy(pTraderSessionReq.MemberID.buf,
			m_param.sParticipantID);
		LIB_NS::UString::Copy(pTraderSessionReq.TraderNo.buf,m_param.sUserID);
		pTraderSessionReq.IsShortCert = _T('0');

		LIB_NS::UString::Copy(loginreq.TraderNo.buf,m_param.sUserID);
		LIB_NS::UString::Copy(loginreq.Pwd.buf,m_param.sPassword);

		iRet = m_pcsQutotAPI->TraderSession(&pTraderSessionReq,
			const_cast<tchar*>(m_param.sPassword.c_str()),
			&pRspMsg,&pTraderSessionRsp);
		if(iRet){
			DBG_TRACE(("�����:%d %s",iRet,pRspMsg.RspMsg.buf));
			return false;
		}

		if(0 != (iRet = m_pcsQutotAPI->Login(&loginreq,&pRspMsg,&loginrsp))){
			DBG_TRACE(("���۵�½ʧ��!"));
			return false;
		}

		if(0 != (iRet=m_pcsQutotAPI->Ready(READY,READY,true))){
			DBG_TRACE(("����׼��ʧ��!"));
			return false;
		}

		/* �ϱ����� */
		PriOnLogin();

		return true;
	}

	bool CDLMarketDataProvider::Logout(void*)
	{
		_fldTraderLogoutReq pLogout;

		LIB_NS::UString::Copy(pLogout.MemberID.buf,m_param.sParticipantID);
		LIB_NS::UString::Copy(pLogout.TraderNo.buf,m_param.sUserID);

		if(0 != m_pcsQutotAPI->ReqQuotUserLogout((uint32*)(&m_dwSeqNo),pLogout))
			return false;
		PriOnLogout();
		return true;
	}

	void CDLMarketDataProvider::onChannelLost(const tchar *szErrMsg)
	{
		PriOnNetDisconnected();
		DBG_TRACE(("����:%s\n�Զ�����...\n��ֹ�ݹ�\n",szErrMsg));
		int32 i = 0;
		while(3 > i && 0 > OnAutoConn()){
			ZThread::Thread::sleep(3000);
			i++;
		}
		if(3 > i)
			PriOnNetConnected();
	}

	int32 CDLMarketDataProvider::OnAutoConn()
	{
		if(!Connect() || !Login(NULL)){
			DBG_TRACE(("����ʧ�ܴ���"));
			return -1;
		}
		return 0;
	}

	tstring CDLMarketDataProvider::GetName()
	{
		return "DaliangMarketDataProvider";
	}

	bool CDLMarketDataProvider::SupportDoM()
	{
		return false;
	}

	CDLMarketDataProvider::~CDLMarketDataProvider()
	{

	}

	void CDLMarketDataProvider::OnRspMsg(const FIX::Message &msg)
	{
		OnRspMsg(msg);
	}
}