/********************************************************
 * 版权所有 (C)2004－2008, 
 * 
 * 文件名称：Constant.h
 * 文件标识：
 * 内容摘要：
 * 其它说明：
 * 当前版本：
 * 作    者：何荣
 * 完成日期：2005-12-13 16:33:53
 * 
 *********************************************************/

#ifndef CONSTANT_H
#define CONSTANT_H

// 套利策略代码
#define	AC_SP              "SP"           //跨期套利
#define	AC_SPC             "SPC"          //两腿跨品种套利
#define	AC_SPX             "SPX"          //压榨套利
#define	AC_CALL            "CSPR"         //Call Spread
#define	AC_PUT             "PSPR"         //Put Spread
#define	AC_COMBO           "COMBO"        //Combo
#define	AC_STRADDLE        "STD"          //Straddle
#define	AC_STRANGLE        "STG"          //Strangle
#define	AC_GUTS            "GUTS"         //Guts
#define	AC_SYNUND          "SYN"          //Synthetic Underlying

// 交易类型
#define	TT_FTR              '0'           //期货
#define	TT_OPT              '1'           //期权

// 买卖标志
#define	BF_BUY              '1'           //买
#define	BF_SELL             '3'           //卖

// 开平标志
#define	EF_OPEN             '1'           //开仓
#define	EF_OFFSET           '2'           //平仓
#define	EF_RULE             '3'           //强减
#define	EF_FORCE            '4'           //强平

// 投保标志
#define	SF_SPEC             '1'           //投机
#define	SF_HEDGE            '3'           //保值

// 定单类别
#define	OS_BASIC            '0'           //基本定单
#define	OS_ARBI             '1'           //套利定单
#define	OS_COMBO            '2'           //组合定单
#define	OS_STRIP            '3'           //批量定单
#define	OS_EXEC             '4'           //期权执行申请定单
#define	OS_BOTH             '5'           //双边报价定单

// 定单属性
#define	OA_NONE             '0'           //无
#define	OA_FOK              '1'           //全部成交定单
#define	OA_FAK              '2'           //立即成交和撤消定单

// 定单类型
#define	OT_LO               '0'           //限价单
#define	OT_MO               '1'           //市价单
#define	OT_SO_LOSS          '2'           //止损定单
#define	OT_SO_PROFIT        '3'           //止盈定单
#define	OT_SLO_LOSS         '4'           //限价止损定单
#define	OT_SLO_PROFIT       '5'           //限价止盈定单

// 定单来源
#define	OS_NONE             '0'           //无
#define	OS_AUTO             '1'           //自动生成
#define	OS_GEN              '2'           //手工生成
#define	OS_INPUT            '3'           //手工录入
#define	OS_AUTO_UPD         '4'           //自动生成修改
#define	OS_GEN_UPD          '5'           //手工生成修改
#define	OS_INPUT_UPD        '6'           //手工录入修改
#define	OS_SYSCANCEL        '7'           //强平系统撤单

// 特殊定单类型
#define	EX_NONE             '0'           //非特殊
#define	EX_FORCE            '1'           //强平定单
#define	EX_SPECIFY          '2'           //套利指定定单

// 委托状态
#define	OS_ORDER            'o'           //已报入
#define	OS_TRIG             'r'           //已触发
#define	OS_COMPLETE         'c'           //完全成交
#define	OS_PART             'p'           //部分成交
#define	OS_CANCEL           'd'           //客户撤单
#define	OS_FOKCANCEL        'k'           //FOK撤单
#define	OS_FAKCANCEL        'a'           //FAK撤单
#define	OS_STRIPCANCEL      's'           //批量定单撤单
#define	OS_FORCECANCEL      'f'           //强平撤单
#define	OS_FORCEUPD         'u'           //强平修改

// 交易会员类型
#define	MT_SELF             '0'           //自营
#define	MT_AGENT            '1'           //经纪
#define	MT_ALL              '2'           //综合

// 注销标志
#define	LF_VALID            '0'           //有效
#define	LF_LOGOUT           '1'           //注销

// 会员退会原因
#define	MLR_FORCE           '0'           //强制退会
#define	MLR_ATTORN          '1'           //会员资格转让

// 会员联系人员类别
#define	CS_MANAGE           '0'           //高管
#define	CS_TRADE            '1'           //出市代表

// 交易编码状态
#define	TS_OPEN             '0'           //开户
#define	TS_LOGOUT           '1'           //销户

// 交易编码销户原因
#define	TLR_ACTIVE          '0'           //会员主动销户
#define	TLR_FORCE           '1'           //交易所强制销户
#define	TLR_SELF            '2'           //退会注销

// 客户开户申请状态
#define	AS_APPLY            '0'           //申请
#define	AS_OK               '1'           //开户成功
#define	AS_REFUSE           '2'           //交易所拒绝开户
#define	AS_CANCEL           '3'           //客户撤销申请

// 席位功能类型
#define	FT_TRADE            '0'           //交易席位
#define	FT_MATCH            '1'           //成交回报席位
#define	FT_QUOT             '2'           //行情席位
#define	FT_ACCESSPOINT      '3'           //分厅接入点

// 席位位置类型
#define	PT_INNER            '0'           //场内
#define	PT_OUTER            '1'           //远程

// 席位位置子类型
#define	SS_ROOT             "00"	          //场内本席位
#define	SS_ADD              "01"	          //场内增加席位
#define	SS_OUTER_OUTER1     "11"	          //第一远程交易席位
#define	SS_OUTER_OUTER2     "12"	          //第二远程交易席位
#define	SS_OUTER_OUTER3     "13"	          //第三远程交易席位
#define	SS_OUTER_OUTER4     "14"	          //第四远程交易席位
#define	SS_OUTER_OUTER5     "15"	          //第五远程交易席位
#define	SS_OUTER_OUTER6     "16"	          //第六远程交易席位
#define	SS_OUTER_OUTER7     "17"	          //第七远程交易席位
#define	SS_OUTER_OUTER8     "18"	          //第八远程交易席位
#define	SS_OUTER_OUTER9     "19"	          //第九远程交易席位
#define	SS_OUTER_OUTER10    "1A"	          //第十远程交易席位
#define	SS_OUTER_OUTER11    "1B"	          //第十一远程交易席位
#define	SS_OUTER_OUTER12    "1C"	          //第十二远程交易席位
#define	SS_OUTER_OUTER13    "1D"	          //第十三远程交易席位
#define	SS_OUTER_OUTER14    "1E"	          //第十四远程交易席位
#define	SS_OUTER_OUTER15    "1F"	          //第十五远程交易席位
#define	SS_OUTER_OUTER16    "1G"	          //第十六远程交易席位
#define	SS_OUTER_OUTER17    "1H"	          //第十七远程交易席位
#define	SS_OUTER_OUTER18    "1I"	          //第十八远程交易席位
#define	SS_OUTER_OUTER19    "1J"	          //第十九远程交易席位
#define	SS_OUTER_OUTER20    "1K"	          //第二十远程交易席位
#define	SS_OUTER_INNER      "1L"	          //所内远程交易席位
#define	SS_SHENYANG         "1M"	          //沈阳分厅席位
#define	SS_CHANGCHUN        "1N"	          //长春分厅席位

// 交易权限
#define	TR_NONE             '0'           //不可交易
#define	TR_OFFSETONLY       '1'           //只可平仓

// 做市商报价权限
#define	MO_NONE             '0'           //不可报价
#define	MO_NORMAL           '1'           //可以报价

// 做市商违规状态
#define	MB_EXEMPT           '0'           //豁免
#define	MB_BREACH           '1'           //违规

// 做市商日终评价的违规原因
#define	FB_ORDEROVER        "10"	          //单笔下单量超限
#define	FB_LOCKOVER         "11"	          //单笔对敲成交量超限
#define	FB_TIME             "20"	          //持续报价时间不足
#define	FB_TOTPOSI          "21"	          //总持仓量不足
#define	FB_MATCHNOT         "22"	          //日成交量不足
#define	FB_MATCHOVER        "23"	          //日成交量超限
#define	FB_NETPOSI          "24"	          //净持仓量不足
#define	FB_LOCK             "25"	          //对敲成交比例超限
#define	FB_MEMBERMATCH      "26"	          //会员日成交量不足

// 证件类型
#define	CT_IDCARD           '0'           //身份证
#define	CT_OFFICER          '1'           //军官证
#define	CT_PASSPORT         '2'           //护照
#define	CT_RESIDENCE        '3'           //户口薄
#define	CT_OTHER            '4'           //其它

// 合约状态
#define	CS_PREPARE          '0'           //预上市
#define	CS_ON               '1'           //已上市
#define	CS_LOGOUT           '2'           //已摘牌

// 是否
#define	IF_NO               '0'           //否
#define	IF_YES              '1'           //是

// 性别
#define	G_MALE              'm'           //男
#define	G_FEMALE            'f'           //女

// 客户性质
#define	CP_INDIVID          '0'           //个人
#define	CP_UNIT             '1'           //单位

// 系统状态
#define	ES_INITING          "00"	          //初始化数据准备中
#define	ES_INITED           "10"	          //初始化数据准备完成
#define	ES_LOADED           "20"	          //初始化数据加载完成
#define	ES_OPEN             "30"	          //开市
#define	ES_PAUSE            "40"	          //暂停
#define	ES_CLOSE            "50"	          //闭市
#define	ES_CLOSEDEALING     "60"	          //闭市后处理中
#define	ES_CLOSEDEALED      "70"	          //闭市后处理完成
#define	ES_CLEARING         "80"	          //结算中
#define	ES_CLEARED          "90"	          //结算完成

// 品种交易状态
#define	VT_INIT             "11"	          //初始化完成
#define	VT_AUCTION_ORDER    "12"	          //集合竞价申报
#define	VT_AUCTION_PAUSE    "13"	          //集合竞价暂停
#define	VT_AUCTION_MATCH    "14"	          //集合竞价撮合
#define	VT_TRADE            "15"	          //连续交易
#define	VT_TRADE_PAUSE      "16"	          //连续交易暂停
#define	VT_CLOSE            "17"	          //收盘

// 品种状态
#define	VS_ON               '0'           //已上市
#define	VS_LOGOUT           '1'           //已摘牌

// 看涨看跌
#define	CP_CALL             'C'           //看涨
#define	CP_PUT              'P'           //看跌

// 合约增加方式
#define	AT_MANUAL           '0'           //手工
#define	AT_AUTO             '1'           //自动

// 计算方式
#define	CT_PLUS             '+'           //加
#define	CT_MINUS            '-'           //减

// 货币单位
#define	C_RMB               '0'           //人民币
#define	C_DOLLAR            '1'           //美元
#define	C_OTHER             '2'           //其他

// 资金帐号状态（废）
#define	BA_VALID            '0'           //正常
#define	BA_FROZEN           '1'           //冻结
#define	BA_LOGOUT           '2'           //注销

// 帐号类型
#define	AT_SELF             '0'           //自营
#define	AT_AGENT            '1'           //代理

// 资金录入性质
#define	MIO_MANUAL          '0'           //手工
#define	MIO_AUTO            '1'           //电子

// 强平原因
#define	FR_NOTENOUGH        '0'           //资金不足
#define	FR_OVERPOSI         '1'           //客户超仓
#define	FR_MEMBEROVERPOSI   '2'           //会员超仓
#define	FR_BREACH           '3'           //违规

// 上限符号
#define	US_SMALL            '0'           //小于
#define	US_SMALLEQUAL       '1'           //小于等于

// 下限符号
#define	DS_BIG              '0'           //大于
#define	DS_BIGQUAL          '1'           //大于等于

// 比对结果
#define	CR_UNCHECKOUT       '0'           //不合格
#define	CR_RECHECK          '1'           //重检
#define	CR_CHECKOUT         '2'           //合格

// 比对内容
#define	CC_SINGLE           '0'           //单项指标
#define	CC_TOTAL            '1'           //总升贴水

// 仓单管理方式
#define	WM_NO               '0'           //不对库
#define	WM_YES              '1'           //对库

// 日期类型
#define	DT_NATURE           '0'           //自然日
#define	DT_TRADE            '1'           //交易日

// 申请状态
#define	AS_INPUT            '0'           //录入
#define	AS_SUCCESS          '1'           //审批成功
#define	AS_FAIL             '2'           //审批失败

// 预清退状态
#define	PWS_INPUT           '0'           //录入
#define	PWS_DEL             '1'           //已删除
#define	PWS_RELEASE         '2'           //已释放

// 期转现状态
#define	FS_INPUT            '0'           //录入
#define	FS_DEL              '1'           //已删除
#define	FS_FAIL             '2'           //处理失败
#define	FS_SUCCESS          '3'           //处理成功

// 滚动交割状态
#define	RS_INPUT            '0'           //录入
#define	RS_DEL              '1'           //已删除
#define	RS_FAIL             '2'           //处理失败
#define	RS_MATCH_SUC        '3'           //配对日处理成功
#define	RS_MATCH_FAIL       '4'           //配对失败
#define	RS_DELIVERY_SUC     '5'           //交收日处理成功

// 交割方式
#define	DT_ROLL             '0'           //滚动交割
#define	DT_CONGREGATE       '1'           //一次性交割
#define	DT_FTS              '2'           //期转现

// 违约方
#define	BO_BUY              'b'           //买方
#define	BO_SELL             's'           //卖方
#define	BO_ALL              'a'           //双方

// 违约处理标志
#define	DF_END              '0'           //终止交割
#define	DF_CONTINUE         '1'           //继续交割
#define	DF_ASK              '2'           //征购
#define	DF_BID              '3'           //竞卖

// 交割货款清退类型
#define	WT_20               '0'           //20%货款
#define	WT_80               '1'           //80%货款

// 交割预报状态
#define	DI_INPUT            '0'           //录入
#define	DI_DEL              '1'           //已删除
#define	DI_CONFIRM          '2'           //已确认预报定金
#define	DI_RET              '3'           //已返还定金

// 仓库质检报告状态
#define	WRS_INPUT           '0'           //录入
#define	WRS_ORDER           '1'           //委托复检
#define	WRS_NOORDER         '2'           //免委托
#define	WRS_RECHECK         '3'           //重检
#define	WRS_RECHECKED       '4'           //已复核

// 垛位状态
#define	BS_INPUT            '0'           //报检
#define	BS_ORDER            '1'           //委托
#define	BS_NOORDER          '2'           //免委托
#define	BS_CHECKOUT         '3'           //合格
#define	BS_RECHECKNO        '4'           //重检报送过期不合格
#define	BS_REGNO            '5'           //注册过期不合格
#define	BS_CHECKNO          '6'           //复核不合格
#define	BS_RECHECK          '7'           //重检
#define	BS_FTR              '8'           //期货

// 结算会员状态
#define	CMS_VALID           '0'           //有效
#define	CMS_LOGOUT          '1'           //注销

// 资金帐号状态
#define	BS_VALID            '0'           //有效
#define	BS_LOGOUT           '1'           //注销
#define	BS_FROZEN           '2'           //冻结

// 套利类别
#define	AT_FTR              '0'           //期货
#define	AT_OPT              '1'           //期权

// 比较符号
#define	CS_LARGE            '0'           //大于
#define	CS_SMALLEQUAL       '1'           //小于等于

// 科目类型
#define	IT_ASSET            '0'           //资产
#define	IT_DEBT             '1'           //负债

// 质押类型
#define	MT_WBILL            '0'           //仓单
#define	MT_NOTWBILL         '1'           //非仓单

// 报价权限
#define	OR_NOTORDER         '0'           //不可报价
#define	OR_ORDER            '1'           //可以报价

// 包含关系
#define	IR_LARGE            '0'           //大于
#define	IR_LARGEEQUAL       '1'           //大于等于

// 强减配对类型
#define	OT_LOCK             '0'           //对锁配对
#define	OT_SEC              '1'           //二次配对
#define	OT_RULE             '2'           //强减配对

// 系统变化状态
#define	EC_INIT             "11"	          //初始化后
#define	EC_AUCTION_ORDER    "12"	          //集合竞价申报
#define	EC_AUCTION_PAUSE    "13"	          //集合竞价暂停
#define	EC_AUCTION_MATCH    "14"	          //集合竞价撮合
#define	EC_TRADE            "15"	          //连续交易
#define	EC_TRADE_PAUSE      "16"	          //连续交易暂停
#define	EC_FORCE_OFFSET     "17"	          //强平
#define	EC_CLOSE            "50"	          //闭市

// 操作类别
#define	OP_ADD              '0'           //增加
#define	OP_UPDATE           '1'           //修改
#define	OP_DELETE           '2'           //删除
#define	OP_QUERY            '3'           //查询
#define	OP_LOGIN            '4'           //登录
#define	OP_LOGOUT           '5'           //退出
#define	OP_OVERTIME         '6'           //超时
#define	OP_CHECK            '7'           //复核
#define	OP_DESTROY          '8'           //注销
#define	OP_CLEAR            '9'           //日终结算
#define	OP_CHGPWD           'A'           //改密码
#define	OP_TRANSMONEY       'B'           //划款
#define	OP_AUTO             'C'           //自动制票
#define	OP_MONTHEND         'D'           //月末结转
#define	OP_MORTIN           'E'           //质入
#define	OP_MORTOUT          'F'           //质出
#define	OP_REJECTED         'G'           //拒绝客户申请

// 歇业状态
#define	OS_OUTBIZ           '0'           //歇业
#define	OS_RESTORE          '1'           //恢复
#define	OS_LOGOUT           '2'           //退会

// 管理级别
#define	ML_FORBID           '0'           //禁止开户
#define	ML_MONITOR          '1'           //重点监控

// 期权申请单来源
#define	AS_TRADE            '0'           //交易
#define	AS_WEB              '1'           //会员服务

// 测算基准价
#define	TC_AVG              '0'           //均价
#define	TC_LAST             '1'           //最新价
#define	TC_RISE             '2'           //涨板价
#define	TC_FALL             '3'           //跌板价

// 结算或测算执行状态
#define	ES_EXEC             '0'           //正在执行
#define	ES_ERR              '1'           //执行错误
#define	ES_CANCEL           '2'           //执行取消
#define	ES_END              '3'           //执行成功

// 保值额度变化原因
#define	CR_SELF             '0'           //自平
#define	CR_FTS              '1'           //期转现
#define	CR_ROLL             '2'           //滚动交割
#define	CR_FORCE            '3'           //强平
#define	CR_RULE             '4'           //强减
#define	CR_DEL              '5'           //删除
#define	CR_CONFIRM          '6'           //审批
#define	CR_DUE              '7'           //到期

// 复检机构状态
#define	OS_NORMAL           '0'           //正常
#define	OS_DEL              '1'           //已删除

// 仓库类型
#define	WT_NORM             '0'           //基准
#define	WT_DIFF             '1'           //非基准

// 仓库状态
#define	WS_NORMAL           '0'           //正常
#define	WS_DEL              '1'           //已删除

// 发送方式
#define	ST_LOGIN            '0'           //登录
#define	ST_SEND             '1'           //推送
#define	ST_BOTH             '2'           //登录或者推送

// 提示信息类别
#define	HT_DATE             '0'           //业务日期类
#define	HT_STATUS           '1'           //业务状态类
#define	HT_DEAL             '2'           //业务处理类
#define	HT_NOTICE           '3'           //业务通知类

// 紧急程度
#define	UD_HIGH             '0'           //高
#define	UD_MID              '1'           //中
#define	UD_LOW              '2'           //低

// 客户类别
#define	CS_SELF             '0'           //自营
#define	CS_AGENT            '1'           //代理

// 监控指标
#define	MT_OVERPOSI         '0'           //爆仓比例
#define	MT_MONEY            '1'           //资金占有率
#define	MT_LARGE_BUY        '2'           //大户买持仓比例
#define	MT_LARGE_SELL       '3'           //大户卖持仓比例
#define	MT_PRICECHANGE      '4'           //价格变化指标
#define	MT_PRICEDIFF        '5'           //价差
#define	MT_SELFMATCH        '6'           //自成交
#define	MT_POSICHANGE       '7'           //持仓变化
#define	MT_RISEFALL         '8'           //价格涨跌
#define	MT_RISEFALLLIMIT    '9'           //涨跌停板

// 期权申请单状态
#define	OA_APPLY            '0'           //申请
#define	OA_CANCEL           '1'           //撤单
#define	OA_COMPLETE         '2'           //全部执行
#define	OA_POSI             '4'           //持仓不足
#define	OA_CLIENTOVER       '5'           //客户超仓
#define	OA_MEMBEROVER       '6'           //会员超仓
#define	OA_MONEY            '7'           //资金不足

// 执行类型
#define	ET_NOEXEC           '0'           //不执行
#define	ET_EXEC             '1'           //执行

// 仓单流水类型
#define	WJ_REG              '0'           //注册
#define	WJ_LOGOUT           '1'           //注销
#define	WJ_IMPAWN           '2'           //冲抵
#define	WJ_WITHDRAW         '3'           //清退
#define	WJ_PREWITHDRAW      '4'           //预清退
#define	WJ_TRANS            '5'           //过户
#define	WJ_MORTFROZEN       '6'           //质押冻结
#define	WJ_MORTRELEASE      '7'           //质押释放
#define	WJ_OTHERFROZEN      '8'           //其他冻结
#define	WJ_OTHERRELEASE     '9'           //其他释放
#define	WJ_DELIVERYTRANS    'A'           //交割过户
#define	WJ_ROLLSUCCESS      'B'           //意向交割量转预交割量（滚动交割配对成功）
#define	WJ_ROLLFAIL         'C'           //意向交割量转冲抵量（滚动交割配对失败）
#define	WJ_CONSUCCESS       'D'           //冲抵量转预交割量（一次性交割配对成功）
#define	WJ_STOPDELIVERY     'E'           //预交割量转流通量（违约处理卖方终止交割）
#define	WJ_ROLLINPUT        'F'           //冲抵量转意向交割量（滚动交割意向录入）
#define	WJ_ROLLUPDUP        'G'           //冲抵量转意向交割量（滚动交割意向修改）
#define	WJ_ROLLUPDDOWN      'H'           //意向交割量转冲抵量（滚动交割意向删除）
#define	WJ_CTNDELIVERY      'I'           //预交割量转其他冻结（违约处理卖方继续交割）

// 数据来源
#define	WS_DELIVERY         '0'           //交割
#define	WS_WEB              '1'           //会员服务

// 质押操作类型
#define	MT_MORTIN           '0'           //质入
#define	MT_MANUALOUT        '1'           //手工质出
#define	MT_DUEOUT           '2'           //期满质出
#define	MT_MORTUPD          '3'           //质押调整

// 盘后成交类型
#define	AM_SPECTOHEDGE      '0'           //投机转套保
#define	AM_EXECOPEN         '1'           //期权执行的期货建仓
#define	AM_DELIVERY         '2'           //交割持仓对冲
#define	AM_RULEOFFSET       '3'           //强制减仓

// 交易阶段
#define	TA_SPECTOHEDGE      '0'           //集合竞价
#define	TA_EXECOPEN         '1'           //连续交易

// 扩展定单类型
#define	EOT_LO              '0'           //限价单
#define	EOT_MO              '1'           //市价单
#define	EOT_SO              '2'           //止损（盈）定单
#define	EOT_SLO             '3'           //限价止损（盈）定单
#define	EOT_EXEC            '4'           //期权执行申请定单

// 保证金率操作类型
#define	MO_ADD              '0'           //增加
#define	MO_UPDATE           '1'           //修改
#define	MO_DELETE           '2'           //删除

// 保证金类型
#define	MT_RATE             '0'           //比率
#define	MT_VALUE            '1'           //固定值

// 期权套利策略保证金收取方法
#define	MW_RECV             '0'           //收取
#define	MW_DIFF             '1'           //执行价格之差
#define	MW_SELL             '2'           //收取卖方保证金
#define	MW_SELLSINGLE       '3'           //收取卖方单腿保证金最大值

// 中间表记录状态
#define	MS_NOCHECKED        '0'           //未复核
#define	MS_CHECKED          '1'           //已复核

// 日期参数种类
#define	DP_IMPAWN           '0'           //仓单冲抵开始日期
#define	DP_LAST             '1'           //品种最后交割日
#define	DP_REUSE            '2'           //套期保值重复使用期限
#define	DP_VALID            '3'           //仓单有效期

// 复核状态
#define	RS_NOCHECKED        '0'           //未复核
#define	RS_CHECKED          '1'           //已复核

// 仓单申请类型
#define	AT_IMPAWN           '0'           //冲抵申请
#define	AT_WITHDRAW         '1'           //清退申请
#define	AT_PREWITHDRAW      '2'           //预清退申请
#define	AT_TRANS            '3'           //过户申请

// 事件类型
#define	ET_STARTTRADEDATE   '0'           //合约开始交易日
#define	ET_MARGINRATE       '1'           //临近交割月合约交易保证金参数变化
#define	ET_POSIQUOTA        '2'           //临近交割月合约限仓参数变化
#define	ET_FALLRISE         '3'           //期货合约进入交割月后涨跌停板参数发生变化
#define	ET_REUSEDATE        '4'           //期货合约套期保值额度重复使用的终止日期的下一个交易日
#define	ET_ENDTRADEDATE     '5'           //合约最后交易日
#define	ET_ENDDELIVERYDATE  '6'           //期货合约最后交割日
#define	ET_EXPIRATIONDATE   '7'           //期权合约到期日
#define	ET_LOGOUTDATE       '8'           //合约停牌日
#define	ET_WBILLVALID       '9'           //仓单有效期

// 涨跌方向
#define	RF_RISE             '0'           //涨停
#define	RF_FALL             '1'           //跌停

// 委托和盈利净持仓的关系
#define	QR_LARGE            '0'           //委托量大于盈利净持仓
#define	QR_SMALLEQUAL       '1'           //委托量小于等于盈利净持仓

// 一次性交割提前处理意向的状态
#define	AD_SELL             '0'           //卖方意向
#define	AD_SELLCANCEL       '1'           //卖方意向取消
#define	AD_SUCCESS          '2'           //提前处理成功

// 接受回报范围
#define	RR_ALL              '0'           //全部席位
#define	RR_THIS             '1'           //本席位

// 做市商报价监控违规原因
#define	MB_NORMAL           '0'           //正常
#define	MB_ORDERQTY         '1'           //报价数量不足
#define	MB_ORDERDEPTH       '2'           //报价档位深度不足
#define	MB_ORDERINTER       '3'           //报价档位间隔超限
#define	MB_PRICEOVER        '4'           //最优买卖价差超限
#define	MB_ORDEROVER        "10"	          //单笔下单量超限
#define	MB_LOCKOVER         "11"	          //单笔对敲成交量超限

// 期货合约状态
#define	FC_ON               '1'           //已上市
#define	FC_LOGOUT           '2'           //已摘牌

// 发货方式
#define	CS_NORMAL           '0'           //正常
#define	CS_PROTOCOL         '1'           //协商

// 用户状态
#define	US_NORMAL           '0'           //正常
#define	US_FROZEN           '1'           //冻结
#define	US_TEMPFROZEN       '2'           //临时冻结
#define	US_LOGOUT           '3'           //注销

// 开户申请处理结果
#define	DS_APPLY            '0'           //申请
#define	DS_SUCCESS          '1'           //开户成功
#define	DS_REFUSE           '2'           //交易所拒绝开户
#define	DS_CANCEL           '3'           //客户撤销申请

// 联络人员子类型
#define	CS_CHAIRMAN         "00"	          //董事长
#define	CS_DCHAIRMAN        "01"	          //副董事长
#define	CS_MANAGER          "02"	          //总经理
#define	CS_DMANAGER         "03"	          //副总经理
#define	CS_BIZ              "10"	          //业务联系人
#define	CS_TRADER           "11"	          //交易员
#define	CS_SEAT             "12"	          //营业部负责人
#define	CS_OTHERS           "13"	          //其他

// 用户类型
#define	UT_ACCESS           '0'           //接入点
#define	UT_MANAGE           '1'           //用户
#define	UT_SERVER           '2'           //服务器
#define	UT_APP              '3'           //应用程序

// 证书启禁标志
#define	CF_PERMIT           '0'           //启用
#define	CF_FORBID           '1'           //禁用

// CA认证的操作类型
#define	CO_SIGNVALIDATE     '0'           //签名验证
#define	CO_SIGN             '1'           //签名

// CA认证的签名状态
#define	CS_SUCCESS          '0'           //成功
#define	CS_FAIL             '1'           //失败
#define	CS_UNDEAL           '2'           //未处理

// 表同步类型
#define	ST_DAY              '0'           //日增型
#define	ST_TIMESTAMP        '1'           //时间戳类型
#define	ST_COVER            '2'           //完全覆盖型

// 同步方式
#define	SS_AUTO             '0'           //自动
#define	SS_MANUAL           '1'           //手工

// 同步周期
#define	SC_HALF             '0'           //半天
#define	SC_ONE              '1'           //一天

// 同步清理周期
#define	SCC_DAY             '0'           //每天
#define	SCC_MONDAY          '1'           //每周一
#define	SCC_MONTH           '2'           //每月1日
#define	SCC_SEASON          '3'           //每季度1日
#define	SCC_YEAR            '4'           //每年1日

// 同步清理方式
#define	SCS_NORMAL          '0'           //正常清理
#define	SCS_PART            '1'           //分区表技术

// 同步操作类型
#define	SOT_SYNC            '0'           //数据同步
#define	SOT_CLEAN           '1'           //数据清理

// 同步操作结果
#define	SOR_SUCCESS         '0'           //成功
#define	SOR_FAIL            '1'           //失败

// 数据库连接状态
#define	DC_VALID            '0'           //有效
#define	DC_INVALID          '1'           //无效

// 菜单功能点关系类型
#define	MR_MAIN             '1'           //主功能点
#define	MR_SEC              '2'           //辅助功能点

// 审核状态
#define	AT_NOCHECKED        '0'           //未审核
#define	AT_CHECKED          '1'           //已审核

// 发票类型
#define	IT_TRADEFEE         '0'           //交易手续费
#define	IT_EXECTRADEFEE     '1'           //期权执行手续费
#define	IT_FTSTRADEFEE      '2'           //期转现手续费
#define	IT_PAYMENT          '3'           //交割货款
#define	IT_DELIVERYFEE      '4'           //交割手续费
#define	IT_MORTFEE          '5'           //质押手续费
#define	IT_YEARFEE          '6'           //年会费
#define	IT_ADDFEE           '7'           //附加席位费

// 资金划拨方向
#define	MD_IN               '0'           //划入
#define	MD_OUT              '1'           //划出
#define	MD_INOUT            '2'           //划入或划出

// 其他资金类型状态
#define	OTS_NORMAL          '0'           //正常
#define	OTS_LOGOUT          '1'           //注销

// 会员状态
#define	MS_VALID            '0'           //有效
#define	MS_LOGOUT           '1'           //注销
#define	MS_OUTBIZ           '2'           //歇业

// 质押计算流水类型
#define	MC_IN               '0'           //质入
#define	MC_MANUALOUT        '1'           //手工质出
#define	MC_DUEOUT           '2'           //期满质出
#define	MC_UPD              '3'           //质押调整

// 多空方向
#define	D_LONG              '0'           //多
#define	D_SHORT             '1'           //空

// 计量单位
#define	MU_TON              '0'           //吨
#define	MU_KL               '1'           //公升

// 期权类型
#define	OT_IN               '0'           //实值期权
#define	OT_OUT              '1'           //虚值期权
#define	OT_AT               '2'           //平值期权

// 期权执行方式
#define	ES_INEXEC           '0'           //实值自动执行
#define	ES_INNOEXEC         '1'           //实值申请不执行
#define	ES_OUTEXEC          '2'           //虚值申请执行

// 第2腿与第3腿的月份远近关系
#define	MR_SAME             '0'           //相同
#define	MR_NEAR             '1'           //由近及远
#define	MR_FAR              '2'           //由远及近

// 豁免原因
#define	ER_OPTPRICE         '0'           //期权价格异常波动
#define	ER_EXCH             '1'           //交易所批准的豁免
#define	ER_ASSETPRICE       '2'           //期权标地资产价格异动

// 预报或变现后重报
#define	WR_PRE              '0'           //预报
#define	WR_AGAIN            '1'           //变现后重报

// 免复检类型
#define	RT_NO               '0'           //非免复检
#define	RT_YES              '1'           //免复检
#define	RT_AGAIN            '2'           //重检

// 结算价计算方案
#define	RT_WHOLEDAY         '0'           //整天
#define	RT_BEFORECLOSE      '1'           //收盘前
#define	RT_THEORYPRICE      '2'           //理论结算价

// 交割流水操作类型
#define	DJ_INPUT            '0'           //交割预报
#define	DJ_RET              '1'           //预报定金返还
#define	DJ_REG              '2'           //注册
#define	DJ_LOGOUT           '3'           //注销
#define	DJ_IMPAWN           '4'           //冲抵
#define	DJ_WITHDRAW         '5'           //清退
#define	DJ_PREWITHDRAW      '6'           //预清退
#define	DJ_TRANSIN          '7'           //过户转入
#define	DJ_TRANSOUT         '8'           //过户转出
#define	DJ_DELIVERYTRANSIN  '9'           //交割买入
#define	DJ_DELIVERYTRANSOUT 'A'           //交割卖出
#define	DJ_MORTFROZEN       'B'           //质押冻结
#define	DJ_MORTRELEASE      'C'           //质押释放
#define	DJ_OTHERFROZEN      'D'           //其他冻结
#define	DJ_OTHERRELEASE     'E'           //其他释放

// 划款申请处理标志
#define	TA_UNDEALED         '0'           //未做处理
#define	TA_DEALED           '1'           //已做处理
#define	TA_CANCE            '2'           //用户撤消

// 限制类型
#define	LT_NONE             '0'           //无限制
#define	LT_COUNT            '1'           //使用次数限制
#define	LT_TIME             '2'           //使用时间限制
#define	LT_BOTH             '3'           //都限制

// 套期保值额度审批流水类型
#define	CT_CONFIRM          '0'           //审批
#define	CT_DEL              '1'           //删除

// 看涨（跌）期权组合类型
#define	CT_LONGCALL         '5'           //多头看涨组合
#define	CT_LONGPUT          '6'           //多头看跌组合
#define	CT_SHORTCALL        '7'           //空头看涨组合
#define	CT_SHORTPUT         '8'           //空头看跌组合

// 检查类型
#define	CT_BEFOREINIT       '0'           //初始化前检查
#define	CT_AFTERINIT        '1'           //初始化后检查
#define	CT_CLEAR            '2'           //结算后检查
#define	CT_BIZ              '3'           //业务数据检查
#define	CT_INIT             '4'           //数据准备
#define	CT_GENDATA          '5'           //数据文件生成
#define	CT_FTPDATA          '6'           //分发数据文件
#define	CT_SYNC             '7'           //数据同步检查

// 仓库信息类型
#define	WI_BASIC            '0'           //基本信息
#define	WI_CONNECT          '1'           //联系方式
#define	WI_CUBAGE           '2'           //库容
#define	WI_TRANSPORT        '3'           //运输
#define	WI_FINANCE          '4'           //财务状况
#define	WI_OTHER            '5'           //其他

// 计数器类型
#define	CT_DAY              '0'           //日清
#define	CT_MONTH            '1'           //月清
#define	CT_YEAR             '2'           //年清
#define	CT_NONE             '3'           //不清

// 联络人员类型
#define	CT_MANAGER          '0'           //高管人员
#define	CT_STAFF            '1'           //工作人员

// 保证金参数类型
#define	MP_COMMON           '0'           //一般月份
#define	MP_NEARBY           '1'           //临近交割月份
#define	MP_POSI             '2'           //持仓量
#define	MP_LIMIT            '3'           //涨跌停
#define	MP_NEW              '4'           //新开仓

// 限仓类型
#define	PQ_COMMON           '0'           //一般月份
#define	PQ_NEARBY           '1'           //临近交割月份

// 仓单流水数据来源
#define	WDS_DELIVERY        '0'           //交割
#define	WDS_WEB             '1'           //会员服务
#define	WDS_CLOSE           '2'           //闭市后处理

// 上场指令类型
#define	CT_MONEYIO          '0'           //出入金
#define	CT_HEDGEQUOTA       '1'           //套保额度审批
#define	CT_CLIENTREG        '2'           //客户开户
#define	CT_CONTRACTRIGHT    '3'           //合约交易权限
#define	CT_SEATRIGHT        '4'           //席位交易权限
#define	CT_CLIENTRIGHT      '5'           //客户交易权限

// 上场指令状态
#define	CS_NOTCONFIRM       '0'           //未确认
#define	CS_CMDSUCCESS       '1'           //成功
#define	CS_BIZERROR         '2'           //业务错误
#define	CS_SYSERROR         '3'           //系统错误

// 登录类型（场上用）
#define	LT_MANAGER          '0'           //管理员
#define	LT_TRADER           '1'           //席位

// 认证方法
#define	CS_LOGIN            '0'           //登录时有CA认证
#define	CS_CANCELSINGLE     '1'           //取消单个CA时的授权码认证
#define	CS_CANCELWHOLE      '2'           //整个系统都取消CA时的认证
#define	CS_CACOLLAPSE       '3'           //CA宕机

// 管理员类型（场上用）
#define	MT_TRADECTRL        '0'           //交易控制员
#define	MT_TECHMANAG        '1'           //技术管理员

// 过户申请状态
#define	TAS_INPUT           '0'           //录入
#define	TAS_SUCCESS         '1'           //审批成功
#define	TAS_FAIL            '2'           //审批失败
#define	TAS_INCONFIRM       '3'           //转入方确认

// 大户报告指标
#define	LRT_POSIRATIO       '1'           //持仓量比例
#define	LRT_RESERVE         '2'           //结算准备金
#define	LRT_RESERVERATIO    '3'           //结算准备金比例
#define	LRT_RESERVECHG      '4'           //结算准备金变化
#define	LRT_BOTH            '5'           //结算准备金加交易保证金
#define	LRT_BOTHRATIO       '6'           //结算准备金加交易保证金比例
#define	LRT_BOTHCHG         '7'           //结算准备金加交易保证金变化
#define	LRT_MATCHQTY        '8'           //成交量

// 大户报告对象
#define	LRO_SELF            '0'           //自营会员
#define	LRO_AGENT           '1'           //经纪会员
#define	LRO_CLIENT          '2'           //客户
#define	LRO_SELFCLEAR       '3'           //结算帐号类型为自营的结算会员
#define	LRO_AGENTCLEAR      '4'           //结算帐号类型为代理的结算会员
#define	LRO_TOT             '5'           //综合会员

// 大户报告月份类型
#define	MT_COMMON           '0'           //一般月
#define	MT_SPOT             '1'           //交割前月
#define	MT_DELIVERY         '2'           //交割月

// 大户报告粒度
#define	LRT_VARIETY         '0'           //品种
#define	LRT_CONTRACT        '1'           //合约
#define	LRT_SERIES          '2'           //系列

// 大户报告状态
#define	LRS_GEN             '0'           //生成
#define	LRS_HANDDEL         '1'           //手工删除
#define	LRS_EXEMPTSEND      '2'           //免发送
#define	LRS_SEND            '3'           //发送

// 重点客户类型
#define	SCT_SPOT            '0'           //资金量大的现货商
#define	SCT_CTRL            '1'           //操纵市场和有操纵嫌疑的客户

// 持有时间类型
#define	HTT_BELOW3          '0'           //3日以内
#define	HTT_BELOW10         '1'           //10日以内
#define	HTT_ON10            '2'           //10日以上
#define	HTT_NOTSURE         '3'           //不确定

// 了结方式
#define	DT_OFFSET           '0'           //平仓
#define	DT_DELIVERY         '1'           //交割
#define	DT_PARTDELIVERY     '2'           //部分交割

// 确认标志
#define	CF_NO               '0'           //未确认
#define	CF_YES              '1'           //已确认

// 手续费设置对象
#define	FO_VARIETY          '1'           //品种有效
#define	FO_CONTRACT         '2'           //合约有效

// 期权手续费设置对象
#define	OFO_VARIETY         '1'           //期权品种有效
#define	OFO_SERIES          '2'           //期权系列有效

// 批量资金录入类型
#define	BT_ADD              '1'           //新增
#define	BT_NOTCHK           '2'           //存在未复核

// 控制类型
#define	CT_SYSSTATUS        '0'           //系统状态

// 资金变化原因
#define	MCR_TRANS           '0'           //交割货款划转
#define	MCR_EXEC            '1'           //期权执行

// 持仓变化原因
#define	PCR_RULEOFFSET      '0'           //强减
#define	PCR_FTS             '1'           //期转现
#define	PCR_ROLL            '2'           //滚动交割
#define	PCR_SPECTOHEDGE     '3'           //投机转套保
#define	PCR_EXECOPEN        '4'           //期权执行期货建仓
#define	PCR_EXECMATCH       '5'           //期权执行交割配对
#define	PCR_CONGREGATE      '6'           //一次性交割
#define	PCR_DELIVERYPOSI    '7'           //交割持仓对冲
#define	PCR_NOTEXEC         '8'           //期权到期未执行

// 消息源
#define	CF_BIZ              '0'           //业务管理系统
#define	CF_TECH             '1'           //技术管理系统
#define	CF_TRADE            '2'           //交易子系统

// 提示状态
#define	HS_VALID            '0'           //有效
#define	HS_UNVALID          '1'           //失效
#define	HS_DEL              '2'           //删除

// 接收者生成方式
#define	GS_DEFAULT          '0'           //系统默认
#define	GS_AUTH             '1'           //根据权限生成

// 发送限制
#define	SL_ALL              '0'           //全体用户
#define	SL_ONLINE           '1'           //在线用户

// 资金类别
#define	MS_INPUT            '0'           //手工录入
#define	MS_CONFIRM          '1'           //需要确认
#define	MS_AUTOYES          '2'           //自动生成且计入其他资金
#define	MS_AUTONO           '3'           //自动生成且不计入其他资金

// $
#define	G_GENERAL           '$'           //全部

// 录入类型
#define	IT_SINGEL           '0'           //单笔
#define	IT_BATCH            '1'           //批量

// 检验结果
#define	CR_RIGHT            '0'           //正确
#define	CR_WRONG            '1'           //错误

// 交易员申请状态
#define	CA_APPLYING         '0'           //申请待审批
#define	CA_DELING           '1'           //删除待审批
#define	CA_SUCCESS          '2'           //已审批
#define	CA_DEL              '3'           //已删除
#define	CA_COMPLETEDEL      '4'           //已彻底删除
#define	CA_REFUSE           '5'           //申请被拒绝

// 平仓类型
#define	OT_OFFSET           '0'           //平仓
#define	OT_FORCE            '1'           //强平
#define	OT_RULEOFFSET       '2'           //强减
#define	OT_FTS              '3'           //期转现
#define	OT_ROLL             '4'           //滚动交割
#define	OT_SPECTOHEDGE      '5'           //投机转套保
#define	OT_DELIVERYPOSI     '6'           //交割持仓对冲
#define	OT_CONGREGATE       '7'           //一次性交割
#define	OT_OPTDELIVERY      '8'           //期权交割持仓平仓

// 同步状态（交易写数据库）
#define	SS_START            '0'           //同步开始
#define	SS_FINISH           '1'           //同步完成

// 交易期间状态
#define	PS_USED             '0'           //已使用
#define	PS_SETNOUSE         '1'           //已设置未使用
#define	PS_NOTSET           '2'           //未设置

// 工作人员类型
#define	ST_MANAGER          '0'           //高管人员
#define	ST_STAFF            '1'           //工作人员

// 职务类型
#define	DT_CHAIRMAN         "00"	          //董事长
#define	DT_DCHAIRMAN        "01"	          //副董事长
#define	DT_MANAGER          "02"	          //总经理
#define	DT_DMANAGER         "03"	          //副总经理
#define	DT_OTHERM           "04"	          //其他
#define	DT_CLIENTREG        "10"	          //客户登记
#define	DT_CLEAR            "11"	          //结算员
#define	DT_DELIVERYER       "12"	          //交割员
#define	DT_OTHERS           "13"	          //其他

// 中性组合类型
#define	NT_BUY              'A'           //买进期货与卖出对应期货合约的买权
#define	NT_SELL             'B'           //卖出期货与卖出对应期货合约的卖权

// 划款单类型
#define	TT_WH_FEE           '0'           //仓储费
#define	TT_DELIVERY_MONEY   '1'           //交割货款

// 自动转换报警
#define	AA_ALARM            "80"	          //自动转换报警

// 授权码类型
#define	GT_NO               '0'           //不使用授权码
#define	GT_DATE             '1'           //使用日期范围作授权码有效期
#define	GT_CNT              '2'           //使用授权码使用次数作授权码有效期
#define	GT_BOTH             '3'           //日期和次数同时生效

// 拒绝客户登记的理由
#define	RR_ID               '0'           //客户身份证号填写有误，请修改后提交
#define	RR_NAME             '1'           //客户姓名填写有误，请修改后提交
#define	RR_UNIT             '2'           //客户单位名称填写有误，请修改后提交
#define	RR_REG              '3'           //客户注册号填写有误，请修改后提交
#define	RR_OVER             '4'           //客户一个月审核期已过，退回申请单位
#define	RR_NOTENOUGH        '5'           //客户资料必须全面，正确填写，请修改后提交
#define	RR_OTHER            '6'           //其他

// 交易权限（全）
#define	TF_NONE             '0'           //不可交易
#define	TF_OFFSETONLY       '1'           //只可平仓
#define	TF_OK               '2'           //可以交易

// 强减执行标志
#define	RO_NONE             '0'           //未设置
#define	RO_EXEC             '1'           //执行
#define	RO_NOEXEC           '2'           //不执行

// 系统禁开状态
#define	FF_NONE             '0'           //无
#define	FF_CALL             '1'           //追保禁开
#define	FF_FORCE            '2'           //强平禁开

// 强平委托状态
#define	FOS_TRIG            'r'           //已触发
#define	FOS_COMPLETE        'c'           //完全成交
#define	FOS_PART            'p'           //部分成交
#define	FOS_CANCEL          'd'           //客户撤单
#define	FOS_FORCECANCEL     'f'           //强平撤单
#define	FOS_FORCEUPD        'u'           //强平修改

// 结算保证金率计算调用时段
#define	CTY_CLEAR           '0'           //结算
#define	CTY_INIT            '1'           //初始化

// 测算状态
#define	TS_START            "00"	          //测算开始
#define	TS_TESTING          "50"	          //测算中
#define	TS_TESTED           "90"	          //测算完成

// 用户信息类型
#define	IT_BASIC            '1'           //用户基本信息
#define	IT_AUTH             '2'           //用户权限信息

// 结算类型
#define	CT_TODAY            '0'           //当日结算
#define	CT_HIS              '1'           //历史结算

// 登录模式
#define	LM_GRANT            '0'           //授权码登录
#define	LM_CA               '1'           //CA登录


//总共197个
#endif
