#ifndef __MACROINSTANCEDEF_H__
#define __MACROINSTANCEDEF_H__
/* 仅内部使用 */

#include "GlobexMarketAdapter.h"
#include "GlobexTraderAdapter.h"

#define __MARKETCLASS GLOBEX_NS::GlobexMarketAdapter
#define __TRADERCLASS GLOBEX_NS::GlobexTraderAdapter

#endif