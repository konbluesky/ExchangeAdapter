#include "ExchangeAdapterConfig.h"
#include "GlobexMarketAdapter.h"
#define TIXML_USE_STL
#include "tinyxml.h"
#include "simsingleton.h"
#include "FixInitiator.h"
#include "DebugUtils.h"
#include "LocalCompile.h"

GLOBEX_NS_BEGIN

std::pair<bool,GlobexMarketAdapter::InitParam> 
	GlobexMarketAdapter::InitParam::LoadConfig(const tstring& fname)
{
	InitParam param;
	TiXmlDocument doc(fname.c_str());
	bool loadOkay = doc.LoadFile();
	TiXmlNode* pNode = NULL;
	tstring str;

	std::memset(&param,0,sizeof(param));
	if (!loadOkay)
		return std::make_pair(false,param);

	pNode = doc.FirstChild("configure");
	XML_GET_STRING(pNode,"cfgpath",param.fixcfgpath);
	XML_GET_STRING(pNode,"cfgpath",param.fixcfgpath);
	XML_GET_STRING(pNode,"targetsubid",param.targetsubid);
	XML_GET_STRING(pNode,"sendersubid",param.sendersubid);
	XML_GET_STRING(pNode,"senderlocationid",param.senderlocationid);
	XML_GET_STRING(pNode,"pricepassword",param.pricepassword);
	XML_GET_STRING(pNode,"agentid",param.agentid);

	return std::make_pair(true,param);
}

GlobexMarketAdapter::GlobexMarketAdapter()
{
	std::memset(&m_para,0,sizeof(m_para));
}

GlobexMarketAdapter::~GlobexMarketAdapter()
{
}

tstring GlobexMarketAdapter::GetName()
{
	return "GLOBEXMarket";
}

bool GlobexMarketAdapter::InitializeByXML(const tchar* cfgpath)
{
	std::pair<bool,InitParam> it = 
		InitParam::LoadConfig(cfgpath);
	if(false == it.first)
		return false;
	return Initialize((void*)(&(it.second)));
}

bool GlobexMarketAdapter::Initialize(void *para)
{
	m_para = *((InitParam*)para);
	GlobexFixInitiator &pfix = 
		simpattern::Singleton_InClass<GlobexFixInitiator>::Instance();
	DBG_ASSERT(false == pfix.IsStarted());

	FixInitiatorPara fixpara;
	GlobexFixInitiatorPara globexfixpara;
	fixpara.fixcfgpath = m_para.fixcfgpath;
	globexfixpara.type = globexfixpara.TRADER;
	globexfixpara.password = m_para.pricepassword;
	globexfixpara.senderlocationid = m_para.senderlocationid;
	globexfixpara.sendersubid = m_para.sendersubid;
	globexfixpara.targetsubid = m_para.targetsubid;

	if(!pfix.GlobexInit(globexfixpara) || !pfix.Register(m_para.agentid,this) ||
		!pfix.Init(fixpara))
		return false;

	return true;
}

void GlobexMarketAdapter::UnInitialize()
{
	GlobexFixInitiator &pfix = 
		simpattern::Singleton_InClass<GlobexFixInitiator>::Instance();
	pfix.Uninit();
	pfix.Unregister(m_para.agentid);
	pfix.GlobexUninit();
}

bool GlobexMarketAdapter::Login(void*)
{
	GlobexFixInitiator &pfix = 
		simpattern::Singleton_InClass<GlobexFixInitiator>::Instance();

	if(0 > pfix.Start())
		return false;
	return true;
}

bool GlobexMarketAdapter::Logout(void*)
{
	FixInitiator &pfix = 
		simpattern::Singleton_InClass<FixInitiator>::Instance();
	pfix.Stop();
	return true;
}

bool GlobexMarketAdapter::SupportDoM()
{
	return true;
}

void GlobexMarketAdapter::Response(FIX::Message &msg)
{
	OnResponseMsg(msg);
}

void GlobexMarketAdapter::AgentOnNetConnected(){}

void GlobexMarketAdapter::AgenOnNetDisconnected(){}

void GlobexMarketAdapter::AgenOnLogin()
{
	std::cout<<"CME报价登录成功"<<std::endl;
	PriOnLogin();
}

void GlobexMarketAdapter::AgenOnLogout()
{
	std::cout<<"CME报价退出成功"<<std::endl;
	PriOnLogout();
}

GLOBEX_NS_END