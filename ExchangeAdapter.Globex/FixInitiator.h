#ifndef __FIX_INITIATOR_H__
#define __FIX_INITIATOR_H__

/**
 * 这部分代码后面需要独立出去
 */ 

#include "GlobexCommon.h"
#include "autoptr.h"

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4275)
#else
#error "不应该出现,以待其他"
#endif


GLOBEX_NS_BEGIN

class IFixInitiatorAgent;

struct FixInitiatorPara {
	tstring fixcfgpath;
};

class FixInitiator : public FIX::Application {
public:
	FixInitiator();
	virtual ~FixInitiator();

public:
	bool Init(FixInitiatorPara &para);
	void Uninit();
	int32 Start();
	void Stop();
	bool IsStarted() {return m_bstarted;}

	bool Register(const tstring &agentid,IFixInitiatorAgent *pagent);
	bool Unregister(const tstring &agentid);

	void Request(const tstring &agentid,FIX::Message& msg);
	virtual void PreToAdm(const FIX::SessionID&,FIX::Message*){};
	virtual void PreToApp(const FIX::SessionID&,FIX::Message*){};
public:
	void onCreate( const FIX::SessionID& seID);
	void onLogon( const FIX::SessionID& seID);
	void onLogout( const FIX::SessionID& seID);
	void toAdmin( FIX::Message& msg, const FIX::SessionID& seID);
	void toApp( FIX::Message& msg, const FIX::SessionID& seID);
	void fromAdmin( const FIX::Message& msg,const FIX::SessionID& seID);
	void fromApp( const FIX::Message& msg, const FIX::SessionID& seID);

private:
	boost::shared_ptr<FIX::SessionSettings> m_pSessionSettings;
	boost::shared_ptr<FIX::MessageStoreFactory> m_pMsgStoreFactory;	
	boost::shared_ptr<FIX::FileLogFactory> m_pLogFactory;
	boost::shared_ptr<FIX::SocketInitiator> m_pInitiator;
	std::map<tstring,IFixInitiatorAgent*> m_agents;
	std::map<tstring,FIX::SessionID> m_seIDs;
	FixInitiatorPara m_para;
	bool m_bstarted;
	bool m_binitialized;
};

class IFixInitiatorAgent{
public:
	IFixInitiatorAgent():m_bstarted(false){}
	virtual ~IFixInitiatorAgent(){};

public:
	virtual void Response(FIX::Message &msg) = 0;
	virtual void AgentOnNetConnected() = 0;
	virtual void AgenOnNetDisconnected() = 0;
	virtual void AgenOnLogin() = 0;
	virtual void AgenOnLogout() = 0;

public:
	bool IsStarted(void){return m_bstarted;}

protected:
	bool m_bstarted;
};

struct GlobexFixInitiatorPara {
	enum {
		TRADER = 0,
		MARKET,
	} type;
	tstring password;
	tstring targetsubid;
	tstring sendersubid;
	tstring senderlocationid;
};

class GlobexFixInitiator : virtual public FixInitiator{
public:
	GlobexFixInitiator(): FixInitiator()
	{
		std::memset(&m_para,0,sizeof(GlobexFixInitiatorPara));
	}
	virtual ~GlobexFixInitiator(){}
public:
	bool GlobexInit(GlobexFixInitiatorPara &para);
	void GlobexUninit();
	virtual void PreToAdm(const FIX::SessionID& seID,FIX::Message* pmsg);
	virtual void PreToApp(const FIX::SessionID& seID,FIX::Message* pmsg);

private:
	GlobexFixInitiatorPara m_para;
};

GLOBEX_NS_END

#ifdef _MSC_VER
#pragma warning(pop)
#else
#error "仅支持VC,以待修改"
#endif

#endif