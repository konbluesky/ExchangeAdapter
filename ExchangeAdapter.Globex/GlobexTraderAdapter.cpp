#include "ExchangeAdapterConfig.h"
#include "GlobexTraderAdapter.h"
#define TIXML_USE_STL
#include "tinyxml.h"
#include "simsingleton.h"
#include "FixInitiator.h"
#include "DebugUtils.h"
#include "LocalCompile.h"

GLOBEX_NS_BEGIN

std::pair<bool,GlobexTraderAdapter::InitParam> 
	GlobexTraderAdapter::InitParam::LoadConfig(const tstring& fname)
{
	InitParam param;
	TiXmlDocument doc(fname.c_str());
	bool loadOkay = doc.LoadFile();
	TiXmlNode* pNode = NULL;
	tstring str;

	std::memset(&param,0,sizeof(param));
	if (!loadOkay)
		return std::make_pair(false,param);

	pNode = doc.FirstChild("configure");
	XML_GET_STRING(pNode,"cfgpath",param.fixcfgpath);
	XML_GET_STRING(pNode,"orderpassword",param.orderpassword);
	XML_GET_STRING(pNode,"targetsubid",param.targetsubid);
	XML_GET_STRING(pNode,"sendersubid",param.sendersubid);
	XML_GET_STRING(pNode,"senderlocationid",param.senderlocationid);
	XML_GET_STRING(pNode,"agentid",param.agentid);

	return std::make_pair(true,param);
}

GlobexTraderAdapter::GlobexTraderAdapter(){}

GlobexTraderAdapter::~GlobexTraderAdapter(){}

tstring GlobexTraderAdapter::GetName(){return "GLOBEXTrader";}

bool GlobexTraderAdapter::InitializeByXML(const tchar* cfgpath)
{
	std::pair<bool,InitParam> it = InitParam::LoadConfig(cfgpath);
	if(false == it.first)
		return false;
	return Initialize((void*)(&(it.second)));
}

bool GlobexTraderAdapter::Initialize(void* ppara)
{
	GlobexFixInitiator &pfix = 
		simpattern::Singleton_InClass<GlobexFixInitiator>::Instance();
	if(pfix.IsStarted())
		return false;

	m_para = *((InitParam*)ppara);

	FixInitiatorPara fixpara;
	GlobexFixInitiatorPara globexfixpara;
	fixpara.fixcfgpath = m_para.fixcfgpath;
	globexfixpara.type = globexfixpara.TRADER;
	globexfixpara.password = m_para.orderpassword;
	globexfixpara.senderlocationid = m_para.senderlocationid;
	globexfixpara.sendersubid = m_para.sendersubid;
	globexfixpara.targetsubid = m_para.targetsubid;

	if(!pfix.GlobexInit(globexfixpara) || !pfix.Register(m_para.agentid,this) ||
		!pfix.Init(fixpara))
		return false;
	return true;
}

bool GlobexTraderAdapter::Login(void*)
{
	GlobexFixInitiator &pfix = 
		simpattern::Singleton_InClass<GlobexFixInitiator>::Instance();

	if(0 > pfix.Start())
		return false;
	return true;
}

bool GlobexTraderAdapter::Logout(void*)
{
	GlobexFixInitiator &pfix = 
		simpattern::Singleton_InClass<GlobexFixInitiator>::Instance();
	pfix.Stop();
	return true;
}

void GlobexTraderAdapter::UnInitialize()
{
	GlobexFixInitiator &pfix = 
		simpattern::Singleton_InClass<GlobexFixInitiator>::Instance();
	pfix.Uninit();
	pfix.Unregister(m_para.agentid);
	pfix.GlobexUninit();
}

void GlobexTraderAdapter::RequestFix(FIX::Message &msg)
{
	GlobexFixInitiator &pfix = 
		simpattern::Singleton_InClass<GlobexFixInitiator>::Instance();
	pfix.Request(m_para.agentid,msg);
}

void GlobexTraderAdapter::Response(FIX::Message &msg)
{
	OnResponseMsg(msg);
}

void GlobexTraderAdapter::AgentOnNetConnected(){}

void GlobexTraderAdapter::AgenOnNetDisconnected(){}

void GlobexTraderAdapter::AgenOnLogin()
{
	std::cout<<"CME交易登录成功"<<std::endl;
	PriOnLogin();
}

void GlobexTraderAdapter::AgenOnLogout()
{
	std::cout<<"CME交易退出成功"<<std::endl;
	PriOnLogout();
}

GLOBEX_NS_END