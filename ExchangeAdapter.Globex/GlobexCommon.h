#ifndef __GLOBEX_COMMON_H__
#define __GLOBEX_COMMON_H__

#include "ExchangeAdapterBase.h"

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4275)
#pragma warning(disable:4100)
#pragma warning(disable:4512)
#pragma warning(disable:4251)
#pragma warning(disable:4125)
#pragma warning(disable:4290)
#else
#error "仅支持VC,以待其他"
#endif

#include "quickfix/Message.h"
#include "quickfix/Application.h"
#include "quickfix/FileStore.h"
#include "quickfix/SocketInitiator.h"
#include "quickfix/FileLog.h"
#include "quickfix/Session.h"
#include "quickfix/MessageCracker.h"

#ifdef _MSC_VER
#pragma warning(pop)
#else
#error "仅支持VC,以待修改"
#endif


#define GLOBEX_NS ExchangeAdapter_Globex
#define GLOBEX_NS_BEGIN namespace GLOBEX_NS {
#define GLOBEX_NS_END	}
#define	USING_GLOBEX_NS	using namespace GLOBEX_NS;

#endif