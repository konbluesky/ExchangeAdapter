#include "ExchangeAdapterConfig.h"
#include "macroinstancedef.h"
#include "LocalCompile.h"

EXCAPI 	IFixMarketProvider* CreateMarketDataProvider()
{
	return new __MARKETCLASS;
}

EXCAPI 	void DestoryMarketDataProvider(IFixMarketProvider* adapter)
{
	if (adapter !=NULL)
		delete adapter;
}

EXCAPI	IFixTraderProvider* CreateTraderProvider()
{
	return new __TRADERCLASS;
}

EXCAPI	void DestoryTraderProvider(IFixTraderProvider* adapter)
{
	if (adapter !=NULL)
		delete adapter;
}