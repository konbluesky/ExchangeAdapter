#ifndef __GLOBEX_TRADER_ADAPTER_H__
#define __GLOBEX_TRADER_ADAPTER_H__

/**
 * 目前暂时让该部分只支持一个交易所.
 * 实际上该部分可以支持很多支持标准FIX协议的交易所.目前暂时先这样实现.
 * 后续考虑使用该部分支持多个交易实体.
 */

#include "GlobexCommon.h"
#include "FixInitiator.h"
#include "IFixTrader.h"

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4275)
#else
#error "不应该出现,以待其他"
#endif

GLOBEX_NS_BEGIN

class GlobexTraderAdapter : public IFixTraderProvider,
	public IFixInitiatorAgent{
public:
	GlobexTraderAdapter();
	virtual ~GlobexTraderAdapter();

public:
	struct InitParam {
		tstring fixcfgpath;

		tstring orderpassword;
		tstring targetsubid;
		tstring sendersubid;
		tstring senderlocationid;

		tstring agentid;
		static std::pair<bool,InitParam> LoadConfig(
			const tstring& fname);
	};

	virtual tstring GetName();
	virtual bool Initialize(void* );
	virtual bool InitializeByXML(const tchar *cfgpath);
	virtual void UnInitialize();
	virtual bool Login(void*);
	virtual bool Logout(void*);
	
	virtual void RequestFix(FIX::Message &msg);

	virtual void AgentOnNetConnected();
	virtual void AgenOnNetDisconnected();
	virtual void AgenOnLogin();
	virtual void AgenOnLogout();

protected:
	void Response(FIX::Message &msg);

private:
	InitParam m_para;
};

GLOBEX_NS_END

#ifdef _MSC_VER
#pragma warning(pop)
#else
#error "仅支持VC,以待修改"
#endif

#endif