#ifndef __GLOBEX_MARKET_ADAPTER_H__
#define __GLOBEX_MARKET_ADAPTER_H__

#include "GlobexCommon.h"
#include "FixInitiator.h"
#include "IFixMarket.h"

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4275)
#else
#error "不应该出现,以待其他"
#endif


GLOBEX_NS_BEGIN

class GlobexMarketAdapter : public IFixMarketProvider,
	public IFixInitiatorAgent {
public:
	GlobexMarketAdapter();
	virtual ~GlobexMarketAdapter();
public:
	struct InitParam
	{
		tstring fixcfgpath;
		tstring targetsubid;
		tstring sendersubid;
		tstring senderlocationid;
		tstring pricepassword;
		tstring agentid;
		EXAPI static std::pair<bool,InitParam> LoadConfig(
			const tstring& fname);
	};

	tstring GetName();
	bool InitializeByXML(const tchar* cfgpath);
	bool Initialize(void *para);
	void UnInitialize();
	bool Login(void*);
	bool Logout(void*);
	bool SupportDoM();
	void Response(FIX::Message &msg);

	virtual void AgentOnNetConnected();
	virtual void AgenOnNetDisconnected();
	virtual void AgenOnLogin();
	virtual void AgenOnLogout();

	InitParam m_para;
};

GLOBEX_NS_END

#ifdef _MSC_VER
#pragma warning(pop)
#else
#error "仅支持VC,以待修改"
#endif

#endif