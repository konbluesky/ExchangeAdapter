#include "StdAfx.h"
#include "ucodehelper.h"
#include "ustringutils.h"
#include "TradeLogDB.h"
#include "loki/Singleton.h"
#include "ucomhelper.h"

#include "otldb.h"
typedef Loki::SingletonHolder<COTLDB> COTLDBHolder;
#pragma warning(disable : 4996)
USING_LIBSPACE

CTradeLogDB::CTradeLogDB(void)
{
}

CTradeLogDB::~CTradeLogDB(void)
{
}

bool CTradeLogDB::InitializeDB(const char* dsn, const char* usr, const char* pwd)
{
	bool ret = COTLDBHolder::Instance().Connect(dsn, usr, pwd);
	return ret;
}

tradeadapter::BasicAccountInfo CTradeLogDB::GetAccountInfo(std::string clientid, const char* exprovider)
{
	tradeadapter::BasicAccountInfo m_info;

	std::string sqlcmd = "SELECT [CLIENTID],\
								[CLIENTNAME] ,\
								[FUNDACCOUNT],\
								[SHSTOCKACCOUNT] , \
								[SZSTOCKACCOUNT] ,\
								[CURRENTBALANCE] ,\
								[ENABLEBALANCE] ,\
								[ASSETBALANCE] ,\
								[FETCHCASH]  FROM ACCOUNTINFO where [CLIENTID]= " + clientid + " AND [EXPROVIDER]= '" + exprovider + "'";

	otl_stream result;
	bool ret = COTLDBHolder::Instance().Select(sqlcmd, &result);
	if (ret)
	{
		while (!result.eof())
		{
			/*
			[CLIENTID] CHAR(16),
			[CLIENTNAME] CHAR,
			[FUNDACCOUNT] CHAR(16),
			[SHSTOCKACCOUNT] CHAR(16),
			[SZSTOCKACCOUNT] CHAR(16),
			[CURRENTBALANCE] DOUBLE,
			[ENABLEBALANCE] DOUBLE,
			[ASSETBALANCE] DOUBLE);
			[FETCHCASH] DOUBLE
			*/
			char CLIENTID[16] = { 0 };
			char CLIENTNAME[16] = { 0 };
			char FUNDACCOUNT[16] = { 0 };
			char SHSTOCKACCOUNT[16] = { 0 };
			char SZSTOCKACCOUNT[16] = { 0 };
			double CURRENTBALANCE = 0.0;
			double ENABLEBALANCE = 0.0;
			double ASSETBALANCE = 0.0;
			double FETCHCASH = 0.0;




			result >> CLIENTID;
			result >> CLIENTNAME;
			result >> FUNDACCOUNT;
			result >> SHSTOCKACCOUNT;
			result >> SZSTOCKACCOUNT;
			result >> CURRENTBALANCE;
			result >> ENABLEBALANCE;
			result >> ASSETBALANCE;
			result >> FETCHCASH;

			m_info.ClientID = U82TS(CLIENTID);
			m_info.ClientName = U82TS(CLIENTNAME);
			m_info.AssetBalance = ASSETBALANCE;
			m_info.CurrentBalance = CURRENTBALANCE;
			m_info.EnableBalance = ENABLEBALANCE;
			m_info.FetchCash = 0;
			m_info.FundAccount = U82TS(FUNDACCOUNT);
			m_info.SHStockAccount = U82TS(SHSTOCKACCOUNT);
			m_info.SZStockAccount = U82TS(SZSTOCKACCOUNT);
		}
	}
	else
	{
		m_info.ClientID = _T("121212");
		m_info.ClientName = _T("模拟账户");
		m_info.AssetBalance = 10000000;
		m_info.CurrentBalance = 10000000;
		m_info.EnableBalance = 10000000;
		m_info.FetchCash = 10000000;
		m_info.FundAccount = _T("testfundaccount");
		m_info.SHStockAccount = _T("shacc");
	}

	return m_info;
}


bool CTradeLogDB::InsertEntrustOrderInfo(int Date, int Time, const char* clientID, const char* StockAccount, const char* ContractCode, int buysell, double Price, int Amount, const char* Comment, int orderid, int Status, const char* exprovide)
{
	char sqlcmd[1024] = { 0 };

	sprintf(sqlcmd,
		"INSERT INTO  [ENTRUSTORDER] ( \
				[EXPROVIDER],[DATE],[TIME],[CLIENTID],[STOCKACCOUNT],[CONTRACTCODE],[BUYSELL],[PRICE],[AMOUNT],	[COMMENT],[ORDERID],[STATUS]\
						) VALUES  \
								('%s',   %d,     %d,    '%s',        '%s',         '%s',      '%d',    %f,   %d,    '%s',  %d,    '%d')",
								exprovide, Date, Time, clientID, StockAccount, ContractCode, buysell, Price, Amount, Comment, orderid, Status);
	COTLDBHolder::Instance().ExcuteSql(sqlcmd);

	return true;
}




bool CTradeLogDB::InsertDealedOrderInfo(int Date, int Time, const char* clientID, const char* StockAccount, const char* ContractCode, int buysell, double Price, int Amount, const char* Comment, int orderid, int Status, int dealordid, const char* exprovide)
{
	char sqlcmd[1024] = { 0 };

	sprintf(sqlcmd,
		"INSERT INTO  [DEALEDTORDER] ( \
				[EXPROVIDER],[DATE],[TIME],[CLIENTID],[STOCKACCOUNT],[CONTRACTCODE],[BUYSELL],[PRICE],[AMOUNT],	[COMMENT],[ORDERID],[STATUS],[DEALEDID]\
						) VALUES  \
								('%s',   %d,     %d,    '%s',        '%s',         '%s',      '%d',    %f,   %d,    '%s',  %d,    '%d',%d)",
								exprovide, Date, Time, clientID, StockAccount, ContractCode, buysell, Price, Amount, Comment, orderid, Status, dealordid);
	COTLDBHolder::Instance().ExcuteSql(sqlcmd);

	return true;
}

bool CTradeLogDB::LoadEntrustOrdersFromDB(std::list<tradeadapter::Order>* porders, int Ndate, const char* exrovide, const char* clientid)
{
	porders->clear();

	std::string  date = TS2GB(UStrConvUtils::ToString(Ndate));
	std::string  sqlcmd = std::string("SELECT [EXPROVIDER],[DATE],[TIME],[CLIENTID],[STOCKACCOUNT],[CONTRACTCODE],[BUYSELL],[PRICE],[AMOUNT],	[COMMENT],[ORDERID],[STATUS] \
									  									  FROM [ENTRUSTORDER] WHERE [DATE] = ") + date + " AND CLIENTID = '" + clientid + "'" + " AND EXPROVIDER='" + exrovide + "'";

	otl_stream result;
	if (COTLDBHolder::Instance().Select(sqlcmd, &result))
	{
		while (!result.eof())
		{
			//Name	Declared Type	Type	Size	Precision	Not Null	Not Null On Conflict	Default Value	Collate
			char EXPROVIDER[255] = { 0 };//	CHAR(255)	CHAR	255	0	False		NULL	
			int DATE = 0;//	INT	INT	0	0	False		NULL	
			int TIME = 0;//	INT	INT	0	0	False		NULL	
			char CLIENTID[64] = { 0 };//	CHAR(64)	CHAR	64	0	False		NULL	
			char STOCKACCOUNT[16] = { 0 };//	CHAR(10)	CHAR	10	0	False		NULL	
			char CONTRACTCODE[12] = { 0 };//	CHAR(12)	CHAR	12	0	False		NULL	
			int BUYSELL = 0;//	INT		0	False		NULL	
			double PRICE = 0;//	DOUBLE	DOUBLE	0	0	False		NULL	
			int AMOUNT = 0;//	INT	INT	0	0	False		NULL	
			char COMMENT[128] = { 0 };//	CHAR(128)	CHAR	128	0	False		NULL	
			int ORDERID = 0;//	INT	INT	0	0	False		NULL	
			int STATUS = 0;//	CHAR(255)	CHAR	255	0	False		NULL	

			result >> EXPROVIDER;
			result >> DATE;
			result >> TIME;
			result >> CLIENTID;
			result >> STOCKACCOUNT;
			result >> CONTRACTCODE;
			result >> BUYSELL;
			result >> PRICE;
			result >> AMOUNT;
			result >> COMMENT;
			result >> ORDERID;
			result >> STATUS;

			tradeadapter::Order order;
			order.Amount = AMOUNT;
			order.clientID = U82TS(CLIENTID);
			order.ContractCode = U82TS(CONTRACTCODE);
			order.Day = DATE;
			order.OpeDir = (tradeadapter::EBuySell)BUYSELL;
			order.Price = PRICE;
			order.Time = TIME;
			order.orderid = ORDERID;


			porders->push_back(order);
		}

		return true;
	}
	else
	{
		//	tstring error = UStrConvUtils::Format(_T("读取顶点业务数据库表[ENTRUSTORDER]时，出错了"));
		//	throw TException(error);
		return false;
	}
}

bool CTradeLogDB::LoadDealedOrderFromDB(std::list<tradeadapter::DealedOrder>* porders, int Ndate, const char* exrovide, const char* clientid)
{
	porders->clear();

	std::string  date = TS2GB(UStrConvUtils::ToString(Ndate));
	std::string  sqlcmd = std::string("SELECT [EXPROVIDER],[DATE],[TIME],[CLIENTID],[STOCKACCOUNT],[CONTRACTCODE],[BUYSELL],[PRICE],[AMOUNT],	[COMMENT],[ORDERID],[STATUS],[DEALEDID] \
									  									  FROM [DEALEDTORDER] WHERE [DATE] = ") + date + " AND CLIENTID = '" + clientid + "'" + " AND EXPROVIDER='" + exrovide + "'" +
																		  " ORDER BY DEALEDID";

	otl_stream result;
	if (COTLDBHolder::Instance().Select(sqlcmd, &result))
	{
		while (!result.eof())
		{
			//Name	Declared Type	Type	Size	Precision	Not Null	Not Null On Conflict	Default Value	Collate
			char EXPROVIDER[255] = { 0 };//	CHAR(255)	CHAR	255	0	False		NULL	
			int DATE = 0;//	INT	INT	0	0	False		NULL	
			int TIME = 0;//	INT	INT	0	0	False		NULL	
			char CLIENTID[64] = { 0 };//	CHAR(64)	CHAR	64	0	False		NULL	
			char STOCKACCOUNT[16] = { 0 };//	CHAR(10)	CHAR	10	0	False		NULL	
			char CONTRACTCODE[12] = { 0 };//	CHAR(12)	CHAR	12	0	False		NULL	
			int BUYSELL = 0;//	INT		0	False		NULL	
			double PRICE = 0;//	DOUBLE	DOUBLE	0	0	False		NULL	
			int AMOUNT = 0;//	INT	INT	0	0	False		NULL	
			char COMMENT[128] = { 0 };//	CHAR(128)	CHAR	128	0	False		NULL	
			int ORDERID = 0;//	INT	INT	0	0	False		NULL	
			int STATUS = 0;//	CHAR(255)	CHAR	255	0	False		NULL	
			int DEALEDID = 0;

			result >> EXPROVIDER;
			result >> DATE;
			result >> TIME;
			result >> CLIENTID;
			result >> STOCKACCOUNT;
			result >> CONTRACTCODE;
			result >> BUYSELL;
			result >> PRICE;
			result >> AMOUNT;
			result >> COMMENT;
			result >> ORDERID;
			result >> STATUS;
			result >> DEALEDID;

			tradeadapter::DealedOrder order;
			order.business_amount = AMOUNT;
			order.ClientID = U82TS(CLIENTID);
			order.stock_code = U82TS(CONTRACTCODE);
			order.date = DATE;
			order.entrust_bs = (tradeadapter::EBuySell)BUYSELL;
			order.business_price = PRICE;
			order.business_time = TIME;
			order.entrust_no = ORDERID;
			order.serial_no = DEALEDID;
			order.business_balance = PRICE* AMOUNT;


			porders->push_back(order);
		}

		return true;
	}
	else
	{
		//	tstring error = UStrConvUtils::Format(_T("读取顶点业务数据库表[ENTRUSTORDER]时，出错了"));
		//	throw TException(error);
		return false;
	}
}



bool CTradeLogDB::InsertOrUpdatePositionInfo(const tradeadapter::PositionInfo& info, const char* clientid, const char* exprovider)
{
	const char* code = TS2GB(info.code).c_str();
	const char* name = TS2GB(info.stock_name).c_str();

	std::string sqlcmd = std::string("SELECT COUNT(*)  FROM [POSITION] WHERE CLIENTID = '") + clientid + "'" + " AND [EXPROVIDER] = '" + exprovider + "'"

		+ " AND [CONTRACECODE] = '" + code + "'";






	std::string insertsqlcmd =
		UStrConvUtils::Format("INSERT INTO [POSITION]([EXPROVIDER], [CLIENTID], [CONTRACECODE], [CONTRACTNAME], [SIDE], [AMOUNT], [AVLPRICE]) VALUES ('%s', '%s', '%s', '%s', %d, %d,%f)",
		exprovider, clientid, code, name, info.posside, info.total_amount, info.cost_price);

	return true;
	//std::string updatesqlcmd = 
}

bool CTradeLogDB::QueryPostions(std::list<tradeadapter::PositionInfo>* pPoss, const std::string& clientid, const char* exprovider)
{
	std::string sqlcmd = std::string("SELECT [CLIENTID],[CONTRACECODE], [CONTRACTNAME], [SIDE], [AMOUNT], [AVLPRICE]  FROM [POSITION] WHERE CLIENTID = '") + clientid + "'" + " AND [EXPROVIDER] = '" + exprovider + "'";




	otl_stream result;
	bool ret = COTLDBHolder::Instance().Select(sqlcmd, &result);
	if (ret)
	{
		while (!result.eof())
		{

			char CLIENTID[16] = { 0 };
			char CONTRACECODE[16] = { 0 };
			char CONTRACTNAME[16] = { 0 };
			char SIDE;//"               CHAR(255) DEFAULT NULL,
			int	 AMOUNT = 0;//"             INT DEFAULT NULL,
			double AVLPRICE = 0.0;


			result >> CLIENTID;
			result >> CONTRACECODE;
			result >> CONTRACTNAME;
			result >> SIDE;
			result >> AMOUNT;
			result >> AVLPRICE;

			tradeadapter::PositionInfo info;
			info.code = U82TS(CONTRACECODE);
		
			info.stock_name = U82TS(CONTRACTNAME);
			info.current_amount = AMOUNT;
			info.cost_price = AVLPRICE;
			info.enable_amount = AMOUNT;
			info.market_value = info.cost_price * info.current_amount;
			info.total_amount = info.enable_amount = info.current_amount;

			pPoss->push_back(info);


		}

		return true;

	}
	return false;
}

bool CTradeLogDB::UpdateAccountInfo(const tradeadapter::BasicAccountInfo& info)
{
	return false;
}
