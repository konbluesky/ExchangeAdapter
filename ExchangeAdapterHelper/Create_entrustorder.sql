----

CREATE TABLE "entrustorder" (
        "Date"               INT DEFAULT NULL,
        "Time"               INT DEFAULT NULL,
        "ClientID"           CHAR(64) DEFAULT NULL,
        "GudongHao"          CHAR(10) DEFAULT NULL,
        "ContractCode"       CHAR(12) DEFAULT NULL,
        "BuySell"            CHAR(255) DEFAULT NULL,
        "Price"              DOUBLE DEFAULT NULL,
        "Amount"             INT DEFAULT NULL,
        "Comment"            CHAR(128) DEFAULT NULL,
        "OrderID"            INT DEFAULT NULL
)
GO


