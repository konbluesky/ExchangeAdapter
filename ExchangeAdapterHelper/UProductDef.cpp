#include "StdAfx.h"
#include "ExchangeAdapterDef.h"
#include "UProductDef.h"
#include "tchar.h"
#include "texceptionex.h"
#pragma warning(disable : 4482)
USING_LIBSPACE



EProductType UProductDef::ParseSHCode( const TCHAR* code )
{
	if(code == NULL || _tcslen(code) == 0 )
	{
		throw TException(_T("错误的代码长度"));
	}
	if( _tcslen(code) !=6)
	{
		return EProductType::E_PUNKOWN;
	}

	//600xxx
	if(code[0] == _T('6') && code[1] == _T('0') && code[2] == _T('0'))
	{
		return EProductType::ESH_A股;			
	}else	

	//国债现货,		//001xxx
	if(code[0] == _T('0') && code[1] == _T('0') && code[2] == _T('1'))
	{
		return EProductType::ESH_国债现货;
	}else

	//企业现货,		//110xxx, 120xxx
	if(code[0] == _T('1') && ( code[1] ==_T('1') || code[1] ==_T('2') ) && code[2] == _T('0'))
	{
		return EProductType::ESH_企业现货;
	}else


	//可转换债券,		//129xxx,100xxx
	if(code[0] == _T('1') &&  code[1] == _T('2') && code[2] == _T('9') )
	{
		return EProductType::ESH_可转换债券; 
	}else



	//可转换债券,		//129xxx,100xxx
	if(code[0] == _T('1') &&  code[1] == _T('0') && code[2] == _T('0') )
	{
		return EProductType::ESH_可转换债券; 
	}else

	if(code[0] == _T('2') && code[1] == _T('0') && code[2] == _T('1'))
	{
		return  EProductType::ESH_国债回购; 
	}else


	if(code[0] == _T('3') && code[1] == _T('1') && code[2] == _T('0'))
	{
		return  EProductType::ESH_国债期货; 
	}else



	if(code[0] == _T('5') && code[1] == _T('0') && code[2] == _T('0'))
	{
		return  EProductType::ESH_基金; 
	}else


	if(code[0] == _T('5') && code[1] == _T('1') && code[2] == _T('0'))
	{
		return  EProductType::ESH_基金; 
	}else

	if(code[0] == _T('5') && code[1] == _T('5') && code[2] == _T('0'))
	{
		return  EProductType::ESH_基金; 
	}else

	if(code[0] == _T('7') && code[1] == _T('0') && code[2] == _T('0'))
	{
		return  EProductType::ESH_配股; 
	}else

	if(code[0] == _T('7') && code[1] == _T('1') && code[2] == _T('0'))
	{
		return  EProductType::ESH_转配股; 
	}else


	if(code[0] == _T('7') && code[1] == _T('0') && code[2] == _T('1'))
	{
		return  EProductType::ESH_转配股再配股;
	}else


	if(code[0] == _T('7') && code[1] == _T('1') && code[2] == _T('1'))
	{
		return  EProductType::ESH_转配股再转配股;	//711xxx
	}else


	if(code[0] == _T('7') && code[1] == _T('2') && code[2] == _T('0'))
	{
		return  EProductType::ESH_红利;			//720xxx
	}else


	if(code[0] == _T('7') && code[1] == _T('3') && code[2] == _T('0'))
	{
		return  EProductType::ESH_新股申购;		//730xxx
	}else

	if(code[0] == _T('7') && code[1] == _T('3') && code[2] == _T('5'))
	{
			return  EProductType::ESH_新基金申购;		//735xxx
	}else

	if(code[0] == _T('7') && code[1] == _T('3') && code[2] == _T('7'))
	{
		return  EProductType::ESH_新股配售;		//737xxx
	}else

	if(code[0] == _T('9') && code[1] == _T('0') && code[2] == _T('0'))
	{
		return  EProductType::ESH_B股;			//900xxx	 
	}else
		return  EProductType::E_PUNKOWN;			

}

EProductType UProductDef::ParseSZCode( const TCHAR* code )
{
	if(code == NULL || _tcslen(code) == 0 )
	{
		throw TException(_T("错误的代码长度"));
	}


	if(code[0] == _T('0') && code[1] == _T('0'))
	{
		return  EProductType::ESZ_A股证券;			//00
	}else

	if(code[0] == _T('3') &&  _tcslen(code) == 6 && code[1] == _T('0'))
	{
		return  EProductType::ESZ_创业版证券;		//30xxxx
	}else

	if(code[0] == _T('3') && _tcslen(code) == 5)
	{
		return  EProductType::ESZ_A股A2权证;		//3xxxx
	}else

	if(code[0] == _T('7') && _tcslen(code) == 5)
	{
		return  EProductType::ESZ_A股增发;			//7xxxx
	}else
	if(code[0] == _T('8') && _tcslen(code) == 5)
	{
		return  EProductType::ESZ_A股A1权证;		//8xxxx
	}else

	if(code[0] == _T('9') && _tcslen(code) == 5)
	{
		return  EProductType::ESZ_A股转配;			//9xxxx
	}else

	if(code[0] == _T('1') && code[1] == _T('0') && _tcslen(code) == 6)
	{
		return  EProductType::ESZ_国债现货;		//10xxxx
	}else

	if(code[0] == _T('1') &&  _tcslen(code) == 5)
	{
		return  EProductType::ESZ_债券;			//1xxxx
	}else


	if(code[0] == _T('2') &&  _tcslen(code) == 5)
	{
		return  EProductType::ESZ_可转换债券;		//2xxxx
	}else
	if(code[0] == _T('3') &&  _tcslen(code) == 5)
	{
		return  EProductType::ESZ_转配股;			//3xxxx
	}else

	if(code[0] == _T('1') &&  _tcslen(code) == 6 && code[1] == _T('7')) 
	{
		return  EProductType::ESZ_原有投资基金;	//17xxxx
	}else

	if(code[0] == _T('8') &&  _tcslen(code) == 5)
	{
		return  EProductType::	ESZ_证券投资基金;	//8xxxx
	}else
	if(code[0] == _T('2') &&  _tcslen(code) == 6 && code[1] == _T('0'))
	{
		return  EProductType::ESZ_B股证券;			//20xxxx
	}else

	if(code[0] == _T('7') &&  _tcslen(code) == 5)
	{
		return  EProductType::ESZ_B股增发;			//7xxxx
	}else

	if(code[0] == _T('8') &&  _tcslen(code) == 5)
	{
		return  EProductType::		ESZ_B股权证;			//8xxxx
	}else

	if(code[0] == _T('8') &&  _tcslen(code) == 5)
	{
		return  EProductType::		ESZ_创业版权证;		//8xxxx
	}else



	if(code[0] == _T('3') &&  _tcslen(code) == 6 && code[1] == _T('9'))
	{
		return  EProductType::		ESZ_综合与成分指数;	//39xxxx
	}else

	if(code[0] == _T('7') &&  _tcslen(code) == 5)
	{
			return  EProductType::ESZ_创业版增发;		//7xxxx
	}else

		return EProductType::E_PUNKOWN;
}

std::pair<EMarketDef,EProductType> UProductDef::ParseCode( const TCHAR* code )
{
	if(code == NULL || _tcslen(code) == 0)
	{
		throw TException(_T("错误的code"));
	}
	//600xxx
	if(code[0] == _T('6') && code[1] == _T('0') && code[2] == _T('0'))
	{
		return  std::make_pair<EMarketDef,EProductType>(EMarketDef::ESH, EProductType::ESH_A股);			
	}else	

	if(code[0] == _T('0') && code[1] == _T('0'))
	{
		return  std::make_pair<EMarketDef,EProductType>(EMarketDef::ESZ,EProductType::ESZ_A股证券);			//00
	}else

	if(code[0] == _T('3') &&  _tcslen(code) == 6 && code[1] == _T('0'))
	{
		return  std::make_pair<EMarketDef,EProductType>(EMarketDef::ESZ,EProductType::ESZ_创业版证券);		//30xxxx
	}else

	if( (code[0] == _T('i') && code[1]==_T('f'))  || (code[0] == _T('I') && code[1]==_T('F')) )
	{
		return std::make_pair<EMarketDef,EProductType>(EMarketDef::EZJ,EProductType::EZJ_FUTURE);		//guzhiqihuo
	}else

	if( (code[0] == _T('t') && code[1]==_T('f'))  || (code[0] == _T('T') && code[1]==_T('F')) )//国债期货
	{
		return std::make_pair<EMarketDef,EProductType>(EMarketDef::EZJ,EProductType::EZJ_FUTURE);		//guzhiqihuo
	}


	EProductType thetype =EProductType::E_PUNKOWN;
	
	thetype = ParseSHCode(code);
	if(thetype != EProductType::E_PUNKOWN)
	{
#if _MSC_VER > 1600
		return  std::make_pair<EMarketDef,EProductType>(std::move(EMarketDef::ESH),std::move( thetype));			
#else
		return  std::make_pair<EMarketDef, EProductType>(EMarketDef::ESH, thetype);
#endif
	}

	thetype = ParseSZCode(code);
	if(thetype != EProductType::E_PUNKOWN)
	{
#if _MSC_VER > 1600
		return  std::make_pair<EMarketDef,EProductType>(std::move(EMarketDef::ESZ), std::move(thetype));			
#else

		return  std::make_pair<EMarketDef,EProductType>(EMarketDef::ESZ, thetype);			
#endif

	}

	throw TException(_T("未知的产品"));
}


