#ifndef __RMMSOFT_COMMON_H__
#define __RMMSOFT_COMMON_H__

#include "ExchangeAdapterBase.h"

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4275)
#pragma warning(disable:4100)
#pragma warning(disable:4512)
#pragma warning(disable:4125)
#else
#error "仅支持VC,以待其他"
#endif

#include "quickfix/Message.h"
#include "quickfix/Application.h"
#include "quickfix/FileStore.h"
#include "quickfix/SocketInitiator.h"
#include "quickfix/FileLog.h"
#include "quickfix/Session.h"
#include "quickfix/MessageCracker.h"

#ifdef _MSC_VER
#pragma warning(pop)
#else
#error "仅支持VC,以待修改"
#endif


#define RMMSOFT_NS ExchangeAdapter_RMMSoft
#define RMMSOFT_NS_BEGIN namespace RMMSOFT_NS {
#define RMMSOFT_NS_END	}
#define	USING_RMMSOFT_NS using namespace RMMSOFT_NS;

#endif