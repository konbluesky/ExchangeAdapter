#include "ExchangeAdapterConfig.h"
#include "FixInitiator.h"
#include "DebugUtils.h"
#include "LocalCompile.h"

#if defined(WIN32)
#ifdef _MSC_VER
#pragma comment(lib,"Ws2_32.lib")
#else
#error 仅支持VC
#endif
#else
#error 仅支持VC
#endif

RMMSOFT_NS_BEGIN

FixInitiator::FixInitiator():m_bstarted(false),m_binitialized(false)
{
	std::memset(&m_para,0,sizeof(FixInitiatorPara));
}

FixInitiator::~FixInitiator()
{
	delete m_pSessionSettings.get();
	delete m_pMsgStoreFactory.get();
	delete m_pLogFactory.get();
	delete m_pInitiator.get();
}

bool FixInitiator::Init(FixInitiatorPara &para)
{
	DBG_ASSERT(0 < para.fixcfgpath.length());
	if(true == m_binitialized)
		return false;

	m_para = para;
	
	/* 后面必须增加检查 */
	try {
		m_pSessionSettings = boost::shared_ptr<FIX::SessionSettings>(
			new FIX::SessionSettings(m_para.fixcfgpath));
		m_pMsgStoreFactory = boost::shared_ptr<FIX::FileStoreFactory>(
			new FIX::FileStoreFactory(*m_pSessionSettings.get()));
		m_pLogFactory = boost::shared_ptr<FIX::FileLogFactory>(
			new FIX::FileLogFactory(*m_pSessionSettings.get()));
		m_pInitiator = boost::shared_ptr<FIX::SocketInitiator>(
			new FIX::SocketInitiator(*this,*m_pMsgStoreFactory.get(),
			*m_pSessionSettings.get(),*m_pLogFactory.get()));
	}
	catch (std::exception& e) {
		std::printf(e.what());
		return false;
	}
	catch(...) {
	}

	const std::set<FIX::SessionID>& sessions = m_pInitiator->getSessions();
	std::set<FIX::SessionID>::iterator it;
	for(it = sessions.begin(); it != sessions.end(); ++it)
		m_seIDs[it->getTargetCompID()] = *it;

	return (m_binitialized = true);
}

void FixInitiator::Uninit()
{
	DBG_ASSERT(m_binitialized);
	m_seIDs.clear();
}

int32 FixInitiator::Start()
{
	if(false == m_binitialized || true == m_bstarted)
		return 0;

	try {
		m_pInitiator->start();
	} catch (std::exception& e) {
		std::printf(e.what());
		return -1;
	} catch(...) {
	}
	m_bstarted = true;
	return 0;
}

void FixInitiator::Stop()
{	
	if(false == m_bstarted)
		return;
	try {
		m_pInitiator->stop(true);
	}catch(...){
		std::cout<<"CME的关闭出错"<<std::endl;
	}
	m_bstarted = false;
}

void FixInitiator::Request(const tstring &agentid,FIX::Message& msg)
{
	DBG_ASSERT(m_bstarted);
	DBG_ASSERT(m_agents.end() != m_agents.find(agentid));
	FIX::Session::sendToTarget(msg,m_seIDs[agentid]);
}

bool FixInitiator::Register(const tstring &agentid,IFixInitiatorAgent *pagent)
{
	if (m_binitialized || m_bstarted)
		return false;

	if(m_agents.end() != m_agents.find(agentid))
		return false;

	m_agents[agentid] = pagent;
	return true;
}

bool FixInitiator::Unregister(const tstring &agentid)
{
	if (m_binitialized || m_bstarted)
		return false;

	if(m_agents.end() != m_agents.find(agentid))
		m_agents.erase(agentid);
	return true;
}

void FixInitiator::fromApp( const FIX::Message& msg,
	const FIX::SessionID& seID)
{
	const tstring &agentid = seID.getTargetCompID();
	DBG_ASSERT(m_agents.end() != m_agents.find(agentid));
	(m_agents[agentid])->Response(*(const_cast<FIX::Message*>(&msg)));
}

/**
 * 管理消息由该模块负责处理实现
 */
void FixInitiator::fromAdmin( const FIX::Message& msg,
	const FIX::SessionID& seID ) 
{	
	UNREF_VAR(msg);
	UNREF_VAR(seID);
}

void FixInitiator::toApp(FIX::Message &msg,
	const FIX::SessionID &seID) 
{
	PreToAdm(seID,&msg);
}

void FixInitiator::onLogout( const FIX::SessionID& seID)
{
	std::cout<<"登出"<<std::endl;
	const tstring &agentid = seID.getTargetCompID();
	DBG_ASSERT(m_agents.end() != m_agents.find(agentid));
	(m_agents[agentid])->AgenOnLogout();
}

void FixInitiator::onCreate( const FIX::SessionID& /*seID*/){}

void FixInitiator::onLogon( const FIX::SessionID& seID)
{
	std::cout<<"登录"<<std::endl;
	const tstring &agentid = seID.getTargetCompID();
	DBG_ASSERT(m_agents.end() != m_agents.find(agentid));
	(m_agents[agentid])->AgenOnLogin();
}

void FixInitiator::toAdmin( FIX::Message& msg,
	const FIX::SessionID& seID)
{
	PreToAdm(seID,&msg);
}

/**
 * 交易所的具体处理
 */
void RMMSoftFixInitiator::PreToAdm(const FIX::SessionID& /*seID*/,
	FIX::Message* /*pmsg*/)
{	
}

void RMMSoftFixInitiator::PreToApp(const FIX::SessionID& /*seID*/,
	FIX::Message* /*pmsg*/)
{
}

bool RMMSoftFixInitiator::RMMSoftInit(RMMSoftFixInitiatorPara &para)
{
	m_para = para;
	return true;
}

void RMMSoftFixInitiator::RMMSoftUninit()
{
}

RMMSOFT_NS_END