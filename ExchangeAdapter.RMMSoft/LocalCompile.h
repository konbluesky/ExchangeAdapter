#ifndef __RMMSOFT_LOCAL_COMPILE_H__
#define __RMMSOFT_LOCAL_COMPILE_H__

#if _MSC_VER
#pragma warning(disable:4996)
#else
#error "��֧��VC,���޸�"
#endif

#define _XML_GET_VALUE(node,nodename)	\
	node->FirstChild(nodename)->FirstChild()->Value()

#define _STR_TO_LOWER(x)	\
	std::transform(x.begin(),x.end(),x.begin(),std::tolower);

#define XML_GET_STRING(node,nodename,nodevalue)	\
	nodevalue = _XML_GET_VALUE(node,nodename);

#define XML_GET_INT(node,nodename,nodevalue)	\
	std::sscanf(_XML_GET_VALUE(node,nodename),"%d",&nodevalue);

#define XML_GET_UINT(node,nodename,nodevalue)	\
	std::sscanf(_XML_GET_VALUE(node,nodename),"%u",&nodevalue);

#define XML_GET_BOOL(node,nodename,nodevalue)	\
{											\
	tstring str = _XML_GET_VALUE(node,nodename);	\
	_STR_TO_LOWER(str);						\
	nodevalue = "true" == str ? true:false;	\
}

#endif