#include "stdafx.h"
#include "ExchangeAdapterDef.h"
#include "UProductDefHelper.h"
#include "texceptionex.h"
#include "ustringutils.h"
#include "ApexHelper.h"
#include "utimeutils.h"

#include "tradelogdb.h"

USING_LIBSPACE

#pragma warning(disable : 4244)

namespace tradeadapter
{
	namespace apex
	{


		std::map<OrderID, Order> UApexHelper::m_todayOrder;
		int UApexHelper::m_today;
		/*
			JYS	交易所
			SH		上海
			SZ		深圳
			HB		沪B
			SB		深B
			TA		三板A股
			TU		三板B股
			*/
		std::string UApexHelper::GetJYSByContractCode(const tstring& code)
		{
			std::pair<EMarketDef, EProductType> ret = UProductDefHelper::ParseCode(code.c_str());
			if (ret.first == EMarketDef::ESH)
				return "SH";
			else if (ret.first == EMarketDef::ESZ)
				return "SZ";
			else
				throw TException(_T("产品定义 函数未完整"));
		}

		std::string UApexHelper::SelectGDHFromProduct(tstring code, std::string sh, std::string sz)
		{
			std::string market = GetJYSByContractCode(code);
			if (market == "SH")
				return sh;
			else if (market == "SZ")
				return sz;
			else
				throw TException(_T("未完整的定义"));
		}

		long UApexHelper::GetJYLBFromBuySell(EBuySell ope)
		{
#pragma warning(disable : 4482)
			if (ope == EBuySell::eBuy)
				return 1;
			else if (ope == EBuySell::eSell)
				return 2;
			else
				throw TException(_T("未定义"));
		}



		Order UApexHelper::GetTodayOrderInfoByID(const char* clientid, int orderid)
		{
			LoadTodayEntrustOrdersFromDB(clientid);

			std::map<OrderID, Order>::iterator it = m_todayOrder.find(orderid);
			if (it != m_todayOrder.end())
			{
				return it->second;
			}
			else
			{
				tstring error = UStrConvUtils::Format(_T("错误的orderid:%d，使在数据库里无法查找到对应的order信息"), orderid);
				throw TException(error);
			}
		}
		/*
				std::string UApexHelper::GetGdhFromOrderID( int orderid )
				{
				std::map<OrderID, Order>::iterator it =  m_map.find(orderid) ;

				if(it != m_map.end())
				{
				std::string ret = GetJYSFromProductT( it->second.ContractCode );
				return ret;
				}
				else
				{
				tstring error = UStrConvUtils::Format(_T("错误的orderid:%d，使在数据库里无法查找到对应的order信息"),orderid);
				throw TException(error);
				}
				}
				*/

		bool UApexHelper::InsertEntrustOrderInfo(int Date, int Time, const char* clientID, const char* StockAccount, const char* ContractCode, int buysell, double Price, int Amount, const char* Comment, int orderid, int Status)
		{
			bool ret = CTradeLogDBHolder::Instance().InsertEntrustOrderInfo(Date, Time, clientID, StockAccount, ContractCode, buysell, Price, Amount, Comment, orderid, Status, "apex");
			return ret;
		}


		void UApexHelper::SaveOrderInfo(int wth, Order order, std::string jys, std::string gdh)
		{
			const char* clientID = "";
			int Date = UTimeUtils::GetNowDate();
			int Time = UTimeUtils::GetNowTime();
			const char* StockAccount = gdh.c_str();
			std::string ContractCode = TS2GB(order.ContractCode.c_str());
			int buysell = (int)order.OpeDir;
			double Price = order.Price;
			int Amount = order.Amount;
			const char* Comment = "";
			int orderid = wth;
			int Status = 0;

			InsertEntrustOrderInfo(Date, Time, clientID, StockAccount, ContractCode.c_str(), buysell, Price, Amount, Comment, orderid, Status);

		}

		bool UApexHelper::LoadTodayEntrustOrdersFromDB(const char* clientid)
		{
			//注：这个函数在初始化时被调用一次，但如果出现连续运行几天的情况，则.....

			if (m_today == UTimeUtils::GetNowDate())
				return true;
			m_today = UTimeUtils::GetNowDate();



			std::cout << "注：顶点的柜台服务器的日期必须是正确的，否则业务会出现错误" << std::endl;

			m_todayOrder.clear();



			std::list<tradeadapter::Order> porders;
			CTradeLogDBHolder::Instance().LoadEntrustOrdersFromDB(&porders, m_today, "apex", clientid);

			for (std::list<tradeadapter::Order>::iterator it = porders.begin(); it != porders.end(); ++it)
			{
				m_todayOrder[it->orderid] = *it;
			}

			return true;
		}

		bool UApexHelper::InitializeDB(const char* dsn, const char* usr, const char* pwd)
		{
			bool ret = CTradeLogDBHolder::Instance().InitializeDB(dsn, usr, pwd);
			return ret;
		}

		tradeadapter::EBuySell UApexHelper::ToBuySell(int JYLB)
		{
			if (1 == JYLB)
				return EBuySell::eBuy;
			else if (2 == JYLB)
				return EBuySell::eSell;
			else
				throw TException(_T("未定义"));

		}


	}
}