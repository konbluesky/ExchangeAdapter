return  
{
	--需要的应答数据字段
	pre_handle =	  --组织请求包
	{
		--应答数据项目定义，可以只定义需要的数据项目

		['__REP_STR__'] = {
			FID_WTH,
		},

		['funcid'] = {def='204502'},	  --请求功能号
		[FID_KHH]  = {def='',field = 'khh'},
		[FID_WTH]  = {def ='',field='xh'},

		[FID_GDH]  = {def='',field='fid_gdh'},
		[FID_JYS]  = {def='',field='fid_jys'},
 		[FID_WTFS] = '32',

		[FID_JYMM]   = {field='khh', change=GetPassword};	--自动读取密码,GetPassword是全局提供的
		[FID_JMLX]	 = '2';	--0，简单加密 2-明文  1-不支持


	},
		
	
	--传给服务器会原样回传的信息
	-- 里面的值可以在生成新文件名的时候替换
	session_handle =
	{
		['khh'] = {def='123', field='khh'};		
		['reqname'] = '[委托撤单]';	--会显示在窗口上
	},
	
	--更新请求数据行
	-- key是需要更新的字段名称
	-- 配置的范式和请求相同，def是默认值， field是返回的字段名（从中取值),change是转换函数function(v)
	post_handle = 
	{
		['xh']={def='', field=FID_WTH};
	},
	
	--多行数据写盘
	table_handle = 
	{
		--写入的文件名,其中格式如${name},是占位符，会用session中的name的值替换
		--去掉占位符的是模板文件名称
		filename = 'wt${khh}.dbf';
		
		--每个字段的处理
		handle = 
		{
		--	['cbjg'] = {def='', field=FID_ZHYE};
		--	['zqdm'] = '600600';
		}

	}






};
