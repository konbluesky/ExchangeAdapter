return  
{
	--需要的应答数据字段
	pre_handle = 
	{
		['__REP_STR__'] = 
		{
			FID_KHH,
			FID_BGHF,
			FID_KQZJ,
			FID_KYZJ
		};
		
		['funcid'] = '303002';
		[FID_KHH]  = {def='300007011551', field="2khh"};
		[FID_BGHF] = {def='', field='xh', change=function(v)
			return 'XX' .. (v or '');
		end};
		[FID_BZ]     = 'RMB';		--币种
		[FID_EXFLG]  = '1';			--查询所有数据

		[FID_JYMM]   = {field='remarkn', change=GetPassword};	--自动读取密码,GetPassword是全局提供的
		[FID_JMLX]	 = '2';	--0，简单加密 2-明文  1-不支持

		['reqname']  = '资金查询';
	},
	
	--传给服务器会原样回传的信息
	-- 里面的值可以在生成新文件名的时候替换
	session_handle =
	{
		['khh'] = {def='123', field='khh'};		
		['reqname'] = '[测试请求]';	--会显示在窗口上
	},
	
	--更新请求数据行
	-- key是需要更新的字段名称
	-- 配置的范式和请求相同，def是默认值， field是返回的字段名（从中取值),change是转换函数function(v)
	post_handle = 
	{
		['zqdm']={def='', field=FID_ZHYE};
		['cbjg']={def='', field='khh'};
	},
	
	--多行数据写盘
	table_handle = 
	{
		--写入的文件名,其中格式如${name},是占位符，会用session中的name的值替换
		--去掉占位符的是模板文件名称
		filename = 'gp${khh}.dbf';
		
		--每个字段的处理
		handle = 
		{
			['cbjg'] = {def='', field=FID_ZHYE};
			['zqdm'] = '000002';
		}
	}
};
