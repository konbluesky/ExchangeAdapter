return  
{
	--需要的应答数据字段
	pre_handle = 
	{
		['__REP_STR__'] = 
		{
				FID_GDH      ,--股东号
				FID_JYS      ,--交易所
				FID_JSZH     ,--结算帐号
				FID_BZ       ,--币种
				FID_GDXM     ,--股东姓名
				FID_ZZHBZ    ,--主帐户标志
		};
		

		['funcid'] = {def='304001'},	  --请求功能号
		
		[FID_KHH]  = {def='', field='khh'};
	},
	
	--传给服务器会原样回传的信息
	-- 里面的值可以在生成新文件名的时候替换
	session_handle =
	{
		['khh'] = {def='', field='khh'};		
		['reqname'] = '[查询账户]';	--会显示在窗口上
	},
	
	--更新请求数据行
	-- key是需要更新的字段名称
	-- 配置的范式和请求相同，def是默认值， field是返回的字段名（从中取值),change是转换函数function(v)
	post_handle = 
	{
--		['zqdm']={def='', field=FID_BZ};
	},
	
	--多行数据写盘
	table_handle = 
	{
		--写入的文件名,其中格式如${name},是占位符，会用session中的name的值替换
		--去掉占位符的是模板文件名称
		filename = 'zh${khh}.dbf';
		
		--每个字段的处理
		handle = 
		{
			['FID_GDH']		= {def='', field=FID_GDH    };---
			['FID_JYS']		= {def='', field=FID_JYS    };
	--		['FID_ZZHBZ']		= {def='', field=FID_ZZHBZ    };
			['FID_BZ']		= {def='', field=FID_BZ};
		}
	}






};
