return  
{
	--需要的应答数据字段
	pre_handle =	  --组织请求包
	{
		--应答数据项目定义，可以只定义需要的数据项目
		['__REP_STR__'] = {
			FID_ZJZH   ,--资金帐号
			FID_BZ     ,--币种
			FID_ZHYE   ,--帐户余额
			FID_DJJE   ,--冻结金额
			FID_KYZJ   ,--可用资金
			FID_KQZJ   ,--可取资金
			FID_QTZC   ,--其它资产
			FID_ZZC    ,--总资产
			FID_XJZC   ,--现金资产
			FID_XJYE   ,--现金余额
			FID_QSJE,	-- 清算金额	
			FID_ZZHBZ,
			FID_ZHLB,
			FID_ZHZT,
			FID_OFSS_JZ,



		},

		['funcid']  = {def='303002'},	  --请求功能号
		[FID_KHH]   = {def='', field="khh"};
		[FID_BZ]    = 'RMB',
	 	[FID_EXFLG] = '1';
	},
		
	
	--传给服务器会原样回传的信息
	-- 里面的值可以在生成新文件名的时候替换
	session_handle =
	{
		['khh'] = {def='123', field='khh'};		
		['reqname'] = '[查询资金]';	--会显示在窗口上
	},
	
	--更新请求数据行
	-- key是需要更新的字段名称
	-- 配置的范式和请求相同，def是默认值， field是返回的字段名（从中取值),change是转换函数function(v)
	post_handle = 
	{
--		['zqdm']={def='', field=FID_ZQDM};
	},
	
	--多行数据写盘
	table_handle = 
	{
		--写入的文件名,其中格式如${name},是占位符，会用session中的name的值替换
		--去掉占位符的是模板文件名称
		filename = 'zj${khh}.dbf';
		
		--每个字段的处理
		handle = 
		{
			['fid_zjzh']    = {def='0', field = FID_ZJZH},		--资金账户
			['fid_bz']    = {def='0', field = FID_BZ},		--货币代码
			['fid_zhye']    = {def='0', field = FID_ZHYE},		--资金余额
			['fid_kyzj']    = {def='0', field = FID_KYZJ},		--可用金额
			['fid_djje']    = {def='0', field = FID_DJJE},		--冻结金额
			['fid_xjzc'] = {def='0', field = FID_XJZC},		--实际金额
			['fid_kqzj']    = {def='0', field = FID_KQZJ},		--可取金额 柜台没有返回暂时取可用资金字段
			['fid_zzc']  = {def='0', field = FID_ZZC},		--总资产
			['fid_zzhbz']  = {def='0', field = FID_ZZHBZ},	
			['fid_zhlb']  = {def='0', field = FID_ZHLB},	
			['fid_zhzt']  = {def='0', field = FID_ZHZT},	
			['fid_ofssjz']  = {def='0', field = FID_OFSS_JZ},	
			['fid_qtzc']  = {def='0', field = FID_QTZC},	

		}
	}






};
