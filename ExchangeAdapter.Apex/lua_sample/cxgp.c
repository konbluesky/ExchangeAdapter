return  
{
	--需要的应答数据字段
	pre_handle = 
	{
		['__REP_STR__'] = 
		{
			FID_GDH			,
			FID_JYS			,
			FID_ZQDM		,
			FID_ZQMC		,
			FID_BZ			,
			FID_ZQSL		,
			FID_DRMCWTSL		,
			FID_DRMRCJSL		,
			FID_DRMCCJSL		,
			FID_KSHSL		,
			FID_KSGSL		,
			FID_KMCSL		,
			FID_DJSL		,
			FID_FLTSL		,
			FID_JCCL		,
			FID_WJSSL		,
			FID_KCRQ		,
			FID_ZXSZ		,
			FID_JYDW		,
			FID_ZXJ			,
			FID_LXBJ		,
			FID_MRJJ		,
			FID_CCJJ		,
			FID_BBJ			,
			FID_FDYK		,
			FID_LJYK		,
			FID_TBCBJ		,
			FID_TBBBJ		,
			FID_TBFDYK		,

		};
		

		['funcid'] = {def='304101'},	  --请求功能号


		[FID_KHH]  = {def='300007011551', field="khh"};
		[FID_EXFLG]  = '1';			--查询所有数据
		[FID_GDH]    = '';

	},
	
	--传给服务器会原样回传的信息
	-- 里面的值可以在生成新文件名的时候替换
	session_handle =
	{
		['khh'] = {def='123', field='khh'};		
		['reqname'] = '[查询持仓]';	--会显示在窗口上
	},
	
	--更新请求数据行
	-- key是需要更新的字段名称
	-- 配置的范式和请求相同，def是默认值， field是返回的字段名（从中取值),change是转换函数function(v)
	post_handle = 
	{
	--	['zqdm']={def='', field=FID_ZQDM};
	},
	
	--多行数据写盘
	table_handle = 
	{
		--写入的文件名,其中格式如${name},是占位符，会用session中的name的值替换
		--去掉占位符的是模板文件名称
		filename = 'gp${khh}.dbf';
		
		--每个字段的处理
		handle = 
		{
			['FID_GDH']= {def='', field=FID_GDH};
			['FID_JYS']		= {def='', field=FID_JYS};
			['FID_ZQDM']		= {def='', field=FID_ZQDM};
			['FID_ZQMC']		= {def='', field=FID_ZQMC};
			['FID_BZ']		= {def='', field=FID_BZ};
			['FID_ZQSL']		= {def='', field=FID_ZQSL};
			['FID_DRMCWTSL']		= {def='', field=FID_DRMCWTSL};
			['FID_DRMRCJSL']		= {def='', field=FID_DRMRCJSL};
			['FID_DRMCCJSL']		= {def='', field=FID_DRMCCJSL};
			['FID_KSHSL']		= {def='', field=FID_KSHSL};
			['FID_KSGSL']		= {def='', field=FID_KSGSL};
			['FID_KMCSL']		= {def='', field=FID_KMCSL};
			['FID_DJSL']		= {def='', field=FID_DJSL};
			['FID_FLTSL']		= {def='', field=FID_FLTSL};
			['FID_JCCL']		= {def='', field=FID_JCCL};
			['FID_WJSSL']		= {def='', field=FID_WJSSL};
			['FID_KCRQ']		= {def='', field=FID_KCRQ};
			['FID_ZXSZ']		= {def='', field=FID_ZXSZ};
			['FID_JYDW']		= {def='', field=FID_JYDW};
			['FID_ZXJ']		= {def='', field=FID_ZXJ};
			['FID_LXBJ']		= {def='', field=FID_LXBJ};
			['FID_MRJJ']		= {def='', field=FID_MRJJ};
			['FID_CCJJ']		= {def='', field=FID_CCJJ};
			['FID_BBJ']		= {def='', field=FID_BBJ};
			['FID_FDYK']		= {def='', field=FID_FDYK};
			['FID_LJYK']		= {def='', field=FID_LJYK};
			['FID_TBCBJ']		= {def='', field=FID_TBCBJ};
			['FID_TBBBJ']		= {def='', field=FID_TBBBJ};
			['FID_TBFDYK']		= {def='', field=FID_TBFDYK};

			['khh']			= {def='', field='khh'};		
		}
	}






};
