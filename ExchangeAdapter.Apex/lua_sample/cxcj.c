return  
{
	--需要的应答数据字段
	pre_handle = 
	{
		['__REP_STR__'] = 
		{
			FID_JYS   ,
			FID_ZQDM,
			FID_BZ,
			FID_WTLB,
			FID_CJSL,
			FID_CJJE,
			FID_QSJE,
		};
		

		['funcid'] 	= {def='304111'},	  --请求功能号
		[FID_KHH]  = {def='', field="khh"};
--		[FID_BZ]     = 'RMB';		--币种
		[FID_EXFLG]  = '1';			--查询所有数据

--		[FID_JYMM]   = {field='remarkn', change=GetPassword};	--自动读取密码,GetPassword是全局提供的
--		[FID_JMLX]	 = '2';	--0，简单加密 2-明文  1-不支持
	},
	
	--传给服务器会原样回传的信息
	-- 里面的值可以在生成新文件名的时候替换
	session_handle =
	{
		['khh'] = {def='123', field='khh'};		
		['reqname'] = '[查询成交]';	--会显示在窗口上
	},
	
	--更新请求数据行
	-- key是需要更新的字段名称
	-- 配置的范式和请求相同，def是默认值， field是返回的字段名（从中取值),change是转换函数function(v)
	post_handle = 
	{
--		['zqdm']={def='', field=FID_ZHYE};
	},
	
	--多行数据写盘
	table_handle = 
	{
		--写入的文件名,其中格式如${name},是占位符，会用session中的name的值替换
		--去掉占位符的是模板文件名称
		filename = 'cj${khh}.dbf';
		
		--每个字段的处理
		handle = 
		{
			['fid_jys']    = {def='0', field = FID_JYS},		--交易所
			['fid_bz']    = {def='0', field = FID_BZ},		--币种
			['fid_zqdm']    = {def='0', field = FID_ZQDM},		--证券代码
			['fid_wtlb']    = {def='0', field = FID_WTLB},		--委托类别
			['fid_cjsl']    = {def='0', field = FID_CJSL},		--成交数量
			['fid_cjje'] = {def='0', field = FID_CJJE},		--成交金额
			['fid_qsje']    = {def='0', field = FID_QSJE},		--清算金额
		}
	}
};
