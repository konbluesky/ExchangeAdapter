return  
{
	--需要的应答数据字段
	pre_handle = 
	{
		['__REP_STR__'] = 
		{
				FID_WTH      ,--委托合同号
				FID_JYS      ,--交易所
				FID_GDH      ,--股东号
				FID_ZQDM     ,--证券代码
				FID_WTLB     ,--委托类别
				FID_CJSL     ,--成交数量
				FID_CJJG     ,--成交价格
				FID_CJJE     ,--成交金额
				FID_CXBZ     ,--撤销标志
				FID_CJBH     ,--成交编号
				FID_CJSJ     ,--成交时间
				FID_ZQMC     ,--证券名称
		};
		

		['funcid'] 	= {def='304110'},	  --请求功能号
		[FID_KHH]  = {def='', field="khh"};
--		[FID_BZ]     = 'RMB';		--币种
		[FID_EXFLG]  = '1';			--查询所有数据

--		[FID_JYMM]   = {field='remarkn', change=GetPassword};	--自动读取密码,GetPassword是全局提供的
--		[FID_JMLX]	 = '2';	--0，简单加密 2-明文  1-不支持
	},
	
	--传给服务器会原样回传的信息
	-- 里面的值可以在生成新文件名的时候替换
	session_handle =
	{
		['khh'] = {def='123', field='khh'};		
		['reqname'] = '[查询分笔成交]';	--会显示在窗口上
	},
	
	--更新请求数据行
	-- key是需要更新的字段名称
	-- 配置的范式和请求相同，def是默认值， field是返回的字段名（从中取值),change是转换函数function(v)
	post_handle = 
	{
--		['zqdm']={def='', field=FID_ZHYE};
	},
	
	--多行数据写盘
	table_handle = 
	{
		--写入的文件名,其中格式如${name},是占位符，会用session中的name的值替换
		--去掉占位符的是模板文件名称
		filename = 'fbcj${khh}.dbf';
		
		--每个字段的处理
		handle = 
		{
			['FID_WTH']    = {def='0', field = FID_WTH },		--委托合同号  
			['FID_JYS']    = {def='0', field = FID_JYS },		--交易所      
			['FID_GDH']    = {def='0', field = FID_GDH },		--股东号      
			['FID_ZQDM']    = {def='0', field = FID_ZQDM},		--证券代码    
			['FID_WTLB']    = {def='0', field = FID_WTLB},		--委托类别    
			['FID_CJSL']    = {def='0', field = FID_CJSL},		--成交数量    
			['FID_CJJG']    = {def='0', field = FID_CJJG},		--成交价格    
			['FID_CJJE']    = {def='0', field = FID_CJJE},		--成交金额    
			['FID_CXBZ']    = {def='0', field = FID_CXBZ},		--撤销标志    
			['FID_CJBH']    = {def='0', field = FID_CJBH},		--成交编号    
			['FID_CJSJ']    = {def='0', field = FID_CJSJ},		--成交时间    
			['FID_ZQMC']    = {def='0', field = FID_ZQMC},		--证券名称    
		}
	}
};
