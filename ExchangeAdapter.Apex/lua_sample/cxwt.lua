return  
{
	--需要的应答数据字段
	pre_handle = 
	{
		['__REP_STR__'] = 
		{
			FID_JYS	 ,	
			FID_GDH	,
			FID_WTH	 ,
			FID_WTLB ,
			FID_CXBZ ,
			FID_ZQDM ,
			FID_ZQMC ,
			FID_DDLX ,
			FID_WTSL ,
			FID_WTJG ,
			FID_WTSJ ,
			FID_SBSJ ,
			FID_SBJG ,
			FID_JGSM ,
			FID_CDSL ,
			FID_CJSL ,
			FID_CJJE ,
			FID_CJJG ,
			FID_BZ	 ,
			FID_WTPCH,

		};
		

		['funcid'] 	= {def='304103'},	  --请求功能号
		[FID_KHH]  = {def='', field="khh"};
		[FID_EN_WTH]     = '';		
		[FID_EXFLG]  = '1';			--查询所有数据

	},
	
	--传给服务器会原样回传的信息
	-- 里面的值可以在生成新文件名的时候替换
	session_handle =
	{
		['khh'] = {def='123', field='khh'};		
		['reqname'] = '[查询委托]';	--会显示在窗口上
	},
	
	--更新请求数据行
	-- key是需要更新的字段名称
	-- 配置的范式和请求相同，def是默认值， field是返回的字段名（从中取值),change是转换函数function(v)
	post_handle = 
	{
--		['zqdm']={def='', field=FID_ZHYE};
	},
	
	--多行数据写盘
	table_handle = 
	{
		--写入的文件名,其中格式如${name},是占位符，会用session中的name的值替换
		--去掉占位符的是模板文件名称
		filename = 'wt${khh}.dbf';
		
		--每个字段的处理
		handle = 
		{
			['FID_JYS']    = {def='0',  field =FID_JYS	},	
			['FID_GDH']    = {def='0',  field =FID_GDH	},	
			['FID_WTH']    = {def='0',  field =FID_WTH	},	
			['FID_WTLB']    = {def='0', field =FID_WTLB},		
			['FID_CXBZ']    = {def='0', field =FID_CXBZ},		
			['FID_ZQDM']    = {def='0', field =FID_ZQDM},		
			['FID_ZQMC']    = {def='0', field =FID_ZQMC},		
			['FID_DDLX']    = {def='0', field =FID_DDLX},		
			['FID_WTSL']    = {def='0', field =FID_WTSL},		
			['FID_WTJG']    = {def='0', field =FID_WTJG},		
			['FID_WTSJ']    = {def='0', field =FID_WTSJ},		
			['FID_SBSJ']    = {def='0', field =FID_SBSJ},		
			['FID_SBJG']    = {def='0', field =FID_SBJG},		
			['FID_JGSM']    = {def='0', field =FID_JGSM},		
			['FID_CDSL']    = {def='0', field =FID_CDSL},		
			['FID_CJSL']    = {def='0', field =FID_CJSL},		
			['FID_CJJE']    = {def='0', field =FID_CJJE},		
			['FID_CJJG']    = {def='0', field =FID_CJJG},		
			['FID_BZ']      = {def='0', field =FID_BZ	},	
			['FID_WTPCh']   = {def='0', field =FID_WTPCH},		
		}
	}
};
