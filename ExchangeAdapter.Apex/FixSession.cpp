#include "stdafx.h"
#include "ExchangeAdapterDef.h"
#include "ustringutils.h"
#include "IOrderService.h"
#include "FixSession.h"
#include "ApexTradeAdapter.h"

tradeadapter::apex::CFixSession::CFixSession(HANDLE_CONN m_Conn)
{
	m_ses = Fix_AllocateSession(m_Conn);
}

void tradeadapter::apex::CFixSession::BeginJob(const ApexInitParamA& info, FunctionID id)
{
	/*
	pszUser     -- [in] 系统要求的柜员代码(八位的字符串)。
	pszWTFS     -- [in] 委托方式。
	pszFBDM     -- [in] 发生营业部的代码信息(四位的字符串)
	pszDestFBDM -- [in] 目标营业部的代码信息(四位的字符串)
	*/
	Fix_SetDefaultInfo(info.gydm.c_str(), info.wtfs.c_str(), info.fbdm.c_str(), info.mbyyb.c_str());
	Fix_CreateHead(m_ses, id);
}

void tradeadapter::apex::CFixSession::AddField(long id, long val)
{
	Fix_SetItem(m_ses, id, val);
}

void tradeadapter::apex::CFixSession::AddField(long id, const char* val)
{
	Fix_SetItem(m_ses, id, val);
}

void tradeadapter::apex::CFixSession::AddField(long id, double val)
{
	Fix_SetItem(m_ses, id, val);
}

bool tradeadapter::apex::CFixSession::CommitJob()
{
	m_ret = Fix_Run(m_ses);
	return m_ret == TRUE;
}

long tradeadapter::apex::CFixSession::GetResultRowNum()
{
	int num = Fix_GetCount(m_ses);
	return num;
}

long tradeadapter::apex::CFixSession::GetResultLong(long id, int rowid/*=-1*/)
{
	long ret = Fix_GetLong(m_ses, id, rowid);
	return ret;
}

double tradeadapter::apex::CFixSession::GetResultDouble(long id, int rowid/*=-1*/)
{
	double ret = Fix_GetDouble(m_ses, id, rowid);
	return ret;
}

std::string tradeadapter::apex::CFixSession::GetResultString(long id, int rowid/*=-1*/)
{
	char out[512] = { 0 };
	Fix_GetItem(m_ses, id, out, sizeof(out), rowid);
	return out;
}

ErrorInfo tradeadapter::apex::CFixSession::GetRunError()
{
	char out[512] = { 0 };
	Fix_GetErrMsg(m_ses, out, sizeof(out));
	ErrorInfo ret;
	ret.SetErrInfo(EErrSystem::ETradeAdapter, -1, out);
	return ret;
}

tradeadapter::apex::CFixSession::~CFixSession()
{
	Fix_ReleaseSession(m_ses);
}
