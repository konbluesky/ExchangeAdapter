#pragma once
#include "stdafx.h"
#include <string>
#include "ITradeAdapter.h"

namespace tradeadapter
{
	namespace apex
	{



		struct ApexInitParam
		{
			tstring AdapterName; //可选
			tstring AdapterVer; //可选
			bool AutoReconnect;
			tstring Addr;
			int			Port;
			tstring ApexUser;
			tstring ApexPwd;
			int Timeout;   // 设6*1000

			tstring ClientID;
			tstring Password;


			tstring YYB;


			tstring gydm;
			tstring wtfs;

			tstring fbdm;
			tstring mbyyb;


			tstring dsn;
			tstring dsnusr;
			tstring dsnpwd;

			ApexInitParam()
			{
				AdapterName = _T("TigEra");
				AdapterVer = _T("1.01");
				AutoReconnect = true;
			}
		};

		struct ApexInitParamA
		{
			std::string AdapterName; //可选
			std::string AdapterVer; //可选
			bool AutoReconnect;
			std::string Addr;
			int			Port;
			std::string ApexUser;
			std::string ApexPwd;
			int Timeout;   // 设6*1000

			std::string YYB;
			std::string ClientID;
			std::string Password;

			std::string Gdh_SH; //不需要填 初始化过程会查
			std::string Gdh_SZ; //不需要填，初始化过程会查




			std::string gydm;
			std::string wtfs;

			std::string fbdm;
			std::string mbyyb;

			std::string dsn;
			std::string dsnusr;
			std::string dsnpwd;


			static ApexInitParamA FromApexInitParam(ApexInitParam input);

			ApexInitParamA()
			{
				memset(this, 0, sizeof(ApexInitParamA));
				AdapterName = "TigEra";
				AdapterVer = "1.01";
				AutoReconnect = true;
			}




		};

		class TAIAPI CApexTradeAdapter : ITradeAdapter
		{
		public:
			CApexTradeAdapter(void);
			virtual ~CApexTradeAdapter(void);


			virtual bool Initialize(void* param = NULL);

			virtual void UnInitialize();

			//从测试的代码，默认可以使用空的继续
			bool _connect(const char* addr, int port, const char* usr = "", const char* pwd = "", int timeout = 1000);




			virtual bool IsInitialized();

			virtual bool InitializeByXML(const TCHAR* fpath);

			virtual bool EntrustOrder(Order order, OrderID* pOutID);

			virtual bool CancelOrder(OrderID orderid);



			virtual bool QueryOrder(OrderID id, int* pCount);

			virtual bool QueryDealedOrders(std::list<DealedOrder>* pres);

			virtual bool QueryAccountInfo(BasicAccountInfo* info);

			void _getaccountmoney(BasicAccountInfo* info);

			void _getaccountname(BasicAccountInfo* info);

			virtual bool QueryPostions(std::list<PositionInfo>* pPoss, const TCHAR* pstock = NULL);




			


			void GetBasicUserInfo1();
			void GetBasicUserInfo2();

			struct stockinfo
			{
				double zdjg;
			};
			stockinfo GetStockInfo(const TCHAR* code);

			/////////////////
			//
			//void GetDealedOrders();

			virtual bool QueryEntrustInfos(std::list<EntrustInfo>* pinfo);

			virtual tstring GetName();



		private:

			//
#ifndef HANDLE_CONN
#define HANDLE_CONN     long
#endif
#pragma warning(disable :4251)
			HANDLE_CONN m_Conn;
			ApexInitParamA m_param;
			bool m_Initialzed;

			
		};
	}
}