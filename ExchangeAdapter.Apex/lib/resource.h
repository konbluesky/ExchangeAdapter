//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by TestApi.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_TESTAPI_DIALOG              102
#define IDR_MAINFRAME                   128
#define IDC_RUN                         1000
#define IDC_QUERY                       1002
#define IDC_NEW                         1003
#define IDC_REGSVC                      1004
#define IDC_CLOSE                       1005
#define IDC_INIT                        1007
#define IDC_UNINIT                      1008
#define IDC_EDIT_ADDR                   1009
#define IDC_ADDR                        1010

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1011
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
