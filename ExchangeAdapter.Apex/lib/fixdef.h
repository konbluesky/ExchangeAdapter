/***********************************************************************
*         Copyright (C) 2004  福建顶点软件有限公司 All rights reserved. 
*
*文件名称：fiddef.h
*项目名称：
*摘要：    信息元定义c/c++编程用头文件。    
*
*原作者：  顶点企业级证券集中交易系统ABOSS2
*创建日期：2006.02.23
*备注：
*
*历史修改记录：
*
*序号    日期             版本                作者                         
*1.      2006.02.23       v1.0.0.1            顶点企业级证券集中交易系统ABOSS2
*    创建。
*
**********************************************************************/
 
#if !defined(_FIDDEF_DEFINE_H)
#define _FIDDEF_DEFINE_H
#define  FID_BCZJYE           501  //本次资金余额  DT：C  
#define  FID_BFXJJ            502  //买风险金  DT：C  
#define  FID_BGHF             503  //买过户费  DT：C  
#define  FID_BJE              504  //买入金额  DT：C  
#define  FID_BJSF             505  //买经手费  DT：C  
#define  FID_BPGDH            506  //报盘股东号  DT：C  
#define  FID_CODE             507  //返回码  DT：C  
#define  FID_MESSAGE          508  //返回说明  DT：C  
#define  FID_BSL              509  //买入数量  DT：C  
#define  FID_BYHS             510  //买印花税  DT：C  
#define  FID_BZ               511  //币种  DT：C  
#define  FID_BZBM             512  //币种编码  DT：C  
#define  FID_BZGF             513  //买证管费  DT：C  
#define  FID_BZMC             514  //币种名称  DT：C  
#define  FID_CBBD             515  //成本变动  DT：R  
#define  FID_MENUID_HC        516  //互斥的菜单系统代码  DT：I  
#define  FID_HLJE             517  //红利金额  DT：R  
#define  FID_CDMC             518  //菜单名称  DT：C  
#define  FID_CDTIME           519  //菜单可用时间  DT：C  
#define  FID_CDTS             520  //菜单提示  DT：C  
#define  FID_CDWZID           521  //菜单位置ID  DT：C  
#define  FID_CJBH             522  //成交编号  DT：C  
#define  FID_CJGY             523  //创建柜员  DT：C  
#define  FID_CJJE             524  //成交金额  DT：C  
#define  FID_CJJG             525  //成交价格  DT：C  
#define  FID_CJRQ             526  //成交日期  DT：C  
#define  FID_CJSJ             527  //成交时间  DT：C  
#define  FID_CJSL             528  //成交数量  DT：C  
#define  FID_CKLL             529  //存款利率  DT：C  
#define  FID_YKBD             530  //盈亏变动  DT：R  
#define  FID_CSZ              531  //参数值  DT：C  
#define  FID_DH               532  //电话  DT：C  
#define  FID_DJLB             533  //冻结类别  DT：C  
#define  FID_DH_SQCZR         534  //授权操作人电话  DT：C  
#define  FID_DZ_SQCZR         535  //授权操作人地址  DT：C  
#define  FID_MM_SQCZR         536  //授权操作人密码  DT：C  
#define  FID_QX_SQCZR         537  //授权操作人权限  DT：C  
#define  FID_XWID             538  //席位编号  DT：I  
#define  FID_XM_SQCZR         539  //授权操作人姓名  DT：C  
#define  FID_DRMCCJJE         540  //当日卖出成交金额  DT：C  
#define  FID_DRMCCJSL         541  //当日卖出成交数量  DT：C  
#define  FID_DRMCJDZJ         542  //当日卖出解冻资金  DT：C  
#define  FID_DRMCWTSL         543  //当日卖出委托数量  DT：C  
#define  FID_DRMRCJJE         544  //当日买入成交金额  DT：C  
#define  FID_DRMRCJSL         545  //当日买入成交数量  DT：C  
#define  FID_DRMRDJZJ         546  //当日买入冻结资金  DT：C  
#define  FID_DRMRWTSL         547  //当日买入委托数量  DT：C  
#define  FID_DRQS             548  //当日已成交的清算资金  DT：C  
#define  FID_DTB              549  //跌停板  DT：C  
#define  FID_DZ               550  //地址  DT：C  
#define  FID_DZLX             551  //待转利息  DT：C  
#define  FID_EMAIL            552  //电子邮件  DT：C  
#define  FID_EN_BZ            553  //可操作的币种  DT：C  
#define  FID_EN_JYS           554  //可操作的交易所  DT：C  
#define  FID_EN_KHH           555  //可操作的客户号  DT：C  
#define  FID_EN_KHQZ          556  //允许操作的客户群组  DT：C  
#define  FID_EN_YYB           557  //允许操作的营业部  DT：C  
#define  FID_EN_ZQLB          558  //可操作的证券类别  DT：C  
#define  FID_EX_KHH           559  //不可操作的客户号  DT：C  
#define  FID_EX_KHQZ          560  //禁止操作的客户群组  DT：C  
#define  FID_EX_YYB           561  //禁止操作的营业部  DT：C  
#define  FID_EX_ZQLB          562  //禁止证券类别范围  DT：C  
#define  FID_FATHERID         563  //父菜单  DT：C  
#define  FID_AUTOSAVE         564  //自动保存  DT：I  
#define  FID_FKEY             565  //加速键  DT：C  
#define  FID_FLDM             566  //数据字典分类代码  DT：C  
#define  FID_FLMC             567  //数据字典分类名称  DT：C  
#define  FID_FLTSL            568  //非流通数量  DT：C  
#define  FID_FSYYB            569  //发生营业部  DT：C  
#define  FID_FWXM             570  //服务项目  DT：C  
#define  FID_GDH              571  //股东号  DT：C  
#define  FID_GDLB             572  //股东类别  DT：C  
#define  FID_ZJBH_GDH         573  //股东证件编号  DT：C  
#define  FID_GDZDSX           574  //股东指定属性  DT：I  
#define  FID_GDXM             575  //股东姓名  DT：C  
#define  FID_GJDM             576  //国籍代码  DT：C  
#define  FID_GMRQ             577  //柜员更密日期  DT：C  
#define  FID_DATE             578  //当前日期  DT：C  DT：YYYY.MM.DD  
#define  FID_HKEY             579  //热键  DT：C  
#define  FID_HKEYMASK         580  //组合键  DT：C  
#define  FID_SHZD             581  //自动发送上海指定  DT：I  DT：0 不发送,1 发送  
#define  FID_IBM              582  //数字编码  DT：C  
#define  FID_JJR              583  //经纪人代码  DT：C  
#define  FID_JJRLB            584  //经纪人类别  DT：C  
#define  FID_JJRMM            585  //经纪人密码  DT：C  
#define  FID_JJRQX            586  //经纪人权限  DT：C  
#define  FID_JKP              587  //今开盘  DT：C  
#define  FID_JSDM             588  //角色代码  DT：C  
#define  FID_JSMC             589  //角色名称  DT：C  
#define  FID_JSRQ             590  //结束日期  DT：C  
#define  FID_JYDW             591  //交易单位  DT：C  
#define  FID_JYFL             592  //交易分类  DT：C  
#define  FID_JYFW             593  //交易范围  DT：C  
#define  FID_JYJW             594  //交易价位  DT：C  
#define  FID_JYLB             595  //交易类别编码  DT：C  
#define  FID_JYLBMC           596  //交易类别名称  DT：C  
#define  FID_JYLBXZ           597  //交易类别限制  DT：C  
#define  FID_JYMM             598  //交易密码  DT：C  
#define  FID_JYS              599  //交易所编码  DT：C  
#define  FID_JYSXH            600  //交易所序号  DT：I  
#define  FID_JYSJC            601  //交易所名称  DT：C  
#define  FID_JYSQC            602  //交易所全称  DT：C  
#define  FID_KCRQ             603  //开仓日期  DT：C  
#define  FID_KEYMASK          604  //菜单组合键  DT：C  
#define  FID_KHH              605  //客户号  DT：C  
#define  FID_KHJB             606  //客户级别  DT：C  
#define  FID_KHJL             607  //客户经理  DT：C  
#define  FID_KHLB             608  //客户类别  DT：C  
#define  FID_KHQC             609  //客户全称  DT：C  
#define  FID_KHQZ             610  //客户群组  DT：C  
#define  FID_KHRQ             611  //开户日期  DT：C  
#define  FID_KHSX             612  //客户属性  DT：C  
#define  FID_KHXM             613  //客户姓名  DT：C  
#define  FID_LSH_ZR           614  //转入业务操作流水号  DT：I  
#define  FID_KMCSL            615  //可卖出数量  DT：C  
#define  FID_KMRSL            616  //可买入数量  DT：C  
#define  FID_KQZJ             617  //可取资金  DT：C  
#define  FID_KSRQ             618  //开始日期  DT：C  
#define  FID_KYZJ             619  //可用资金  DT：C  
#define  FID_LL               620  //利率  DT：C  
#define  FID_YJLX             621  //预计利息  DT：R  
#define  FID_LXJS             622  //利息积数  DT：R  
#define  FID_LXS              623  //预计利息税  DT：R  
#define  FID_LXSL             624  //利息税率  DT：R  
#define  FID_MCJE             625  //卖出金额  DT：C  
#define  FID_MCJJ             626  //卖出均价  DT：C  
#define  FID_MCJS             627  //卖出基数  DT：C  
#define  FID_MCSL             628  //卖出数量  DT：C  
#define  FID_MENUID           629  //菜单的系统代码  DT：C  
#define  FID_MKBH             630  //模块编号  DT：C  
#define  FID_MM               631  //密码  DT：C  
#define  FID_MMXZ             632  //买卖限制  DT：C  
#define  FID_YKTZMS           633  //盈亏调整模式  DT：I  DT：0 根据输入调整，1 自动调整  
#define  FID_MRJE             634  //买入金额  DT：C  
#define  FID_JYMQZ            635  //交易码前缀  DT：C  
#define  FID_MRJJ             636  //买入均价  DT：C  
#define  FID_MRJS             637  //买入基数  DT：C  
#define  FID_MRSL             638  //买入数量  DT：C  
#define  FID_NBMC             639  //摘要内部名称  DT：C  
#define  FID_NEWMM            640  //新密码  DT：C  
#define  FID_NODE             641  //操作站点  DT：C  
#define  FID_NOTE             642  //说明  DT：C  
#define  FID_PYDM             643  //拼音代码  DT：C  
#define  FID_PZBZ             644  //是否打印凭证  DT：C  
#define  FID_ZJZH_ZR          645  //转入资金帐号  DT：C  
#define  FID_QSJE             646  //清算金额  DT：C  
#define  FID_QSZJ             647  //清算资金  DT：C  
#define  FID_QYBM             648  //区域编码  DT：C  
#define  FID_QZFW             649  //取值范围  DT：C  
#define  FID_QZMC             650  //群组名称  DT：C  
#define  FID_RQ               651  //日期  DT：C  
#define  FID_RZJE             652  //融资金额  DT：C  
#define  FID_RZLL             653  //融资利率  DT：C  
#define  FID_SFXJJ            654  //卖风险基金  DT：C  
#define  FID_SGHF             655  //卖过户费  DT：C  
#define  FID_SJCD             656  //数据长度  DT：C  
#define  FID_SJE              657  //卖出金额  DT：C  
#define  FID_SJJJR            658  //上级经纪人  DT：C  
#define  FID_SJSF             659  //卖经手费  DT：C  
#define  FID_SJYYB            660  //上级营业部  DT：C  
#define  FID_SLXZ             661  //数量限制  DT：C  
#define  FID_CZCS             662  //冲帐次数  DT：I  
#define  FID_SRZJYE           663  //上日资金余额  DT：C  
#define  FID_SSL              664  //卖出数量  DT：C  
#define  FID_SXLB             665  //属性类别  DT：C  
#define  FID_SYHS             666  //卖印花税  DT：C  
#define  FID_SZGF             667  //卖证管费  DT：C  
#define  FID_T0JS             668  //当日应交收资金  DT：C  
#define  FID_T1JS             669  //T+1清算应交收资金  DT：C  
#define  FID_T2JS             670  //T+2清算应交收资金  DT：C  
#define  FID_TBCBJ            671  //摊薄成本价  DT：C  
#define  FID_TBTS             672  //特别提示  DT：C  
#define  FID_TJFL             673  //统计分类  DT：C  
#define  FID_TZJS             674  //透支积数  DT：C  
#define  FID_TZLL             675  //透支利率  DT：C  
#define  FID_CBTZMS           676  //成本调整模式  DT：I  DT：0 根据输入调整，1 自动调整  
#define  FID_USERID           677  //柜员号  DT：C  
#define  FID_USERNAME         678  //柜员名称  DT：C  
#define  FID_VALUE            679  //属性值  DT：C  
#define  FID_WTFS             680  //委托方式  DT：C  
#define  FID_WTH              681  //委托号  DT：C  
#define  FID_WTJG             682  //委托价格  DT：C  
#define  FID_WTLB             683  //委托类别  DT：C  
#define  FID_WTSL             684  //委托数量  DT：C  
#define  FID_JSDM_HC          685  //权限互斥的角色代码  DT：C  
#define  FID_WTSX             686  //委托上限  DT：C  
#define  FID_WTXX             687  //委托下限  DT：C  
#define  FID_XHRQ             688  //销户日期  DT：C  
#define  FID_XLDM             689  //学历代码  DT：C  
#define  FID_XM               690  //姓名  DT：C  
#define  FID_GDZT             691  //股东状态  DT：I  
#define  FID_YHDM             692  //银行代码  DT：C  
#define  FID_YHZH             693  //银行帐号  DT：C  
#define  FID_YWKM             694  //业务科目  DT：C  
#define  FID_YWLB             695  //业务类别  DT：C  
#define  FID_YWMC             696  //业务名称  DT：C  
#define  FID_YYB              697  //营业部  DT：C  
#define  FID_KHH_ZR           698  //转入客户号  DT：C  
#define  FID_CLSJ             699  //处理时间  DT：C  
#define  FID_KHXM_ZR          700  //转入客户姓名  DT：C  
#define  FID_GYDM_ZP          701  //指派柜员代码  DT：C  
#define  FID_YZBM             702  //邮政编码  DT：C  
#define  FID_ZDBJ             703  //最低报价  DT：C  
#define  FID_ZDJ              704  //最低价  DT：C  
#define  FID_EN_WTH           705  //委托合同号范围  DT：C  
#define  FID_ZGBJ             706  //最高报价  DT：C  
#define  FID_ZGJ              707  //最高价  DT：C  
#define  FID_KHFLFS           708  //客户分类方式  DT：I  
#define  FID_ZHYE             709  //帐户余额  DT：C  
#define  FID_ZHZT             710  //帐户状态  DT：C  
#define  FID_ZJBH             711  //证件编号  DT：C  
#define  FID_YJDJCL           712  //佣金定价策略  DT：I  
#define  FID_ZJLB             713  //证件类别  DT：C  
#define  FID_ZJMM             714  //资金密码  DT：C  
#define  FID_ZJYE             715  //资金余额  DT：C  
#define  FID_ZJZH             716  //资金帐号  DT：C  
#define  FID_GDH_OLD          717  //原股东号  DT：C  
#define  FID_LJS1             718  //累计佣金  DT：R  
#define  FID_ZQDM             719  //证券代码  DT：C  
#define  FID_ZQLB             720  //证券类别  DT：C  
#define  FID_ZQLBMC           721  //证券类别名称  DT：C  
#define  FID_ZQMC             722  //证券名称  DT：C  
#define  FID_ZQQC             723  //证券全称  DT：C  
#define  FID_ZQSL             724  //证券数量  DT：C  
#define  FID_ZSBH             725  //证书编号  DT：C  
#define  FID_ZSP              726  //昨收盘  DT：C  
#define  FID_ZZHBZ            727  //主帐户标志  DT：I  
#define  FID_ZTB              728  //涨停板  DT：C  
#define  FID_ZXJ              729  //最新价  DT：C  
#define  FID_ZY               730  //摘要  DT：C  
#define  FID_ZYDM             731  //职业代码  DT：C  
#define  FID_ZZFS             732  //转帐申请方式  DT：C  
#define  FID_ZZJE             733  //支汇票金额  DT：C  
#define  FID_ZZKZ             734  //转帐控制  DT：C  
#define  FID_MMLB             735  //密码类别  DT：C  
#define  FID_HTHQZ            736  //合同号前缀  DT：C  
#define  FID_NEWQZ            737  //新群组编号  DT：C  
#define  FID_EN_ZJZH          738  //可操作的资金帐号  DT：C  
#define  FID_LOGICAL          739  //逻辑判断操作（是、否）  DT：C  
#define  FID_KHFW             740  //客户范围  DT：C  
#define  FID_SQFS             741  //申请方式  DT：I  
#define  FID_CKCS             742  //存款次数限制  DT：C  
#define  FID_CKZE             743  //存款总额限制  DT：C  
#define  FID_CKDBSX           744  //存款单笔限制  DT：C  
#define  FID_QKCS             745  //取款次数限制  DT：C  
#define  FID_QKZE             746  //取款总额限制  DT：C  
#define  FID_QKDBSX           747  //取款单笔限制  DT：C  
#define  FID_TBBBJ            748  //摊薄保本价  DT：C  
#define  FID_TBFDYK           749  //摊薄浮动盈亏  DT：C  
#define  FID_WTSJ             750  //委托时间  DT：C  
#define  FID_SBSJ             751  //申报时间  DT：C  
#define  FID_MMLBSM           752  //买卖类别说明  DT：C  
#define  FID_SBJG             753  //申报结果  DT：C  
#define  FID_SBJGSM           754  //申报结果说明  DT：C  
#define  FID_CXBZ             755  //撤销标志  DT：C  
#define  FID_DLSF             756  //登录身份  DT：C  
#define  FID_JCCL             757  //今持仓量  DT：C  
#define  FID_WJSSL            758  //未交收数量  DT：C  
#define  FID_CDCZDX           759  //菜单操作对象  DT：I  
#define  FID_ZXSZ             760  //最新市值  DT：C  
#define  FID_FDYK             761  //浮动盈亏  DT：C  
#define  FID_DGDHBZ           762  //是否允许登记多股东号  DT：I  DT：0 允许，1 不允许  
#define  FID_BROWINDEX        763  //起始历史记录索引值  DT：C  
#define  FID_DJZJ             764  //冻结资金  DT：C  
#define  FID_LOGINPWD         765  //用户登录密码  DT：C  
#define  FID_S1               766  //佣金  DT：C  
#define  FID_S2               767  //印花税  DT：C  
#define  FID_S3               768  //过户费  DT：C  
#define  FID_S4               769  //附加费  DT：C  
#define  FID_S5               770  //结算费  DT：C  
#define  FID_S6               771  //交易规费  DT：C  
#define  FID_YSJE             772  //应收金额  DT：C  
#define  FID_LXSR             773  //利息收入  DT：R  
#define  FID_LXFC             774  //利息付出  DT：R  
#define  FID_BZS1             775  //标准佣金  DT：C  
#define  FID_BCZQSL           776  //本次股份余额  DT：C  
#define  FID_BRZQSL           777  //本日股份余额  DT：C  
#define  FID_BRZJYE           778  //本日资金余额  DT：C  
#define  FID_GDJG             779  //转帐勾对结果  
#define  FID_CSID             780  //参数编号ID  DT：I  
#define  FID_JMLX             781  //加密类型  DT：C  
#define  FID_SQLB             782  //申请类别  DT：C  
#define  FID_DLXH             783  //登录序号  DT：INT  
#define  FID_ZPJE             784  //支票金额  DT：C  
#define  FID_GPSZ             785  //股票市值  DT：C  
#define  FID_MMYZFS           786  //密码验证方式  DT：I  
#define  FID_FZYZFS           787  //辅助身份验证方式  DT：I  
#define  FID_COUNT            788  //笔数  DT：C  
#define  FID_GDHH             789  //股东号前缀  DT：C  
#define  FID_GDHL             790  //股东号长度  DT：C  
#define  FID_ZQDML            791  //证券代码长度  DT：C  
#define  FID_ENDWTH           792  //结束委托号  DT：C  
#define  FID_LOGINID          793  //登录ID  DT：C  
#define  FID_ZHXM             794  //帐户姓名  DT：C  
#define  FID_JSZH             795  //结算帐户  DT：C  
#define  FID_FAX              796  //传真FAX  DT：C  
#define  FID_LSH              797  //流水号  DT：C  
#define  FID_HOST             798  //主机地址  DT：C  
#define  FID_TZLX             799  //透支利息  DT：C  
#define  FID_GSFL             800  //公司级客户分类  DT：C  
#define  FID_SRFS             801  //输入方式  DT：C  
#define  FID_XJZC             802  //现金资产  DT：C  
#define  FID_SRYE             803  //上日余额  DT：C  
#define  FID_ZHSX             804  //帐户属性  DT：C  
#define  FID_WJSJE            805  //未交收金额  DT：C  
#define  FID_CQBZ             806  //存取标志  DT：C  
#define  FID_YZZZBZ           807  //银证转帐标志  DT：C  
#define  FID_FSJE             808  //发生金额  DT：C  
#define  FID_NEWKHJL          809  //迁入客户经理  DT：C  
#define  FID_SCZJYE           810  //上次资金余额  DT：C  
#define  FID_SQPCH            811  //申请批次号  DT：I  
#define  FID_DJRQ             812  //登记日期  DT：C  
#define  FID_EN_GSFL          813  //允许操作的客户公司分类  DT：C  
#define  FID_ITEM             814  //配置项  DT：C  
#define  FID_XGRQ             815  //修改日期  DT：C  
#define  FID_CSJB             816  //参数级别  DT：C  
#define  FID_SXMC             817  //属性名称  DT：C  
#define  FID_PZH2             818  //凭证号2  DT：C  
#define  FID_LPID             819  //礼品ID号  DT：C  
#define  FID_LPBM             820  //礼品编码  DT：C  
#define  FID_LPMC             821  //礼品名称  DT：C  
#define  FID_LPJZ             822  //礼品价值  DT：C  
#define  FID_ZSFS             823  //赠送方式  DT：C  
#define  FID_ZCKZED           824  //资产控制额度  DT：C  
#define  FID_ZJKZED           825  //资金控制额度  DT：C  
#define  FID_DJJE             826  //冻结金额  DT：C  
#define  FID_YCDJJE           827  //异常冻结金额  DT：C  
#define  FID_QSDM             828  //券商代码  DT：C  
#define  FID_QSMC             829  //券商名称  DT：C  
#define  FID_JGSM             830  //结果说明  DT：C  
#define  FID_CLRQ             831  //处理日期  DT：I  
#define  FID_JGBM             832  //机构编码  DT：C  
#define  FID_JGMC             833  //机构名称  DT：C  
#define  FID_JGJC             834  //机构简称  DT：C  
#define  FID_CITY             835  //城市  DT：C  
#define  FID_PROVINCE         836  //省份  DT：C  
#define  FID_SJJG             837  //上级机构  DT：C  
#define  FID_SJJGLB           838  //上级机构类别  DT：I  
#define  FID_JGLB             839  //机构类别  DT：I  
#define  FID_EN_SJJGLB        840  //允许上级机构类别范围  DT：C  
#define  FID_EN_JYLB          841  //允许的交易类别  DT：C  
#define  FID_MRJG1            842  //买入价格一  DT：C  
#define  FID_MRSL1            843  //买入数量一  DT：C  
#define  FID_MRJG2            844  //买入价格二  DT：C  
#define  FID_MRSL2            845  //买入数量二  DT：C  
#define  FID_MRJG3            846  //买入价格三  DT：C  
#define  FID_MRSL3            847  //买入数量三  DT：C  
#define  FID_MRJG4            848  //买入价格四  DT：C  
#define  FID_MRSL4            849  //买入数量四  DT：C  
#define  FID_MCJG1            850  //卖出价格一  DT：C  
#define  FID_MCSL1            851  //卖出数量一  DT：C  
#define  FID_MCJG2            852  //卖出价格二  DT：C  
#define  FID_MCSL2            853  //卖出数量二  DT：C  
#define  FID_MCJG3            854  //卖出价格三  DT：C  
#define  FID_MCSL3            855  //卖出数量三  DT：C  
#define  FID_MCJG4            856  //卖出价格四  DT：C  
#define  FID_MCSL4            857  //卖出数量四  DT：C  
#define  FID_DYBL             858  //抵押比例  DT：R  
#define  FID_JJRXM            859  //经纪人姓名  DT：C  
#define  FID_TPBZ             860  //停牌标志  DT：C  
#define  FID_JSLX             861  //结算类型  DT：I  
#define  FID_HBXH             862  //回报序号  DT：C  
#define  FID_SBGDH            863  //三板股东号  DT：C  
#define  FID_WTGY             864  //委托柜员  DT：C  
#define  FID_FSSJ             865  //发生时间  DT：C  
#define  FID_WTZKXS           866  //委托折扣系数  DT：C  
#define  FID_CXZKXS           867  //查询折扣系数  DT：C  
#define  FID_QTZKXS           868  //前台折扣系数  DT：C  
#define  FID_ZQSZ             869  //证券市值  DT：C  
#define  FID_KYZJXE           870  //可用资金限额  DT：R  
#define  FID_XMLSH            871  //项目流水号  DT：I  
#define  FID_ZCZH             872  //转存帐号  DT：C  
#define  FID_ZCBL             873  //转存比例  DT：C  
#define  FID_SFFS             874  //收费方式  DT：C  
#define  FID_SFBZ             875  //收费标准  DT：C  
#define  FID_JFFS             876  //计费方式  DT：C  
#define  FID_JFQD             877  //计费起点  DT：C  
#define  FID_JFDW             878  //计费单位  DT：C  
#define  FID_CZSJ             879  //操作时间  DT：C  
#define  FID_CLJG             880  //处理结果  DT：C  
#define  FID_GYFL             881  //柜员分类  DT：C  
#define  FID_WBGYDM           882  //外部柜员代码  DT：C  
#define  FID_XQXZ             883  //星期限制  DT：C  
#define  FID_ZCXZ             884  //注册限制  DT：C  
#define  FID_SLSX             885  //数量上限  DT：C  
#define  FID_CDSL             886  //撤单数量  DT：C  
#define  FID_YJZKBL           887  //佣金折扣比例  DT：R  
#define  FID_FJFZKBL          888  //附加费折扣比例  DT：R  
#define  FID_BH               889  //编号  DT：C  
#define  FID_SHXYM            890  //审核校验码  DT：C  
#define  FID_ZLLB             891  //指令类别  DT：C  
#define  FID_CDJSP            892  //菜单JSP  DT：C  
#define  FID_EN_NODE          893  //允许操作站点  DT：C  
#define  FID_EN_MYYYB         894  //允许漫游营业部范围  DT：C  
#define  FID_ZJGMRQ           895  //系统用户最近更密日期  DT：I  
#define  FID_JGSX             896  //机构属性  DT：I  
#define  FID_CDSX             897  //参数属性  DT：I  
#define  FID_BM               898  //数据字典编码  DT：I  
#define  FID_BMSM             899  //数据字典编码说明  DT：C  
#define  FID_CDDLL            900  //菜单DLL名称  DT：C  
#define  FID_SXZ              901  //属性值  DT：C  
#define  FID_YHSX             902  //银行属性  DT：C  
#define  FID_YHYW             903  //银行业务  DT：C  
#define  FID_ZQYW             904  //证券业务  DT：C  
#define  FID_ZJMMXY           905  //资金密码效验  DT：C  
#define  FID_JYMMXY           906  //交易买卖效验  DT：C  
#define  FID_YHMMXY           907  //银行买卖效验  DT：C  
#define  FID_YHMC             908  //银行名称  DT：C  
#define  FID_HBLB             909  //货币类别  DT：C  
#define  FID_WBZHMM           910  //外部帐户密码  DT：C  
#define  FID_DYBS             911  //转帐对应标识  DT：C  
#define  FID_SQH              912  //申请号  DT：C  
#define  FID_WBLSH            913  //外部流水号  DT：C  
#define  FID_BDDJJE           914  //本地冻结金额  DT：R  
#define  FID_CJBS             915  //成交笔数  DT：I  
#define  FID_SHLB             916  //审核类别  DT：C  
#define  FID_SHJG             917  //审核结果  DT：C  
#define  FID_YEGXSJ           918  //余额更新时间  DT：C  
#define  FID_QSMM             919  //券商密码  DT：C  
#define  FID_MMMY             920  //密码密钥  DT：C  
#define  FID_CSMY             921  //传输密钥  DT：C  
#define  FID_FJXX             922  //附加信息  DT：C  
#define  FID_WBCLJG           923  //外部处理结果  DT：C  
#define  FID_SQWD             924  //申请网点  DT：C  
#define  FID_XYBZ             925  //效验标志  DT：C  
#define  FID_CXSQH            926  //撤销申请号  DT：C  
#define  FID_JYBS             927  //交易标识  DT：C  
#define  FID_CLLB             928  //处理类别  DT：C  
#define  FID_ZHLB             929  //帐户类别  DT：C  
#define  FID_FQF              930  //发起方  DT：C  
#define  FID_HZBS             931  //汇总笔数  DT：C  
#define  FID_HZJE             932  //汇总金额  DT：C  
#define  FID_LY               933  //来源  DT：C  
#define  FID_WDH              934  //网点号  DT：C  
#define  FID_QD1              935  //起点一  DT：C  
#define  FID_QD2              936  //起点二  DT：C  
#define  FID_QD3              937  //起点三  DT：C  
#define  FID_QD4              938  //起点四  DT：C  
#define  FID_QD5              939  //起点五  DT：C  
#define  FID_BL1              940  //佣金一  DT：C  
#define  FID_BL2              941  //佣金二  DT：C  
#define  FID_BL3              942  //佣金三  DT：C  
#define  FID_BL4              943  //佣金四  DT：C  
#define  FID_BL5              944  //佣金五  DT：C  
#define  FID_KHJCGX           945  //客户继承关系  DT：C  
#define  FID_SYZH1            946  //收益帐号一  DT：C  
#define  FID_SYZH2            947  //收益帐号二  DT：C  
#define  FID_JSTS             948  //计算天数  DT：C  
#define  FID_TJJYR            949  //统计交易日  DT：C  
#define  FID_TJTS             950  //统计天数  DT：C  
#define  FID_FPBL             951  //分配比例  DT：C  
#define  FID_ZKFS             952  //折扣方式  DT：C  
#define  FID_ZJED             953  //资金额度  DT：C  
#define  FID_ZCED             954  //资产额度  DT：C  
#define  FID_GLF              955  //管理费  DT：C  
#define  FID_YJDX             956  //佣金底限  DT：C  
#define  FID_FHBZ             957  //复核标志  DT：C  
#define  FID_BDSL             958  //变动数量  DT：C  
#define  FID_HZFS             959  //汇总方式  DT：C  
#define  FID_LPSL             960  //礼品数量  DT：C  
#define  FID_LXBJ             961  //利息报价  DT：C  
#define  FID_KLX              962  //卡类型  DT：C  
#define  FID_KMC              963  //卡名称  DT：C  
#define  FID_BSC              964  //标识串  DT：C  
#define  FID_BSCKSWZ          965  //标识串开始位置  DT：C  
#define  FID_FJC              966  //附加串  DT：C  
#define  FID_FJCKSWZ          967  //附加串开始位置  DT：C  
#define  FID_KHKSWZ           968  //卡号开始位置  DT：C  
#define  FID_KHCD             969  //卡号长度  DT：C  
#define  FID_EN_YWKM          970  //允许业务科目  DT：C  
#define  FID_ZRSZ             971  //昨日市值  DT：C  
#define  FID_NEXTDATA         972  //继续取数标志  DT：C  
#define  FID_SYL              973  //收益率  DT：C  
#define  FID_FHS1             974  //已返还佣金  DT：C  
#define  FID_GJBM1            975  //国籍编码一  DT：C  
#define  FID_GJBM2            976  //国籍编码二  DT：C  
#define  FID_GJBM3            977  //国籍编码三  DT：C  
#define  FID_GJMC             978  //国籍名称  DT：C  
#define  FID_EGJMC            979  //国籍英文名称  DT：C  
#define  FID_CCCB             980  //持仓成本  DT：C  
#define  FID_BDRQ             981  //变动日期  DT：C  
#define  FID_MMXYLX           982  //密码效验类型  DT：C  
#define  FID_XZSJ             983  //限制时间  DT：C  
#define  FID_KHDXLX           984  //客户对象类型  DT：C  
#define  FID_JYSXGX           985  //交易所相关性  DT：C  
#define  FID_BZXGX            986  //币种相关性  DT：C  
#define  FID_QZXGX            987  //群组相关性  DT：C  
#define  FID_EXFLG            988  //查询扩展信息标志  DT：C  
#define  FID_MAX_D            989  //最大值（浮点）  DT：C  
#define  FID_MIN_D            990  //最小值（浮点）  DT：C  
#define  FID_MAX_L            991  //最大值（整型）  DT：C  
#define  FID_MIN_L            992  //最小值（整型）  DT：C  
#define  FID_GYFJQX           993  //柜员附加权限  DT：C  
#define  FID_ZHTZE            994  //帐户投资额  DT：C  
#define  FID_QSJE_B           995  //回报买清算资金  DT：C  
#define  FID_QSJE_S           996  //回报卖清算资金  DT：C  
#define  FID_FDYK_TB          997  //摊薄浮动盈亏  DT：C  
#define  FID_LLCSLB           998  //利率参数类别  DT：C  
#define  FID_EX_JJR           999  //禁止经纪人  DT：C  
#define  FID_FHFS             1000  //佣金返还方式  DT：C  
#define  FID_BZ_FH            1001  //佣金返还币种  DT：C  
#define  FID_HBDHBL           1002  //货币兑换比例  DT：C  
#define  FID_SYZH3            1003  //收益帐户3  DT：C  
#define  FID_SYBL1            1004  //收益比例1  DT：C  
#define  FID_SYBL2            1005  //收益比例2  DT：C  
#define  FID_SYBL3            1006  //收益比例3  DT：C  
#define  FID_GDKZSX           1007  //股东控制属性  DT：I  
#define  FID_RCKHH            1008  //容错客户号标志  DT：I  
#define  FID_RQ2              1009  //日期2  DT：C  
#define  FID_EN_YXZZDM        1010  //营销组织代码范围  DT：C  
#define  FID_SBWTH            1011  //申报委托号  DT：C  
#define  FID_CITYID           1012  //城市ID  DT：C  
#define  FID_DDLX             1013  //订单类型  DT：I  
#define  FID_SECTIONID        1014  //辖区ID  DT：C  
#define  FID_SECTION          1015  //辖区名称  DT：C  
#define  FID_SFZQ             1016  //收费周期  DT：C  
#define  FID_WTPCH            1017  //委托批次号  DT：I  
#define  FID_KXDLSF           1018  //可选登录身份  DT：I  
#define  FID_EN_YWLB          1019  //业务类别范围  DT：C  
#define  FID_BZDM             1020  //标准代码  DT：C  
#define  FID_WBDM             1021  //外部代码  DT：C  
#define  FID_CSDM             1022  //参数代码  DT：C  
#define  FID_CSMC             1023  //参数名称  DT：C  
#define  FID_QZSM             1024  //取值说明  DT：C  
#define  FID_EN_FJBZ          1025  //允许的附加标志  DT：C  
#define  FID_HYMC             1026  //行业名称  DT：C  
#define  FID_JSJG             1027  //结算机构  DT：C  
#define  FID_CWLX             1028  //错误类型  DT：C  
#define  FID_CWDM             1029  //错误类型  DT：C  
#define  FID_CWSM             1030  //错误类型  DT：C  
#define  FID_ZXZS             1031  //最新指数  DT：C  
#define  FID_TZEBZ            1032  //投资额标志  DT：C  
#define  FID_KBBZ             1033  //佣金捆绑标志  DT：C  
#define  FID_KHTZFL           1034  //客户投资分类(X,DM.KH,ZFL)  DT：C  
#define  FID_ZJZR             1035  //转入资金  DT：C  
#define  FID_ZJZC             1036  //转出资金  DT：C  
#define  FID_SZZR             1037  //转入市值  DT：C  
#define  FID_SZZC             1038  //转出市值  DT：C  
#define  FID_MKKZ             1039  //卖空控制  DT：I  
#define  FID_MFCS             1040  //免费登录次数  DT：C  
#define  FID_XZCS             1041  //限制登录次数  DT：C  
#define  FID_SJSX_A           1042  //每日使用总时间上限  DT：C  
#define  FID_SJSX_S           1043  //每日使用单站点时间上限  DT：C  
#define  FID_MMCSCS           1044  //密码尝试次数  DT：C  
#define  FID_ZDJDTS           1045  //自动解冻天数  DT：C  
#define  FID_MFHQCS           1046  //免费查询行情次数  DT：C  
#define  FID_HQSFSX           1047  //行情收费上限  DT：C  
#define  FID_HKSFSX           1048  //划卡收费上限  DT：C  
#define  FID_SJSFSX           1049  //使用时间收费上限  DT：C  
#define  FID_ZSFSX            1050  //总收费上限  DT：C  
#define  FID_DBYJXX           1051  //单笔佣金下限  DT：C  
#define  FID_DBYJSX           1052  //单笔佣金上限  DT：C  
#define  FID_ZCJJE            1053  //总成交金额  DT：R  
#define  FID_YJDJFS           1054  //佣金定价方式XTDM.YJDJFS  DT：C  
#define  FID_FHGY             1055  //复核柜员  DT：C  
#define  FID_TJZQLB           1056  //统计证券类别  DT：C  
#define  FID_DJJSFS           1057  //佣金定价结算方式  DT：I  
#define  FID_FDBH             1058  //分段编号  DT：C  
#define  FID_CFCDBZ           1059  //是否允许重复撤单  DT：I  DT：0 不允许；1 允许  
#define  FID_ZHDM             1060  //专户代码  DT：C  
#define  FID_TZDW             1061  //投资单位  DT：C  
#define  FID_TZXX             1062  //投资下限  DT：C  
#define  FID_SYZKXS           1063  //收益折扣系统  DT：C  
#define  FID_SYTZFS           1064  //收益投资方式  DT：C  
#define  FID_TZJESX           1065  //投资金额上限  DT：C  
#define  FID_ZJLCDS           1066  //资金留存底数  DT：C  
#define  FID_TZBDJE           1067  //投资额变更数  DT：C  
#define  FID_ZHKHH            1068  //专户客户号  DT：C  
#define  FID_ZHZJZH           1069  //专户资金账户  DT：C  
#define  FID_LCJZJE           1070  //理财集中金额  DT：C  
#define  FID_LCFHJE           1071  //理财返还金额  DT：C  
#define  FID_SYZTZBZ          1072  //理财收益再投资标志  DT：C  
#define  FID_SYZTZZQ          1073  //收益再投资周期  DT：C  
#define  FID_LCZSY            1074  //理财总收益  DT：C  
#define  FID_SYZCJE           1075  //理财收益转存金额  DT：C  
#define  FID_SYZTZE           1076  //收益再投资金额  DT：C  
#define  FID_XYBH             1077  //协议编号  DT：C  
#define  FID_LCTZJE           1078  //理财投资金额  DT：C  
#define  FID_SGDM             1079  //专户申购代码  DT：C  
#define  FID_CLBZ             1080  //处理标志  DT：C  
#define  FID_FPSY             1081  //分配收益  DT：C  
#define  FID_GBZQL            1082  //公布中签率  DT：C  
#define  FID_SGSL             1083  //申购数量  DT：C  
#define  FID_SGJG             1084  //申购价格  DT：C  
#define  FID_SSRQ             1085  //上市日期  DT：C  
#define  FID_HKRQ             1086  //还款日期  DT：C  
#define  FID_SSDM             1087  //上市代码  DT：C  
#define  FID_PHDM             1088  //配号代码  DT：C  
#define  FID_HKDM             1089  //还款代码  DT：C  
#define  FID_SGRQ             1090  //申购日期  DT：C  
#define  FID_JEBL             1091  //金额比率  DT：C  
#define  FID_LCJZQTJE         1092  //理财集中金额  DT：C  
#define  FID_HZGDH            1093  //合作股东号  DT：C  
#define  FID_SYDX             1094  //适用对象  DT：C  
#define  FID_JYLJSFS          1095  //交易量计算方式  DT：C  
#define  FID_JYLLJZQ          1096  //交易量累计周期  DT：C  
#define  FID_DJFS             1097  //定价方式  DT：C  
#define  FID_BBJ              1098  //保本价  DT：C  
#define  FID_HZLB             1099  //合作帐户类别  DT：C  
#define  FID_FSJE_LCBJ        1100  //本金发生金额  DT：C  
#define  FID_BCYE_LCBJ        1101  //本次本金余额  DT：C  
#define  FID_FSJE_SY          1102  //收益发生金额  DT：C  
#define  FID_BCYE_SY          1103  //待转收益余额  DT：C  
#define  FID_FSJE_BZJ         1104  //保证金发生额  DT：C  
#define  FID_BCYE_BZJ         1105  //保证金帐户余额  DT：C  
#define  FID_HZLBMC           1106  //合作类别名称  DT：C  
#define  FID_KSFW             1107  //起始范围  DT：C  
#define  FID_JSFW             1108  //结束范围  DT：C  
#define  FID_ZDKZBZ           1109  //指定控制标志  DT：I  
#define  FID_KHS              1110  //开户户数  DT：C  
#define  FID_XHS              1111  //销户户数  DT：C  
#define  FID_ZHS              1112  //总户数  DT：C  
#define  FID_ZDGDHS           1113  //指定股东户数  DT：C  
#define  FID_CXZDHS           1114  //撤销股东户数  DT：C  
#define  FID_CCGDHS           1115  //持仓股东户数  DT：C  
#define  FID_TJZJYE           1116  //统计资金余额  DT：C  
#define  FID_TJZJCK           1117  //统计增加存款  DT：C  
#define  FID_TJZJQK           1118  //统计增加取款  DT：C  
#define  FID_TJZJGP           1119  //统计增加市值  DT：C  
#define  FID_TJJSGP           1120  //统计减少市值  DT：C  
#define  FID_TJGPSZ           1121  //统计股票市值  DT：C  
#define  FID_TJZZC            1122  //统计总资产  DT：C  
#define  FID_FDB1             1123  //统计分段开始1  DT：C  
#define  FID_FDE1             1124  //统计分段结束1  DT：C  
#define  FID_FDB2             1125  //统计分段开始2  DT：C  
#define  FID_FDE2             1126  //统计分段结束2  DT：C  
#define  FID_FDB3             1127  //统计分段开始3  DT：C  
#define  FID_FDE3             1128  //统计分段结束3  DT：C  
#define  FID_FDB4             1129  //统计分段开始4  DT：C  
#define  FID_FDE4             1130  //统计分段结束4  DT：C  
#define  FID_FDB5             1131  //统计分段开始5  DT：C  
#define  FID_FDE5             1132  //统计分段结束5  DT：C  
#define  FID_FDB6             1133  //统计分段开始6  DT：C  
#define  FID_FDE6             1134  //统计分段结束6  DT：C  
#define  FID_TJJGGS           1135  //统计结果个数  DT：C  
#define  FID_TJJGSJ           1136  //统计结果数据  DT：C  
#define  FID_QSBZ             1137  //实时清算标志  DT：C  
#define  FID_TZJE             1138  //透支金额  DT：C  
#define  FID_WJSZJ            1139  //未交收资金  DT：C  
#define  FID_ZJDJLSH          1140  //资金冻结流水号  DT：I  
#define  FID_YDZD             1141  //应答字段  DT：C  
#define  FID_YDZDSM           1142  //应答字段说明  DT：C  
#define  FID_YDSJ             1143  //应答数据  DT：C  
#define  FID_JCJJE            1144  //净成交金额  DT：C  
#define  FID_YXHS             1145  //有效户数  DT：C  
#define  FID_CJSLBL           1146  //成交数量比例  DT：C  
#define  FID_CJJEBL           1147  //成交金额比例  DT：C  
#define  FID_S1BL             1148  //佣金收入比例  DT：C  
#define  FID_ZZCBL1           1149  //营业部的资产比例  DT：C  
#define  FID_ZZCBL2           1150  //总公司的资产比例  DT：C  
#define  FID_ZSLBL1           1151  //营业部的数量比例  DT：C  
#define  FID_ZSLBL2           1152  //总公司的数量比例  DT：C  
#define  FID_ZHSBL1           1153  //营业部的户数比例  DT：C  
#define  FID_ZHSBL2           1154  //总公司的户数比例  DT：C  
#define  FID_JEXX             1155  //金额下限  DT：C  
#define  FID_ZQDJLSH          1156  //证券冻结流水号  DT：I  
#define  FID_WTSB             1157  //银证转帐参数的申报状态  DT：C  
#define  FID_QDBZ             1158  //银证转帐参数的签到标志  DT：C  
#define  FID_WBZH             1159  //外部帐号  DT：C  
#define  FID_SQBH             1160  //业务申请编号  DT：C  
#define  FID_WBCKKY           1161  //外部帐户参考可用余额  DT：C  
#define  FID_WBCKKQ           1162  //外部帐户参考可取资金余额  DT：C  
#define  FID_WBDJJE           1163  //外部帐户参考冻结金额  DT：C  
#define  FID_JYXZ             1164  //银证通交易限制  DT：C  
#define  FID_ZJXYFS           1165  //银证通资金校验方式  DT：C  
#define  FID_HBBZ             1166  //银证通资料回报类型  DT：C  
#define  FID_ZJHZFS           1167  //银证通资金回转方式  DT：C  
#define  FID_DRQKJE           1168  //当日取款金额  DT：R  
#define  FID_JGKZBZ           1169  //价格控制标志  DT：I  
#define  FID_ZZJSSJ           1170  //银证转帐停止正常转帐交易的时间  DT：C  
#define  FID_CZDRZXBZ         1171  //撤指当日是否允许注销  DT：I  DT：0 不允许，1 允许  
#define  FID_LSH_LXS          1172  //收取利息税的业务流水号  DT：I  
#define  FID_LX               1173  //利息  DT：R  
#define  FID_ZJMXLSH          1174  //资金明细流水号  DT：I  
#define  FID_YSSL             1175  //应收数量  DT：I  
#define  FID_BBYK             1176  //本笔盈亏  DT：R  
#define  FID_JYSFY            1177  //交易所费用  DT：R  
#define  FID_BCJKYE           1178  //本次借款余额  DT：R  
#define  FID_KSGSL            1179  //可申购数量  DT：I  
#define  FID_ZSZBL1           1180  //营业部的市值比例  DT：C  
#define  FID_ZSZBL2           1181  //总公司的市值比例  DT：C  
#define  FID_QCKHS            1182  //期初客户数  DT：C  
#define  FID_QMKHS            1183  //期末客户数  DT：C  
#define  FID_KSHSL            1184  //可赎回数量  DT：I  
#define  FID_EN_ZQDM          1185  //证券代码范围  DT：C  
#define  FID_XCMRJ            1186  //现钞买入价  DT：C  
#define  FID_XCMCJ            1187  //现钞卖出价  DT：C  
#define  FID_XHMRJ            1188  //现汇买入价  DT：C  
#define  FID_XHMCJ            1189  //现汇卖出价  DT：C  
#define  FID_GSBL             1190  //估算比例  DT：C  
#define  FID_HLBZ             1191  //汇率标志  DT：C  
#define  FID_DRCKJE           1192  //当日帐户存款金额  DT：C  
#define  FID_TZCKJE           1193  //当日通知存款金额  DT：C  
#define  FID_JGMC_E           1194  //机构英文名称  DT：C  
#define  FID_FRDB             1195  //法人代表  DT：C  
#define  FID_WTQR             1196  //委托确认库  DT：C  
#define  FID_HYLB             1197  //行业类别  DT：C  
#define  FID_YWFW             1198  //业务范围  DT：C  
#define  FID_ZCDZ             1199  //注册地址  DT：C  
#define  FID_ZCZB             1200  //注册资本  DT：C  
#define  FID_ZCYE             1201  //资产余额  DT：C  
#define  FID_FZYE             1202  //负债余额  DT：C  
#define  FID_YSZC             1203  //原始资产  DT：C  
#define  FID_CXWTH            1204  //撤单撤销委托号  DT：I  
#define  FID_YWXZ             1205  //业务限制  DT：C  
#define  FID_SJXM             1206  //审计项目  DT：C  
#define  FID_SJMC             1207  //审计名称  DT：C  
#define  FID_JS               1208  //债券增值积数  DT：C  
#define  FID_CCJJ             1209  //持仓均价  DT：C  
#define  FID_LJYK             1210  //累计盈亏  DT：C  
#define  FID_PGSL             1211  //配股数量  DT：C  
#define  FID_BL               1212  //比例  DT：C  
#define  FID_CSLB             1213  //参数类别  DT：C  
#define  FID_FJBZ             1214  //附加标识  DT：C  
#define  FID_QZQKBZ           1215  //强制取款标志  DT：C  
#define  FID_SBRQ             1216  //委托申报日期  DT：I  
#define  FID_ZCXX             1217  //资产下限  DT：C  
#define  FID_ZCSX             1218  //资产上限  DT：C  
#define  FID_TIME             1219  //操作时间  DT：C  
#define  FID_PGJE             1220  //配股金额  DT：C  
#define  FID_TGZH             1221  //债券系统托管帐户  DT：C  
#define  FID_GZQX             1222  //国债期限  DT：C  
#define  FID_HGTS             1223  //回购天数  DT：C  
#define  FID_GZJYSX           1224  //国债交易属性  DT：C  
#define  FID_ISLOGIN          1225  //是否登录标志  DT：I  
#define  FID_GZNBDM           1226  //债券内部代码  DT：C  
#define  FID_GZFJSX           1227  //国债附加属性  DT：C  
#define  FID_GZMZSX           1228  //面值属性  DT：C  
#define  FID_GZJXTS           1229  //国债计息天数  DT：C  
#define  FID_FXJG             1230  //债券发行价格  DT：C  
#define  FID_TGBH             1231  //托管编号  DT：C  
#define  FID_DYSL             1232  //抵押数量  DT：C  
#define  FID_TGRQ             1233  //托管日期  DT：C  
#define  FID_SXF              1234  //手续费  DT：C  
#define  FID_DJSL             1235  //冻结数量  DT：C  
#define  FID_CQDJ             1236  //长期冻结  DT：C  
#define  FID_JYFY             1237  //交易费用  DT：C  
#define  FID_DQBX             1238  //债券到期本息  DT：C  
#define  FID_GZLL             1239  //国债利率  DT：C  
#define  FID_DQRQ             1240  //到期（兑付日期）  DT：C  
#define  FID_JXRQ             1241  //开始记息日期  DT：C  
#define  FID_LTRQ             1242  //开始流通日期  DT：C  
#define  FID_FXZQDJR          1243  //派息登记日  DT：C  
#define  FID_ZQFXRQ           1244  //派息日  DT：C  
#define  FID_DFZQDJR          1245  //兑付登记日  DT：C  
#define  FID_BRLX             1246  //记账式债券单位（100）本日利息  DT：C  
#define  FID_HGJG             1247  //回购价格  DT：C  
#define  FID_GHJG             1248  //购回价格  DT：C  
#define  FID_KTBZ             1249  //开通标志  DT：C  
#define  FID_HGSL             1250  //回购数量  DT：C  
#define  FID_HGRQ             1251  //回购日期  DT：C  
#define  FID_GHRQ             1252  //购回日期  DT：C  
#define  FID_ZRTGZH           1253  //转入托管帐户  DT：C  
#define  FID_QTZC             1254  //其他资产  DT：C  
#define  FID_ZZC              1255  //总资产  DT：C  
#define  FID_WQSZJ            1256  //未清算资金  DT：C  
#define  FID_JGDM             1257  //机构代码  DT：C  
#define  FID_DFSXFL           1258  //兑付手续费率  DT：C  
#define  FID_DFLXSL           1259  //兑付利息税率  DT：C  
#define  FID_JXFS             1260  //计息方式  DT：C  
#define  FID_CSSM             1261  //参数说明  DT：C  
#define  FID_SZSM             1262  //参数设置说明  DT：C  
#define  FID_XGGY             1263  //修改柜员  DT：C  
#define  FID_LOGINNAME        1264  //登录用户名称  DT：C  
#define  FID_MRDLSF           1265  //默认登录身份  DT：I  
#define  FID_MGZC             1266  //每股资产  DT：R  
#define  FID_SNMGSY           1267  //上年每股收益  DT：R  
#define  FID_XZJB             1268  //限制级别  DT：I  
#define  FID_EN_MCQZ          1269  //名称前缀范围  DT：C  
#define  FID_BDZQDM           1270  //本地证券代码  DT：C  DT：买断式回购本地债券代码  
#define  FID_LYJBL            1271  //履约金比例  DT：R  
#define  FID_RQWGHZJ          1272  //融券未购回资金(权益)  DT：R  
#define  FID_RZWGHZJ          1273  //融资未购回资金(负债)  DT：R  
#define  FID_BWJSZJ           1274  //买入未交收资金  DT：R  
#define  FID_SWJSZJ           1275  //卖出未交收资金  DT：R  
#define  FID_YSYXYED          1276  //已使用信用额度  DT：R  
#define  FID_DJWTK            1277  //登记委托库  DT：C  
#define  FID_DJHBK            1278  //登记回报库  DT：C  
#define  FID_CDSB             1279  //撤单申报库  DT：C  
#define  FID_CDQR             1280  //撤单确认库  DT：C  
#define  FID_WTQRNO           1281  //委托确认笔数  DT：I  
#define  FID_CDQRNO           1282  //撤单确认笔数  DT：I  
#define  FID_TYPE_BF          1283  //备份通道类型  DT：I  
#define  FID_MODULE_BF        1284  //备份申报模块  DT：C  
#define  FID_HOST_BF          1285  //备份申报主机地址  DT：C  
#define  FID_WTSB_BF          1286  //备份委托申报库  DT：C  
#define  FID_WTQR_BF          1287  //备份委托确认库  DT：C  
#define  FID_CDSB_BF          1288  //备份撤单申报库  DT：C  
#define  FID_CDQR_BF          1289  //备份撤单确认库  DT：C  
#define  FID_WTQRNO_BF        1290  //备份通道委托申报确认笔数  DT：I  
#define  FID_CDQRNO_BF        1291  //备份通道撤单确认笔数  DT：I  
#define  FID_TDBH             1292  //通道编号  DT：I  
#define  FID_EN_TDBH          1293  //通道编号范围  DT：C  
#define  FID_LTXW             1294  //联通席位  DT：C  
#define  FID_YYBXZMS          1295  //营业部限制模式  DT：I  
#define  FID_ZQLBXZMS         1296  //证券类别限制模式  DT：I  
#define  FID_SBBS             1297  //申报笔数  DT：I  
#define  FID_SBBS_S           1298  //单次轮询最大申报笔数  DT：I  
#define  FID_JGSJ             1299  //间隔时间  DT：I  
#define  FID_YXHSBL1          1300  //营业部有效户数比例  DT：C  
#define  FID_YXHSBL2          1301  //总部有效户数比例  DT：C  
#define  FID_ENDKHH           1302  //结束客户号  DT：C  
#define  FID_FILESIZE         1303  //文件大小  DT：C  
#define  FID_FILETRANSIZE     1304  //文件传送时的大小,比如：压缩后  DT：C  
#define  FID_YJXX             1305  //最低佣金  DT：C  
#define  FID_YJSX             1306  //最高佣金  DT：C  
#define  FID_YHSL             1307  //印花税率  DT：C  
#define  FID_GHFL             1308  //过户费率  DT：C  
#define  FID_GHFXX            1309  //最低过户费  DT：C  
#define  FID_GHFSX            1310  //最高过户费  DT：C  
#define  FID_FJF              1311  //附加费  DT：C  
#define  FID_JSFL             1312  //结算费率  DT：R  
#define  FID_JSFXX            1313  //最低结算费  DT：C  
#define  FID_JSFSX            1314  //最高结算费  DT：C  
#define  FID_JYGFL            1315  //交易规费率  DT：C  
#define  FID_JYGFXX           1316  //交易规费下限  DT：C  
#define  FID_JYGFSX           1317  //交易规费上限  DT：C  
#define  FID_QTFL             1318  //其它费率  DT：C  
#define  FID_QTFXX            1319  //最低其他费用  DT：C  
#define  FID_QTFSX            1320  //最高其他费用  DT：C  
#define  FID_FID              1321  //FIX包中的FID编号  DT：I  
#define  FID_EN_XWH           1322  //席位号范围  DT：C  
#define  FID_FWDM             1323  //客户服务代码  DT：C  
#define  FID_FWMC             1324  //客户服务名称  DT：C  
#define  FID_FWLX             1325  //客户服务类型  DT：C  
#define  FID_SLZ              1326  //客户服务受理主体  DT：C  
#define  FID_CLFS             1327  //客户服务处理方式  DT：C  
#define  FID_FILE             1328  //文件名  DT：C  
#define  FID_FILE2            1329  //文件名二  DT：C  
#define  FID_PROCNAME         1330  //过程名  DT：C  
#define  FID_CSXH             1331  //参数序号  DT：C  
#define  FID_TYPE             1332  //参数类型  DT：C  
#define  FID_QSZ              1333  //缺省值  DT：C  
#define  FID_CJHB             1334  //成交回报库  DT：C  
#define  FID_CS1              1335  //参数1  DT：C  
#define  FID_CS2              1336  //参数2  DT：C  
#define  FID_CS3              1337  //参数3  DT：C  
#define  FID_CS4              1338  //参数4  DT：C  
#define  FID_CS5              1339  //参数5  DT：C  
#define  FID_CS6              1340  //参数6  DT：C  
#define  FID_CS7              1341  //参数7  DT：C  
#define  FID_CS8              1342  //参数8  DT：C  
#define  FID_CS9              1343  //参数9  DT：C  
#define  FID_CS10             1344  //参数10  DT：C  
#define  FID_CS11             1345  //参数11  DT：C  
#define  FID_CS12             1346  //参数12  DT：C  
#define  FID_CS13             1347  //参数13  DT：C  
#define  FID_CS14             1348  //参数14  DT：C  
#define  FID_CS15             1349  //参数15  DT：C  
#define  FID_OPTERATOR        1350  //核心开户操作类型  DT：C  
#define  FID_OPTION           1351  //核心开户参数1  DT：C  
#define  FID_PACKINDEX        1352  //数据包序列号  DT：C  
#define  FID_GYDM             1353  //柜员代码  DT：C  
#define  FID_JYJS             1354  //交易基数  DT：C  
#define  FID_QTSX             1355  //其他属性  DT：C  
#define  FID_TDJY             1356  //替代交易  DT：C  
#define  FID_MRJG5            1357  //买入价格5  DT：C  
#define  FID_MRSL5            1358  //买入数量5  DT：C  
#define  FID_MCJG5            1359  //卖出价格5  DT：C  
#define  FID_MCSL5            1360  //卖出数量5  DT：C  
#define  FID_ZQSZ_RMB         1361  //人民币证券市值  DT：C  
#define  FID_ZQSZ_USD         1362  //美元证券市值  DT：C  
#define  FID_ZQSZ_HKD         1363  //港币证券市值  DT：C  
#define  FID_ZQSZ_ZSRMB       1364  //总证券市值折算人民币  DT：C  
#define  FID_ZJYE_RMB         1365  //人民币资金余额  DT：C  
#define  FID_ZJYE_USD         1366  //美元资金余额  DT：C  
#define  FID_ZJYE_HKD         1367  //港币资金余额  DT：C  
#define  FID_ZJYE_ZSRMB       1368  //总资金余额折算人民币  DT：C  
#define  FID_QTZC_RMB         1369  //人民币其他资产  DT：C  
#define  FID_QTZC_USD         1370  //美元其他资产  DT：C  
#define  FID_QTZC_HKD         1371  //港币其他资产  DT：C  
#define  FID_QTZC_ZSRMB       1372  //总其他资产折算人民币  DT：C  
#define  FID_LASTRECNO        1373  //最近回报记录号  DT：I  
#define  FID_HTHQZFW          1374  //合同号前缀范围  DT：C  
#define  FID_TZZHDM           1375  //投资组合代码  DT：C  
#define  FID_TZZHMC           1376  //投资组合代码  DT：C  
#define  FID_TZZHQZ           1377  //投资组合代码  DT：C  
#define  FID_TZZHLB           1378  //投资组合代码  DT：C  
#define  FID_HTHQJ            1379  //合同号区间  DT：C  
#define  FID_FWLB             1380  //服务类别  DT：C  
#define  FID_XMZX             1381  //项目子项  DT：C  
#define  FID_DZLB             1382  //主动推送允许地址类别  DT：C  
#define  FID_QSDZLB           1383  //主动推送缺省地址类别  DT：C  
#define  FID_SJFY             1384  //手机短信方式的服务收费标准  DT：C  
#define  FID_YJFY             1385  //EMAIL方式的服务收费标准  DT：C  
#define  FID_ZF               1386  //涨幅  DT：C  
#define  FID_DF               1387  //跌幅  DT：C  
#define  FID_FSCS             1388  //发送次数  DT：C  
#define  FID_HQNR             1389  //行情点播内容  DT：C  
#define  FID_ZDSJ             1390  //行情点播允许发送指定时间点  DT：C  
#define  FID_SJQJ             1391  //行情点播允许发送时间区间  DT：C  
#define  FID_ZQDM1            1392  //证券代码1  DT：C  
#define  FID_ZQDM2            1393  //证券代码2  DT：C  
#define  FID_ZQDM3            1394  //证券代码3  DT：C  
#define  FID_ZQDM4            1395  //证券代码4  DT：C  
#define  FID_ZQDM5            1396  //证券代码5  DT：C  
#define  FID_RIGH             1397  //坐席用户权限  DT：C  
#define  FID_MGSY             1398  //每股收益  DT：C  
#define  FID_SHDM             1399  //上海市场股票代码(配售)  DT：C  
#define  FID_SZDM             1400  //深圳市场股票代码(配售)  DT：C  
#define  FID_GSHY             1401  //上市公司行业  DT：C  
#define  FID_GSMC             1402  //上市公司名称  DT：C  
#define  FID_FXZL             1403  //发行总量  DT：C  
#define  FID_FILEWRITETIME    1404  //文件修改时间  DT：C  
#define  FID_DHXL             1405  //单户申购限量  DT：C  
#define  FID_BLFM             1406  //比例分母,配股、送股等  DT：C  
#define  FID_BLFZ             1407  //比例分子,配股、送股等  DT：C  
#define  FID_ZQL              1408  //中签率  DT：C  
#define  FID_SSGJ             1409  //上市估价  DT：C  
#define  FID_FXRQ             1410  //发行日期  DT：C  
#define  FID_ZQYHRQ           1411  //中签摇号日期  DT：C  
#define  FID_CQRQ             1412  //除权日期  DT：C  
#define  FID_JKQSRQ           1413  //缴款起始日期  DT：C  
#define  FID_JKJSRQ           1414  //缴款结束日期  DT：C  
#define  FID_TSXX             1415  //特别提示信息  DT：C  
#define  FID_ZGB              1416  //上市公司总股本  DT：C  
#define  FID_MGGJJ            1417  //每股资本公积金  DT：R  
#define  FID_MGWFPLR          1418  //每股未分配利润  DT：R  
#define  FID_CLMC             1419  //佣金定价策略名称  DT：C  
#define  FID_EN_FLLB          1420  //费率类别范围  DT：C  
#define  FID_GHFZKL           1421  //过户费折扣率  DT：R  
#define  FID_LJJS             1422  //累计计数  DT：I  
#define  FID_LJZQ             1423  //累计周期  DT：I  
#define  FID_ZCJSFS           1424  //资产计算方式  DT：I  
#define  FID_ZSYJL            1425  //折算佣金率  DT：I  
#define  FID_JYLFDBH          1426  //交易量分段编号  DT：I  
#define  FID_ZCFDBH           1427  //资产分段编号  DT：I  
#define  FID_JYLXX            1428  //交易量下限  DT：R  
#define  FID_JYLSX            1429  //交易量上限  DT：R  
#define  FID_GHJE             1430  //归还金额  DT：R  
#define  FID_FZYWDM           1431  //复制业务代码  DT：C  
#define  FID_LOGIN_ENABLE     1432  //登录允许状态  DT：I  DT：0 禁止；1 允许  
#define  FID_ZQJY_ENABLE      1433  //证券交易允许状态  DT：I  DT：0 禁止；1 允许  
#define  FID_ZJYW_ENABLE      1434  //资金业务允许状态  DT：I  DT：0 禁止；1 允许  
#define  FID_LSCXYW_ENABLE    1435  //历史查询业务状态  DT：I  DT：0 禁止；1 允许  
#define  FID_OFS_ENABLE       1436  //开放式基金允许状态  DT：I  DT：0 禁止；1 允许  
#define  FID_KHYW_ENABLE      1437  //客户业务允许状态  DT：I  DT：0 禁止；1 允许  
#define  FID_SFYG             1438  //是否内部员工  DT：I  
#define  FID_XZYS             1439  //限制月数  DT：I  
#define  FID_FUNCID           1440  //函数功能号  DT：I  
#define  FID_CXJE             1441  //撤销金额  DT：R  
#define  FID_ZJLY             1442  //资金来源  DT：C  DT：银行、交易所...  
#define  FID_WTSF             1443  //委托收费金额  DT：R  
#define  FID_CDSF             1444  //撤单收费金额  DT：R  
#define  FID_XYSYBZ           1445  //信用使用标志  DT：I  
#define  FID_LSCLMS           1446  //冲销资金业务原流水处理模式  DT：I  DT：0 保留，1 删除原流水  
#define  FID_JLLSBZ           1447  //是否记录流水标志  DT：I  DT：0 默认,记录流水; 1 不记录流水  
#define  FID_TZMS             1448  //调整模式  DT：I  DT：0 直接调整，1 累加  
#define  FID_DOMAIN1          1449  //查询域  DT：C  DT：全体查询当前数据所在的域  
#define  FID_REGLOG           1450  //是否记录日志  DT：I  DT：0 记录，1 不记录  
#define  FID_SLTSFS           1451  //数量提示方式  DT：C  
#define  FID_OWNER_KH         1452  //允许拥有客户  DT：I  
#define  FID_OWNER_JJR        1453  //允许拥有经纪人  DT：I  
#define  FID_KZFS_KHQX        1454  //客户权限控制方式  DT：I  
#define  FID_KZFS_YHQX        1455  //用户权限控制方式  DT：I  
#define  FID_KZFS_CSQX        1456  //参数权限控制方式  DT：I  
#define  FID_KZFS_KHFL        1457  //客户分类控制方式  DT：I  
#define  FID_YXRQ             1458  //有效日期  DT：I  
#define  FID_WJLX             1459  //文件类型  DT：I  
#define  FID_WJLJ             1460  //文件路径  DT：C  
#define  FID_SYFW             1461  //适用范围  DT：I  
#define  FID_ZJDZ             1462  //证件上的地址  DT：C  
#define  FID_FXJB             1463  //风险级别  DT：I  
#define  FID_SQCZRH           1464  //授权操作人编号  DT：C  
#define  FID_ZHGLJG           1465  //帐户管理机构  DT：C  
#define  FID_ZJDZ_FRDB        1466  //法人代表的证件地址  DT：C  
#define  FID_XM_JBR           1467  //机构经办人姓名  DT：C  
#define  FID_ZJLB_JBR         1468  //机构经办人证件类别  DT：I  
#define  FID_ZJBH_JBR         1469  //机构经办人证件编号  DT：C  
#define  FID_ZCFZ             1470  //资产分组  DT：C  
#define  FID_EN_ZCFZ          1471  //资产分组范围  DT：C  
#define  FID_XJYE             1472  //现金余额  DT：R  
#define  FID_RZRQ             1473  //入帐日期  DT：I  
#define  FID_TXXS             1474  //是否显示图像  DT：I  
#define  FID_CDWZID_NEW       1475  //新菜单位置代码  DT：I  
#define  FID_KZYWDM           1476  //扩展业务代码  DT：C  
#define  FID_KZYWMC           1477  //扩展业务名称  DT：C  
#define  FID_KZYWSM           1478  //扩展业务说明  DT：C  
#define  FID_KZYWZT           1479  //扩展业务状态  DT：I  
#define  FID_ZZXY             1480  //自助协议  DT：C  
#define  FID_NZZCJE           1481  //内转转出金额  DT：R  
#define  FID_NZZRJE           1482  //内转转入金额  DT：R  
#define  FID_QTCKJE           1483  //其它存款金额  DT：R  
#define  FID_QTQKJE           1484  //其它取款金额  DT：R  
#define  FID_ZPCKJE           1485  //支票存款金额  DT：R  
#define  FID_ZPQKJE           1486  //支票取款金额  DT：R  
#define  FID_ZZZCJE           1487  //银证转帐转出金额  DT：R  
#define  FID_ZZZRJE           1488  //银证转帐转入金额  DT：R  
#define  FID_HCJE             1489  //红冲金额  DT：R  
#define  FID_LBJE             1490  //兰补金额  DT：R  
#define  FID_TJZQ             1491  //统计周期  DT：I  DT：0 按天；1 按月；2 按年  
#define  FID_TJDW             1492  //统计单位  DT：C  
#define  FID_TGZRHS           1493  //转托管转入户数  DT：I  
#define  FID_TGZCHS           1494  //转托管转出户数  DT：I  
#define  FID_TGZRSZ           1495  //转托管转入市值  DT：R  
#define  FID_TGZCSZ           1496  //转托管转出市值  DT：R  
#define  FID_ZDZRSZ           1497  //指定转入市值  DT：R  
#define  FID_CZZCSZ           1498  //撤指转出市值  DT：R  
#define  FID_JGLBMC           1499  //机构类别名称  DT：C  
#define  FID_FXLB             1500  //发行类别  DT：I  
#define  FID_FXRDM            1501  //发行人代码  DT：C  
#define  FID_QZDM             1502  //权证代码  DT：C  
#define  FID_QZLX             1503  //权证类型  DT：I  
#define  FID_XQBL             1504  //行权比例  DT：R  
#define  FID_FILEDATA         1505  //文件数据  DT：C  
#define  FID_FILEZIP          1506  //文件是否压缩传送(由服务端决定)  DT：C  
#define  FID_FILECOMPANYNAME  1507  //文件的公司名称  DT：C  
#define  FID_FILEPRODUCTNAME  1508  //文件的相关产品名称  DT：C  
#define  FID_FILEDIR          1509  //文件所存放目录  DT：C  
#define  FID_FILENEW          1510  //是新文件，强制用户更新  DT：C  
#define  FID_FILERELATION     1511  //上传数据块与已存在的文件的关系  DT：C  
#define  FID_FILEPOSITION     1512  //文件中相对于文件头的偏移  DT：C  
#define  FID_FILETYPE         1513  //文件的业务类型  DT：C  
#define  FID_XQDM             1514  //行权代码  DT：C  
#define  FID_XQFS             1515  //行权方式  DT：I  
#define  FID_XQJG             1516  //行权价格  DT：R  
#define  FID_ZCLB             1517  //资产类别  DT：I  
#define  FID_DBXS             1518  //担保系数  DT：R  
#define  FID_SJZD             1519  //数据字典  DT：C  
#define  FID_SRKJLX           1520  //输入控件类型  DT：I  
#define  FID_DZFS             1521  //客户对帐单寄送方式  DT：I  
#define  FID_CXSL             1522  //撤销数量  DT：I  
#define  FID_BLSFUNC          1523  //对应的BLS功能码  DT：I  
#define  FID_QSFY             1524  //清算费用  DT：R  
#define  FID_RGRQ             1525  //认购日期  DT：I  
#define  FID_JGRQ             1526  //交割日期  DT：I  
#define  FID_JYSCPFL          1527  //交易所产品分类  DT：C  
#define  FID_JYSCPZL          1528  //交易所产品子类  DT：C  
#define  FID_DDLXXZ           1529  //订单类型限制  DT：C  
#define  FID_BSDDZXSL         1530  //冰山订单最小商量  DT：I  
#define  FID_BSDDZXPLSL       1531  //冰山订单最小披露数量  DT：I  
#define  FID_JYQX             1532  //交易权限  DT：I  
#define  FID_SBJB             1533  //申报级别  DT：I  
#define  FID_ZSXJ             1534  //止损限价  DT：R  
#define  FID_ISIN             1535  //ISIN代码  DT：C  
#define  FID_DDYXRQ           1536  //订单有效日期  DT：I  
#define  FID_DDSXXZ           1537  //订单时效限制  DT：I  
#define  FID_DDJYXZ           1538  //订单交易限制  DT：I  
#define  FID_JYSDDBH          1539  //交易所订单编号  DT：C  
#define  FID_BRCJSL           1540  //本日成交数量  DT：I  
#define  FID_BRCJJE           1541  //本日成交金额  DT：R  
#define  FID_EN_SBJB          1542  //申报级别范围  DT：C  
#define  FID_DZXXBZ           1543  //多值信息标志  DT：I  DT：0 单值，1 多值  
#define  FID_DXLX             1544  //对象类型  DT：I  
#define  FID_EN_LOGINID       1545  //登录ID范围  DT：C  
#define  FID_EN_JSDM          1546  //角色代码  DT：C  
#define  FID_JYBWFL           1547  //交易备忘分类  DT：I  
#define  FID_XYDJ             1601  //信用等级  DT：C  
#define  FID_CCSLXZ           1602  //持仓数量限制  DT：C  
#define  FID_YJBL             1603  //预警比例  DT：C  
#define  FID_PCBL             1604  //平仓比例  DT：C  
#define  FID_LTSL             1605  //流通数量  DT：C  
#define  FID_TABLENAME        1606  //表名  DT：C  
#define  FID_FIELDNAME        1607  //字段名  DT：C  
#define  FID_JDBZ             1608  //借贷标志  DT：C  
#define  FID_HYZ              1609  //合约状态  DT：C  
#define  FID_HYB              1610  //合约标题  DT：C  
#define  FID_JKHH             1611  //借方客户号  DT：C  
#define  FID_DKHH             1612  //贷方客户号  DT：C  
#define  FID_JYYB             1613  //借方营业部  DT：C  
#define  FID_DYYB             1614  //贷方营业部  DT：C  
#define  FID_ZCKZDX           1615  //资产控制底线  DT：C  
#define  FID_HYH              1616  //合约号  DT：C  
#define  FID_HYJE             1617  //合约金额  DT：C  
#define  FID_ZJZHBH           1618  //资金账户编号  DT：C  
#define  FID_RZLV             1619  //融资利率  DT：C  
#define  FID_JQRQ             1620  //结清日期  DT：C  
#define  FID_ZHYH             1621  //子合约号  DT：C  
#define  FID_JZJZH            1622  //借方资金账号  DT：C  
#define  FID_DZJZH            1623  //贷方资金账号  DT：C  
#define  FID_JZJYYB           1624  //借方资金营业部  DT：C  
#define  FID_DZJYYB           1625  //贷方资金营业部  DT：C  
#define  FID_EN_JYYB          1626  //借方营业部  DT：C  
#define  FID_EN_DYYB          1627  //贷方营业部  DT：C  
#define  FID_EN_JZJYYB        1628  //借方资金营业部  DT：C  
#define  FID_EN_DZJYYB        1629  //贷方资金营业部  DT：C  
#define  FID_DJGY             1630  //登记柜员  DT：C  
#define  FID_SHGY             1631  //审核柜员  DT：C  
#define  FID_JKHXM            1632  //借方客户姓名  DT：C  
#define  FID_DKHXM            1633  //贷方客户姓名  DT：C  
#define  FID_EN_BH            1634  //资金编号  DT：C  
#define  FID_EN_ZBH           1635  //资金子编号  DT：C  
#define  FID_ZHDYCS           1636  //帐户抵押次数  DT：I  
#define  FID_ZBH              1637  //资金子编号  DT：C  
#define  FID_ZHMC             1638  //帐户名称  DT：C  
#define  FID_JZHYE            1639  //借方帐户余额  DT：C  
#define  FID_DZHYE            1640  //贷方帐户余额  DT：C  
#define  FID_JZXSZ            1641  //借方最新市值  DT：C  
#define  FID_DZXSZ            1642  //贷方最新市值  DT：C  
#define  FID_JZCZZ            1643  //借方资产总值  DT：C  
#define  FID_DZCZZ            1644  //贷方资产总值  DT：C  
#define  FID_SHTG             1645  //审核是否通过  DT：C  
#define  FID_JQHYJE           1646  //已结清合约金额  DT：C  
#define  FID_EN_KHYYB         1647  //客户营业部  DT：C  
#define  FID_EN_ZJYYB         1648  //资金营业部  DT：C  
#define  FID_SRYE2            1649  //上日余额2  DT：C  
#define  FID_ZHYE2            1650  //帐户余额2  DT：C  
#define  FID_ZPJE2            1651  //支票金额2  DT：C  
#define  FID_DJJE2            1652  //冻结金额2  DT：C  
#define  FID_YCDJJE2          1653  //异常冻结金额2  DT：C  
#define  FID_ZJCQDJ2          1654  //资金长期冻结2  DT：C  
#define  FID_WJSJE2           1655  //未交收金额2  DT：C  
#define  FID_KYZJ2            1656  //可用资金2  DT：C  
#define  FID_KQZJ2            1657  //可取资金2  DT：C  
#define  FID_XJZC2            1658  //现金资产2  DT：C  
#define  FID_GPSZ2            1659  //股票市值2  DT：C  
#define  FID_LX2              1660  //预计利息2  DT：C  
#define  FID_LXS2             1661  //预计利息税2  DT：C  
#define  FID_TZLX2            1662  //透支利息2  DT：C  
#define  FID_RZJE2            1663  //融资金额2  DT：C  
#define  FID_QSJE_B2          1664  //回报买清算资金2  DT：C  
#define  FID_QSJE_S2          1665  //回报卖清算资金2  DT：C  
#define  FID_DRCKJE2          1666  //当日帐户存款金额2  DT：C  
#define  FID_TZCKJE2          1667  //当日通知存款金额2  DT：C  
#define  FID_HAVE_ZCXX        1668  //是否查询资产信息  DT：C  
#define  FID_HAVE_JKCS        1669  //是否查询融资监控参数  DT：C  
#define  FID_XZDJ             1670  //限制等级  DT：C  
#define  FID_CCBLXZ           1671  //持仓比例限制  DT：C  
#define  FID_ZHYJE            1672  //总合约金额  DT：C  
#define  FID_RZJEBL           1673  //融资金额比例  DT：C  
#define  FID_RZBLSX           1674  //融资金额比例限制  DT：C  
#define  FID_SHQD             1675  //业务审核起点  DT：R  
#define  FID_YWDM             1676  //业务代码  DT：C  
#define  FID_SHFS             1677  //审核方式  DT：I  
#define  FID_SHJS             1678  //审核级数  DT：I  
#define  FID_LCMC             1679  //流程名称  DT：C  
#define  FID_SHJB             1680  //审核级别  DT：I  
#define  FID_LGFH             1681  //临柜复核  DT：I  
#define  FID_HTSH             1682  //后台审核  DT：I  
#define  FID_LGFHJSDM         1683  //临柜复核角色代码  DT：C  
#define  FID_JKYWDM           1684  //接口业务代码  DT：C  
#define  FID_SFJYLX           1685  //身份校验类型  DT：I  
#define  FID_MBMC             1686  //模板名称  DT：C  
#define  FID_XSBQ             1687  //显示标签  DT：C  
#define  FID_FLLB             1688  //费率类别  DT：I  
#define  FID_GFBDFX           1689  //股份变动方向  DT：I  
#define  FID_GFJSTS           1690  //股份交收天数  DT：I  
#define  FID_JHJJKS           1691  //集合竞价开始时间  DT：C  
#define  FID_JHJJJS           1692  //集合竞价结束时间  DT：C  
#define  FID_JJJYBZ           1693  //净价交易标志  DT：I  
#define  FID_JSFBZ            1694  //经手费标志  DT：I  
#define  FID_JYZT             1695  //交易状态  DT：I  
#define  FID_JYKZ             1696  //交易控制  DT：I  
#define  FID_QSDZ             1697  //券商地址  DT：C  
#define  FID_SHSBBZ           1698  //上海申报标志  DT：C  
#define  FID_SZSBBZ           1699  //深圳申报标志  DT：C  
#define  FID_SZCDBZ           1700  //深圳撤单标志  DT：C  
#define  FID_SWKSSJ           1701  //上午开市时间  DT：C  
#define  FID_SWBSSJ           1702  //上午闭市时间  DT：C  
#define  FID_XWKSSJ           1703  //下午开市时间  DT：C  
#define  FID_XWBSSJ           1704  //下午闭市时间  DT：C  
#define  FID_SJXZ             1705  //时间限制  DT：I  
#define  FID_XJFS             1706  //限价方式  DT：I  
#define  FID_ZJBDFX           1707  //资金变动方向  DT：I  
#define  FID_ZJJSTS           1708  //资金交收天数  DT：I  
#define  FID_XGDM             1709  //相关代码  DT：C  
#define  FID_PCKZ             1710  //平仓控制  DT：C  
#define  FID_JSFS             1711  //结算方式  DT：C  
#define  FID_XWLX             1712  //席位类型  DT：C  
#define  FID_JGDW             1713  //价格单位  DT：C  
#define  FID_KCDW             1715  //开仓单位  DT：C  
#define  FID_JYBM             1716  //交易编码  DT：C  
#define  FID_TZLB             1717  //投资类别  DT：C  
#define  FID_KPBZ             1718  //开平标志  DT：C  
#define  FID_BZJ              1720  //保证金  DT：C  
#define  FID_JYSBZJ           1721  //交易所保证金  DT：C  
#define  FID_JGSL             1722  //交割数量  DT：C  
#define  FID_LJZK             1723  //累计折扣  DT：C  
#define  FID_FXL              1724  //风险率  DT：C  
#define  FID_ZFXL             1725  //昨风险率  DT：C  
#define  FID_PCSL             1726  //平仓数量  DT：C  
#define  FID_PCYK             1727  //平仓盈亏  DT：C  
#define  FID_PCRQ             1728  //平仓日期  DT：C  
#define  FID_CCWTH            1729  //持仓委托号  DT：C  
#define  FID_GSDM             1730  //公司代码  DT：C  
#define  FID_SBJLH            1731  //申报记录号  DT：C  
#define  FID_ZKFY             1732  //折扣费用  DT：C  
#define  FID_FHJE             1733  //返还金额  DT：C  
#define  FID_CJJJ             1734  //成交均价  DT：C  
#define  FID_DJBZJ            1735  //冻结保证金  DT：C  
#define  FID_SBGY             1736  //申报柜员  DT：C  
#define  FID_BDJE             1737  //变动金额  DT：C  
#define  FID_BCDJ             1738  //本次冻结  DT：C  
#define  FID_BDSJ             1739  //变动时间  DT：C  
#define  FID_CCSL             1740  //持仓数量  DT：C  
#define  FID_BZJBL            1741  //保证金比例  DT：C  
#define  FID_DWBZJ            1742  //单位保证金  DT：C  
#define  FID_JYSBZJBL         1743  //交易所保证金比例  DT：C  
#define  FID_JYSDWBZJ         1744  //交易所单位保证金  DT：C  
#define  FID_LJFH             1745  //累计返还  DT：C  
#define  FID_BRSXF            1746  //本日手续费  DT：C  
#define  FID_BRPCYK           1747  //本日平仓盈亏  DT：C  
#define  FID_JYSBRSXF         1748  //交易所本日手续费  DT：C  
#define  FID_JYSCCXE          1749  //交易所持仓限额  DT：C  
#define  FID_KHCCXE           1750  //客户持仓限额  DT：C  
#define  FID_SQCZBZ           1751  //授权操作标志  DT：I  
#define  FID_JSLB             1752  //角色类别  DT：I  
#define  FID_QXLB             1753  //用户权限类别  DT：I  
#define  FID_SHBZ             1754  //审核标志  DT：I  
#define  FID_AZRQ             1755  //安装日期  DT：I  
#define  FID_CPDM             1756  //产品代码  DT：C  
#define  FID_CPMC             1757  //产品名称  DT：C  
#define  FID_DFRQ             1758  //兑付日期  DT：I  
#define  FID_XLH              1759  //序列号  DT：C  
#define  FID_YXXG             1760  //是否允许修改  DT：I  
#define  FID_ZJSQRQ           1761  //最近授权日期  DT：I  
#define  FID_CWJB             1762  //错误级别  DT：I  
#define  FID_JKLB             1763  //接口类别  DT：I  
#define  FID_JKCWDM           1764  //接口错误代码  DT：C  
#define  FID_CWLY             1765  //错误来源  DT：C  
#define  FID_KSKHH            1766  //开始客户号  DT：C  
#define  FID_JSKHH            1767  //结束客户号  DT：C  
#define  FID_KYKHH            1768  //可用客户号  DT：C  
#define  FID_SJKZFS           1769  //时间控制方式  DT：I  
#define  FID_CPXM             1770  //产品项目  DT：C  
#define  FID_SJXZ_SQCZR       1771  //授权操作人时间期限限制  DT：I  
#define  FID_TBTS_SQCZR       1772  //授权操作人特别提示  DT：C  
#define  FID_ZJBH_SQCZR       1773  //授权操作人证件编号  DT：C  
#define  FID_ZJLB_SQCZR       1774  //授权操作人证件类别  DT：I  
#define  FID_JFQSRQ           1775  //积分起始日期  DT：I  
#define  FID_JGBZ             1776  //机构帐户标志  DT：I  
#define  FID_KHJF             1777  //客户积分  DT：I  
#define  FID_KHKH             1778  //客户卡号  DT：C  
#define  FID_MOBILE           1779  //移动电话  DT：C  
#define  FID_TZZFL            1780  //投资者分类  DT：I  
#define  FID_YXZZDM           1781  //营销组织代码  DT：C  
#define  FID_ZDXM             1782  //帐单项目  DT：C  
#define  FID_ZDZQ             1783  //帐单周期  DT：I  
#define  FID_ZJDLRQ           1784  //最近登录日期  DT：I  
#define  FID_SXDM             1785  //附加属性代码  DT：C  
#define  FID_LQRQ             1786  //领取日期  DT：I  
#define  FID_JGXZ             1787  //机构性质  DT：I  
#define  FID_ZJLB_FRDB        1788  //证件类别(法人代表)  DT：I  
#define  FID_ZJBH_FRDB        1789  //证件编号(法人代表)  DT：C  
#define  FID_ZCSJ             1790  //注册时间  DT：I  
#define  FID_KHJLXM           1791  //客户经理姓名  DT：C  
#define  FID_BZ1              1792  //币种1  DT：C  
#define  FID_BZ2              1793  //币种2  DT：C  
#define  FID_MBBH             1794  //模板编号  DT：I  
#define  FID_TABLE            1795  //数据表  DT：C  
#define  FID_ACT              1796  //动作  DT：I  
#define  FID_XZLB             1797  //限制类别  DT：I  
#define  FID_KHQZFW1          1798  //客户群组范围  DT：C  
#define  FID_SJYWKM           1799  //上级业务科目  DT：C  
#define  FID_SQL              1811  //  DT：C  
#define  FID_FILENAME         1812  //文件名称  DT：C  
#define  FID_FILEVERSION      1813  //文件版本  DT：C  
#define  FID_MEMHEAD          1815  //  DT：C  
#define  FID_MEMDATA          1816  //  DT：C  
#define  FID_MEMCOUNT         1817  //  DT：C  
#define  FID_MEMSIZE          1818  //  DT：C  
#define  FID_KQBZ             1850  //开启标志  DT：I  
#define  FID_KSSJ1            1851  //开始时间1  DT：C  
#define  FID_JSSJ1            1852  //结束时间1  DT：C  
#define  FID_KSSJ2            1853  //开始时间2  DT：C  
#define  FID_JSSJ2            1854  //结束时间2  DT：C  
#define  FID_WXWTFBZ          1855  //无效委托标志  DT：I  
#define  FID_MFCXCS           1856  //免费查询次数  DT：I  
#define  FID_KYXYED           1857  //可用信用额度  DT：R  
#define  FID_XYED             1858  //信用额度  DT：R  
#define  FID_BDKYZJ           1861  //外部帐户本地可用资金  DT：R  
#define  FID_SHLX             1862  //税后利息  DT：R  
#define  FID_JCFL             1863  //基础分类  DT：I  
#define  FID_YSJYBZ           1864  //夜市交易标志  DT：I  
#define  FID_YSKSSJ           1865  //夜市开市时间  DT：C  
#define  FID_YSBSSJ           1866  //夜市闭市时间  DT：C  
#define  FID_JYSSXF           1867  //交易所手续费  DT：R  
#define  FID_YJL              1868  //佣金率  DT：R  
#define  FID_YJLXX            1869  //最低佣金率  DT：R  
#define  FID_SLXX             1870  //数量下限  DT：I  
#define  FID_XMDM             1871  //项目代码  DT：C  
#define  FID_DAID             1872  //档案ID  DT：I  
#define  FID_INSINDEX         1873  //插入索引  DT：C  
#define  FID_MODINDEX         1874  //回写索引  DT：C  
#define  FID_LSHMC            1875  //流水号名称  DT：C  
#define  FID_TRANPORT         1876  //传输服务端口  DT：I  
#define  FID_DATABASE         1877  //数据库名  DT：C  
#define  FID_DUMPPATH         1878  //DUMP文件路径  DT：C  
#define  FID_DUMPTYPE         1879  //DUMP类型 0全备，1差备  DT：I  
#define  FID_DUMPNOTE         1880  //DUMP描述名  DT：C  
#define  FID_UPDATEITEM       1881  //UPDATE的字段名  DT：C  
#define  FID_IDENTITY         1882  //是否自增列  DT：I  
#define  FID_LOWPAR           1883  //LARGE_INTEGER型的低位部分  DT：C  
#define  FID_HIGHPAR          1884  //LARGE_INTEGER型的高位部分  DT：C  
#define  FID_CZYBH            1885  //操作员编号  DT：C  
#define  FID_KHDDH            1886  //开户点代号  DT：C  
#define  FID_QYLB             1887  //股东性质  DT：C  
#define  FID_TZRXM            1888  //投资人名称  DT：C  
#define  FID_TZRLB            1889  //投资人类别  DT：C  
#define  FID_JZJ              1890  //基准价类型  DT：I  
#define  FID_GDXM_OLD         1891  //原股东姓名/单位全称  DT：C  
#define  FID_QQLB             1892  //请求类别  DT：C  
#define  FID_GDJC             1893  //股东简称  DT：C  
#define  FID_GFXZ             1894  //股份性质  DT：C  
#define  FID_XJFSMC           1896  //限价方式名称  DT：C  
#define  FID_XJLB             1897  //限价类别  DT：I  
#define  FID_WTRQ             1898  //委托日期  DT：C  
#define  FID_DYBZ             1899  //打印标志  DT：C  
#define  FID_BZXX             1900  //备注信息  DT：C  
#define  FID_TZRMC            1901  //股东姓名/单位全称  DT：C  
#define  FID_ZJBH_OLD         1902  //原证件编号  DT：C  
#define  FID_TZRMC_OLD        1903  //原股东姓名/单位全称  DT：C  
#define  FID_SBBZ             1904  //申报标志  DT：C  
#define  FID_CLSM             1905  //处理说明  DT：C  
#define  FID_CXLB             1906  //查询类别  DT：C  
#define  FID_XWH              1907  //席位号  DT：C  
#define  FID_KHHY             1908  //开户会员  DT：C  
#define  FID_SXZ_OLD          1909  //原属性值  DT：C  
#define  FID_TZRJC            1910  //投资人简称  DT：C  
#define  FID_XB               1911  //性别  DT：C  
#define  FID_DMQZ             1912  //代码前缀  DT：C  
#define  FID_CSRQ             1913  //出生日期  DT：C  
#define  FID_MCQZ             1914  //名称前缀  DT：C  
#define  FID_FRLB             1915  //法人类别  DT：C  
#define  FID_HYDM             1916  //行业代码  DT：C  
#define  FID_JSHY             1917  //结算会员  DT：C  
#define  FID_SRGDH            1920  //受让方股东号  DT：C  
#define  FID_SRXWH            1921  //受让席位号  DT：C  
#define  FID_HBRQ             1922  //回报日期  DT：C  
#define  FID_YWLX             1923  //业务类型  DT：C  
#define  FID_WWMC             1924  //外文名称  DT：C  
#define  FID_TJSL             1925  //统计数量  DT：C  
#define  FID_SFJE             1926  //收费金额  DT：C  
#define  FID_DJGSSF           1927  //登记公司收费  DT：C  
#define  FID_DLJGSF           1928  //代理机构收费  DT：C  
#define  FID_SRZJBH           1929  //受让股东证件编号  DT：C  
#define  FID_SRGDXM           1930  //受让股东姓名  DT：C  
#define  FID_SRDZ             1931  //受让方通讯地址  DT：C  
#define  FID_SRDH             1932  //受让方电话号码  DT：C  
#define  FID_SRYZBM           1933  //受让方邮政编码  DT：C  
#define  FID_SRGFXZ           1934  //受让股份性质  DT：C  
#define  FID_DFXW             1935  //对方席位号  DT：C  
#define  FID_DFGDH            1936  //对方股东号  DT：C  
#define  FID_DFXZ             1937  //对方性质  DT：C  
#define  FID_QRBZ             1938  //确认标志  DT：C  
#define  FID_QQSL             1939  //确权数量  DT：I  
#define  FID_ZBQS             1940  //主办券商代码  DT：C  
#define  FID_GHSL             1941  //过户数量  DT：I  
#define  FID_DBQS             1942  //代码券商代码  DT：C  
#define  FID_GFXZSM           1943  //股份性质说明  DT：C  
#define  FID_QSLB             1944  //券商类别  DT：I  
#define  FID_QQSF_GR          1945  //个人确权收费  DT：C  
#define  FID_QQSF_JG          1946  //机构确权收费  DT：C  
#define  FID_ZBQSMC           1948  //主办券商名称  DT：C  
#define  FID_DYZQDM           1949  //抵押证券代码  DT：C  
#define  FID_GHTS             1950  //购回天数  DT：I  
#define  FID_JXTS             1951  //记息天数  DT：I  
#define  FID_CLEARFEE         1952  //结算费率  DT：R  
#define  FID_FXJJFL           1953  //风险基金费率  DT：R  
#define  FID_ZGFL             1954  //证管费率  DT：R  
#define  FID_QTFLXX           1955  //其它费用下限  DT：R  
#define  FID_QTFLSX           1956  //其它费用上限  DT：R  
#define  FID_CJJEQD           1957  //成交金额起点  DT：R  
#define  FID_SFQD             1958  //收费起点  DT：R  
#define  FID_FJYRBS           1959  //非交易日标识  DT：I  
#define  FID_YEAR             1960  //年份  DT：C  
#define  FID_MBLB             1961  //模板类别  DT：I  
#define  FID_ZJDS             1962  //资金底数  DT：R  
#define  FID_ZCDS             1963  //资产底数  DT：R  
#define  FID_DJYHZHS          1964  //单户允许登记的银行帐户数  DT：I  
#define  FID_FJXYXX           1965  //附加校验信息  DT：C  
#define  FID_FJYRZZBZ         1966  //非交易日是否允许转帐  DT：I  
#define  FID_GSKYE            1968  //公司卡余额  DT：R  
#define  FID_JCGSKYE          1969  //是否检查公司卡余额  DT：I  
#define  FID_LXCZCS           1970  //连续冲正次数  DT：I  
#define  FID_QDSJ             1971  //签到时间  DT：C  
#define  FID_QYZZBS           1972  //是否启用转帐标识  DT：I  
#define  FID_ZJQDRQ           1973  //最近签到日期  DT：I  
#define  FID_KSWTH            1974  //开始委托号  DT：C  
#define  FID_JSWTH            1975  //结束委托号  DT：C  
#define  FID_KYWTH            1976  //可用委托号  DT：C  
#define  FID_SQXQ             1977  //申请详情  DT：T  
#define  FID_SHZT             1978  //审核状态  DT：I  
#define  FID_SQGY             1979  //申请柜员  DT：C  
#define  FID_SQRQ             1980  //申请日期  DT：I  
#define  FID_SQSJ             1981  //申请时间  DT：C  
#define  FID_HKQX             1982  //还款期限(天)  DT：I  
#define  FID_MXTS             1983  //免息天数  DT：I  
#define  FID_YWSQH            1984  //业务申请号  DT：I  
#define  FID_SHRQ             1985  //审核日期  DT：I  
#define  FID_SHSJ             1986  //审核时间  DT：C  
#define  FID_YMBBH            1987  //原模板编号  DT：I  
#define  FID_XSXH             1988  //显示序号  DT：I  
#define  FID_MMTB             1989  //密码同步标志  DT：I  
#define  FID_EN_GYDM          1990  //允许柜员代码范围  DT：C  
#define  FID_KHYYB            1991  //客户所属营业部  DT：C  
#define  FID_NEWJJR           1992  //新经纪人  DT：C  
#define  FID_ZPKZBZ           1993  //支票余额控制标志  DT：I  
#define  FID_DRCKKZ           1994  //当日存款金额控制标志  DT：I  
#define  FID_QYDM             1996  //权益代码  DT：C  
#define  FID_QYSL             1997  //权益数量  DT：I  
#define  FID_QYJE             1998  //权益金额  DT：R  
#define  FID_JYRBS            1999  //交易日标识  DT：I  
#define  FID_TIME_INIT        2000  //初始化时间  DT：C  DT：YYYYMMDD HH:MM:SS  
#define  FID_TIME_CLOSE       2001  //收盘时间  DT：C  DT：YYYYMMDD HH:MM:SS  
#define  FID_LDSMJG           2003  //漏单扫描间隔  DT：I  
#define  FID_XCXZBS           2004  //写出限制笔数  DT：I  
#define  FID_OFSS_JZ          2100  //开放式基金净值  DT：R  
#define  FID_CZRBM            2502  //出质人编码  DT：C  
#define  FID_CZRMC            2503  //出质人名称  DT：C  
#define  FID_ZQRBM            2504  //质权人编码  DT：C  
#define  FID_ZQRMC            2505  //质权人名称  DT：C  
#define  FID_ZYHYBH           2506  //质押合约编号  DT：C  
#define  FID_DKHYBH           2507  //贷款合约编号  DT：C  
#define  FID_DKJE             2508  //贷款金额  DT：C  
#define  FID_DKDQRQ           2509  //贷款到期日  DT：C  
#define  FID_HTDJRQ           2510  //合同登记日期  DT：C  
#define  FID_WTZT             2514  //委托状态  DT：C  
#define  FID_ZYZH             2522  //质押帐号  DT：C  
#define  FID_DJBH             2523  //登记编号  DT：C  
#define  FID_SBLSH            2524  //申报流水号  DT：C  
#define  FID_FU_HYQY          2600  //期货合约权益  DT：R  
#define  FID_CZMM             3002  //操作密码  DT：C  
#define  FID_TXMM             3004  //通信密码  DT：C  
#define  FID_GTKHLB           3006  //柜台客户组织类别  DT：C  
#define  FID_ROUTER           3007  //路由营业部名称  DT：C  
#define  FID_ENTRY            3008  //配置文件变量名  DT：C  
#define  FID_KHHQZ            3009  //客户号前缀  DT：C  
#define  FID_CHECKBOX         3010  //CheckBox  DT：C  
#define  FID_CZLB             3011  //操作类别  DT：C  
#define  FID_ZJBL             3022  //资金比率  DT：C  
#define  FID_HQZD             3023  //行情涨跌  DT：C  
#define  FID_ZDF              3024  //行情涨跌幅  DT：R  
#define  FID_HQZF             3027  //行情振幅  DT：C  
#define  FID_HSL              3028  //换手率  DT：R  
#define  FID_ICON             3029  //图标号  DT：C  
#define  FID_FZDM             3030  //客户号所属分组代码  DT：C  
#define  FID_FZMC             3031  //客户号所属分组名称  DT：C  
#define  FID_HFBH             3032  //回复编号  DT：C  
#define  FID_FSKHH            3033  //发送客户号  DT：C  
#define  FID_FSFJBZ           3034  //发送客户号附加标志  DT：C  
#define  FID_DFKHH            3035  //对方客户代码  DT：C  
#define  FID_STATE            3036  //在线状态  DT：C  
#define  FID_DLSJ             3037  //登录时间  DT：C  
#define  FID_QTSJ             3038  //签退时间  DT：C  
#define  FID_MLLB             3040  //目录类别  DT：C  
#define  FID_MLBM             3041  //目录编码  DT：C  
#define  FID_MLMC             3042  //目录名称  DT：C  
#define  FID_MLSM             3043  //目录说明  DT：C  
#define  FID_FORMAT           3044  //目录格式  DT：C  
#define  FID_DEFAULT          3045  //缺省值  DT：C  
#define  FID_DATA             3046  //  DT：C  
#define  FID_JLBZ             3047  //记录标志,取值范围  DT：C  
#define  FID_MBSMS            3050  //对方手机号码  DT：C  
#define  FID_MBMAIL           3051  //对方邮件地址  DT：C  
#define  FID_LBMC             3053  //类别名称  DT：C  
#define  FID_CSLX             3054  //参数类型  DT：C  
#define  FID_XMMC             3060  //服务项目名称  DT：C  
#define  FID_FWXZ             3061  //服务项目限制  DT：C  
#define  FID_RECNO            3062  //结果记录号  DT：C  
#define  FID_YJB              3063  //邮件标题  DT：C  
#define  FID_CCMAIL           3064  //附送Mail地址  DT：C  
#define  FID_YJXM             3065  //预警项目  DT：C  
#define  FID_PRODUCT          3071  //产品名称  DT：C  
#define  FID_VERSION          3072  //产品版本号  DT：C  
#define  FID_COMPANY          3073  //公司名称  DT：C  
#define  FID_LWTIME           3075  //文件最近修改时间  DT：C  
#define  FID_PKLEN            3076  //文件数据包大小  DT：C  
#define  FID_PACKAGE          3077  //文件数据包  DT：C  
#define  FID_SUBDIR           3078  //文件子目录  DT：C  
#define  FID_ZHBZ             3081  //帐户标志  DT：C  
#define  FID_IBY              3082  //备用域...  DT：C  
#define  FID_BYDZ1            3085  //备用地址1  DT：C  
#define  FID_BYDZ2            3086  //备用地址2  DT：C  
#define  FID_BYDZ3            3087  //备用地址3  DT：C  
#define  FID_XH               3090  //序号  DT：C  
#define  FID_DXFDED           3107  //短信封顶额度  DT：C  
#define  FID_YJFDED           3108  //邮件封顶额度  DT：C  
#define  FID_DXFYBL           3109  //短信费用比率  DT：C  
#define  FID_YJFYBL           3110  //邮件费用比率  DT：C  
#define  FID_YXSJ             3113  //有效时间  DT：C  
#define  FID_LYLX             3114  //留言类型  DT：C  
#define  FID_KTZT             3115  //开通状态  DT：I  
#define  FID_YWSM             3116  //业务说明  DT：C  
#define  FID_WTFSDM           3200  //委托方式代码  DT：C  
#define  FID_PRIVATEKEY       3202  //私钥保护密码  DT：C  
#define  FID_DEVICENAME       3800  //物理设备名称  DT：C  
#define  FID_DATANAME         3801  //数据库名称  DT：C  
#define  FID_DATASIZE         3802  //数据库大小  DT：C  
#define  FID_DEVICESIZE       3803  //设备大小  DT：C  
#define  FID_YJBCDM           3804  //原件保存代码  DT：C  
#define  FID_QZYZFS           3900  //强制验证方式  DT：C  
#define  FID_KXYZFS           3901  //可选验证方式  DT：C  
#define  FID_YZFS_SQCZR       3902  //授权操作人验证方式  DT：C  
#define  FID_ID               9001  //档案编号  DT：C  
#define  FID_MODULE           9002  //模块名  DT：C  
#define  FID_NAME             9003  //模块文件  DT：C  
#define  FID_CLASE            9004  //模块分类  DT：C  
#define  FID_LEVEL            9006  //优先级别  DT：C  
#define  FID_OLDVER           9007  //旧版本  DT：C  
#define  FID_LASTVER          9008  //新版本  DT：C  
#define  FID_WHYS             9009  //修改原因  DT：C  
#define  FID_FILEPATH         9011  //文件路径  DT：C  
#define  FID_MENDER           9012  //修改人  DT：C  
#define  FID_CONNER           9013  //测试人  DT：C  
#define  FID_MODIRQ           9014  //修改日期  DT：C  
#define  FID_TESTRQ           9015  //测试日期  DT：C  
#define  FID_CONCLUSION       9016  //测试结论  DT：C  
#define  FID_UPNOTE           9017  //升级提示  DT：C  
#define  FID_STATUS           9018  //记录状态  DT：C  
#define  FID_RESOLVEN         9019  //关联提示  DT：C  
#define  FID_FLAG             9020  //档案类型  DT：C  
#define  FID_DIFF             9021  //难度  DT：C  
#define  FID_YZJB             9022  //严重级别  DT：C  
#define  FID_RWBH             9023  //任务编号  DT：C  
#define  FID_DIR              9024  //运行目录  DT：C  
#define  FID_CPID             9025  //产品ID  DT：C  
#define  FID_XQLX             9026  //需求类型  DT：C  
#define  FID_XQLY             9027  //需求来源  DT：C  
#define  FID_XQMS             9028  //需求描述  DT：C  
#define  FID_XQLXF            9029  //需求联系方  DT：C  
#define  FID_XGFA             9030  //修改方案  DT：C  
#define  FID_FASP             9031  //方案审批人  DT：C  
#define  FID_FASPRQ           9032  //方案审批日期  DT：C  
#define  FID_JRX              9033  //兼容性  DT：C  
#define  FID_FFJH             9034  //发放计划  DT：C  
#define  FID_XGR              9035  //修改人  DT：C  
#define  FID_XGJD             9036  //修改进度  DT：C  
#define  FID_CSR              9037  //测试人  DT：C  
#define  FID_CSJD             9038  //测试进度  DT：C  
#define  FID_YMTS             9039  //源码提示  DT：C  
#define  FID_XGFK             9040  //修改反馈  DT：C  
#define  FID_XGWCRQ           9041  //修改完成日期  DT：C  
#define  FID_CSFK             9042  //测试反馈  DT：C  
#define  FID_CSWCRQ           9043  //测试完成日期  DT：C  
#define  FID_HFYJ             9044  //回访意见  DT：C  
#define  FID_HFRQ             9045  //回访日期  DT：C  
#define  FID_HFR              9046  //回访人  DT：C  
#define  FID_RWB              9047  //任务标题  DT：C  
#define  FID_MTYPE            9048  //类型  DT：C  
#define  FID_GYXM             9049  //柜员姓名  DT：C  
#define  FID_JLZ              9050  //记录状态  DT：C  
#define  FID_GH               9051  //工号  DT：C  
#define  FID_KHMC             9052  //客户名称  DT：C  
#define  FID_WHR              9053  //维护人  DT：C  
#define  FID_XZR              9054  //协助人  DT：C  
#define  FID_KHID             9055  //客户ID  DT：C  
#define  FID_WTMS             9056  //问题描述  DT：C  
#define  FID_FXDW             9057  //分析定位  DT：C  
#define  FID_JJFS             9058  //解决方式  DT：C  
#define  FID_JJGC             9059  //解决过程  DT：C  
#define  FID_CLYS             9060  //处理用时  DT：C  
#define  FID_FSRQ             9061  //发生日期  DT：C  
#define  FID_WTFL             9062  //  DT：C  
#define  FID_GROUP            9063  //组  DT：C  
#define  FID_SECURITY         9064  //  DT：C  
#define  FID_GXQ              9065  //  DT：C  
#define  FID_CPXX             9066  //产品信息  DT：C  
#define  FID_KHZB             9067  //客户总部  DT：C  
#define  FID_QYZB             9068  //  DT：C  
#define  FID_ZJL              9070  //总经理  DT：C  
#define  FID_ZJLDH            9071  //总经理电话  DT：C  
#define  FID_ZJLMAIL          9072  //总经理EMAIL  DT：C  
#define  FID_JL               9073  //经理  DT：C  
#define  FID_JLDH             9074  //经理电话  DT：C  
#define  FID_JLMAIL           9075  //经理EMAIL  DT：C  
#define  FID_LXR              9076  //联系人  DT：C  
#define  FID_CRDATE           9077  //创建日期  DT：C  
#define  FID_ENGINEERS        9078  //工程师  DT：C  
#define  FID_HFRQ2            9079  //恢复日期2  DT：C  
#define  FID_DJR              9080  //登记人  DT：C  
#define  FID_ZW               9081  //职位  DT：C  
#define  FID_WHBT             9082  //维护标题  DT：C  
#define  FID_FLAG1            9083  //FLAG1  DT：C  
#define  FID_FLAG2            9084  //FLAG2  DT：C  
#define  FID_XQLB             9085  //需求类别  DT：C  
#define  FID_MSN              9086  //MSN  DT：C  
#define  FID_SPFA             9087  //审批方案  DT：C  
#define  FID_SJNR             9088  //涉及内容  DT：C  
#define  FID_LCRZ             9089  //流程日志  DT：C  
#define  FID_SPR              9090  //审批人  DT：C  
#define  FID_SPRQ             9091  //审批日期  DT：C  
#define  FID_GZR              9092  //跟踪人  DT：C  
#define  FID_XQBT             9093  //需求标题  DT：C  
#define  FID_ZPR              9094  //指派人  DT：C  
#define  FID_RWMS             9095  //任务描述  DT：C  
#define  FID_ZPRQ             9096  //指派日期  DT：C  
#define  FID_GMZQ             9097  //更密周期  DT：N  
#define  FID_GYZT             9098  //柜员状态  DT：N  
#define  FID_KSSJ             9099  //开始时间  DT：C  
#define  FID_JSSJ             9100  //结束时间  DT：C  
#define  FID_XZQYDM           9101  //行政区域代码  DT：C  
#define  FID_XZQYMC           9102  //行政区域名称  DT：C  
#define  FID_XZQYLB           9103  //行政区域类别  DT：C  
#define  FID_SJXZQYDM         9104  //上级行政区域代码  DT：C  
#define  FID_DHQH             9105  //电话区号  DT：N  
#define  FID_KHBS             9106  //客户标识  DT：C  
#define  FID_KHZT             9107  //客户状态  DT：N  
#define  FID_ZT               9108  //状态  DT：N  
#define  FID_SORTTYPE         9109  //数据查询的排序方式  DT：I  
#define  FID_ROWCOUNT         9110  //行号  DT：N  
#define  FID_LPDM             9111  //礼品代码  DT：C  
#define  FID_YWSHBZ           9112  //业务审核标志  DT：N  
#define  FID_YWSHQD           9113  //是否需业务审核  DT：N  
#define  FID_LPZCDS           9114  //礼品转存底数  DT：R  
#define  FID_SRJE             9115  //收入金额  DT：R  
#define  FID_FCJE             9116  //付出金额  DT：R  
#define  FID_CZBZ             9117  //冲销标志  DT：C  
#define  FID_JSJ              9118  //结算价  DT：R  
#define  FID_YSJE_YJ          9119  //一级应收金额  DT：R  
#define  FID_JGZLLB           9120  //交割资料类别  DT：N  
#define  FID_S11              9121  //一级经手费  DT：R  
#define  FID_S12              9122  //一级证管费  DT：R  
#define  FID_S13              9123  //一级过户费  DT：R  
#define  FID_S15              9124  //一级结算费  DT：R  
#define  FID_S16              9125  //一级风险基金  DT：R  
#define  FID_BZQDM            9126  //标准券代码  DT：C  
#define  FID_CWLB             9127  //错误类别  DT：C  
#define  FID_YHMM             9128  //银行密码  DT：C  
#define  FID_JJDM             9129  //基金代码  DT：C  
#define  FID_JJMC             9130  //基金名称  DT：C  
#define  FID_XJTDBL           9131  //现金替代比例  DT：R  
#define  FID_WTJE             9132  //委托金额  DT：R  
#define  FID_SGSHZT           9133  //申购赎回状态  DT：N  
#define  FID_TDBZ             9134  //替代标志  DT：N  
#define  FID_ZDXW             9135  //指定席位  DT：C  
#define  FID_XJCE             9136  //现金差额  DT：R  
#define  FID_TDJE             9137  //替代金额（现金替代金额）  DT：R  
#define  FID_DWJZ             9138  //单位净值  DT：R  
#define  FID_SGSHDWJZ         9139  //申购赎回单位净值  DT：R  
#define  FID_CZGY             9140  //操作柜员  DT：C  
#define  FID_FYBL             9141  //费用比例  DT：R  
#define  FID_JYL              9142  //交易量  DT：R  
#define  FID_XGPZ             9143  //相关品种  DT：C  
#define  FID_XGZH             9144  //相关帐号  DT：C  
#define  FID_JESX             9145  //金额上限  DT：R  
#define  FID_FSSL             9146  //发生数量  DT：N  
#define  FID_LXJE             9147  //利息金额  DT：R  
#define  FID_LXJG             9148  //利息价格  DT：R  
#define  FID_PLWTPCH          9149  //批量委托批次号  DT：N  
#define  FID_SEQNO            9150  //历史数据序列号  DT：N  
#define  FID_SBXW             9151  //申报席位  DT：C  
#define  FID_XWDM             9152  //席位代码  DT：C  
#define  FID_EN_JGZLLB        9153  //交割资料类别范围  DT：C  
#define  FID_WBJGDM           9154  //外部机构代码  DT：C  
#define  FID_WBFJJYLB         9155  //外部附加交易类别（业务类别）  DT：N  
#define  FID_CZWBSQBH         9156  //冲正外部申请编号  DT：C  
#define  FID_WBSQBH           9157  //外部申请编号  DT：C  
#define  FID_RWSM             9158  //任务说明  DT：C  
#define  FID_FHRQ             9159  //复核日期  DT：I  
#define  FID_FLLBFW           9160  //费率类别范围  DT：C  
#define  FID_JYSFW            9161  //交易所范围  DT：C  
#define  FID_SDSL             9162  //所得税率  DT：R  
#define  FID_WTFSFW           9163  //委托方式范围  DT：I  
#define  FID_ZDJSBZ           9164  //自动计算标志  DT：I  
#define  FID_JYLBFW           9165  //交易类别范围  DT：C  
#define  FID_TJZQS            9166  //统计周期数  DT：I  
#define  FID_JJZHCD           9167  //基金帐号长度  DT：I  
#define  FID_JYMCD            9168  //交易码长度  DT：I  
#define  FID_SQBHCD           9169  //申请编号长度  DT：I  
#define  FID_JJZHSCFS         9170  //基金帐号生成方式  DT：I  
#define  FID_JYZHQZ           9171  //交易帐号前缀  DT：C  
#define  FID_KYJJZH_GR        9172  //可用基金帐号_个人  DT：C  
#define  FID_KYJJZH_JG        9173  //可用基金帐号_机构  DT：C  
#define  FID_FEDZZT           9174  //份额对帐状态  DT：I  
#define  FID_HBDZBZ           9175  //合并对帐标志  DT：I  
#define  FID_FEDZXM           9176  //份额对帐项目  DT：I  
#define  FID_FEDZFS           9177  //份额对帐方式  DT：I  
#define  FID_FJYGHLB          9178  //代理非交易过户类别  DT：I  
#define  FID_JGSCRGZDZJ       9179  //机构首次认购最低金额  DT：R  
#define  FID_GRSCRGZDZJ       9180  //个人首次认购最低金额  DT：R  
#define  FID_GRZJRGZDZJ       9181  //个人追加认购最低金额  DT：R  
#define  FID_JGZJRGZDZJ       9182  //机构追加认购最低金额  DT：R  
#define  FID_GRZDRGZJ         9183  //个人最低认购金额  DT：R  
#define  FID_JGZDRGZJ         9184  //机构最低认购金额  DT：R  
#define  FID_GRSCSGZDZJ       9185  //个人首次申购最低金额  DT：R  
#define  FID_JGSCSGZDZJ       9186  //机构首次申购最低金额  DT：R  
#define  FID_GRZJSGZDZJ       9187  //个人追加申购最低金额  DT：R  
#define  FID_JGZJSGZDZJ       9188  //机构追加申购最低金额  DT：R  
#define  FID_GRSHZDFE         9189  //个人赎回最低份额  DT：R  
#define  FID_JGSHZDFE         9190  //机构赎回最低份额  DT：R  
#define  FID_GRCCZDFE         9191  //个人持仓最低限额  DT：R  
#define  FID_JGCCZDFE         9192  //机构持仓最低限额  DT：R  
#define  FID_SHSYTS           9193  //赎回顺延天数  DT：I  
#define  FID_HLHKTS           9194  //红利到款延迟天数  DT：I  
#define  FID_GRZDZHFE         9195  //个人最低转换份额  DT：R  
#define  FID_JGZDZHFE         9196  //机构最低转换份额  DT：R  
#define  FID_SGJSBZ           9197  //手工结算标志  DT：I  
#define  FID_TADM             9198  //基金公司代码  DT：C  
#define  FID_FQRDM            9199  //发起人代码  DT：C  
#define  FID_FXZFE            9200  //发行总份额  DT：R  
#define  FID_GLRDM            9201  //管理人代码  DT：C  
#define  FID_JJJC             9202  //基金简称  DT：C  
#define  FID_JJQC             9203  //基金全称  DT：C  
#define  FID_JJJZ             9204  //基金净值  DT：R  
#define  FID_DFJJDM           9205  //对方基金代码  DT：C  
#define  FID_RGFS             9206  //认购方式  DT：I  
#define  FID_RGJS             9207  //认购基数  DT：I  
#define  FID_SGJS             9208  //申购基数  DT：I  
#define  FID_TGRDM            9209  //托管人代码  DT：C  
#define  FID_TDLX             9210  //通道类型  DT：I  
#define  FID_MODULE_ID        9211  //模块ID  DT：I  
#define  FID_ZKLX             9212  //折扣类型  DT：I  
#define  FID_ZKL              9213  //折扣率  DT：R  
#define  FID_DXBS             9214  //对象标识  DT：C  
#define  FID_DXLB             9215  //对象类别  DT：I  
#define  FID_BDKHBZ           9216  //本地开户标志  DT：I  
#define  FID_DJYY             9217  //冻结原因  DT：I  
#define  FID_DZDLB            9219  //对帐单类别  DT：I  
#define  FID_YSQBH            9225  //原申请编号  DT：C  
#define  FID_ZJJSLX           9226  //资金结算类型  DT：I  
#define  FID_YYRQ             9227  //预约日期  DT：I  
#define  FID_JJZH             9228  //基金帐号  DT：C  
#define  FID_JJZHXM           9229  //基金帐户姓名  DT：C  
#define  FID_XSDM             9231  //销售代码  DT：C  
#define  FID_TJDM             9232  //统计代码  DT：C  
#define  FID_JJFL             9233  //基金分类  DT：I  
#define  FID_GRZGCYBL         9234  //个人最高持有比例  DT：R  
#define  FID_JGZGCYBL         9235  //机构最高持有比例  DT：R  
#define  FID_MRFS             9236  //认购申购默认方式  DT：I  
#define  FID_KYSL             9237  //可用数量  DT：I  
#define  FID_WTFE             9238  //委托份额  DT：R  
#define  FID_YYBZ             9239  //预约标志  DT：I  
#define  FID_ORDERS           9240  //订单顺序号  DT：I  
#define  FID_DFJJZH           9241  //对方基金帐号  DT：C  
#define  FID_DFJYZH           9242  //对方交易帐号  DT：C  
#define  FID_DFWDH            9243  //对方网点号  DT：C  
#define  FID_DFXSSDM          9244  //对方销售商代码  DT：C  
#define  FID_PZDM             9245  //品种代码  DT：C  
#define  FID_DFSFFS           9246  //对方收费方式  DT：I  
#define  FID_GHYY             9247  //过户原因  DT：I  
#define  FID_JJSL             9248  //基金份额数量  DT：R  
#define  FID_LJWYCS           9249  //累计违约次数  DT：I  
#define  FID_LXWYCS           9250  //连续违约次数  DT：I  
#define  FID_QQCS             9251  //欠缺次数  DT：I  
#define  FID_SCKKRQ           9252  //上次扣款日期  DT：I  
#define  FID_MYKKRQ           9253  //每月扣款日期  DT：I  
#define  FID_SGJE             9254  //申购金额  DT：16  DT：2  
#define  FID_SCSGZDJE         9255  //首次申购最低金额  DT：R  
#define  FID_SGZDJE           9256  //申购最低金额  DT：R  
#define  FID_CONTENT          9257  //内容  DT：C  
#define  FID_TITLE            9258  //标题  DT：C  
#define  FID_GGLB             9259  //公告类别  DT：I  
#define  FID_JYZH             9260  //交易帐号  DT：C  
#define  FID_TALSH            9261  //TA流水号  DT：C  
#define  FID_QRRQ             9262  //确认日期  DT：I  
#define  FID_QRFE             9263  //确认份额  DT：R  
#define  FID_QRJE             9264  //确认金额  DT：R  
#define  FID_DLF              9265  //代理非  DT：R  
#define  FID_SHF              9266  //赎回费  DT：R  
#define  FID_YHS              9267  //印花税  DT：R  
#define  FID_ZFE              9268  //总份额  DT：R  
#define  FID_DJFE             9269  //冻结份额  DT：R  
#define  FID_KYFE             9270  //可用份额  DT：R  
#define  FID_DZRQ             9271  //到帐日期  DT：I  
#define  FID_SFHLJE           9272  //实际红利金额  DT：R  
#define  FID_HLZJE            9273  //红利总金额  DT：R  
#define  FID_FHDWFE           9274  //分红单位份额  DT：R  
#define  FID_FHJJFE           9275  //分红基金份额  DT：R  
#define  FID_ZTZFE            9276  //再投资份额  DT：R  
#define  FID_DJZTZFE          9277  //冻结再投资份额  DT：R  
#define  FID_YJKBFS           9278  //佣金捆绑方式  DT：I  
#define  FID_ZFHJE            9279  //总返还金额  DT：R  
#define  FID_HGRQFCJE         9280  //回购融券付出金额  DT：R  
#define  FID_HGRZSRJE         9281  //回购融资收入金额  DT：R  
#define  FID_RQGHSRJE         9282  //融券购回收入金额  DT：R  
#define  FID_RZGHFCJE         9283  //融资购回付出金额  DT：R  
#define  FID_HLPFJE           9284  //红利派发金额  DT：R  
#define  FID_PGJKJE           9285  //配股缴款金额  DT：R  
#define  FID_SGZQFC           9286  //申购中签付出  DT：R  
#define  FID_ZQDFJE           9287  //债券兑付金额  DT：R  
#define  FID_ZZLGJE           9288  //转债零股资金  DT：R  
#define  FID_BYSJE            9289  //买应收金额  DT：R  
#define  FID_SGKFC            9290  //申购款付出金额  DT：R  
#define  FID_SGKSR            9291  //申购款收入金额  DT：R  
#define  FID_SYSJE            9292  //卖应收金额  DT：R  
#define  FID_WGHZJ            9293  //未购回资金  DT：R  
#define  FID_WHSGK            9294  //未回申购款  DT：R  
#define  FID_BRKHSL           9295  //本日开户数量  DT：I  
#define  FID_BRXHSL           9296  //本日销户数量  DT：I  
#define  FID_MCDXSL           9297  //卖出抵消数量  DT：I  
#define  FID_MRDXSL           9298  //买入抵消数量  DT：I  
#define  FID_SGCJSL           9299  //申购成交数量  DT：I  
#define  FID_SHCJSL           9300  //赎回成交数量  DT：I  
#define  FID_FEMXLSH          9301  //份额明细流水号  
#define  FID_FEDZRQ           9302  //份额到帐日期  
#define  FID_ZJDZRQ           9303  //资金到帐日期  
#define  FID_FHDJFE           9304  //分红冻结份额  
#define  FID_FEYE             9305  //份额余额  
#define  FID_RGJE             9306  //认购金额  DT：R22.2  
#define  FID_SHFE             9307  //赎回份额  
#define  FID_BRBD             9308  //本日变动金额  DT：R  
#define  FID_XYJKFSJE         9309  //信用借款发生金额  DT：R  
#define  FID_XYJKYE           9310  //信用借款余额  DT：R  

#define  FUN_MSG_PARAM_CHANGE            100001  //基本参数变化消息
#define  FUN_MSG_YWSH                    100002  //后台审核业务申请
#define  FUN_MSG_YWSH_ANSWER             100003  //业务审核应答
#define  FUN_MSG_ENTRUST_TRADE           100004  //股票买卖委托成功
#define  FUN_MSG_ENTRUST_BARGAINON       100005  //股票委托成交消息
#define  FUN_MSG_ZJJYSQ_ZQ               100006  //证券发起资金交易申请成功
#define  FUN_MSG_ZJJYSQ_ZQ_PROC          100007  //证券发起资金交易处理
#define  FUN_MSG_ADD_JSTZ                100008  //新发布即时通知信息
#define  FUN_MSG_ADD_JYBW                100009  //新发布交易备忘信息
#define  FUN_PARAM_LIST_MYYWXZ           101001  //查询营业部漫游业务限制
#define  FUN_PARAM_GET_CURTIME           101002  //取基础库当前时间
#define  FUN_PARAM_LIST_JGLB             101003  //获取机构类别定义参数
#define  FUN_PARAM_LIST_DATADICT         101004  //获取系统数据字典
#define  FUN_PARAM_LIST_BZ               101005  //获取币种参数
#define  FUN_PARAM_LIST_BRANCH           101006  //获取营业部参数
#define  FUN_PARAM_GET_XTCS              101007  //查询某一系统参数配置
#define  FUN_PARAM_LIST_ACCOUNTITEM      101008  //获取所有业务科目参数
#define  FUN_PARAM_LIST_DATADICT_BYNAME  101009  //根据数据字典名称取数据字典
#define  FUN_PARAM_LIST_MENU             101010  //获取所有菜单
#define  FUN_PARAM_LIST_CARDINFO         101013  //获取各类卡参数
#define  FUN_PARAM_LIST_NATIONCODE       101014  //获取所有国籍代码对照
#define  FUN_PARAM_LIST_XZQYDM           101016  //获取所有行政区域代码参数
#define  FUN_PARAM_LIST_SJCS             101017  //(删除)查询所有审计参数
#define  FUN_PARAM_LIST_CWDMDZ           101018  //查询内部错误代码对照
#define  FUN_PARAM_GET_XTZT              101020  //查询系统状态
#define  FUN_PARAM_GET_EXTENDBRANCHINFO  101021  //查询扩展营业部信息
#define  FUN_PARAM_LIST_XTCS             101025  //查询杂项参数信息
#define  FUN_PARAM_LIST_SFYZCS           101026  //查询营业部身份验证参数
#define  FUN_PARAM_RSQL                  101027  //远端SQL执行
#define  FUN_PARAM_LIST_BROKER           101031  //查询经纪人信息
#define  FUN_PARAM_LIST_KHJLXX           101032  //查询客户经理信息
#define  FUN_PARAM_LIST_PRODUCT          101033  //查询系统产品清单
#define  FUN_PARAM_LIST_AUTHSERIALNO     101034  //查询产品授权序列号
#define  FUN_PARAM_LIST_WBCWDMDZ         101035  //查询外部错误代码对照
#define  FUN_PARAM_LIST_JUNIORBROKER     101036  //查询下级经纪人
#define  FUN_PARAM_LIST_USEROPRIGHTS     101101  //获取用户菜单操作权限
#define  FUN_PARAM_GET_USERINFO          101102  //获取指定柜员基本信息
#define  FUN_PARAM_LIST_USERBASERIGHT    101103  //获取指定用户基本权限
#define  FUN_PARAM_LIST_USERINFO         101104  //获取指定营业部范围的所有柜员基本信息
#define  FUN_PARAM_LIST_INHERITRIGHTS    101105  //获取用户继承权限
#define  FUN_PARAM_LIST_ORIGINALRIGHTS   101106  //获取用户直接菜单权限
#define  FUN_PARAM_LIST_USERBYMENUID     101108  //查询指定菜单已授权的所有用户信息
#define  FUN_PARAM_LIST_USEREXKHH        101109  //查询柜员屏蔽客户
#define  FUN_PARAM_LIST_USERROLE         101110  //查询用户角色
#define  FUN_PARAM_LIST_ROLEINFO         101111  //查询角色信息
#define  FUN_PARAM_LIST_DLZT             101112  //查询用户登录状态
#define  FUN_PARAM_GET_EXTBRANCH_PARAM   101113  //查询用户参数管理的允许营业部范围
#define  FUN_PARAM_GET_EXTBRANCH_USER    101114  //查询用户管理用户允许的营业部范围
#define  FUN_PARAM_LIST_LOGINID          101115  //获取所有指定营业部的登录用户
#define  FUN_PARAM_LIST_ROLERIGHTS       101116  //查询角色的菜单操作权限
#define  FUN_PARAM_LIST_ROLERIGHTROLE    101117  //查询角色对角色的权限
#define  FUN_PARAM_LIST_WSHQXGYDM        101118  //查询有未审核权限的柜员列表
#define  FUN_PARAM_LIST_WSHQXJJR         101119  //查询有未审核权限的经纪人列表
#define  FUN_PARAM_LIST_WSHQXKHJL        101120  //查询有未审核权限的客户经理列表
#define  FUN_PARAM_LIST_CDCZDXXZ         101121  //查询指定用户菜单操作对象限制
#define  FUN_PARAM_LIST_WSHQXROLE        101122  //查询有未审核权限的角色信息
#define  FUN_PARAM_LIST_CDHCGX           101123  //查询指定菜单权限互斥的菜单
#define  FUN_PARAM_LIST_JSHCGX           101124  //查询指定角色权限互斥的角色
#define  FUN_PARAM_GET_CDCZQX            101125  //查询用户在指定菜单的操作权限
#define  FUN_PARAM_LIST_QXHC_LOGINID     101126  //查询所有拥有互斥菜单权限的用户
#define  FUN_PARAM_LIST_JSHC_LOGINID     101127  //查询所有拥有互斥角色的用户
#define  FUN_PARAM_LIST_ROLEBYMENUID     101128  //查询指定菜单已授权的所有用户信息
#define  FUN_PARAM_LIST_JSTZ             101150  //查询即时通知信息
#define  FUN_PARAM_LIST_JSTZ_OWNER       101151  //按发布者查询即时通知
#define  FUN_PARAM_LIST_JYBW             101152  //查询交易备忘信息
#define  FUN_PARAM_LIST_JYBW_OWNER       101153  //按发布者查询交易备忘信息
#define  FUN_PARAM_LIST_JYBWDY           101154  //查询当前用户的交易备忘订阅情况
#define  FUN_PARAM_LIST_YWSHGL           101201  //查询业务审核管理参数
#define  FUN_PARAM_LIST_YWSHLCBYCSID     101202  //根据参数编码查询业务审核流程
#define  FUN_PARAM_LIST_SHXMQD           101203  //查询审核项目清单
#define  FUN_PARAM_LIST_JSYWSHQX         101204  //查询角色业务审核权限
#define  FUN_PARAM_LIST_GYYWSHQX         101205  //查询柜员业务审核权限
#define  FUN_PARAM_LIST_YWSHMB           101206  //查询业务审核模板
#define  FUN_PARAM_GET_YWSHCS            101207  //查询指定业务审核参数
#define  FUN_PARAM_LIST_YWSHSQ           101208  //查询业务审核申请流水
#define  FUN_PARAM_LIST_YWSHMX           101209  //查询业务审核明细
#define  FUN_PARAM_LIST_YWSHLCBYSQH      101210  //通过业务申请号查询审核流程信息
#define  FUN_PARAM_LIST_YWSHLC           101211  //查询所有业务审核流程
#define  FUN_PARAM_LIST_YWSHZP_SQR       101212  //查询用户已指派出去的业务审核权限
#define  FUN_PARAM_LIST_YWSHZP_BSQR      101213  //查询用户通过其它用户指派而拥有的审核权限
#define  FUN_PARAM_LIST_YWSHKHFW         101214  //查询业务审核客户范围定义
#define  FUN_PARAM_LIST_QSZJFP           101301  //查询清算主机分配
#define  FUN_PARAM_LIST_QSZLPZ           101302  //查询清算资料文件
#define  FUN_PARAM_LIST_QSRWKZ           101303  //查询清算任务
#define  FUN_PARAM_LIST_QSRWWJ           101304  //查询清算任务文件
#define  FUN_PARAM_LIST_QSRWZT           101305  //查询清算任务状态
#define  FUN_PARAM_SYNC_TABLE            101901  //参数表同步
#define  FUN_CUSTOM_LIST_KHQZ            102001  //获取所有客户群组参数
#define  FUN_CUSTOM_LIST_KHSXLB          102002  //获取所有客户属性类别定义
#define  FUN_IS_MY_CUSTOM                102003  //检查是否本柜员可操作的客户
#define  FUN_CUSTOM_LIST_GIFTINFO        102004  //查询礼品参数
#define  FUN_CUSTOM_LIST_GSFL            102005  //查询所有公司分类参数
#define  FUN_CUSTOM_LIST_KHHRES          102006  //查询客户号资源参数
#define  FUN_CUSTOM_LIST_ZYXZCS          102007  //查询资源限制参数
#define  FUN_CUSTOM_GET_SFBZ_ZYXZ        102008  //查询资源限制收费标准
#define  FUN_CUSTOM_LIST_KHFLFS          102009  //查询客户分类方式
#define  FUN_CUSTOM_LIST_KZYWDM          102010  //查询系统扩展业务代码定义
#define  FUN_CUSTOM_LIST_KZYWCS          102011  //查询扩展业务参数定义
#define  FUN_CUSTOM_SYNC_TABLE           102901  //参数表同步
#define  FUN_ACCOUNT_LIST_HLCS           103001  //查询汇率参数
#define  FUN_ACCOUNT_LIST_LLMB           103002  //查询利率模板
#define  FUN_ACCOUNT_LIST_LLMBCS         103003  //查询利率模板参数
#define  FUN_ACCOUNT_LIST_QKKZMB         103004  //查询取款底数控制模板
#define  FUN_ACCOUNT_LIST_QKKZMBCS       103005  //查询取款底数控制模板参数
#define  FUN_ACCOUNT_LIST_QZCQED         103006  //查询群组存取额度
#define  FUN_ACCOUNT_LIST_XYED_YYB       103007  //查询营业部信用额度参数
#define  FUN_ACCOUNT_LIST_XYED_JJR       103008  //查询经纪人信用额度参数
#define  FUN_ACCOUNT_LIST_XYED_ZH        103009  //查询资金帐户信用额度参数
#define  FUN_ACCOUNT_LIST_WBJGDM         103010  //获取所有外部机构代码
#define  FUN_ACCOUNT_LIST_YZZZCS         103011  //查询银证转帐参数
#define  FUN_ACCOUNT_LIST_YZTCS          103012  //查询银证通参数
#define  FUN_ACCOUNT_LIST_WBYWCS         103013  //查询外部业务参数对照
#define  FUN_ACCOUNT_LIST_LLMBDX_KH      103101  //查询客户使用的利率模板对象
#define  FUN_ACCOUNT_LIST_LLMBDX_QZ      103102  //查询指定群组使用的利率模板对象
#define  FUN_ACCOUNT_LIST_QKKZMBDX_KH    103103  //查询客户使用的取款控制模板对象
#define  FUN_ACCOUNT_LIST_QKKZMBDX_QZ    103104  //查询指定群组使用的取款控制模板对象
#define  FUN_ACCOUNT_LIST_LXFHGL_KH      103105  //查询客户利差返还参数
#define  FUN_ACCOUNT_LIST_LXFHGL_JJR     103106  //查询经纪人利差返还参数
#define  FUN_ACCOUNT_SYNC_TABLE          103901  //参数表同步
#define  FUN_SECU_LIST_MARKET            104001  //获取交易所参数
#define  FUN_SECU_LIST_ZQMB              104002  //获取证券模板参数
#define  FUN_SECU_LIST_JYLB              104003  //获取所有交易类别参数
#define  FUN_SECU_LIST_XJFSCS            104004  //查询限价方式参数
#define  FUN_SECU_LIST_ZQDYBL            104005  //查询债券抵押比例
#define  FUN_SECU_LIST_QSDMDZ            104006  //查询交易所的券商代码对照
#define  FUN_SECU_LIST_SFBZ_WT           104007  //查询委托收费标准
#define  FUN_SECU_LIST_SECUCODE          104008  //证券代码查询
#define  FUN_SECU_LIST_HGCS              104009  //查询回购参数
#define  FUN_SECU_LIST_SFBZ_ZHYW         104010  //查询帐户类业务收费标准
#define  FUN_SECU_LIST_JYR               104011  //查询交易日参数
#define  FUN_SECU_LIST_SBWTH             104012  //查询申报委托号参数定义
#define  FUN_SECU_LIST_RCZH              104013  //查询容错帐户
#define  FUN_SECU_LIST_CYLXZCS           104014  //查询持有量限制参数
#define  FUN_SECU_LIST_JYPZXZCS          104015  //查询交易品种限制参数
#define  FUN_SECU_LIST_HGCS_MD           104016  //查询买断式回购参数
#define  FUN_SECU_LIST_SBQQ_ZQDM         104017  //查询三板确权证券代码
#define  FUN_SECU_LIST_SBQQ_DBJKLJ       104018  //查询代办券商业务接口库路径
#define  FUN_SECU_LIST_SBQQ_ZBJKLJ       104019  //查询主办券商业务接口路径
#define  FUN_SECU_LIST_SBXWCS            104020  //查询申报席位参数
#define  FUN_SECU_LIST_SBSJCS            104021  //查询申报时间限制参数
#define  FUN_SECU_LIST_SBTDCS            104022  //查询申报通道参数
#define  FUN_SECU_LIST_SBZJFP            104023  //查询申报主机分配参数
#define  FUN_SECU_LIST_HBTDCS            104024  //查询回报通道参数
#define  FUN_SECU_LIST_ZQDMEX            104025  //查询证券代码扩展信息
#define  FUN_SECU_LIST_QSZLPZ            104026  //查询清算资料配置参数
#define  FUN_SECU_LIST_QZXX              104027  //查询权证信息
#define  FUN_SECU_LIST_SBXWZCS           104028  //查询申报席位组参数
#define  FUN_SECU_LIST_SECUINFO          104051  //获取所有证券信息
#define  FUN_SECU_LIST_ZQLB              104052  //获取所有证券类别参数
#define  FUN_SECU_LIST_ZQJYSX            104053  //查询证券交易属性参数
#define  FUN_SECU_GETYYBXWDM             104055  //查询营业部席位代码
#define  FUN_SECU_LIST_SBBZ              104056  //查询订单申报标志
#define  FUN_SECU_LIST_JYXZJB_KH         104057  //查询客户交易限制级别(交易品种及持有量)
#define  FUN_SECU_LIST_JYXZJB_QZ         104058  //查询群组交易限制级别(交易品种及持有量)
#define  FUN_SECU_GET_BGJSRQ             104059  //查询B股交收日期
#define  FUN_SECU_GET_JYRBS              104060  //查询某日的交易日标识
#define  FUN_SECU_GET_NEXTJYR            104061  //查询某日的下一个交易日
#define  FUN_SECU_GET_PREVJYR            104062  //查询某日的上一个交易日
#define  FUN_QUOTE_LIST_ALLZQHQ          104100  //查询全体当前行情信息
#define  FUN_QUOTE_GETHQ                 104101  //证券行情查询
#define  FUN_QUOTE_LISTHQ                104102  //多股简单行情查询
#define  FUN_QUOTE_LISTZQXX              104103  //证券代码查询
#define  FUN_SECU_LIST_CYLXZCS_KH        104104  //查询客户证券持有量限制参数
#define  FUN_SECU_LIST_ETFXX             104105  //查询ETF基本信息
#define  FUN_SECU_LIST_ETFCFGXX          104106  //查询ETF成份股信息
#define  FUN_TZLC_LIST_ZHDM              104301  //查询专户代码信息(投资理财)
#define  FUN_TZLC_LIST_ZHSGXX            104302  //查询专户申购信息
#define  FUN_TZLC_LIST_ZHXX              104303  //查询理财客户信息
#define  FUN_TZLC_LIST_DHSGXX            104304  //理财客户单户申购信息查询
#define  FUN_TZLC_LIST_JZHZXX            104305  //查询集中划转信息
#define  FUN_SECU_LIST_RATE_BZYJ         104501  //查询标准一级清算费率
#define  FUN_SECU_LIST_RATE_BZEJ         104502  //查询标准二级清算费率
#define  FUN_SECU_LIST_RATE_HGYJ         104503  //查询回购一级清算费率
#define  FUN_SECU_LIST_RATE_HGEJ         104504  //查询回购二级清算费率
#define  FUN_SECU_LIST_YJDJ_DJCLID       104601  //查询佣金定价策略
#define  FUN_SECU_LIST_YJDJ_ZKCS         104602  //查询佣金定价折扣参数
#define  FUN_SECU_LIST_YJDJ_JYLFD        104603  //查询交易量分段参数
#define  FUN_SECU_LIST_YJDJ_ZCFD         104604  //查询资产分段参数
#define  FUN_SECU_LIST_YJFHCS_KH         104605  //查询客户佣金返还参数
#define  FUN_SECU_LIST_YJFHCS_QZ         104606  //查询群组佣金返还参数
#define  FUN_SECU_LIST_YJFHCS_JJR        104607  //查询经纪人佣金返还参数
#define  FUN_SECU_LIST_BZQZKL_KH         104608  //查询客户标准券折扣率参数
#define  FUN_SECU_LIST_YJDJ_KHTJSX       104611  //查询客户统计属性信息
#define  FUN_SECU_LIST_YJDJDX_KH         104612  //查询客户佣金定价策略
#define  FUN_SECU_LIST_YJDJDX_QZ         104613  //查询群组佣金定价策略
#define  FUN_SECU_LIST_YJDJDX_JJR        104614  //查询经纪人佣金定价策略
#define  FUN_SECU_LIST_WTFSYJZK          104619  //查询委托方式佣金折扣参数
#define  FUN_SECU_SYNC_TABLE             104901  //参数表同步
#define  FUN_OFS_LIST_TAXX               105001  //查询基金公司参数
#define  FUN_OFS_LIST_JJXX               105002  //查询基金代码参数
#define  FUN_OFS_LIST_YWDM               105003  //查询基金业务代码
#define  FUN_OFS_LIST_TAYWDM             105004  //查询基金公司支持的业务代码
#define  FUN_OFS_LIST_JKMK               105005  //查询接口模块参数
#define  FUN_OFS_LIST_JKPZCS             105006  //查询开放式基金接口配置参数
#define  FUN_OFS_LIST_ZKL                105007  //查询基金折扣参数设置
#define  FUN_OFS_LIST_DQDESGPZ           105008  //查询定期定额申购品种
#define  FUN_OFS_LIST_DQDESGXY           105009  //查询客户定期定额协议
#define  FUN_OFS_LIST_GGXX               105010  //查询公告信息
#define  FUN_OFS_LIST_JYR                105011  //查询基金交易日参数
#define  FUN_OFS_GET_JYRBS               105100  //查询某日的基金交易日标识
#define  FUN_OFS_GET_NEXTJYR             105101  //查询某日的下一个基金交易日
#define  FUN_OFS_GET_PREVJYR             105102  //查询某日的上一个基金交易日
#define  FUN_PARAM_ADD_MYYWXZ            121001  //添加营业部漫游业务限制
#define  FUN_PARAM_ADD_DATADICT          121004  //增加系统数据字典
#define  FUN_PARAM_ADD_BZ                121005  //增加币种参数
#define  FUN_PARAM_ADD_BRANCH            121006  //增加营业部参数
#define  FUN_PARAM_ADD_ACCOUNTITEM       121008  //增加业务科目参数
#define  FUN_PARAM_ADD_MENU              121010  //添加系统菜单
#define  FUN_PARAM_ADD_CARDINFO          121013  //添加各类卡参数
#define  FUN_PARAM_ADD_NATIONCODE        121014  //添加国籍代码对照参数
#define  FUN_PARAM_ADD_XZQYDM            121016  //添加行政区域代码参数
#define  FUN_PARAM_ADD_CWDMDZ            121018  //添加内部错误代码对照
#define  FUN_PARAM_ADD_TASKLOG           121019  //添加系统工作日志
#define  FUN_PARAM_ADD_XTCS              121025  //添加杂项参数信息
#define  FUN_PARAM_ADD_BROKER            121031  //添加经纪人信息
#define  FUN_PARAM_ADD_KHJLXX            121032  //添加客户经理信息
#define  FUN_PARAM_ADD_WBCWDMDZ          121035  //添加外部错误代码对照
#define  FUN_PARAM_ADD_ROLEINFO          121111  //添加角色信息
#define  FUN_PARAM_ADD_CDHCGX            121123  //添加指定菜单权限互斥的菜单
#define  FUN_PARAM_ADD_JSHCGX            121124  //添加指定角色权限互斥的角色
#define  FUN_PARAM_ADD_JSTZ              121151  //添加即时通知信息
#define  FUN_PARAM_ADD_JYBW              121153  //添加交易备忘信息
#define  FUN_PARAM_ADD_JYBWDY            121154  //添加交易备忘订阅
#define  FUN_PARAM_ADD_YWSHGL            121201  //添加业务审核管理参数
#define  FUN_PARAM_ADD_SHXMQD            121203  //添加审核项目清单
#define  FUN_PARAM_ADD_YWSHLC            121211  //添加业务审核流程
#define  FUN_PARAM_ADD_YWSHZP            121212  //用户临时指派业务审核权限
#define  FUN_PARAM_ADD_YWSHKHFW          121214  //添加业务审核客户范围定义
#define  FUN_PARAM_ADD_QSZJFP            121301  //新增清算主机分配
#define  FUN_PARAM_ADD_QSZLPZ            121302  //增加清算资料文件
#define  FUN_PARAM_ADD_QSRWKZ            121303  //增加清算任务
#define  FUN_PARAM_ADD_QSRWWJ            121304  //添加清算任务文件
#define  FUN_CUSTOM_ADD_KHQZ             122001  //增加客户群组参数
#define  FUN_CUSTOM_ADD_KHSXLB           122002  //增加客户属性类别参数
#define  FUN_CUSTOM_ADD_GIFTINFO         122004  //登记礼品参数
#define  FUN_CUSTOM_ADD_GSFL             122005  //增加系统公司分类参数
#define  FUN_CUSTOM_ADD_KHHRES           122006  //添加客户号资源参数
#define  FUN_CUSTOM_ADD_ZYXZCS           122007  //添加资源限制参数
#define  FUN_CUSTOM_ADD_SFBZ_ZYXZ        122008  //添加资源限制收费标准参数
#define  FUN_CUSTOM_ADD_KZYWDM           122010  //添加扩展业务代码定义
#define  FUN_CUSTOM_ADD_KZYWCS           122011  //添加扩展业务参数定义
#define  FUN_ACCOUNT_ADD_HLCS            123001  //添加汇率参数
#define  FUN_ACCOUNT_ADD_LLMB            123002  //添加利率模板
#define  FUN_ACCOUNT_ADD_LLMBCS          123003  //添加利率模板参数
#define  FUN_ACCOUNT_ADD_QKKZMB          123004  //添加取款底数控制模板
#define  FUN_ACCOUNT_ADD_QKKZMBCS        123005  //添加取款底数控制模板参数
#define  FUN_ACCOUNT_ADD_QZCQED          123006  //添加群组存取额度
#define  FUN_ACCOUNT_ADD_XYED_YYB        123007  //添加营业部信用额度参数
#define  FUN_ACCOUNT_ADD_XYED_JJR        123008  //添加经纪人信用额度参数
#define  FUN_ACCOUNT_ADD_XYED_ZH         123009  //添加资金帐户信用额度参数
#define  FUN_ACCOUNT_ADD_WBJGDM          123010  //添加外部机构代码
#define  FUN_ACCOUNT_ADD_YZZZCS          123011  //添加银证转帐参数
#define  FUN_ACCOUNT_ADD_YZTCS           123012  //添加银证通参数
#define  FUN_ACCOUNT_ADD_WBYWCS          123013  //添加外部业务参数对照
#define  FUN_ACCOUNT_ADD_LLMBDX_KH       123101  //添加客户利率模板对象
#define  FUN_ACCOUNT_ADD_LLMBDX_QZ       123102  //添加群组利率模板对象
#define  FUN_ACCOUNT_ADD_LXFHGL_KH       123105  //添加客户利差返还参数
#define  FUN_ACCOUNT_ADD_LXFHGL_JJR      123106  //添加经纪人利差返还参数
#define  FUN_SECU_ADD_MARKET             124001  //添加交易所参数
#define  FUN_SECU_ADD_ZQMB               124002  //添加证券模板参数
#define  FUN_SECU_ADD_JYLB               124003  //添加交易类别参数
#define  FUN_SECU_ADD_XJFSCS             124004  //添加限价方式参数
#define  FUN_SECU_ADD_ZQDYBL             124005  //添加债券抵押比例
#define  FUN_SECU_ADD_QSDMDZ             124006  //添加交易所的券商代码对照
#define  FUN_SECU_ADD_SFBZ_WT            124007  //登记委托收费参数
#define  FUN_SECU_ADD_SECUCODE           124008  //添加证券代码
#define  FUN_SECU_ADD_HGCS               124009  //添加回购参数
#define  FUN_SECU_ADD_SFBZ_ZHYW          124010  //添加帐户类业务收费标准
#define  FUN_SECU_ADD_JYR                124011  //添加某年交易日参数
#define  FUN_SECU_ADD_SBWTH              124012  //添加申报委托号参数定义
#define  FUN_SECU_ADD_RCZH               124013  //添加容错帐户参数定义
#define  FUN_SECU_ADD_CYLXZCS            124014  //添加持有量限制参数
#define  FUN_SECU_ADD_JYPZXZCS           124015  //添加交易品种限制参数
#define  FUN_SECU_ADD_HGCS_MD            124016  //添加买断式回购参数
#define  FUN_SECU_ADD_SBQQ_ZQDM          124017  //添加三板确权证券代码
#define  FUN_SECU_ADD_SBQQ_DBJKLJ        124018  //添加代办券商业务接口路径
#define  FUN_SECU_ADD_SBQQ_ZBJKLJ        124019  //添加主办券商业务接口路径
#define  FUN_SECU_ADD_SBXWCS             124020  //添加申报席位参数
#define  FUN_SECU_ADD_SBSJCS             124021  //添加申报时间限制参数
#define  FUN_SECU_ADD_SBTDCS             124022  //添加申报通道参数
#define  FUN_SECU_ADD_SBZJFP             124023  //添加申报主机分配参数
#define  FUN_SECU_ADD_HBTDCS             124024  //添加回报通道参数
#define  FUN_SECU_ADD_ZQDMEX             124025  //添加证券代码扩展信息
#define  FUN_SECU_ADD_QZXX               124027  //添加权证信息
#define  FUN_SECU_ADD_SBXWZCS            124028  //添加申报席位组参数
#define  FUN_SECU_ADD_ZQLB               124052  //添加证券类别参数
#define  FUN_SECU_ADD_ZQJYSX             124053  //添加证券交易属性参数
#define  FUN_SECU_INITZQ                 124061  //证券代码添加和行情更新
#define  FUN_SECU_INIT_BZQCL             124062  //帐户式回购标准券初始化处理
#define  FUN_SECU_ADD_CYLXZCS_KH         124104  //增加客户证券持有量限制参数
#define  FUN_SECU_ADD_ETFXX              124105  //增加ETF基本信息
#define  FUN_SECU_ADD_ETFCFGXX           124106  //增加ETF成份股信息
#define  FUN_TZLC_ADD_ZHDM               124301  //增加专户代码信息
#define  FUN_TZLC_ADD_ZHSGXX             124302  //增加专户申购信息
#define  FUN_TZLC_REG_ZHXX               124303  //登记合作理财客户信息
#define  FUN_TZLC_ADD_DHSGXX             124304  //增加理财客户单户申购信息
#define  FUN_TZLC_ADD_JZHZXX             124305  //增加集中划转信息
#define  FUN_SECU_ADD_RATE_BZYJ          124501  //添加标准一级清算费率
#define  FUN_SECU_ADD_RATE_BZEJ          124502  //添加标准二级清算费率
#define  FUN_SECU_ADD_RATE_HGYJ          124503  //添加回购一级清算费率
#define  FUN_SECU_ADD_RATE_HGEJ          124504  //添加回购二级清算费率
#define  FUN_SECU_ADD_YJDJ_DJCLID        124601  //添加佣金定价策略
#define  FUN_SECU_ADD_YJDJ_ZKCS          124602  //添加佣金定价折扣参数
#define  FUN_SECU_ADD_YJDJ_JYLFD         124603  //添加交易量分段参数
#define  FUN_SECU_ADD_YJDJ_ZCFD          124604  //添加资产分段参数
#define  FUN_SECU_ADD_YJFHCS_KH          124605  //增加客户佣金返还参数
#define  FUN_SECU_ADD_YJFHCS_QZ          124606  //增加群组佣金返还参数
#define  FUN_SECU_ADD_YJFHCS_JJR         124607  //增加经纪人佣金返还参数
#define  FUN_SECU_ADD_BZQZKL_KH          124608  //添加客户标准券折扣率参数
#define  FUN_SECU_ADD_WTFSYJZK           124619  //增加委托方式佣金折扣参数
#define  FUN_SECU_ADD_YJFHMX             124621  //增加佣金返还明细
#define  FUN_SECU_ADD_GZCS               124801  //增加国债代码参数
#define  FUN_SECU_MODI_GZCS              124802  //修改国债代码参数
#define  FUN_SECU_DEL_GZCS               124803  //删除国债代码参数
#define  FUN_SECU_LIST_GZCSXX            124804  //查询国债代码参数信息
#define  FUN_SECU_LIST_GZLLXX            124805  //查询国债利率参数信息
#define  FUN_SECU_ADD_GZLL               124806  //增加国债利率参数
#define  FUN_SECU_DEL_GZLL               124807  //删除国债利率参数
#define  FUN_SECU_MODI_GZLL              124808  //修改国债利率参数
#define  FUN_SECU_GZ_XQFX                124809  //国债新券发行
#define  FUN_SECU_GZ_DBGCQ               124810  //国债代保管存券
#define  FUN_SECU_LIST_GZDBGPZ           124811  //查询代保管基本信息
#define  FUN_SECU_GS_GZDBGPZ             124812  //国债代保管凭证挂失
#define  FUN_SECU_JG_GZDBGPZ             124813  //国债代保管凭证解挂
#define  FUN_SECU_DEL_GZDBGPZ            124814  //国债代保管凭证删除
#define  FUN_SECU_GM_GZDBGPZ             124815  //国债代保管凭证更名
#define  FUN_SECU_GH_GZDBGPZ             124816  //国债代保管凭证过户
#define  FUN_SECU_BD_GZDBGPZ             124817  //国债代保管凭证补登
#define  FUN_SECU_DF_GZDBGPZ             124818  //国债代保管兑付
#define  FUN_SECU_PLDF_GZDBGPL           124819  //国债代保管派利兑付
#define  FUN_SECU_LIST_GZDBGPL           124820  //查询国债代保管派利兑付信息
#define  FUN_SECU_DFLX_GZDBGPL           124821  //国债代保管兑付领息
#define  FUN_SECU_JGKHDFLX_GZDBGPL       124822  //机构客户领国债代保管利息
#define  FUN_SECU_DQDBGJX_GZDBGPZ        124823  //定期代保管计息
#define  FUN_SECU_DQDBGDF_GZDBGPZ        124824  //定期代保管兑付
#define  FUN_SECU_ZQJZTJ_TEMKCB          124825  //国债结账统计
#define  FUN_SECU_LIST_TGZKCB            124826  //流览国债结算统计结果
#define  FUN_SECU_XQDQDF_TXQBDMX         124827  //国债实物券到期兑付
#define  FUN_SECU_LIST_TGZDBGMX          124828  //查询代保管业务操作明细
#define  FUN_SECU_LIST_TXQBDMX           124829  //查询实物券兑付业务操作明细
#define  FUN_SECU_LIST_GYDRDBGMX         124830  //查询当日柜员代保管明细
#define  FUN_SECU_DBGYWCQ				         124831  //代保管业务冲券
#define  FUN_SECU_LIST_GYDRXQMX				   124832  //查询柜员当日实物券兑付明细	
#define  FUN_SECU_XQYWCQ							   124833  //实物券兑付冲券	

#define  FUN_OFS_ADD_TAXX                125001  //登记基金公司参数
#define  FUN_OFS_ADD_JJXX                125002  //增加基金代码参数
#define  FUN_OFS_ADD_JJYWDM              125003  //增加基金业务代码
#define  FUN_OFS_ADD_TAYWDM              125004  //增加基金公司支持的业务代码
#define  FUN_OFS_ADD_JKMK                125005  //增加接口模块参数
#define  FUN_OFS_ADD_JKPZCS              125006  //增加基金接口配置参数
#define  FUN_OFS_ADD_ZKL                 125007  //增加基金折扣参数
#define  FUN_OFS_ADD_DQDESGPZ            125008  //增加定期定额申购品种
#define  FUN_OFS_ADD_DQDESGXY            125009  //增加客户定期定额申购协议
#define  FUN_OFS_ADD_JYR                 125011  //添加某年基金交易日参数
#define  FUN_PARAM_MODI_MYYWXZ           141001  //修改营业部漫游业务限制
#define  FUN_PARAM_MODI_DATADICT         141004  //修改系统数据字典
#define  FUN_PARAM_MODI_BZ               141005  //修改币种参数
#define  FUN_PARAM_MODI_BRANCH           141006  //修改营业部参数
#define  FUN_PARAM_MODI_ACCOUNTITEM      141008  //修改业务科目参数
#define  FUN_PARAM_MODI_MENU             141010  //修改系统菜单参数
#define  FUN_PARAM_MODI_CARDINFO         141013  //修改各类卡参数
#define  FUN_PARAM_MODI_NATIONCODE       141014  //修改国籍代码对照参数
#define  FUN_PARAM_MODI_XZQYDM           141016  //修改行政区域代码参数
#define  FUN_PARAM_MODI_CWDMDZ           141018  //修改内部错误代码对照
#define  FUN_PARAM_MODI_TASKLOG          141019  //修改系统工作日志
#define  FUN_PARAM_MODI_XTZT             141020  //修改系统状态
#define  FUN_PARAM_MODI_XTCS             141025  //修改杂项参数信息
#define  FUN_PARAM_MODI_SFYZCS           141026  //修改营业部身份验证参数
#define  FUN_PARAM_MODI_BROKER           141031  //修改经纪人信息
#define  FUN_PARAM_MODI_KHJLXX           141032  //修改客户经理信息
#define  FUN_PARAM_MODI_WBCWDMDZ         141035  //修改外部错误代码对照
#define  FUN_PARAM_MODI_ROLEINFO         141111  //修改角色信息
#define  FUN_PARAM_MODI_YWSHGL           141201  //修改业务审核管理参数
#define  FUN_PARAM_MODI_SHXMQD           141203  //修改审核项目清单
#define  FUN_PARAM_MODI_YWSHLC           141211  //修改业务审核流程
#define  FUN_PARAM_MODI_QSZJFP           141301  //修改清算主机分配
#define  FUN_PARAM_MODI_QSZLPZ           141302  //修改清算文件配置
#define  FUN_PARAM_MODI_QSRWKZ           141303  //修改清算任务
#define  FUN_PARAM_MODI_QSRWWJ           141304  //修改清算任务文件
#define  FUN_PARAM_MODI_QSRWZT           141305  //修改清算任务状态
#define  FUN_PARAM_MODI_XTCSJB           141999  //修改杂项参数级别
#define  FUN_CUSTOM_MODI_KHQZ            142001  //修改客户群组参数
#define  FUN_CUSTOM_MODI_KHSXLB          142002  //修改客户属性类别参数
#define  FUN_CUSTOM_MODI_GIFTINFO        142004  //修改礼品参数
#define  FUN_CUSTOM_MODI_GSFL            142005  //修改系统公司分类参数
#define  FUN_CUSTOM_MODI_KHHRES          142006  //修改客户号资源参数
#define  FUN_CUSTOM_MODI_ZYXZCS          142007  //修改资源限制参数
#define  FUN_CUSTOM_MODI_SFBZ_ZYXZ       142008  //修改资源限制收费标准参数
#define  FUN_CUSTOM_MODI_KZYWDM          142010  //修改扩展业务代码定义
#define  FUN_CUSTOM_MODI_KZYWCS          142011  //修改扩展业务参数定义
#define  FUN_ACCOUNT_MODI_HLCS           143001  //修改汇率参数
#define  FUN_ACCOUNT_MODI_LLMB           143002  //修改利率模板
#define  FUN_ACCOUNT_MODI_LLMBCS         143003  //修改利率模板参数
#define  FUN_ACCOUNT_MODI_QKKZMB         143004  //修改取款底数控制模板
#define  FUN_ACCOUNT_MODI_QKKZMBCS       143005  //修改取款底数控制模板参数
#define  FUN_ACCOUNT_MODI_QZCQED         143006  //修改群组存取额度
#define  FUN_ACCOUNT_MODI_XYED_YYB       143007  //修改营业部信用额度参数
#define  FUN_ACCOUNT_MODI_XYED_JJR       143008  //修改经纪人信用额度参数
#define  FUN_ACCOUNT_MODI_XYED_ZH        143009  //修改资金帐户信用额度参数
#define  FUN_ACCOUNT_MODI_WBJGDM         143010  //修改外部机构代码参数
#define  FUN_ACCOUNT_MODI_YZZZCS         143011  //修改银证转帐参数
#define  FUN_ACCOUNT_MODI_YZTCS          143012  //修改银证通参数
#define  FUN_ACCOUNT_MODI_WBYWCS         143013  //修改外部业务参数对照
#define  FUN_ACCOUNT_MODI_LLMBDX_KH      143101  //修改客户使用的利率模板对象
#define  FUN_ACCOUNT_MODI_LLMBDX_QZ      143102  //修改群组使用的利率模板对象
#define  FUN_ACCOUNT_MODI_QKKZMBDX_KH    143103  //修改客户使用的取款控制模板对象
#define  FUN_ACCOUNT_MODI_QKKZMBDX_QZ    143104  //修改群组使用的取款控制模板对象
#define  FUN_ACCOUNT_MODI_LXFHGL_KH      143105  //修改客户利差返还参数
#define  FUN_ACCOUNT_MODI_LXFHGL_JJR     143106  //修改经纪人利差返还参数
#define  FUN_SECU_MODI_MARKET            144001  //修改交易所参数
#define  FUN_SECU_MODI_ZQMB              144002  //修改证券模板参数
#define  FUN_SECU_MODI_JYLB              144003  //修改交易类别参数
#define  FUN_SECU_MODI_XJFSCS            144004  //修改限价方式参数
#define  FUN_SECU_MODI_ZQDYBL            144005  //修改债券抵押比例
#define  FUN_SECU_MODI_QSDMDZ            144006  //修改交易所的券商代码对照
#define  FUN_SECU_MODI_SFBZ_WT           144007  //修改委托收费参数
#define  FUN_SECU_MODI_SECUCODE          144008  //修改证券代码信息
#define  FUN_SECU_MODI_HGCS              144009  //修改回购参数
#define  FUN_SECU_MODI_SFBZ_ZHYW         144010  //修改帐户类业务收费标准
#define  FUN_SECU_MODI_JYR               144011  //修改交易日参数
#define  FUN_SECU_MODI_SBWTH             144012  //修改申报委托号参数定义
#define  FUN_SECU_MODI_RCZH              144013  //修改容错帐户参数定义
#define  FUN_SECU_MODI_CYLXZCS           144014  //修改持有量限制参数
#define  FUN_SECU_MODI_JYPZXZCS          144015  //修改交易品种限制参数
#define  FUN_SECU_MODI_HGCS_MD           144016  //修改买断式回购参数
#define  FUN_SECU_MODI_SBQQ_ZQDM         144017  //修改三板确权证券代码
#define  FUN_SECU_MODI_SBQQ_DBJKLJ       144018  //修改代办券商业务接口路径
#define  FUN_SECU_MODI_SBQQ_ZBJKLJ       144019  //修改主办券商业务接口路径
#define  FUN_SECU_MODI_SBXWCS            144020  //修改申报席位参数
#define  FUN_SECU_MODI_SBSJCS            144021  //修改申报时间限制参数
#define  FUN_SECU_MODI_SBTDCS            144022  //修改申报通道参数
#define  FUN_SECU_MODI_SBZJFP            144023  //修改申报主机分配参数
#define  FUN_SECU_MODI_HBTDCS            144024  //修改回报通道参数
#define  FUN_SECU_MODI_ZQDMEX            144025  //修改证券代码扩展信息
#define  FUN_SECU_MODI_SBXWZCS           144028  //修改申报席位组参数
#define  FUN_SECU_MODI_ZQLB              144052  //修改证券类别参数
#define  FUN_SECU_MODI_ZQJYSX            144053  //修改证券交易属性参数
#define  FUN_SECU_MODI_JYXZJB_KH         144057  //修改客户交易限制级别
#define  FUN_SECU_MODI_JYXZJB_QZ         144058  //修改群组交易限制级别
#define  FUN_SECU_REG_LASTRECNO          144059  //记录成交回报笔数
#define  FUN_SECU_REG_WTQRNO             144060  //记录委托确认笔数
#define  FUN_SECU_MODI_CYLXZCS_KH        144104  //修改客户证券持有量上限
#define  FUN_SECU_MODI_ETFXX             144105  //修改ETF基本信息
#define  FUN_TZLC_MODI_ZHDM              144301  //修改专户代码信息
#define  FUN_TZLC_MODI_ZHSGXX            144302  //修改专户申购信息
#define  FUN_TZLC_MODI_ZHXX              144303  //修改合作理财客户信息
#define  FUN_TZLC_MODI_DHSGXX            144304  //修改合作理财单户申购信息
#define  FUN_TZLC_MODI_JZHZXX            144305  //修改集中划转信息
#define  FUN_SECU_MODI_RATE_BZYJ         144501  //修改标准一级清算费率
#define  FUN_SECU_MODI_RATE_BZEJ         144502  //修改标准二级清算费率
#define  FUN_SECU_MODI_RATE_HGYJ         144503  //修改回购一级清算费率
#define  FUN_SECU_MODI_RATE_HGEJ         144504  //修改回购二级清算费率
#define  FUN_SECU_MODI_YJDJ_DJCLID       144601  //修改佣金定价策略
#define  FUN_SECU_MODI_YJDJ_ZKCS         144602  //修改佣金定价折扣参数
#define  FUN_SECU_MODI_YJDJ_JYLFD        144603  //修改交易量分段参数
#define  FUN_SECU_MODI_YJDJ_ZCFD         144604  //修改资产分段参数
#define  FUN_SECU_MODI_YJFHCS_KH         144605  //修改客户佣金返还参数
#define  FUN_SECU_MODI_YJFHCS_QZ         144606  //修改群组佣金返还参数
#define  FUN_SECU_MODI_YJFHCS_JJR        144607  //修改经纪人佣金返还参数
#define  FUN_SECU_MODI_BZQZKL_KH         144608  //修改客户标准券折扣率参数
#define  FUN_SECU_MODI_YJDJ_KHTJSX       144611  //修改客户统计属性信息
#define  FUN_SECU_MODI_YJDJDX_KH         144612  //修改客户佣金定价策略
#define  FUN_SECU_MODI_YJDJDX_QZ         144613  //修改群组佣金定价策略
#define  FUN_SECU_MODI_YJDJDX_JJR        144614  //修改经纪人佣金定价策略
#define  FUN_SECU_YJDJ_CLEARKHJYL        144615  //佣金定价客户交易量归零
#define  FUN_SECU_YJDJ_CLEARQZJYL        144616  //佣金定价群组交易量归零
#define  FUN_SECU_YJDJ_CLEARJJRJYL       144617  //佣金定价经纪人交易量归零
#define  FUN_SECU_YJDJ_XGKHTJ            144618  //修改客户佣金定价统计值
#define  FUN_SECU_MODI_WTFSYJZK          144619  //修改委托方式佣金折扣参数
#define  FUN_SECU_INIT_KHTJSX            144620  //初始化客户统计信息
#define  FUN_SECU_MODI_YJFHMX            144621  //修改佣金返还明细
#define  FUN_OFS_MODI_TAXX               145001  //修改基金公司参数
#define  FUN_OFS_MODI_JJXX               145002  //修改基金代码参数
#define  FUN_OFS_MODI_JJYWDM             145003  //修改基金业务代码
#define  FUN_OFS_MODI_JKMK               145005  //修改接口模块参数
#define  FUN_OFS_MODI_JKPZCS             145006  //修改基金接口配置参数
#define  FUN_OFS_MODI_ZKL                145007  //修改基金折扣参数
#define  FUN_OFS_MODI_DQDESGPZ           145008  //修改定期定额申购品种
#define  FUN_OFS_MODI_DQDESGXY           145009  //修改定期定额申购协议
#define  FUN_OFS_MODI_JYR                145011  //修改基金交易日参数
#define  FUN_PARAM_DEL_MYYWXZ            161001  //删除营业部漫游业务限制
#define  FUN_PARAM_DEL_DATADICT          161004  //删除系统数据字典
#define  FUN_PARAM_DEL_BZ                161005  //删除币种参数
#define  FUN_PARAM_DEL_BRANCH            161006  //删除营业部参数
#define  FUN_PARAM_DEL_ACCOUNTITEM       161008  //删除业务科目参数
#define  FUN_PARAM_DEL_MENU              161010  //删除系统菜单
#define  FUN_PARAM_DEL_CARDINFO          161013  //删除各类卡参数
#define  FUN_PARAM_DEL_NATIONCODE        161014  //删除国籍代码对照参数
#define  FUN_PARAM_DEL_XZQYDM            161016  //删除指定行政区域代码参数
#define  FUN_PARAM_DEL_CWDMDZ            161018  //删除内部错误代码对照
#define  FUN_PARAM_DEL_XTCS              161025  //删除杂项参数信息
#define  FUN_PARAM_DEL_SFYZCS            161026  //删除营业部身份验证参数
#define  FUN_PARAM_DEL_BROKER            161031  //注销经纪人信息
#define  FUN_PARAM_DEL_KHJLXX            161032  //注销客户经理信息
#define  FUN_PARAM_DEL_WBCWDMDZ          161035  //删除外部错误代码对照
#define  FUN_PARAM_DEL_ROLEINFO          161111  //删除指定角色信息
#define  FUN_PARAM_DEL_CDHCGX            161123  //删除指定菜单权限互斥的菜单
#define  FUN_PARAM_DEL_JSHCGX            161124  //删除指定角色权限互斥的角色
#define  FUN_PARAM_DEL_JSTZ              161151  //删除即时通知信息
#define  FUN_PARAM_DEL_JYBW              161153  //删除交易备忘信息
#define  FUN_PARAM_DEL_JYBWDY            161154  //删除交易备忘订阅
#define  FUN_PARAM_DEL_YWSHGL            161201  //删除业务审核管理参数
#define  FUN_PARAM_DEL_SHXMQD            161203  //删除审核项目清单
#define  FUN_PARAM_DEL_YWSHLC            161211  //删除业务审核流程
#define  FUN_PARAM_DEL_YWSHZP            161212  //删除用户临时指派的业务审核权限
#define  FUN_PARAM_DEL_YWSHKHFW          161214  //删除业务审核客户范围定义
#define  FUN_PARAM_DEL_QSZJFP            161301  //删除清算主机配置
#define  FUN_PARAM_DEL_QSZLPZ            161302  //删除清算文件配置
#define  FUN_PARAM_DEL_QSRWKZ            161303  //删除清算任务
#define  FUN_PARAM_DEL_QSRWWJ            161304  //删除清算任务文件
#define  FUN_CUSTOM_DEL_KHQZ             162001  //删除客户群组参数
#define  FUN_CUSTOM_DEL_KHSXLB           162002  //删除客户属性类别参数
#define  FUN_CUSTOM_DEL_GIFTINFO         162004  //删除礼品参数
#define  FUN_CUSTOM_DEL_GSFL             162005  //删除系统公司分类参数
#define  FUN_CUSTOM_DEL_KHHRES           162006  //删除客户号资源参数
#define  FUN_CUSTOM_DEL_ZYXZCS           162007  //删除资源限制参数
#define  FUN_CUSTOM_DEL_SFBZ_ZYXZ        162008  //删除资源限制收费标准参数
#define  FUN_CUSTOM_DEL_KZYWDM           162010  //删除扩展业务代码定义
#define  FUN_CUSTOM_DEL_KZYWCS           162011  //删除扩展业务参数定义
#define  FUN_ACCOUNT_DEL_HLCS            163001  //删除汇率参数
#define  FUN_ACCOUNT_DEL_LLMB            163002  //删除利率模板
#define  FUN_ACCOUNT_DEL_LLMBCS          163003  //删除利率模板参数
#define  FUN_ACCOUNT_DEL_QKKZMB          163004  //删除取款底数控制模板
#define  FUN_ACCOUNT_DEL_QKKZMBCS        163005  //删除取款底数控制模板参数
#define  FUN_ACCOUNT_DEL_QZCQED          163006  //删除群组存取额度
#define  FUN_ACCOUNT_DEL_XYED_YYB        163007  //删除营业部信用额度参数
#define  FUN_ACCOUNT_DEL_XYED_JJR        163008  //删除经纪人信用额度参数
#define  FUN_ACCOUNT_DEL_XYED_ZH         163009  //删除资金帐户信用额度参数
#define  FUN_ACCOUNT_DEL_WBJGDM          163010  //删除外部机构代码
#define  FUN_ACCOUNT_DEL_YZZZCS          163011  //删除银证转帐参数
#define  FUN_ACCOUNT_DEL_YZTCS           163012  //删除银证通参数
#define  FUN_ACCOUNT_DEL_WBYWCS          163013  //删除外部业务参数对照
#define  FUN_ACCOUNT_DEL_LLMBDX_KH       163101  //删除客户使用的利率模板对象
#define  FUN_ACCOUNT_DEL_LLMBDX_QZ       163102  //删除群组使用的利率模板对象
#define  FUN_ACCOUNT_DEL_QKKZMBDX_KH     163103  //删除客户使用的取款控制模板对象
#define  FUN_ACCOUNT_DEL_QKKZMBDX_QZ     163104  //删除群组使用的取款控制模板对象
#define  FUN_ACCOUNT_DEL_LXFHGL_KH       163105  //删除客户利差返还参数
#define  FUN_ACCOUNT_DEL_LXFHGL_JJR      163106  //删除经纪人利差返还参数
#define  FUN_SECU_DEL_MARKET             164001  //删除交易所参数
#define  FUN_SECU_DEL_ZQMB               164002  //删除证券模板参数
#define  FUN_SECU_DEL_JYLB               164003  //删除交易类别参数
#define  FUN_SECU_DEL_XJFSCS             164004  //删除限价方式参数
#define  FUN_SECU_DEL_ZQDYBL             164005  //删除债券抵押比例
#define  FUN_SECU_DEL_QSDMDZ             164006  //删除交易所的券商代码对照
#define  FUN_SECU_DEL_SFBZ_WT            164007  //删除委托收费参数
#define  FUN_SECU_DEL_SECUCODE           164008  //删除证券代码
#define  FUN_SECU_DEL_HGCS               164009  //删除回购参数
#define  FUN_SECU_DEL_SFBZ_ZHYW          164010  //删除帐户类业务收费标准
#define  FUN_SECU_DEL_JYR                164011  //删除某年交易日参数
#define  FUN_SECU_DEL_SBWTH              164012  //删除申报委托号参数定义
#define  FUN_SECU_DEL_RCZH               164013  //删除容错帐户参数定义
#define  FUN_SECU_DEL_CYLXZCS            164014  //删除持有量限制参数
#define  FUN_SECU_DEL_JYPZXZCS           164015  //删除交易品种限制参数
#define  FUN_SECU_DEL_HGCS_MD            164016  //删除买断式回购参数
#define  FUN_SECU_DEL_SBQQ_ZQDM          164017  //删除三板确权证券代码
#define  FUN_SECU_DEL_SBQQ_DBJKLJ        164018  //删除代办券商业务接口路径
#define  FUN_SECU_DEL_SBQQ_ZBJKLJ        164019  //删除主办券商业务接口路径
#define  FUN_SECU_DEL_SBXWCS             164020  //删除申报席位参数
#define  FUN_SECU_DEL_SBSJCS             164021  //删除申报时间限制参数
#define  FUN_SECU_DEL_SBTDCS             164022  //删除申报通道参数
#define  FUN_SECU_DEL_SBZJFP             164023  //删除申报主机分配参数
#define  FUN_SECU_DEL_HBTDCS             164024  //删除回报通道参数
#define  FUN_SECU_DEL_ZQDMEX             164025  //删除证券代码扩展信息
#define  FUN_SECU_DEL_QZXX               164027  //删除权证信息
#define  FUN_SECU_DEL_SBXWZCS            164028  //删除申报席位组参数
#define  FUN_SECU_DEL_ZQLB               164052  //删除证券类别参数
#define  FUN_SECU_DEL_ZQJYSX             164053  //删除证券交易属性参数
#define  FUN_SECU_DEL_JYXZJB_KH          164057  //删除客户交易限制级别
#define  FUN_SECU_DEL_JYXZJB_QZ          164058  //删除群组交易限制级别
#define  FUN_SECU_DEL_CYLXZCS_KH         164104  //删除客户证券持有量上限
#define  FUN_SECU_DEL_ETFXX              164105  //删除ETF基本信息
#define  FUN_SECU_DEL_ETFCFGXX           164106  //删除ETF成份股信息
#define  FUN_TZLC_UNREG_ZHXX             164303  //注销合作理财客户信息
#define  FUN_TZLC_DEL_JZHZXX             164305  //删除集中划转信息
#define  FUN_SECU_DEL_RATE_BZYJ          164501  //删除标准一级清算费率
#define  FUN_SECU_DEL_RATE_BZEJ          164502  //删除标准二级清算费率
#define  FUN_SECU_DEL_RATE_HGYJ          164503  //删除回购一级清算费率
#define  FUN_SECU_DEL_RATE_HGEJ          164504  //删除回购二级清算费率
#define  FUN_SECU_DEL_YJDJ_DJCLID        164601  //删除佣金定价策略
#define  FUN_SECU_DEL_YJDJ_ZKCS          164602  //删除佣金定价折扣参数
#define  FUN_SECU_DEL_YJDJ_JYLFD         164603  //删除交易量分段参数
#define  FUN_SECU_DEL_YJDJ_ZCFD          164604  //删除资产分段参数
#define  FUN_SECU_DEL_YJFHCS_KH          164605  //删除客户佣金返还参数
#define  FUN_SECU_DEL_YJFHCS_QZ          164606  //删除群组佣金返还参数
#define  FUN_SECU_DEL_YJFHCS_JJR         164607  //删除经纪人佣金返还参数
#define  FUN_SECU_DEL_BZQZKL_KH          164608  //删除客户标准券折扣率参数
#define  FUN_SECU_DEL_YJDJ_KHTJSX        164611  //删除客户统计属性信息
#define  FUN_SECU_DEL_YJDJDX_KH          164612  //删除客户佣金定价策略
#define  FUN_SECU_DEL_YJDJDX_QZ          164613  //删除群组佣金定价策略
#define  FUN_SECU_DEL_YJDJDX_JJR         164614  //删除经纪人佣金定价策略
#define  FUN_SECU_DEL_WTFSYJZK           164619  //删除委托方式佣金折扣参数
#define  FUN_SECU_DEL_YJFHMX             164621  //删除佣金返还明细
#define  FUN_SECU_ROLL_YJFHMX            164622  //回滚佣金返还明细数据
#define  FUN_OFS_DEL_TAXX                165001  //删除基金公司参数
#define  FUN_OFS_DEL_JJXX                165002  //删除基金代码参数
#define  FUN_OFS_DEL_JJYWDM              165003  //删除基金业务代码
#define  FUN_OFS_DEL_TAYWDM              165004  //删除基金公司支持的业务代码
#define  FUN_OFS_DEL_JKMK                165005  //删除接口模块参数
#define  FUN_OFS_DEL_JKPZCS              165006  //删除基金接口配置参数
#define  FUN_OFS_DEL_ZKL                 165007  //删除基金折扣参数
#define  FUN_OFS_DEL_DQDESGPZ            165008  //删除定期定额申购品种
#define  FUN_OFS_DEL_DQDESGXY            165009  //中止客户定期定额申购协议
#define  FUN_OFS_DEL_JYR                 165011  //删除某年基金交易日参数
#define  FUN_EXEC_TEST                   179997  //单行业执行测试
#define  FUN_MORE_TEST                   179998  //多行返回测试
#define  FUN_GET_TEST                    179999  //单行查询测试
#define  FUN_USER_REGLOG                 190001  //记录柜员日志
#define  FUN_USER_LOGIN                  190003  //用户登录(柜员、经纪人或客户经理)
#define  FUN_USER_LOGOUT                 190004  //用户退出
#define  FUN_USER_MODI_PWD               190006  //用户密码修改
#define  FUN_USER_RESET_PWD              190007  //重置用户密码
#define  FUN_USER_MODI_MRDLSF            190008  //更改登录ID默认的登录身份
#define  FUN_CUSTOM_CHKTRDPWD            190101  //交易密码效验
#define  FUN_CUSTOM_CHKFUNDPWD           190102  //资金密码效验
#define  FUN_CUSTOM_CHKSQCZRPWD          190103  //授权操作人密码校验
#define  FUN_CUSTOM_GETUSERBYCARDNO      190110  //根据卡号获取系统用户信息
#define  FUN_FILE_GET_INFO               191001  //查询文件信息
#define  FUN_FILE_DOWNLOAD               191002  //文件下载
#define  FUN_FILE_UPLOAD                 191003  //文件上传
#define  FUN_FILE_LIST_ALLINFO           191004  //查询指定目录全部文件信息
#define  FUN_CHECK_SYNC                  192001  //临柜业务复核
#define  FUN_CHECK_ASYNC                 192002  //后台审核业务
#define  FUN_CHECK_OPER_REQ              192003  //需要后台审核的业务申请
#define  FUN_CHECK_OPER_CANCEL           192004  //取消审核业务申请
#define  FUN_CHECK_MODI_STATUS           192005  //修改审核业务申请流水标志
#define  FUN_CHECK_OPER_DORESULT         192006  //提交审核业务结果
#define  FUN_ACCOUNT_CHECK_LLMBDX_KH     193001  //复核客户使用的利率模板
#define  FUN_ACCOUNT_CHECK_LLMBDX_QZ     193002  //复核群组使用的利率模板
#define  FUN_ACCOUNT_CHECK_LXFHGL_KH     193003  //复核客户利息返还参数
#define  FUN_ACCOUNT_CHECK_LXFHGL_JJR    193004  //复核经纪人利息返还参数
#define  FUN_ACCOUNT_CHECK_KHQKMB_KH     193005  //复核客户取款模板
#define  FUN_ACCOUNT_CHECK_KHQKMB_QZ     193006  //复核群组取款模板
#define  FUN_SECU_CHECK_JYXZJB_KH        193007  //复核客户交易限制级别参数
#define  FUN_SECU_CHECK_JYXZJB_QZ        193008  //复核群组交易限制级别参数
#define  FUN_SECU_CHECK_YJDJDX_KH        193009  //复核客户佣金定价对象参数
#define  FUN_SECU_CHECK_YJDJDX_QZ        193010  //复核群组佣金定价对象参数
#define  FUN_SECU_CHECK_YJDJDX_JJR       193011  //复核经纪人佣金定价对象参数
#define  FUN_SECU_CHECK_YJFHCS_KH        193012  //复核客户佣金返还参数
#define  FUN_SECU_CHECK_YJFHCS_QZ        193013  //复核群组佣金返还参数
#define  FUN_SECU_CHECK_YJFHCS_JJR       193014  //复核经纪人佣金返还参数
#define  FUN_OFS_CHECK_ZKL               193015  //复核折扣参数设置
#define  FUN_SECU_CHECK_BZQZKL_KH        193018  //复核客户标准券折扣率参数
#define  FUN_SECU_CHECK_WTFSYJZK         193019  //复核委托方式佣金折扣参数
#define  FUN_ABOSS_CHECK_RIGHTBYMENUID   201001  //检查用户是否有对指定菜单的操作权限
#define  FUN_ABOSS_REMOVE_POST           201002  //员工岗位调动
#define  FUN_ABOSS_RZ                    201003  //基础系统日终处理
#define  FUN_ABOSS_ROLLBACK_RZ           201004  //基础系统日终回滚
#define  FUN_CUSTOM_REG                  202001  //客户信息登记
#define  FUN_CUSTOM_MODI_INFO            202002  //客户信息修改
#define  FUN_CUSTOM_DESTROY              202003  //客户销户
#define  FUN_CUSTOM_ADD_EXINFO           202004  //客户附加属性登记
#define  FUN_CUSTOM_MODI_EXINFO          202005  //客户附加属性修改
#define  FUN_CUSTOM_DEL_EXINFO           202006  //客户附加属性删除
#define  FUN_CUSTOM_ADD_SQCZR            202007  //客户授权操作人登记
#define  FUN_CUSTOM_MODI_SQCZR           202008  //客户授权操作人信息修改
#define  FUN_CUSTOM_DEL_SQCZR            202009  //客户授权操作人注销
#define  FUN_CUSTOM_MODI_TRDPWD          202010  //客户交易密码修改
#define  FUN_CUSTOM_RESET_TRDPWD         202011  //重置客户交易密码
#define  FUN_CUSTOM_MODI_FUNDPWD         202012  //客户资金密码修改
#define  FUN_CUSTOM_RESET_FUNDPWD        202013  //重置客户资金密码
#define  FUN_CUSTOM_REG_JGKHXX           202014  //机构客户信息登记
#define  FUN_CUSTOM_MODI_JGKHXX          202015  //机构客户信息修改
#define  FUN_CUSTOM_DESTROYCHECKFORXHXD  202016  //销户向导进入检查(是否可以进入销户向导销户)
#define  FUN_CUSTOM_RESET_SQCZRPWD       202017  //重置客户授权操作人密码
#define  FUN_CUSTOM_MODI_WTFS            202018  //客户允许委托方式设置
#define  FUN_CUSTOM_MODI_KZSX            202019  //客户控制属性设置
#define  FUN_CUSTOM_MODI_KHKH            202020  //客户卡信息修改
#define  FUN_CUSTOM_MODI_KHZT            202021  //客户状态修改
#define  FUN_CUSTOM_REG_CZMX             202022  //记录客户资料变更明细
#define  FUN_CUSTOM_SET_GSFL             202023  //设置客户公司分类
#define  FUN_CUSTOM_SET_KHQZ             202024  //设置客户群组
#define  FUN_CUSTOM_SET_FXJB             202025  //设置客户风险级别
#define  FUN_CUSTOM_REG_BROKERCUSTOM     202026  //经纪人客户登记
#define  FUN_CUSTOM_DEL_BROKERCUSTOM     202027  //删除经纪人客户
#define  FUN_CUSTOM_MOVE_BROKERCUSTOM    202028  //经纪人客户迁移
#define  FUN_CUSTOM_REG_ZYSY             202029  //资源使用登记
#define  FUN_CUSTOM_ADD_ZCFZ             202030  //新增客户资产分组定义
#define  FUN_CUSTOM_DEL_ZCFZ             202031  //删除客户资产分组定义
#define  FUN_CUSTOM_REG_GIFT             202033  //客户礼品领用登记
#define  FUN_CUSTOM_RESTORE_GIFT         202034  //客户归还领用的礼品
#define  FUN_CUSTOM_CANCEL_DESTROY       202035  //取消客户销户
#define  FUN_CUSTOM_REQ_KZYW             202036  //客户扩展业务登记申请
#define  FUN_CUSTOM_ADD_KZYW_PARAM       202037  //添加扩展业务申请参数
#define  FUN_CUSTOM_PROC_KZYW_REQ        202038  //处理自定义扩展业务申请
#define  FUN_CUSTOM_REG_KHJLCUSTOM       202040  //客户经理客户登记
#define  FUN_CUSTOM_DEL_KHJLCUSTOM       202041  //删除客户经理客户
#define  FUN_CUSTOM_MOVE_KHJLCUSTOM      202042  //客户经理客户迁移
#define  FUN_CUSTOM_ADD_MYXZ             202043  //客户开通漫游业务
#define  FUN_CUSTOM_MODI_MYXZ            202044  //修改客户漫游限制参数
#define  FUN_CUSTOM_DEL_MYXZ             202045  //删除客户漫游限制参数
#define  FUN_CUSTOM_ADD_DZDCS            202046  //客户邮寄对帐单参数登记
#define  FUN_CUSTOM_MODI_DZDCS           202047  //客户邮寄对帐单参数修改
#define  FUN_CUSTOM_DEL_DZDCS            202048  //删除客户邮寄对帐单参数
#define  FUN_CUSTOM_RZ                   202049  //客户系统日终处理
#define  FUN_CUSTOM_MODI_MMTBBZ          202050  //修改客户密码同步标志
#define  FUN_CUSTOM_ROLLBACK_RZ          202051  //客户系统日终回滚
#define  FUN_CUSTOM_BAT_KTWTFS           202101  //批量委托方式开通
#define  FUN_CUSTOM_BAT_QXWTFS           202102  //批量委托方式取消
#define  FUN_CUSTOM_BAT_KTFWXM           202103  //批量服务项目开通
#define  FUN_CUSTOM_BAT_QXFWXM           202104  //批量服务项目取消
#define  FUN_CUSTOM_BAT_SETKHQZ          202105  //批量客户群组设置
#define  FUN_CUSTOM_BAT_SETGSFL          202106  //批量客户公司分类设置
#define  FUN_CUSTOM_BAT_DJKHKZSX         202107  //批量客户控制属性登记
#define  FUN_CUSTOM_BAT_QXKHKZSX         202108  //批量客户控制属性取消
#define  FUN_CUSTOM_REGCERTIMAGE         202201  //登记客户和代理人证件影像资料
#define  FUN_CUSTOM_MODICERTIMAGE        202202  //更新修改客户或代理人证件影像资料
#define  FUN_CUSTOM_DELCERTIMAGE         202203  //删除客户或代理人证件影像资料
#define  FUN_CUSTOM_REGFINGERPRINT       202204  //登记客户或代理人指纹信息
#define  FUN_CUSTOM_DELFINGERPRINT       202205  //删除客户或代理人指纹信息
#define  FUN_ACCOUNT_REG_ZJZH            203001  //资金帐号开户
#define  FUN_ACCOUNT_MODI_INFO           203002  //资金帐户信息修改
#define  FUN_ACCOUNT_DESTROY             203003  //资金帐户销户
#define  FUN_ACCOUNT_SET_MAIN            203008  //设置某币种主资金帐号
#define  FUN_ACCOUNT_FREEZE_ZJZH         203009  //冻结资金帐户
#define  FUN_ACCOUNT_UNFREEZE_ZJZH       203010  //解冻资金帐户
#define  FUN_ACCOUNT_CANCEL_DESTROY      203011  //取消资金帐户销户
#define  FUN_ACCOUNT_MODI_ZHYWFW         203012  //资金帐户业务范围修改
#define  FUN_ACCOUNT_MODI_ZJZHSX         203013  //资金帐户控制属性修改
#define  FUN_ACCOUNT_REG_ZPFKZH          203014  //登记客户支票付款帐户
#define  FUN_ACCOUNT_DESTROY_ZPFKZH      203015  //注销客户支票付款帐户
#define  FUN_ACCOUNT_MODI_ZCFZ           203016  //修改资金帐户所属资产分组
#define  FUN_ACCOUNT_RZ                  203017  //资金系统日终处理
#define  FUN_ACCOUNT_ROLLBACK_RZ         203018  //资金系统日终回滚
#define  FUN_WBZH_REG_ASSOCIATE_REQ      203101  //登记银证转帐对应关系申请
#define  FUN_WBZH_PROC_ASSOCIATE_REG     203102  //处理银证转帐对应关系登记
#define  FUN_WBZH_DESTROY_ASSOCIATE_REQ  203103  //注销银证转帐对应关系申请
#define  FUN_WBZH_PROC_ASSOCIATE_ZX      203104  //处理银证转帐对应注销
#define  FUN_WBZH_MODI_ASSOCIATEINFO     203105  //修改银证转帐对应信息(改为修改银证对应关系状态)
#define  FUN_WBZH_REG_ASSOCIATE_EXTEND   203106  //外部登记银证转帐对应关系申请
#define  FUN_WBZH_DESTROY_ASSOCIATE_EXT  203107  //外部取消银证转帐对应关系申请
#define  FUN_WBZH_TRANSFER_SAVING_ZQ     203111  //证券发起资金存入(银行转证券)
#define  FUN_WBZH_PROC_SAVING_ZQ         203112  //处理证券发起资金转入(银转证)业务
#define  FUN_WBZH_TRANSFER_TAKEOUT_ZQ    203113  //证券发起资金取出(证券转银行)
#define  FUN_WBZH_PROC_TAKEOUT_ZQ        203114  //处理证券发起资金取出(证转银)业务
#define  FUN_WBZH_RECALL_TRANSFER_ZQ     203115  //证券发起冲正转帐业务申请
#define  FUN_WBZH_PROC_RECALLTRAN_ZQ     203116  //处理证券发起冲正转帐业务
#define  FUN_WBZH_VERIFY_TRANSFER_ZQ     203117  //证券发起交易核实申请
#define  FUN_WBZH_PROC_VERIFYTRAN_ZQ     203118  //处理证券发起的转帐交易核实业务
#define  FUN_WBZH_REQ_BALANCE_ZQ         203119  //证券发起查询外部转帐帐户余额申请
#define  FUN_WBZH_PROC_BALANCE_ZQ        203120  //处理证券发起查询转帐帐户余额业务
#define  FUN_WBZH_REQ_YWKZZL_ZQ          203121  //证券发起业务控制指令申请
#define  FUN_WBZH_PROC_YWKZZL_ZQ         203122  //处理证券发起业务控制指令
#define  FUN_WBZH_TRANSFER_SAVING_WB     203131  //处理外部发起资金转入(银转证)业务
#define  FUN_WBZH_RECALLTRAN_SAVING_WB   203132  //处理外部发起冲销资金转入(银转证)业务
#define  FUN_WBZH_TRANSFER_TAKEOUT_WB    203133  //处理外部发起资金转出(证转银)业务
#define  FUN_WBZH_RECALLTRAN_TAKEOUT_WB  203134  //处理外部发起冲销资金转出(证转银)业务
#define  FUN_WBZH_GET_BALANCE_WB         203135  //处理外部发起查询本地保证金帐户余额
#define  FUN_WBZH_YWKZZL_WB              203136  //处理外部发起业务控制指令
#define  FUN_WBZH_ADD_ZZMXDZZL           203151  //添加银证转帐明细对帐资料
#define  FUN_WBZH_DEL_ZZMXDZZL           203152  //删除银证转帐明细对帐资料
#define  FUN_WBZH_VERIFY_ZZMXDZZL        203153  //勾对银证转帐明细对帐资料
#define  FUN_WBZH_ZJJYSQ_COMMIT          203154  //证券发起资金交易申请处理提交
#define  FUN_WBZH_ADD_WBZJQS             203155  //增加外部资金清算
#define  FUN_WBZH_DEL_WBZJQS             203156  //删除外部资金清算
#define  FUN_WBZH_LIST_WBZJQS            203157  //查询外部资金清算
#define  FUN_WBZH_ADJUST_TRANSFER_TRADE  203161  //转帐业务手工调帐
#define  FUN_WBZH_PATCH_TRANSFER_TRADE   203162  //转帐业务手工补帐
#define  FUN_WBZH_YZT_GET_BALANCE        203201  //查询外部结算类帐户余额
#define  FUN_WBZH_REG                    203251  //登记外部帐户
#define  FUN_WBZH_DESTROY                203252  //注销外部帐户
#define  FUN_WBZH_MODI_INFO              203253  //修改外部帐户资料
#define  FUN_WBZH_MODI_ZHYWFW            203254  //修改外部帐户业务范围
#define  FUN_WBZH_CHKTRDPWD              203261  //校验外部帐户密码
#define  FUN_FUND_ACCESS                 203501  //资金存取
#define  FUN_FUND_INSIDE_TRANSFER        203502  //资金内部划转
#define  FUN_FUND_ADJUST_BALANCE         203503  //资金红冲兰补
#define  FUN_FUND_FORCETAKEOUT           203504  //强制取款
#define  FUN_FUND_FREEZE                 203505  //资金冻结
#define  FUN_FUND_UNFREEZE               203506  //资金解冻
#define  FUN_FUND_ACCRUALTOBALANCE       203507  //单一资金帐户利息入帐
#define  FUN_FUND_OVERDRAFT_ACCRUAL      203508  //单一资金帐户透支罚息
#define  FUN_FUND_DESTROY_FORCETAKEOUT   203510  //销户强制取款
#define  FUN_FUND_REQBUY_AHEADUNFREEZE   203511  //申购资金提前解冻
#define  FUN_FUND_RECALL_ACCESS          203512  //冲销本日存取业务
#define  FUN_FUND_RECALL_FREEZE          203513  //撤销资金冻结(解冻)业务
#define  FUN_FUND_CHECK_ACCESS           203514  //资金存取检查
#define  FUN_FUND_BOOKING_ACCESS         203515  //超额存取预约
#define  FUN_FUND_ADJUST_ACCRUALSTORE    203516  //调整利息积数
#define  FUN_FUND_ADJUST_OVERDRAFTSTORE  203517  //调整罚息积数
#define  FUN_FUND_ADJUST_CHEQUEBALANCE   203518  //修改支票金额
#define  FUN_FUND_ADJUST_TODAYSAVINGSUM  203519  //调整当日存款金额
#define  FUN_FUND_NOCHKACCRUALTOBALANCE  203520  //单一资金帐户利息入帐(不审核)
#define  FUN_FUND_SETTLE_ACCRUALNOTOBAL  203521  //单一资金帐户结息不入帐
#define  FUN_FUND_PSZQ_FREEZE            203523  //配售中签资金冻结
#define  FUN_FUND_XYSYJXRZ               203524  //客户信用使用结息入帐
#define  FUN_FUND_CAL_SXKQZJ             203525  //计算受资金存取参数控制后的可取资金
#define  FUN_FUND_TRANSFER               203551  //资金划拨(仅供清算使用)
#define  FUN_FUND_RECALL_FREEZE_NOAUDIT  203552  //撤销资金冻结解冻(清算)
#define  FUN_FUND_TRANSWJS               203553  //未交收资金划拔(清算)
#define  FUN_FUND_QSFREEZE               203554  //清算资金冻结解冻(清算)
#define  FUN_FUND_RECALL_TRANSFER        203555  //撤销资金划拨
#define  FUN_FUND_RECALL2_FREEZE         203556  //反撤销资金冻结解冻(清算)
#define  FUN_SECU_REG_GDH                204001  //股东帐号登记
#define  FUN_SECU_MODI_GDH               204002  //股东帐户信息修改
#define  FUN_SECU_DESTROY_GDH            204003  //股东帐户注销
#define  FUN_SECU_MOVE_GDH               204004  //股东帐户迁移
#define  FUN_SECU_SET_MAINGDH            204005  //设置主股东帐号
#define  FUN_SECU_FREEZE_GDH             204006  //股东帐户冻结
#define  FUN_SECU_UNFREEZE_GDH           204007  //股东帐户解冻
#define  FUN_SECU_LOSS_GDH               204008  //股东帐号挂失
#define  FUN_SECU_UNCHAIN_LOSS_GDH       204009  //股东帐户解挂
#define  FUN_SECU_MODI_GDH_SQUARE_ACC    204010  //股东结算帐户变更
#define  FUN_SECU_DESTROY_NB_GDH         204011  //股东帐户内部注销
#define  FUN_SECU_MODI_GDH_ZCFZ          204012  //修改股东帐号资产分组
#define  FUN_SECU_REG_FWXM_GDH           204013  //股东帐户服务项目登记
#define  FUN_SECU_DESTROY_FWXM_GDH       204014  //取消股东帐户已经开通的服务项目
#define  FUN_SECU_REG_JGRQ               204015  //记录客户交割日期
#define  FUN_SECU_CDHB                   204103  //撤单回报处理
#define  FUN_SECU_SBQR                   204104  //申报确认处理
#define  FUN_SECU_SSQS                   204105  //实时清算处理
#define  FUN_SECU_ENT_DECLARE_COMMIT     204106  //委托申报提交
#define  FUN_SECU_ENT_DECLARE_ROLLBACK   204107  //委托申报回滚
#define  FUN_SECU_REG_ENTRUST_SBJLH      204108  //记录委托申报记录号
#define  FUN_SECU_CJHBYCCL               204114  //成交回报异常处理
#define  FUN_SECU_CLOSE_TRADE            204150  //收盘作业
#define  FUN_SECU_TRADE_INIT             204151  //证券交易系统初始化
#define  FUN_SECU_PURGE_ONE_HOLDSTOCK    204201  //清除股东某证券所有持仓
#define  FUN_SECU_GFRZ                   204202  //股份入帐
#define  FUN_SECU_RECALL_WTGFDJ          204203  //撤销委托股份冻结
#define  FUN_SECU_MODI_DJSQSZL           204204  //修改待交收记录
#define  FUN_SECU_ADD_WKHQY              204205  //登记未开户权益
#define  FUN_STOCK_MODI_ZQYE             204206  //调整股份余额
#define  FUN_STOCK_QSFREEZE              204207  //清算证券冻结
#define  FUN_SECU_RECALL_GFRZ            204208  //撤销股份入帐
#define  FUN_SECU_DEL_WKHQY              204209  //删除未开户权益
#define  FUN_STOCK_RECALL_QSFREEZE       204210  //撤销清算证券冻结
#define  FUN_STOCK_RECALL_ZQYE           204211  //撤销调整股份余额
#define  FUN_SECU_RECALL_CXWTGFDJ        204212  //撤销委托股份冻结回滚
#define  FUN_TZLC_PROC_LCDHZJFH          204301  //理财合作单户资金集中返还
#define  FUN_TZLC_PROC_LCDHZJCQ          204302  //理财单户资金存取
#define  FUN_SECU_RZ                     204401  //证券交易系统日终处理
#define  FUN_SECU_ROLLBACK_RZ            204402  //证券交易系统日终回滚
#define  FUN_SECU_ENTRUST_TRADE          204501  //股票买卖委托
#define  FUN_SECU_ENTRUST_WITHDRAW       204502  //股票委托撤单
#define  FUN_SECU_CAL_TRADEABLEAMOUNT    204503  //可买卖数量计算
#define  FUN_SECU_CHANGE_TRUSTEE_SZ      204504  //深圳转托管
#define  FUN_SECU_BOOKING_ENTRUST_BUY    204505  //预约买入委托
#define  FUN_SECU_BOOKING_ENTRUST_SALE   204506  //预约卖出委托
#define  FUN_SECU_REPAIR_ENTRUST_BUY     204507  //补单买入委托
#define  FUN_SECU_REPAIR_ENTRUST_SALE    204508  //补单卖出委托
#define  FUN_SECU_BAT_ENTRUST_BUY        204509  //批量买入委托
#define  FUN_SECU_BAT_ENTRUST_SALE       204510  //批量卖出委托
#define  FUN_SECU_BAT_ENTRUST_WITHDRAW   204511  //批量撤单
#define  FUN_SECU_RECALL_ENTRUST_QUOTA   204512  //上海配售委托冲正
#define  FUN_SECU_RECALLTRUST_ENTRUST_A  204514  //上海A股撤销指定
#define  FUN_SECU_RECALLTRUST_ENTRUST_H  204515  //上海回购撤销指定
#define  FUN_SECU_TRUST_ENTRUST_B        204516  //上海B股指定交易
#define  FUN_SECU_RECALLTRUST_ENTRUST_B  204517  //上海B股撤指交易
#define  FUN_SECU_ETF_BOOKING_ENTRUST    204518  //ETF网下预约委托
#define  FUN_SECU_ETF_CXBOOKING_ENTRUST  204519  //ETF网下预约委托撤单
#define  FUN_SECU_CALACCRUAL_HGRZRQ      204520  //计算回购利息(融资,融券)
#define  FUN_SECU_ETF_XGWXRGSBJG         204521  //修改ETF网下认购申报结果
#define  FUN_SECU_CAL_TRADERATE          204522  //计算清算费率
#define  FUN_SECU_ETF_WSSB               204523  //ETF网下委托网上申报
#define  FUN_STOCK_ADJUST_DECREASE       204601  //股份转出
#define  FUN_STOCK_ADJUST_INCREASE       204602  //股份转入
#define  FUN_STOCK_ADJUST_SALEABLE       204603  //调整可卖数量
#define  FUN_STOCK_ADJUSTINC_UNSALEABLE  204604  //非流通股份转入
#define  FUN_STOCK_ADJUSTDEC_UNSALEABLE  204605  //非流通股份转出
#define  FUN_STOCK_TIMELESS_FREEZE       204606  //股份长期冻结
#define  FUN_STOCK_RECALLTIMELESSFREEZE  204607  //撤销股份长期冻结
#define  FUN_STOCK_ADJINC_STANDARDBOND   204608  //标准券转入
#define  FUN_STOCK_ADJDEC_STANDARDBOND   204609  //标准券转出
#define  FUN_STOCK_ADJUST_HOLDCOST       204610  //持仓成本调整
#define  FUN_STOCK_SWITCH_CODE           204611  //证券代码转换
#define  FUN_STOCK_ADD_PSSZ              204612  //添加上海配售市值(799990)
#define  FUN_STOCK_DEL_PSSZ              204613  //删除上海配售市值(799990)
#define  FUN_STOCK_PSSZ_SWITCH_PSQY      204614  //配售市值转换成配售权益
#define  FUN_STOCK_BONDTOBZQ             204616  //债券抵押转标准券
#define  FUN_STOCK_RECALL_BONDTOBZQ      204617  //撤销债券抵押转标准券
#define  FUN_SBQQ_FRONTFOR_REG           204701  //主办三板确权登记申请
#define  FUN_SBQQ_FRONTFOR_TRANSFER      204702  //主办三板确权过户申请
#define  FUN_SBQQ_FRONTFOR_IMPORT_GDMC   204711  //主办股东名册导入
#define  FUN_SBQQ_FRONTFOR_FREEZE_GDMC   204712  //主办股东名册股份冻结解冻
#define  FUN_SBQQ_FRONTFOR_DEL_GDMC      204713  //主办股东名册删除
#define  FUN_SBQQ_AGENT_REG              204751  //代办三板确权登记申请
#define  FUN_SBQQ_AGENT_TRANSFER         204752  //代办三板确权过户申请
#define  FUN_OFS_JJZHKH                  205001  //基金帐户开户
#define  FUN_OFS_JJZHWT                  205002  //基金帐户类委托(销户，冻结，解冻，挂失，解挂)
#define  FUN_OFS_JJZHXXXG                205003  //基金帐户信息修改
#define  FUN_OFS_JJZHKZSXXG              205005  //基金帐户控制属性修改
#define  FUN_OFS_MODI_JJZH_ZCFZ          205006  //修改基金帐户资产分组信息
#define  FUN_OFS_MODI_JJZHJSZH           205008  //修改基金帐户结算帐户
#define  FUN_OFS_CXYYWT                  205010  //撤消基金预约委托
#define  FUN_OFS_CXDRWT                  205011  //撤消基金当日委托
#define  FUN_OFS_CXMCDJ                  205012  //撤销基金卖出冻结份额
#define  FUN_OFS_RECALL_CXMCDJ           205013  //撤销基金卖出冻结回滚
#define  FUN_OFS_JJWT                    205021  //基金认购申购赎回
#define  FUN_OFS_SZFHFS                  205022  //设置分红方式
#define  FUN_OFS_ZTG                     205023  //基金转托管
#define  FUN_OFS_NBZTG                   205024  //开放式基金内部转托管
#define  FUN_OFS_JJZH                    205025  //基金转换
#define  FUN_OFS_JJYW                    205026  //开放式基金份额存取(本地)
#define  FUN_OFS_XGSBJG_JJWT             205027  //修改基金委托申报结果
#define  FUN_OFS_XGSBJG_ZHWT             205028  //修改帐户委托申报结果
#define  FUN_OFS_INS_GGXX                205029  //插入公告信息
#define  FUN_OFS_DEL_GGXX                205030  //删除公告信息
#define  FUN_OFS_JJJG                    205034  //开放式基金交割入帐
#define  FUN_OFS_RECALL_JJJG             205035  //冲销基金入帐
#define  FUN_OFS_MODI_DJSQSZL            205036  //修改待交收清算资料
#define  FUN_OFS_JJDJ                    205038  //基金冻结解冻(TA，长期)
#define  FUN_OFS_TRADE_INIT              205100  //基金交易系统初始化
#define  FUN_OFS_CLOSE_TRADE             205101  //基金系统收盘作业
#define  FUN_OFS_XTRZ                    205102  //基金系统日终
#define  FUN_OFS_ROLLBACK_XTRZ           205103  //基金系统日终回滚
#define  FUN_DATACENTER_DEL_ZQHQ         299001  //删除历史证券行情
#define  FUN_DATACENTER_ADD_ZQHQ         299002  //添加历史证券行情
#define  FUN_DATACENTER_DEL_OF_JJJZ      299003  //删除历史基金净值
#define  FUN_DATACENTER_ADD_OF_JJJZ      299004  //添加历史基金净值
#define  FUN_ABOSS_LIST_USERLOG          301001  //查询柜员日志
#define  FUN_ABOSS_LIST_YWSHSQBYKHH      301002  //按客户号查询业务审核申请流水
#define  FUN_ABOSS_LIST_ALLUSERLOG       301101  //全体当日柜员日志查询
#define  FUN_ABOSS_LIST_ALLYWSHSQ        301102  //全体业务审核申请流水查询
#define  FUN_CUSTOM_GET_CUSTINFO         302001  //查询指定客户基本信息
#define  FUN_CUSTOM_LIST_SQCZR           302002  //查询指定客户所有授权操作人信息
#define  FUN_CUSTOM_LIST_DZDCS           302003  //查询客户邮寄对帐单参数
#define  FUN_CUSTOM_GET_KHMYXZ           302004  //查询客户漫游业务限制
#define  FUN_CUSTOM_LIST_EXINFO          302005  //查询客户附加属性
#define  FUN_CUSTOM_LIST_CUSTGIFTINFO    302006  //查询客户礼品信息
#define  FUN_CUSTOM_GET_JGKHXX           302007  //查询机构客户信息
#define  FUN_CUSTOM_GETKHHBYKHKH         302008  //根据客户卡号查询客户号
#define  FUN_CUSTOM_LIST_JJRKH           302009  //查询全体经纪人客户基本对应关系
#define  FUN_CUSTOM_LIST_BROKERCUSTOM    302010  //查询经纪人客户
#define  FUN_CUSTOM_GET_BROKERCUSTCOUNT  302011  //获取指定经纪人下挂的客户数
#define  FUN_CUSTOM_LIST_KHJLCUSTOM      302012  //查询客户经理的客户列表
#define  FUN_CUSTOM_GET_KHJLCUSTCOUNT    302013  //获取指定客户经理的客户数
#define  FUN_CUSTOM_LIST_KHJLKH          302014  //全体客户经理与客户对应关系
#define  FUN_CUSTOM_LISTKHHBYZJBH        302015  //根据证件编号查询客户号
#define  FUN_CUSTOM_GETKHH_BLURRY        302016  //模糊查询客户号
#define  FUN_CUSTOM_GETKHHBYZJBH         302017  //根据证件编号查询客户号
#define  FUN_CUSTOM_LIST_CZMX            302018  //客户资料变更明细查询
#define  FUN_CUSTOM_LIST_KZYWSQ          302019  //查询客户申请的扩展业务
#define  FUN_CUSTOM_LIST_PARAM_KZYWSQ    302020  //查询客户已申请的扩展业务内容
#define  FUN_CUSTOM_LIST_ZCFZ            302021  //查询客户的所有资产分组
#define  FUN_CUSTOM_LIST_DRCZMXBYYWSQH   302022  //根据审核业务申请号查询当日资料变更明细
#define  FUN_CUSTOM_LIST_JJR             302023  //查询客户所属经纪人
#define  FUN_CUSTOM_LIST_KHJL            302024  //查询客户所属客户经理
#define  FUN_CUSTOM_LIST_BROKER_DRCZMX   302030  //查询经纪人客户当日资料变更明细
#define  FUN_CUSTOM_LIST_KHJL_DRCZMX     302040  //查询客户经理客户当日资料变更明细
#define  FUN_CUSTOMER_COLLECT_ASSET      302050  //客户资产汇总查询
#define  FUN_CUSTOM_GET_ASSET_RMB        302051  //查询客户折合人民币后的资产情况
#define  FUN_CUSTOM_GET_SQCZRXX          302101  //查询指定授权操作人详细信息
#define  FUN_CUSTOM_LIST_ALLCUSTOMINFO   302201  //查询全体客户信息
#define  FUN_CUSTOM_LIST_ALLSQCZR        302202  //查询全体客户授权操作人
#define  FUN_CUSTOM_LIST_ALLJGKHXX       302203  //全体机构客户信息查询
#define  FUN_CUSTOM_LIST_ALLEXINFO       302205  //查询全体客户附加属性
#define  FUN_CUSTOM_LIST_ALLGIFTINFO     302206  //查询全体客户礼品信息
#define  FUN_CUSTOM_LIST_ALLDRCZMX       302207  //全体当日客户资料变更明细查询
#define  FUN_CUSTOM_LIST_ALLKZYWSQ       302208  //查询全体已申请的自定义扩展业务
#define  FUN_CUSTOM_LIST_ALLDZDCS        302209  //查询全体客户邮寄对帐单参数
#define  FUN_CUSTOM_COLLECT_KZYW         302301  //查询营业部扩展业务汇总
#define  FUN_CUSTOM_COLLECT_WTFS         302302  //查询营业部委托方式开通汇总
#define  FUN_CUSTOM_COLLECT_KHS          302303  //查询营业部客户开户情况
#define  FUN_ACCOUNT_GETZJXXBYZJZH       303001  //按资金帐号查询资金信息
#define  FUN_ACCOUNT_LIST_ZJXXBYKHH      303002  //按客户号查询资金
#define  FUN_ACCOUNT_GETKHHBYZJZH        303003  //根据资金帐号获取客户号
#define  FUN_ACCOUNT_LIST_ZJDJMX         303009  //查询客户资金冻结明细
#define  FUN_ACCOUNT_LIST_ZJMX           303010  //查询客户资金明细
#define  FUN_ACCOUNT_LIST_WJQXY          303011  //查询客户未结清信用
#define  FUN_ACCOUNT_LIST_ZJDJXMMX       303012  //查询客户资金冻结项目明细
#define  FUN_ACCOUNT_LIST_ZPFKZH         303013  //查询客户支票付款帐户
#define  FUN_ACCOUNT_LIST_DRZJMXBYYWSQH  303014  //根据审核业务申请号查询当日资金明细
#define  FUN_ACCOUNT_LIST_ZJDJMXBYYWSQH  303015  //根据审核业务申请号查询当日资金冻结明细
#define  FUN_ACCOUNT_LIST_ZHCQSQ         303016  //查询客户存取授权流水
#define  FUN_ACCOUNT_LIST_BROKER_ZJZH    303030  //查询经纪人客户的资金帐户
#define  FUN_ACCOUNT_LIST_BROKER_DRZJMX  303031  //查询经纪人客户的当日资金变动明细
#define  FUN_ACCOUNT_LIST_BROKER_ZJDJMX  303032  //查询经纪人客户的当日资金冻结明细
#define  FUN_ACCOUNT_LIST_KHJL_ZJZH      303040  //查询客户经理客户的资金帐户
#define  FUN_ACCOUNT_LIST_KHJL_DRZJMX    303041  //查询客户经理客户的当日资金变动明细
#define  FUN_ACCOUNT_LIST_KHJL_ZJDJMX    303042  //查询客户经理客户的当日资金冻结明细
#define  FUN_WBZH_GETKHHBYWBZH           303101  //根据外部帐号获取客户号
#define  FUN_WBZH_GETZJXXBYWBZH          303102  //按外部帐号查询资金信息
#define  FUN_WBZH_LIST_YZZZDY            303103  //查询银证转帐对应关系
#define  FUN_WBZH_LIST_WBZHBYKHH         303104  //按客户号查询外部帐户参考资金信息
#define  FUN_WBZH_GET_CUSTINFO           303105  //查询外部帐户资料
#define  FUN_WBZH_LIST_ZJJYSQ_ZQ         303111  //查询证券发起资金交易申请
#define  FUN_WBZH_LIST_ZJJYSQ_WB         303112  //查询外部发起资金交易申请
#define  FUN_WBZH_LIST_ALLZJJYSQ_ZQ      303151  //查询全体当日证券发起资金业务流水查询
#define  FUN_WBZH_LIST_ALLZJJYSQ_WB      303152  //查询全体当日外部发起资金交易申请
#define  FUN_WBZH_LIST_ZZMXDZZL          303153  //查询全体银证转帐明细对帐资料
#define  FUN_WBZH_LIST_ZZMX_ZQ           303154  //查询证券方当日转帐明细(对帐用)
#define  FUN_WBZH_LIST_ZZHZZL_ZQ         303155  //查询证券方当日转帐汇总资料(对帐用)
#define  FUN_ACCOUNT_LIST_ALLACCINFO     303201  //全体资金帐号查询
#define  FUN_ACCOUNT_LIST_ALLWBZH        303202  //全体外部帐号查询
#define  FUN_ACCOUNT_LIST_ALLYZZZDY      303203  //全体银证转帐对应查询
#define  FUN_ACCOUNT_LIST_ALLDRZJMX      303204  //全体当日资金明细查询
#define  FUN_ACCOUNT_LIST_ALLDRZJDJMX    303205  //全体当日资金冻结明细查询
#define  FUN_ACCOUNT_LIST_ALLASSETS      303206  //查询全体帐户资产
#define  FUN_ACCOUNT_LIST_ALLBDZJZH      303207  //全体当日变动的资金帐户数据
#define  FUN_ACCOUNT_LIST_ALLZHCQSQ      303208  //查询全体帐户存取授权流水
#define  FUN_ACCOUNT_LIST_ACCRUALBYYYB   303301  //营业部汇总利息查询
#define  FUN_ACCOUNT_COLLECT_FUND        303302  //查询营业部资金汇总
#define  FUN_ACCOUNT_COLLECT_YZZZKHS     303303  //查询营业部银证转帐登记汇总数
#define  FUN_ACCOUNT_COLLECT_TODAYOPER   303304  //查询营业部当前资金类业务汇总
#define  FUN_ACCOUNT_COLLECT_XYED        303305  //查询营业部信用使用情况汇总
#define  FUN_SECU_LIST_GDHBYKHH          304001  //客户股东号查询
#define  FUN_SECU_GET_GDHINFO            304002  //股东资料查询
#define  FUN_SECU_GETKHHBYGDH            304003  //根据股东帐号获取客户号
#define  FUN_SECU_LIST_BROKER_GDHINFO    304030  //查询经纪人客户的股东帐户信息
#define  FUN_SECU_LIST_BROKER_HOLDSTOCK  304031  //查询经纪人客户持仓信息
#define  FUN_SECU_LIST_BROKER_ENTRUST    304032  //查询经纪人客户当日委托信息
#define  FUN_SECU_LIST_BROKER_SSCJ       304033  //查询经纪人客户实时成交信息
#define  FUN_SECU_LIST_BROKER_BGJS       304035  //查询经纪人客户未交收的B股成交
#define  FUN_SECU_LIST_BROKER_WGHHG      304036  //查询经纪人客户未购回回购
#define  FUN_SECU_LIST_KHJL_GDHINFO      304040  //查询客户经理客户的股东帐户信息
#define  FUN_SECU_LIST_KHJL_HOLDSTOCK    304041  //查询客户经理客户持仓信息
#define  FUN_SECU_LIST_KHJL_ENTRUST      304042  //查询客户经理客户当日委托信息
#define  FUN_SECU_LIST_KHJL_SSCJ         304043  //查询客户经理客户实时成交信息
#define  FUN_SECU_LIST_KHJL_BGJS         304045  //查询客户经理客户未交收的B股成交
#define  FUN_SECU_LIST_KHJL_WGHHG        304046  //查询客户经理客户未购回回购
#define  FUN_SECU_LIST_SENTCOUNTBYXWH    304050  //取指定席位委托已申报笔数
#define  FUN_SECU_GET_BARGAINONINFO      304051  //获取指定回报通道成交信息
#define  FUN_SECU_LIST_ENTRUST_DECLARE   304053  //委托申报扫描
#define  FUN_SECU_LIST_HOLDSTOCK         304101  //客户持仓查询
#define  FUN_SECU_LIST_COLLECTHOLDSTOCK  304102  //客户持仓汇总查询
#define  FUN_SECU_LIST_ENTRUST           304103  //客户当日委托查询
#define  FUN_SECU_LIST_WGHHG             304104  //查询客户未购回回购
#define  FUN_SECU_LIST_SQUARE            304105  //客户交割流水查询(不含历史)
#define  FUN_SECU_LIST_YIELDFROMSTOCK    304106  //查询客户股票投资损益(证券库,无历史)
#define  FUN_SECU_LIST_BGJS              304107  //客户B股未交收查询
#define  FUN_SECU_LIST_ETFWXSGWT         304108  //查询客户ETF网下申购委托
#define  FUN_SECU_LIST_SSCJ              304109  //客户实时成交查询
#define  FUN_SECU_LIST_FBCJ              304110  //客户分笔成交查询
#define  FUN_SECU_LIST_HZCJ              304111  //客户实时成交汇总查询
#define  FUN_SECU_GET_DRJGMXBYYWSQH      304112  //根据业务申请号查询交割明细
#define  FUN_SECU_GET_DRWTBYYWSQH        304113  //根据业务申请号查询委托信息
#define  FUN_SECU_LIST_ZQDJXMMX          304115  //查询客户股份冻结项目明细
#define  FUN_SECU_GET_ENTRUSTINFO        304116  //查询指定委托号的委托详细信息
#define  FUN_SECU_LIST_PSZQSTOCK         304117  //查询客户配售中签股份
#define  FUN_SECU_GET_JGRQ               304118  //查询客户最近交割日期
#define  FUN_SECU_LIST_ETFCFG_HOLDSTOCK  304119  //查询客户ETF成份股的持仓信息
#define  FUN_SECU_LIST_HGTQJL            304120  //查询客户回购透券记录
#define  FUN_SECU_LIST_ALLGDHINFO        304201  //全体股东号查询
#define  FUN_SECU_LIST_ALLHOLDSTOCKINFO  304202  //全体持仓信息查询
#define  FUN_SECU_LIST_HZALLHOLDSTOCK    304203  //全体持仓汇总查询
#define  FUN_SECU_LIST_ALLFLTHOLDSTOCK   304204  //全体非流通持仓查询
#define  FUN_SECU_LIST_HALLFLTHOLDSTOCK  304205  //全体非流通股份汇总查询
#define  FUN_SECU_LIST_ALLEJGDHOLDSTOCK  304206  //全体二级股东持仓查询
#define  FUN_SECU_LIST_HZALLEJGDHGF      304207  //全体二级股东持仓股份汇总查询
#define  FUN_SECU_LIST_ALLWHGF           304208  //全体无户股份查询
#define  FUN_SECU_LIST_ALLKPSSL          304209  //全体可配售数量查询
#define  FUN_SECU_LIST_ALLPSZQ           304210  //全体配售中签信息查询
#define  FUN_SECU_LIST_ALLWJKQZ          304211  //全体未缴款权证查询
#define  FUN_SECU_GET_TODAYPSQYCOUNT     304212  //查询今日导入的配售权益记录条数
#define  FUN_SECU_LIST_ALLKPGSL          304213  //全体可配股数量查询
#define  FUN_SECU_LIST_HZALLHOLDSTOCK_X  304214  //全体持仓汇总查询(报表查询专用版)
#define  FUN_SECU_LIST_ALLHGTQJL         304215  //查询全体回购透券记录
#define  FUN_SECU_LIST_ALLTODAYENTRUST   304301  //全体当日委托查询
#define  FUN_SECU_LIST_HALLTODAYENTRUST  304302  //全日当日委托汇总查询
#define  FUN_SECU_LIST_ALLSSCJ           304303  //全体当日实时成交查询
#define  FUN_SECU_LIST_ALLFBCJ           304304  //全体当日分笔成交查询
#define  FUN_SECU_LIST_ALLHZCJ           304305  //全体当日成交汇总查询
#define  FUN_SECU_LIST_ALLBGJS           304306  //全体B股未交收查询
#define  FUN_SECU_LIST_ALLDRZQMX         304307  //全体当日证券流水查询
#define  FUN_SECU_LIST_ALLCJXS           304309  //全体成交显示信息查询
#define  FUN_SECU_LIST_ALLWGHHG          304310  //全体未购回回购查询
#define  FUN_SECU_LIST_HZALLWGHHG        304311  //全体未购回回购汇总查询
#define  FUN_SECU_LIST_ALLSQUARE         304312  //全体交割流水(不含历史)查询
#define  FUN_SECU_LIST_ALLDQSENTRUST     304314  //全体待清算委托查询
#define  FUN_SECU_LIST_ALLJGHZ           304315  //当日交割汇总查询
#define  FUN_SECU_LIST_YJFHMX            304316  //查询客户佣金返还明细
#define  FUN_SECU_LIST_ALLWHHL           304350  //全体无户红利查询
#define  FUN_SECU_LIST_ALLSGDJ           304351  //全体申购冻结查询
#define  FUN_SECU_COLLECT_GDZH           304401  //查询营业部分客户群组股东开户汇总
#define  FUN_SECU_TAXIS_SSCJ             304451  //查询营业部客户实时成交排行
#define  FUN_SBQQ_LIST_FRONTFOR_GDMC     304701  //查询主办股东名册
#define  FUN_SBQQ_LIST_ENTRUST           304702  //查询三板确权委托明细
#define  FUN_OFS_LIST_JJZHBYKHH          305001  //查询基金帐户信息
#define  FUN_OFS_LIST_JJFE               305002  //查询客户基金份额
#define  FUN_OFS_LIST_JJWT               305003  //查询客户基金委托
#define  FUN_OFS_GETKHHBYJJZH            305004  //按基金帐号查询客户号
#define  FUN_OFS_LIST_ALLJJZH            305201  //全体基金帐户查询
#define  FUN_OFS_LIST_ALLJJFE            305202  //查询全体客户基金份额
#define  FUN_OFS_LIST_ALLJJWT            305203  //查询全体基金委托
#define  FUN_OFS_LIST_ALLDJS             305205  //查询全体基金待交收资料
#define  FUN_DATA_BCPFROMSQL             400001  //根据SQL语句类似BCP出数据
#define  FUN_CUSTOM_GET_CZMXBYLSH        402001  //查询指定流水的操作明细
#define  FUN_CUSTOM_COLLECT_KXHTJ        402101  //查询营业部开户开销户统计
#define  FUN_CUSTOM_LIST_CZMXLS_KH       402201  //客户操作明细历史查询
#define  FUN_CUSTOM_LIST_ALLCZMXLS       402301  //全体操作明细历史查询
#define  FUN_ACCOUNT_GET_ZJMXBYLSH       403001  //查询指定流水号的资金明细
#define  FUN_ACCOUNT_GET_ZJDJMXBYLSH     403002  //查询指定流水号的资金冻结明细
#define  FUN_ACCOUNT_COLLECT_KXHTJ       403101  //查询营业部资金帐户开销户统计
#define  FUN_ACCOUNT_COLLECT_WBZHTJ      403102  //查询营业部外部帐户开销户统计
#define  FUN_ACCOUNT_COLLECT_ZJXXTJ      403103  //查询营业部资金信息统计
#define  FUN_ACCOUNT_LIST_ZJMXLS_KH      403201  //客户资金明细历史查询
#define  FUN_ACCOUNT_LIST_ZJDJMXLS_KH    403202  //客户资金冻结明细历史查询
#define  FUN_ACCOUNT_LIST_ZJYELS_KH      403203  //客户历史资金余额查询
#define  FUN_ACCOUNT_LIST_ZJJYSQLS_ZQ    403251  //客户资金交易申请(证券发起)历史查询
#define  FUN_ACCOUNT_LIST_ZJJYSQLS_WB    403252  //客户资金交易申请(外部发起)历史查询
#define  FUN_ACCOUNT_LIST_ALLZJMXLS      403301  //全体资金明细历史查询
#define  FUN_ACCOUNT_LIST_ALLZJDJMXLS    403302  //全体资金冻结明细历史查询
#define  FUN_ACCOUNT_LIST_ALLZJYELS      403303  //全体资金余额历史查询
#define  FUN_ACCOUNT_LIST_ALLZJJYSQLS_Z  403351  //全体资金交易申请(证券发起)历史查询
#define  FUN_ACCOUNT_LIST_ALLZJJYSQLS_W  403352  //全体资金交易申请(外部发起)历史查询
#define  FUN_SECU_GET_JGMXBYLSH          404001  //查询指定流水号的交割明细
#define  FUN_SECU_GET_WTLSBYWTH          404002  //根据委托号查询委托信息
#define  FUN_SECU_COLLECT_GDKXHTJ        404101  //查询营业部股东帐户开销户统计
#define  FUN_SECU_COLLECT_GDTGTJ         404102  //查询营业部股东托管统计
#define  FUN_SECU_COLLECT_QZTGTJ         404103  //查询营业部分券种托管统计
#define  FUN_SECU_COLLECT_ZQJYTJ         404104  //查询营业部证券交易统计
#define  FUN_SECU_COLLECT_ZQJYTJ_TJFL    404105  //查询营业部分类证券交易统计
#define  FUN_SECU_COLLECT_ZQJYTJ_WTFS    404106  //查询营业部委托方式证券交易统计
#define  FUN_SECU_COLLECT_ZQJYTJ_SCZYL   404107  //查询营业部证券交易市场占有率统计
#define  FUN_SECU_COLLECT_QSZJTJ         404108  //查询营业部清算资金统计
#define  FUN_SECU_TAXIS_HISTORY_HOLDSTK  404151  //查询客户历史证券持仓排行
#define  FUN_SECU_TAXIS_HISTORY_JYL      404152  //查询客户历史交易量排行
#define  FUN_SECU_LIST_JGMXLS_KH         404201  //客户交割明细历史查询
#define  FUN_SECU_LIST_WTLS_KH           404202  //客户委托历史查询
#define  FUN_SECU_LIST_ZQYELS_KH         404205  //客户证券历史持仓查询
#define  FUN_SECU_LIST_ALLJGMXLS         404301  //全体交割明细历史查询
#define  FUN_SECU_LIST_ALLWTLS           404302  //全体委托历史查询
#define  FUN_SECU_LIST_ALLZQYELS         404305  //全体证券历史持仓查询
#define  FUN_SECU_LIST_HZALLJGLS         404401  //全体交割历史汇总查询
#define  FUN_OFS_LIST_JJWTLS             405001  //查询客户历史基金委托
#define  FUN_OFS_LIST_JJJGLS             405003  //查询客户历史基金交割
#define  FUN_OFS_LIST_JJFELS_KH          405005  //查询客户历史基金份额查询
#define  FUN_OFS_GET_JJWTBYWTH           405006  //根据委托号查询基金委托信息
#define  FUN_OFS_LIST_ALLJJWTLS          405101  //查询全体历史基金委托
#define  FUN_OFS_LIST_ALLJGLS            405103  //查询全体基金交割历史数据
#define  FUN_OFS_LIST_HZALLJGLS          405104  //查询全体历史基金交割汇总
#define  FUN_OFS_LIST_ALLJJFELS          405105  //全体基金历史份额查询
#define  FUN_BLS_TEST                    989898  //TEST
#define  FUN_QUERY                       999998  //分页查询
#define  FUN_CHECK                       999999  //审核输入

                                                                                                                  
#define		FID_15_BCZJYE		501		//本次资金余额                                                                    #endif // !defined(_FIDDEF_DEFINE_H)
#define		FID_15_BFXJJ		502		//买风险基金                                                                      
#define		FID_15_BGHF		   503		//买过户费                                                                      
#define		FID_15_BJE			504		//买入金额                                                                        
#define		FID_15_BJSF		   505		//买经手费                                                                      
#define		FID_15_BPGDH		506		//报盘股东号                                                                      
#define		FID_15_CODE		   507		//返回码                                                                        
#define		FID_15_MESSAGE		508		//返回说明                                                                      
#define		FID_15_BSL			509		//买入数量                                                                        
#define		FID_15_BYHS		   510		//买印花税                                                                      
#define		FID_15_BZ			511		//币种                                                                              
#define		FID_15_BZBM		512		//币种编码                                                                          
#define		FID_15_BZGF		513		//买证管费                                                                          
#define		FID_15_BZMC		514		//币种名称                                                                          
#define		FID_15_CBJS		515		//成本积数                                                                          
#define		FID_15_CBM			516		//字符编码                                                                        
#define		FID_15_CDID		517		//菜单ID号                                                                          
#define		FID_15_CDMC		518		//菜单名称                                                                          
#define		FID_15_CDTIME		519		//菜单可用时间                                                                    
#define		FID_15_CDTS		520		//菜单提示                                                                          
#define		FID_15_CDWZID		521		//菜单位置ID                                                                      
#define		FID_15_CJBH		522		//成交编号                                                                          
#define		FID_15_CJGY		523		//创建柜员                                                                          
#define		FID_15_CJJE		524		//成交金额                                                                          
#define		FID_15_CJJG		525		//成交价格                                                                          
#define		FID_15_CJRQ		526		//成交日期                                                                          
#define		FID_15_CJSJ		527		//成交时间                                                                          
#define		FID_15_CJSL		528		//成交数量                                                                          
#define		FID_15_CKLL		529		//存款利率                                                                          
#define		FID_15_CS			530		//城市                                                                              
#define		FID_15_CSZ			531		//参数值                                                                          
#define		FID_15_DH			532		//电话                                                                              
#define		FID_15_DJLB		533		//冻结类别                                                                          
#define		FID_15_DLRDH		534		//代理人电话                                                                      
#define		FID_15_DLRDZ		535		//代理人地址                                                                      
#define		FID_15_DLRMM		536		//代理人密码                                                                      
#define		FID_15_DLRQX		537		//代理人权限                                                                      
#define		FID_15_DLRSFZH		538		//代理人身份证号                                                                
#define		FID_15_DLRXM		539		//代理人姓名                                                                      
#define		FID_15_DRMCCJJE	540		//当日卖出成交金额                                                                
#define		FID_15_DRMCCJSL	541		//当日卖出成交数量                                                                
#define		FID_15_DRMCJDZJ	542		//当日卖出解冻资金                                                                
#define		FID_15_DRMCWTSL	543		//当日卖出委托数量                                                                
#define		FID_15_DRMRCJJE	544		//当日买入成交金额                                                                
#define		FID_15_DRMRCJSL	545		//当日买入成交数量                                                                
#define		FID_15_DRMRDJZJ	546		//当日买入冻结资金                                                                
#define		FID_15_DRMRWTSL	547		//当日买入委托数量                                                                
#define		FID_15_DRQS		548		//当日已成交的清算资金                                                              
#define		FID_15_DTB			549		//跌停板                                                                          
#define		FID_15_DZ			550		//地址                                                                              
#define		FID_15_DZLX		551		//待转利息                                                                          
#define		FID_15_EMAIL		552		//电子邮件                                                                        
#define		FID_15_EN_BZ		553		//可操作的币种                                                                    
#define		FID_15_EN_JYS		554		//可操作的交易所                                                                  
#define		FID_15_EN_KHH		555		//可操作的客户号                                                                  
#define		FID_15_EN_KHQZ		556		//允许操作的客户群组                                                            
#define		FID_15_EN_YYB		557		//允许操作的营业部                                                                
#define		FID_15_EN_ZQLB		558		//可操作的证券类别                                                              
#define		FID_15_EX_KHH		559		//不可操作的客户号                                                                
#define		FID_15_EX_KHQZ		560		//禁止操作的客户群组                                                            
#define		FID_15_EX_YYB		561		//禁止操作的营业部                                                                
#define		FID_15_EX_ZQLB		562		//不可操作的证券类别                                                            
#define		FID_15_FATHERID	563		//父菜单                                                                          
                                                                                                                  
#define		FID_15_FKEY		565		//加速键                                                                            
#define		FID_15_FLDM		566		//数据字典分类代码                                                                  
#define		FID_15_FLMC		567		//数据字典分类名称                                                                  
#define		FID_15_FLTSL		568		//非流通数量                                                                      
#define		FID_15_FSYYB		569		//发生营业部                                                                      
#define		FID_15_FWXM		570		//服务项目                                                                          
#define		FID_15_GDH			571		//股东号                                                                          
#define		FID_15_GDLB		572		//股东类别                                                                          
#define		FID_15_GDSFZH		573		//股东身份证号                                                                    
#define		FID_15_GDSX		574		//股东属性                                                                          
#define		FID_15_GDXM		575		//股东姓名                                                                          
#define		FID_15_GJDM		576		//国籍代码                                                                          
#define		FID_15_GMRQ		577		//柜员更密日期                                                                      
                                                                                                                  
#define		FID_15_HKEY		579		//热键                                                                              
#define		FID_15_HKEYMASK	580		//组合键                                                                          
#define		FID_15_HTH			581		//合同号                                                                          
#define		FID_15_IBM			582		//数字编码                                                                        
#define		FID_15_JJR			583		//经纪人代码                                                                      
#define		FID_15_JJRLB		584		//经纪人类别                                                                      
#define		FID_15_JJRMM		585		//经纪人密码                                                                      
#define		FID_15_JJRQX		586		//经纪人权限                                                                      
#define		FID_15_JKP			587		//今开盘                                                                          
#define		FID_15_JSDM		588		//角色代码                                                                          
#define		FID_15_JSMC		589		//角色名称                                                                          
#define		FID_15_JSRQ		590		//结束日期                                                                          
#define		FID_15_JYDW		591		//交易单位                                                                          
#define		FID_15_JYFL		592		//交易分类                                                                          
#define		FID_15_JYFW		593		//交易范围                                                                          
#define		FID_15_JYJW		594		//交易价位                                                                          
#define		FID_15_JYLB		595		//交易类别编码                                                                      
#define		FID_15_JYLBMC		596		//交易类别名称                                                                    
#define		FID_15_JYLBXZ		597		//交易类别限制                                                                    
#define		FID_15_JYMM		598		//交易密码                                                                          
#define		FID_15_JYS			599		//交易所编码                                                                      
#define		FID_15_JYSDM		600		//交易所代码                                                                      
#define		FID_15_JYSJC		601		//交易所名称                                                                      
#define		FID_15_JYSQC		602		//交易所全称                                                                      
#define		FID_15_KCRQ		603		//开仓日期                                                                          
#define		FID_15_KEYMASK		604		//菜单组合键                                                                    
#define		FID_15_KHH			605		//客户号                                                                          
#define		FID_15_KHJB		606		//客户级别                                                                          
#define		FID_15_KHJL		607		//客户经理                                                                          
#define		FID_15_KHLB		608		//客户类别                                                                          
#define		FID_15_KHQC		609		//客户全称                                                                          
#define		FID_15_KHQZ		610		//客户群组                                                                          
#define		FID_15_KHRQ		611		//开户日期                                                                          
#define		FID_15_KHSX		612		//客户属性                                                                          
#define		FID_15_KHXM		613		//客户姓名                                                                          
#define		FID_15_KHZT		614		//客户状态                                                                          
#define		FID_15_KMCSL		615		//可卖出数量                                                                      
#define		FID_15_KMRSL		616		//可买入数量                                                                      
#define		FID_15_KQZJ		617		//可取资金                                                                          
#define		FID_15_KSRQ		618		//开始日期                                                                          
#define		FID_15_KYZJ		619		//可用资金                                                                          
#define		FID_15_LL			620		//利率                                                                              
#define		FID_15_LX			621		//预计利息                                                                          
#define		FID_15_LXJS		622		//利息积数                                                                          
#define		FID_15_LXS			623		//预计利息税                                                                      
#define		FID_15_LXSL		624		//利息税率                                                                          
#define		FID_15_MCJE		625		//卖出金额                                                                          
#define		FID_15_MCJJ		626		//卖出均价                                                                          
#define		FID_15_MCJS		627		//卖出基数                                                                          
#define		FID_15_MCSL		628		//卖出数量                                                                          
#define		FID_15_MENUID		629		//菜单的系统代码                                                                  
#define		FID_15_MKBH		630		//模块编号                                                                          
#define		FID_15_MM			631		//密码                                                                              
#define		FID_15_MMXZ		632		//买卖限制                                                                          
#define		FID_15_MMYK		633		//买卖盈亏                                                                          
#define		FID_15_MRJE		634		//买入金额                                                                          
#define		FID_15_EN_ZQDM		635		//证券代码                                                                      
#define		FID_15_MRJJ		636		//买入均价                                                                          
#define		FID_15_MRJS		637		//买入基数                                                                          
#define		FID_15_MRSL		638		//买入数量                                                                          
#define		FID_15_NBMC		639		//摘要内部名称                                                                      
#define		FID_15_NEWMM		640		//新密码                                                                          
#define		FID_15_NODE		641		//操作站点                                                                          
#define		FID_15_NOTE		642		//说明                                                                              
#define		FID_15_PYDM		643		//拼音代码                                                                          
#define		FID_15_PZBZ		644		//是否打印凭证                                                                      
#define		FID_15_PZH			645		//凭证号                                                                          
#define		FID_15_QSJE		646		//清算金额                                                                          
#define		FID_15_QSZJ		647		//清算资金                                                                          
#define		FID_15_QYBM		648		//区域编码                                                                          
#define		FID_15_QZFW		649		//取值范围                                                                          
#define		FID_15_QZMC		650		//群组名称                                                                          
#define		FID_15_RQ			651		//日期                                                                              
#define		FID_15_RZJE		652		//融资金额                                                                          
#define		FID_15_RZLL		653		//融资利率                                                                          
#define		FID_15_SFXJJ		654		//卖风险基金                                                                      
#define		FID_15_SGHF		655		//卖过户费                                                                          
#define		FID_15_SJCD		656		//数据长度                                                                          
#define		FID_15_SJE			657		//卖出金额                                                                        
#define		FID_15_SJJJR		658		//上级经纪人                                                                      
#define		FID_15_SJSF		659		//卖经手费                                                                          
#define		FID_15_SJYYB		660		//上级营业部                                                                      
#define		FID_15_SLXZ		661		//数量限制                                                                          
#define		FID_15_SM			662		//说明                                                                              
#define		FID_15_SRZJYE		663		//上日资金余额                                                                    
#define		FID_15_SSL			664		//卖出数量                                                                        
#define		FID_15_SXLB		665		//属性类别                                                                          
#define		FID_15_SYHS		666		//卖印花税                                                                          
#define		FID_15_SZGF		667		//卖证管费                                                                          
#define		FID_15_T0JS		668		//当日应交收资金                                                                    
#define		FID_15_T1JS		669		//T+1清算应交收资金                                                                 
#define		FID_15_T2JS		670		//T+2清算应交收资金                                                                 
#define		FID_15_TBCBJ		671		//摊薄成本价                                                                      
#define		FID_15_TBTS		672		//特别提示                                                                          
#define		FID_15_TJFL		673		//统计分类                                                                          
#define		FID_15_TZJS		674		//透支积数                                                                          
#define		FID_15_TZLL		675		//透支利率                                                                          
#define		FID_15_USERGROUP	676		//用户组别                                                                      
#define		FID_15_USERID		677		//柜员号                                                                          
#define		FID_15_USERNAME	678		//柜员名称                                                                        
#define		FID_15_VALUE		679		//属性值                                                                          
#define		FID_15_WTFS		680		//委托方式                                                                          
#define		FID_15_WTH			681		//委托号                                                                          
#define		FID_15_WTJG		682		//委托价格                                                                          
#define		FID_15_WTLB		683		//委托类别                                                                          
#define		FID_15_WTSL		684		//委托数量                                                                          
#define		FID_15_WTSSL		685		//未交收证券数量                                                                  
#define		FID_15_WTSX		686		//委托上限                                                                          
#define		FID_15_WTXX		687		//委托下限                                                                          
#define		FID_15_XHRQ		688		//销户日期                                                                          
#define		FID_15_XLDM		689		//学历代码                                                                          
#define		FID_15_XM			690		//姓名                                                                              
#define		FID_15_XWDM		691		//席位代码                                                                          
#define		FID_15_YHDM		692		//银行代码                                                                          
#define		FID_15_YHZH		693		//银行帐号                                                                          
#define		FID_15_YWKM		694		//业务科目                                                                          
#define		FID_15_YWLB		695		//业务类别                                                                          
#define		FID_15_YWMC		696		//业务名称                                                                          
#define		FID_15_YYB			697		//营业部                                                                          
#define		FID_15_YYBJC		698		//营业部简称                                                                      
#define		FID_15_YYBLB		699		//营业部分类                                                                      
#define		FID_15_YYBMC		700		//营业部名称                                                                      
#define		FID_15_YYDH		701		//预约单号                                                                          
#define		FID_15_YZBM		702		//邮政编码                                                                          
#define		FID_15_ZDBJ		703		//最低报价                                                                          
#define		FID_15_ZDJ			704		//最低价                                                                          
#define		FID_15_ZDXW		705		//指定席位                                                                          
#define		FID_15_ZGBJ		706		//最高报价                                                                          
#define		FID_15_ZGJ			707		//最高价                                                                          
#define		FID_15_ZHFL		708		//资金帐户分类                                                                      
#define		FID_15_ZHYE		709		//帐户余额                                                                          
#define		FID_15_ZHZT		710		//帐户状态                                                                          
#define		FID_15_ZJBH		711		//证件编号                                                                          
#define		FID_15_ZJCQDJ		712		//资金长期冻结                                                                    
#define		FID_15_ZJLB		713		//证件类别                                                                          
#define		FID_15_ZJMM		714		//资金密码                                                                          
#define		FID_15_ZJYE		715		//资金余额                                                                          
#define		FID_15_ZJZH		716		//资金帐号                                                                          
#define		FID_15_ZQDJSL		717		//证券冻结数量                                                                    
#define		FID_15_ZQDJYE		718		//证券冻结余额                                                                    
#define		FID_15_ZQDM		719		//证券代码                                                                          
#define		FID_15_ZQLB		720		//证券类别                                                                          
#define		FID_15_ZQLBMC		721		//证券类别名称                                                                    
#define		FID_15_ZQMC		722		//证券名称                                                                          
#define		FID_15_ZQQC		723		//证券全称                                                                          
#define		FID_15_ZQSL		724		//证券数量                                                                          
#define		FID_15_ZSBH		725		//证书编号                                                                          
#define		FID_15_ZSP			726		//昨收盘                                                                          
#define		FID_15_ZT			727		//状态                                                                              
#define		FID_15_ZTB			728		//涨停板                                                                          
#define		FID_15_ZXJ			729		//最新价                                                                          
#define		FID_15_ZY			730		//摘要                                                                              
#define		FID_15_ZYDM		731		//职业代码                                                                          
#define		FID_15_ZZFS		732		//转帐申请方式                                                                      
#define		FID_15_ZZJE		733		//支汇票金额                                                                        
#define		FID_15_ZZKZ		734		//转帐控制                                                                          
#define		FID_15_MMLB		735		//密码类别                                                                          
#define		FID_15_ZZFX		736		//转帐方向                                                                          
#define		FID_15_NEWQZ		737		//新群组编号                                                                      
#define		FID_15_EN_ZJZH		738		//可操作的资金帐号                                                              
#define		FID_15_LOGICAL		739		//逻辑判断操作（是、否）                                                        
#define		FID_15_EN_TZLX		740		//是否调整利息                                                                  
#define		FID_15_SQFS		741		//申请方式                                                                          
#define		FID_15_CKCS		742		//存款次数限制                                                                      
#define		FID_15_CKZE		743		//存款总额限制                                                                      
#define		FID_15_CKDBSX		744		//存款单笔限制                                                                    
#define		FID_15_QKCS		745		//取款次数限制                                                                      
#define		FID_15_QKZE		746		//取款总额限制                                                                      
#define		FID_15_QKDBSX		747		//取款单笔限制                                                                    
#define		FID_15_TBBBJ		748		//摊薄保本价                                                                      
#define		FID_15_TBFDYK		749		//摊薄浮动盈亏                                                                    
#define		FID_15_WTSJ		750		//委托时间                                                                          
#define		FID_15_SBSJ		751		//申报时间                                                                          
#define		FID_15_MMLBSM		752		//买卖类别说明                                                                    
#define		FID_15_SBJG		753		//申报结果                                                                          
#define		FID_15_SBJGSM		754		//申报结果说明                                                                    
#define		FID_15_CXBZ		755		//撤销标志                                                                          
                                                                                                                  
#define		FID_15_JCCL		757		//今持仓量                                                                          
#define		FID_15_WJSSL		758		//未交收数量                                                                      
#define		FID_15_ZXSZ		760		//最新市值                                                                          
#define		FID_15_FDYK		761		//浮动盈亏                                                                          
#define		FID_15_ROWCOUNT	762		//最大结果集                                                                      
#define		FID_15_BROWINDEX	763		//起始历史记录索引值                                                            
#define		FID_15_DJZJ		764		//冻结资金                                                                          
                                                                                                                  
#define		FID_15_S1			766		//佣金                                                                              
#define		FID_15_S2			767		//印花税                                                                            
#define		FID_15_S3			768		//过户费                                                                            
#define		FID_15_S4			769		//附加费                                                                            
#define		FID_15_S5			770		//结算费                                                                            
#define		FID_15_S6			771		//交易规费                                                                          
#define		FID_15_YSJE		772		//应收金额                                                                          
#define		FID_15_HGLXSR		773		//回购利息收入                                                                    
#define		FID_15_HGLXFC		774		//回购利息付出                                                                    
#define		FID_15_BZS1		775		//标准佣金                                                                          
#define		FID_15_BCZQSL		776		//本次股份余额                                                                    
#define		FID_15_BRZQSL		777		//本日股份余额                                                                    
#define		FID_15_BRZJYE		778		//本日资金余额                                                                    
#define		FID_15_GYMM		779		//柜员密码                                                                          
#define		FID_15_KHK			780		//客户卡号                                                                        
#define		FID_15_JMLX		781		//加密类型                                                                          
#define		FID_15_SQLB		782		//申请类别                                                                          
#define		FID_15_ZPJE		784		//支票金额                                                                          
#define		FID_15_GPSZ		785		//股票市值                                                                          
#define		FID_15_WTQRBZ		786		//委托确认标志                                                                    
#define		FID_15_COUNT		788		//笔数                                                                            
#define		FID_15_GDHH		789		//股东号前缀                                                                        
#define		FID_15_GDHL		790		//股东号长度                                                                        
#define		FID_15_ZQDML		791		//证券代码长度                                                                    
#define		FID_15_ENDWTH		792		//结束委托号                                                                      
#define		FID_15_LOGINID		793		//登录ID                                                                        
#define		FID_15_ZHXM		794		//帐户姓名                                                                          
#define		FID_15_JSZH		795		//结算帐户                                                                          
#define		FID_15_FAX			796		//传真FAX                                                                         
#define		FID_15_LSH			797		//流水号                                                                          
#define		FID_15_SFZH		798		//身份证号                                                                          
#define		FID_15_TZLX		799		//透支利息                                                                          
                                                                                                                  
#define		FID_15_SRFS		801		//输入方式                                                                          
#define		FID_15_XJZC		802		//现金资产                                                                          
#define		FID_15_SRYE		803		//上日余额                                                                          
#define		FID_15_ZHSX		804		//帐户属性                                                                          
#define		FID_15_WJSJE		805		//未交收金额                                                                      
#define		FID_15_CQBZ		806		//存取标志                                                                          
#define		FID_15_YZZZBZ		807		//银证转帐标志                                                                    
#define		FID_15_FSJE		808		//发生金额                                                                          
#define		FID_15_ZPBZ		809		//支票标志                                                                          
#define		FID_15_SCZJYE		810		//上次资金余额                                                                    
#define		FID_15_YWPCH		811		//业务批次号                                                                      
#define		FID_15_DJRQ		812		//登记日期                                                                          
                                                                                                                  
#define		FID_15_ITEM		814		//配置项                                                                            
#define		FID_15_XGRQ		815		//修改日期                                                                          
#define		FID_15_CSJB		816		//参数级别                                                                          
#define		FID_15_SXMC		817		//属性名称                                                                          
#define		FID_15_PZH2		818		//凭证号2                                                                           
#define		FID_15_LPID		819		//礼品ID号                                                                          
#define		FID_15_LPBM		820		//礼品编码                                                                          
#define		FID_15_LPMC		821		//礼品名称                                                                          
#define		FID_15_LPJZ		822		//礼品价值                                                                          
#define		FID_15_ZSFS		823		//赠送方式                                                                          
#define		FID_15_ZCKZED		824		//资产控制额度                                                                    
#define		FID_15_ZJKZED		825		//资金控制额度                                                                    
#define		FID_15_DJJE		826		//冻结金额                                                                          
#define		FID_15_YCDJJE		827		//异常冻结金额                                                                    
#define		FID_15_QSDM		828		//券商代码                                                                          
#define		FID_15_QSMC		829		//券商名称                                                                          
#define		FID_15_JGSM		830		//结果说明                                                                          
#define		FID_15_CZZD		831		//操作站点                                                                          
#define		FID_15_JGBM		832		//机构编码                                                                          
#define		FID_15_JGMC		833		//机构名称                                                                          
#define		FID_15_JGJC		834		//机构简称                                                                          
#define		FID_15_CITY		835		//城市                                                                              
#define		FID_15_PROVINCE	836		//省份                                                                            
#define		FID_15_SJJG		837		//上级机构                                                                          
#define		FID_15_SJJGLB		838		//上级机构类别                                                                    
#define		FID_15_JGLB		839		//机构类别                                                                          
#define		FID_15_SLTSFS		840		//数量提示方式                                                                    
#define		FID_15_EN_JYLB		841		//允许的交易类别                                                                
#define		FID_15_MRJG1		842		//买入价格一                                                                      
#define		FID_15_MRSL1		843		//买入数量一                                                                      
#define		FID_15_MRJG2		844		//买入价格二                                                                      
#define		FID_15_MRSL2		845		//买入数量二                                                                      
#define		FID_15_MRJG3		846		//买入价格三                                                                      
#define		FID_15_MRSL3		847		//买入数量三                                                                      
#define		FID_15_MRJG4		848		//买入价格四                                                                      
#define		FID_15_MRSL4		849		//买入数量四                                                                      
#define		FID_15_MCJG1		850		//卖出价格一                                                                      
#define		FID_15_MCSL1		851		//卖出数量一                                                                      
#define		FID_15_MCJG2		852		//卖出价格二                                                                      
#define		FID_15_MCSL2		853		//卖出数量二                                                                      
#define		FID_15_MCJG3		854		//卖出价格三                                                                      
#define		FID_15_MCSL3		855		//卖出数量三                                                                      
#define		FID_15_MCJG4		856		//卖出价格四                                                                      
#define		FID_15_MCSL4		857		//卖出数量四                                                                      
#define		FID_15_DYBL		858		//抵押比例                                                                          
#define		FID_15_JJRXM		859		//经纪人姓名                                                                      
#define		FID_15_TPBZ		860		//停牌标志                                                                          
#define		FID_15_JSLX		861		//结算类型                                                                          
#define		FID_15_HBXH		862		//回报序号                                                                          
#define		FID_15_GDZT		863		//股东状态                                                                          
#define		FID_15_WTGY		864		//委托柜员                                                                          
#define		FID_15_FSSJ		865		//发生时间                                                                          
#define		FID_15_WTZKXS		866		//委托折扣系数                                                                    
#define		FID_15_CXZKXS		867		//查询折扣系数                                                                    
#define		FID_15_QTZKXS		868		//前台折扣系数                                                                    
#define		FID_15_ZQSZ		869		//证券市值                                                                          
#define		FID_15_WTSF		870		//委托收费                                                                          
#define		FID_15_CDSF		871		//撤单收费                                                                          
#define		FID_15_ZCZH		872		//转存帐号                                                                          
#define		FID_15_ZCBL		873		//转存比例                                                                          
#define		FID_15_SFFS		874		//收费方式                                                                          
#define		FID_15_SFBZ		875		//收费标准                                                                          
#define		FID_15_JFFS		876		//计费方式                                                                          
#define		FID_15_JFQD		877		//计费起点                                                                          
#define		FID_15_JFDW		878		//计费单位                                                                          
#define		FID_15_CZSJ		879		//操作时间                                                                          
#define		FID_15_CLJG		880		//处理结果                                                                          
#define		FID_15_GYFL		881		//柜员分类                                                                          
#define		FID_15_GYZT		882		//柜员状态                                                                          
#define		FID_15_XQXZ		883		//星期限制                                                                          
#define		FID_15_ZCXZ		884		//注册限制                                                                          
#define		FID_15_SLSX		885		//数量上限                                                                          
#define		FID_15_CDSL		886		//撤单数量                                                                          
#define		FID_15_YJZKXS		887		//佣金折扣系数                                                                    
#define		FID_15_FJFZKXS		888		//附加费折扣系数                                                                
#define		FID_15_BH			889		//编号                                                                              
#define		FID_15_YJTCBL		890		//佣金提成比例                                                                    
#define		FID_15_YJFHSL		891		//佣金返还税率                                                                    
#define		FID_15_GYH			892		//柜员号                                                                          
#define		FID_15_LPDM		893		//礼品代码                                                                          
#define		FID_15_ZC			894		//资产                                                                              
#define		FID_15_SRJE		895		//收入金额                                                                          
#define		FID_15_FCJE		896		//付出金额                                                                          
#define		FID_15_CZBZ		897		//冲帐标志                                                                          
#define		FID_15_KSSJ		898		//开始时间                                                                          
#define		FID_15_JSSJ		899		//结束时间                                                                          
#define		FID_15_GMZQ		900		//更密周期                                                                          
#define		FID_15_SXZ			901		//属性值                                                                          
#define		FID_15_YHSX		902		//银行属性                                                                          
#define		FID_15_YHYW		903		//银行业务                                                                          
#define		FID_15_ZQYW		904		//证券业务                                                                          
#define		FID_15_ZJMMXY		905		//资金密码效验                                                                    
#define		FID_15_JYMMXY		906		//交易买卖效验                                                                    
#define		FID_15_YHMMXY		907		//银行买卖效验                                                                    
#define		FID_15_YHMC		908		//银行名称                                                                          
#define		FID_15_HBLB		909		//货币类别                                                                          
#define		FID_15_YHMM		910		//银行密码                                                                          
#define		FID_15_ZZBS		911		//转帐标识                                                                          
#define		FID_15_SQH			912		//申请号                                                                          
#define		FID_15_YHLSH		913		//银行流水号                                                                      
#define		FID_15_ZQLSH		914		//证券流水号                                                                      
#define		FID_15_BCYE		915		//本次余额                                                                          
#define		FID_15_SHLB		916		//审核类别                                                                          
#define		FID_15_SHJG		917		//审核结果                                                                          
#define		FID_15_YEGXSJ		918		//余额更新时间                                                                    
#define		FID_15_QSMM		919		//券商密码                                                                          
#define		FID_15_MMMY		920		//密码密钥                                                                          
#define		FID_15_CSMY		921		//传输密钥                                                                          
#define		FID_15_FJXX		922		//附加信息                                                                          
#define		FID_15_YHCLJG		923		//银行处理结果                                                                    
#define		FID_15_SQWD		924		//申请网点                                                                          
#define		FID_15_XYBZ		925		//效验标志                                                                          
#define		FID_15_YSQH		926		//原申请号                                                                          
#define		FID_15_JYBS		927		//交易标识                                                                          
#define		FID_15_CLLB		928		//处理类别                                                                          
#define		FID_15_ZHLB		929		//帐户类别                                                                          
#define		FID_15_FQF			930		//发起方                                                                          
#define		FID_15_HZBS		931		//汇总笔数                                                                          
#define		FID_15_HZJE		932		//汇总金额                                                                          
#define		FID_15_LY			933		//来源                                                                              
#define		FID_15_WDH			934		//网点号                                                                          
#define		FID_15_QD1			935		//起点一                                                                          
#define		FID_15_QD2			936		//起点二                                                                          
#define		FID_15_QD3			937		//起点三                                                                          
#define		FID_15_QD4			938		//起点四                                                                          
#define		FID_15_QD5			939		//起点五                                                                          
#define		FID_15_BL1			940		//佣金一                                                                          
#define		FID_15_BL2			941		//佣金二                                                                          
#define		FID_15_BL3			942		//佣金三                                                                          
#define		FID_15_BL4			943		//佣金四                                                                          
#define		FID_15_BL5			944		//佣金五                                                                          
#define		FID_15_KHJCGX		945		//客户继承关系                                                                    
#define		FID_15_SYZH1		946		//收益帐号一                                                                      
#define		FID_15_SYZH2		947		//收益帐号二                                                                      
#define		FID_15_JSTS		948		//计算天数                                                                          
#define		FID_15_TJJYR		949		//统计交易日                                                                      
#define		FID_15_TJTS		950		//统计天数                                                                          
#define		FID_15_FPBL		951		//分配比例                                                                          
#define		FID_15_ZKFS		952		//折扣方式                                                                          
#define		FID_15_ZJED		953		//资金额度                                                                          
#define		FID_15_ZCED		954		//资产额度                                                                          
#define		FID_15_GLF			955		//管理费                                                                          
#define		FID_15_YJDX		956		//佣金底限                                                                          
#define		FID_15_FHBZ		957		//复核标志                                                                          
#define		FID_15_BDSL		958		//变动数量                                                                          
#define		FID_15_HZFS		959		//汇总方式                                                                          
#define		FID_15_LPSL		960		//礼品数量                                                                          
#define		FID_15_LXBJ		961		//利息报价                                                                          
#define		FID_15_KLX			962		//卡类型                                                                          
#define		FID_15_KMC			963		//卡名称                                                                          
#define		FID_15_BSC			964		//标识串                                                                          
#define		FID_15_BSCKSWZ		965		//标识串开始位置                                                                
#define		FID_15_FJC			966		//附加串                                                                          
#define		FID_15_FJCKSWZ		967		//附加串开始位置                                                                
#define		FID_15_KHKSWZ		968		//卡号开始位置                                                                    
#define		FID_15_KHCD		969		//卡号长度                                                                          
#define		FID_15_EN_YWKM		970		//允许业务科目                                                                  
#define		FID_15_ZRSZ		971		//昨日市值                                                                          
#define		FID_15_NEXTDATA	972		//继续取数标志                                                                    
#define		FID_15_SYL			973		//收益率                                                                          
#define		FID_15_FHS1		974		//已返还佣金                                                                        
#define		FID_15_GJBM1		975		//国籍编码一                                                                      
#define		FID_15_GJBM2		976		//国籍编码二                                                                      
#define		FID_15_GJBM3		977		//国籍编码三                                                                      
#define		FID_15_GJMC		978		//国籍名称                                                                          
#define		FID_15_EGJMC		979		//国籍英文名称                                                                    
#define		FID_15_CCCB		980		//持仓成本                                                                          
#define		FID_15_BDRQ		981		//变动日期                                                                          
#define		FID_15_MMXYLX		982		//密码效验类型                                                                    
#define		FID_15_XZSJ		983		//限制时间                                                                          
#define		FID_15_KHDXLX		984		//客户对象类型                                                                    
#define		FID_15_JYSXGX		985		//交易所相关性                                                                    
#define		FID_15_BZXGX		986		//币种相关性                                                                      
#define		FID_15_QZXGX		987		//群组相关性                                                                      
#define		FID_15_EXFLG		988		//查询扩展信息标志                                                                
#define		FID_15_MAX_D		989		//最大值（浮点）                                                                  
#define		FID_15_MIN_D		990		//最小值（浮点）                                                                  
#define		FID_15_MAX_L		991		//最大值（整型）                                                                  
#define		FID_15_MIN_L		992		//最小值（整型）                                                                  
#define		FID_15_GYFJQX		993		//柜员附加权限                                                                    
#define		FID_15_ZHTZE		994		//帐户投资额                                                                      
#define		FID_15_QSJE_B		995		//回报买清算资金                                                                  
#define		FID_15_QSJE_S		996		//回报卖清算资金                                                                  
#define		FID_15_FDYK_TB		997		//摊薄浮动盈亏                                                                  
#define		FID_15_LLCSLB		998		//利率参数类别                                                                    
#define		FID_15_EX_JJR		999		//禁止经纪人                                                                      
#define		FID_15_FHFS		1000	//佣金返还方式                                                                      
                                                                                                                  
//## 报表类                                                                                                       
#define		FID_15_RPT_NAME		1001                                                                                  
#define		FID_15_RPT_REQTYPE		1002	//请求类型	0:请求变量                                                      
#define		FID_15_RPT_VAR			1003	//报表交互变量                                                                
#define		FID_15_RPT_XML			1004	//XML数据(所有数据)                                                           
#define		FID_15_RPT_XML_NAME	1005	//XML名字                                                                     
#define		FID_15_RPT_XMLFORMT	1006	//XML格式文件                                                                 
#define		FID_15_RPT_MEMNAME		1009	//报表数据名称                                                              
#define		FID_15_RPT_PRO			1010	//是否主数据集.	  1:主数据集（显示在ApexListCtrl上）。                        
                                                                                                                  
#define		FID_15_BZ_FH		1001	//佣金返还币种                                                                    
#define		FID_15_HBDHBL		1002	//货币兑换比例                                                                    
#define		FID_15_SYZH3		1003	//收益帐户3                                                                       
#define		FID_15_SYBL1		1004	//收益比例1                                                                       
#define		FID_15_SYBL2		1005	//收益比例2                                                                       
#define		FID_15_SYBL3		1006	//收益比例3                                                                       
#define		FID_15_FY			1007	//交易费用                                                                          
#define		FID_15_JYZT		1008	//交易状态                                                                          
#define		FID_15_RQ2			1009	//日期2                                                                           
#define		FID_15_PROVINCEID	1010	//省份ID                                                                        
#define		FID_15_CITYID		1012	//城市ID                                                                          
#define		FID_15_SECTIONID	1014	//辖区ID                                                                        
#define		FID_15_SECTION		1015	//辖区名称                                                                      
#define		FID_15_SFZQ		1016	//收费周期                                                                          
#define		FID_15_WBJGLB		1017	//外部机构类别                                                                    
//#define	  FID_15_YWLB		1018	//业务类别                                                                        
#define		FID_15_EN_YWLB		1019	//业务类别范围                                                                  
#define		FID_15_BZDM		1020	//标准代码                                                                          
#define		FID_15_WBDM		1021	//外部代码                                                                          
#define		FID_15_CSDM		1022	//参数代码                                                                          
#define		FID_15_CSMC		1023	//参数名称                                                                          
#define		FID_15_QZSM		1024	//取值说明                                                                          
#define		FID_15_EN_FJBZ		1025	//允许的附加标志                                                                
#define		FID_15_GZJJBZ		1026	//国债净价标志                                                                    
#define		FID_15_JSJG		1027	//结算价格                                                                          
#define		FID_15_CWLX		1028	//错误类型                                                                          
#define		FID_15_CWDM		1029	//错误类型                                                                          
#define		FID_15_CWSM		1030	//错误类型                                                                          
#define		FID_15_ZXZS		1031	//最新指数                                                                          
#define		FID_15_TZEBZ		1032	//投资额标志                                                                      
#define		FID_15_KBBZ		1033	//佣金捆绑标志                                                                      
#define		FID_15_KHTZFL		1034	//客户投资分类(tXTDM.KHTZFL)                                                      
#define		FID_15_ZJZR		1035	//转入资金                                                                          
#define		FID_15_ZJZC		1036	//转出资金                                                                          
#define		FID_15_SZZR		1037	//转入市值                                                                          
#define		FID_15_SZZC		1038	//转出市值                                                                          
                                                                                                                  
#define		FID_15_MFDLCS		1040	//免费登录次数                                                                    
#define		FID_15_DLXZCS		1041	//限制登录次数                                                                    
#define		FID_15_Z_SJSX		1042	//每日使用时间上限(总)                                                            
#define		FID_15_S_SJSX		1043	//每日使用时间上限(单)                                                            
#define		FID_15_MMCSCS		1044	//密码尝试次数                                                                    
#define		FID_15_JDSJ		1045	//自动解冻时间                                                                      
#define		FID_15_MFHQCS		1046	//免费查询行情次数                                                                
#define		FID_15_HQSFSX		1047	//行情收费上限                                                                    
#define		FID_15_HKSFSX		1048	//划卡收费上限                                                                    
#define		FID_15_SJSFSX		1049	//使用时间收费上限                                                                
#define		FID_15_ZSFSX		1050	//总收费上限                                                                      
#define		FID_15_DBYJXX		1051	//单笔佣金下限                                                                    
#define		FID_15_DBYJSX		1052	//单笔佣金上限                                                                    
#define		FID_15_YJKBCL		1053	//佣金捆绑方式 tXTDM.YJKBCL                                                       
#define		FID_15_YJDJFS		1054	//佣金定价方式 tXTDM.YJDJFS                                                       
#define		FID_15_FHGY		1055	//复核柜员                                                                          
#define		FID_15_TJZQLB		1056	//统计证券类别                                                                    
#define		FID_15_EXWTFS		1057	//屏蔽委托方式                                                                    
#define		FID_15_FDBH		1058	//分段编号                                                                          
                                                                                                                  
#define		FID_15_ZHDM		1060	//专户代码                                                                          
#define		FID_15_TZDW		1061	//投资单位                                                                          
#define		FID_15_TZXX		1062	//投资下限                                                                          
#define		FID_15_SYZKXS		1063	//收益折扣系统                                                                    
#define		FID_15_SYTZFS		1064	//收益投资方式                                                                    
#define		FID_15_TZJESX		1065	//投资金额上限                                                                    
#define		FID_15_ZJLCDS		1066	//资金留存底数                                                                    
#define		FID_15_TZBDJE		1067	//投资额变更数                                                                    
#define		FID_15_ZHKHH		1068	//专户客户号                                                                      
#define		FID_15_ZHZJZH		1069	//专户资金账户                                                                    
#define		FID_15_LCJZJE		1070	//理财集中金额                                                                    
#define		FID_15_LCFHJE		1071	//理财返还金额                                                                    
#define		FID_15_SYZTZBZ		1072	//理财收益再投资标志                                                            
#define		FID_15_SYZTZZQ		1073	//收益再投资周期                                                                
#define		FID_15_LCZSY		1074	//理财总收益                                                                      
#define		FID_15_SYZCJE		1075	//理财收益转存金额                                                                
#define		FID_15_SYZTZE		1076	//收益再投资金额                                                                  
#define		FID_15_XYBH		1077	//协议编号                                                                          
#define		FID_15_LCTZJE		1078	//理财投资金额                                                                    
#define		FID_15_SGDM		1079	//专户申购代码                                                                      
#define		FID_15_CLBZ		1080	//处理标志                                                                          
#define		FID_15_FPSY		1081	//分配收益                                                                          
#define		FID_15_GBZQL		1082	//公布中签率                                                                      
#define		FID_15_SGSL		1083	//申购数量                                                                          
#define		FID_15_SGJG		1084	//申购价格                                                                          
#define		FID_15_SSRQ		1085	//上市日期                                                                          
#define		FID_15_HKRQ		1086	//还款日期                                                                          
#define		FID_15_SSDM		1087	//上市代码                                                                          
#define		FID_15_PHDM		1088	//配号代码                                                                          
#define		FID_15_HKDM		1089	//还款代码                                                                          
#define		FID_15_SGRQ		1090	//申购日期                                                                          
#define		FID_15_JEBL		1091	//金额比率                                                                          
#define		FID_15_LCJZQTJE	1092	//理财集中金额                                                                    
#define		FID_15_HZGDH		1093	//合作股东号                                                                      
#define		FID_15_SYDX		1094	//适用对象                                                                          
#define		FID_15_JYLJSFS		1095	//交易量计算方式                                                                
#define		FID_15_JYLLJZQ		1096	//交易量累计周期                                                                
#define		FID_15_DJFS		1097	//定价方式                                                                          
#define		FID_15_BBJ			1098	//保本价                                                                          
#define		FID_15_HZLB		1099	//合作帐户类别                                                                      
#define		FID_15_FSJE_LCBJ	1100	//本金发生金额                                                                  
#define		FID_15_BCYE_LCBJ	1101	//本次本金余额                                                                  
#define		FID_15_FSJE_SY		1102	//收益发生金额                                                                  
#define		FID_15_BCYE_SY		1103	//待转收益余额                                                                  
#define		FID_15_FSJE_BZJ	1104	//保证金发生额                                                                    
#define		FID_15_BCYE_BZJ	1105	//保证金帐户余额                                                                  
#define		FID_15_HZLBMC		1106	//合作类别名称                                                                    
#define		FID_15_KSFW		1107	//起始范围                                                                          
#define		FID_15_JSFW		1108	//结束范围                                                                          
#define		FID_15_KHS			1110	//开户户数                                                                        
#define		FID_15_XHS			1111	//销户户数                                                                        
#define		FID_15_ZHS			1112	//总户数                                                                          
#define		FID_15_ZDGDHS		1113	//指定股东户数                                                                    
#define		FID_15_CXZDHS		1114	//撤销股东户数                                                                    
#define		FID_15_CCGDHS		1115	//持仓股东户数                                                                    
#define		FID_15_TJZJYE		1116	//统计资金余额                                                                    
#define		FID_15_TJZJCK		1117	//统计增加存款                                                                    
#define		FID_15_TJZJQK		1118	//统计增加取款                                                                    
#define		FID_15_TJZJGP		1119	//统计增加市值                                                                    
#define		FID_15_TJJSGP		1120	//统计减少市值                                                                    
#define		FID_15_TJGPSZ		1121	//统计股票市值                                                                    
#define		FID_15_TJZZC		1122	//统计总资产                                                                      
#define		FID_15_FDB1		1123	//统计分段开始1                                                                     
#define		FID_15_FDE1		1124	//统计分段结束1                                                                     
#define		FID_15_FDB2		1125	//统计分段开始2                                                                     
#define		FID_15_FDE2		1126	//统计分段结束2                                                                     
#define		FID_15_FDB3		1127	//统计分段开始3                                                                     
#define		FID_15_FDE3		1128	//统计分段结束3                                                                     
#define		FID_15_FDB4		1129	//统计分段开始4                                                                     
#define		FID_15_FDE4		1130	//统计分段结束4                                                                     
#define		FID_15_FDB5		1131	//统计分段开始5                                                                     
#define		FID_15_FDE5		1132	//统计分段结束5                                                                     
#define		FID_15_FDB6		1133	//统计分段开始6                                                                     
#define		FID_15_FDE6		1134	//统计分段结束6                                                                     
#define		FID_15_TJJGGS		1135   //统计结果个数                                                                   
#define		FID_15_TJJGSJ		1136   //统计结果数据                                                                   
#define		FID_15_QSBZ		  1137	 //实时清算标志                                                                   
#define		FID_15_TZJE		  1138	 //透支金额                                                                       
#define		FID_15_WJSZJ		   1139	  //未交收资金                                                                  
                                                                                                                  
#define		FID_15_YDZD		  1141	 //应答字段                                                                       
#define		FID_15_YDZDSM		1142   //应答字段说明                                                                   
#define		FID_15_YDSJ		  1143	 //应答数据                                                                       
#define		FID_15_JCJJE		   1144	  //净成交金额                                                                  
#define		FID_15_YXHS		  1145	 //有效户数                                                                       
#define		FID_15_CJSLBL		1146   //成交数量比例                                                                   
#define		FID_15_CJJEBL		1147   //成交金额比例                                                                   
#define		FID_15_S1BL		  1148	 //佣金收入比例                                                                   
#define		FID_15_ZZCBL1		1149   //营业部的资产比例                                                               
#define		FID_15_ZZCBL2		1150   //总公司的资产比例                                                               
#define		FID_15_ZSLBL1		1151   //营业部的数量比例                                                               
#define		FID_15_ZSLBL2		1152   //总公司的数量比例                                                               
#define		FID_15_ZHSBL1		1153   //营业部的户数比例                                                               
#define		FID_15_ZHSBL2		1154   //总公司的户数比例                                                               
#define		FID_15_JEXX		  1155	 //金额下限                                                                       
#define		FID_15_GSKH		  1156	 //银证转帐及银证通参数中的公司卡号信息                                           
#define		FID_15_SBZT		  1157	 //银证转帐参数的申报状态                                                         
#define		FID_15_QDBZ		  1158	 //银证转帐参数的签到标志                                                         
#define		FID_15_WBZH		  1159	 //外部帐号                                                                       
#define		FID_15_WBJGDM		1160   //外部机构代码                                                                   
#define		FID_15_WBKYYE		1161   //外部帐户参考可用余额                                                           
#define		FID_15_WBLJZJ		1162   //外部帐户参考可取资金余额                                                       
#define		FID_15_WBDJJE		1163   //外部帐户参考冻结金额                                                           
#define		FID_15_JYXZ		  1164	 //银证通交易限制                                                                 
#define		FID_15_ZJXYFS		1165   //银证通资金校验方式                                                             
#define		FID_15_HBBZ		  1166	 //银证通资料回报类型                                                             
#define		FID_15_ZJHZFS		1167   //银证通资金回转方式                                                             
#define		FID_15_BDDJJE		1168   //银证通客户买入委托时本地资金冻结数                                             
#define		FID_15_ZZJSSJ		1170   //银证转帐停止正常转帐交易的时间                                                 
#define		FID_15_YWSQH		1171	//业务申请号                                                                      
#define		FID_15_SHNODE		1172	//审核网点                                                                        
#define		FID_15_SHSM		1173	//审核说明                                                                          
                                                                                                                  
#define		FID_15_ZSZBL1		1180   //营业部的市值比例                                                               
#define		FID_15_ZSZBL2		1181   //总公司的市值比例                                                               
#define		FID_15_QCKHS		1182   //期初客户数                                                                     
#define		FID_15_QMKHS		1183   //期末客户数                                                                     
                                                                                                                  
#define		FID_15_XCMRJ		   1186	  //现钞买入价                                                                  
#define		FID_15_XCMCJ		   1187	  //现钞卖出价                                                                  
#define		FID_15_XHMRJ		   1188	  //现汇买入价                                                                  
#define		FID_15_XHMCJ		   1189	  //现汇卖出价                                                                  
#define		FID_15_GSBL		  1190	 //估算比例                                                                       
#define		FID_15_HLBZ		  1191	 //汇率标志                                                                       
#define		FID_15_DRCKJE		1192   //当日帐户存款金额                                                               
#define		FID_15_TZCKJE		1193   //当日通知存款金额                                                               
#define		FID_15_JGMC_E		1194   //机构英文名称                                                                   
#define		FID_15_FRDB		  1195	 //法人代表                                                                       
#define		FID_15_FRSFZH		1196   //法人身份证号                                                                   
#define		FID_15_HYLB		  1197	 //行业类别                                                                       
#define		FID_15_YWFW		  1198	 //业务范围                                                                       
#define		FID_15_ZCDZ		  1199	 //注册地址                                                                       
#define		FID_15_ZCZB		  1200	 //注册资本                                                                       
#define		FID_15_SQL		1201                                                                                      
#define		FID_15_MEMHEAD	 1202                                                                                   
#define		FID_15_MEMDATA	 1203                                                                                   
#define		FID_15_MEMCOUNT	  1204                                                                                  
#define		FID_15_MEMSIZE	 1205                                                                                   
                                                                                                                  
#define		FID_15_FILENAME	   1201	  //文件名称                                                                    
#define		FID_15_FILEVERSION	  1202	 //文件版本                                                                 
                                                                                                                  
#define		FID_15_ZCYE		  1201	 //资产余额                                                                       
#define		FID_15_FZYE		  1202	 //负债余额                                                                       
#define		FID_15_YSZC		  1203	 //原始资产                                                                       
                                                                                                                  
#define		FID_15_YWXZ		  1205	 //业务限制                                                                       
#define		FID_15_SJXM		  1206	 //审计项目                                                                       
#define		FID_15_SJMC		  1207	 //审计名称                                                                       
#define		FID_15_JS		   1208	  //债券增值积数                                                                    
#define		FID_15_CCJJ		  1209	 //持仓均价                                                                       
#define		FID_15_LJYK		  1210	 //累计盈亏                                                                       
#define		FID_15_PGSL		  1211	 //配股数量                                                                       
#define		FID_15_BL		1212   //比例                                                                               
#define		FID_15_CSLB	   1213		 //参数类别                                                                       
#define		FID_15_FJBZ	   1214		 //附加标识                                                                       
#define		FID_15_QZQKBZ		 1215	   //强制取款标志                                                                 
#define		FID_15_SORTTYPE	1216   //数据查询的排序方式	0,正常顺序(先后) 1,逆序(后先)                               
#define		FID_15_ZCXX	   1217	  //资产下限                                                                        
#define		FID_15_ZCSX	   1218	  //资产上限                                                                        
#define		FID_15_TIME		  1219	 //操作时间                                                                       
#define		FID_15_PGJE		  1220	 //配股金额                                                                       
                                                                                                                  
                                                                                                                  
//以下为债券系统特别需要                                                                                          
                                                                                                                  
#define		FID_15_TGZH	   1221	  //债券系统托管帐户                                                                
#define		FID_15_GZQX	   1222	  //国债期限                                                                        
#define		FID_15_HGTS	   1223	  //回购天数                                                                        
#define		FID_15_GZJYSX		 1224	//国债交易属性                                                                    
#define		FID_15_ZRKHH		1225   //转入客户号                                                                     
#define		FID_15_GZNBDM		 1226	//债券内部代码                                                                    
#define		FID_15_GZFJSX		 1227	//国债附加属性                                                                    
#define		FID_15_GZMZSX		 1228	//面值属性                                                                        
#define		FID_15_GZJXTS		 1229	//国债计息天数                                                                    
#define		FID_15_FXJG	   1230	  //债券发行价格                                                                    
#define		FID_15_TGBH	   1231	  //托管编号                                                                        
#define		FID_15_DYSL	   1232	  //抵押数量                                                                        
#define		FID_15_TGRQ	   1233	  //托管日期                                                                        
#define		FID_15_SXF		 1234	//手续费                                                                            
#define		FID_15_DJSL	   1235	  //冻结数量                                                                        
#define		FID_15_CQDJ	   1236	  //长期冻结                                                                        
#define		FID_15_JYFY	   1237	  //交易费用                                                                        
#define		FID_15_DQBX	   1238	  //债券到期本息                                                                    
#define		FID_15_GZLL	   1239	  //国债利率                                                                        
#define		FID_15_DQRQ	   1240	  //到期（兑付日期）                                                                
#define		FID_15_JSJXRQ		 1241	//开始记息日期                                                                    
#define		FID_15_LTRQ	   1242	  //开始流通日期                                                                    
#define		FID_15_FXZQDJR	  1243	 //派息登记日                                                                   
#define		FID_15_ZQFXRQ		 1244	//派息日                                                                          
#define		FID_15_DFZQDJR	  1245	 //兑付登记日                                                                   
#define		FID_15_BRLX	   1246	  //记账式债券单位（100）本日利息                                                   
#define		FID_15_HGJG	   1247	  //回购价格                                                                        
#define		FID_15_GHJG	   1248	  //购回价格                                                                        
#define		FID_15_KTBZ	   1249	  //开通标志                                                                        
#define		FID_15_HGSL	   1250	  //回购数量                                                                        
#define		FID_15_HGRQ	   1251	  //回购日期                                                                        
#define		FID_15_GHRQ	   1252	  //购回日期                                                                        
#define		FID_15_ZRTGZH		 1253	//转入托管帐户                                                                    
#define		FID_15_QTZC	   1254	  //其他资产                                                                        
#define		FID_15_ZZC		 1255	//总资产                                                                            
#define		FID_15_WQSZJ		1256   //未清算资金                                                                     
#define		FID_15_JGDM	   1257	  //机构代码                                                                        
#define		FID_15_DFSXFL		 1258	//兑付手续费率                                                                    
#define		FID_15_DFLXSL		 1259	//兑付利息税率                                                                    
#define		FID_15_JXFS	   1260	  //计息方式                                                                        
                                                                                                                  
#define		FID_15_YXHSBL1	  1300	   //营业部有效户数比例                                                         
#define		FID_15_YXHSBL2	  1301	   //总部有效户数比例                                                           
#define		FID_15_ENDKHH		 1302	  //结束客户号                                                                    
                                                                                                                  
#define		FID_15_FILETRANSIZE	1304   //文件传送时的大小,比如：压缩后                                              
#define		FID_15_FILESIZE	   1303	  //文件大小                                                                    
                                                                                                                  
//Add Jerry	  Gu   2003.4.21                                                                                      
#define		FID_15_YJL			  1303		 //佣金率                                                                     
#define		FID_15_YJLXX		 1304		//最低佣金率                                                                    
#define		FID_15_YJXX		1305	//最低佣金                                                                          
#define		FID_15_YJSX		1306	//最高佣金                                                                          
#define		FID_15_YHSL		1307	//印花税率                                                                          
#define		FID_15_GHFL		1308	//过户费率                                                                          
#define		FID_15_GHFXX		 1309	 //最低过户费                                                                     
#define		FID_15_GHFSX		 1310	 //最高过户费                                                                     
#define		FID_15_FJF			  1311	  //附加费                                                                      
#define		FID_15_JSFL		1312	//结算费率                                                                          
#define		FID_15_JSFXX		 1313	 //最低结算费                                                                     
#define		FID_15_JSFSX		 1314	 //最高结算费                                                                     
#define		FID_15_JYGFL		 1315	 //交易规费率                                                                     
#define		FID_15_JYGFXX		  1316	  //交易规费下限                                                                
#define		FID_15_JYGFSX		  1317	  //交易规费上限                                                                
#define		FID_15_QTFL		1318	//其它费率                                                                          
#define		FID_15_QTFXX		 1319	 //最低其他费用                                                                   
#define		FID_15_QTFSX		 1320	 //最高其他费用                                                                   
#define		FID_15_FID			  1321	 //字段FID                                                                      
                                                                                                                  
//add by DaiXiaoGe 2003.05.09 客户服务定制                                                                        
#define		FID_15_FWDM		1323	  //客户服务代码                                                                    
#define		FID_15_FWMC		1324	  //客户服务名称                                                                    
#define		FID_15_FWLX		1325	  //客户服务类型                                                                    
#define		FID_15_SLZT		1326	  //客户服务受理主体                                                                
#define		FID_15_CLFS		1327	  //客户服务处理方式                                                                
#define		FID_15_FILE		1328	  //文件名                                                                          
#define		FID_15_FILE2		 1329	   //文件名二                                                                     
#define		FID_15_PROCNAME		 1330	   //过程名                                                                     
#define		FID_15_CSXH		1331	  //参数序号                                                                        
#define		FID_15_TYPE		1332	  //参数类型                                                                        
#define		FID_15_QSZ			  1333	//缺省值                                                                        
#define		FID_15_CS1			  1335	//参数1                                                                         
#define		FID_15_CS2			  1336	//参数2                                                                         
#define		FID_15_CS3			  1337	//参数3                                                                         
#define		FID_15_CS4			  1338	//参数4                                                                         
#define		FID_15_CS5			  1339	//参数5                                                                         
#define		FID_15_CS6			  1340	//参数6                                                                         
#define		FID_15_CS7			  1341	//参数7                                                                         
#define		FID_15_CS8			  1342	//参数8                                                                         
#define		FID_15_CS9			  1343	//参数9                                                                         
#define		FID_15_CS10		1344	  //参数10                                                                          
#define		FID_15_CS11		1345	  //参数11                                                                          
#define		FID_15_CS12		1346	  //参数12                                                                          
#define		FID_15_CS13		1347	  //参数13                                                                          
#define		FID_15_CS14		1348	  //参数14                                                                          
#define		FID_15_CS15		1349	  //参数15                                                                          
#define		FID_15_OPTERATOR		 1350	   //核心开户操作类型                                                         
#define		FID_15_OPTION		  1351	//核心开户参数1                                                                 
#define		FID_15_PACKINDEX		 1352	   //数据包序列号                                                             
#define		FID_15_GYDM				  1353		  //柜员代码                                                                
#define		FID_15_JYJS		1354	  //交易基数                                                                        
#define		FID_15_QTSX		1355	  //其他属性                                                                        
#define		FID_15_TDJY		1356	  //替代交易                                                                        
                                                                                                                  
#define		FID_15_MRJG5		 1357	//买入价格5                                                                       
#define		FID_15_MRSL5		 1358	//买入数量5                                                                       
#define		FID_15_MCJG5		 1359	//卖出价格5                                                                       
#define		FID_15_MCSL5		 1360	//卖出数量5                                                                       
                                                                                                                  
#define		FID_15_ZQSZ_RMB		 1361	//人民币证券市值                                                                
#define		FID_15_ZQSZ_USD		 1362	//美元证券市值                                                                  
#define		FID_15_ZQSZ_HKD		 1363	//港币证券市值                                                                  
#define		FID_15_ZQSZ_ZSRMB		   1364	  //总证券市值折算人民币                                                    
#define		FID_15_ZJYE_RMB		 1365	//人民币资金余额                                                                
#define		FID_15_ZJYE_USD		 1366	//美元资金余额                                                                  
#define		FID_15_ZJYE_HKD		 1367	//港币资金余额                                                                  
#define		FID_15_ZJYE_ZSRMB		   1368	  //总资金余额折算人民币                                                    
#define		FID_15_QTZC_RMB		 1369	//人民币其他资产                                                                
#define		FID_15_QTZC_USD		 1370	//美元其他资产                                                                  
#define		FID_15_QTZC_HKD		 1371	//港币其他资产                                                                  
#define		FID_15_QTZC_ZSRMB		   1372	  //总其他资产折算人民币                                                    
                                                                                                                  
//ADD BY ZHAOLIN FOR 组合宝                                                                                       
#define		FID_15_TZZHDM			1375	//投资组合代码                                                                  
#define		FID_15_TZZHMC			1376	//投资组合代码                                                                  
#define		FID_15_TZZHQZ			1377	//投资组合代码                                                                  
#define		FID_15_TZZHLB			1378	//投资组合代码                                                                  
                                                                                                                  
                                                                                                                  
//add by LiuJianBao	2004-02-12 For MPSS(消息推送服务)                                                             
#define		FID_15_FWLB			1380	//服务类别                                                                        
#define		FID_15_XMZX			1381	//项目子项                                                                        
#define		FID_15_DZLB			1382	//主动推送允许地址类别                                                            
#define		FID_15_QSDZLB			1383	//主动推送缺省地址类别                                                          
#define		FID_15_SJFY			1384	//手机短信方式的服务收费标准                                                      
#define		FID_15_YJFY			1385	//EMAIL方式的服务收费标准                                                         
#define		FID_15_ZF				1386	//涨幅                                                                            
#define		FID_15_DF				1387	//跌幅                                                                            
#define		FID_15_FSCS			1388	//发送次数                                                                        
#define		FID_15_HQNR			1389	//行情点播内容                                                                    
#define		FID_15_ZDSJ			1390	//行情点播允许发送指定时间点                                                      
#define		FID_15_SJQJ			1391	//行情点播允许发送时间区间                                                        
#define		FID_15_ZQDM1			1392	//证券代码1                                                                     
#define		FID_15_ZQDM2			1393	//证券代码2                                                                     
#define		FID_15_ZQDM3			1394	//证券代码3                                                                     
#define		FID_15_ZQDM4			1395	//证券代码4                                                                     
#define		FID_15_ZQDM5			1396	//证券代码5                                                                     
#define		FID_15_RIGHT			1397	//坐席用户权限                                                                  
#define		FID_15_MGSY			1398	//每股收益                                                                        
#define		FID_15_SHDM			1399	//上海市场股票代码(配售)                                                          
#define		FID_15_SZDM			1400	//深圳市场股票代码(配售)                                                          
#define		FID_15_GSHY			1401	//上市公司行业                                                                    
#define		FID_15_GSMC			1402	//上市公司名称                                                                    
#define		FID_15_FXZL			1403	//发行总量                                                                        
#define		FID_15_DHXL			1405	//单户申购限量                                                                    
#define		FID_15_BLFM			1406	//比例分母	配股、送股等                                                          
#define		FID_15_BLFZ			1407	//比例分子	配股、送股等                                                          
#define		FID_15_ZQL				1408	//中签率                                                                        
#define		FID_15_SSGJ			1409	//上市估价                                                                        
#define		FID_15_FXRQ			1410	//发行日期                                                                        
#define		FID_15_ZQYHRQ			1411	//中签摇号日期                                                                  
#define		FID_15_CQRQ			1412	//除权日期                                                                        
#define		FID_15_JKQSRQ			1413	//缴款起始日期                                                                  
#define		FID_15_JKJSRQ			1414	//缴款结束日期                                                                  
#define		FID_15_TSXX			1415	//特别提示信息                                                                    
#define		FID_15_ZGB				1416	//上市公司总股本                                                                
//add end by LiuJian 2004-02-12	For	MPSS                                                                          
#define		FID_15_FILEWRITETIME	 1404	//文件修改时间                                                              
#define		FID_15_FILEDATA	   1505	  //文件数据                                                                    
#define		FID_15_FILEZIP	 1506	//文件是否压缩传送(由服务端决定)                                                  
#define		FID_15_FILECOMPANYNAME	  1507	  //文件的公司名称                                                      
#define		FID_15_FILEPRODUCTNAME	  1508	  //文件的相关产品名称                                                  
#define		FID_15_FILEDIR	 1509	//文件所存放目录                                                                  
#define		FID_15_FILENEW	 1510	//是新文件，强制用户更新                                                          
#define		FID_15_FILERELATION	1511   //上传数据块与已存在的文件的关系(0 新建,1 追加,2	文件指定位置覆盖)           
#define		FID_15_FILEPOSITION	1512   //文件中相对于文件头的偏移                                                   
#define		FID_15_FILETYPE	   1513	  //文件的业务类型                                                              
                                                                                                                  
//CTMIS 接口 报文格式 1550-1560                                                                                   
                                                                                                                  
#define     FID_15_CTVER		   1550   //信息格式版本                                                              
#define     FID_15_CTCODE		   1551   //交易代码                                                                  
#define     FID_15_CTFUNC		   1552   //功能代码                                                                  
#define     FID_15_CTCHANNEL	   1553   //通道                                                                    
#define     FID_15_CTCENTERID	   1554   //分中心编号                                                              
#define     FID_15_CTNODEID	   1555   //节点编号                                                                  
#define     FID_15_CTTELLERID	   1556   //操作员号                                                                
#define     FID_15_CTSEQID		   1557   //流水号                                                                  
#define     FID_15_CTDATE		   1558   //交易日期                                                                  
#define     FID_15_CTTIME		   1559   //交易时间                                                                  
#define		FID_15_CTUSERID	   1560	  //客户编号                                                                    
#define     FID_15_CTDATA		   1561   //业务数据                                                                  
#define     FID_15_CTANSDATA	   1562   //回报数据                                                                
#define     FID_15_BZJZH		   1563	  //保证金帐号                                                                
#define		FID_15_KHF			   1564	  //保证金开户方                                                                
                                                                                                                  
//权证业务                                                                                                        
#define		FID_15_QZDM		   1565	  //权证代码                                                                      
#define		FID_15_XQDM		   1566	  //行权代码                                                                      
#define		FID_15_QZLX		   1567	  //权证类型                                                                      
#define		FID_15_XQFS		   1568	  //行权方式                                                                      
#define		FID_15_XQJG		   1569	  //行权价格                                                                      
#define		FID_15_XQBL		   1570	  //行权比例                                                                      
//T+0 授信                                                                                                        
#define		FID_15_XYJB		   1571	  //信用级别                                                                      
#define		FID_15_MCQS		   1572	  //卖出清算资金                                                                  
#define		FID_15_XYBL		   1573	  //信用比例                                                                      
#define		FID_15_MCGHXY		   1574	  //卖出自动归还信用标志                                                        
#define		FID_15_ZDPCBZ		   1575	  //自动平仓标志                                                                
#define		FID_15_DZJE		   1576	  //待转金额                                                                      
#define		FID_15_KSXYED		   1577	  //起点信用额度                                                                
#define		FID_15_JSXYED		   1578	  //终点信用额度                                                                
#define		FID_15_FJXYED		   1579	  //附加信用额度                                                                
                                                                                                                  
                                                                                                                  
//客户间融资                                                                                                      
#define		FID_15_XYDJ			1601   //信用等级                                                                       
#define		FID_15_CCSLXZ			1602   //持仓数量限制                                                                 
#define		FID_15_YJBL			1603   //预警比例                                                                       
#define		FID_15_PCBL			1604   //平仓比例                                                                       
#define		FID_15_LTSL			1605   //流通数量                                                                       
#define		FID_15_TABLENAME		1606   //表名                                                                       
#define		FID_15_FIELDNAME		1607   //字段名                                                                     
#define		FID_15_JDBZ			1608   //借贷标志                                                                       
#define		FID_15_HYZT			1609   //合约状态                                                                       
#define		FID_15_HYBT			1610   //合约标题                                                                       
#define		FID_15_JKHH			1611   //借方客户号                                                                     
#define		FID_15_DKHH			1612   //贷方客户号                                                                     
#define		FID_15_JYYB			1613   //借方营业部                                                                     
#define		FID_15_DYYB			1614   //贷方营业部                                                                     
#define		FID_15_ZCKZDX			1615   //资产控制底线                                                                 
#define		FID_15_HYH			1616   //合约号                                                                         
#define		FID_15_HYJE			1617   //合约金额                                                                       
#define		FID_15_ZJZHBH			1618   //资金账户编号                                                                 
#define		FID_15_RZLV			1619   //融资利率                                                                       
#define		FID_15_JQRQ			1620   //结清日期                                                                       
#define		FID_15_ZHYH			1621   //子合约号                                                                       
#define		FID_15_JZJZH			1622   //借方资金账号                                                                 
#define		FID_15_DZJZH			1623   //贷方资金账号                                                                 
#define		FID_15_JZJYYB			1624   //借方资金营业部                                                               
#define		FID_15_DZJYYB			1625   //贷方资金营业部                                                               
#define		FID_15_EN_JYYB		1626   //借方营业部                                                                   
#define		FID_15_EN_DYYB		1627   //贷方营业部                                                                   
#define		FID_15_EN_JZJYYB		1628   //借方资金营业部                                                             
#define		FID_15_EN_DZJYYB		1629   //贷方资金营业部                                                             
#define		FID_15_DJGY			1630   //登记柜员                                                                       
#define		FID_15_SHGY			1631   //审核柜员                                                                       
#define		FID_15_JKHXM			1632   //借方客户姓名                                                                 
#define		FID_15_DKHXM			1633   //贷方客户姓名                                                                 
#define		FID_15_EN_BH			1634   //资金编号                                                                     
#define		FID_15_EN_ZBH			1635   //资金子编号                                                                   
#define		FID_15_ZHDYCS			1636   //账户抵押次数                                                                 
#define		FID_15_ZBH			1637   //资金子编号                                                                     
#define		FID_15_ZHMC			1638   //账户名称                                                                       
#define		FID_15_JZHYE			1639   //借方账户余额                                                                 
#define		FID_15_DZHYE			1640   //贷方账户余额                                                                 
#define		FID_15_JZXSZ			1641   //借方最新市值                                                                 
#define		FID_15_DZXSZ			1642   //贷方最新市值                                                                 
#define		FID_15_JZCZZ			1643   //借方资产总值                                                                 
#define		FID_15_DZCZZ			1644   //贷方资产总值                                                                 
#define		FID_15_SHTG			1645   //审核是否通过                                                                   
#define		FID_15_JQHYJE			1646   //已结清合约金额                                                               
#define		FID_15_EN_KHYYB		1647   //客户营业部                                                                   
#define		FID_15_EN_ZJYYB		1648   //资金营业部                                                                   
                                                                                                                  
#define		FID_15_SRYE2			1649	//上日余额2                                                                     
#define		FID_15_ZHYE2			1650	//帐户余额2                                                                     
#define		FID_15_ZPJE2			1651	//支票金额2                                                                     
#define		FID_15_DJJE2			1652	//冻结金额2                                                                     
#define		FID_15_YCDJJE2		1653	//异常冻结金额2                                                                 
#define		FID_15_ZJCQDJ2		1654	//资金长期冻结2                                                                 
#define		FID_15_WJSJE2			1655	//未交收金额2                                                                   
#define		FID_15_KYZJ2			1656	//可用资金2                                                                     
#define		FID_15_KQZJ2			1657	//可取资金2                                                                     
#define		FID_15_XJZC2			1658	//现金资产2                                                                     
#define		FID_15_GPSZ2			1659	//股票市值2                                                                     
#define		FID_15_LX2			1660	//预计利息2                                                                       
#define		FID_15_LXS2			1661	//预计利息税2                                                                     
#define		FID_15_TZLX2			1662	//透支利息2                                                                     
#define		FID_15_RZJE2			1663	//融资金额2                                                                     
#define		FID_15_QSJE_B2		1664	//回报买清算资金2                                                               
#define		FID_15_QSJE_S2		1665	//回报卖清算资金2                                                               
#define		FID_15_DRCKJE2		1666	//当日帐户存款金额2                                                             
#define		FID_15_TZCKJE2		1667	//当日通知存款金额2                                                             
#define		FID_15_HAVE_ZCXX		1668	//是否查询资产信息                                                            
#define		FID_15_HAVE_JKCS		1669	//是否查询融资监控参数                                                        
#define		FID_15_XZDJ			1670	//限制等级                                                                        
#define		FID_15_CCBLXZ			1671	//持仓比例限制                                                                  
#define		FID_15_ZHYJE			1672	//总合约金额                                                                    
#define		FID_15_RZJEBL			1673	//融资金额比例                                                                  
#define		FID_15_RZBLSX			1674	//融资金额比例限制                                                              
                                                                                                                  
#define		FID_15_DDLX			1681	//订单类型                                                                        
#define		FID_15_ZSJG			1682	//止损价格                                                                        
#define		FID_15_PLSL			1683	//披露数量                                                                        
#define		FID_15_JYSDDH			1684	//交易所订单号                                                                  
#define		FID_15_RQ1 			1685	//日期1                                                                           
#define		FID_15_ZJJS 			1686	//资金结算主席位                                                                
#define		FID_15_YWSJ 			1687	//业务数据                                                                      
#define		FID_15_SQBZ1 			1688	//申请标志1                                                                     
#define		FID_15_SQBZ2			1689	//申请标志2                                                                     
#define		FID_15_SQWTZJ			1690	//申请委托资金                                                                  
#define		FID_15_QRLSHM			1691	//确认流水号码                                                                  
                                                                                                                  
//期货系统                                                                                                        
#define		FID_15_PCKZ			1710	//平仓控制                                                                        
#define		FID_15_JSFS			1711	//结算方式                                                                        
#define		FID_15_XWLX			1712	//席位类型                                                                        
#define		FID_15_JGDW			1713	//价格单位                                                                        
//#define		FID_15_BDJW			1714	//变动价位                                                                      
#define		FID_15_KCDW			1715	//开仓单位                                                                        
#define		FID_15_JYBM			1716	//交易编码                                                                        
#define		FID_15_TZLB			1717	//投资类别                                                                        
#define		FID_15_KPBZ			1718	//开平标志                                                                        
#define		FID_15_BZJ				1720	//保证金                                                                        
#define		FID_15_JYSBZJ			1721	//交易所保证金                                                                  
#define		FID_15_JYSSXF			1721	//交易所手续费                                                                  
#define		FID_15_JGSL			1722	//交割数量                                                                        
#define		FID_15_LJZK			1723	//累计折扣                                                                        
#define		FID_15_FXL				1724	//风险率                                                                        
#define		FID_15_ZFXL			1725	//昨风险率                                                                        
#define		FID_15_PCSL			1726	//平仓数量                                                                        
#define		FID_15_PCYK			1727	//平仓盈亏                                                                        
#define		FID_15_PCRQ			1728	//平仓日期                                                                        
#define		FID_15_CCWTH			1729	//持仓委托号                                                                    
#define		FID_15_GSDM			1730	//公司代码                                                                        
#define		FID_15_SBJLH			1731	//申报记录号                                                                    
#define		FID_15_ZKFY			1732	//折扣费用                                                                        
#define		FID_15_FHJE			1733	//返还金额                                                                        
#define		FID_15_CJJJ			1734	//成交均价                                                                        
#define		FID_15_DJBZJ			1735	//冻结保证金                                                                    
#define		FID_15_SBGY			1736	//申报柜员                                                                        
#define		FID_15_BDJE			1737	//变动金额                                                                        
#define		FID_15_BCDJ			1738	//本次冻结                                                                        
#define		FID_15_BDSJ			1739	//变动时间                                                                        
#define		FID_15_CCSL			1740	//持仓数量                                                                        
#define		FID_15_BZJBL			1741	//保证金比例                                                                    
#define		FID_15_DWBZJ			1742	//单位保证金                                                                    
#define		FID_15_JYSBZJBL		1743	//交易所保证金比例                                                              
#define		FID_15_JYSDWBZJ		1744	//交易所单位保证金                                                              
#define		FID_15_LJFH			1745	//累计返还                                                                        
#define		FID_15_BRSXF			1746	//本日手续费                                                                    
#define		FID_15_BRPCYK			1747	//本日平仓盈亏                                                                  
#define		FID_15_JYSBRSXF		1748	//交易所本日手续费                                                              
#define		FID_15_JYSCCXE			1749	//交易所持仓限额                                                              
#define		FID_15_KHCCXE			1750	//客户持仓限额                                                                  
// 开放基金	  2000-----2499                                                                                       
#define		FID_15_OFSS_TADM		  2000	 //TA代码                                                                   
#define		FID_15_OFSS_JSLX		  2001	 //结算类型                                                                 
#define		FID_15_OFSS_JSJG		  2002	 //结算机构                                                                 
#define		FID_15_OFSS_JSZH		  2003	 //结算帐号                                                                 
#define		FID_15_OFSS_BZ		   2004	  //结算币种                                                                  
#define		FID_15_OFSS_JYFW		  2005	 //交易范围                                                                 
#define		FID_15_OFSS_YJFS		  2007	 //邮寄方式                                                                 
#define		FID_15_OFSS_FHFS		  2008	 //分红方式                                                                 
#define		FID_15_OFSS_JJZHSX		2009   //帐户属性                                                                 
#define		FID_15_OFSS_JJZHLB		 2010	//帐户类别                                                                  
#define		FID_15_OFSS_JJZHZT		 2011	//帐户状态                                                                  
#define		FID_15_OFSS_JJZHXM		 2012	//帐户姓名                                                                  
                                                                                                                  
#define		FID_15_OFSS_JJZH		   2015	  //基金帐号                                                                
#define		FID_15_OFSS_KHZT		   2016	  //客户状态                                                                
#define		FID_15_OFSS_DJYY		  2017	 //冻结原因***                                                              
#define		FID_15_OFSS_JDYY		  2019	 //解冻原因***                                                              
#define		FID_15_OFSS_CRFE		  2021	 //存入份额***                                                              
#define		FID_15_OFSS_QCFE		  2022	 //取出份额***                                                              
#define		FID_15_OFSS_WTFE		   2023	  //委托份额                                                                
                                                                                                                  
#define		FID_15_OFSS_WTFS		   2025	  //委托方式                                                                
#define		FID_15_OFSS_SQBH		  2026	 //申请编号                                                                 
#define		FID_15_OFSS_JJDM		  2027	 //基金代码                                                                 
#define		FID_15_OFSS_WTJE		   2028	  //委托金额                                                                
#define		FID_15_OFSS_JESHCL		 2029	//巨额赎回处理标志                                                          
#define		FID_15_OFSS_YYRQ		   2030	  //预约日期                                                                
#define		FID_15_OFSS_YSQBH		   2031	  //原申请编号                                                              
#define		FID_15_OFSS_DFXSDM		 2032	//对方销售商代码                                                            
#define		FID_15_OFSS_DFWDH		2033   //对方网点号                                                                 
#define		FID_15_OFSS_DFJYZH		 2034	//对方交易账号                                                              
#define		FID_15_OFSS_DFJJZH		 2035	//对方基金帐号                                                              
#define		FID_15_OFSS_DFJJDM		 2036	//对方基金代码                                                              
#define		FID_15_OFSS_DQSGRQ1		  2037	 //每月扣款日期1                                                          
#define		FID_15_OFSS_DQSGRQ2		   2038	  //每月扣款日期2                                                         
#define		FID_15_OFSS_DQSGRQ3		   2039	  //每月扣款日期3                                                         
#define		FID_15_OFSS_ZHSBFS		 2042	//帐号识别方式                                                              
#define		FID_15_OFSS_FRDB		   2043	  //法人代表                                                                
                                                                                                                  
//#define	  FID_15_OFSS_YYB		2045   //营业部                                                                     
#define		FID_15_OFSS_JJMC		   2046	  //基金名称                                                                
#define		FID_15_OFSS_ZQSL		   2047	  //基金数量                                                                
#define		FID_15_OFSS_DJSL		   2048	  //冻结数量                                                                
#define		FID_15_OFSS_KYSL		   2049	  //可用数量                                                                
#define		FID_15_OFSS_YWDM		   2051	  //业务代码                                                                
#define		FID_15_OFSS_YWMC		   2052	  //业务名称                                                                
#define		FID_15_OFSS_QRFE		   2054	  //确认份额                                                                
#define		FID_15_OFSS_QRJE		   2055	  //确认金额                                                                
#define		FID_15_OFSS_SBJG		   2057	  //申报结果                                                                
#define		FID_15_OFSS_CDSJ		  2058	 //撤单时间                                                                 
#define		FID_15_OFSS_JJZHZJLB		   2064	  //投资人证件类别                                                      
#define		FID_15_OFSS_JBRXM		   2065	  //经办人姓名                                                              
#define		FID_15_OFSS_JBRZJLB		  2066	 //经办人证件类别                                                         
#define		FID_15_OFSS_JBRZJBH		  2067	 //经办人证件编号                                                         
#define		FID_15_OFSS_FRXM		  2068	 //法人姓名                                                                 
#define		FID_15_OFSS_FRZJLB		2069   //法人证件类别                                                             
#define		FID_15_OFSS_FRZJBH		2070   //法人证件编号                                                             
#define		FID_15_OFSS_YWLB		  2071	 //业务类别                                                                 
#define		FID_15_OFSS_JYBM		  2072	  //交易编码                                                                
#define		FID_15_OFSS_JGDM		  2073	 //机构代码                                                                 
#define		FID_15_OFSS_JGMC		  2074	 //机构名称                                                                 
#define		FID_15_OFSS_JGQC		  2075	 //机构全称                                                                 
#define		FID_15_OFSS_TAMC		  2076	 //TA名称                                                                   
#define		FID_15_OFSS_TYBM		  2077	 //统一编码                                                                 
#define		FID_15_OFSS_TAJC		  2078	 //TA简称                                                                   
#define		FID_15_OFSS_DZ		   2079	  //地址                                                                      
#define		FID_15_OFSS_XGRQ		  2080	 //修改日期                                                                 
#define		FID_15_OFSS_JJJC		  2081	 //基金简称                                                                 
#define		FID_15_OFSS_JYZT		  2082	 //交易状态                                                                 
#define		FID_15_OFSS_JJJZ		  2083	 //基金状态                                                                 
#define		FID_15_OFSS_FXJG		  2084	 //发行价格                                                                 
#define		FID_15_OFSS_FSXZ		  2085	 //                                                                         
#define		FID_15_OFSS_PXFS		  2086	 //                                                                         
#define		FID_15_OFSS_FXZFE		   2087	  //发行总份额                                                              
#define		FID_15_OFSS_GRZGBL		2088   //个人最高比例                                                             
#define		FID_15_OFSS_GRZGRGFE		   2089	  //个人最高认购份额                                                    
#define		FID_15_OFSS_GRZGRGZJ		   2090	  //个人最高认购金额                                                    
#define		FID_15_OFSS_FRZGRGFE		   2091	  //法人最高认购份额                                                    
#define		FID_15_OFSS_FRZGRGZJ		   2092	  //法人最高认购金额                                                    
#define		FID_15_OFSS_GRSCSGZDZJ	 2093	//个人首次申购最低资金                                                    
#define		FID_15_OFSS_FRSCSGZDZJ	 2094	//法人首次申购最低资金                                                    
#define		FID_15_OFSS_GRZJSGZDZJ	 2095	//个人追加申购最低资金                                                    
#define		FID_15_OFSS_FRZJSGZDZJ	 2096	//法人追加申购最低资金                                                    
#define		FID_15_OFSS_JJZSSHFE		   2097	  //基金最少赎回份额                                                    
#define		FID_15_OFSS_RGKSRQ		2098   //认购开始日期                                                             
#define		FID_15_OFSS_RGJSRQ		2099   //认购结束日期                                                             
#define		FID_15_OFSS_JSBZ		  2100	 //结算币种                                                                 
#define		FID_15_OFSS_XXGGRQ		2101   //信息公告日期                                                             
                                                                                                                  
#define		FID_15_OFSS_SCJYRQ		2103   //上次交易日期                                                             
#define		FID_15_OFSS_JJZHDJRQ		   2104	  //基金帐户冻结日期                                                    
#define		FID_15_OFSS_JJSL		  2105	 //基金份额数量                                                             
#define		FID_15_OFSS_ZXSZ		  2106	 //基金最新市值                                                             
#define		FID_15_OFSS_XSWD		  2110	 //销售网点                                                                 
#define		FID_15_OFSS_JJZHSFZH		   2111	  //身份证号                                                            
                                                                                                                  
#define		FID_15_OFSS_SQJE		  2112	 //申请金额                                                                 
#define		FID_15_OFSS_DJZJ		  2114	 //冻结资金                                                                 
#define		FID_15_OFSS_QSZJ		  2115	 //清算资金                                                                 
#define		FID_15_OFSS_GSQX		  2116	 //挂失期限***                                                              
#define		FID_15_OFSS_DJQX		  2117	 //冻结期限***                                                              
#define		FID_15_OFSS_KKRQ		  2118	 //扣款日期                                                                 
#define		FID_15_OFSS_YYBZ		  2119	 //预约标志                                                                 
#define		FID_15_OFSS_BCDJSL		2120   //本次冻结数量                                                             
#define		FID_15_OFSS_DJJSRQ		2129   //冻结结束日期                                                             
#define		FID_15_OFSS_CZZD		  2131	 //操作站点                                                                 
#define		FID_15_OFSS_JDJE		  2132	 //解冻金额                                                                 
#define		FID_15_OFSS_BCDJ		  2133	 //本次冻结                                                                 
#define		FID_15_OFSS_FCJE		  2135	 //付出金额                                                                 
#define		FID_15_OFSS_SRJE		  2136	 //收入金额                                                                 
#define		FID_15_OFSS_ZJYE		  2137	 //资金余额                                                                 
#define		FID_15_OFSS_SQBHCD		2139   //申请编号长度                                                             
#define		FID_15_OFSS_JJZHCD		2140   //基金帐号长度                                                             
#define		FID_15_OFSS_GRJJZSSHFE	 2141	//个人基金最少赎回份额                                                    
#define		FID_15_OFSS_GRJJZSZHFE	 2142	//个人基金最少转换份额                                                    
#define		FID_15_OFSS_FRJJZSSHFE	 2143	//法人基金最少赎回份额                                                    
#define		FID_15_OFSS_FRJJZSZHFE	 2144	//法人基金最少转换份额                                                    
#define		FID_15_OFSS_DJFS		  2145	 //登记方式                                                                 
#define		FID_15_OFSS_GHYY		  2146	 //过户原因                                                                 
#define		FID_15_OFSS_JGZGBL		2147   //机构最高比例                                                             
#define		FID_15_OFSS_JJZSZHFE		   2148	  //基金最少转换份额                                                    
#define		FID_15_OFSS_PZDM		  2149	 //品种代码                                                                 
#define		FID_15_OFSS_DQSGNS		2150   //年数                                                                     
#define		FID_15_OFSS_LSDJ		  2151	 //临时冻结处理                                                             
#define		FID_15_OFSS_MCWTSL		2152   //卖出委托数量                                                             
                                                                                                                  
#define		FID_15_OFSS_TALSH		2153   //TA流水号                                                                   
#define		FID_15_OFSS_FELB		2154   //份额类别                                                                   
#define		FID_15_OFSS_FHJJFE		2155   //分红基金份额                                                             
#define		FID_15_OFSS_FHDWJE		2156   //分红单位金额                                                             
#define		FID_15_OFSS_FHBZ		2157   //分红标志                                                                   
#define		FID_15_OFSS_HLZJE		2158   //红利总金额                                                                 
#define		FID_15_OFSS_SFZJE		2159   //实发红利金额                                                               
#define		FID_15_OFSS_HGZFE		2160   //红股总份额                                                                 
#define		FID_15_OFSS_DJHGFE		2161   //冻结红股份额                                                             
#define		FID_15_OFSS_QYDJRQ		2162   //权益登记日期                                                             
#define		FID_15_OFSS_FHRQ		2163   //分红日期                                                                   
                                                                                                                  
//ADD BY CRD                                                                                                      
#define		FID_15_OFSS_ZKLX		2164   //折扣类型                                                                   
#define		FID_15_OFSS_ZKYW		2165   //折扣业务                                                                   
#define		FID_15_OFSS_JEXX		2166   //金额下限                                                                   
#define		FID_15_OFSS_JESX		2167   //金额上限                                                                   
#define		FID_15_OFSS_ZKL			2168	  //折扣率                                                                    
#define		FID_15_OFSS_GRBZ		2169	 //个人标志                                                                   
#define		FID_15_OFSS_KHSBBS		2170	//客户识别标识                                                              
#define		FID_15_OFSS_ORDERS		2171	//定期定额定单号                                                            
#define		FID_15_OFSS_DFJJMC		2172	//对方基金名称                                                              
                                                                                                                  
#define		FID_15_OFSS_QQCS		2173	//欠缺次数                                                                    
#define		FID_15_OFSS_WYCS		2174	//违约次数                                                                    
                                                                                                                  
#define		FID_15_OFSS_JYZH		2175	//交易账号                                                                    
#define		FID_15_OFSS_GRZDRGZJ	2176	//个人最底认购金额                                                          
#define		FID_15_OFSS_FRZDRGZJ	2177	//法人最底认购金额                                                          
                                                                                                                  
//股票质押 LISH	2500 －－2550                                                                                     
#define		FID_15_ZY_HTBH				  2501	 //合同编号                                                               
#define		FID_15_ZY_CZRBM				  2502	 //出质人编码                                                             
#define		FID_15_ZY_CZRMC				  2503	 //出质人名称                                                             
#define		FID_15_ZY_ZQRBM				  2504	 //质权人编码                                                             
#define		FID_15_ZY_ZQRMC				  2505	 //质权人名称                                                             
#define		FID_15_ZY_ZYHYBH				  2506	 //质押合约编号                                                         
#define		FID_15_ZY_DKHYBH				  2507	 //贷款合约编号                                                         
#define		FID_15_ZY_DKJE				  2508	 //贷款金额                                                               
#define		FID_15_ZY_DKDQRQ				  2509	 //贷款到期日                                                           
#define		FID_15_ZY_HTDJRQ				  2510	 //合同登记日期                                                         
#define		FID_15_ZY_GYDM				  2511	 //合同编号                                                               
#define		FID_15_ZY_KSRQ				  2512	 //贷款到期日                                                             
#define		FID_15_ZY_JSRQ				  2513	 //合同登记日期                                                           
#define		FID_15_ZY_WTZT				  2514	 //委托状态                                                               
#define		FID_15_ZY_SHGY				2515   //申核柜员                                                                 
#define		FID_15_ZY_SHRQ				2516   //申核日期                                                                 
#define		FID_15_ZY_SHSJ				2517   //申核时间                                                                 
#define		FID_15_ZY_SHZD				2518   //申核站点                                                                 
//--end                                                                                                           
#define		FID_15_KSKH				2519   //开始客户号                                                                   
#define		FID_15_JSKH				2520   //结束客户号                                                                   
#define		FID_15_BCZQYE				2521   //本次证券余额                                                               
#define		FID_15_ZY_ZYZH				2522   //质押帐号                                                                 
#define		FID_15_ZY_DJBH				2523   //登记编号                                                                 
#define		FID_15_ZY_SBLSH				2524   //申报流水号                                                               
#define		FID_15_ZY_HTZT				2525   //质押合同状态                                                             
                                                                                                                  
//ISSW FID_15_	define move	from dep2 by liufaxian                                                                
                                                                                                                  
#define		FID_15_KHDM				   3001		   //客户代码                                                               
#define		FID_15_CZMM				   3002		   //操作密码                                                               
#define		FID_15_SMS					3003		   //短信号码                                                               
#define		FID_15_TXMM				   3004		   //通信密码                                                               
#define		FID_15_GTKHLB				3006		   //柜台客户组织类别                                                       
#define		FID_15_ROUTER				3007		   //路由营业部名称                                                         
#define		FID_15_ENTRY				3008		   //配置文件变量名                                                         
#define		FID_15_KHHQZ				3009		   //客户号前缀                                                             
#define		FID_15_CHECKBOX			3010		   //CheckBox                                                               
#define		FID_15_CZLB				   3011		   //操作类别                                                               
#define		FID_15_ZJBL				   3022		   //资金比率                                                               
#define		FID_15_HQZD				   3023		   //行情涨跌                                                               
#define		FID_15_HQZDF				3024		   //行情涨跌幅                                                             
#define		FID_15_HQZGB				3025		   //行情总股本                                                             
#define		FID_15_HQLTSL				3026		   //行情流通股数量                                                         
#define		FID_15_HQZF				   3027		   //行情振幅                                                               
#define		FID_15_HQHSL				3028		   //行情换手率                                                             
#define		FID_15_ICON				   3029		   //图标号                                                                 
#define		FID_15_FZDM				   3030		   //客户号所属分组代码                                                     
#define		FID_15_FZMC				   3031		   //客户号所属分组名称                                                     
#define		FID_15_HFBH				   3032		   //回复编号                                                               
#define		FID_15_FSKHDM				3033		   //发送客户号                                                             
#define		FID_15_FSFJBZ				3034		   //发送客户号附加标志                                                     
#define		FID_15_DFKHDM				3035		   //对方客户代码                                                           
#define		FID_15_STATE				3036		   //在线状态                                                               
#define		FID_15_DLSJ				   3037		   //登录时间                                                               
#define		FID_15_QTSJ				   3038		   //签退时间                                                               
#define		FID_15_MLLB				   3040		   //目录类别                                                               
#define		FID_15_MLBM				   3041		   //目录编码                                                               
#define		FID_15_MLMC				   3042		   //目录名称                                                               
#define		FID_15_MLSM				   3043		   //目录说明                                                               
#define		FID_15_FORMAT				3044		   //目录格式                                                               
#define		FID_15_DEFAULT				3045		   //缺省值                                                               
#define		FID_15_DATA				   3046                                                                               
#define		FID_15_JLBZ				   3047		   //记录标志		  //取值范围                                                
#define		FID_15_MBSMS				3050		   //对方手机号码                                                           
#define		FID_15_MBMAIL				3051		   //对方邮件地址                                                           
#define		FID_15_LBMC				   3053		   //类别名称                                                               
#define		FID_15_CSLX				   3054		   //参数类型                                                               
#define		FID_15_XMMC				   3060		   //服务项目名称                                                           
#define		FID_15_FWXZ				   3061		   //服务项目限制                                                           
#define		FID_15_RECNO				3062		   //结果记录号                                                             
#define		FID_15_YJBT				   3063		   //邮件标题                                                               
#define		FID_15_CCMAIL				3064		   //附送Mail地址                                                           
#define		FID_15_YJXM				   3065		   //预警项目                                                               
#define		FID_15_PRODUCT				3071		   //产品名称                                                             
#define		FID_15_VERSION				3072		   //产品版本号                                                           
#define		FID_15_COMPANY				3073		   //公司名称                                                             
#define		FID_15_COMMENT				3074		   //备注                                                                 
#define		FID_15_LWTIME				3075		   //文件最近修改时间                                                       
#define		FID_15_PKLEN				3076		   //文件数据包大小                                                         
#define		FID_15_PACKAGE				3077		   //文件数据包                                                           
#define		FID_15_SUBDIR				3078		   //文件子目录                                                             
#define		FID_15_ZJHM				   3080		   //证件号码                                                               
#define		FID_15_ZHBZ				   3081		   //帐户标志                                                               
#define		FID_15_IBY					3082		   //备用域...                                                              
#define		FID_15_SJHM				   FID_15_SMS		//手记号码                                                            
#define		FID_15_DZYJ				   FID_15_EMAIL	//电子邮件                                                            
#define		FID_15_BYDZ1				3085		   //备用地址1                                                              
#define		FID_15_BYDZ2				3086		   //备用地址2                                                              
#define		FID_15_BYDZ3				3087		   //备用地址3                                                              
#define		FID_15_XH					3090		   //序号                                                                     
#define		FID_15_JEX					3092		   //金额限                                                                 
#define		FID_15_SXJ					3095                                                                                
#define		FID_15_XXJ					3096                                                                                
#define		FID_15_CJLX				   3097                                                                               
#define		FID_15_CJEX				   3098                                                                               
#define		FID_15_DXFDED				3107		   //短信封顶额度                                                           
#define		FID_15_YJFDED				3108		   //邮件封顶额度                                                           
#define		FID_15_DXFYBL				3109		   //短信费用比率                                                           
#define		FID_15_YJFYBL				3110		   //邮件费用比率                                                           
#define		FID_15_YXSJ				   3113		   //有效时间                                                               
#define		FID_15_LYLX				   3114		   //留言类型                                                               
#define		FID_15_KTZT				   3115		   //开通状态                                                               
#define		FID_15_YWSM				   3116		   //业务说明                                                               
#define		FID_15_WTFSDM				3200		   //委托方式代码                                                           
#define		FID_15_YYBBM				3201		   //本地营业部代码                                                         
#define		FID_15_PRIVATEKEY			3202		   //私钥保护密码                                                         
#define		FID_15_SJ					3203		   //时间                                                                     
#define		FID_15_WTQJ				   3204		   //委托全价                                                               
//ETF增加                                                                                                         
#define		FID_15_KSGSL				3205		   //可申购数量                                                             
#define		FID_15_KSHSL				3206		   //可赎回数量                                                             
#define		FID_15_WTCJSL				3207		   //当前委托的成交委托                                                     
#define     FID_15_WTJW             3208        //委托价位                                                        
#define		FID_15_IOPV				   3209		   //净值估算                                                               
#define		FID_15_TDJE				   3210		   //替代金额                                                               
#define     FID_15_NPSL             3211        //内盘数量                                                        
#define     FID_15_WPSL             3212        //外盘数量                                                        
#define     FID_15_FSCJJG           3213        //分时成交价                                                      
#define     FID_15_FSCJSL           3214        //分时成交量                                                      
#define     FID_15_FSCJJE           3215        //分时成交金额                                                    
#define     FID_15_IMAGE            3216        //image                                                           
#define     FID_15_IDNOTE           3217                                                                          
#define     FID_15_KYDZLB           3218                                                                          
//影像档案管理                                                                                                    
#define		FID_15_DEVICENAME	3800	//物理设备名称                                                                  
#define		FID_15_DATANAME	3801	//数据库名称                                                                      
#define		FID_15_DATASIZE	3802	//数据库大小                                                                      
#define		FID_15_DEVICESIZE	3803	//设备大小                                                                      
#define		FID_15_SPQMC		3804	//适配器名称                                                                      
#define		FID_15_SPQSL		3805	//适配器实例                                                                      
#define		FID_15_GTLX		3806	//柜台类型                                                                          
#define		FID_15_SXH		3807	//顺序号                                                                            
                                                                                                                  
//BFSVR/BFCLNT 增加的数据域	add	by LiuJianBao 2004-05-31                                                          
#define			FID_15_BF_SQL				4002	 //SQL语句                                                                  
#define			FID_15_BF_TABLE			4003	 //表名                                                                     
#define			FID_15_BF_NOTE				4004                                                                            
#define			FID_15_BF_INSINDEX			4005	 //插入索引                                                             
#define			FID_15_BF_MODINDEX			4006	 //回写索引                                                             
#define			FID_15_BF_LSHMC			4007	 //流水号名称                                                               
#define			FID_15_BF_LSH				4008	 //流水号                                                                   
#define			FID_15_BF_ROWCOUNT			4009	 //结果记录数                                                           
#define			FID_15_BF_TRANPORT			4010	 //文件传输服务端口                                                     
#define			FID_15_BF_DATABASE			4011	 //数据库名                                                             
#define			FID_15_BF_DUMPPATH			4012	 //DUMP文件                                                             
#define			FID_15_BF_DUMPTYPE			4013	 //DUMP类型	0 全备，1 差备                                              
#define			FID_15_BF_DUMPNOTE			4014	 //DUMP描述名                                                           
#define			FID_15_BF_UPDATEITEM		4015	 //UPDATE 的字段名                                                      
#define			FID_15_BF_DUMPCREATETIME	4016	 //文件创建时间                                                       
#define			FID_15_BF_DUMPLASTWRITE	4017	 //最后修改时间                                                         
#define			FID_15_BF_IDENTITY			4018	 //是否自增列                                                           
#define			FID_15_BF_STYPE			4019	 //是否委托表                                                               
#define			FID_15_LOWPART				4020	 //LARGE_INTEGER型的低位部分                                              
#define			FID_15_HIGHPART			4021	 //LARGE_INTEGER型的高位部分                                                
                                                                                                                  
//实时开户类自定义FID                                                                                             
#define		FID_15_CZLX	   7000	  //操作类型                                                                        
#define		FID_15_ZQZH	   7001	  //证券账号                                                                        
#define		FID_15_TXDZ	   7003	  //通信地址                                                                        
#define		FID_15_LXDH	   7004	  //联系电话                                                                        
#define		FID_15_CZH		 7005	//传真号                                                                            
#define		FID_15_YYBDM		7006   //营业部代码                                                                     
#define		FID_15_CZYBH		7007   //操作员编号                                                                     
#define		FID_15_KHDDH		7008   //开户点代号                                                                     
#define		FID_15_QYLB	   7009	  //股东性质                                                                        
#define		FID_15_TZRXM		7010   //投资人名称                                                                     
#define		FID_15_TZRLB		7011   //投资人类别                                                                     
#define		FID_15_ZJDM	   7012	  //证件代码                                                                        
//#define	 FID_15_YSFZH		   7013	//原股东姓名/单位全称                                                           
#define		FID_15_YSFZ	   7013		 //原股东姓名/单位全称                                                            
#define		FID_15_YGDXM		7014   //原身份证号/注册号码                                                            
#define		FID_15_XZQZH		7015   //最终证券帐号                                                                   
#define		FID_15_QQLB	   7016	  //请求类别                                                                        
#define		FID_15_YSFZH		7017   //原股东姓名/单位全称                                                            
#define		FID_15_GDJC		7018	//股东简称                                                                          
//三板认领                                                                                                        
#define		FID_15_GDDM		 7101	//老股东号                                                                          
#define		FID_15_SBGDDM		7102   //三板股东号                                                                     
#define		FID_15_GFXZ		 7103	//股份性质                                                                          
#define		FID_15_DJGS		 7104	//待登记，确认股数                                                                  
#define		FID_15_DHHM		 7105	//电话号码                                                                          
#define		FID_15_WTXH		 7106	//委托序号                                                                          
#define		FID_15_WTRQ		 7107	//委托日期                                                                          
#define		FID_15_DYBZ		 7108	//打印标志                                                                          
#define		FID_15_BZXX		 7109	//备注信息                                                                          
#define		FID_15_TZRMC		   7110	  //股东姓名/单位全称                                                           
#define		FID_15_ZJDM_OLD	  7111	 //原身份证号                                                                   
#define		FID_15_TZRMC_OLD	7112   //原股东姓名/单位全称                                                          
#define		FID_15_SBBZ		 7113	//申报标志                                                                          
#define		FID_15_CLSM		 7114	//处理说明                                                                          
#define		FID_15_CXLB		 7115	//查询类别                                                                          
#define		FID_15_XWH		7116   //席位号                                                                           
#define		FID_15_KHHY		 7117	//开户会员                                                                          
#define		FID_15_TZRJC		   7119	  //投资人简称                                                                  
#define		FID_15_XB		   7120	  //性别                                                                            
#define		FID_15_TZLLB		   7121	  //投资人类别                                                                  
#define		FID_15_CSRQ		 7122	//出生日期                                                                          
#define		FID_15_FRLB		 7124	//法人类别                                                                          
#define		FID_15_HYDM		 7125	//行业代码                                                                          
#define		FID_15_JSHY		 7126	//结算会员                                                                          
#define		FID_15_GCZH		 7127	//过出证券帐户                                                                      
#define		FID_15_GCXW		 7128	//过出席位号                                                                        
#define		FID_15_GRZH		 7129	//过入证券帐户                                                                      
#define		FID_15_GRXW		 7130	//过入席位号                                                                        
#define		FID_15_HBRQ		 7131	//回报日期                                                                          
#define		FID_15_YWLX		 7132	//业务类型                                                                          
#define		FID_15_WWMC		 7133	//外文名称                                                                          
#define		FID_15_TJSL		  7134	 //统计数量                                                                       
#define		FID_15_SFJE		  7135	 //收费金额                                                                       
#define		FID_15_DJGSSF		7136	//登记公司收费                                                                    
#define		FID_15_DLJGSF		7137	//代理机构收费                                                                    
#define		FID_15_GRSFZH		7138	//待过入身份证号                                                                  
#define		FID_15_GRGDXM		7139	//待过入股东姓名                                                                  
#define		FID_15_GRTXDZ		7140	//待过入通讯地址                                                                  
#define		FID_15_GRDHHM		7141	//待过入电话号码                                                                  
#define		FID_15_GRYZBM		7142	//待过入邮政编码                                                                  
#define		FID_15_WTGS		7143	//委托股数                                                                          
#define		FID_15_DFXW		7144	//对方席位                                                                          
#define		FID_15_DFDM		7145	//对方帐户                                                                          
#define		FID_15_DFXZ		7146	//对方性质                                                                          
#define		FID_15_QRBZ		7147	//确认标志                                                                          
#define		FID_15_QQGS		7148	//确权股数                                                                          
#define		FID_15_ZBQS		7149	//主办券商                                                                          
#define		FID_15_GHGS		7150	//确权股数                                                                          
#define		FID_15_DBQS		7151	//主办券商                                                                          
#define		FID_15_GFXZSM		7152	//主办券商                                                                        
//add by xiongbin 2004.03.26                                                                                      
#define		FID_15_LB			7153	//券商类别                                                                          
#define		FID_15_QQSF_GR		7154	//个人确权收费                                                                  
#define		FID_15_QQSF_JG		7155	//机构确权收费                                                                  
#define		FID_15_CBQSDM		7156	//承办券商代码                                                                    
#define		FID_15_CBQSMC		7157	//承办券商名称                                                                    
                                                                                                                  
// add by CHF 2004.01.29 档案管理                                                                                 
#define		FID_15_ID					9001	//档案编号                                                                      
#define		FID_15_MODULE				9002	//模块名                                                                      
#define		FID_15_NAME				9003	//模块文件                                                                      
#define		FID_15_CLASE				9004	//模块分类                                                                    
#define		FID_15_LEVEL				9006	//优先                                                                        
#define		FID_15_OLDVER				9007	//旧版本                                                                      
#define		FID_15_LASTVER				9008	//新版本                                                                    
#define		FID_15_WHYS				9009	//修改原因                                                                      
#define		FID_15_FILEPATH			9011	//文件路径                                                                    
#define		FID_15_MENDER				9012	//修改人                                                                      
#define		FID_15_CONNER				9013	//测试人                                                                      
#define		FID_15_MODIRQ				9014	//修改日期                                                                    
#define		FID_15_TESTRQ				9015	//测试日期                                                                    
#define		FID_15_CONCLUSION			9016	//测试结论                                                                  
#define		FID_15_UPNOTE				9017	//升级提示                                                                    
#define		FID_15_STATUS				9018	//记录状态                                                                    
#define		FID_15_RESOLVENT			9019	//关联提示                                                                  
#define		FID_15_FLAG				9020	//档案类型                                                                      
#define		FID_15_DIFF				9021	//难度                                                                          
#define		FID_15_YZJB				9022	//严重级别                                                                      
#define		FID_15_RWBH				9023	//任务编号                                                                      
#define		FID_15_DIR					9024	//运行目录                                                                    
#define		FID_15_CPID				9025	//产品ID                                                                        
#define		FID_15_XQLX				9026	//需求类型                                                                      
#define		FID_15_XQLY				9027	//需求来源                                                                      
#define		FID_15_XQMS				9028	//需求描述                                                                      
#define		FID_15_XQLXF				9029	//需求联系方                                                                  
#define		FID_15_XGFA				9030	//修改方案                                                                      
#define		FID_15_FASP				9031	//方案审批人                                                                    
#define		FID_15_FASPRQ				9032	//方案审批日期                                                                
#define		FID_15_JRX					9033	//兼容性                                                                      
#define		FID_15_FFJH				9034	//发放计划                                                                      
#define		FID_15_XGR					9035	//修改人                                                                      
#define		FID_15_XGJD				9036	//修改进度                                                                      
#define		FID_15_CSR					9037	//测试人                                                                      
#define		FID_15_CSJD				9038	//测试进度                                                                      
#define		FID_15_YMTS				9039	//源码提示                                                                      
#define		FID_15_XGFK				9040	//修改反馈                                                                      
#define		FID_15_XGWCRQ				9041	//修改完成日期                                                                
#define		FID_15_CSFK				9042	//测试反馈                                                                      
#define		FID_15_CSWCRQ				9043	//测试完成日期                                                                
#define		FID_15_HFYJ				9044	//回访意见                                                                      
#define		FID_15_HFRQ				9045	//回访日期                                                                      
#define		FID_15_HFR					9046	//回访人                                                                      
#define		FID_15_RWBT				9047	//任务标题                                                                      
#define		FID_15_MTYPE				9048	//类型                                                                        
#define		FID_15_GYXM				9049	//柜员姓名                                                                      
#define		FID_15_JLZT				9050	//记录状态                                                                      
#define		FID_15_GH					9051	//工号                                                                          
                                                                                                                  
#define		FID_15_KHMC				9052	//客户名称                                                                      
#define		FID_15_WHR					9053	//维护人                                                                      
#define		FID_15_XZR					9054	//协助人                                                                      
#define		FID_15_KHID				9055	//客户ID                                                                        
#define		FID_15_WTMS				9056	//问题描述                                                                      
#define		FID_15_FXDW				9057	//分析定位                                                                      
#define		FID_15_JJFS				9058	//解决方式                                                                      
#define		FID_15_JJGC				9059	//解决过程                                                                      
#define		FID_15_CLYS				9060	//处理用时                                                                      
#define		FID_15_FSRQ				9061	//发生日期                                                                      
                                                                                                                  
#define		FID_15_WTFL				9062                                                                                  
#define		FID_15_GROUP				9063	//组                                                                          
#define		FID_15_SECURITY			9064	//                                                                            
#define		FID_15_GXQ					9065	//                                                                            
#define		FID_15_CPXX				9066	//产品信息                                                                      
#define		FID_15_KHZB				9067	//客户总部                                                                      
#define		FID_15_QYZB				9068	//                                                                              
#define		FID_15_CZ					9069	//传真                                                                          
#define		FID_15_ZJL					9070	//总经理                                                                      
#define		FID_15_ZJLDH				9071	//总经理电话                                                                  
#define		FID_15_ZJLMAIL				9072	//总经理EMAIL                                                               
#define		FID_15_JL					9073	//经理                                                                          
#define		FID_15_JLDH				9074	//经理电话                                                                      
#define		FID_15_JLMAIL				9075	//经理EMAIL                                                                   
#define		FID_15_LXR					9076	//联系人                                                                      
#define		FID_15_CRDATE				9077	//创建日期                                                                    
#define		FID_15_ENGINEERS			9078	//工程师                                                                    
#define		FID_15_HFRQ2				9079	//恢复日期2                                                                   
#define		FID_15_DJR					9080	//登记人                                                                      
#define		FID_15_ZW					9081	//职位                                                                          
#define		FID_15_WHBT				9082	//维护标题                                                                      
#define		FID_15_FLAG1				9083	//FLAG1                                                                       
#define		FID_15_FLAG2				9084	//FLAG2                                                                       
#define		FID_15_XQLB				9085	//需求类别                                                                      
#define		FID_15_MSN					9086	//MSN                                                                         
#define		FID_15_SPFA				9087	//审批方案                                                                      
#define		FID_15_SJNR				9088	//涉及内容                                                                      
#define		FID_15_LCRZ				9089	//流程日志                                                                      
#define		FID_15_SPR					9090	//审批人                                                                      
#define		FID_15_SPRQ				9091	//审批日期                                                                      
#define		FID_15_GZR					9092	//跟踪人                                                                      
#define		FID_15_XQBT				9093	//需求标题                                                                      
#define		FID_15_ZPR					9094	//指派人                                                                      
#define		FID_15_RWMS				9095	//任务描述                                                                      
#define		FID_15_ZPRQ				9096	//指派日期                                                                      
                                                                                                                  
//影像档案管理                                                                                                    
#define		FID_15_SLXX				3801	//数量下限                                                                      
#define		FID_15_XMDM				3802	//项目代码                                                                      
#define		FID_15_DAID				3803	//档案ID                                                                        
#define		FID_15_YJBCDM				3804	//原件保存代码                                                                
                                                                                                                  
                                                                                                                  
//指纹认证类add	by CYQ 2004.05.25                                                                                 
#define		 FID_15_QZYZFS	3900   //强制验证方式                                                                   
#define		 FID_15_KXYZFS	3901   //可选验证方式                                                                   
#define		 FID_15_DLRYZFS	3902   //代理人验证方式                                                                 
                                                                                                                  
//信用授权add by Hangyu	Pan	2004.09.07                                                                            
#define		 FID_15_DSHXY	3903	//授权信用额度                                                                      
#define		 FID_15_XYED	3904	//信用额度                                                                          
#define		 FID_15_XYKM	3905	//信用科目:0 信用申请,1	信用修改,2 信用删除                                         
#define		 FID_15_XYSY	3906	//已使用信用                                                                        
                                                                                                                  
//经纪人工作平台 add by lxj 2005.06.06                                                                            
#define 	 FID_15_GYFLSM              3501 //柜员分类说明                                                         
#define 	 FID_15_ZJDLSJ 	         3502 //最近登录时间                                                            
#define 	 FID_15_ZJTCSJ 	         3503 //最近退出时间                                                            
#define 	 FID_15_ZJGMRQ              3504 //最近更密日期                                                         
#define 	 FID_15_ZTSM                3505 //状态说明                                                             
#define 	 FID_15_GYXMM               3506 //柜员新密码                                                           
#define 	 FID_15_GJCBM               3507 //国籍字符编码                                                         
#define 	 FID_15_GJIBM               3508 //国籍数字编码                                                         
                                                                                                                  
#define 	 FID_15_PROVINCECBM          3510 //省份字符编码                                                        
#define 	 FID_15_PROVINCEMC           3511 //省份名称                                                            
#define 	 FID_15_CITYMC               3512 //城市名称                                                            
#define 	 FID_15_CITYCBM              3513 //城市字符编码                                                        
#define 	 FID_15_SEC                  3514 //辖区数字编码                                                        
#define 	 FID_15_SECCBM               3515 //辖区字符编码                                                        
#define 	 FID_15_SECMC                3516 //辖区名称                                                            
#define	    	FID_15_JJRBH       	    3517   //经纪人编号                                                         
#define	       	FID_15_CSNY        	    3518   //出生年月                                                         
#define	       	FID_15_ZJLBMC      	    3519   //证件类别名称                                                     
#define	       	FID_15_XL          	    3520   //学历                                                             
#define	       	FID_15_GJ          	    3521   //国籍                                                             
#define	       	FID_15_ZHZTMC      	    3522   //帐户状态名称                                                     
#define	       	FID_15_LXID        	    3523   //联系ID                                                           
#define	       	FID_15_CW          	    3524   //称谓                                                             
#define	       	FID_15_JTDH        	    3525   //家庭电话                                                         
#define	       	FID_15_DWDH        	    3526   //单位电话                                                         
#define	       	FID_15_NC          	    3527   //昵称                                                             
#define	       	FID_15_DXHM        	    3528   //短信号码                                                         
#define	       	FID_15_QQH         	    3529   //交流账号                                                         
#define	       	FID_15_MSNID       	    3531   //MSN号码                                                          
#define	       	FID_15_QTHM        	    3532   //其他号码                                                         
#define	       	FID_15_YXLXFS      	    3533   //优先联系方式                                                     
#define	       	FID_15_YXLXFSMC    	    3534   //优先联系方式名称                                                 
#define	       	FID_15_DWDZ        	    3535   //单位地址                                                         
#define	       	FID_15_DWYB        	    3536   //单位邮编                                                         
#define	       	FID_15_JTDZ        	    3537   //家庭地址                                                         
#define	       	FID_15_JTYB        	    3538   //家庭邮编                                                         
#define	       	FID_15_TJRQ        	    3539   //添加日期                                                         
#define			FID_15_WTFSMC			3540 //委托方式名称                                                                 
#define			FID_15_FWXMMC			3541 // 服务项目名称                                                                
#define			FID_15_KHZTMC			3542 // 客户状态名称                                                                
#define			FID_15_DLXM  			3543 // 代理人姓名                                                                  
#define			FID_15_DLDZ  			3544 // 代理地址                                                                    
#define			FID_15_DLQX  			3545 // 代理权限                                                                    
#define			FID_15_DLQXMC			3546 // 代理权限名称                                                                
#define			FID_15_SJXZ  			3547 // 时间限制                                                                    
#define			FID_15_SXYK  			3548 // 实现盈亏                                                                    
#define			FID_15_FSSL			3549 //发生数量                                                                       
#define			FID_15_BCSL            3550 //本次数量                                                                
#define			FID_15_ZJBD            3551 //资金变动                                                                
#define			FID_15_WTLBMC            3552 //委托类别名称                                                          
#define			FID_15_CJBS              3553 //成交笔数                                                              
#define			FID_15_CJBS              3553 //成交笔数                                                              
#define			FID_15_FSDXLX 				3554 //发送短信类型                                                             
#define			FID_15_SJR					3555 //收件人                                                                     
#define			FID_15_SJRDM               3556 //收件人代码                                                          
#define			FID_15_FSBT                3557 //发送标题                                                            
#define			FID_15_FSNR                3558 //发送内容                                                            
#define			FID_15_WJ                  3559 //附件地址                                                            
#define			FID_15_FSKSSJ              3560 //发送开始时间                                                        
#define			FID_15_FSJSSJ              3561 //发送结束时间                                                        
#define			FID_15_FSDXLXMC            3562 //发送对象类型名称                                                    
#define			FID_15_DZLBMC              3563 //地址类别名称                                                        
#define			FID_15_HY					3564//行业                                                                          
#define			FID_15_GPPJ                3565//股票评级                                                             
#define			FID_15_JBMPF               3566//基本面平分                                                           
#define			FID_15_JSMPF               3567//技术面平分                                                           
#define			FID_15_JGRTD               3568//机构认同度                                                           
#define			FID_15_YLYC                3569//盈利预测                                                             
#define			FID_15_TZPJ                3570//投资评级                                                             
#define			FID_15_ZRZDF               3571//昨日涨跌幅                                                           
#define			FID_15_YZZDF               3572//一周涨跌幅                                                           
#define			FID_15_YYZDF               3573//一月涨跌幅                                                           
#define			FID_15_SYZDF               3574//三月涨跌幅                                                           
#define			FID_15_BNZDF               3575//半年涨跌幅                                                           
#define			FID_15_YNZDF               3576//一年涨跌幅                                                           
#define			FID_15_ZRHSL               3577//昨日换手率                                                           
#define			FID_15_YZHSL               3578//一周换手率                                                           
#define			FID_15_YYHSL               3579//一月换手率                                                           
#define			FID_15_SYHSL               3580//三月换手率                                                           
#define			FID_15_BNHSL               3581//半年换手率                                                           
#define			FID_15_YNHSL               3582//一年换手率                                                           
#define			FID_15_LTG                 3583//总市值                                                               
#define			FID_15_ZSZ                 3584//流通市值                                                             
#define			FID_15_LTSZ                3585//流通股比率                                                           
#define			FID_15_LTGBL               3586//国有股比例                                                           
#define			FID_15_GYGBL               3587//十大股东持股                                                         
#define			FID_15_SDGDCG              3588//人均持股数                                                           
#define			FID_15_RJCGS               3589//每股收益                                                             
#define			FID_15_MGJZC               3590//每股净资产                                                           
#define			FID_15_MGXJL               3591//每股现金流                                                           
#define			FID_15_MLL                 3592//毛利率                                                               
#define			FID_15_JLRL                3593//净利润率                                                             
#define			FID_15_ZCFZL               3594//资产负债率                                                           
#define			FID_15_GXL                 3595//股息率                                                               
#define			FID_15_JGXJL               3596//价格/现金流                                                          
#define			FID_15_SJL                 3597//市净率                                                               
#define			FID_15_PEZZL               3598//PE/增长率                                                            
#define			FID_15_SNSG                3599//上年送股                                                             
#define			FID_15_SNHL                3600//上年红利                                                             
#define			FID_15_SNPG                3601//上年配股                                                             
#define			FID_15_BYCJE				3602//本月成交额                                                                  
#define			FID_15_BNCJE				3603//本年成交额                                                                  
#define			FID_15_SNCJE				3604//上年成交额                                                                  
#define			FID_15_BYYJ 				3605//本月佣金                                                                    
#define			FID_15_BNYJ 				3606//本年佣金                                                                    
#define			FID_15_SNYJ 				3607//上年佣金                                                                    
                                                                                                                  
                                                                                                                  
#define			FID_15_DLR					3608//代理人                                                                      
#define			FID_15_ZJYE_HK             3609//港币余额                                                             
#define			FID_15_ZJYE_USA            3610//美圆余额                                                             
#define			FID_15_LXJS_RMB            3611//人民币积数                                                           
#define			FID_15_LXJS_HK             3612//港币积数                                                             
#define			FID_15_LXJS_USA            3613//美圆积数                                                             
#define			FID_15_AGSZ                3614//A股市值                                                              
#define			FID_15_BGSZ                3615//B股市值                                                              
#define			FID_15_JJSZ                3616//基金市值                                                             
#define			FID_15_GZSZ                3617//国债市值                                                             
#define			FID_15_HGSZ                3618//回购市值                                                             
#define			FID_15_DQZC                3619//当前资产                                                             
#define			FID_15_BYPJZC              3620//本月平均资产                                                         
#define			FID_15_BNPJZC              3621//本年平均资产                                                         
#define			FID_15_SNPJZC              3622//上年平均资产                                                         
#define			FID_15_DQCCL               3623//当前持仓率                                                           
#define			FID_15_NCCCL               3624//年初持仓率                                                           
#define			FID_15_SNCCCL              3625//上年初持仓率                                                         
#define			FID_15_BYZCLD              3626//本月资产流动                                                         
#define			FID_15_BNZCLD              3627//本年资产流动                                                         
#define			FID_15_SNZCLD              3628//上年资产流动                                                         
#define			FID_15_BYAGCJ              3629//本月A股成交                                                          
#define			FID_15_BYBGCJ              3630//本月B股成交                                                          
#define			FID_15_BYJJCJ              3631//本月基金成交                                                         
#define			FID_15_BYGZCJ              3632//本月国债成交                                                         
#define			FID_15_BYHGCJ              3633//本月回购成交                                                         
#define			FID_15_BNAGCJ              3634//本年A股成交                                                          
#define			FID_15_BNBGCJ              3635//本年B股成交                                                          
#define			FID_15_BNJJCJ              3636//本年基金成交                                                         
#define			FID_15_BNGZCJ              3637//本年国债成交                                                         
#define			FID_15_BNHGCJ              3638//本年回购成交                                                         
#define			FID_15_SNAGCJ              3639//上年A股成交                                                          
#define			FID_15_SNBGCJ              3640//上年B股成交                                                          
#define			FID_15_SNJJCJ              3641//上年基金成交                                                         
#define			FID_15_SNGZCJ              3642//上年国债成交                                                         
#define			FID_15_SNHGCJ              3643//上年回购成交                                                         
#define			FID_15_BYZZL               3644//本月周转率                                                           
#define			FID_15_BNZZL               3645//本年周转率                                                           
#define			FID_15_SNZZL               3646//上年周转率                                                           
#define			FID_15_BYAGYJ              3647//本月A股佣金                                                          
#define			FID_15_BYBGYJ              3648//本月B股佣金                                                          
#define			FID_15_BYJJYJ              3649//本月基金佣金                                                         
#define			FID_15_BYGZYJ              3650//本月国债佣金                                                         
#define			FID_15_BYHGYJ              3651//本月回购佣金                                                         
#define			FID_15_BNAGYJ              3652//本年A股佣金                                                          
#define			FID_15_BNBGYJ              3653//本年B股佣金                                                          
#define			FID_15_BNJJYJ              3654//本年基金佣金                                                         
#define			FID_15_BNGZYJ              3655//本年国债佣金                                                         
#define			FID_15_BNHGYJ              3656//本年回购佣金                                                         
#define			FID_15_SNAGYJ              3657//上年A股佣金                                                          
#define			FID_15_SNBGYJ              3658//上年B股佣金                                                          
#define			FID_15_SNJJYJ              3659//上年基金佣金                                                         
#define			FID_15_SNGZYJ              3660//上年国债佣金                                                         
#define			FID_15_SNHGYJ              3661//上年回购佣金                                                         
#define			FID_15_BYYJL               3662//本月佣金率                                                           
#define			FID_15_BNYJL               3663//本年佣金率                                                           
#define			FID_15_SNYJL               3664//上年佣金率                                                           
#define			FID_15_AGFDYK              3665//A股浮动盈亏                                                          
#define			FID_15_BGFDYK              3666//B股浮动盈亏                                                          
#define			FID_15_JJFDYK              3667//基金浮动盈亏                                                         
#define			FID_15_GZFDYK              3668//国债浮动盈亏                                                         
#define			FID_15_HGFDYK              3669//回购浮动盈亏                                                         
#define			FID_15_FDYKL               3670//浮动盈亏率                                                           
#define			FID_15_BYZYK               3671//本月总盈亏                                                           
#define			FID_15_BNZYK               3672//本年总盈亏                                                           
#define			FID_15_LNZYK               3673//两年总盈亏                                                           
#define			FID_15_BYYKL               3674//本月盈亏率                                                           
#define			FID_15_BNYKL				3675//本年盈亏率                                                                  
#define			FID_15_LNYKL               3676//两年盈亏率                                                           
#define			FID_15_KMSL                3677//可卖数量                                                             
#define			FID_15_CBJ                 3678//成本价                                                               
#define			FID_15_HGLX                3679//换股类型                                                             
#define			FID_15_STAR                3680//评分                                                                 
#define			FID_15_BT                  3681//标题                                                                 
#define			FID_15_PLLX                3682//评论类型                                                             
#define         FID_15_CXFW                3683//查询范围                                                         
#define			FID_15_XDSYZDF               3684//相对三月涨跌幅                                                     
#define			FID_15_XDBNZDF               3685//相对半年涨跌幅                                                     
#define			FID_15_XDYNZDF               3686//相对一年涨跌幅                                                     
                                                                                                                  
                                                                                                                  
                                                                                                                  
//## 文件下载类                                                                                                   
#define		 FUNC_ID_FILEINFO	1901   //文件检查                                                                     
#define		 FUNC_ID_GETFILE	  1902	 //文件下载                                                                   
#define		 FUNC_ID_ALLFILEINFO   1903	  //全部文件信息                                                            
#define		 FUNC_ID_UPLOADFILE	  1904	 //上传文件                                                                 
                                                                                                                  
                                                                                                                  
//客户管理类                                                                                                      
#define		FUNC_KH_KHXXDJ		11001	//客户信息登记                                                                  
#define		FUNC_KH_KHXXXG		11002	//客户信息修改                                                                  
#define		FUNC_KH_KHXH		11003	//客户销户                                                                        
#define		FUNC_KH_FJSXDJ		11004	//客户附加属性登记                                                              
#define		FUNC_KH_FJSXXG		11005	//客户附加属性修改                                                              
#define		FUNC_KH_FJSXSC		11006	//客户附加属性删除                                                              
#define		FUNC_KH_DLRDJ		11007	//代理人登记                                                                      
#define		FUNC_KH_DLRZX		11008	//代理人注销                                                                      
#define		FUNC_KH_DLRXG		11009	//代理人信息修改                                                                  
#define		FUNC_KH_KHMMXG		11010	//客户密码修改                                                                  
#define		FUNC_KH_KHMMQK		11011	//客户密码清空                                                                  
#define		FUNC_KH_ZJZHKH		11012	//资金帐号开户                                                                  
#define		FUNC_KH_ZJZHXXXG	11013	//资金帐户信息修改                                                              
#define		FUNC_KH_ZJZHMMXG	11014	//资金帐户密码修改                                                              
#define		FUNC_KH_ZJZHXH		11015	//资金帐户销户                                                                  
#define		FUNC_KH_CXJYPZXZ	11016	//查询客户交易品种限制                                                          
#define		FUNC_KH_SZJYPZXZ	11017	//新增客户交易品种限制                                                          
#define		FUNC_KH_SCJYPZXZ	11018	//删除客户交易品种限制                                                          
                                                                                                                  
#define		FUNC_KH_GDZHDJ		11022	//股东帐号登记                                                                  
#define		FUNC_KH_GDZHXXXG	11023   //股东帐号信息修改                                                            
#define		FUNC_KH_GDZHZX		11024	//股东帐号注销                                                                  
#define		FUNC_KH_GDZHZY		11025	//股东帐号转移                                                                  
#define		FUNC_KH_SZZGDH		11026	//设置主股东帐号                                                                
#define		FUNC_KH_PLWTFSXG	11027   //批量委托方式修改                                                            
#define		FUNC_KH_PLWTFSQX	11028   //批量委托方式取消                                                            
#define		FUNC_KH_PLFWXMXG	11029   //批量服务项目修改                                                            
#define		FUNC_KH_PLFWXMQX	11030   //批量服务项目取消                                                            
#define		FUNC_KH_PLKHQZXG	11031   //批量客户群组修改                                                            
#define		FUNC_KH_DJKHLPXX	11032   //登记客户礼品信息                                                            
#define		FUNC_KH_SCKHLPXX	11033   //删除客户礼品信息                                                            
#define		FUNC_KH_CXKHLPXX	11034   //查询客户礼品信息                                                            
#define		FUNC_KH_FWXMDJ		11035   //服务项目登记                                                                
#define		FUNC_KH_FWXMQX		11036   //服务项目取消                                                                
#define		FUNC_KH_QTYWSF		11037   //前台业务收费                                                                
#define		FUNC_KH_CXQTYWSFBZ	11038	//查询前台业务收费标准                                                        
#define		FUNC_KH_JGXXDJ		11039   //机构信息登记                                                                
#define		FUNC_KH_JGXXXG		11040   //机构信息修改                                                                
#define		FUNC_KH_JGXXZX		11041   //机构信息删除                                                                
#define		FUNC_KH_CXMYXZ		11042   //查询客户漫游限制信息                                                        
#define		FUNC_KH_KHZXZXKH	11043	//客户注销转新客户                                                              
#define		FUNC_KH_CHECKXHXD	11044   //销户向导检查(检查是否可以进入销户向导)                                      
#define		FUNC_KH_CXKHTSXX	11045	//查询客户提示信息                                                              
#define		FUNC_KH_SCKHTSXX	11046	//删除客户提示信息                                                              
#define		FUNC_KH_DJZPFKZH	11047	//登记支票付款帐户                                                              
#define		FUNC_KH_XGZPFKZH	11048	//修改支票付款帐户                                                              
#define		FUNC_KH_SCZPFKZH	11049	//删除支票付款帐户                                                              
#define		FUNC_KH_CXZPFKZH	11050	//查询支票付款帐户                                                              
#define		FUNC_KH_QZCSZHDJ	11051	//登记权证创设帐户                                                              
#define		FUNC_KH_QZCSZHSC	11052	//删除权证创设帐户                                                              
#define		FUNC_KH_QZCSZHCX	11053	//查询权证创设帐户                                                              
#define		FUNC_KH_DJKHTSXX	11054	//登记客户提示信息                                                              
#define		FUNC_KH_CXTSXX		11055	//查询提示信息                                                                  
#define		FUNC_KH_CXQTTSXX	11056	//查询全体客户提示信息                                                          
                                                                                                                  
#define	   FUNC_KH_CXWBZH		11070	//查询客户已登记的STOCK2000系统中的资金帐号                                     
#define	   FUNC_KH_XGWBZH		11071	//修改ABOSS系统中的STOCK2000资金帐号                                            
#define	   FUNC_KH_SCWBZH		11072	//删除ABOSS系统中的STOCK2000资金帐号                                            
#define	   FUNC_KH_DJWBZH		11073	//登记ABOSS系统中的STOCK2000资金帐号                                            
//增加 客户管理	模块功能时，请用11070之前的功能码                                                                 
                                                                                                                  
//资金交易类                                                                                                      
#define		 FUNC_ZJ_CHECKCQ	12000	//存取检测                                                                      
#define		 FUNC_ZJ_ZJCQ	  12001	  //资金存取                                                                      
#define		 FUNC_ZJ_ZJNBHZ	 12002	 //资金内部划转                                                                 
#define		 FUNC_ZJ_HCLB	  12003	  //红冲兰补                                                                      
#define		 FUNC_ZJ_LXRZ	  12004	  //利息入帐                                                                      
#define		 FUNC_ZJ_QZQK	  12005	  //强制取款                                                                      
#define		 FUNC_ZJ_ZJCQSQ	 12006	 //资金存取额度授权                                                             
#define		 FUNC_ZJ_DRZJDJ	 12007	 //当日资金冻结                                                                 
#define		 FUNC_ZJ_DRZJJD	 12008	 //当日资金解冻                                                                 
#define		 FUNC_ZJ_ZJCQDJ	 12009	 //资金长期冻结                                                                 
#define		 FUNC_ZJ_ZJCQJD	 12010	 //资金长期解冻                                                                 
#define		 FUNC_ZJ_TZFX	  12011	  //透支罚息                                                                      
#define		 FUNC_ZJ_XGLXJS	 12012	 //调整利息积数                                                                 
#define		 FUNC_ZJ_XGFXJS	 12013	 //调整罚息积数                                                                 
#define		 FUNC_ZJ_KHLPDJ	 12014	 //客户礼品登记                                                                 
                                                                                                                  
#define		 FUNC_GF_ZQZC	  12015	  //证券转出                                                                      
#define		 FUNC_GF_ZQZR	  12016	  //证券转入                                                                      
#define		 FUNC_GF_XGKMSL	 12017	 //修改可卖数量                                                                 
#define		 FUNC_GF_FLTZQZR	  12018	  //非流通股份转入                                                            
#define		 FUNC_GF_FLTZQZC	  12019	  //非流通股份转出                                                            
#define		 FUNC_GF_ZQCQDJ	 12020	 //股份长期冻结                                                                 
#define		 FUNC_GF_ZQCQJD	 12021	 //股份长期解冻                                                                 
                                                                                                                  
#define		FUNC_GF_BZQZR		12022	//标准券转入                                                                      
#define		FUNC_GF_BZQZC		12023	//标准券转出                                                                      
#define		FUNC_GF_XGCCCB		12024	//持仓成本调整                                                                  
#define		FUNC_ZJ_XGZZJE		12025	//修改支票金额                                                                  
#define		FUNC_ZJ_GYYWCZ		12026	//柜员本日业务冲销                                                              
#define		FUNC_ZJ_YZJCQ		12027	//资金预存取(需审核)                                                              
#define		FUNC_ZJ_CXDSHZJCQ	12028	//查询待审核的存取业务                                                          
#define		FUNC_ZJ_SHZJYW		12029	//审核资金业务                                                                  
#define		FUNC_ZJ_CXYSHZJCQ	12030	//查询已审核的存取业务                                                          
#define		FUNC_ZJ_RZCR		12031	//融资存入                                                                        
#define		FUNC_ZJ_RZQC		12032	//融资取出                                                                        
#define		FUNC_ZJ_CXWJQRZ		12033	//查询未结融资                                                                  
#define		FUNC_ZJ_CXYJQRZ		12034	//查询已结融资                                                                  
#define		FUNC_ZJ_CXYWKMXX	12035	//查询业务科目信息                                                              
#define		FUNC_ZJ_SGJD		12036	//申购解冻资金                                                                    
#define		FUNC_ZJ_YYCQ		12037	//存取预约                                                                        
#define		FUNC_ZJ_YYCQSP		12038	//存取预约审批                                                                  
#define		FUNC_ZJ_CXYYCQ		12039	//查询存取预约                                                                  
#define		FUNC_ZJ_TZDRCKJE	12040	//调整帐户当日存款金额                                                          
#define		FUNC_ZJ_CXSXKQZJ	12041	//查询帐户受限可取资金                                                          
#define		FUNC_ZJ_CXYYCQ_KH	12042	//查询客户预约存取明细                                                          
#define		FUNC_ZJ_ZJYW		12043	//资金存取                                                                        
#define		FUNC_ZJ_XHQZQK		12044	//销户强制取款                                                                  
#define		FUNC_ZJ_XYCX		12045	//资金信用查询                                                                    
#define		FUNC_ZJ_XYSQ		12046	//资金信用授权                                                                    
#define		FUNC_ZJ_CXKJDZJ		12047	//查询可解冻资金                                                                
#define		FUNC_ZJ_BZZH		12048	//币种转换                                                                        
#define		FUNC_ZJ_JYSJDJ		12050	//交易时间登记                                                                  
#define		FUNC_ZJ_SXEDTZ		12051	//授信额度调整                                                                  
#define		FUNC_ZJ_SXEDHS		12052	//授信额度回收                                                                  
#define		FUNC_ZJ_SXZJCSH		12053	//授信资金初始化                                                                
                                                                                                                  
                                                                                                                  
//银证业务类                                                                                                      
#define		 FUNC_YZZZ_YHCSCX		12501	//查询银行参数                                                                
#define		 FUNC_YZZZ_YHZHCX		12502	//查询银行帐号                                                                
#define		 FUNC_YZZZ_DJYHZH		12503	//登记银行帐号                                                                
#define		 FUNC_YZZZ_ZXYHZH		12504	//注销银行帐号                                                                
#define		 FUNC_YZZZ_YHZHXXXG		12505	//银行帐号信息修改                                                          
#define		 FUNC_YZZZ_CXYHYE		12506	//查询银行余额                                                                
#define		 FUNC_YZZZ_ZQ_ZZSQ		12507	//证券转帐申请                                                              
#define		 FUNC_YZZZ_ZQ_ZZCL		12508	//证券转帐处理                                                              
#define		 FUNC_YZZZ_YH_ZZSQ		12509	//银行转帐申请                                                              
                                                                                                                  
#define		 FUNC_YZZZ_CZSQ			12511	//冲正申请                                                                    
#define		 FUNC_YZZZ_CZCL			12512	//冲正处理                                                                    
#define		 FUNC_YZZZ_SGTZCL		12513	//手工调帐处理                                                                
#define		 FUNC_YZZZ_DJYZTYHZH	12514	//登记银证通银行帐号                                                        
#define		 FUNC_YZZZ_ZXYZTYHZH	12515	//注销银证通银行帐号                                                        
#define		 FUNC_YZZZ_YZTZJCL		12516	//银证通资金处理                                                            
#define		 FUNC_YZZZ_ZZLSCX		12517	//转帐流水查询                                                                
#define		FUNC_YZZZ_YHCTMIS		12518	//银行主动CTIMS                                                               
#define		FUNC_YZZZ_ZQCTMIS		12519	//证券主动CTIMS                                                               
#define		FUNC_YZZZ_BZJDYGX		12520	//保证金对应关系                                                              
#define		FUNC_YZZZ_YHBZJLS		12521	//银行保证金流水                                                              
                                                                                                                  
#define		FUNC_YZZZ_DJYHCS		12522	//登记银行参数                                                                
#define		FUNC_YZZZ_SCYHCS		12523	//删除银行参数                                                                
#define		FUNC_YZZZ_XGYHCS		12524	//修改银行参数                                                                
#define		FUNC_YZZZ_ZZJGCX		12525	//转帐结果查询                                                                
#define		FUNC_YZZZ_YH_ZZMX		12526	//银行主动转帐流水查询                                                        
                                                                                                                  
#define		FUNC_YZZZ_SJHZCX		12528	//汇总数据查询                                                                
#define		FUNC_YZZZ_CRDZD			12529	//插入对帐单                                                                  
#define		FUNC_YZZZ_ZZYWZX		12530	//撤销转帐业务                                                                
#define		FUNC_YZZZ_KH_ZQZDLS		12531	//客户证券主动流水查询                                                      
#define		FUNC_YZZZ_KH_YHZDLS		12532	//客户银行主动流水查询                                                      
#define		FUNC_YZZZ_SCYHMXDZD		12533	//删除银行明细对帐单                                                        
#define		FUNC_YZZZ_YHZZMXGD		12534	//银行转帐明细勾兑                                                          
#define		FUNC_YZZZ_YHDZDCX		12535	//银行对帐单查询                                                              
#define		FUNC_YZZZ_YHWTZJSQ		12536	//银行委托资金申请                                                          
#define		FUNC_YZZZ_ZJDJJD		12537	//结算帐号资金冻结解冻                                                        
                                                                                                                  
#define		FUNC_YZZZ_CXYZTCS		12547	//查询银证通参数                                                              
#define		FUNC_YZZZ_CXWBDMDZ		12548	//查询外部代码对照                                                          
#define		FUNC_YZZZ_CXYZZZDY		12549	//查询银证转账对应关系                                                      
#define		FUNC_YZZZ_CXDRYHLS		12550	//查询当日银行主动流水                                                      
#define		FUNC_YZZZ_CXDRZQLS		12551	//查询当日证券主动流水                                                      
#define		FUNC_YZZZ_XGYZZZDY		12552	//修改银证转帐对应关系的对应标识                                            
#define		FUNC_YZZZ_CXYCZCLS		12553	//查询异常转出流水                                                          
#define		FUNC_YZZZ_CFSBCZLS		12554	//重发失败冲正流水                                                          
#define		FUNC_YZZZ_XHQZQK		12555	//客户销户强制取款                                                            
#define		FUNC_YZZZ_CXYHKXH		12556	//查询客户开销户信息                                                          
#define		FUNC_YZZZ_YZTZHXG		12557	//更换银证通银行帐号                                                          
#define		FUNC_YZZZ_ALLJGZL		12558	//查询当日交割资料                                                            
#define		FUNC_YZZZ_YZTNEWGDZH	12559	//查询新开银证通股东户                                                      
#define		FUNC_YZZZ_YZTZJHSFIX	12560	//查询资金清算                                                              
#define		FUNC_YZZZ_XTQS			12561	//查询系统是否清算                                                            
#define		FUNC_YZZZ_YZTCXLSZJSQ	12562	//查询银证通历史委托资金申请                                                
#define		FUNC_YZZZ_YZTCXLSZJSQTJ	12563	//查询银证通历史委托资金申请统计                                          
#define		FUNC_YZZZ_YZTQSTYCL		12564	//银证通清算通用处理                                                        
#define		FUNC_YZZZ_YZTSGSBJG		12565	//修改申报结果4->0(证券端为转发的模式)                                      
#define		FUNC_YZZZ_YZTSJYSQ		12566	//银证通交易申请                                                            
#define		FUNC_YZZZ_CXPSYHYE		12567	//银证通配售查询银行资金                                                    
#define		FUNC_YZZZ_CXDJJG		12568	//银证通查询冻结结果                                                          
#define		FUNC_YZZZ_QSHGHKHSJ		12569	//取上海工行开户数据                                                        
#define		FUNC_YZZZ_GXYHZJ		12570	//银证通更新银行资金信息                                                      
#define		FUNC_YZZZ_YZTBDZJDJ		12571	//银证通本地资金冻结                                                        
#define		FUNC_YZZZ_YH_DSFSQ		12572	//银行代收付申请                                                            
#define		FUNC_YZZZ_CXYZZZDYEX	12573	//扩展银证转帐对应关系查询                                                  
#define		FUNC_YZZZ_CZZTGXGSBJG	12574	//修改撤指转托管申报结果                                                    
#define		FUNC_YZZZ_ALLJGZLMF		12575	//查询交割资料(当日清算)                                                    
#define		FUNC_YZZZ_XGYZZZFJCS	12576	//修改银证转帐附加参数                                                      
#define		FUNC_YZZZ_CXYZZZFJCS	12577	//查询银证转帐附加参数                                                      
#define		FUNC_YZZZ_CXJJQS		12578	//查询基金是否清算                                                            
                                                                                                                  
#define		FUNC_JJWBZZ_ZQ_ZZCL		12600	//基金买卖证券方转帐处理                                                    
                                                                                                                  
//证券交易类                                                                                                      
#define		FUNC_JY_ZJMMJY			13001	//资金密码效验                                                                
#define		FUNC_JY_JYMMJY			13002	//交易密码效验                                                                
#define		FUNC_JY_MMWT			13003	//买卖委托                                                                      
#define		FUNC_JY_GETWTH			13004	//获取合同号                                                                  
#define		FUNC_JY_MMCD			13005	//买卖撤单                                                                      
#define		FUNC_JY_SQWTH			13006	//申请委托号                                                                    
#define		FUNC_JY_KMSLJS			13007	//可买入数量计算                                                              
#define		FUNC_JY_JYFYJS			13008	//交易费用计算                                                                
#define		FUNC_JY_SZZTG			13009	//深圳转托管                                                                    
#define		FUNC_JY_YYMRWT			13010	//预约买入委托                                                                
#define		FUNC_JY_YYMCWT			13011	//预约卖出委托                                                                
#define		FUNC_JY_BDWT			13012	//补单委托                                                                      
#define		FUNC_JY_BDMCWT			13013	//补单卖出委托                                                                
#define		FUNC_JY_PLMRWT			13014	//批量买入委托                                                                
#define		FUNC_JY_PLMCWT			13015	//批量卖出委托                                                                
#define		FUNC_JY_PLCD			13016	//批量撤单                                                                      
#define		FUNC_JY_HGWT			13017	//回购委托                                                                      
#define		FUNC_JY_ETFSGSH			13018	//ETF申购赎回                                                                 
#define		FUNC_JY_WTSF			13019	//委托收费                                                                      
#define		FUNC_JY_ZTGSF			13020	//转托管收费                                                                    
#define		FUNC_JY_CDWT			13021	//撤单收费                                                                      
#define		FUNC_JY_KHHCX			13022	//客户号查询                                                                    
#define		FUNC_JY_SHZDJY			13023	//上海A股指定交易                                                             
#define		FUNC_JY_SHCZJY			13024	//上海A股撤指交易                                                             
#define		FUNC_JY_HGZDJY			13025	//上海A股回购交易                                                             
#define		FUNC_JY_HGCZJY			13026	//上海A股回购撤指交易                                                         
#define		FUNC_JY_HBZDJY			13027	//上海B股指定交易                                                             
#define		FUNC_JY_HBCZJY			13028	//上海B股撤指交易                                                             
#define		FUNC_JY_WXYYWT			13029	//网下预约委托                                                                
#define		FUNC_JY_WXYYWTCD		13030	//网下预约委托撤单                                                            
#define		FUNC_JY_HGKMRSL			13031	//计算可融资数量                                                              
#define		FUNC_JY_HGMRLX			13032	//计算融资应付利息                                                            
#define		FUNC_JY_HGKMCSL			13033	//计算可融券数量                                                              
#define		FUNC_JY_HGMCLX			13034	//计算融券利息收入                                                            
#define		FUNC_JY_CXHBZDJYWTMX	13035	//查询沪B指定交易委托明细                                                   
#define		FUNC_GPZY_HTDJ			13036	//质押合同登记                                                                
#define		FUNC_GPZY_HTXXXG		13037	//质押合同修改                                                                
#define		FUNC_GPZY_HTSC			13038	//质押合同删除                                                                
#define		FUNC_GPZY_HTCX			13039	//质押合同查询                                                                
#define		FUNC_GPZY_WTJY			13040	//质押交易                                                                    
#define		FUNC_GPZY_YWCD			13041	//股票质押业务撤单                                                            
#define		FUNC_GPZY_YWCX			13042	//股票质押委托查询                                                            
#define		FUNC_GPZY_CXSHHYWT		13043	//查询上海和约委托                                                          
#define		FUNC_GPZY_SHYW			13044	//质押业务审核                                                                
#define		FUNC_GPZY_SHZYCX		13045	//上海股票质押撤单                                                            
#define		FUNC_JY_QZCSZX			13046	//权证创设注销委托                                                            
                                                                                                                  
#define		FUNC_JY_ZHJYTSXXCX		13048	//组合交易提示信息查询                                                      
#define		FUNC_JY_SHPSCZ			13049	//上海配售冲正                                                                
#define		FUNC_JY_YSINIT			13050	//交易中心日终处理                                                            
#define		FUNC_JY_JYZXINIT		13051	//交易中心系统初始化                                                          
#define		FUNC_JY_PSGFZDJ			13052	//配售股份转登记                                                              
#define		FUNC_JY_SFZQDJSQ		13053	//司法证券解冻冻结申请                                                        
#define		FUNC_JY_SFZQDJSQQR		13054	//司法证券解冻冻结申请确认                                                  
#define		FUNC_JY_CXSFZQDJLS		13055	//查询司法证券冻结申请流水                                                  
#define		FUNC_JY_XGSFZQDJSQCLJG	13056	//修改司法证券冻结申请申报结果                                            
                                                                                                                  
#define		FUNC_JY_ZQJYWT			13081	//证券交易委托                                                                
                                                                                                                  
#define		FUNC_JY_CJSSJD			13201	// 成交实时分部解冻                                                           
#define		FUNC_KH_KHKK			13202	// 实时开户/股份认领扣款                                                        
                                                                                                                  
//交易查询类                                                                                                      
#define		FUNC_JY_DRWTCX			14001	// 客户当日委托查询                                                           
#define		FUNC_JY_DRCJCX			14002	// 客户当日成交查询                                                           
#define		FUNC_JY_FBCJCX			14003	// 客户当日分笔成交查询                                                       
#define		FUNC_JY_CCCX			14004	// 客户持仓查询                                                                 
#define		FUNC_JY_LSWTCX			14005	// 客户历史委托查询                                                           
#define		FUNC_JY_LSCJCX			14006	// 客户历史成交查询                                                           
#define		FUNC_JY_ZJDJCX			14007	// 资金冻结明细查询                                                           
#define		FUNC_JY_ZQDMCX			14008	// 证券代码查询                                                               
#define		FUNC_JY_ZQDJCX			14009	// 股份冻结明细查询                                                           
#define		FUNC_JY_CCHZCX			14010	// 持仓汇总查询                                                               
#define		FUNC_JY_KHZLCX			14011	// 客户资料查询                                                               
#define		FUNC_JY_GDHCX			14012	// 客户股东号查询                                                               
#define		FUNC_JY_ZJCX_ZJZH		14013	// 资金查询按资金号                                                           
#define		FUNC_JY_YHZHCX			14014	// 客户银行帐号查询                                                           
#define		FUNC_JY_ZJCX_KHH		14015	// 资金查询按客户号                                                           
#define		FUNC_JY_KHKDYZQ			14016	// 客户可抵押国债余额                                                         
#define		FUNC_JY_ZHMHCX			14017	// 帐户模糊查询                                                               
#define		FUNC_JY_LSCJHZ			14018	// 客户历史成交汇总                                                           
#define		FUNC_JY_QTLSWTCX		14019	// 全体历史委托查询                                                           
#define		FUNC_JY_QTLSCJCX		14020	// 全体历史成交查询                                                           
#define		FUNC_JY_QTKHCX			14021	// 全体客户查询                                                               
#define		FUNC_JY_DRCJHZ			14022	// 客户当日成交汇总                                                           
#define		FUNC_JY_BGWJSCX			14023	// 客户B股未交收查询                                                          
#define		FUNC_JY_QTLSHZCJCX		14024	// 全体历史汇总成交查询                                                     
#define		FUNC_JY_FJSXCX			14025	// 客户附加属性查询                                                           
#define		FUNC_JY_DLRCX			14026	// 客户代理人查询                                                               
#define		FUNC_JY_KHLLCX			14027	// 客户利率查询                                                               
#define		FUNC_JY_HQCX			14028	// 行情查询                                                                     
#define		FUNC_JY_HQCX_SIMPLE		14029	// 简易行情查询                                                             
#define		FUNC_JY_QSMCCX			14030	// 查询券商名称                                                               
#define		FUNC_JY_GDZLCX			14031	// 股东资料查询                                                               
#define		FUNC_JY_KHPHCX			14032	// 客户配号查询                                                               
#define		FUNC_JY_GZDYDMCX		14033	// 回购可抵押国债代码查询                                                     
#define		FUNC_JY_GZDYCX			14034	// 回购已抵押国债查询                                                         
#define		FUNC_JY_JJRCX			14035	// 经纪人查询                                                                   
#define		FUNC_JY_JJRKHCX			14036	// 经纪人客户查询                                                             
#define		FUNC_JY_QTGDHCX			14037	// 全体股东号查询                                                             
#define		FUNC_JY_QTFJSXCX		14038	// 全体附加属性查询                                                           
#define		FUNC_JY_QTZJZHCX		14039	// 全体资金帐号查询                                                           
#define		FUNC_JY_TSLLZHCX		14040	// 特殊利率帐号查询                                                           
#define		FUNC_JY_QTYZZZDYCX		14041	// 全体银证转帐对应查询                                                     
#define		FUNC_JY_QTGFCX			14042	// 全体股份明细查询                                                           
#define		FUNC_JY_QTGFHZCX		14043	// 全体股份汇总查询                                                           
#define		FUNC_JY_QTFLTGFCX		14044	// 全体非流通股份查询                                                         
#define		FUNC_JY_QTFLTGFHZ		14045	// 全体非流通股份汇总                                                         
#define		FUNC_JY_CZMXCX			14046	// 操作明细查询                                                               
#define		FUNC_JY_GYRZCX			14047	// 柜员日志查询                                                               
#define		FUNC_JY_ZQDJMXCX		14048	// 证券冻结明细查询                                                           
#define		FUNC_JY_ZJDJMXCX		14049	// 资金冻结明细查询                                                           
#define		FUNC_JY_QTDRCJCX		14050	// 全体当日成交查询                                                           
#define		FUNC_JY_QTDRFBCJCX		14051	// 全体当日分笔成交查询                                                     
#define		FUNC_JY_QTDRWTCX		14052	// 全体当日委托查询                                                           
#define		FUNC_JY_EJGDGFCX		14053	// 二级股东股份查询                                                           
#define		FUNC_JY_WJKQZCX			14054	// 未缴款权证查询                                                             
#define		FUNC_JY_WHGFCX			14055	// 无户股份查询                                                               
#define		FUNC_JY_WHHLCX			14056	// 无户红利查询                                                               
#define		FUNC_JY_KHZJMXCX		14057	// 客户资金明细查询                                                           
#define		FUNC_JY_KHZQMXCX		14058	// 客户证券明细查询                                                           
#define		FUNC_JY_KHCZMXCX		14059	// 客户操作明细查询                                                           
#define		FUNC_JY_DRQTCJHZ		14060	// 当日全体成交汇总                                                           
#define		FUNC_JY_LSHQCX			14061	// 历史行情查询                                                               
#define		FUNC_JY_PZHCXZJMX		14062	// 按凭证号查询资金明细                                                       
#define		FUNC_JY_QTZJMXCX		14063	// 全体资金明细查询                                                           
#define		FUNC_JY_QTZQMXCX		14064	// 全体证券明细查询                                                           
#define		FUNC_JY_CXCJGG			14065	// 查询成交公告                                                               
#define		FUNC_JY_CXPHGG			14066	// 查询配号公告                                                               
#define		FUNC_JY_CXZQGG			14067	// 查询中签公告                                                               
#define		FUNC_JY_CXSGKK			14068	// 查询申购扣款                                                               
#define		FUNC_JY_CXDLBZGD		14069	// 查询代理标志股东                                                           
#define		FUNC_JY_CXKHLSZJYE		14070	// 客户历史资金余额查询                                                     
#define		FUNC_JY_CXQTLSZJYE		14071	// 全体历史资金余额查询                                                     
#define		FUNC_JY_CXKHLSZQYE		14072	// 客户历史股份余额查询                                                     
#define		FUNC_JY_CXQTLSZQYE		14073	// 全体历史股份余额查询                                                     
#define		FUNC_JY_CXKHTZELS		14074	// 客户投资额历史查询                                                         
#define		FUNC_JY_CXKHTZSY		14075	// 客户投资损益                                                               
#define		FUNC_JY_CXKHJYL			14076	// 客户交易量查询                                                             
#define		FUNC_JY_CXKHDZD			14077	// 客户历史对帐单                                                             
#define		FUNC_JY_CXKHHZDZD		14078	// 客户历史汇总对帐单                                                         
#define		FUNC_JY_CXZJJR			14079	// 查询子经纪人                                                               
#define		FUNC_JY_CXWGHHG			14080	// 查询未购回回购                                                             
#define		FUNC_JY_CXKHHBYSFZH		14081	// 根据身份证号查询客户号                                                   
#define		FUNC_JY_CXQTKPSSL		14082	// 查询全体客户可配售数量                                                     
#define		FUNC_JY_LSHCXCZMX		14083	// 按流水号查询操作明细                                                       
#define		FUNC_JY_CXPSZQ			14084	// 查询配售中签                                                               
#define		FUNC_JY_LSHCXZQMX		14085	// 按流水号查询证券明细                                                       
#define		FUNC_JY_LSHCXZJDJMX		14086	// 按流水号查询资金冻结明细                                                 
#define		FUNC_JY_QTWBZHCX		14087	// 全体外部帐户查询                                                           
#define		FUNC_JY_QTDLRCX			14088	// 查询全体代理人                                                             
#define		FUNC_JY_EJGFHZCX		14089	// 二级股东股份汇总                                                           
#define		FUNC_JY_DRYYBWTHZ		14090	//当日营业部委托汇总                                                          
#define		FUNC_JY_DRYYBZJKH		14091	//当日营业部资金开户                                                          
#define		FUNC_JY_LSWTCX_KHJL		14092	//历史委托查询(客户经理)                                                    
#define		FUNC_JY_DRWTCX_KHJL		14093	//当日委托查询(客户经理)                                                    
#define		FUNC_JY_LSCJCX_KHJL		14094	//历史成交查询(客户经理)                                                    
#define		FUNC_JY_DRCJCX_KHJL		14095	//当日成交查询(客户经理)                                                    
#define		FUNC_JY_DRCJHZ_FJSX		14096	//当日成交汇总(附加属性)                                                    
#define		FUNC_JY_LSCJHZ_FJSX		14097	//历史成交汇总(客户经理)                                                    
#define		FUNC_JY_HZCXWGHHG		14098	//汇总查询未购回回购                                                          
#define		FUNC_JY_MZQZXJ			14099	//多代码行情查询                                                              
#define		FUNC_JY_MDRWTCX			14100	//按委托号查询当日委托                                                        
#define		FUNC_JY_JJRGDZLCX		14101	//经纪人股东资料查询                                                          
#define		FUNC_JY_JJRZJZHCX		14102	//经纪人客户资金帐号查询                                                      
#define		FUNC_JY_JJRGFCX			14103	//经纪人股份明细查询                                                          
#define		FUNC_JY_JJRGFHZCX		14104	//经纪人股份汇总查询                                                          
#define		FUNC_JY_JJRCJTJCX		14105	//经纪人成交统计查询                                                          
#define		FUNC_JY_JJRCZMX			14106	//经纪人客户操作明细                                                          
#define		FUNC_JY_JJRZJMX			14107	//经纪人客户资金明细                                                          
#define		FUNC_JY_JJRWTMX			14108	//经纪人客户委托明细                                                          
#define		FUNC_JY_JJRCJMX			14109	//经纪人客户成交明细                                                          
#define		FUNC_JY_BCMMYK			14110	//查询卖出盈亏                                                                
#define		FUNC_JY_CXJGKHXX		14111	//查询机构客户信息                                                            
#define		FUNC_JY_CXKHZC_EX		14112	//查询客户资产_扩展                                                           
#define		FUNC_JY_CXCJXSXX		14113	//查询成交显示信息                                                            
#define		FUNC_JY_JJRKHCJPH		14114	//经纪人客户成交排行                                                          
#define		FUNC_JY_JJRCJPH			14115	//各经纪人成交排行                                                            
#define		FUNC_JY_CXJJRZHYJ		14116	//查询经纪人综合业绩                                                          
#define		FUNC_JY_CXJJRZHZBPH		14117	//查询经纪人综合指标排行                                                    
#define		FUNC_JY_CCCX_N			14118	//新客户持仓查询                                                              
#define		FUNC_JY_CCHZCX_N		14119	//新持仓汇总查询                                                              
#define		FUNC_JY_QTGFCX_N		14120	//新全体股份明细查询                                                          
#define		FUNC_JY_QTGFHZCX_N		14121	//新全体股份汇总查询                                                        
#define		FUNC_JY_CXSGKDJMX		14122	//查询申购款冻结明细                                                          
#define		FUNC_JY_CXKHZCXX		14123	//查询客户资产信息                                                            
#define		FUNC_JY_WXYYWTCX		14124	//网下委托查询                                                                
#define		FUNC_JY_CXKHZJCS		14125	//查询客户资金参数(包括所有有关本客户的tKHLLCS，tQZLLCS的参数限制)            
#define		FUNC_JY_CXKHYJZKCS		14126	//查询客户佣金折扣参数(包括所有有关本客户的tQSFYZK_KH，tQSFYZK_QZ的参数限制)
#define		FUNC_JY_CXKHYJFHCS		14127	//查询客户佣金返还参数(包括所有有关本客户的tYJFHCS_KH，tYJFHCS_QZ的参数限制)
#define		FUNC_JY_CXJJSHWHK 		14128 	//查询基金赎回未还款                                                      
#define		FUNC_JY_KHZCMXCX		14129	//全体客户资产明细查询                                                        
#define		FUNC_JY_KHLPXXCX		14130	//全体客户礼品信息查询                                                        
#define		FUNC_JY_KHZCCX			14131	//新全体客户资产查询                                                          
#define		FUNC_JY_XGZQCX_KHJL  	14132	//新股中签查询(按客户经理)                                                  
#define		FUNC_JY_ZCJYLZHCX		14133	//资产交易量综合查询                                                          
#define		FUNC_JY_KHJLLSCJTJ  	14134	//客户经理历史成交统计                                                      
#define		FUNC_JY_KHJLSSCJTJ      14135	//客户经理实时成交统计                                                    
#define		FUNC_JY_KHTZMXCX	    14136	//客户透支明细查询                                                          
#define		FUNC_JY_QTKHCJTJ		14137	//全体客户成交统计   --add by xiongbin 2005.05.26                             
#define		FUNC_JY_JJRKHCJTJCX		14138	//经纪人客户成交统计查询  --add by xiongbin 2005.05.26                      
#define		FUNC_JY_JJRZHCX			14139	//经纪人帐户查询	--add by xiongbin 2005.06.14                                
#define		FUNC_JY_KHCCCX_FJDLR	14140	//客户持仓查询_附加代理人信息	--add by xiongbin 2005.06.22                  
#define		FUNC_JY_CXYYCQ     	  	14141	//查询存取预约明细	--add by cailei 2005.06.27                            
#define		FUNC_JY_GLZHCJMX		14142	//关联成交明细                                                                
#define		FUNC_JY_GLZHDZD			14143	//关联对帐单                                                                  
#define		FUNC_JY_GLZHHZDZD		14144	//关联汇总对帐单                                                              
#define		FUNC_JY_GLLSCJHZ		14145	//关联历史成交汇总                                                            
#define		FUNC_JY_GLDRWTCX		14146	//关联当日委托查询                                                            
#define		FUNC_JY_GLDRCJ			14147	//关联当日成交查询                                                            
#define		FUNC_JY_GLZJCX			14148	//关联资金帐户查询                                                            
#define		FUNC_JY_GLGDHCX			14149	//关联股东号查询                                                              
#define		FUNC_JY_GLCCCX			14150	//关联持仓查询                                                                
#define		FUNC_JY_ZDJGDHCX		14151	//转登记股东帐户查询                                                          
#define		FUNC_JY_SBXWCX		    14152	//申报席位查询                                                              
#define		FUNC_JY_ZDJWTMXCX		14153	//转登记委托明细查询                                                          
#define		FUNC_JY_QTZJZHCX2		14154	//全体资金帐号查询(按客户)                                                    
#define     FUNC_JY_ETFWXRG_DSB     14155   //查询ETF网下认购未申报记录                                           
#define     FUNC_JY_ETFWXRGSB       14156   //ETF网下认购申报                                                     
#define     FUNC_JY_ETFWXRGZWS      14157   //ETF网下现金认购转网上                                               
#define		FUNC_JY_ZJMMJY_WBJG		14191	//外部系统密码效验                                                          
#define		FUNC_JY_ZJCX_WBZH		14200	//资金查询按外部帐号                                                          
                                                                                                                  
//查询当日业务 add by crd                                                                                         
#define		 FUNC_JY_KHDRZJMXCX		14201	  //客户当日资金明细查询                                                    
#define		 FUNC_JY_QTDRZJMXCX		14202	  //全体当日资金明细查询                                                    
#define		 FUNC_JY_KHDRZQMXCX		14203	  //客户当日证券明细查询                                                    
#define		 FUNC_JY_QTDRZQMXCX		14204	  //全体当日证券明细查询                                                    
#define		 FUNC_JY_DRCZMXCX		 14205	   //当日操作明细查询                                                       
#define		 FUNC_JY_KHDRCZMXCX		14206	  //客户当日操作明细查询                                                    
#define		 FUNC_JY_DRZJDJCX		 14207	   //当日资金冻结明细查询                                                   
#define		 FUNC_JY_DRZJDJMXCX		14208	  //当日资金冻结明细查询                                                    
#define		 FUNC_JY_DRZQDJCX		 14209	   //当日股份冻结明细查询                                                   
#define		 FUNC_JY_DRZQDJMXCX		14210	  //当日证券冻结明细查询                                                    
#define		 FUNC_JY_DRGYRZCX		 14211	   //当日柜员日志查询                                                       
#define		 FUNC_JY_LSPZHCXZJMX	 14212	   //按历史凭证号查询资金明细                                             
#define		 FUNC_JY_LSLSHCXCZMX	 14213	   //按历史流水号查询操作明细                                             
#define		 FUNC_JY_LSLSHCXZQMX	 14214	   //按历史流水号查询证券明细                                             
#define		 FUNC_JY_LSLSHCXZJDJMX	14215	  //按历史流水号查询资金冻结明细                                          
#define		 FUNC_JY_CXSHZYHYWT		14216	 //查询上海质押和约委托                                                     
#define		 FUNC_JY_CXGPZYWT		14217	 //查询查询股票质押委托                                                       
                                                                                                                  
//投资理财类-- Add By Jerry	  Gu                                                                                  
#define		FUNC_TZLC_LCZHCX		14501	//理财专户查询                                                                
#define		FUNC_TZLC_LCZHDJ		14502	//理财专户登记                                                                
#define		FUNC_TZLC_LCZHXH		14503	//理财专户销户                                                                
#define		FUNC_TZLC_LCZHTHYY		14504	//理财专户退户预约                                                          
#define		FUNC_TZLC_LCZHTHYYQX	14505	//理财专户退户预约取消                                                      
#define		FUNC_TZLC_XGLCZHSX		14506	//修改理财专户属性                                                          
                                                                                                                  
#define		FUNC_TZLC_DTHZZHDJ		14512	//动态合作帐户开户登记                                                      
#define		FUNC_TZLC_DTHZZHXG		14513	//动态合作帐户信息修改                                                      
#define		FUNC_TZLC_HZZHTH		14515	//合作帐户退户                                                                
#define		FUNC_TZLC_DHHZZHXXCX	14516	//单户合作帐户信息查询                                                      
                                                                                                                  
#define		FUNC_TZLC_LCZJPLKJZJECX	14520	//理财资金批量可集中金额查询                                              
#define		FUNC_TZLC_LCZJPLJZ		14521	//理财资金批量集中                                                          
#define		FUNC_TZLC_LCZJJZQKCX	14522	//理财资金集中情况查询                                                      
#define		FUNC_TZLC_LCZJPLKFHCX	14523	//理财资金批量可返还查询                                                    
#define		FUNC_TZLC_LCZJPLFH		14524	//理财资金批量返还                                                          
#define		FUNC_TZLC_DHLCZJJZ		14525	//单户理财资金集中                                                          
#define		FUNC_TZLC_DHLCZJFH		14526	//单户理财资金返还                                                          
#define		FUNC_TZLC_DTHZJPLDJ		14527	//动态户资金批量冻结                                                        
                                                                                                                  
#define		FUNC_TZLC_GDKLCJY		14540	//股东卡理财借用                                                              
#define		FUNC_TZLC_LCGDKTZJY		14541	//理财股东卡停止借用                                                        
#define		FUNC_TZLC_HZPSZJCQ		14544	//合作配售资金存取                                                          
#define		FUNC_TZLC_HZPSJKPLZJDJ	14545	//合作配售缴款日批量资金冻结                                              
                                                                                                                  
#define		FUNC_TZLC_ZHSGXXCX		14600	//专户申购信息查询                                                          
#define		FUNC_TZLC_DHSGXXCX		14601	//单户申购信息查询                                                          
#define		FUNC_TZLC_LCZJHZXXCX	14602	//理财资金划转信息查询                                                      
#define		FUNC_TZLC_LCZJQSYCCX	14603	//理财资金清算异常查询                                                      
#define		FUNC_TZLC_LCZHZJCX		14604	//专户理财资金情况查询                                                      
#define		FUNC_TZLC_HZWTLSCX		14605	//合作委托历史查询                                                          
#define		FUNC_TZLC_LCZJCQ		14606	//理财资金存取                                                                
#define		FUNC_TZLC_CXLCXMXX		14607	//查询理财项目信息                                                          
#define		FUNC_TZLC_CXLCXMFEXX	14608	//查询理财项目份额信息                                                      
#define		FUNC_TZLC_CXQTKHLCXX	14609	//查询全体客户理财信息                                                      
                                                                                                                  
#define		FUNC_TZLC_LCSYFP		14611	//理财收益分配                                                                
#define		FUNC_TZLC_CXKHKYLCZH	14612	//查询客户可用理财专户信息                                                  
                                                                                                                  
#define		FUNC_TZLC_TZCKPLCX		14614	//可操作通知存款查询                                                        
#define		FUNC_TZLC_TZCKPLDJ		14615	//通知存款批量登记                                                          
#define		FUNC_TZLC_TZCKKHDJ		14616	//通知存款客户登记                                                          
#define		FUNC_TZLC_TZCKPLJQ		14617	//通知存款批量结清                                                          
#define		FUNC_TZLC_TZCKKHJQ		14618	//通知存款单户结清                                                          
#define		FUNC_TZLC_TZCKCKCX		14619	//通知存款未结存款查询                                                      
#define		FUNC_TZLC_TZCKKHCX		14620	//查询通知存款可用客户                                                      
#define		FUNC_TZLC_GDSYKHDJ		14621	//固定收益客户登记                                                          
#define		FUNC_TZLC_GDSYLXRZ		14622	//固定收益利息入账                                                          
#define		FUNC_TZLC_GDSYKHJZ		14623	//固定收益客户结账                                                          
#define		FUNC_TZLC_GDSYKHXMCX	14624	//固定收益客户投资项目查询                                                  
#define		FUNC_TZLC_ZQZZDQFH		14625	//债券增值定期分红                                                          
#define		FUNC_TZLC_ZQZZYWCX		14626	//债券增值业务查询                                                          
#define		FUNC_TZLC_ZQZZFHCX		14627	//债券增值业务当日分红查询                                                  
#define		FUNC_TZLC_YQHZKHDY		14628	//银券合作客户对应                                                          
#define		FUNC_TZLC_YQHZYWSQ		14629	//银券合作业务申请                                                          
#define		FUNC_TZLC_DQTZXMDJ		14630	//短期投资项目登记                                                          
#define		FUNC_TZLC_DQTZKYZJ		14631	//短期投资可用资金查询                                                      
#define		FUNC_TZLC_YHSGZJZCCX	14632	//银行资金转出查询                                                          
#define		FUNC_TZLC_YHSGZJDZ		14633	//银行资金对帐                                                              
#define		FUNC_TZLC_ZQZZDRMXCX	14634	//理财当日明细查询                                                          
#define		FUNC_TZLC_ZQZZJSCX		14635	//债券增值查询                                                              
#define		FUNC_TZLC_ZQZZJXXH		14636	//债券增值销户结息                                                          
#define		FUNC_TZLC_ZQLCSYTQ		14637	//理财收益提取                                                              
#define		FUNC_TZLC_ZQZZXJCQ		14638	//债券增值业务现金存取                                                      
#define		FUNC_TZLC_YHSGKH		14639	//银行申购开户                                                                
#define		FUNC_TZLC_YHSGXH		14640	//银行申购销户                                                                
#define		FUNC_TZLC_YHSGZJRZ		14641	//银行申购资金入帐                                                          
#define		FUNC_TZLC_YHSGLX		14642	//银行申购立项                                                                
#define		FUNC_TZLC_YHSGZJJD		14643	//银行申购资金解冻、收益分配                                                
#define		FUNC_TZLC_YHSGZJTK		14644	//银行申购资金退款                                                          
#define		FUNC_TZLC_YHSGZHCX		14645	//银行申购帐户查询                                                          
#define		FUNC_TZLC_YHZHCX		14646	//银行申购专户查询                                                            
#define		FUNC_TZLC_YHZHDJ		14647	//银行申购专户登记                                                            
#define		FUNC_TZLC_YHZHZX		14648	//银行申购专户注销                                                            
#define		FUNC_TZLC_ZQZZHCLB		14649	//债券增值本金红冲兰补                                                      
#define		FUNC_TZLC_ZQZZDRPZ		14650	//债券增值当日凭证查询                                                      
#define		FUNC_TZLC_LCYWMXCX		14651	//理财业务明细查询                                                          
#define		FUNC_TZLC_LCYWMXPZ		14652	//理财业务资金明细凭证                                                      
#define		FUNC_TZLC_TSPSWT		14653	//理财业务资金明细凭证                                                        
#define		FUNC_TZLC_TSPSSZCL		14654	//特殊配售深圳市值处理                                                      
#define		FUNC_TZLC_TSPSPHCX		14655	//特殊配售配号查询                                                          
#define		FUNC_TZLC_TSPSYCCX		14656	//特殊配售异常查询                                                          
#define		FUNC_TZLC_TSPSYCCL		14657	//特殊配售异常处理                                                          
                                                                                                                  
//系统参数类                                                                                                      
#define		FUNC_XT_ISMYCUSTOM		15001	//取柜员某菜单对股民权限                                                    
#define		FUNC_XT_GETGYQX			15002	//获得柜员权限                                                                
#define		FUNC_XT_GETGYXX			15003	//获取柜员信息                                                                
#define		FUNC_XT_GETGYSX			15004	//获得柜员属性                                                                
#define		FUNC_XT_DOCZLS			15005	//记录柜员操作流水                                                            
#define		FUNC_XT_GETSJZD			15006	//获得数据字典                                                                
#define		FUNC_XT_GETALLJYS		15007	//获得交易所                                                                  
#define		FUNC_XT_GETALLBZ		15008	//获得币种                                                                    
#define		FUNC_XT_GETALLZQLB		15009	//获得证券类别                                                              
#define		FUNC_XT_GETALLYYB		15010	//获得营业部                                                                  
#define		FUNC_XT_GETALLKHQZ		15011	//获得客户群组                                                              
#define		FUNC_XT_GETALLKHSX		15012	//获得客户属性                                                              
#define		FUNC_XT_GETXTCS			15013	//查询系统参数                                                                
#define		FUNC_XT_GETALLJYLB		15014	//查询交易类别                                                              
#define		FUNC_XT_GETALLYWKM		15015	//查询业务科目                                                              
#define		FUNC_XT_GETGYCZQX		15016	//查询柜员对菜单操作权限                                                      
#define		FUNC_XT_GYHCX			15017	//查询柜员号                                                                    
#define		FUNC_XT_CXZQXX			15018	//查询证券信息                                                                
#define		FUNC_XT_GETALLMENU		15019	//查询所有菜单                                                              
#define		FUNC_XT_GETGYJCQX		15020	//查询柜员继承权限                                                            
#define		FUNC_XT_GETGYZJQX		15021	//查询柜员直接权限                                                            
#define		FUNC_XT_GETCARDCS		15022	//查询卡号参数配置                                                            
#define		FUNC_XT_CXGJDMDZ		15023	//查询国籍代码对照                                                            
#define		FUNC_XT_CXYHDM			15024	//查询外部机构代码                                                            
#define		FUNC_XT_CXSFDM			15025	//查询省份代码                                                                
#define		FUNC_XT_CXCSDM			15026	//查询城市代码                                                                
#define		FUNC_XT_CXQXDM			15027	//查询区县代码                                                                
#define		FUNC_XT_CDGYXX			15028	//查询有指定菜单权限的柜员号                                                  
#define		FUNC_XT_CXHL			15029	//查询汇率                                                                      
#define		FUNC_XT_CXCWDM			15030	//查询错误代码                                                                
#define		FUNC_XT_JLGZRZ			15031	//记录工作日志                                                                
#define		FUNC_XT_CXGZRZ			15032	//查询工作日志                                                                
#define		FUNC_XT_GETGYWSHQX		15033	//查询柜员未审核的权限                                                      
#define		FUNC_XT_EXTENDYYB		15034	//查询扩展营业部信息                                                          
#define		FUNC_XT_SJCS			15035	//查询申计参数                                                                  
#define		FUNC_XT_GETALLZQJYSX	15036	//查询证券交易属性参数                                                      
#define		FUNC_XT_GETQSDM			15037	//查询某交易所的券商代码                                                      
#define		FUNC_XT_RSQLCX			15038	//RSQL用于查询的请求                                                          
#define		FUNC_XT_RSQLCZ			15039	//RSQL用于业务操作                                                            
#define		FUNC_XT_GGXXCX			15040	//证券信息扩展查询                                                            
#define		FUNC_XT_CXETFDM			15041	//查询ETF证券代码                                                             
#define		FUNC_XT_CXETFMX			15042	//查询ETF一篮子明细                                                           
#define		FUNC_XT_ETFTZFX			15043	//ETF投资分析                                                                 
#define		FUNC_XT_CXGZSYL			15044	//查询债券到期收益率                                                          
#define		FUNC_XT_CXQZXX			15045	//查询权证信息                                                                
#define		FUNC_XT_CXSXXX			15046	//查询授信信息                                                                
#define		FUNC_XT_CXGYJSXX		15047	//查询有指定角色的柜员号                                                      
#define		FUNC_XT_GETGYLSJCQX		15048	//查询柜员临时继承权限                                                      
#define		FUNC_XT_GETGYLSZJQX		15049	//查询柜员临时直接权限                                                      
#define		FUNC_XT_GETGYLSWSHQX	15050	//查询柜员临时未审核的权限                                                  
#define		FUNC_XT_CXCSZT			15051	//查询系统测试状态                                                            
                                                                                                                  
//营业部管理类                                                                                                    
#define		FUNC_JYGL_CXWTSF		15501   //查询委托收费                                                              
#define		FUNC_JYGL_DJWTSF		15502   //登记委托收费                                                              
#define		FUNC_JYGL_XGWTSF		15503   //修改委托收费                                                              
#define		FUNC_JYGL_SCWTSF		15504   //删除委托收费                                                              
#define		FUNC_JYGL_CXQTSF		15505   //查询前台收费                                                              
#define		FUNC_JYGL_DJQTSF		15506   //登记前台收费                                                              
#define		FUNC_JYGL_XGQTSF		15507   //修改前台收费                                                              
#define		FUNC_JYGL_SCQTSF		15508   //删除前台收费                                                              
#define		FUNC_JYGL_CXCXSF		15509   //查询查询收费                                                              
#define		FUNC_JYGL_DJCXSF		15510   //登记查询收费                                                              
#define		FUNC_JYGL_XGCXSF		15511   //修改查询收费                                                              
#define		FUNC_JYGL_SCCXSF		15512   //删除查询收费                                                              
#define		FUNC_JYGL_CXWTFSSFXS	15513	//查询委托方式收费系数                                                      
#define		FUNC_JYGL_DJWTFSSFXS	15514	//登记委托方式收费系数                                                      
#define		FUNC_JYGL_XGWTFSSFXS	15515	//修改委托方式收费系数                                                      
#define		FUNC_JYGL_SCWTFSSFXS	15516	//删除委托方式收费系数                                                      
#define		FUNC_JYGL_CXKHQZSFXS	15517	//查询客户群组收费系数                                                      
#define		FUNC_JYGL_DJKHQZSFXS	15518	//登记客户群组收费系数                                                      
#define		FUNC_JYGL_XGKHQZSFXS	15519	//修改客户群组收费系数                                                      
#define		FUNC_JYGL_SCKHQZSFXS	15520	//删除客户群组收费系数                                                      
#define		FUNC_JYGL_DJKHQZ		15521   //登记客户群组                                                              
#define		FUNC_JYGL_XGKHQZ		15522   //修改客户群组                                                              
#define		FUNC_JYGL_SCKHQZ		15523   //删除客户群组                                                              
#define		FUNC_JYGL_DJJJR			15524	//登记经纪人                                                                  
#define		FUNC_JYGL_XGJJR			15525	//修改经纪人                                                                  
#define		FUNC_JYGL_SCJJR			15526	//删除经纪人                                                                  
#define		FUNC_JYGL_CXQZLLCS		15527	//查询客户群组利率参数                                                      
#define		FUNC_JYGL_DJQZLLCS		15528	//登记客户群组利率参数                                                      
#define		FUNC_JYGL_XGQZLLCS		15529	//修改客户群组利率参数                                                      
#define		FUNC_JYGL_SCQZLLCS		15530	//删除客户群组利率参数                                                      
#define		FUNC_JYGL_CXKHLLCS		15531	//查询客户利率参数                                                          
#define		FUNC_JYGL_DJKHLLCS		15532	//登记客户利率参数                                                          
#define		FUNC_JYGL_XGKHLLCS		15533	//修改客户利率参数                                                          
#define		FUNC_JYGL_SCKHLLCS		15534	//删除客户利率参数                                                          
#define		FUNC_JYGL_BGCDQX		15535   //变更客户菜单权限                                                          
                                                                                                                  
#define		FUNC_JYGL_CXDFHCDQX		15537   //查询待复核菜单权限                                                      
#define		FUNC_JYGL_CXFHCDQXLS	15538	//查询复核菜单权限流水                                                      
#define		FUNC_JYGL_SHCDQX		15539   //审核菜单权限                                                              
#define		FUNC_JYGL_DJJJRKH		15540	//登记经纪人客户                                                              
#define		FUNC_JYGL_SCJJRKH		15541	//删除经纪人客户                                                              
#define		FUNC_JYGL_CXQZYJZK		15542	//查询群组佣金折扣                                                          
#define		FUNC_JYGL_DJQZYJZK		15543	//登记群组佣金折扣                                                          
#define		FUNC_JYGL_XGQZYJZK		15544	//修改群组佣金折扣                                                          
#define		FUNC_JYGL_SCQZYJZK		15545	//删除群组佣金折扣                                                          
#define		FUNC_JYGL_CXKHYJZK		15546	//查询客户佣金折扣                                                          
#define		FUNC_JYGL_DJKHYJZK		15547	//登记客户佣金折扣                                                          
#define		FUNC_JYGL_XGKHYJZK		15548	//修改客户佣金折扣                                                          
#define		FUNC_JYGL_SCKHYJZK		15549	//删除客户佣金折扣                                                          
#define		FUNC_JYGL_CXKHYJFH		15550	//查询客户佣金返还                                                          
#define		FUNC_JYGL_DJKHYJFH		15551	//登记客户佣金返还                                                          
#define		FUNC_JYGL_XGKHYJFH		15552	//修改客户佣金返还                                                          
#define		FUNC_JYGL_SCKHYJFH		15553	//删除客户佣金返还                                                          
#define		FUNC_JYGL_CXJJRYJFH		15554   //查询经纪人佣金返还                                                      
#define		FUNC_JYGL_DJJJRYJFH		15555   //登记经纪人佣金返还                                                      
#define		FUNC_JYGL_XGJJRYJFH		15556   //修改经纪人佣金返还                                                      
#define		FUNC_JYGL_SCJJRYJFH		15557   //删除经纪人佣金返还                                                      
#define		FUNC_JYGL_CXQZYJFH		15558	//查询群组佣金返还                                                          
#define		FUNC_JYGL_DJQZYJFH		15559	//登记群组佣金返还                                                          
#define		FUNC_JYGL_XGQZYJFH		15560	//修改群组佣金返还                                                          
#define		FUNC_JYGL_SCQZYJFH		15561	//删除群组佣金返还                                                          
#define		FUNC_JYGL_CXLPCS		15562   //查询礼品参数                                                              
#define		FUNC_JYGL_DJLPCS		15563   //登记礼品参数                                                              
#define		FUNC_JYGL_XGLPCS		15564   //修改礼品参数                                                              
#define		FUNC_JYGL_SCLPCS		15565   //删除礼品参数                                                              
#define		FUNC_JYGL_XGGYXX		15566   //修改柜员信息                                                              
#define		FUNC_JYGL_SCGYXX		15567   //删除柜员                                                                  
#define		FUNC_JYGL_DJGYXX		15568   //登记柜员                                                                  
#define		FUNC_JYGL_CXGYJS		15569   //查询柜员角色                                                              
#define		FUNC_JYGL_DJGYJS		15570   //登记柜员角色                                                              
#define		FUNC_JYGL_SCGYJS		15571   //删除柜员角色                                                              
#define		FUNC_JYGL_CXQZCQED		15572	//查询群组存取额度                                                          
#define		FUNC_JYGL_DJQZCQED		15573	//登记群组存取额度                                                          
#define		FUNC_JYGL_XGQZCQED		15574	//修改群组存取额度                                                          
#define		FUNC_JYGL_SCQZCQED		15575	//删除群组存取额度                                                          
#define		FUNC_JYGL_CXKHCQED		15576	//查询客户存取额度                                                          
#define		FUNC_JYGL_DJKHCQED		15577	//登记客户存取额度                                                          
#define		FUNC_JYGL_XGKHCQED		15578	//修改客户存取额度                                                          
#define		FUNC_JYGL_SCKHCQED		15579	//删除客户存取额度                                                          
#define		FUNC_JYGL_CXJSXX		15580   //查询角色信息                                                              
#define		FUNC_JYGL_CXJGRQ		15581   //查询上次交割日期                                                          
#define		FUNC_JYGL_JLJGRZ		15582   //记录交割日志                                                              
#define		FUNC_JYGL_CXJGRZ	 	15583   //查询交割日志                                                              
#define		FUNC_JYGL_CXZXCS		15584   //查询杂项参数                                                              
#define		FUNC_JYGL_DJZXCS		15585   //登记杂项参数                                                              
#define		FUNC_JYGL_XGZXCS		15586   //修改杂项参数                                                              
#define		FUNC_JYGL_SCZXCS		15587   //删除杂项参数                                                              
#define		FUNC_JYGL_GYFJXXDJ		15588	//登记柜员附加信息                                                          
#define		FUNC_JYGL_GYFJXXSC		15589	//删除柜员附加信息                                                          
#define		FUNC_JYGL_GYFJXXCX		15590	//查询柜员附加信息                                                          
#define		FUNC_JYGL_CXJJRZK		15591	//查询经纪人佣金折扣参数                                                      
#define		FUNC_JYGL_DJJJRZK		15592	//登记经纪人佣金折扣参数                                                      
#define		FUNC_JYGL_XGJJRZK		15593	//修改经纪人佣金折扣参数                                                      
#define		FUNC_JYGL_SCJJRZK		15594	//删除经纪人佣金折扣参数                                                      
#define		FUNC_JYGL_CXDLZT		15595	//查询登录状态                                                                
#define		FUNC_JYGL_SCDLZT		15596	//删除登录状态                                                                
                                                                                                                  
#define		FUNC_JYGL_SHKHYJZK		15601	//审核客户佣金折扣参数                                                      
#define		FUNC_JYGL_SHJJRYJZK		15602	//审核经纪人佣金折扣参数                                                    
#define		FUNC_JYGL_SHQZYJZK		15603	//审核群组佣金折扣参数                                                      
#define		FUNC_JYGL_SHKHYJFH		15604	//审核客户佣金返还参数                                                      
#define		FUNC_JYGL_SHJJRYJFH		15605	//审核经纪人佣金返还参数                                                    
#define		FUNC_JYGL_SHQZYJFH		15606	//审核群组佣金返还参数                                                      
#define		FUNC_JYGL_CXSHXM		15607	//查询需审核的项目                                                            
#define		FUNC_JYGL_CXSHXMQX		15608	//查询审核项目的授权情况                                                    
#define		FUNC_JYGL_SHSQ			15609	//审核项目授权                                                                
#define		FUNC_JYGL_QXSHSQ		15610	//取消审核项目授权                                                            
#define		FUNC_JYGL_CXYWSHKM		15611	//查询业务审核科目                                                          
#define		FUNC_JYGL_CHECKSHXX		15612	//检查业务审核信息                                                          
#define		FUNC_JYGL_CXYWSHYW		15613	//查询业务审核信息                                                          
#define		FUNC_JYGL_CXYWSHMX		15614	//查询业务审核明细                                                          
#define		FUNC_JYGL_FJYWSHSQ		15615	//否决业务审核申请                                                          
#define		FUNC_JYGL_CXYWSHCS		15616	//查询业务审核参数                                                          
#define		FUNC_JYGL_SHQXJC		15617	//检测审核权限                                                                
                                                                                                                  
#define		 FUNC_JYGL_XZYSYJDYCS	15620	//新增预收佣金抵用参数                                                      
#define		 FUNC_JYGL_CXYSYJDYCS	15621	//查询预收佣金抵用参数                                                      
#define		 FUNC_JYGL_SCYSYJDYCS	15622	//删除预收佣金抵用参数                                                      
#define		 FUNC_JYGL_YSYJCR		15623   //预收佣金存入                                                              
#define		 FUNC_JYGL_YSYJQC		15624   //预收佣金取出                                                              
#define		 FUNC_JYGL_CXKDYYJYE  	15625   //查询可抵用佣金余额                                                    
#define		 FUNC_JYGL_CXKDYYJBDMX	15626   //查询可抵用佣金变动明细                                                
#define		 FUNC_JYGL_SHSQXG		15627	//资金审核上限修改                                                            
                                                                                                                  
                                                                                                                  
#define		 FUNC_JYGL_CXGYWSQJS	 15640	 //查询未审核柜员角色                                                     
//Add by Jerry Gu                                                                                                 
#define		 FUNC_JYGL_CXKHLXFHCS	  15641	 //查询客户利息返还参数                                                   
#define		 FUNC_JYGL_XZKHLXFHCS	  15642	 //新增客户利息返还参数                                                   
#define		 FUNC_JYGL_XGKHLXFHCS	  15643	 //修改客户利息返还参数                                                   
#define		 FUNC_JYGL_SCKHLXFHCS	  15644	 //删除客户利息返还参数                                                   
#define		 FUNC_JYGL_SHKHLXFHCS	  15645	 //审核客户利息返还参数                                                   
#define		 FUNC_JYGL_CXJJRLXFHCS	15646  //查询经纪人利息返还参数                                                 
#define		 FUNC_JYGL_XZJJRLXFHCS	15647  //新增经纪人利息返还参数                                                 
#define		 FUNC_JYGL_XGJJRLXFHCS	15648  //修改经纪人利息返还参数                                                 
#define		 FUNC_JYGL_SCJJRLXFHCS	15649  //删除经纪人利息返还参数                                                 
#define		 FUNC_JYGL_SHJJRLXFHCS	15650  //审核经纪人利息返还参数                                                 
                                                                                                                  
#define		 FUNC_JYGL_CXZZZDXZCS	   15651  //查询自助站点限制参数                                                  
#define		 FUNC_JYGL_XZZZZDXZCS	   15652  //新增自助站点限制参数                                                  
#define		 FUNC_JYGL_SCZZZDXZCS	   15653  //删除自助站点限制参数                                                  
#define		 FUNC_JYGL_XGZZZDXZCS	   15654  //修改自助站点限制参数                                                  
                                                                                                                  
#define		 FUNC_JYGL_CXYYBBZQSFLCS	  15662	 //查询营业部标准清算费率参数                                         
#define		 FUNC_JYGL_DJYYBBZQSFLCS	  15663	 //登记营业部标准清算费率参数                                         
#define		 FUNC_JYGL_XGYYBBZQSFLCS	  15664	 //修改营业部标准清算费率参数                                         
#define		 FUNC_JYGL_SCYYBBZQSFLCS	  15665	 //删除营业部标准清算费率参数                                         
#define		 FUNC_JYGL_CXHGQSFLCS	15666	//查询回购清算费率参数                                                      
#define		 FUNC_JYGL_DJHGQSFLCS	15667	//登记回购清算费率参数                                                      
#define		 FUNC_JYGL_XGHGQSFLCS	15668	//修改回购清算费率参数                                                      
#define		 FUNC_JYGL_SCHGQSFLCS	15669	//删除回购清算费率参数                                                      
#define		 FUNC_XT_QXDTGY	  15670	  //复制柜员的权限                                                              
                                                                                                                  
#define		 FUNC_JYGL_CXGYKSQCD	   15671   //查询柜员可授权菜单                                                   
#define		 FUNC_JYGL_CXGYKFHCD	   15672   //查询柜员可复核菜单                                                   
#define		 FUNC_JYGL_SZGYSQCD		  15673	  //新增柜员可授权菜单                                                    
#define		 FUNC_JYGL_SZGYFHCD		  15674	  //新增柜员可复核菜单                                                    
#define		 FUNC_JYGL_DELGYSQCD	   15675   //删除柜员可授权菜单                                                   
#define		 FUNC_JYGL_DELGYFHCD	   15676   //删除柜员可复核菜单                                                   
#define		 FUNC_JYGL_GYSQCDDT		  15677	  //柜员可授权菜单等同                                                    
#define		 FUNC_JYGL_GYFHCDDT		  15678	  //柜员可复核菜单等同                                                    
                                                                                                                  
#define		 FUNC_JYGL_CXMYYWXZ		15680		//查询漫游业务限制                                                        
#define		 FUNC_JYGL_MODIMYYWXZ	15681		//变更漫游业务限制                                                        
#define		 FUNC_JYGL_DELMYYWXZ	15682		//删除漫游业务限制                                                        
                                                                                                                  
#define		 FUNC_JYGL_CXKHJL	15683		//查询客户经理信息                                                            
#define		 FUNC_JYGL_DJKHJL	15684		//登记客户经理信息                                                            
#define		 FUNC_JYGL_MODIKHJL	15685		//修改客户经理信息                                                          
#define		 FUNC_JYGL_DELKHJL	15686		//删除客户经理信息                                                          
#define			FUNC_JYGL_CXZBLLCS		 15687		 //查询总部利率参数                                                   
                                                                                                                  
//add by CYQ 2004.05.25                                                                                           
#define		 FUNC_JYGL_CXSFYZFS			   15690		//查询身份验证方式参数                                              
#define		 FUNC_JYGL_SZSFYZFS			   15691		//设置身份验证方式参数                                              
#define		 FUNC_JYGL_ZXSFYZFS			   15692		//注销身份验证方式参数                                              
//add by zlq 2004.09.03                                                                                           
#define		 FUNC_JYGL_XZQZMMTBFS		15693	 //新增按群组密码同步方式                                                 
#define		 FUNC_JYGL_XGQZMMTBFS		15694	 //修改按群组密码同步方式                                                 
#define		 FUNC_JYGL_SCQZMMTBFS		15695	 //删除按群组密码同步方式                                                 
#define		 FUNC_JYGL_CXQZMMTBFS		15696	 //查询按群组密码同步方式                                                 
#define		 FUNC_JYGL_XZKHMMTBFS		15697	 //新增按客户密码同步方式                                                 
#define		 FUNC_JYGL_XGKHMMTBFS		15698	 //修改按客户密码同步方式                                                 
#define		 FUNC_JYGL_SCKHMMTBFS		15699	 //删除按客户密码同步方式                                                 
#define		 FUNC_JYGL_CXKHMMTBFS		15700	 //查询按客户密码同步方式                                                 
//add by zlq 2005.05.30                                                                                           
#define		 FUNC_JYGL_JJRKHQY			15701	 //经纪人客户迁移                                                         
//add by XiongBin 2005.06.03                                                                                      
#define		 FUNC_JYGL_CXDQDESFCS		15702	//查询定期定额收费参数                                                    
#define		 FUNC_JYGL_XZDQDESFCS		15703	//新增定期定额收费参数                                                    
#define		 FUNC_JYGL_XGDQDESFCS		15704	//修改定期定额收费参数                                                    
#define		 FUNC_JYGL_SCDQDESFCS		15705	//删除定期定额收费参数                                                    
                                                                                                                  
#define		 FUNC_JYGL_DJSXZH			15706	//登记授信帐户                                                              
#define		 FUNC_JYGL_XGSXZH			15707	//修改授信帐户                                                              
#define		 FUNC_JYGL_SCSXZH			15708	//删除授信帐户                                                              
#define		 FUNC_JYGL_CXSXCS			15709	//查询授信参数                                                              
//add by 张利强 2006.02.17                                                                                        
#define		 FUNC_JYGL_DJKHHGED			15710	//登记客户回购额度                                                        
#define		 FUNC_JYGL_XGKHHGED			15711	//修改客户回购额度                                                        
#define		 FUNC_JYGL_SCKHHGED			15712	//删除客户回购额度                                                        
#define		 FUNC_JYGL_CXKHHGED			15713	//查询客户回购额度                                                        
                                                                                                                  
#define		 FUNC_YJDJ_CXZQLBCS		15901	 //查询交易量券种定义                                                       
#define		 FUNC_YJDJ_XZZQLBCS		15902	 //新增交易量券种定义                                                       
#define		 FUNC_YJDJ_SCZQLBCS		15903	 //删除交易量券种定义                                                       
#define		 FUNC_YJDJ_XGZQLBCS		15904	 //修改交易量券种定义                                                       
#define		 FUNC_YJDJ_CXJYLFDCS	15913	  //查询交易量分段参数                                                      
#define		 FUNC_YJDJ_XZJYLFDCS	15914	  //新增交易量分段参数                                                      
#define		 FUNC_YJDJ_SCJYLFDCS	15915	  //删除交易量分段参数                                                      
#define		 FUNC_YJDJ_CXJYLZKCS	15917	  //查询交易量折扣参数                                                      
#define		 FUNC_YJDJ_XZJYLZKCS	15918	  //新增交易量折扣参数                                                      
#define		 FUNC_YJDJ_SCJYLZKCS	15919	  //删除交易量折扣参数                                                      
#define		 FUNC_YJDJ_XGJYLZKCS	15920	  //修改交易量折扣参数                                                      
#define		 FUNC_YJDJ_CXZCFDCS		15921	 //查询资产分段参数                                                         
#define		 FUNC_YJDJ_XZZCFDCS		15922	 //新增资产分段参数                                                         
#define		 FUNC_YJDJ_SCZCFDCS		15923	 //删除资产分段参数                                                         
                                                                                                                  
#define		 FUNC_YJDJ_CXDHJYLCS	15940	//某对象的交易量定价                                                        
#define		 FUNC_YJDJ_CXZCJYLDX	15941	//查询资产交易量对象                                                        
#define		 FUNC_YJDJ_XZZCJYLDX	15942	//新增资产交易量对象                                                        
#define		 FUNC_YJDJ_SCZCJYLDX	15943	//删除资产交易量对象                                                        
#define		 FUNC_YJDJ_XGZCJYLDX	15944	//修改资产交易量对象                                                        
#define		 FUNC_YJDJ_FHZCJYLDX	15945	//复核资产交易量对象                                                        
                                                                                                                  
                                                                                                                  
#define		 FUNC_YJDJ_CXJYLDKFDCS	 15946	 //查询交易量抵扣分段参数                                               
#define		 FUNC_YJDJ_XZJYLDKFDCS	 15947	 //新增交易量抵扣分段参数                                               
#define		 FUNC_YJDJ_SCJYLDKFDCS	 15948	 //删除交易量抵扣分段参数                                               
#define		 FUNC_YJDJ_CXJYLDKCS	  15949	  //查询交易量抵扣比例参数                                                
#define		 FUNC_YJDJ_XZJYLDKCS	  15950	  //新增交易量抵扣比例参数                                                
#define		 FUNC_YJDJ_SCJYLDKCS	  15951	  //删除交易量抵扣比例参数                                                
#define		 FUNC_YJDJ_JYLDKCL		15952	//单一客户交易量抵扣处理                                                    
#define		 FUNC_YJDJ_XGJYLDKCS	  15953	  //修改交易量抵扣比例参数                                                
                                                                                                                  
#define		 FUNC_YJDJ_CXYJDJCL		 15961	 //查询佣金定价策略                                                       
#define		 FUNC_YJDJ_XZYJDJCL		 15962	 //新增佣金定价策略                                                       
#define		 FUNC_YJDJ_XGYJDJCL		 15963	 //修改佣金定价策略                                                       
#define		 FUNC_YJDJ_SCYJDJCL		 15964	 //删除佣金定价策略                                                       
#define		 FUNC_YJDJ_YJDJJYLQL	  15965	  //佣金定价交易量清零                                                    
#define		 FUNC_YJDJ_CXYJDJJYL	  15966	  //查询佣金定价交易量                                                    
//#define		 FUNC_YJDJ_JYLCSH		  15967	  //佣金定价交易量初始化                                                  
                                                                                                                  
//系统类                                                                                                          
#define		 FUNC_XT_GYLOGIN	  16001	  //柜员登录                                                                  
#define		 FUNC_XT_GYLOGOUT	   16002   //柜员退出                                                                 
#define		 FUNC_XT_GYXGMM	 16003	 //柜员改密                                                                     
#define		 FUNC_XT_GYENTERMENU   16004   //柜员进入菜单                                                           
#define		 FUNC_XT_GYEXITMENU	  16005	  //柜员退出菜单                                                            
#define		 FUNC_XT_GYCZMM	 16006	 //重置柜员密码                                                                 
#define		 FUNC_XT_GYMMXY	 16007	 //柜员密码效验                                                                 
#define		 FUNC_TRAN_GETDATA	 16008	 //从营业部上传数据到总部                                                   
#define		 FUNC_TRAN_SENDDATA	 16009	 //删除或插入数据到营业部                                                   
#define		 FUNC_TRAN_GETSUM	 16010	 //获得营业部对帐数据                                                         
#define      FUNC_TRAN_GETBCP  16011   //获得营业部BCP数据到总部                                                  
#define      FUNC_TRAN_INSBCP  16012   //总部BCP出的数据插入营业部                                                
#define      FUNC_TRAN_FILE  16013   //下载营业部文件                                                             
                                                                                                                  
#define		 FUNC_TRAN_GETDATA_ZB	 16018	 //从总部上传数据到营业部                                                 
#define		 FUNC_TRAN_SENDDATA_ZB	 16019	 //删除或插入数据到总部                                                 
#define		 FUNC_TRAN_GETSUM_ZB	 16020	 //获得总部对帐数据                                                       
#define      FUNC_TRAN_GETBCP_ZB  16021   //获得总部BCP数据到营业部                                               
#define      FUNC_TRAN_INSBCP_ZB  16022   //营业部BCP出的数据插入总部                                             
#define      FUNC_TRAN_FILE_ZB  16023   //下载总部文件                                                            
                                                                                                                  
//期货交易系统                                                                                                    
#define		FUNC_QH_JYBMDJ			16101	//交易编码登记                                                                
#define		FUNC_QH_JYBMZX			16102	//交易编码注销                                                                
#define		FUNC_QH_JYBMSXKZ		16103	//交易编码属性控制                                                            
#define		FUNC_QH_JYBMXG			16104	//交易编码修改                                                                
#define		FUNC_QH_CXKHJYBM		16106	//查询客户交易编码                                                            
#define		FUNC_QH_CXQTJYBM		16107	//查询全体交易编码                                                            
#define		FUNC_QH_DHZJXX			16108	//单户期货资金信息                                                            
#define		FUNC_QH_QTZJXX			16109	//全体期货资金信息                                                            
#define		FUNC_QH_QTZJFX			16110	//全体期货资金风险                                                            
#define		FUNC_QH_JYFYJS			16111	//期货交易费用计算                                                            
#define		FUNC_QH_KWTSL			16112	//期货可委托数量                                                                
                                                                                                                  
#define		FUNC_QH_HQCX			16128	//期货行情查询                                                                  
                                                                                                                  
#define		FUNC_QH_WTSB			16148	//期货委托申报                                                                  
#define		FUNC_QH_CJBD			16149	//期货成交补单                                                                  
#define		FUNC_QH_DRWT			16150	//期货交易委托                                                                  
#define		FUNC_QH_DRKHWTCX		16151	//期货客户当日委托查询                                                        
#define		FUNC_QH_DRQTWTCX		16152	//期货全体当日委托查询                                                        
#define		FUNC_QH_WTCD			16153	//期货基本委托撤单                                                              
#define		FUNC_QH_DHCCCX			16154	//期货客户持仓查询                                                            
#define		FUNC_QH_QTCCCX			16155	//期货全体持仓查询                                                            
#define		FUNC_QH_YYWTQR			16156	//期货预约单确认                                                              
#define		FUNC_QH_KHYYWTCX		16157	//期货客户预约委托查询                                                        
#define		FUNC_QH_QTYYWTCX		16158	//期货全体预约委托查询                                                        
#define		FUNC_QH_DRKHCJCX		16159	//期货客户当日成交查询                                                        
#define		FUNC_QH_DRQTCJCX		16160	//期货全体当日成交查询                                                        
#define		FUNC_QH_KHFBCJCX		16161	//期货客户当日分笔成交                                                        
#define		FUNC_QH_DRQTCJHZ		16162	//期货当日全体成交汇总                                                        
#define		FUNC_QH_QTCCHZCX		16163	//期货全体持仓汇总                                                            
#define		FUNC_QH_DRCJPH			16164	//期货当日成交排行                                                            
#define		FUNC_QH_DBWTCX			16165	//期货单笔委托查询                                                            
#define		FUNC_QH_CJBDCX			16166	//期货成交补单查询                                                            
                                                                                                                  
#define		FUNC_QH_LSKHWTCX		16171	//期货单户历史委托                                                            
#define		FUNC_QH_LSQTWTCX		16172	//期货全体历史委托                                                            
#define		FUNC_QH_LSKHCJCX		16173	//期货单户历史成交                                                            
#define		FUNC_QH_LSQTCJCX		16174	//期货全体历史成交                                                            
#define		FUNC_QH_LSKHPCYK		16175	//期货单户平仓盈亏                                                            
#define		FUNC_QH_LSQTPCYK		16176	//期货全体平仓盈亏                                                            
#define		FUNC_QH_LSKHBZJDJ		16177	//期货单户保证金变动                                                          
#define		FUNC_QH_LSQTBZJDJ		16178	//期货全体保证金变动                                                          
#define		FUNC_QH_LSKHCCCX		16179	//期货单户历史持仓                                                            
#define		FUNC_QH_LSQTCCCX		16180	//期货全体历史持仓                                                            
#define		FUNC_QH_LSKHZCCX		16181	//期货单户历史资产                                                            
#define		FUNC_QH_LSCJHZ			16182	//期货历史成交汇总                                                            
#define		FUNC_QH_LSCCHZ			16183	//期货历史持仓汇总                                                            
#define		FUNC_QH_LSCJPH			16184	//期货历史成交排行                                                            
#define		FUNC_QH_LSQTZCCX		16185	//期货全体历史资产                                                            
#define		FUNC_QH_PCYKPH			16186	//期货平仓盈亏排行                                                            
                                                                                                                  
#define		FUNC_QH_CXXTDM			16200	//查询合约数据字典                                                            
#define		FUNC_QH_CXJYS			16201	//查询合约交易所                                                                
#define		FUNC_QH_CXJYXW			16202	//查询合约交易席位                                                            
#define		FUNC_QH_CXHYDM			16203	//查询合约代码                                                                
#define		FUNC_QH_CXHYPZ			16204	//查询合约品种                                                                
#define		FUNC_QH_CXDHPZFL		16205	//查询单户品种费率                                                            
#define		FUNC_QH_XZDHPZFL		16206	//新增单户品种费率                                                            
#define		FUNC_QH_XGDHPZFL		16207	//修改单户品种费率                                                            
#define		FUNC_QH_SCDHPZFL		16208	//删除单户品种费率                                                            
#define		FUNC_QH_CXDHPZBZJ		16209	//查询单户品种保证金                                                          
#define		FUNC_QH_XZDHPZBZJ		16210	//新增单户品种保证金                                                          
#define		FUNC_QH_XGDHPZBZJ		16211	//修改单户品种保证金                                                          
#define		FUNC_QH_SCDHPZBZJ		16212	//删除单户品种保证金                                                          
                                                                                                                  
                                                                                                                  
//客户间融资管理类                                                                                                
#define		 FUNC_RZGL_CXKHXZJYPZ	16501	//查询客户限制交易品种                                                      
#define		 FUNC_RZGL_CXKHXYJB		16502	//查询客户信用级别设置信息                                                  
#define		 FUNC_RZGL_XZKHXYJB		16503	//新增客户信用级别设置                                                      
#define		 FUNC_RZGL_XGKHXYJB		16504	//修改客户信用级别设置                                                      
#define		 FUNC_RZGL_DELKHXYJB	16505	//删除客户信用级别设置                                                      
#define		 FUNC_RZGL_DELXZJYPZ	16506	//删除客户限制交易品种                                                      
#define		 FUNC_RZGL_XZXZJYPZ		16507	//新增客户限制交易品种                                                      
#define		 FUNC_RZGL_XGXZJYPZ		16508	//修改客户限制交易品种                                                      
#define		 FUNC_RZGL_CXRZJKCS		16509	//查询融资监控参数                                                          
#define		 FUNC_RZGL_XZRZJKCS		16510	//新增融资监控参数                                                          
#define		 FUNC_RZGL_XGRZJKCS		16511	//修改融资监控参数                                                          
#define		 FUNC_RZGL_DELRZJKCS	16512	//删除融资监控参数                                                          
#define		 FUNC_RZGL_CXZQLTXX		16513	//查询融资证券流通信息                                                      
#define		 FUNC_RZGL_XZZQLTXX		16514	//新增融资证券流通信息                                                      
#define		 FUNC_RZGL_XGZQLTXX		16515	//修改融资证券流通信息                                                      
#define		 FUNC_RZGL_DELZQLTXX	16516	//删除融资证券流通信息                                                      
#define		 FUNC_RZGL_GETMAXVALUE	16517	//取得某个表中某个字段的当前最大值                                        
#define		 FUNC_RZGL_XZZJDZXX		16518	//新增融资资金对照信息                                                      
#define		 FUNC_RZGL_DJRZHY		16519	//登记融资合约                                                                
#define		 FUNC_RZGL_DJRZZHY		16520	//登记融资子合约                                                            
#define		 FUNC_RZGL_RZHYCX		16521	//融资合约查询                                                                
#define		 FUNC_RZGL_RZZHYCX		16522	//融资子合约查询                                                            
#define		 FUNC_RZGL_RZZJZHCX		16523	//融资资金账户                                                              
#define		 FUNC_RZGL_RZHYSH		16524	//融资合约登记审核                                                            
#define		 FUNC_RZGL_RZZJZHSH		16525	//融资资金账户登记审核                                                      
#define		 FUNC_RZGL_RZHYJQ		16526	//融资合约结清                                                                
#define		 FUNC_RZGL_RZZJZHJQ		16527	//融资资金账户结清                                                          
#define		 FUNC_RZGL_XGRZHY		16528	//修改融资合约                                                                
#define		 FUNC_RZGL_DELRZHY		16529	//删除融资合约                                                              
#define		 FUNC_RZGL_XGRZZHY		16530	//修改融资子合约                                                            
#define		 FUNC_RZGL_DELRZZHY		16531	//删除融资子合约                                                            
#define		 FUNC_RZGL_DELRZZJZH	16532	//删除融资资金账户                                                          
#define		 FUNC_RZGL_RZHYCXSH		16533	//融资合约撤销登记审核                                                      
#define		 FUNC_RZGL_RZZJZHCXSH	16534	//融资资金账户撤销登记审核                                                  
#define		 FUNC_RZGL_RZKHCCCX		16535	//融资客户持仓查询                                                          
#define		 FUNC_RZGL_RZDQCX		16536	//融资合约到期查询                                                            
#define		 FUNC_RZGL_RZWDQHYCX	 16537	 //融未到期合约查询                                                       
#define		 FUNC_RZGL_CYSXPZJK		 16538	 //持有受限品种监控                                                       
#define		 FUNC_RZGL_SXPZJYJK		 16539	 //受限品种交易监控                                                       
#define		 FUNC_RZGL_SXPZCEJK		 16540	 //受限品种超额监控                                                       
#define		 FUNC_RZGL_KHYGBLCEJK	 16541	 //客户拥股比例超额监控                                                   
#define		 FUNC_RZGL_KHRZBLCEJK	 16542	 //客户融资比例超额监控                                                   
                                                                                                                  
//营业分析类                                                                                                      
#define		 FUNC_YYFX_KHKXHTJ	 17001	 //营业部客户开销户状况                                                     
#define		 FUNC_YYFX_ZJZHKXH	 17002	 //营业部资金帐户开销户                                                     
#define		 FUNC_YYFX_GDBDQK	   17003   //营业部股东变化情况                                                       
#define		 FUNC_YYFX_YHZHQK	   17004   //银行帐户情况查询                                                         
#define		 FUNC_YYFX_KHZCFX	   17005   //营业部客户资产指标分析                                                   
#define		 FUNC_YYFX_ZCFDCX	   17006   //营业部资产分段查询                                                       
#define		 FUNC_YYFX_TZPZFX	   17007   //客户投资品种分析                                                         
#define		 FUNC_YYFX_TZYKFX	   17008   //客户投资盈亏排行分析                                                     
#define		 FUNC_YYFX_LCJSCX		17009	//查询日期范围内的利息积数的差额   add by xiongbin 2005.05.23                 
#define		 FUNC_YYFX_JJRKHLCJSCX	17010	//查询日期范围内经纪人的所有客户的利息积数差额 add by xiongbin 2005.05.25 
#define		 FUNC_YYFX_KHLCJSCX		17011	//查询日期范围内客户的利息积数差额 add by xiongbin 2005.05.25               
#define		 FUNC_YYFX_DQJYLFX	 17012	 //营业部定期交易量分析                                                     
#define		 FUNC_YYFX_YYBZHYWFX	  17015	  //营业部综合业务查询                                                    
#define		 FUNC_YYFX_YYBDQCJFLHZ	 17016	 //	营业部定期交易量分类汇总                                            
#define		 FUNC_YYFX_YYBDQCJFDCX	 17017	 //	营业部定期交易量分段查询                                            
#define		 FUNC_YYFX_YYBDRZCCX   17018   // 营业部定日资产查询                                                    
#define		 FUNC_YYFX_FXJBKHHZCX	17019	//风险级别客户汇总查询  add by xiongbin 2005.06.21                          
#define		 FUNC_YYFX_YYBZQCCHZ   17020   // 营业部定日股票持仓汇总排行                                            
#define		 FUNC_YYFX_YYBZQCCMX   17021   //营业部定日某股票持仓明细                                               
#define		 FUNC_YYFX_YYBDRTZCX   17051   // 营业部当日透支查询                                                    
#define		 FUNC_YYFX_YYBDECJCX   17052   // 营业部大额成交查询                                                    
#define		 FUNC_YYFX_YYBDEZJBD   17053   // 营业部大额资金变动查询                                                
#define		 FUNC_YYFX_YYBDEZJBD_HZ	  17054	  // 营业部当日大额资金变动查询（汇总）                                 
#define		 FUNC_YYFX_XMKHCX		17055	//休眠客户查询 add by xiongbin 2005.05.30                                     
                                                                                                                  
#define		 FUNC_YYFX_ZJHLSZC		17301	//按资金帐号查询客户历史资产                                                
#define		 FUNC_YYFX_KHHLSZC		17302	//按客户号查询客户历史资产                                                  
#define		 FUNC_YYFX_ZJHLSGP		17303	//按资金帐号查询历史证券余额                                                
#define		 FUNC_YYFX_KHHLSGP	 17304	 //按客户号查询客户历史证券余额                                             
#define		 FUNC_YYFX_ZJZHLSYE	  17305	  //按客户号查询历史余额                                                    
#define		 FUNC_YYFX_KHZCFLFX	  17306	  //按客户分类查询客户资产组成                                              
#define		 FUNC_YYFX_DQJYLSCFEFX	 17307	 //定期营业部交易量占市场份额的变化情况                                 
#define		 FUNC_YYFX_DQJYLZC_KH	  17308	 //按客户类别查交易组成                                                   
#define		 FUNC_YYFX_DQJYLZC_WTFS	  17309	  //按委托方式查交易组成                                                
#define		 FUNC_YYFX_DQJYLZC_ZQLB	  17310	  //按交易品种查交易组成                                                
#define		 FUNC_YYFX_KHJYLPH	 17311	 //查询客户交易量排行                                                       
#define		 FUNC_YYFX_KHZCPH	  17312		 //查询客户资产排行                                                         
#define		 FUNC_YYFX_ZQCCPH	  17313		 //查询持仓证券排行                                                         
#define		 FUNC_YYFX_KHZQCCPH	  17314	  //查询某证券持仓排行                                                      
#define		 FUNC_YYFX_DQWTFSKHS   17315   //查询营业部当前委托方式开户数                                           
#define		 FUNC_YYFX_DQFWXMKHS   17316   //查询营业部当前服务项目开户数                                           
#define		 FUNC_YYFX_DQGDZHKHS   17317   //查询营业部当前股东帐户开户数                                           
#define		 FUNC_YYFX_DQKHKHS	 17318	 //查询营业部当前客户开户数                                                 
#define		 FUNC_YYFX_DQZJZHKHS   17319   //查询营业部当前资金帐户数                                               
#define		 FUNC_YYFX_DQYZZZKHS   17320   //查询营业部当前各银行转帐开通户数                                       
#define		 FUNC_YYFX_DQBZJHZ	 17321	 //查询营业部当前分类保证金汇总                                             
#define		 FUNC_YYFX_DQKHCCHZ	  17322	  //查询营业部当前客户持仓汇总                                              
#define		 FUNC_YYFX_TBJKKHZC	  17323	  //查询特别资产监控客户的资产                                              
#define		 FUNC_YYFX_CXKHZCJKCS	17324	//查询客户资产监控参数                                                      
#define		 FUNC_YYFX_XZKHZCJKCS	17325	//新增客户资产监控参数                                                      
#define		 FUNC_YYFX_SCKHZCJKCS	17326	//删除客户资产监控参数                                                      
#define		 FUNC_YYFX_XGKHZCJKCS	17327	//修改客户资产监控参数                                                      
#define		 FUNC_YYFX_DRKHQKTJ	  17328	  //定日开户情况统计                                                        
#define		 FUNC_YYFX_DRGDQKTJ	  17329	  //定日股东情况统计                                                        
#define		 FUNC_YYFX_DRJYLPH	 17330	 //查询当日交易量排行                                                       
#define		 FUNC_YYFX_CXYYBTJXM   17331   //查询被支持的营业部情况统计项目                                         
#define		 FUNC_YYFX_CXYYBTJXMXX 17332   //查询营业部情况统计项目信息                                             
#define		 FUNC_YYFX_KHJLKHTJ	   17333	//客户经理开户统计                                                        
#define		 FUNC_YYFX_KHJLYJRTJ   17334	//客户经理业绩日统计                                                      
#define		FUNC_YYFX_HZDZ	17335			//资金股份汇总对帐                                                            
//债券系统                                                                                                        
#define		 FUNC_GZYW_TGZHDJ	18001	//托管账户开户                                                                  
#define		 FUNC_GZYW_TGZHXH	18002	//托管账户销户                                                                  
#define		 FUNC_GZXW_TGZHZTXG	  18003	  //托管账户状态修改                                                        
#define		 FUNC_GZYW_TGZHXXXG	  18004	  //托管账户信息修改                                                        
#define		 FUNC_GZYW_ZHMMGSSQ	  18005	  //登记中心密码挂失申请                                                    
#define		 FUNC_GZYW_ZQFXYW	18006	//债券发行认购                                                                  
#define		 FUNC_GZYW_JZZQMR	18007	//记账式债券买入                                                                
#define		 FUNC_GZYW_JZZQMC	18008	//记账式债券卖出                                                                
#define		 FUNC_GZYW_JZZQHG	18009	//记账式债券回购                                                                
#define		 FUNC_GZYW_JZZQGH	18010	//记账式债券购回业务                                                            
#define		 FUNC_GZYW_DBGGSDJ	 18011	 //代管单挂失冻结管理                                                       
#define		 FUNC_GZYW_ZQZTGSS	18012	//债券转托管理上市处理                                                        
                                                                                                                  
                                                                                                                  
#define		 FUNC_GZYW_DBGDF	  18014	  //代保管单兑付                                                              
#define		 FUNC_GZYW_JZGZDJ	18015	//记账式债券冻结                                                                
#define		 FUNC_GZYW_ZQHCLB	18017	//记账式债券红冲兰补                                                            
#define		 FUNC_GZYW_ZQZTG	  18018	  //记账式债券转托管                                                          
#define		 FUNC_GZYW_ZQFJYGH	 18019	 //记账式债券非交易过户                                                     
#define		 FUNC_GZYW_ZQZY	 18020	 //记账式债券质押                                                               
#define		 FUNC_GZYW_ZQJZY	  18021	  //记账式债券解质押                                                          
#define		 FUNC_GZYW_ZQZYGH	18022	//债券质押清偿非交易过户                                                        
		  /*综合查询*/                                                                                                
#define		 FUNC_GZYW_DHXXCX	18022	//债券系统单户信息查询                                                          
#define		 FUNC_GZYW_ZQYWCX	18023	//债券业务流水查询                                                              
#define		 FUNC_GZYW_LSZQYWCX	  18024	  //历史债券业务流水查询                                                    
#define		 FUNC_GZYW_XTDM	 18030	 //债券系统系统数据字典                                                         
#define		 FUNC_GZYW_CXTGXX	18031	//查询帐户记账式债券信息                                                        
#define		 FUNC_GZYW_ZQDMXX	18032	//债券代码信息                                                                  
#define		 FUNC_GZYW_TGZHXX	18033	//托管帐户信息                                                                  
#define		 FUNC_GZYW_CXDBGZQ	 18034	 //查询代保管债券信息                                                       
#define		 FUNC_GZYW_CXHGXX	18035	//查询债券回购代码信息                                                          
#define		 FUNC_GZYW_CXHGGHXX	  18036	  //查询客户回购购回信息                                                    
#define		 FUNC_GZYW_CXZQLL	18037	//查询债券利率                                                                  
#define		 FUNC_GZYW_XJJZZQMR	  18038	  //询价记帐式债券买入                                                      
#define		 FUNC_GZYW_XJJZZQMC	  18039	  //询价记帐式债券卖出                                                      
                                                                                                                  
//--------------------------------------------基金功能码定义----------------------------------------------        
//基金帐户管理类  (19001-19300)                                                                                   
#define		 FUNC_JJ_KH_JJZHKH		19001	//基金帐户开户                                                              
#define		 FUNC_JJ_KH_JJZHXH		19002	//基金帐户销户                                                              
#define		 FUNC_JJ_KH_DJJJZH		19003	//基金帐户登记                                                              
#define		 FUNC_JJ_KH_JJZHQXDJ	  19004	  //基金帐户取消登记                                                      
#define		 FUNC_JJ_KH_JJZHXG		19005	//基金帐户信息修改                                                          
#define		 FUNC_JJ_KH_ZHZTXG		19006	//基金帐户状态修改                                                          
#define		 FUNC_JJ_KH_JSZHDJ		19008	//结算帐号登记                                                              
#define		 FUNC_JJ_KH_JSZHZX		19009	//结算帐号注销                                                              
#define		 FUNC_JJ_KH_NBZTG		19010	//内部转托管                                                                  
#define		 FUNC_JJ_KH_JJZHKH_YZT	19011	//银证通基金帐户开户                                                      
#define		 FUNC_JJ_KH_JSZHXG		19012	//修改结算帐户                                                              
#define		 FUNC_JJ_KH_ZHWTCD		19013	//基金帐户委托撤单                                                          
#define		 FUNC_JJ_KH_JJZHNBGH		19014	//基金帐户内部过户                                                        
                                                                                                                  
//基金份额管理类                                                                                                  
#define		 FUNC_JJ_FE_FECQ		 19021	 //基金份额存取                                                             
#define		 FUNC_JJ_FE_JJDJJD		19022	//基金份额冻结解冻                                                          
//业务审核类                                                                                                      
#define		 FUNC_JJ_SH_JJYWSH		19025	//基金业务审核                                                              
#define		 FUNC_JJ_SH_JJHLFF		19026	//基金红利发放                                                              
                                                                                                                  
//投资人交易委托类                                                                                                
#define		 FUNC_JJ_JY_MMWT		 19031	 //基金买卖委托                                                             
#define		 FUNC_JJ_JY_WTCD		 19032	 //基金委托撤单                                                             
#define		 FUNC_JJ_JY_YYMMWT		19033	//基金预约买卖委托                                                          
//基金特殊交易类                                                                                                  
#define		 FUNC_JJ_TSJY_ZTG		  19041	  //基金转托管                                                              
#define		 FUNC_JJ_TSJY_FJYGH		 19042	 //基金非交易过户                                                         
#define		 FUNC_JJ_TSJY_JJZH		19043	//基金转换                                                                  
#define		 FUNC_JJ_TSJY_DSDESG	  19044	  //定时定额申购触发                                                      
#define		 FUNC_JJ_TSJY_SZFHFS	  19045	  //设置分红方式                                                          
#define		 FUNC_JJ_TSJY_DSDESGSZ	 19046	 //定时定额申购设置                                                     
#define		 FUNC_JJ_TSJY_DSDESGQX	 19047	 //定时定额申购取消                                                     
//交易对帐单打印类                                                                                                
#define		FUNC_JJ_DY_CJDZD		 19051	 //基金成交对帐单                                                           
#define		FUNC_JJ_DY_ZJDZD		 19052	 //基金交易资金对帐单                                                       
//单户查询                                                                                                        
#define		FUNC_JJ_CX_JJZH		19062	//单户基金帐户查询                                                              
#define		FUNC_JJ_CX_JJZHFE		  19063	  //单户基金份额查询                                                        
#define		FUNC_JJ_CX_JJWT		19065	//客户当日委托查询                                                              
#define		FUNC_JJ_CX_JJZHWT		  19066	  //客户当日账户委托查询                                                    
#define		FUNC_JJ_CX_JJFEBD		  19067	  //客户当日基金流水查询                                                    
#define		FUNC_JJ_CX_FEDJJD		  19068	  //客户当日基金冻结解冻查询                                                
#define		FUNC_JJ_CX_JJWJS		 19069	 //客户基金未交收查询                                                       
#define		FUNC_JJ_CX_JSZH		19070	//客户结算帐户查询                                                              
#define		FUNC_JJ_CX_KHH			19071	//客户号查询                                                                  
                                                                                                                  
//参数类                                                                                                          
#define		FUNC_JJ_CS_CXYWDM		  19081	  //业务代码                                                                
#define		FUNC_JJ_CS_CXJSJG		  19082	  //基金外部结算机构                                                        
#define		FUNC_JJ_CS_CXTAXX		  19083	  //基金公司信息                                                            
#define		FUNC_JJ_CS_CXJJXX		  19084	  //基金信息                                                                
#define		FUNC_JJ_CS_CXXSDZ		  19085	  //基金销售对照                                                            
#define		FUNC_JJ_CS_CXDQDESGPZ	   19086   //查询定期定额申购品种                                                 
#define		FUNC_JJ_CS_MGRZKL	   19087   //营业部折扣率参数管理                                                     
#define		FUNC_JJ_CS_CXJJSFBZ		19088	//查询基金收费标准                                                          
                                                                                                                  
#define		FUNC_HXKH		 19100 //核新开户                                                                           
                                                                                                                  
//综合查询                                                                                                        
#define		FUNC_JJ_QTCX_JJZH		  19162	  //全体基金帐户查询                                                        
#define		FUNC_JJ_QTCX_JJZHFE		 19163	 //全体基金份额查询                                                       
#define		FUNC_JJ_QTCX_JJWT		  19165	  //全体当日委托查询                                                        
#define		FUNC_JJ_QTCX_JJZHWT		 19166	 //全体当日账户委托查询                                                   
#define		FUNC_JJ_QTCX_JJFEBD		 19167	 //全体当日基金流水查询                                                   
#define		FUNC_JJ_QTCX_FEDJJD		 19168	 //全体当日基金冻结解冻查询                                               
#define		FUNC_JJ_QTCX_JJWJS		19169	//全体基金未交收查询                                                        
#define		FUNC_JJ_QTCX_DSDESG		 19171	 //全体定时定额申购查询                                                   
#define		FUNC_JJ_QTCX_WJSHL		 19172	 //未交收红利查询                                                         
#define		FUNC_JJ_QTCX_WKTJJZH	 19173	//未开通基金账户客户查询                                                  
                                                                                                                  
//历史资料                                                                                                        
#define		FUNC_JJ_QTCX_JJCJ		  19264	  //全体基金成交查询                                                        
#define		FUNC_JJ_QTCX_LSWT		  19265	  //全体历史委托查询                                                        
#define		FUNC_JJ_QTCX_LSZHWT		 19266	 //全体历史账户委托查询                                                   
#define		FUNC_JJ_QTCX_LSFEBD		 19267	 //全体历史基金流水查询                                                   
#define		FUNC_JJ_QTCX_LSDJJD		 19268	 //全体历史基金冻结解冻查询                                               
                                                                                                                  
#define		FUNC_JJ_CX_WTQRLS		  19269	  //基金委托确认流水查询                                                    
#define		FUNC_JJ_CX_ZHQRLS		  19270	  //帐户委托确认流水查询                                                    
#define		FUNC_JJ_CX_JJFHMX		  19271	  //基金分红明细查询                                                        
#define		FUNC_JJ_CX_WBZJQS		  19272	  //外部资金清算查询                                                        
                                                                                                                  
#define		FUNC_JJ_TJ_ZHWTQR		  19273	  //帐户委托确认统计                                                        
#define		FUNC_JJ_TJ_JJWTQR		  19274	  //基金委托确认统计                                                        
#define		FUNC_JJ_TJ_JJJSTJ		  19275	  //基金交收统计                                                            
#define		FUNC_JJ_TJ_JJCJTJ		  19276	  //基金成交统计  add by xiongbin 2005.05.24                                
                                                                                                                  
#define		FUNC_JJ_CX_KHBYFECX		  19277	  //客户保有份额查询                                                      
                                                                                                                  
//安全认证部分功能代码区间[19301 ～	19400]                                                                        
#define	FUNC_SAFE_SAVEIMAGE	  19301	 //用于保存前端送入的图像信息到tIDINFO                                        
#define	  FUNC_SAFE_UPDATEIMAGE	  19302		 //修改客户的图像信息                                                   
#define	  FUNC_SAFE_GETIMAGE   19303	  //查询客户的图像信息                                                      
#define	  FUNC_SAFE_CXALLIMAGE	 19304		//全体保存有图像的客户或者代理人信息的查询                              
#define	FUNC_SAFE_UPDATEZJBH   19305	  //更新客户或者代理人信息中的证件编号                                      
#define	FUNC_SAFE_DELETEIMAGE	19306	   //删除客户的图像信息                                                       
                                                                                                                  
                                                                                                                  
//add by CYQ 2004.05.25                                                                                           
#define	FUNC_SAFE_REGFINGERIMAGE		19320			//登记验证对象的指纹影像                                            
#define	FUNC_SAFE_REGFINGERTEMPLATE		19321			//登记验证对象的指纹模板                                          
#define	FUNC_SAFE_CLSFINGERINFO			19322			//注销验证对象的指纹信息(影像、模板)                                
#define	FUNC_SAFE_GETFINGERIMAGE		19323			//下载验证对象的指纹影像                                            
#define	FUNC_SAFE_GETFINGERTEMPLATE		19324			//下载验证对象的指纹模板                                          
#define	FUNC_SAFE_VERONTTOMORE			19325			//1:N指纹验证                                                       
                                                                                                                  
//add by LiuJianBao 2005-05-26  实时股东开户证件及开户申请资料管理                                                
#define    FUNC_SAFE_KH_SAVEIMAGE		19351   //用于保存前端送入的股东开户图像信息到tKH_GDINFO                      
#define    FUNC_SAFE_KH_UPDATEIMAGE		19352   //修改股东开户的图像信息                                            
#define    FUNC_SAFE_KH_GETIMAGE		19353   //查询股东开户图像信息                                                
#define    FUNC_SAFE_KH_DELETEIMAGE		19354   //删除客户的图像信息                                                
                                                                                                                  
                                                                                                                  
//消息主动推送服务MPSS系统使用的功能码区间[19401 ～	19600]                                                        
#define	FUNC_MPSS_CXXTDM		19401		//查询MPSS系统数据字典                                                        
#define	FUNC_MPSS_CXFWML		19402		//查询MPSS服务目录                                                            
#define	FUNC_MPSS_GXDZFWDJ		19403		//客户个性定制消息推送服务登记                                              
#define	FUNC_MPSS_GXDZFWXG		19404		//客户个性定制消息推送服务修改                                              
#define	FUNC_MPSS_GXDZFWSC		19405		//客户个性定制消息推送服务删除                                              
#define	FUNC_MPSS_GXDZFWCX		19406		//客户个性定制消息推送服务查询                                              
#define	FUNC_MPSS_SSJKFWDJ		19407		//客户实时监控消息推送服务登记                                              
#define	FUNC_MPSS_SSJKFWXG		19408		//客户实时监控消息推送服务修改                                              
#define	FUNC_MPSS_SSJKFWSC		19409		//客户实时监控消息推送服务删除                                              
#define	FUNC_MPSS_SSJKFWCX		19410		//客户实时监控消息推送服务查询                                              
#define	FUNC_MPSS_HQJKFWDJ		19411		//客户行情监控消息推送服务登记                                              
#define	FUNC_MPSS_HQJKFWXG		19412		//客户行情监控消息推送服务修改                                              
#define	FUNC_MPSS_HQJKFWSC		19413		//客户行情监控消息推送服务删除                                              
#define	FUNC_MPSS_HQJKFWCX		19414		//客户行情监控消息推送服务查询                                              
#define	FUNC_MPSS_HQDBFWDJ		19415		//客户行情点播消息推送服务登记                                              
#define	FUNC_MPSS_HQDBFWXG		19416		//客户行情点播消息推送服务修改                                              
#define	FUNC_MPSS_HQDBFWSC		19417		//客户行情点播消息推送服务删除                                              
#define	FUNC_MPSS_HQDBFWCX		19418		//客户行情点播消息推送服务查询                                              
#define	FUNC_MPSS_PTGGFWDJ		19419		//客户普通公告信息推送服务登记                                              
#define	FUNC_MPSS_PTGGFWXG		19420		//客户普通公告信息推送服务修改                                              
#define	FUNC_MPSS_PTGGFWSC		19421		//客户普通公告信息推送服务删除                                              
#define	FUNC_MPSS_PTGGFWCX		19422		//客户普通公告信息推送服务查询                                              
#define	FUNC_MPSS_QTGXDZFWCX	19423		//全体开通个性定制推送服务查询                                              
#define	FUNC_MPSS_QTSSJKFWCX	19424		//全体开通实时监控推送服务查询                                              
#define	FUNC_MPSS_QTHQJKFWCX	19425		//全体开通行情监控推送服务查询                                              
#define	FUNC_MPSS_QTHQDBFWCX	19426		//全体开通行情点播推送服务查询                                              
#define	FUNC_MPSS_QTPTGGFWCX	19427		//全体开通普通公告推送服务查询                                              
#define	FUNC_MPSS_DELALLHQJK	19428		//取消客户已定制的所有行情监控服务                                          
#define	FUNC_MPSS_DELALLHQDB	19429		//取消客户已定制的所有行情点播服务                                          
#define	FUNC_MPSS_ZXUSERLOGIN	19430		//坐席用户登录                                                              
#define	FUNC_MPSS_MODIUSERPWD	19431		//坐席用户密码修改                                                          
#define	FUNC_MPSS_ADDUSER		19432		//新增坐席用户                                                                
#define	FUNC_MPSS_ADDZQDM		19433		//添加通知使用的证券代码信息                                                  
#define	FUNC_MPSS_MODIZQDM		19434		//修改通知使用的证券代码信息                                                
#define	FUNC_MPSS_DELZQDM		19435		//删除通知使用的证券代码信息                                                  
#define	FUNC_MPSS_CXZQDM		19436		//查询通知使用的证券代码信息                                                  
#define	FUNC_MPSS_ADDFSXX		19437		//添加待发送信息                                                              
#define	FUNC_MPSS_CXFWDZQK		19438		//查询服务定制情况                                                          
                                                                                                                  
//---------------------------------- 其它－－－－－－－－－－－－－－－－－－                                     
                                                                                                                  
#define		 FUNC_RPT_XMLFORMAT	  1801	 //查询报表样板                                                             
#define		 FUNC_RPT_TEST	1802   //测试报表                                                                       
#define		 FUNC_XT_SQL	 1803	//语句执行                                                                          
                                                                                                                  
                                                                                                                  
//重新规范使用实时开户系统的功能码定义                                                                            
#define		FUNC_SSKH_CXCZLX		71100	//查询实时开户系统操作类型定义                                                
#define		FUNC_SSKH_KHWT			71101	//实时开户业务委托                                                            
#define		FUNC_SSKH_CXDRKHWT		71102	//查询当日实时开户委托流水                                                  
#define		FUNC_SSKH_CXDSHKHWT		71103	//查询待审核开户委托流水                                                    
#define		FUNC_SSKH_CXYSHKHWT		71104	//查询已审核开户委托流水                                                    
#define		FUNC_SSKH_SHKHWT		71105	//审核开户委托流水                                                            
#define		FUNC_SSKH_GDKDY			71106	//股东卡打印                                                                  
                                                                                                                  
#define		FUNC_SSKH_CXLSKHWT		71150	//查询历史实时开户委托流水                                                  
                                                                                                                  
#define		FUNC_SSKH_CXKHHYCS		71190	//查询开户会员参数                                                          
                                                                                                                  
//A股开户类                                                                                                       
#define		 FUNC_KHSZ_KHZLDJ		  71001	  //深圳A、B股开户资料登记                                                  
#define		 FUNC_KHSZ_KHZLXG		  71002	  //深圳A、B股开户资料修改                                                  
#define		 FUNC_KHSZ_KHZLSC		  71003	  //删除深圳A、B股开户资料                                                  
#define		 FUNC_KHSZ_KHZLSH		  71004	  //深圳A、B股开户资料审核                                                  
#define		 FUNC_KHSZ_KHZLSHQX		71005	//深圳A、B股开户资料审核取消                                                
#define		 FUNC_KHSZ_KHZLSB		  71006	  //深圳A、B股开户资料申报                                                  
#define		 FUNC_KHSZ_KHZLCX		  71007	  //深圳A、B股开户资料未申报查询                                            
#define		 FUNC_KHSZ_GGGDZL		  71008	  //深圳A、B股股东资料修改                                                  
#define		 FUNC_KHSZ_HBDMK		 71009	 //深圳A、B股合并代码卡                                                     
#define		 FUNC_KHSZ_CXGDZH		  71010	  //深圳A、B股股东帐户查询                                                  
#define		 FUNC_KHSZ_CXWTSJ		  71012	  //深圳A、B股开户委托数据查询                                              
#define		 FUNC_KHSZ_CXHBSJ		  71013	  //深圳A、B股开户回报数据查询                                              
#define		 FUNC_KHSZ_KHDY		71014	//置开户打印标志                                                                
#define		 FUNC_KHSZ_DRCXHB		  71015	  //查询当日回报                                                            
//B股开户类                                                                                                       
#define		 FUNC_KHSZBG_KHZLDJ		71021	//深圳A、B股开户资料登记                                                    
#define		 FUNC_KHSZBG_KHZLXG		71022	//深圳A、B股开户资料修改                                                    
#define		 FUNC_KHSZBG_KHZLSC		71023	//删除深圳A、B股开户资料                                                    
#define		 FUNC_KHSZBG_KHZLSH		71024	//深圳A、B股开户资料审核                                                    
#define		 FUNC_KHSZBG_KHZLSHQX	  71025	  //深圳A、B股开户资料审核取消                                            
#define		 FUNC_KHSZBG_KHZLSB		71026	//深圳A、B股开户资料申报                                                    
#define		 FUNC_KHSZBG_KHZLCX		71027	//深圳A、B股开户资料未申报查询                                              
#define		 FUNC_KHSZBG_GGGDZL		71028	//深圳A、B股股东资料修改                                                    
#define		 FUNC_KHSZBG_HBDMK		   71029   //深圳A、B股合并代码卡                                                 
#define		 FUNC_KHSZBG_CXGDZH		71030	//深圳A、B股股东帐户查询                                                    
#define		 FUNC_KHSZBG_CXWTSJ		71032	//深圳A、B股开户委托数据查询                                                
#define		 FUNC_KHSZBG_CXHBSJ		71033	//深圳A、B股开户回报数据查询                                                
#define		 FUNC_KHSZ_CXXGWTXX		71034	//深圳A, B股股东资料修改委托查询                                            
                                                                                                                  
//三板开户                                                                                                        
#define		FUNC_KHPT_KHZLDJ		71201	//三板开户资料登记                                                            
#define		FUNC_KHPT_KHZLXG		71202	//三板开户资料修改                                                            
#define		FUNC_KHPT_KHZLSC		71203	//删除三板开户资料                                                            
#define		FUNC_KHPT_KHZLSH		71204	//三板开户资料审核                                                            
#define		FUNC_KHPT_KHZLSHQX		71205	//三板开户资料审核取消                                                      
#define		FUNC_KHPT_KHZLSB		71206	//三板开户资料申报                                                            
#define		FUNC_KHPT_KHZLCX		71207	//三板开户资料未申报部分查询                                                  
#define		FUNC_KHPT_GGGDZL		71208	//三板股东资料修改                                                            
#define		FUNC_KHPT_HBDMK			71209	//三板合并代码卡                                                              
#define		FUNC_KHPT_CXGDZH		71210	//三板股东帐户查询                                                            
#define		FUNC_KHPT_CXWTSJ		71211	//三板开户委托数据查询                                                        
#define		FUNC_KHPT_CXHBSJ		71212	//三板开户回报数据查询                                                        
#define		FUNC_KHPT_CXRYJ			71213	//三板开户日月结数据查询                                                      
#define		FUNC_KHPT_PZDY			71218	//三板开户凭证打印                                                            
#define		FUNC_KHPT_SFLSCX		71220	//收费流水查询                                                                
#define		FUNC_KHPT_QSDMCX		71221	//三板券商代码查询                                                            
//三板转托管                                                                                                      
#define		 FUNC_KHPT_ZTGDJ		 71214	 //三板转托管登记                                                           
#define		 FUNC_KHPT_ZTGCX		 71215	 //三板转托管查询                                                           
#define		 FUNC_KHPT_ZTGSC		 71216	 //三板转托管删除                                                           
#define		 FUNC_KHPT_ZTGXG		 71217	 //三板转托管修改                                                           
                                                                                                                  
//三板股份认领登记                                                                                                
#define		 FUNC_KHRL_RLDJ	 71231	 //三板股份登记                                                                 
#define		 FUNC_KHRL_ZLXG	 71232	 //三板股份登记资料修改                                                         
#define		 FUNC_KHRL_ZLSC	 71233	 //三板股份登记资料删除                                                         
#define		 FUNC_KHRL_WTCX	 71234	 //三板股份登记资料查询                                                         
#define		 FUNC_KHRL_HBCX	 71235	 //三板股份登记回报查询                                                         
#define		 FUNC_KHRL_PZDY	 71236	 //三板股份登记凭证打印                                                         
                                                                                                                  
#define		 FUNC_KHRL_SSGFCX	   71238   //承办认领股份查询                                                         
#define		 FUNC_KHRL_ZQCX	 71239	 //三板承办证券代码查询                                                         
                                                                                                                  
                                                                                                                  
//上海A股开户                                                                                                     
#define		 FUNC_KHSHA_LRZLDJ		71251	//上海A股开户资料输入                                                       
#define		 FUNC_KHSHA_LRZLXG		71252	//上海A股录入资料修改                                                       
#define		 FUNC_KHSHA_LRZLSC		71253	//上海A股录入资料删除                                                       
//#define	 FUNC_KHSHA_LRZLSH		71254	//上海A股录入资料审核                                                       
//#define	 FUNC_KHSHA_LRZLSHQX	  71255	  //上海A股录入资料审核取消                                               
#define		 FUNC_KHSHA_LRZLSB		71256	//上海A股录入资料申报                                                       
#define		 FUNC_KHSHA_LRZLCX		71257	//上海A股录入资料查询                                                       
#define		 FUNC_KHSHA_ZHKDY		  71258	  //上海A股帐户卡打印                                                       
#define		 FUNC_KHSHA_XHZHLR		71259	//上海A股转户销户录入                                                       
#define		 FUNC_KHSHA_XHZHXG		71260	//上海A股转户销户录入资料修改                                               
#define		 FUNC_KHSHA_XHZHSC		71261	//上海A股转户销户录入资料删除                                               
#define		 FUNC_KHSHA_XHZHSB		71262	//上海A股转户销户录入资料申报                                               
#define		 FUNC_KHSHA_XHZHCX		71263	//上海A股转户销户录入资料查询                                               
#define		 FUNC_KHSHA_SBSJCX		71269	//上海A股转户销户委托查询                                                   
                                                                                                                  
#define		 FUNC_KHSHA_XHZHJGCX	  71264	  //上海A股转户销户处理结果查询                                           
#define		 FUNC_KHSHA_CXKHSJ		71265	//上海A股开户数据查询                                                       
#define		 FUNC_KHSHA_CXHBSJ		71266	//上海A股开户回报查询                                                       
#define		 FUNC_KHSHA_XGGDZL		71267	//上海A股修改股东资料                                                       
#define		 FUNC_KHSHA_RJCX	  71268	  //上海A深圳A日结查询                                                        
#define	   FUNC_KHSHA_GDGSSQ		   71270   //上海A股股东挂失                                                      
#define	   FUNC_KHSHA_CXGDGSSQ		 71271	 //上海A股股东挂失查询                                                  
#define	   FUNC_KHSHA_XHZHWTCX		71272	//上海A股股东挂失查询                                                     
                                                                                                                  
                                                                                                                  
#define		 FUNC_KHSHB_ZDJYDJ		71330	//上海B股指定资料录入                                                       
#define		 FUNC_KHSHB_ZDJYXG		71331	//上海B股指定资料修改                                                       
#define		 FUNC_KHSHB_ZDJYSC		71332	//上海B股指定资料删除                                                       
#define		 FUNC_KHSHB_ZDJYSB		71333	//上海B股指定资料提交                                                       
#define		 FUNC_KHSHB_ZDJYCX		71334	//上海B股指定资料查询                                                       
                                                                                                                  
//上海B股                                                                                                         
#define	  FUNC_KHSHB_LRZLDJ	 71301 //上海B股开户资料输入                                                          
#define	  FUNC_KHSHB_LRZLXG	 71302 //上海B股录入资料修改                                                          
#define	  FUNC_KHSHB_LRZLSC	 71303 //上海B股录入资料删除                                                          
#define	  FUNC_KHSHB_LRZLSH	 71304 //上海B股录入资料审核                                                          
#define	  FUNC_KHSHB_LRZLSHQX	   71305 //上海B股录入资料审核取消                                                  
#define	  FUNC_KHSHB_LRZLSB	 71306 //上海B股录入资料申报                                                          
#define	  FUNC_KHSHB_LRZLCX	 71307 //上海B股录入资料查询                                                          
#define	  FUNC_KHSHB_ZHKDY	71308 //上海B股帐户卡打印                                                             
                                                                                                                  
#define	  FUNC_KHSHBN_LRZLDJ		71421 //上海B股非实时开户资料输入                                                 
#define	  FUNC_KHSHBN_LRZLXG		71422 //上海B股(非实时)录入资料修改                                               
#define	  FUNC_KHSHBN_LRZLSC		71423 //上海B股(非实时)录入资料删除                                               
#define	  FUNC_KHSHBN_LRZLSH		71424 //上海B股(非实时)录入资料审核                                               
#define	  FUNC_KHSHBN_LRZLSGQX		71425 //上海B股(非实时)录入资料审核取消                                         
#define	  FUNC_KHSHBN_LRZLSB		71426 //上海B股(非实时)录入资料申报                                               
                                                                                                                  
#define	  FUNC_KHSHB_CXKHSJ		71315 //上海B股开户数据查询                                                         
#define	  FUNC_KHSHB_CXHBSJ		71316 //上海B股开户回报查询                                                         
                                                                                                                  
//add by DaiXiaoGe 2003.05.09 客户服务定制 liufaxian 改为17500~17599                                              
#define	  FUNC_KHFW_CXFWXM		17501	//查询已定制的客户服务项目                                                    
#define	  FUNC_KHFW_CXFWCS		17502	//定制客户服务参数查询                                                        
#define	  FUNC_KHFW_KHFWXMSQCX		17503	//客户定制服务申请或撤销                                                  
#define	  FUNC_KHFW_CXKHSQLS		17504	//客户定制服务受理情况查询                                                  
#define	  FUNC_KHFW_CXQTWSLFW		17505	//全体未受理服务查询                                                        
#define	  FUNC_KHFW_CXQTYSLFWLS		17506	//全体已受理服务明细查询                                                  
                                                                                                                  
// ISSW	FUNC_ DEFINE BEGIN                                                                                        
// ISSW	from dep2 by Liufaxian                                                                                    
#define	FUNC_ISSW_KH_ZXKHDJ			31001		   //在线客户登记；ID号申请。                                             
#define	FUNC_ISSW_KH_ZXKHJY			31002		   //在线客户校验                                                         
#define	FUNC_ISSW_KH_CXKHH			31003		   //查询客户号                                                           
#define	FUNC_ISSW_KH_KHHDJ			31004		   //客户号登记                                                           
#define	FUNC_ISSW_KH_KHHQX			31005		   //客户号取消                                                           
#define	FUNC_ISSW_KH_KHXXXG			31006		   //客户信息修改                                                         
#define	FUNC_ISSW_KH_KHHXXXG		   31007		   //客户号信息修改                                                   
#define	FUNC_ISSW_KH_KHXXCX			31008		   //客户号信息查询                                                       
//2003.01.06 zqh                                                                                                  
#define	FUNC_ISSW_KH_QTZXKHCX		31009		   //查询全体在线客户已登记的基本信息，包括在线状态(在线，不在线).        
#define	FUNC_ISSW_KH_ZXKHXXXG		31010		   //修改在线客户已登记的基本信息                                         
#define	FUNC_ISSW_KH_ZXKHXXSC		31011		   //删除在线客户已登记的基本信息                                         
#define	FUNC_ISSW_KH_DGZXKHCX		31012		   //查询单个在线客户已登记的基本信息(按客户号查询或客户代码查询)         
                                                                                                                  
//##2002.11.26,	YZP新增                                                                                           
#define	FUNC_ISSW_SJ_INFO			   31021		   //查找指定目录中ISSW客户模块的信息                                   
#define	FUNC_ISSW_SJ_FILE			   31022		   //传输指定的文件                                                     
//2003.03.05 Liufaxian                                                                                            
#define	FUNC_XT_GETALLROUTER		   31051		   //路由营业部查询,对应于(AI_XT_ROUTE)                               
#define	FUNC_XT_GETISSWXTXX			31052		   //取ISSW系统信息                                                       
#define  FUNC_XT_GETDATETIME        31053       //返回系统时间 //2005.05.23 liufaxian add                         
#define	FUNC_XT_ZS_FRESH			   31060		   //更新指数                                                           
#define	FUNC_XT_ZS_GETZS			   31061		   //取指数                                                             
//证书处理 31030~31039                                                                                            
#define	FUNC_ISSW_CERTREQ				31030		   //申请证书                                                             
#define	FUNC_ISSW_CERTRECV			31031		   //接收证书文件                                                         
//行情处理 31040~31049                                                                                            
#define  FUNC_ISSW_HQ_HQCX          31040       //行情查询                                                        
#define  FUNC_ISSW_HQ_FSZS          31041       //查询分时走势                                                    
#define  FUNC_ISSW_HQ_CJMX          31042       //查询成交明细                                                    
#define  FUNC_ISSW_HQ_DAY           31043       //K(日)线查询                                                     
#define  FUNC_ISSW_HQ_MN5           31044       //5分钟线查询                                                     
//柜台与服务扩展                                                                                                  
#define	FUNC_ZH_TZZHSJ				   31101		   //投资组合设计                                                       
#define	FUNC_ZH_TZZHCX				   31102		   //投资组合查询                                                       
#define	FUNC_ZH_ZHXMCX				   31103		   //组合项目查询                                                       
#define	FUNC_ZH_ZHXMSJ				   31104		   //组合项目设计                                                       
                                                                                                                  
#define	FUNC_ISSW_MD_MDJLCX			31111		   //埋单记录查询                                                         
#define	FUNC_ISSW_MD_MDJLWH			31112		   //埋单记录维护                                                         
#define	FUNC_ISSW_MD_MDJLCL			31113		   //埋单记录处理                                                         
                                                                                                                  
#define	FUNC_ISSW_PL_GETPCH			31120		   //获取业务批次号                                                       
#define	FUNC_ISSW_PL_PLWTDJ			31121		   //批量委托登记                                                         
#define	FUNC_ISSW_PL_WTPCCX			31122		   //查询业务批次(按客户号查询批次号)                                     
#define	FUNC_ISSW_PL_PLWTCX			31123		   //批量委托查询(按批次号查询批量委托记录)                               
                                                                                                                  
//服务类                                                                                                          
#define	FUNC_ISSW_FW_ZXGSJ			31201		   //自选股设计                                                           
#define	FUNC_ISSW_FW_ZXGCX			31202		   //自选股查询                                                           
#define	FUNC_ISSW_FW_FWMLCX			31203		   //统一服务目录清单查询                                                 
#define	FUNC_ISSW_FW_HQYJSJ			31204		   //增加、删除、修改行情预警项目                                         
#define	FUNC_ISSW_FW_HQYJCX			31205		   //行情预警查询                                                         
#define	FUNC_ISSW_FW_HQFSSJ			31206		   //登记、删除定时行情发送项目                                           
#define	FUNC_ISSW_FW_HQFSCX			31207		   //查询客户定义的行情发送项目                                           
#define	FUNC_ISSW_FW_SJDJSQ			31208		   //申请或取消登记手机号，由AUGATE发送手机密码                           
#define	FUNC_ISSW_FW_SMS			   31209		   //发送短信                                                           
#define	FUNC_ISSW_FW_EMAIL			31210		   //发送邮件                                                             
#define	FUNC_ISSW_FW_FWXMCX			31211		   //查询柜台和客户服务中间件可提供的服务项目                             
#define	FUNC_ISSW_FW_FWXYCX			31212		   //服务项目协议查询                                                     
#define	FUNC_ISSW_FW_FWXMSQ			31213		   //服务项目申请开通或取消                                               
//#2002.10.31 zqh                                                                                                 
#define	FUNC_ISSW_FW_XXTZSJ			31214		   //普通消息通知设计                                                     
#define	FUNC_ISSW_FW_XXTZCX			31215		   //普通消息通知查询                                                     
#define	FUNC_ISSW_FW_HLCX			   31216		   //汇率查询                                                           
                                                                                                                  
//主动推送服务...2002.12.25	zqh                                                                                   
//##查询类...                                                                                                     
#define	FUNC_ISSW_TSFW_CX_GXDZ		31220		   //个性化服务查询                                                     
#define	FUNC_ISSW_TSFW_CX_JYJK		31221		   //交易监控服务查询                                                   
#define	FUNC_ISSW_TSFW_CX_HQJK		31222		   //行情监控服务查询                                                   
#define	FUNC_ISSW_TSFW_CX_PTTG		31223		   //普通通告服务查询                                                   
#define	FUNC_ISSW_TSFW_CX_GGHQ		31224		   //行情点播服务查询                                                   
#define	FUNC_ISSW_TSFW_CX_KHDZ		31225		   //客户联系信息查询                                                   
#define	FUNC_ISSW_TSFW_CX_FWML		31226		   //服务目录查询                                                       
//##2002.12.29 zqh                                                                                                
#define	FUNC_ISSW_TSFW_CX_XTDM		31227		   //系统代码查询                                                       
#define	FUNC_ISSW_TSFW_CX_KHLB		31228		   //客户类别查询                                                       
//##end                                                                                                           
//##维护类...                                                                                                     
//##1.登记...                                                                                                     
#define	FUNC_ISSW_TSFW_DJ_GXDZ		31330		   //个性化服务登记                                                     
#define	FUNC_ISSW_TSFW_DJ_JYJK		31331		   //交易监控服务登记                                                   
#define	FUNC_ISSW_TSFW_DJ_HQJK		31332		   //行情阀值服务登记                                                   
#define	FUNC_ISSW_TSFW_DJ_PTTG		31333		   //普通通告服务登记                                                   
#define	FUNC_ISSW_TSFW_DJ_GGHQ		31334		   //个股行情通告登记                                                   
#define	FUNC_ISSW_TSFW_DJ_KHDZ		31335		   //客户联系信息登记                                                   
//##2.修改...                                                                                                     
#define	FUNC_ISSW_TSFW_XG_GXDZ		31340		   //个性化服务修改                                                     
#define	FUNC_ISSW_TSFW_XG_JYJK		31341		   //交易监控服务修改                                                   
#define	FUNC_ISSW_TSFW_XG_HQJK		31342		   //行情监控服务修改                                                   
#define	FUNC_ISSW_TSFW_XG_PTTG		31343		   //普通通告服务修改                                                   
#define	FUNC_ISSW_TSFW_XG_GGHQ		31344		   //行情点播服务修改                                                   
#define	FUNC_ISSW_TSFW_XG_KHDZ		31345		   //客户联系信息修改                                                   
//##3.取消或删除                                                                                                  
#define	FUNC_ISSW_TSFW_QX_GXDZ		31350		   //个性化服务取消                                                     
#define	FUNC_ISSW_TSFW_QX_JYJK		31351		   //交易监控服务取消                                                   
#define	FUNC_ISSW_TSFW_QX_HQJK		31352		   //行情监控服务取消                                                   
#define	FUNC_ISSW_TSFW_QX_PTTG		31353		   //普通通告服务取消                                                   
#define	FUNC_ISSW_TSFW_QX_GGHQ		31354		   //行情点播服务取消                                                   
#define	FUNC_ISSW_TSFW_SC_KHDZ		31355		   //客户联系信息删除                                                   
                                                                                                                  
//.....##在线交流类##.....                                                                                        
#define	FUNC_ISSW_TX_TXLFZCX		   31401		   //通讯录分组查询                                                   
#define	FUNC_ISSW_TX_TXLFZWH		   31402		   //通讯录分组维护                                                   
#define	FUNC_ISSW_TX_TXLCX			31403		   //客户通讯录查询                                                       
#define	FUNC_ISSW_TX_TXLWH			31404		   //客户通讯录维护                                                       
#define	FUNC_ISSW_TX_XXFS			   31405		   //在线信息发布                                                       
#define	FUNC_ISSW_TX_XXCX			   31406		   //在线信息查询                                                       
#define	FUNC_ISSW_TX_ZXKHCX			31407		   //在线客户查询                                                         
#define	FUNC_ISSW_TX_ZXZTSZ			31408		   //在线状态设置                                                         
//##2002.10.17 ZQH新增                                                                                            
#define	FUNC_ISSW_TX_KHSXDL			31409		   //客户上线登录                                                         
#define	FUNC_ISSW_TX_KHQTLX			31410		   //客户签退离线                                                         
//##2003.01.07 zqh新增                                                                                            
#define	FUNC_ISSW_JL_DJ_KHZX		   31411		   //客户咨询登记                                                     
#define	FUNC_ISSW_JL_DJ_QSLY		   31412		   //券商留言登记                                                     
#define	FUNC_ISSW_JL_CX_KHZX		   31413		   //客户咨询查询                                                     
#define	FUNC_ISSW_JL_CX_QSLY		   31414		   //券商留言查询                                                     
#define	FUNC_ISSW_JL_CX_QSHF		   31415		   //券商回复查询                                                     
#define	FUNC_ISSW_JL_CX_QSTG		   31416		   //券商通告查询                                                     
//2003.03.05 liu add                                                                                              
#define	FUNC_ISSW_JL_SC_JLXX		   31417		   //删除已阅读的交流信息                                             
// 31500~31599 为ISSW内部数据定义                                                                                 
// ISSW	FUNC_ DEFINE END                                                                                          
                                                                                                                  
                                                                                                                  
#define	FUNC_DAGL_MKDJ			30001	//单元模块登记                                                                  
#define	FUNC_DAGL_DAXG			30002	//档案修改                                                                      
#define	FUNC_DAGL_SCDA			30003	//档案删除                                                                      
#define	FUNC_DAGL_DACX			30004	//档案查询                                                                      
#define	FUNC_DAGL_CSBJ			30005	//测试标记                                                                      
#define	FUNC_DAGL_RWDJ			30006	//任务登记                                                                      
#define	FUNC_DAGL_RWXG			30007	//任务修改                                                                      
#define	FUNC_DAGL_RWCX			30008	//任务查询                                                                      
#define	FUNC_DAGL_RWSC			30009	//任务删除                                                                      
#define	FUNC_DAGL_CSDJ			30010	//参数登记                                                                      
#define	FUNC_DAGL_CSXG			30011	//参数修改                                                                      
#define	FUNC_DAGL_CSSC			30012	//参数删除                                                                      
#define	FUNC_DAGL_CXRWBH		30013	//任务编号查询                                                                  
#define	FUNC_DAGL_CXZD			30014	//字典查询                                                                      
#define	FUNC_DAGL_CXGYSX		30015	//柜员查询                                                                      
#define	FUNC_DAGL_CXTS			30016	//查询消息                                                                      
#define	FUNC_DAGL_TJFX			30017	//查询消息                                                                      
#define	FUNC_DAGL_DABF			30018	//备份档案库                                                                    
#define	FUNC_DAGL_GDJC			30019	//归档检查                                                                      
#define	FUNC_DAGL_RWDY			30020	//任务打印                                                                      
#define	FUNC_DAGL_WHCX			30021	//维护档案查询                                                                  
#define	FUNC_DAGL_WHDJ			30022	//维护档案登记                                                                  
#define	FUNC_DAGL_WHXG			30023	//维护档案修改                                                                  
#define	FUNC_DAGL_ANTJ			30024	//客户资料查询                                                                  
                                                                                                                  
#define	FUNC_DAGL_KHXQCX		30025	//客户需求查询                                                                  
#define	FUNC_DAGL_KHXQDJ		30026	//客户需求登记                                                                  
#define	FUNC_DAGL_KHXQXG		30027	//客户需求修改                                                                  
#define	FUNC_DAGL_KHXQSC		30028	//客户需求删除                                                                  
                                                                                                                  
#define	FUNC_DAGL_KHCX			30029	//客户资料查询                                                                  
#define	FUNC_DAGL_KHDJ			30030	//客户资料登记                                                                  
#define	FUNC_DAGL_KHXG			30031	//客户资料修改                                                                  
#define	FUNC_DAGL_KHDEL			30032	//客户资料删除                                                                  
                                                                                                                  
#define	FUNC_DAGL_KHCPCX		30033	//客户产品查询                                                                  
#define	FUNC_DAGL_KHCPDJ		30034	//客户产品登记                                                                  
#define	FUNC_DAGL_KHCPXG		30035	//客户产品修改                                                                  
                                                                                                                  
#define	FUNC_DAGL_KHLXRCX		30036	//客户联系人查询                                                                
#define	FUNC_DAGL_KHLXRDJ		30037	//客户联系人登记                                                                
#define	FUNC_DAGL_KHLXRXG		30038	//客户联系人修改                                                                
                                                                                                                  
#define	FUNC_DAGL_WHCJWTDJ		30039	//                                                                            
#define	FUNC_DAGL_WHCJWTCX		30040	//                                                                            
                                                                                                                  
#define	FUNC_DAGL_RZCX			30041	//档案管理日志查询                                                              
                                                                                                                  
#define	FUNC_DAGL_FIELDXXCX		30050	//数据域信息查询                                                              
#define	FUNC_DAGL_FIELDXXDJ		30051	//数据域信息登记                                                              
#define	FUNC_DAGL_FIELDXXXG		30052	//数据域信息修改                                                              
#define	FUNC_DAGL_FIELDXXSC		30053	//数据域信息删除                                                              
                                                                                                                  
#define	FUNC_DAGL_FUNCXXCX		30054	//功能码信息查询                                                              
#define	FUNC_DAGL_FUNCXXDJ		30055	//功能码信息登记                                                              
#define	FUNC_DAGL_FUNCXXXG		30056	//功能码信息修改                                                              
#define	FUNC_DAGL_FUNCXXSC		30057	//功能码信息删除                                                              
                                                                                                                  
#define	FUNC_DAGL_FUNCPARACX	30058	//功能码参数查询                                                              
#define	FUNC_DAGL_FUNCPARADJ	30059	//功能码参数登记                                                              
#define	FUNC_DAGL_FUNCPARAXG	30060	//功能码参数修改                                                              
#define	FUNC_DAGL_FUNCPARASC	30061	//功能码参数删除                                                              
                                                                                                                  
#define	FUNC_DAGL_DLGYCX		30062	//登陆柜员查询                                                                  
#define	FUNC_DAGL_TJDH			30063	//添加对话                                                                      
#define	FUNC_DAGL_DHXXCX		30064	//对话读取                                                                      
#define	FUNC_DAGL_GYQD			30065	//柜员签到                                                                      
#define	FUNC_DAGL_GYQT			30066	//柜员签退                                                                      
                                                                                                                  
#define	FUNC_DAGL_TEST_TD		30067	//SHMM测试数据管理                                                              
#define	FUNC_DAGL_TEST_TS		30068	//SHMM测试                                                                      
#define	FUNC_DAGL_TEST_TR		30069	//SHMM测试后数据分析报告                                                        
#define	FUNC_DAGL_GET_PROC_VER	30070	//获取存储过程版本号                                                        
                                                                                                                  
//消息订阅(RTS)定义	开始                                                                                          
#define	FUNC_RTS_CJHB_DY			81001	//成交回报订阅                                                                
#define	FUNC_RTS_CJHB_FB			81002	//成交回报发布                                                                
#define	FUNC_RTS_CJHB_CX			81003	//成交回报订阅结果查询                                                        
#define	FUNC_RTS_CJHB_DEL			81005	//取消成交回报订阅                                                            
                                                                                                                  
#define	FID_15_TASKID		FID_15_LOGINID			//订约任务ID                                                          
#define	FUNC_RTS_BEGIN			81001                                                                                 
#define	FUNC_RTS_END			81999                                                                                   
//消息订阅(RTS)定义	结束                                                                                          
                                                                                                                  
//影像系统参数类                                                                                                  
#define		 FUNC_DA_XT_ISMYCUSTOM		33101	//取柜员某菜单对股民权限                                                
#define		 FUNC_DA_XT_GETGYXX		33103	//获取柜员信息                                                              
#define		 FUNC_DA_XT_GETGYSX		33104	//获得柜员属性                                                              
#define		 FUNC_DA_XT_GETALLKHSX		33112	//获得客户属性                                                          
#define		 FUNC_DA_XT_GETALLMENU		33119	//查询所有菜单                                                          
#define		 FUNC_DA_XT_GETGYZJQX		33121	//查询柜员直接权限                                                        
#define		 FUNC_DA_XT_CXGJDMDZ		33123	//查询国籍代码对照                                                        
#define		 FUNC_DA_XT_CXYHDM		33124	//查询外部机构代码                                                          
#define		 FUNC_DA_XT_CXSFDM		33125	//查询省份代码                                                              
#define		 FUNC_DA_XT_CXCSDM		33126	//查询城市代码                                                              
#define		 FUNC_DA_XT_CXQXDM		33127	//查询区县代码                                                              
#define		 FUNC_DA_XT_CDGYXX		33128	//查询有指定菜单权限的柜员号                                                
#define		 FUNC_DA_XT_CXCWDM		33130	 //查询错误代码                                                             
#define		 FUNC_DA_XT_JLGZRZ		33131	 //记录工作日志                                                             
#define		 FUNC_DA_XT_CXGZRZ		33132	 //查询工作日志                                                             
#define		 FUNC_DA_XT_GETGYWSHQX		33133	//查询柜员未审核的权限                                                  
#define		 FUNC_DA_XT_CXZXCS		33134	//查询杂项参数                                                              
#define		 FUNC_DA_XT_XGZXCS		33135	//修改杂项参数                                                              
                                                                                                                  
                                                                                                                  
//影像营业部管理                                                                                                  
#define		 FUNC_DA_JYGL_BGCDQX		33201	//变更客户菜单权限                                                        
#define		 FUNC_DA_JYGL_CXDFHCDQX		33202	//查询待复核菜单权限                                                    
#define		 FUNC_DA_JYGL_CXFHCDQXLS	33203	//查询复核菜单权限流水                                                  
#define		 FUNC_DA_JYGL_SHCDQX		33204	//审核菜单权限                                                            
#define		 FUNC_DA_JYGL_XGGYXX		33205	//修改柜员信息                                                            
#define		 FUNC_DA_JYGL_SCGYXX		33206	//删除柜员                                                                
#define		 FUNC_DA_JYGL_DJGYXX		33207	//登记柜员                                                                
#define		 FUNC_DA_JYGL_QXDTGY		33208	//复制柜员的权限                                                          
#define		 FUNC_DA_JYGL_CXDLZT		33212	//查询登录状态                                                            
#define		 FUNC_DA_JYGL_SCDLZT		33213	//删除登录状态                                                            
#define		 FUNC_DA_JYGL_KHSXLBZJ		33214	//客户属性类别增加                                                      
#define		 FUNC_DA_JYGL_KHSXLBXG		33215	//客户属性类别修改                                                      
#define		 FUNC_DA_JYGL_KHSXLBSC		33216	//客户属性类别删除                                                      
#define		 FUNC_DA_JYGL_KHSXLBCX		33217	//客户属性类别查询                                                      
#define		 FUNC_DA_JYGL_SJZDZJ		33218	//数据字典增加                                                            
#define		 FUNC_DA_JYGL_SJZDXG		33219	//数据字典修改                                                            
#define		 FUNC_DA_JYGL_SJZDSC		33220	//数据字典删除                                                            
#define		 FUNC_DA_JYGL_SJZDCX		33221	//数据字典查询                                                            
#define		 FUNC_DA_JYGL_JGDMZJ		33222	//机构代码增加                                                            
#define		 FUNC_DA_JYGL_JGDMXG		33223	//机构代码修改                                                            
#define		 FUNC_DA_JYGL_JGDMSC		33224	//机构代码删除                                                            
#define		 FUNC_DA_JYGL_JGDMCX		33225	//机构代码查询                                                            
#define		 FUNC_DA_JYGL_CDZJ			33226	//菜单增加                                                                
#define		 FUNC_DA_JYGL_CDXG			33227	//菜单修改                                                                
#define		 FUNC_DA_JYGL_CDSC			33228	//菜单删除                                                                
#define		 FUNC_DA_JYGL_CDCX			33229	//菜单查询                                                                
#define		 FUNC_DA_JYGL_DATACX		33230	//数据库信息查询                                                          
#define		 FUNC_DA_JYGL_DEVICECX		33231	//设备信息查询                                                          
#define		 FUNC_DA_JYGL_DATABF		33232	//数据备份                                                                
#define		 FUNC_DA_JYGL_BFWJCX		33233	//备份文件信息查询                                                        
#define		 FUNC_DA_JYGL_QBFJLCX		33234	//数据库全备份记录查询                                                    
#define		 FUNC_DA_JYGL_QBFXXCX		33235	//数据库全备份信息查询                                                    
#define		 FUNC_DA_JYGL_SJKJCCX		33236	//数据库进程查询                                                          
#define		 FUNC_DA_JYGL_DATAHY		33237	//数据库还原                                                              
#define		 FUNC_DA_JYGL_GTPZCX		33238	//柜台DBF配置查询                                                         
#define		 FUNC_DA_JYGL_GTPZZJ		33239	//柜台DBF配置增加                                                         
#define		 FUNC_DA_JYGL_GTPZXG		33240	//柜台DBF配置修改                                                         
#define		 FUNC_DA_JYGL_GTPZSC		33241	//柜台DBF配置删除                                                         
                                                                                                                  
//影像客户管理                                                                                                    
#define		 FUNC_DA_KHGL_KHDJ			33301	//客户登记                                                                
#define		 FUNC_DA_KHGL_KHJBXXXG		33302	//客户信息修改                                                          
#define		 FUNC_DA_KHGL_KHSC			33303	//客户注销                                                                
#define		 FUNC_DA_KHGL_KHJBXXCX		33304	//客户信息查询                                                          
#define		 FUNC_DA_KHGL_KHFJSXDJ		33305	//客户附加属性登记                                                      
#define		 FUNC_DA_KHGL_KHFJSXXG		33306	//客户附加属性修改                                                      
#define		 FUNC_DA_KHGL_KHFJSXSC		33307	//客户附加属性删除                                                      
#define		 FUNC_DA_KHGL_KHFJSXCX		33308	//客户附加属性查询                                                      
#define		 FUNC_DA_KHGL_KHYXDAJBXXDJ	33309	//客户影像档案基本信息登记                                            
#define		 FUNC_DA_KHGL_KHYXDAJBXXXG	33310	//客户影像档案基本信息修改                                            
#define		 FUNC_DA_KHGL_KHYXDAJBXXSC	33311	//客户影像档案基本信息删除                                            
#define		 FUNC_DA_KHGL_KHYXDAJBXXCX	33312	//客户影像档案基本信息查询                                            
#define		 FUNC_DA_KHGL_KHYXDATXXXBC	33313	//客户影像档案图像信息保存                                            
#define		 FUNC_DA_KHGL_KHYXDATXXXCX	33314	//客户影像档案图像信息查询                                            
#define		 FUNC_DA_KHGL_KHYXDAGJXXDJ	33315	//客户影像档案关键信息登记                                            
#define		 FUNC_DA_KHGL_KHYXDAGJXXXG	33316	//客户影像档案关键信息修改                                            
#define		 FUNC_DA_KHGL_KHYXDAGJXXSC	33317	//客户影像档案关键信息删除                                            
#define		 FUNC_DA_KHGL_KHYXDAGJXXCX	33318	//客户影像档案关键信息查询                                            
#define		 FUNC_DA_KHGL_DALXDJ		33319	//档案类型登记                                                            
#define		 FUNC_DA_KHGL_DALXXG		33320	//档案类型修改                                                            
#define		 FUNC_DA_KHGL_DALXSC		33321	//档案类型删除                                                            
#define		 FUNC_DA_KHGL_DALXCX		33322	//档案类型查询                                                            
#define		 FUNC_DA_KHGL_DALXGJXXDJ	33323	//档案类型关键信息登记                                                  
#define		 FUNC_DA_KHGL_DALXGJXXXG	33324	//档案类型关键信息修改                                                  
#define		 FUNC_DA_KHGL_DALXGJXXSC	33325	//档案类型关键信息删除                                                  
#define		 FUNC_DA_KHGL_DALXGJXXCX	33326	//档案类型关键信息查询                                                  
#define		 FUNC_DA_KHGL_KHYXDASH		33327	//客户影像档案审核                                                      
#define		 FUNC_DA_KHGL_SHBHEDAWH		33328	//审核不合格档案维护                                                    
#define		 FUNC_DA_KHGL_GYCZLSCX		33330	//柜员操作流水查询                                                      
#define		 FUNC_DA_KHGL_SHBHEDACX		33331	//审核不合格档案查询                                                    
#define		 FUNC_DA_KHGL_KHDAMBDJ		33332	//客户档案模板登记                                                      
#define		 FUNC_DA_KHGL_KHDAMBXG		33333	//客户档案模板修改                                                      
#define		 FUNC_DA_KHGL_KHDAMBSC		33334	//客户档案模板删除                                                      
#define		 FUNC_DA_KHGL_KHDAMBCX		33335	//客户档案模板查询                                                      
#define		 FUNC_DA_KHGL_KHDAMBJC		33336	//客户档案模板检测                                                      
#define		 FUNC_DA_KHGL_DAYJDJ		33337	//档案原件登记                                                            
#define		 FUNC_DA_KHGL_DAYJXG		33338	//档案原件修改                                                            
#define		 FUNC_DA_KHGL_DAYJSC		33339	//档案原件删除                                                            
#define		 FUNC_DA_KHGL_DAYJCX		33340	//档案原件查询                                                            
#define		 FUNC_DA_KHGL_ZDJCGZDJ		33341	//自动检测规则登记                                                      
#define		 FUNC_DA_KHGL_ZDJCGZXG		33342	//自动检测规则修改                                                      
#define		 FUNC_DA_KHGL_ZDJCGZSC		33343	//自动检测规则删除                                                      
#define		 FUNC_DA_KHGL_ZDJCGZCX		33344	//自动检测规则查询                                                      
#define		 FUNC_DA_KHGL_DAZDJC		33345	//档案自动检测                                                            
#define		 FUNC_DA_KHGL_DAFLCX		33346	//档案分类查询                                                            
#define		 FUNC_DA_KHGL_QSDACX		33347	//缺少档案查询                                                            
#define		 FUNC_DA_KHGL_JCNRCX		33348	//检测内容查询                                                            
#define		 FUNC_DA_KHGL_DAGJJC		33349	//档案高级检测                                                            
#define		 FUNC_DA_KHGL_DADJQTZL		33350	//登记其他资料                                                          
#define		 FUNC_DA_KHGL_DACLGTLS		33351	//处理柜台流水                                                          
#define		 FUNC_DA_KHGL_KHTXCX		33352	//客户图像信息查询                                                        
#define		 FUNC_DA_KHGL_GDHCX			33353	//查询股东号                                                              
#define		 FUNC_DA_KHGL_SFZCX			33354	//查询身份证                                                              
#define		 FUNC_DA_KHGL_DLRSFZCX		33355	//查询代理人身份证                                                      
#define		 FUNC_DA_KHGL_DALXFLDJ		33356	//档案类型分类登记                                                      
#define		 FUNC_DA_KHGL_DALXFLXG		33357	//档案类型分类修改                                                      
#define		 FUNC_DA_KHGL_DALXFLSC		33358	//档案类型分类删除                                                      
#define		 FUNC_DA_KHGL_DALXFLCX		33359	//档案类型分类查询                                                      
//为新修改的档案管理用                                                                                            
#define		 FUNC_DA_KHGL_ADDKHIMAGE		33360	//添加或修改客户图像资料                                              
#define		 FUNC_DA_KHGL_CXKHIMAGE		33361	//查询客户图像资料                                                      
#define		 FUNC_DA_KHGL_CXIMAGE		33362	//查询图像                                                                
#define		 FUNC_DA_KHGL_DELIMAGE		33363	//删除图像                                                              
//到stock2000的接口                                                                                               
#define      FUNC_DA_STK_CXKHXX		33370	//查询stock2000客户信息                                                   
//适配器                                                                                                          
#define		 FUNC_DA_CEAC_KHXXWBCX		33180	//客户信息外部查询                                                      
#define		 FUNC_DA_CEAC_KHXXTB		33181	//客户信息同步                                                            
#define		 FUNC_DA_CEAC_ZJZHTB		33182	//资金账户同步                                                            
#define		 FUNC_DA_CEAC_GDHTB		33183	//股东号同步                                                                
#define		 FUNC_DA_CEAC_YZZZDYTB		33184	//银证转帐对应同步                                                      
//中天要求加的                                                                                                    
#define		 FUNC_DA_ZT_ZJZL		33185	//增加资料                                                                    
#define		 FUNC_DA_ZT_SCZL		33186	//删除资料                                                                    
#define		 FUNC_DA_ZT_CXZL		33187	//查询资料                                                                    
#define		 FUNC_DA_CEAC_KHXXZLTB		33188	//客户信息增量同步                                                      
#define		 FUNC_DA_CEAC_ZJZHZLTB		33189	//资金账户增量同步                                                      
#define		 FUNC_DA_CEAC_GDHZLTB		33190	//股东号增量同步                                                          
#define		 FUNC_DA_CEAC_YZZZDYZLTB		33191	//银证转帐对应增量同步                                                
#define		 FUNC_DA_ZT_CXKH		33192	//查询卡号                                                                    
#define		 FUNC_DA_CEAC_CZMXZLTB		33193	//操作明细增量同步                                                      
#define		 FUNC_DA_ZT_ZLSLTJ		33194	//统计资料数量                                                              
#define		 FUNC_DA_CEAC_DLRZLTB		33195	//代理人增量同步                                                          
#define		 FUNC_DA_ZT_QSDACX		33196	//缺少档案查询                                                              
#define		 FUNC_DA_ZT_BQDACX		33197	//不缺档案查询                                                              
                                                                                                                  
//BFSVR/BFCLNT 实时热备程序应用功能码[60001	~ 60999]                                                              
#define		FUNC_BF_QUERYTABLES			60001	 //查询可同步的表                                                         
#define		FUNC_BF_QUERYSQL			60002	 //指定SQL获取更新数据                                                      
#define		FUNC_BF_SYNCTABLE			60003	 //同步指定表                                                               
#define		FUNC_BF_QUERYDUMP			60004	 //查询DUMP信息                                                             
#define		FUNC_BF_QUERYRECORD			60005	 //由指定索引字段查表中特定INSERT的记录                                   
#define		FUNC_BF_QUERYREQTABLES		60006	 //查询可定时同步的表                                                   
#define		FUNC_BF_QUERYALLRECORD		60007	 //由指定索引字段查表中所有的记录                                       
#define		FUNC_BF_QUERYUPDATERECORD	60008	 //由指定索引字段查表中特定UPDATE的记录                                 
#define		FUNC_BF_SYNCTABLE_YELS		60009	 //同步指定余额历史表                                                   
                                                                                                                  
#define		FUNC_BF_FILETRANSFER		60100	 //文件传输                                                               
//#define		FUNC_BF_GETFILEINFO			60101	 //获取指定文件的详细信息                                               
                                                                                                                  
//经纪人工作平台 add by lxj 20050606                                                                              
#define     FUNC_BRK_GYXXCX             32001  //柜员信息查询                                                     
#define     FUNC_BRK_GYXXCX_GYDM        32002  //按柜员代码查询柜员信息                                           
#define     FUNC_BRK_GYXXDJ             32003  //柜员信息登记                                                     
#define     FUNC_BRK_GYXXXG             32004  //柜员信息修改                                                     
#define     FUNC_BRK_GYXGMM             32005  //柜员修改密码                                                     
#define     FUNC_BRK_GYCZMM             32006  //柜员修改密码                                                     
#define     FUNC_BRK_GYDRXT             32007  //柜员登录系统                                                     
#define     FUNC_BRK_GYTCXT             32008  //柜员退出系统                                                     
#define     FUNC_BRK_GYMM_VALID         32009  //检查柜员密码有效性                                               
#define     FUNC_BRK_GYZTXG             32010  //柜员状态修改                                                     
                                                                                                                  
#define     FUNC_BRK_XTDMCX_FLDM        32030  //按分类代码查询系统代码                                           
#define     FUNC_BRK_YYBCX              32031  //营业部查询                                                       
#define     FUNC_BRK_GJCX               32032  //国籍查询                                                         
#define     FUNC_BRK_SFCX               32033  //省份查询                                                         
#define     FUNC_BRK_SQCX               32034  //市区查询                                                         
#define     FUNC_BRK_HQCX               32035  //辖区查询                                                         
                                                                                                                  
#define     FUNC_BRK_JJRCX              32080  //经纪人查询                                                       
#define     FUNC_BRK_JJRCX_JJRBH        32081  //经纪人查询,按代码                                                
#define     FUNC_BRK_JJRDJ              32082  //经纪人登记                                                       
#define     FUNC_BRK_JJRXG              32083  //经纪人修改                                                       
#define     FUNC_BRK_JJRZTXG            32084  //经纪人状态修改                                                   
#define     FUNC_BRK_JJGXCX             32085  //经纪人关系查询                                                   
#define     FUNC_BRK_JJGXDJ             32086  //经纪人关系登记                                                   
#define     FUNC_BRK_JJGXSC             32087  //经纪人关系删除                                                   
#define     FUNC_BRK_KHCX_SIM           32120  //客户查询                                                         
#define     FUNC_BRK_KHCX_ALL           32121  //客户查询                                                         
#define     FUNC_BRK_KHCX_DLR           32122  //代理人                                                           
#define     FUNC_BRK_KHCX_ZJZH          32123  //资金帐号                                                         
#define     FUNC_BRK_KHCX_CCCX          32124  //持仓查询                                                         
#define     FUNC_BRK_KHCX_ZJMX          32125  //资金明细                                                         
#define     FUNC_BRK_KHCX_ZQMX          32126  //证券明细                                                         
#define     FUNC_BRK_KHXG_LXXX          32127  //客户修改联系信息                                                 
#define     FUNC_BRK_KHCX_JGLS          32128  //交割流水查询                                                     
#define     FUNC_BRK_KHCX_GDH           32129  //股东号查询                                                       
                                                                                                                  
#define     FUNC_BRK_XXFS               32160  //信息发送                                                         
#define     FUNC_BRK_XXFSCX             32161  //信息发送查询                                                     
                                                                                                                  
#define     FUNC_BRK_ZQDMCX             32180  //证券代码查询                                                     
#define     FUNC_BRK_ZQJBMCX            32181  //证券基本面查询                                                   
#define     FUNC_BRK_ZQHQCX             32182  //证券行情查询                                                     
#define     FUNC_BRK_GGYLYC_BN          32183  //个股盈利预测查询（本年）                                         
#define     FUNC_BRK_GGYLYC_DN          32184  //个股盈利预测查询（多年）                                         
#define     FUNC_BRK_TZPJCX             32185  //投资评级查询                                                     
#define     FUNC_BRK_FXSPJCX            32186  //分析师评级查询                                                   
#define     FUNC_BRK_FXSSDCX            32187  //分析师视点查询                                                   
#define     FUNC_BRK_HGCX               32188  //换股查询                                                         
#define     FUNC_BRK_TJHG               32190  //条件换股查询                                                     
#define     FUNC_BRK_HGJY_ZQDM          32191  //换股建议-按证券代码                                              
#define     FUNC_BRK_HGJY_KHH           32192  //换股建议-按客户号                                                
#define     FUNC_BRK_GGHYXX             32193  //个股行业信息                                                     
#define     FUNC_BRK_GGJJ               32194  //个股聚焦                                                         
#define     FUNC_BRK_ZQHQ_JRZX          32195  //证券行情查询（今日咨询）                                         
#define     FUNC_BRK_ZSCX_JRZX          32196  //指数查询（今日咨询）                                             
                                                                                                                  
#define     FUNC_BRK_CYGPTZ             32220  //客户持有股票统计                                                 
#define     FUNC_BRK_KHZBCX             32221  //客户指标查询                                                     
#define     FUNC_BRK_KHCX_ZQDM          32222  //按股票查找客户                                                   
#define     FUNC_BRK_CCGPZD          32223  //持仓股票诊断                                                        
#define     FUNC_BRK_CCGPZS          32224  //持仓股票诊断   
#endif                                                     
                                                                                                                  
                                                                                                                  
                                                                                                                  