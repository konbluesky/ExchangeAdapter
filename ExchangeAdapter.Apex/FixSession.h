
#include "fixdef.h"
#include "FixApi.h"

namespace tradeadapter
{
	namespace apex
	{
		struct ApexInitParamA;

		//顶点的一些有用的api不在FixApi里定义了(但在pdf文档和dll里有，因此获取并使用之)
		enum FunctionID
		{
			B_FUN_SECU_ENTRUST_TRADE = FUN_SECU_ENTRUST_TRADE,//         204501  //股票买卖委托
			B_FUN_SECU_ENTRUST_WITHDRAW = FUN_SECU_ENTRUST_WITHDRAW,	//     204502  //股票委托撤单
			B_FUN_QUOTE_GETHQ = FUN_QUOTE_GETHQ,			// 	   104101  //证券行情查询
			B_FUN_SECU_LIST_GDHBYKHH = FUN_SECU_LIST_GDHBYKHH,//         304001  //客户股东号查询
			B_FUN_CUSTOM_CHKTRDPWD = FUN_CUSTOM_CHKTRDPWD,		 //    190101  //交易密码效验
			B_FUN_SECU_LIST_HOLDSTOCK = FUN_SECU_LIST_HOLDSTOCK,//         304101  //客户持仓查询
			B_FUN_SECU_LIST_HZCJ = FUN_SECU_LIST_HZCJ,//             304111  //客户实时成交汇总查询
			B_FUN_SECU_GET_ENTRUSTINFO = FUN_SECU_GET_ENTRUSTINFO, //       304116  //查询指定委托号的委托详细信息
			B_FUN_SECU_LIST_SSCJ = FUN_SECU_LIST_SSCJ,//             304109  //客户实时成交查询
			B_FUN_SECU_LIST_FBCJ = FUN_SECU_LIST_FBCJ,//             304110  //客户分笔成交查询
			B_FUN_CUSTOM_GET_CUSTINFO = FUN_CUSTOM_GET_CUSTINFO,//        302001  //查询指定客户基本信息
			B_FUN_ACCOUNT_LIST_ZJXXBYKHH = FUN_ACCOUNT_LIST_ZJXXBYKHH,//     303002  //按客户号查询资金
		};


		class TAIAPI CFixSession
		{
		public:
			CFixSession(HANDLE_CONN m_Conn);
			virtual ~CFixSession();

			void BeginJob(const ApexInitParamA& info, FunctionID id);
			void AddField(long id, long val);
			void AddField(long id, const char* val);
			void AddField(long id, double val);
			bool CommitJob();

			long GetResultRowNum();
			long GetResultLong(long id, int rowid = -1);
			double GetResultDouble(long id, int rowid = -1);
			std::string GetResultString(long id, int rowid = -1);
			ErrorInfo GetRunError();
		private:
			HANDLE_SESSION m_ses;
			BOOL m_ret;
		};
	}
}