#pragma once


/*
JYS	交易所	
SH		上海
SZ		深圳
HB		沪B
SB		深B
TA		三板A股
TU		三板B股

================
交易类别	说明
1	买入
2	卖出

*/
class UApexHelper
{
public:

	std::string GetBuySell(BuySell ope);

	static std::string GetStockExchange(tstring code);

};
