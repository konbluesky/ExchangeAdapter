#include "StdAfx.h"
#include "ucodehelper.h"
#include "ustringutils.h"
#include "tinyxml.h"
#include "tconfigsetting.h"


#include "umaths.h"
#include "ApexHelper.h"
#include "ucomhelper.h"
#include "ApexTradeAdapter.h"
#include "lib\FixApi.h"
#include "stdio.h"
#include "LogHelper.h"


#include "FixSession.h"
#pragma comment(lib,"comsuppw.lib")
#pragma warning(disable : 4482)
namespace tradeadapter
{
	namespace apex
	{
		CApexTradeAdapter::CApexTradeAdapter(void) : m_Conn(NULL)
		{
		}

		CApexTradeAdapter::~CApexTradeAdapter(void)
		{
		}

		bool CApexTradeAdapter::Initialize(void* param /*= NULL */)
		{
			ApexInitParam tparam = *(ApexInitParam*)param;
			m_param = ApexInitParamA::FromApexInitParam(tparam);
			bool ret = false;
			//只能调用一次
			BOOL onlycallonce = Fix_Initialize();
			if (!onlycallonce)
			{
				
				SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,-1, _T("初始化接口失败")));
				ret = false;
				return ret;
			}


			WriteLog(_T("log"), LTALL, _T("初始化Ok"));


			ret = this->_connect(m_param.Addr.c_str(), m_param.Port, m_param.ApexUser.c_str(), m_param.ApexPwd.c_str(), m_param.Timeout);

			if (!ret)
			{
				UnInitialize();
				ret = false;
				return ret;
			}


			ret = UApexHelper::InitializeDB(m_param.dsn.c_str(), m_param.dsnusr.c_str(), m_param.dsnpwd.c_str());
			ret = UApexHelper::LoadTodayEntrustOrdersFromDB(m_param.ClientID.c_str());


			GetBasicUserInfo1();
			return true;

		}



		void CApexTradeAdapter::UnInitialize()
		{
			if (m_Conn != NULL)
			{
				Fix_Close(m_Conn);
				m_Conn = NULL;

			}
			//只能调用一次
			static BOOL ret = Fix_Uninitialize();
			if (ret)
			{
				WriteLog(_T("log"), LTALL, _T("结束Ok"));
			}
			else
			{
				SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,-1,_T("结束接口失败")));
			}
		}


		//注：
		bool CApexTradeAdapter::_connect(const char* addr, int port, const char* usr, const char* pwd, int timeout/*=1000 */)
		{

			if (m_Conn)
			{
				Fix_Close(m_Conn);
				m_Conn = NULL;
			}

			char connetstr[1024] = { 0 };
			sprintf_s(connetstr, "%s@%d/tcp", addr, port);
			std::cout << "connected to : " << connetstr << std::endl;


			m_Conn = Fix_Connect(connetstr, usr, pwd, timeout);

			if (m_Conn == NULL)
			{
				SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter, -1, _T("连接服务器failed")));
				return false;
			}

			m_Initialzed = true;
			return true;
		}




		bool CApexTradeAdapter::IsInitialized()
		{
			return m_Initialzed;
		}

		bool CApexTradeAdapter::InitializeByXML(const TCHAR* fpath)
		{
			TConfigSetting cfg;
			cfg.LoadConfig(fpath);

			ApexInitParam param;

			param.AdapterName = U82TS(cfg["configuration"]["adaptername"].EleVal().c_str());
			param.AdapterVer = U82TS(cfg["configuration"]["adapterver"].EleVal().c_str());
			param.AutoReconnect = cfg["configuration"]["autoreconnect"].EleVal() == "true";
			param.Addr = U82TS(cfg["configuration"]["addr"].EleVal().c_str());
			param.Port = UMaths::ToInt32(cfg["configuration"]["port"].EleVal().c_str());
			param.ApexUser = U82TS(cfg["configuration"]["apexuser"].EleVal().c_str());
			param.ApexPwd = U82TS(cfg["configuration"]["apexpwd"].EleVal().c_str());
			param.Timeout = UMaths::ToInt32(cfg["configuration"]["timeout"].EleVal().c_str());
			param.ClientID = U82TS(cfg["configuration"]["clientid"].EleVal().c_str());
			param.Password = U82TS(cfg["configuration"]["password"].EleVal().c_str());
			param.YYB = U82TS(cfg["configuration"]["yyb"].EleVal().c_str());
			param.gydm = U82TS(cfg["configuration"]["gydm"].EleVal().c_str());
			param.wtfs = U82TS(cfg["configuration"]["wtfs"].EleVal().c_str());
			param.fbdm = U82TS(cfg["configuration"]["fbdm"].EleVal().c_str());
			param.mbyyb = U82TS(cfg["configuration"]["mbyyb"].EleVal().c_str());
			param.dsn = U82TS(cfg["configuration"]["dsn"].EleVal().c_str());
			param.dsnusr = U82TS(cfg["configuration"]["dsnusr"].EleVal().c_str());
			param.dsnpwd = U82TS(cfg["configuration"]["dsnpwd"].EleVal().c_str());

			bool ret = this->Initialize(&param);
			return ret;
		}

		bool CApexTradeAdapter::EntrustOrder(Order order, OrderID* pOutID)
		{
			*pOutID = tradeadapter::InvalidOrderId;
			CFixSession sess(m_Conn);
			sess.BeginJob(m_param, FunctionID::B_FUN_SECU_ENTRUST_TRADE);
			sess.AddField(FID_KHH, m_param.ClientID.c_str());//客户号
			sess.AddField(FID_JYMM, m_param.Password.c_str()); //交易密码
			sess.AddField(FID_JMLX, (long)2);//'2';	--0，简单加密 2-明文  1-不支持,注文档里此类型为n


			std::string gdh = UApexHelper::SelectGDHFromProduct(order.ContractCode, m_param.Gdh_SH, m_param.Gdh_SZ);
			sess.AddField(FID_GDH, gdh.c_str());//股东号

			std::string jys = UApexHelper::GetJYSByContractCode(order.ContractCode);
			sess.AddField(FID_JYS, jys.c_str()); //交易所
			std::string stockcode = TS2GB(order.ContractCode);
			sess.AddField(FID_ZQDM, stockcode.c_str()); //证券代码
			long bs = UApexHelper::GetJYLBFromBuySell(order.OpeDir);
			sess.AddField(FID_JYLB, bs); //交易类别：1买入  2 卖出其他请参考交易类别说明


			sess.AddField(FID_WTSL, (long)order.Amount); //委托数量

			sess.AddField(FID_WTJG, (double)order.Price); // 委托价格，对普通回购表示年利率，对报价回购提前终止(36)可以不送
			sess.AddField(FID_DDLX, (long)0); //订单类型:0限价,1最优5档剩余撤单,2最优5档剩下转限价等，FLDM=DDLX
			sess.AddField(FID_WTFS, (long)32);//          680  //委托方式  DT：C  32		网上交易



			if (sess.CommitJob())
			{
				int code = sess.GetResultLong(FID_CODE);
				std::string msg = sess.GetResultString(FID_MESSAGE);


				std::cout << code << "   " << msg << std::endl;
				long wth = sess.GetResultLong(FID_WTH); //委托号

				std::cout << "wth:" << wth << std::endl;
				std::string sbwth = sess.GetResultString(FID_SBWTH);
				std::cout << "SBWTH" << sbwth << std::endl;


				if (wth != 0)
				{

				}
				else
				{
					wth = -1;
				}
				UApexHelper::SaveOrderInfo(wth, order, jys, gdh);

				*pOutID = (int)wth;
				return true;
			}
			else
			{
				sess.GetRunError();
				return false;
			}
		}



		//#ifndef FID_ZXSZ 
#pragma message("this id is not defined in apex current version")
#define FID_ZXSZ 760
		//#endif

		bool CApexTradeAdapter::QueryPostions(std::list<PositionInfo>* pPoss, const TCHAR* pstock)
		{
			pPoss->clear();
			CFixSession sess(m_Conn);
			sess.BeginJob(m_param, B_FUN_SECU_LIST_HOLDSTOCK);
			sess.AddField(FID_KHH, m_param.ClientID.c_str());
			sess.AddField(FID_EXFLG, (long)1);
			if (sess.CommitJob())
			{
				int num = sess.GetResultRowNum();
				for (int i = 0; i < num; ++i)
				{
					PositionInfo info;

					std::string zqdm = sess.GetResultString(FID_ZQDM, i);
					std::string zqmc = sess.GetResultString(FID_ZQMC, i);
					std::string gdh = sess.GetResultString(FID_GDH, i);
					long kmcsl = sess.GetResultLong(FID_KMCSL, i);
					double zxsz = sess.GetResultDouble(FID_ZXSZ, i);
					long zqsl = sess.GetResultLong(FID_ZQSL, i);

					info.enable_amount = kmcsl;
					info.stock_account = GB2TS(gdh);
					info.code = GB2TS(zqdm);
					info.stock_name = GB2TS(zqmc.c_str());
					info.current_amount = zqsl;
					info.market_value = zxsz;

					double zxj = GetStockInfo(GB2TS(zqdm).c_str()).zdjg;//sess.GetResultDouble(FID_ZXJ);
					info.last_price = zxj;

					pPoss->push_back(info);

				}
			}
			else
			{
				sess.GetRunError();
				return false;
			}
			return true;
		}

		CApexTradeAdapter::stockinfo CApexTradeAdapter::GetStockInfo(const TCHAR* code)
		{
			CApexTradeAdapter::stockinfo info;
			CFixSession sess(m_Conn);
			sess.BeginJob(m_param, B_FUN_QUOTE_GETHQ);

			sess.AddField(FID_ZQDM, TS2GB(code));
			std::string jys = UApexHelper::GetJYSByContractCode(code);
			sess.AddField(FID_JYS, jys.c_str());
			if (sess.CommitJob())
			{
				double zdjg = sess.GetResultDouble(FID_ZDBJ);
				info.zdjg = zdjg;
			}
			return info;
		}

		//TODO, apex's doc mybe wrong ,which is not same as lua's source code
		bool CApexTradeAdapter::CancelOrder(OrderID orderid)
		{
			CFixSession sess(m_Conn);
			sess.BeginJob(m_param, FunctionID::B_FUN_SECU_ENTRUST_WITHDRAW);
			sess.AddField(FID_KHH, m_param.ClientID.c_str());//客户号

			Order orderinfo = UApexHelper::GetTodayOrderInfoByID(m_param.ClientID.c_str(), orderid);

			std::string   gdh = UApexHelper::SelectGDHFromProduct(orderinfo.ContractCode, m_param.Gdh_SH, m_param.Gdh_SZ);

			sess.AddField(FID_GDH, gdh.c_str());//股东号

			std::string jys = UApexHelper::GetJYSByContractCode(orderinfo.ContractCode);

			sess.AddField(FID_JYS, jys.c_str()); //交易所

			sess.AddField(FID_WTH, (long)orderid); //委托号
			sess.AddField(FID_JYMM, m_param.Password.c_str()); //交易密码
			sess.AddField(FID_JMLX, (long)2);//'2';	--0，简单加密 2-明文  1-不支持,注文档里此类型为n






			if (sess.CommitJob())
			{
				int code = sess.GetResultLong(FID_CODE);
				std::string msg = sess.GetResultString(FID_MESSAGE);
				if (code == -204502014) //已经全部成交了
				{
					return false;
				}

				//TODO,here need check the output code .

				int wth = sess.GetResultLong(FID_WTH);
				std::cout << code << "   " << msg << std::endl;
				return true;
			}
			else
			{

				return false;
			}


		}

		bool CApexTradeAdapter::QueryOrder(OrderID id, int* pCount)
		{
			*pCount = 0;
			CFixSession sess(m_Conn);
			sess.BeginJob(m_param, FunctionID::B_FUN_SECU_GET_ENTRUSTINFO);
			Order orderinfo = UApexHelper::GetTodayOrderInfoByID(m_param.ClientID.c_str(), id);
			std::string jys = UApexHelper::GetJYSByContractCode(orderinfo.ContractCode);

			sess.AddField(FID_JYS, jys.c_str());
			sess.AddField(FID_WTH, (long)id);
			if (sess.CommitJob())
			{
				int cjsl = sess.GetResultLong(FID_CJSL);		//成交数量
				double cjjg = sess.GetResultDouble(FID_CJJG); //成交价格

				double wtjg = sess.GetResultDouble(FID_WTJG);//委托价格				
				std::string zqmc = sess.GetResultString(FID_ZQMC);//
				std::string zqdm = sess.GetResultString(FID_ZQDM);//
				std::string wtsj = sess.GetResultString(FID_WTSJ);
				std::string msg = sess.GetResultString(FID_MESSAGE);
				int code = sess.GetResultLong(FID_CODE);

				std::cout << "code" << code << "    " << "msg" << msg << std::endl;

				std::cout << "CJSL" << cjsl << std::endl;

				*pCount = cjsl;
				return true;

			}


			return false;
		}



		bool CApexTradeAdapter::QueryDealedOrders(std::list<DealedOrder>* pres)
		{
			CFixSession sess(m_Conn);
			pres->clear();
			sess.BeginJob(m_param, FunctionID::B_FUN_SECU_LIST_FBCJ);//注：另外一个查
			sess.AddField(FID_KHH, m_param.ClientID.c_str());
			sess.AddField(FID_ROWCOUNT, (long)1000);
			if (sess.CommitJob())
			{
				int count = sess.GetResultRowNum();
				for (int i = 0; i < count; ++i)
				{
					std::string fid_rowcount = sess.GetResultString(FID_BROWINDEX, i);
					std::string jys = sess.GetResultString(FID_JYS, i);
					std::string gdh = sess.GetResultString(FID_GDH, i);

					int wth = sess.GetResultLong(FID_WTH, i);//委托号

					std::string sbwth = sess.GetResultString(FID_SBWTH, i);//申报委托号
					//int wth = sess.GetResultLong(FID_WTH); 
					std::string cxbz = sess.GetResultString(FID_CXBZ, i);//撤销标志，O表示委托，W表示撤单记录
					if (cxbz == "W")
						continue;  //这种是不能成交的单子。内部已经撤销。

					std::string zqdm = sess.GetResultString(FID_ZQDM, i);//证券代码

					std::string zqmc = sess.GetResultString(FID_ZQMC, i);//证券名称

					int wtlb = sess.GetResultLong(FID_WTLB, i);// 交易类别，见数据字典1	买入				2	卖出

					int cjsl = sess.GetResultLong(FID_CJSL, i);//成交数量

					double cjjg = sess.GetResultDouble(FID_CJJG, i);//成交价格

					double cjje = sess.GetResultDouble(FID_CJJE, i);//成交金额

					std::string cjsj = sess.GetResultString(FID_CJSJ, i);//成交时间
					std::string cjbh = sess.GetResultString(FID_CJBH, i); //成交编号



					DealedOrder order;
					//	order.date = UTimeUtils::GetNowDate();
					order.business_time = UTimeUtils::FormTimeString(GB2TS(cjsj).c_str());
					//order.bs_name
					order.business_price = cjjg;
					order.business_amount = cjsl;
					order.entrust_bs = UApexHelper::ToBuySell(wtlb);
					order.stock_code = GB2TS(zqdm);
					order.entrust_no = wth;//UMaths::ToInt32(sbwth);
					order.serial_no = UMaths::ToInt32(cjbh);
					order.stock_name = GB2TS(zqmc);
					//				order.exchange_type = GB2TS(jys);
					order.stock_account = GB2TS(gdh);
					order.report_no = UMaths::ToInt32(sbwth);


					pres->push_back(order);
					//order.bs_name;
					//todo chh

				}
				return true;
			}
			return false;
		}

		bool CApexTradeAdapter::QueryAccountInfo(BasicAccountInfo* info)
		{
			_getaccountname(info);
			_getaccountmoney(info);



			return true;
		}



		void CApexTradeAdapter::_getaccountname(BasicAccountInfo* info)
		{
			CFixSession sess(m_Conn);
			sess.BeginJob(m_param, FunctionID::B_FUN_CUSTOM_GET_CUSTINFO);
			sess.AddField(FID_KHH, m_param.ClientID.c_str());
			if (sess.CommitJob())
			{
				std::string khxm = sess.GetResultString(FID_KHXM);
				std::string yyb = sess.GetResultString(FID_YYB);

				info->ClientName = GB2TS(khxm);

			}
		}



		


		void CApexTradeAdapter::GetBasicUserInfo2()
		{
			CFixSession sess(m_Conn);
			sess.BeginJob(m_param, FunctionID::B_FUN_CUSTOM_CHKTRDPWD);
			sess.AddField(FID_KHH, m_param.ClientID.c_str());
			sess.AddField(FID_JYMM, m_param.Password.c_str());
			if (sess.CommitJob())
			{

			}
		}

		void CApexTradeAdapter::GetBasicUserInfo1()
		{
			CFixSession sess(m_Conn);
			sess.BeginJob(m_param, FunctionID::B_FUN_SECU_LIST_GDHBYKHH);
			sess.AddField(FID_KHH, m_param.ClientID.c_str());
			if (sess.CommitJob())
			{
				int rows = sess.GetResultRowNum();
				std::string gdh = sess.GetResultString(FID_GDH);
				std::string jys = sess.GetResultString(FID_JYS);
				if (jys == "SH")
				{
					m_param.Gdh_SH = gdh;
				}
				if (jys == "SZ")
				{
					m_param.Gdh_SZ = gdh;
				}
			}
		}

		void CApexTradeAdapter::_getaccountmoney(BasicAccountInfo* info)
		{
			CFixSession sess(m_Conn);
			sess.BeginJob(m_param, FunctionID::B_FUN_ACCOUNT_LIST_ZJXXBYKHH);
			sess.AddField(FID_KHH, m_param.ClientID.c_str());
			sess.AddField(FID_BZ, "RMB");
			if (sess.CommitJob())
			{
				std::string zjzh = sess.GetResultString(FID_ZJZH);
				std::string bz = sess.GetResultString(FID_BZ);
				double zjye = sess.GetResultDouble(FID_ZHYE);
				double djje = sess.GetResultDouble(FID_DJJE);

				info->FundAccount = GB2TS(zjzh);
				info->CurrentBalance = zjye;
				info->EnableBalance = zjye - djje;

			}
		}

		bool CApexTradeAdapter::QueryEntrustInfos(std::list<EntrustInfo>* pinfo)
		{
			SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter, -1, _T("Apex QueryEntrustInfos() not implement")));
			return false;
		}

		tstring CApexTradeAdapter::GetName()
		{
			return _T("Apex Trade Adapter");
		}


		tradeadapter::apex::ApexInitParamA ApexInitParamA::FromApexInitParam(ApexInitParam input)
		{
			ApexInitParamA ret;
			ret.AdapterName = TS2GB(input.AdapterName);
			ret.AdapterVer = TS2GB(input.AdapterVer);
			ret.AutoReconnect = input.AutoReconnect;
			ret.Addr = TS2GB(input.Addr);
			ret.Port = input.Port;
			ret.ApexUser = TS2GB(input.ApexUser);
			ret.ApexPwd = TS2GB(input.ApexPwd);
			ret.Timeout = input.Timeout;
			ret.ClientID = TS2GB(input.ClientID);
			ret.Password = TS2GB(input.Password);
			ret.YYB = TS2GB(input.YYB);


			ret.gydm = TS2GB(input.gydm);
			ret.wtfs = TS2GB(input.wtfs);

			ret.fbdm = TS2GB(input.fbdm);
			ret.mbyyb = TS2GB(input.mbyyb);

			ret.dsn = TS2GB(input.dsn);
			ret.dsnusr = TS2GB(input.dsnusr);
			ret.dsnpwd = TS2GB(input.dsnpwd);


			return ret;
		}

	}
}