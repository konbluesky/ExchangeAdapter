#pragma once
#include "stdafx.h"
#include "ExchangeAdapterDef.h"
#include "IOrderService.h"
#include <map>

namespace tradeadapter
{
	namespace apex
	{
		class  TAIAPI UApexHelper
		{
		public:
			static Order GetTodayOrderInfoByID(const char* clientid, int orderid);

			static std::string SelectGDHFromProduct(tstring code, std::string sh, std::string sz);

			static std::string GetJYSByContractCode(const tstring& code);
			
			static void SaveOrderInfo(int wth, Order order, std::string jys, std::string gdh);
			
			static bool InsertEntrustOrderInfo(int Date, int Time, const char* clientID, const char* StockAccount, const char* ContractCode, int buysell, double Price, int Amount, const char* Comment, int orderid, int Status);
			
			static void InsertErrorLog(int Date, int Time, const char* ExProvider, int errcode, const char* errmsg);
			
			static long GetJYLBFromBuySell(EBuySell ope);
			
			static EBuySell ToBuySell(int JYLB);

			static bool InitializeDB(const char* dsn, const char* usr, const char* pwd);
			
			static bool LoadTodayEntrustOrdersFromDB(const char* clietid);
		private:

#pragma warning(disable :4251)
			//注：下面两个变量是相关联的，有可能服务器隔夜未重启，因此每次取GetTodayEntrustOrdersFromDB()都要判断时间
			static std::map<OrderID, Order>  m_todayOrder;
			static int m_today;
		};
	}
}