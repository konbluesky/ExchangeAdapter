#pragma once
#include <string>
#include <vector>
#include <algorithm>
#include <tchar.h>
#include "ErrorDefinition.h"
#include "ITradeAdapter.h"
#include "IOrderService.h"
#include "HsQhHelper.h"
#include "ustringutils.h"
#include "threadlock.h"
USING_LIBSPACE

namespace tradeadapter
{


	namespace hsqh
	{	

		enum OrderStatus
		{
			Failed,
			Online,
			PartDealed,
			CancelOnline,
			AllDealed,
			
		};
		class  CHSTradeAdapter : ITradeAdapter
		{
		public:
			CHSTradeAdapter();

			virtual bool Initialize(void* InitParam);
			virtual bool InitializeByXML(const TCHAR* fname);

			virtual bool QueryOrder(OrderID id, int* pCount);


			virtual bool EntrustOrder(Order order, OrderID* pOutID);

			virtual	bool CancelOrder(OrderID orderid);

			bool _cancelorder( OrderID orderid );

			virtual void UnInitialize();

			bool _connect(const wchar_t* addr, int port);
			std::pair<bool,Order> GetOrderInfo(OrderID id);



			virtual bool QueryPostions(std::list<PositionInfo>* pPoss ,const TCHAR* pstock);






			struct StockInfo
			{
				hsstring stock_code;
				hsstring stock_name;
				hsstring exchange_type;
				hsstring stock_type;
			};
			StockInfo GetStockInfo(const TCHAR* stock_code);

			bool ReConnect();
			//如果不成功则返回0，如果成功，则返回成交量
			bool GetDealedOrderInfo(std::list<DealedOrder>* pinfos ,CONST TCHAR* stockid = _T("") ,OrderID id = -1);
			std::pair<int,OrderStatus>  GetOnlinedOrderInfo( OrderID id );	

			int GetOrderTransactionQueries( OrderID id );	

			ResultSet		QuerySystemStatus();		
			ResultSets	QueryAccountCapital(void);		





		private:	
			void BeginJob(int jobid);
			void AddFieldValue(const wchar_t* pKey,const wchar_t* pVal);
			void AddFieldValue(const wchar_t* pKey, const char* pVal);
			bool CommitJob();
			hsstring GetFieldValW(const wchar_t* key);
			std::string GetFieldValA(const wchar_t* key);

			tstring GetFieldValT(const wchar_t* key);


			StockInfo GetStockInfoFromOrderID( OrderID id );
			

			virtual bool QueryDealedOrders(std::list<DealedOrder>* pres);
			bool GetDealedOrderInfoVithPos(std::list<DealedOrder>* pinfos, CONST TCHAR* stockcode,OrderID id ,int iPos);

			void QueryAccountBasicInfo1();
			void QueryAccountBasicInfo2();
			void QueryAccountBasicInfo3();

			virtual bool QueryAccountInfo( BasicAccountInfo* info );

			


		private:		
			void _updatetLastError();

			virtual bool IsInitialized() ;

			virtual bool QueryEntrustInfos( std::list<EntrustInfo>* pinfo );

			

			virtual tstring GetName();

		private:
#pragma warning(disable : 4251)	

			//注这个信息是从系统查出来的。
			BasicAccountInfo m_accountInfo;
			threadlock::multi_threaded_local m_lock;
			HSIniParamW m_usrinfo;
			std::map<int,Order> m_idorders;
			ICommPtr m_hsconn;
			StockInfo m_laststockinfo; 
			std::vector<std::pair<std::wstring,std::wstring> > m_inputdatas;	
			
			bool m_conneted;
		};
	};
};