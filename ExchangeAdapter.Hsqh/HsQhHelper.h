#ifndef __HSQH_HELPER_H__
#define __HSQH_HELPER_H__

#include <iostream>
#include <string>
#include <map>
#include <list>
#include "ustringutils.h"
#include "IOrderService.h"
USING_LIBSPACE


namespace tradeadapter
{


	namespace hsqh
	{

		//comment: hsqh is based on com, which means all char/string  type is widechar
		typedef wchar_t hschar;
		typedef std::wstring hsstring;

		typedef std::map<std::wstring,std::wstring> ResultSet;
		std::ostream& operator << (std::ostream& ,const  ResultSet& res );
		typedef std::list<ResultSet> ResultSets;



		ETradeUnit FromHSStringToTradeUnit(const tstring  pinfo);



		hsstring BSToHSString(EBuySell ope);



		enum MoneyType
		{
			人民币 = '0',
			美圆  ='1' ,
			港币 = '2' 	
		};

		//注
		enum Exchange_Type
		{
			前台未知交易所  ='0',
			上海=	'1'  ,
			深圳 = '2' ,
			金交所 = '5',
			创业板 = '8' ,
			上海B = 'D',
			深圳B ='H',
		};

		enum FunctionCode
		{
			INQUERY_SYSTEM_STATUS = 100,//系统登录（不支持）
			GET_MARKETINFO = 105,//查询股票市场
			ENTRUST_STOCK=300,//委托股票
			USER_CONFIRM = 200,//客户登录校验

			//这个才是真正买卖股票的功能
			ENTRUST_CONFRIM=302, //委托确认
			ENTRUST_CANCEL=304,  //委托撤单
			INQUIRY_ENTRUST_STATUS=401, //查询委托
			//查询成交单的内容
			INQUIRY_KNOCKDOWN=402, // 查询成交
			INQUIRY_STOCK=403,	   // 查询股票
			INQUIRY_CAPTIAL=405,		// 查询资金

			// 期货相关
			FUTURES_COMMISSION_CONFIRM = 1004,			// 期货委托确认
			FUTURES_RETREATING_FORM = 1005,				// 期货委托撤单
			FUTURES_TRADING_ACCOUNT_QUERIES = 1500,		// 期货交易帐号查询
			FUTURES_POSITIONS_QUERY = 1503,				// 期货持仓查询
			FUTURES_DELEGATE_QUERIES_DAY = 1504,		// 期货当日委托查询
			FUTURES_TRANSACTION_QUERIES_DAY = 1506,		// 期货当日成交查询
			FUTURE_CLIENTS_QUERY = 1508,				// 期货客户资金查询
			FUTURES_CONTRACTS_MARKET_QUERY = 1509,		// 期货合约行情查询
			FUTURES_HISTORY_TRANSACTIONS_QUERY = 1531,	// 期货历史交易流水查询
		};


		enum entrust_way//：委托方式
		{
			自助委托 = '0',
			电话委托 = '1',
			驻留委托 = '2',
			远程委托 = '3',
			柜台委托 ='4',	
			漫游委托  ='5', 
			分支委托 ='6',
			internet委托 = '7',
		};


		//'1' -- 买入  	'2' -- 卖出 		对于期货 1 开仓	2 平仓//
		enum Entrust_bs 
		{
			buy = 1,
			sell = 2,
		};


		//'1' -- 买入  	'2' -- 卖出 		对于期货 1 开仓	2 平仓

		std::ostream& GetLogStream();




	};
}
#endif