#include "stdafx.h"
#include "ExchangeAdapterDef.h"
#include <iostream>

#pragma warning(disable : 4996)
#pragma warning(disable : 4251)
#pragma warning(disable : 4390)
#pragma warning(disable : 4018)
#pragma warning(disable : 4482)

#include "texceptionex.h"
#include "LogHelper.h"
#include "tconfigsetting.h"
#include "tinyxml.h"
#include "umaths.h"
#include "uinteger.h"
#include "UCodeHelper.h"
#include "HSCOMTradeAdapter.h"

#define AUTOGARD()   threadlock::autoguard<threadlock::multi_threaded_local>  _guard_(&m_lock)

USING_LIBSPACE


#define CHECK_RESULT(ret,expt,msg) if(ret != expt) { SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,-1,msg);  goto LEXIST;}
#define LOG(msg)   
namespace tradeadapter
{


namespace hsqh
{
	void CHSTradeAdapter::AddFieldValue( const wchar_t* pKey,const wchar_t* pVal )
	{
		m_inputdatas.push_back(std::make_pair<std::wstring,std::wstring>(pKey,pVal));
	}

	void CHSTradeAdapter::AddFieldValue(const wchar_t* pKey, const char* pVal)
	{
		auto val =  UCodeHelper::ToWS(pVal);
		m_inputdatas.push_back(std::make_pair<std::wstring, std::wstring>(RMOVE(pKey), RMOVE(val)));
	}


	std::string CHSTradeAdapter::GetFieldValA( const wchar_t* key )
	{		
		try
		{
			_bstr_t  data = m_hsconn->FieldByName(key);
			char* aptr = (char*) data;
			return aptr == NULL ? "" : aptr;
		}
		catch (...)
		{
			return "";
		}
	}

	hsstring CHSTradeAdapter::GetFieldValW(const  wchar_t* key )
	{		
		try
		{
			_bstr_t		data	= m_hsconn->FieldByName(key);
			wchar_t*	ptr		= (wchar_t*) data;
			return ptr == NULL ? L"" : ptr;
		}
		catch (...)
		{
			return L"";
		}		
	}
	tstring CHSTradeAdapter:: GetFieldValT(const wchar_t* key)
	{
#if defined( UNICODE ) || defined(_UNICODE)
		return GetFieldValW(key);
#else
		return GetFieldValA(key);
#endif // UNICODE ) || defined(_UNICODE)

	}

	void CHSTradeAdapter::_updatetLastError()
	{
		long code = m_hsconn->GetErrorNo();
		
		if(code )
		{	
			_bstr_t msg = m_hsconn->GetErrorMsg();

			const TCHAR* ret = (const TCHAR*)msg;

			
			SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter, code, ret));
			
		}
		
	}



	bool CHSTradeAdapter::_connect(const wchar_t* addr,int port )
{
		AUTOGARD();

		HRESULT ret = S_FALSE;
		long runret = -1;

		//1.Create instance
		ret = m_hsconn.CreateInstance(__uuidof(Comm));
		//CHECK_RESULT(ret,S_OK, _T("m_hsconn.CreateInstance(__uuidof(Comm)); failed") ) ;

		//2.it's clumzy
		try
		{
			ret = m_hsconn->Create();
		}
		catch(...)
		{
		}
		//3.get ip address & port
		m_usrinfo.addr = UCodeHelper::ToWS(addr);
		//WriteDetailLog(_T("debug"),LTALL,(m_usrinfo.addr.c_str()));;
		m_usrinfo.port = port;

		//4. connect server
		runret = m_hsconn->ConnectX(1,m_usrinfo.addr.c_str(),m_usrinfo.port,0,L"",0);		
//		CHECK_RESULT(runret,0, (_T("m_hsconn->ConnectX; failed") ) );

		if(runret != 0)
		{
			_updatetLastError();
		}

		if(runret == 0)
		{
			m_conneted = true;
		}
		return runret ==0;
	}

	bool CHSTradeAdapter::Initialize( void* pParam /*= NULL*/ )
	{
		HSIniParam* input = (HSIniParam *)pParam;
		m_usrinfo = HSIniParamW::FromHSIniParam(*input);

		//////////////////////////////////////////////////////////////////////////
		// 获取用户名和密码
		HINSTANCE hInst;
		hInst = LoadLibraryA("ColleInterface.dll");
		typedef int (*ADDPROC)(HSIniParamW *);
		ADDPROC Show = (ADDPROC)GetProcAddress(hInst,"Show");
		if(!Show)
		{
		}
		Show(&m_usrinfo);
		FreeLibrary(hInst);

		//////////////////////////////////////////////////////////////////////////// 
		// 获取期货账号
		AUTOGARD();
		BeginJob(FUTURES_TRADING_ACCOUNT_QUERIES);
		AddFieldValue(L"version",			L"1");
		AddFieldValue(L"op_branch_no",		m_usrinfo.op_branch_no.c_str());
		AddFieldValue(L"op_entrust_way",	m_usrinfo.op_entrust_way.c_str());
		AddFieldValue(L"identity_type",		L"" );
		AddFieldValue(L"op_station",		m_usrinfo.op_station.c_str());	
		AddFieldValue(L"function_id",		L"1500" );
		AddFieldValue(L"branch_no",			m_usrinfo.sbranch_no.c_str());	
		AddFieldValue(L"fund_account",		m_usrinfo.fund_account.c_str() );
		AddFieldValue(L"password",			m_usrinfo.password.c_str() );
		AddFieldValue(L"request_num",		L"1000" );
		AddFieldValue(L"position_str",		L"" );
		CommitJob();

		if(m_hsconn->ErrorNo != 0 )
		{
			_updatetLastError();
			//return 
		}

		m_usrinfo.futures_account		= GetFieldValW(L"futures_account");
		//////////////////////////////////////////////////////////////////////////

		HRESULT ret = S_FALSE;
		ret = ::CoInitializeEx(NULL,COINIT_APARTMENTTHREADED);

		bool runret = this->_connect(m_usrinfo.addr.c_str(),m_usrinfo.port);
		//CHECK_RESULT(runret,true,(_T("连接服务器出错")));

		return runret;
	}

	void CHSTradeAdapter::UnInitialize()
	{	
		AUTOGARD();

		if(m_hsconn != NULL)
		{
			m_hsconn->DisConnect();
			m_hsconn = NULL;
		}
		m_conneted = false;
		::CoUninitialize();
	}

	CHSTradeAdapter::StockInfo CHSTradeAdapter::GetStockInfo( const TCHAR* stock_code )
	{

		AUTOGARD();
		// StockInfo m_laststockinfo; //for cache
		if(m_laststockinfo.stock_code == UCodeHelper::ToWS(stock_code))
		{
			return m_laststockinfo;
		}
		bool ret = false;

		BeginJob(GET_MARKETINFO);
		AddFieldValue(L"version",			L"1");
		AddFieldValue(L"op_entrust_way",	m_usrinfo.op_entrust_way.c_str());
		AddFieldValue(L"op_station",		m_usrinfo.op_station.c_str());	
		AddFieldValue(L"branch_no",			m_usrinfo.sbranch_no.c_str());	
		AddFieldValue(L"stock_code",		stock_code );
		CommitJob();

		if(m_hsconn->ErrorNo != 0 )
		{
			_updatetLastError();
			//return 
		}

		StockInfo info;
		info.stock_code		= UCodeHelper::ToWS(stock_code);
		info.stock_name		= GetFieldValW(L"stock_name");
		info.exchange_type	= GetFieldValW(L"exchange_type");		
		info.stock_type		= GetFieldValW(L"stock_type");
		m_laststockinfo		= info;

		return info;
	}

	//委托下单
	bool CHSTradeAdapter::EntrustOrder(Order order, OrderID* pOutID)
	{
		*pOutID = tradeadapter::InvalidOrderId;
		AUTOGARD();
		bool ret = false;
		BeginJob(FUTURES_COMMISSION_CONFIRM);

		AddFieldValue(L"version",		L"1");
		AddFieldValue(L"op_branch_no",	m_usrinfo.op_branch_no.c_str());
		AddFieldValue(L"op_entrust_way",m_usrinfo.op_entrust_way.c_str());
		AddFieldValue(L"l_op_code",		m_usrinfo.l_op_code.c_str());
		AddFieldValue(L"vc_op_password",m_usrinfo.vc_op_password.c_str());		
		AddFieldValue(L"identity_type",	L"");// 账户身份类别 1 调拨人 2 指令人 3 查询 默认不输入
		AddFieldValue(L"op_station",	m_usrinfo.op_station.c_str());
		AddFieldValue(L"function_id",	UCodeHelper::ToWS( UStrConvUtils::ToString(FUTURES_COMMISSION_CONFIRM)).c_str());
		AddFieldValue(L"branch_no",		m_usrinfo.sbranch_no.c_str());
		AddFieldValue(L"fund_account",	m_usrinfo.fund_account.c_str());
		AddFieldValue(L"password",		m_usrinfo.password.c_str());
		AddFieldValue(L"futu_exch_type",L"F4"); // 交易类别，没有相关说明，暂时填空
		AddFieldValue(L"futures_account",m_usrinfo.futures_account.c_str());
		AddFieldValue(L"stock_code",	UCodeHelper::ToWS( order.ContractCode).c_str());
		hsstring data =  BSToHSString(order.OpeDir);
		AddFieldValue(L"entrust_bs",data.c_str());	// 买卖方向
		int nKpbz = 2;
		if (order.openclose == EOpenClose::eOpen ) { nKpbz = 1; }
		AddFieldValue(L"futures_direction",UCodeHelper::ToWS(UStrConvUtils::ToString(nKpbz)).c_str());	// 开平方向
		int nKbbz = 1; // 默认是套保的
		if (order.hedge == EHedgeFlag::eSpeculation ) { nKbbz = 0; }
		AddFieldValue(L"hedge_type", UCodeHelper::ToWS(UStrConvUtils::ToString(nKbbz)).c_str());	// 套保标志
		AddFieldValue(L"entrust_amount",UStrConvUtils::ToString(order.Amount).c_str());
		AddFieldValue(L"entrust_price", UCodeHelper::ToWS(UStrConvUtils::ToString(order.Price)).c_str());
		CommitJob();

		if(m_hsconn->ErrorNo != 0 )
		{
			_updatetLastError();
			/*
			错误号：-52：“此股票不存在”
			-51：“未登记该交易所帐号”
			-64:“此股不能进行此类委托” 
			-66：“帐户状态不正常”
			-67：“您未作指定交易”
			-57：“可用金额不足”
			-58：“股票余额不足”
			-55：“数量不符合交易所规定”
			-56：“委托价格不符合交易所规定”
			-59：“闭市时间不能委托”
			-65：“此股票停牌，不能委托”    （如果停牌允许委托，不报错误）
			-61：“委托失败”                （其他错误）
			80520：“该资金帐号禁止周边交易”
			*/
			return false;			
		}

		ResultSet res;
		res[L"_下单"] = L"\n";
		//res[L"error_no"] = GetFieldValW(L"error_no");
		res[L"error_info"] = GetFieldValW(L"error_info");
		res[L"entrust_no"] = GetFieldValW(L"entrust_no");
		res[L"batch_no"] = GetFieldValW(L"batch_no");
		GetLogStream()<<res;

		auto no = res[L"entrust_no"];
		int order_no = UMaths::ToInt32(no.c_str());

		*pOutID =  order_no;
		return true;
	}

	

	//TODO 注：hsqh的撤单，返回的有待撤的状态，这个会使业务调用撤单函数后单子的状态未知，因此需要修改完善此函数，对于自动交易的
	//应该继续调用撤单的函数，知道这个返回的状态是明确的。
	bool CHSTradeAdapter::CancelOrder( OrderID orderid )
	{
		int n=0;

		_cancelorder(orderid);
		
		while(true)
		{
			if(	GetOnlinedOrderInfo(orderid).second != OrderStatus::CancelOnline) 
			{
				break;
			}
			::Sleep(1000);
			n++;
			if(n > 30)
			{
				break;
			}
		}
		return true;

	}



	bool CHSTradeAdapter::CommitJob()
	{
		HRESULT ret = S_FALSE;
		int cout = m_inputdatas.size();

		//default 1 row, 
		ret = this->m_hsconn->SetRange(cout,1);

		for (int i =0; i<m_inputdatas.size(); ++i)
		{
			this->m_hsconn->AddField(m_inputdatas[i].first.c_str());
		}

		for (int i=0; i<m_inputdatas.size(); ++ i)
		{
			this->m_hsconn->AddValue(m_inputdatas[i].second.c_str());
		}

		static long sendid = UTimeUtils::GetNowTime();

		m_hsconn->SenderId = sendid ++;

		ret = m_hsconn->Send();
		ret = m_hsconn->FreePack();
		ret = m_hsconn->Receive();
		return true;
	}



	void CHSTradeAdapter::BeginJob(int jobid)
	{
		HRESULT ret = S_FALSE;
		m_inputdatas.clear();
		if(m_hsconn == NULL)
		{
			GetLogStream()<<"m_hsconn == NULL"<<std::endl;
			throw TException(_T("com对象为空"));
		}
		ret = m_hsconn->SetHead(m_usrinfo.branch_no, jobid);
		//CHECK_RESULT(ret,S_OK, ( _T("called m_hsconn->SetHead(m_branch_no, jobid) failed" )));		


	}

	ResultSets CHSTradeAdapter::QueryAccountCapital(void)
	{
		// 期货客户资金查询
		AUTOGARD();
		BeginJob(FUTURE_CLIENTS_QUERY);
		AddFieldValue(L"version",		L"1");
		AddFieldValue(L"op_branch_no",	m_usrinfo.op_branch_no.c_str() );
		AddFieldValue(L"op_entrust_way",m_usrinfo.op_entrust_way.c_str());
		AddFieldValue(L"identity_type",	L"");
		AddFieldValue(L"op_station",	m_usrinfo.op_station.c_str());
		AddFieldValue(L"function_id",	L"1508");
		AddFieldValue(L"branch_no",		m_usrinfo.sbranch_no.c_str());
		AddFieldValue(L"fund_account",	m_usrinfo.fund_account.c_str());
		AddFieldValue(L"password",		m_usrinfo.password.c_str());
		AddFieldValue(L"money_type",	L"0");
		CommitJob();

		ResultSets ret;
		while (m_hsconn->Eof == 0)
		{
			ResultSet res;

			res[L"money_type"] = GetFieldValW(L"money_type");
			res[L"current_balance"] = GetFieldValW(L"current_balance");
			res[L"enable_balance"] = GetFieldValW(L"enable_balance");
			res[L"fetch_balance"] = GetFieldValW(L"fetch_balance");
			res[L"asset_balance"] = GetFieldValW(L"asset_balance");
			res[L"pre_interest"] = GetFieldValW(L"pre_interest");

			ret.push_back(res);

			m_hsconn->MoveBy(1);
		}
		return ret;

	}

	ResultSet CHSTradeAdapter::QuerySystemStatus()
	{
		AUTOGARD();
		BeginJob(INQUERY_SYSTEM_STATUS);
		AddFieldValue(L"version",			L"1");
		AddFieldValue(L"op_entrust_way",	m_usrinfo.op_entrust_way.c_str());
		AddFieldValue(L"op_station",		m_usrinfo.op_station.c_str());
		CommitJob();

		ResultSet res;
		if (m_hsconn->Eof == 0)
		{
			res[L"_查询系统状态"] = L"\n";
			res[L"init_date"] = GetFieldValW(L"init_date");
			res[L"sys_status"] = GetFieldValW(L"sys_status");
			res[L"sys_status_name"] = GetFieldValW(L"sys_status_name");			
			GetLogStream()<<res;
		}
		return res;
	}

	bool CHSTradeAdapter::InitializeByXML( const TCHAR* fname )
	{
		TConfigSetting cfgsetting ;
		bool ret = cfgsetting.LoadConfig(fname);
		if(!ret)
		{
			std::cout<<"无法加载配置文件，是否是当前目录"<<std::endl;
			return ret;
		}
		HSIniParam input;


		std::string ipaddr = cfgsetting["configuration"]["saddressip"].EleVal();
		input.addr = UCodeHelper::ToTS(ipaddr);

		std::string ipport = cfgsetting["configuration"]["iport"].EleVal();
		input.port = UMaths::ToInt32(UCodeHelper::ToWS(ipport).c_str());

		std::string sbranchno = cfgsetting["configuration"]["sbranchno"].EleVal();
		input.sbranch_no = UCodeHelper::ToTS(sbranchno);

		std::string usr = cfgsetting["configuration"]["susr"].EleVal();
		input.usr = UCodeHelper::ToTS(usr);

		std::string password = cfgsetting["configuration"]["spwd"].EleVal();
		input.password = UCodeHelper::ToTS(password);

		input.branch_no = UMaths::ToInt32(m_usrinfo.sbranch_no.c_str());		

		std::string op_station = cfgsetting["configuration"]["op_station"].EleVal();
		input.op_station = UCodeHelper::ToTS(op_station);

		std::string op_entrust_way = cfgsetting["configuration"]["op_entrust_way"].EleVal();
		input.op_entrust_way = UCodeHelper::ToTS(op_entrust_way);

		std::string fund_account = cfgsetting["configuration"]["fund_account"].EleVal();
		input.fund_account = UCodeHelper::ToTS(fund_account);

		std::string l_op_code = cfgsetting["configuration"]["l_op_code"].EleVal();
		input.l_op_code = UCodeHelper::ToTS(l_op_code);

		std::string vc_op_password =  cfgsetting["configuration"]["vc_op_password"].EleVal();
		input.vc_op_password = UCodeHelper::ToTS(vc_op_password);


		ret = this->Initialize(&input);
		return ret;
	}



	// 成交数量
	bool CHSTradeAdapter::QueryOrder(OrderID id, int* pCount)
	{
		std::list<DealedOrder> dealedorders;

		// 期货当日成交查询
		bool ret =  GetDealedOrderInfo(&dealedorders);
		if(ret)
		{
			for(std::list<DealedOrder>::iterator it = dealedorders.begin(); it != dealedorders.end(); ++it)
			{
				if( it->entrust_no == id)
				{
					*pCount =  (int)it->business_amount;
					return true;
				}
			}
		}

		// 期货当日委托查询
		*pCount =  GetOrderTransactionQueries(id);
		return true;
	}

	

	std::pair<bool,Order> CHSTradeAdapter::GetOrderInfo( OrderID id )
	{
		std::map<int,Order>::iterator it;
		it = m_idorders.find(id);
		if(it == m_idorders.end())
		{
			Order dummy;
			return std::make_pair<bool, Order>(false, RMOVE(dummy));
		}
		return std::make_pair<bool, Order>(true, RMOVE(it->second));
	}

	hsqh::CHSTradeAdapter::StockInfo CHSTradeAdapter::GetStockInfoFromOrderID( OrderID id )
	{
		StockInfo info;
		if( GetOrderInfo(id).first == true )
		{
			info = GetStockInfo(GetOrderInfo(id).second.ContractCode.c_str());			
		}
		else
		{
			//注：在交易中，这些是必须要填的字段，
			info.exchange_type = L"2";
			info.stock_code = L"002535";
		}
		return info;
	}



	bool CHSTradeAdapter::GetDealedOrderInfo(std::list<DealedOrder>* pinfos, CONST TCHAR* stockcode,OrderID id )
	{
		/*
		static std::map<tstring,std::list<DealedOrder> > xmaps;
		static std::map<tstring,int> xxmap;		
		*/
		AUTOGARD();
		int POS = 0;
		std::list<DealedOrder> tmp;
		pinfos->clear();
		while( true )
		{
			GetDealedOrderInfoVithPos(&tmp,stockcode,id,POS);
			if(tmp.size() ==0)
			{
				return true;
			}
			else
			{
				for(std::list<DealedOrder>::iterator it = tmp.begin() ; it != tmp.end(); ++it)
				{				
					pinfos->push_back(*it);
				}
				POS = pinfos->size();
			}
		}
		return true;

	}
	bool CHSTradeAdapter::GetDealedOrderInfoVithPos(std::list<DealedOrder>* pinfos, CONST TCHAR* stockcode,OrderID id ,int Pos)
	{

		TCHAR pos[12] = {0};
		_stprintf(pos, _T("%d"),Pos);

		pinfos->clear();

		BeginJob(FUTURES_TRANSACTION_QUERIES_DAY);
		AddFieldValue(L"version",L"1");
		AddFieldValue(L"op_branch_no",m_usrinfo.op_branch_no.c_str());
		AddFieldValue(L"op_entrust_way",m_usrinfo.op_entrust_way.c_str());
		AddFieldValue(L"identity_type",L"");
		AddFieldValue(L"op_station",m_usrinfo.op_station.c_str());
		AddFieldValue(L"function_id",L"1506");
		AddFieldValue(L"branch_no",m_usrinfo.sbranch_no.c_str());
		AddFieldValue(L"fund_account",m_usrinfo.fund_account.c_str());
		AddFieldValue(L"password",m_usrinfo.password.c_str());
		AddFieldValue(L"exchange_type",L"");
		AddFieldValue(L"futures_account",m_usrinfo.futures_account.c_str());
		AddFieldValue(L"contract_code",L"");
		AddFieldValue(L"query_mode",L"0");//查询模式 '0' 明细 '1' 委托合并 '2' 证券汇总 '3' 按资金帐号汇总
		AddFieldValue(L"query_direction",L"1");
		AddFieldValue(L"request_num",L"1000");
		AddFieldValue(L"futu_position_str",pos);
		CommitJob();

		if( m_hsconn->ErrorNo !=0)
		{
			_updatetLastError();
			return false;
		}

		while (m_hsconn->Eof == 0)
		{
			DealedOrder order;

			order.serial_no = UMaths::ToInt32 (GetFieldValW(L"futu_business_no").c_str()); // 成交编号

			order.stock_account = UCodeHelper::ToTS(GetFieldValW(L"futures_account")); // 期货账号
			order.stock_code = UCodeHelper::ToTS(GetFieldValW(L"contract_code"));      // 合约代码
			order.stock_name = UCodeHelper::ToTS(GetFieldValW(L"contract_name"));      // 品种简称
			order.entrust_bs = (EBuySell )UMaths::ToInt32( GetFieldValW(L"entrust_bs").c_str() );
			order.business_price = UMaths::ToDouble( GetFieldValW(L"futu_business_price").c_str() );
			order.business_amount = UMaths::ToDouble( GetFieldValW(L"business_amount").c_str() );
			order.business_time = UMaths::ToInt32( GetFieldValW(L"business_time") );
			order.entrust_no = UMaths::ToInt32( GetFieldValW(L"entrust_no").c_str());;

			m_hsconn->MoveBy(1);
			pinfos->push_back(order);
		}
		return true;
	}

	int CHSTradeAdapter::GetOrderTransactionQueries( OrderID id )
	{
		AUTOGARD();
		OrderStatus status = OrderStatus::AllDealed;
		BeginJob(FUTURES_DELEGATE_QUERIES_DAY);
		AddFieldValue(L"version",L"1");
		AddFieldValue(L"op_branch_no",m_usrinfo.op_branch_no.c_str());
		AddFieldValue(L"op_entrust_way",m_usrinfo.op_entrust_way.c_str());
		AddFieldValue(L"identity_type",L"");
		AddFieldValue(L"op_station",m_usrinfo.op_station.c_str());
		AddFieldValue(L"function_id",L"1504");
		AddFieldValue(L"branch_no",m_usrinfo.sbranch_no.c_str());
		AddFieldValue(L"fund_account",m_usrinfo.fund_account.c_str());
		AddFieldValue(L"password",m_usrinfo.password.c_str());
		AddFieldValue(L"futu_exch_type",L"F4");//交易类别
		AddFieldValue(L"futures_account",m_usrinfo.futures_account.c_str());//期货账号
		AddFieldValue(L"contract_code",L"");//合约代码
		AddFieldValue(L"en_entrust_status",L"");
		AddFieldValue(L"query_direction",L"1");//查询方向，0：顺查；1：倒查
		AddFieldValue(L"entrust_no",UStrConvUtils::ToString(id).c_str()); //指定委托号，查指定委托号的委托，且request_num为1，且前台在收到应答后要检查返回的委托的委托号是否与要求查询的一致，此时exchange_type和stock_code要为空串。</param>
		AddFieldValue(L"futu_entrust_type",L"");//委托类型
		AddFieldValue(L"request_num",L"");//请求行数，不送按50行处理，超过系统指定值（1000行）按系统指定值（1000行）处理</param>
		AddFieldValue(L"position_str",L"");
		CommitJob();

		if( m_hsconn->ErrorNo !=0)
		{
			_updatetLastError();
			return Failed;
		}
		///-52	无此股票	（此错误可不作判断）
		///-60	没有委托记录
		///-61	查询委托失败	（其他错误）

		int sum = 0;
		if (m_hsconn->Eof == 0)
		{
			int num = 0;
			ResultSet res;
			res[L"_获取在途订单信息"] =L"";
			res[L"entrust_no"] =  GetFieldValW(L"entrust_no");
			m_hsconn->MoveBy(1);
			if(id == UMaths::ToInt32(res[L"business_amount"].c_str() ))
			{
				num = UMaths::ToInt32(res[L"business_amount"].c_str() );
				sum +=num;
			}
		}
		return sum;
	}

	std::pair<int,OrderStatus> CHSTradeAdapter::GetOnlinedOrderInfo( OrderID id )
	{
		AUTOGARD();
		OrderStatus status = OrderStatus::AllDealed;
		BeginJob(FUTURES_DELEGATE_QUERIES_DAY);
		AddFieldValue(L"version",L"1");
		AddFieldValue(L"op_branch_no",m_usrinfo.op_branch_no.c_str());
		AddFieldValue(L"op_entrust_way",m_usrinfo.op_entrust_way.c_str());
		AddFieldValue(L"identity_type",L"");
		AddFieldValue(L"op_station",m_usrinfo.op_station.c_str());
		AddFieldValue(L"function_id",L"1504");
		AddFieldValue(L"branch_no",m_usrinfo.sbranch_no.c_str());
		AddFieldValue(L"fund_account",m_usrinfo.fund_account.c_str());
		AddFieldValue(L"password",m_usrinfo.password.c_str());
		AddFieldValue(L"futu_exch_type",L"F4");//交易类别
		AddFieldValue(L"futures_account",m_usrinfo.futures_account.c_str());//期货账号
		AddFieldValue(L"contract_code",L"");//合约代码
		AddFieldValue(L"en_entrust_status",L"");
		AddFieldValue(L"query_direction",L"1");//查询方向，0：顺查；1：倒查
		AddFieldValue(L"entrust_no", UCodeHelper::ToTS(UStrConvUtils::ToString(id)).c_str()); //指定委托号，查指定委托号的委托，且request_num为1，且前台在收到应答后要检查返回的委托的委托号是否与要求查询的一致，此时exchange_type和stock_code要为空串。</param>
		AddFieldValue(L"futu_entrust_type",L"");//委托类型
		AddFieldValue(L"request_num",L"");//请求行数，不送按50行处理，超过系统指定值（1000行）按系统指定值（1000行）处理</param>
		AddFieldValue(L"position_str",L"");
		CommitJob();

		if( m_hsconn->ErrorNo !=0)
		{
			_updatetLastError();
			return std::make_pair<int,OrderStatus>(0,Failed);
		}
		///-52	无此股票	（此错误可不作判断）
		///-60	没有委托记录
		///-61	查询委托失败	（其他错误）

		int sum = 0;
		if (m_hsconn->Eof == 0)
		{
			int num = 0;
			ResultSet res;
			res[L"_获取在途订单信息"] =L"";
			res[L"status_name"] =  GetFieldValW(L"status_name");
			m_hsconn->MoveBy(1);
			if(res[L"status_name"] == L"待撤")
			{
				status = OrderStatus::CancelOnline;
			}
			num = UMaths::ToInt32(res[L"business_amount"].c_str() );
			sum +=num;
			GetLogStream()<<res;
		}
		return std::make_pair<int,OrderStatus>(RMOVE(sum),RMOVE(status));
	}

	bool CHSTradeAdapter::ReConnect()
	{
		if(m_hsconn != NULL)
		{
			m_hsconn->DisConnect();
			m_hsconn = NULL;
		}
		m_hsconn->Connect();
		return false;
	}

	bool CHSTradeAdapter::QueryDealedOrders(std::list<DealedOrder>* pres)
	{
		pres->clear();

		bool ret = this->GetDealedOrderInfo(pres);
		return ret;
	}

	void CHSTradeAdapter::QueryAccountBasicInfo1()
	{
		if(m_accountInfo.ClientName != _T(""))
		{
			return;
		}
		AUTOGARD();

		BeginJob(USER_CONFIRM);
		AddFieldValue(L"version",L"1");
		AddFieldValue(L"op_branch_no",m_usrinfo.op_branch_no.c_str());
		AddFieldValue(L"op_entrust_way",m_usrinfo.op_entrust_way.c_str());
		AddFieldValue(L"op_station",m_usrinfo.op_station.c_str());
		AddFieldValue(L"branch_no",m_usrinfo.sbranch_no.c_str());
		AddFieldValue(L"input_content",L"1");
		AddFieldValue(L"password",m_usrinfo.password.c_str());
		AddFieldValue(L"content_type",L"0");
		AddFieldValue(L"account_content",m_usrinfo.usr.c_str());
		CommitJob();

		if(m_hsconn->ErrorNo != 0)
		{
			_updatetLastError();
			return;
		}
		m_accountInfo.FundAccount = UCodeHelper::ToTS( GetFieldValW(L"fund_account") );
		m_accountInfo.ClientName = UCodeHelper::ToTS(GetFieldValW(L"client_name"));
		m_accountInfo.MoneyType = _T("0");
	}

	void CHSTradeAdapter::QueryAccountBasicInfo2()
	{
		AUTOGARD();
		BeginJob(FUTURE_CLIENTS_QUERY);
		AddFieldValue(L"version",		L"1");
		AddFieldValue(L"op_branch_no",	m_usrinfo.op_branch_no.c_str() );
		AddFieldValue(L"op_entrust_way",m_usrinfo.op_entrust_way.c_str());
		AddFieldValue(L"identity_type",	L"");
		AddFieldValue(L"op_station",	m_usrinfo.op_station.c_str());
		AddFieldValue(L"function_id",	L"1508");
		AddFieldValue(L"branch_no",		m_usrinfo.sbranch_no.c_str());
		AddFieldValue(L"fund_account",	m_usrinfo.fund_account.c_str());
		AddFieldValue(L"password",		m_usrinfo.password.c_str());
		AddFieldValue(L"money_type",	L"0");
		CommitJob();

		ResultSets ret;
		while (m_hsconn->Eof == 0)
		{
			m_accountInfo.CurrentBalance = UMaths::ToDouble( GetFieldValW(L"current_balance"));
			m_accountInfo.EnableBalance = UMaths::ToDouble(GetFieldValW(L"enable_balance"));
			m_accountInfo.AssetBalance =  UMaths::ToDouble(GetFieldValW(L"asset_balance"));
			m_accountInfo.ExProvider = _T("hsqh");
			m_hsconn->MoveBy(1);
		}
	}

	void CHSTradeAdapter::QueryAccountBasicInfo3()
	{
		//查询股东
	}

	bool CHSTradeAdapter::QueryAccountInfo( BasicAccountInfo* info )
	{
		QueryAccountBasicInfo1();
		QueryAccountBasicInfo2();
		QueryAccountBasicInfo3();
		*info = m_accountInfo;
		return true;
	}



	// 期货持仓查询
	bool CHSTradeAdapter::QueryPostions( std::list<PositionInfo>* presult ,const TCHAR* stock)
	{
		AUTOGARD();
		presult->clear();
		BeginJob(FUTURES_POSITIONS_QUERY);

		AddFieldValue(L"version",L"1");
		AddFieldValue(L"op_branch_no",m_usrinfo.op_branch_no.c_str());
		AddFieldValue(L"op_entrust_way",m_usrinfo.op_entrust_way.c_str());
		AddFieldValue(L"identity_type",L"");
		AddFieldValue(L"op_station",m_usrinfo.op_station.c_str());
		AddFieldValue(L"function_id",L"1503");
		AddFieldValue(L"branch_no",m_usrinfo.sbranch_no.c_str());
		AddFieldValue(L"fund_account",m_usrinfo.fund_account.c_str());
		AddFieldValue(L"password",m_usrinfo.password.c_str());
		AddFieldValue(L"futu_exch_type",L"F4");
		AddFieldValue(L"futures_account",m_usrinfo.futures_account.c_str());
		if(stock == NULL)
		{
			AddFieldValue(L"contract_code",L"");
		}
		else
		{
			AddFieldValue(L"contract_code",stock);
		}
		AddFieldValue(L"entrust_bs",L""); // 1,期货开仓  2,期货平仓
		AddFieldValue(L"query_direction",L"");
		AddFieldValue(L"query_mode",L"0"); //0明细，1汇总
		AddFieldValue(L"request_num",L"1000");
		AddFieldValue(L"futu_position_str",L"");// 定位串
		CommitJob();


		if(m_hsconn->ErrorNo!=0)
		{
			_updatetLastError();
			return false;
		}		

		while (m_hsconn->Eof == 0)
		{
			PositionInfo info;
			info.code = GetFieldValT(L"contract_code");		// 合约代码
			info.stock_name = GetFieldValT(L"contract_name");	// 品种简称
			info.current_amount = UMaths::ToDouble(GetFieldValW(L"enable_amount"));		// 可用数量,可用数量
			info.enable_amount = UMaths::ToDouble(GetFieldValW(L"real_amount"));		// 可卖股票,当日可平仓数量
			info.last_price = UMaths::ToDouble(GetFieldValW(L"futu_last_price"));		// 最新价格
			info.cost_price = UMaths::ToDouble(L"");
			//info.cost_price = UMaths::ToDouble(GetFieldValW(L"futu_average_price"));	// 平均价(没有返回成本用平均价代替)
			info.market_value = info.current_amount*info.last_price;					// 市值(可用数量*最新价格)
			info.income_balance = UMaths::ToDouble(GetFieldValW(L"hold_profit_float"));	// 持仓浮动盈亏

			int nByeShell = -1;
			nByeShell  =  UMaths::ToInt32(GetFieldValW(L"entrust_bs"));

			info.posside = (nByeShell == 1 ? EPosSide ::eMore : EPosSide::eShort);

			presult->push_back(info);
			m_hsconn->MoveBy(1);
		}
		return true;
	}

	

	

	bool CHSTradeAdapter::IsInitialized() 
	{
		AUTOGARD();
		return m_hsconn != NULL && m_conneted == true;
	}

	CHSTradeAdapter::CHSTradeAdapter()
	{
		m_conneted = false;
	}

	bool CHSTradeAdapter::_cancelorder( OrderID orderid )
	{
		AUTOGARD();
		bool ret = false;
		BeginJob(FUTURES_RETREATING_FORM);

		AddFieldValue(L"version",		L"1");
		//AddFieldValue(L"l_op_code",		m_usrinfo.l_op_code.c_str());
		//AddFieldValue(L"vc_op_password",m_usrinfo.vc_op_password.c_str());
		AddFieldValue(L"op_branch_no",	m_usrinfo.op_branch_no.c_str());
		AddFieldValue(L"op_entrust_way",m_usrinfo.op_entrust_way.c_str());
		AddFieldValue(L"identity_type",L"");
		AddFieldValue(L"op_station",	m_usrinfo.op_station.c_str());
		AddFieldValue(L"function_id", UCodeHelper::ToTS(UStrConvUtils::ToString(FUTURES_RETREATING_FORM)).c_str());
		AddFieldValue(L"branch_no",		m_usrinfo.sbranch_no.c_str());
		AddFieldValue(L"fund_account",	m_usrinfo.fund_account.c_str());
		AddFieldValue(L"password",		m_usrinfo.password.c_str());
		AddFieldValue(L"entrust_no", UCodeHelper::ToTS(UStrConvUtils::ToString(orderid)).c_str()); //注:TODO ,LIN ZOU ZHONGJI SHIYONG 2
		CommitJob();

		ResultSet res;
		try
		{
			res[L"_取消订单:"] =L"\n";
			res[L"error_no"] = GetFieldValW(L"error_no");
			res[L"error_info"] = GetFieldValW(L"error_info");
			res[L"entrust_no"] = GetFieldValW(L"entrust_no");
			//	res[L"batch_no"] = GetFieldValW(L"batch_no"); //注：这个字段是自己填写
			GetLogStream()<<res;

			auto error_no = res[L"error_no"];
			int ner_no = UMaths::ToInt32(error_no.c_str());
			if(ner_no == -53||ner_no == 81501||ner_no == 0)		// -53:“无此委托”            （包含此委托不是该股民的）文档不对
			{
				return true;
			}else{

				return true;
			}
		}
		catch(...)
		{

		}
		return true;
	}

	bool CHSTradeAdapter::QueryEntrustInfos( std::list<EntrustInfo>* pinfo )
	{
		wchar_t pos[12] = {0};
		swprintf
			(pos, L"%d", 0);

		pinfo->clear();

		BeginJob(FUTURES_DELEGATE_QUERIES_DAY);
		AddFieldValue(L"version",L"1");
		AddFieldValue(L"op_branch_no",m_usrinfo.op_branch_no.c_str());
		AddFieldValue(L"op_entrust_way",m_usrinfo.op_entrust_way.c_str());
		AddFieldValue(L"identity_type",L"");
		AddFieldValue(L"op_station",m_usrinfo.op_station.c_str());
		AddFieldValue(L"function_id",L"1504");
		AddFieldValue(L"branch_no",m_usrinfo.sbranch_no.c_str());
		AddFieldValue(L"fund_account",m_usrinfo.fund_account.c_str());
		AddFieldValue(L"password",m_usrinfo.password.c_str());
		AddFieldValue(L"futu_exch_type",L"");
		AddFieldValue(L"futures_account",m_usrinfo.futures_account.c_str());
		AddFieldValue(L"contract_code",L"");
		AddFieldValue(L"en_entrust_status",L"");
		AddFieldValue(L"query_direction",L"0"); // 查询方向，0：顺查；1：倒查
		AddFieldValue(L"batch_no",L"");				// 委托批号
		AddFieldValue(L"entrust_no",L"");			// 委托编号
		AddFieldValue(L"futu_entrust_type",L"");	// 委托类型
		AddFieldValue(L"request_num",L"1000");		// 查询条数
		AddFieldValue(L"position_str",pos);			// 定位串
		CommitJob();

		if( m_hsconn->ErrorNo !=0)
		{
			_updatetLastError();
			return false;
		}

		while (m_hsconn->Eof == 0)
		{
			EntrustInfo Info;

			Info.matchqty = UMaths::ToInt32(GetFieldValW(L"business_amount"));	// 成交数量
			Info.operdate  =  UMaths::ToInt32(GetFieldValW(L"init_date"));		// 成交日期
			Info.opertime;	// 成交时间

			//下面是原来委托时的信息
			Info.Ope = (EBuySell )UMaths::ToInt32( GetFieldValW(L"entrust_bs").c_str() );		// 委托方向
			Info.orderid = UMaths::ToInt32( GetFieldValW(L"entrust_no").c_str() );				// 委托编号
			Info.orderdate = UMaths::ToInt32(GetFieldValW(L"entrust_time"));					// 委托日期
			Info.ordertime;	// 委托时间
			Info.stkcode = GetFieldValT(L"contract_code");	// 股票代码
			Info.stkname = GetFieldValT(L"contract_name");;	// 股票名称
			Info.orderqty = UMaths::ToInt32(GetFieldValW(L"entrust_amount"));	// 委托数量
			Info.orderprice = UMaths::ToDouble( GetFieldValW(L"futu_entrust_price").c_str() );// 期货委托价格
			Info.orderfrzamt = UMaths::ToDouble( GetFieldValW(L"frozen_fare").c_str() );// 冻结金额
			Info.futureOC = (EOpenClose)(UMaths::ToInt32(GetFieldValW(L"futures_direction"))+1);		// 开平仓方向

			Info.matchamt = Info.matchqty*Info.orderprice;	// 成交金额(成交数量*期货委托价格)

			m_hsconn->MoveBy(1);
			pinfo->push_back(Info);
		}
		return true;
	}

	tstring CHSTradeAdapter::GetName()
	{
		return _T("HS QiHuo Trade Adapter");
	}

};


}