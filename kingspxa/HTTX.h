#ifndef __HTTX_H__
#define  __HTTX_H__

#pragma pack(1)

//#define HTTX_SIGNATURE	0x742582af
#define HTTX_LOGINCMD	0x11
#define HTTX_PINGCMD	0x12
#define HTTX_DATACMD	0x13

#define HTTX_LOGINRSPCMD	0x91
#define HTTX_PINGRSPCMD	0x92
#define HTTX_DATARSPCMD	0x93

#define HTTX_MONITORCMD 0x8ad2			// 彭铭设置的监控HTS的命令，不占用0~32767的命令空间
#define MAXPACKSIZE             65536
#define MAXRANSTRSIZE           20		 //登录随机串的长度
#define MAXCRCSIZE              8		//CRC串长度
#define MAXERRORINFO			256       //系统报错信息长度
#define MAXTHREAD               20


//通讯包头
typedef struct _SComm_Header {
	BYTE	cmd;  		// 数据包的类型，分为CONNECT包/PING包/数据包/文件类四类
	BYTE	flag; 		// flag & FIRST_FLAG 表示首包，flag & LAST_FLAG 表示是结束包
	// 客户端收到一个last包才向上层系统返回数据，对于批方式发送数据的时候有效
	// 第一次实现的时候，只实现单包交换
	int 	error; 		// 0 表示正常，非0为错误包，错误包的格式为4字节retcode和最长255字节的错误信息，如果省略包体，则错误代码为999，错误信息为“通讯错误”
	BYTE	result_count;   // 包体中的记录个数，仅供参考
	BYTE	encrypt_method; // 加密方式
	BYTE	compress_method;// 压缩算法
	WORD 	org_len;	// 原始数据包的长度
	WORD 	len;		// 通讯数据包的长度
	long	userdata;	// 客户端的数据，服务器原样返回
	WORD	crc;		// 包头部分的CRC
} SCOMM_HEADER, *LPSCOMM_HEADER;


#ifndef MAXAPPNAME
#define MAXAPPNAME		30
#endif

#ifndef MAXAPPVERSION
#define MAXAPPVERSION	30
#endif

//Login请求
typedef struct _reqConnect {
	BYTE encrypt_method;		// 加密方式
	BYTE compress_method;		// 压缩算法
	char szAppName[MAXAPPNAME];	// 客户端程序的名字
	char szAppVersion[MAXAPPVERSION];	// 客户端程序的版本号
	char szDLLName[20];			// DLLNAME
	char pDLLID[8];				// DLLID
	char szMacAddress[22];		// 客户端的网卡地址
	char szIP[20];				// 客户端IP地址，可能是一个内部的IP地址
	char szOPNO[20];			// 客户编码
	char szOPPass[20];			// 客户密码
	char pKey[256];				// 客户端下一次使用的key
	// 首包可能采用RSA算法2048位密钥
} CONNECT_REQ, *LPCONNECT_REQ;

//Login应答
typedef struct _ansConnect {
	BYTE encrypt_method;		// 加密方式
	BYTE compress_method;		// 压缩算法
	BYTE version;				// 柜台的版本号，5－－V5柜台、6－－V6柜台
	char cWtfs;					// 适用的委托方式代码
	char szGYS[3];				// 供应商代码
	char szCompanyName[9];		// 适用的经纪公司代码
	char szDate[9];				// 服务器的日期
	char szTime[9];				// 服务器的时间
	char Key[32];				// 服务器下一次使用的key
	// 后续包只会采用对称算法，支持256位密钥
} CONNECT_ANS, *LPCONNECT_ANS;


//业务包头
typedef struct _SApp_Header {
	int    func_no;                 //业务功能号
	char   cust_no[22];             //客户号
	char   password[20];              //交易密码，非必填
	char   entrust_type[8];           //委托方式
	char   branch[4];                 //客户所属营业部（目的营业部）
} SAPP_HEADER, *LPSAPP_HEADER;

#pragma pack()

#endif // __HTTX_H__
