// SList.cpp: implementation of the CSList class.
//
//////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "SList.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void CSList::GetAtIndex(const char *mainstring, int index, char *szFieldValue, int nFieldSize, char mark/*='|'*/)
{
	int i,j,k,l;
	int strLen=strlen(mainstring);

	*szFieldValue=0;
	if (index>=0)
	for(i=j=0,k=-1; i<strLen; i++)
	{
		if(*(mainstring+i)==mark)
		{
			k++;
			if (k==index) {
				l = i-j;
				if (l>nFieldSize-1) l=nFieldSize-1;

				if(l>0)
				{
					strncpy(szFieldValue, mainstring+j,l);
					szFieldValue[l]='\0';
				}
				return;
			}
			j=i+1;
		}
	}

	if (i>=j) {
		k++;
		if (k==index) {
			l = i-j;
			if (l>nFieldSize-1) l=nFieldSize-1;
			
			if(l>0)
			{
				strncpy(szFieldValue, mainstring+j,l);
				szFieldValue[l]='\0';
			}
		}
	}
}

CSList::CSList()
{
	Clear();
}

CSList::~CSList()
{
	m_lst.clear();
}

bool CSList::Add(char *buffer)
{
	string str=buffer;
	m_lst.push_back(str);
	m_count++;
	return true;
}

char * CSList::GetAt(int nIndex)
{
   	if(nIndex<0 || nIndex>=m_count) return (char*)" "; 
  	STRINGLIST::iterator plist;
    plist=m_lst.begin();
	for(int i=0;i<nIndex;i++) plist++;
    return (char*)plist->c_str();
}

void CSList::SetAt(char *buffer,int nIndex)
{
    if(nIndex<0 || nIndex>=m_count) return;
    STRINGLIST::iterator plist;
    plist=m_lst.begin();
    for(int i=0;i<nIndex;i++)
	    plist++;
	string str=buffer;
	m_lst.insert(plist,str);
    m_lst.erase(plist);
}

int CSList::GetCount()
{
	return m_count;
}

void CSList::Clear()
{
    m_count=0;
    m_lst.clear();
}

int CSList::GetBuff(char *buffer,bool trimflag, char mark/*='|'*/)
{
	int ii=0;
	char szMark[2];
	szMark[0]=mark;
	szMark[1]=0;

	try
	{
		if(buffer!=NULL) *(buffer)=0;
		else return 0;
		STRINGLIST::iterator plist;
		for(plist=m_lst.begin();plist!=m_lst.end();plist++)
		{
			if(trimflag)
			{
				char buf[4096];
				memset(buf,0,sizeof(buf));
				strcpy(buf,(char*)plist->c_str());
				trim(buf);
				if (buf[0])
					strcat(buffer,trim(buf));
				else
					strcat(buffer," ");
			}
			else strcat(buffer,plist->c_str());
			strcat(buffer,szMark);
		}
		ii=strlen(buffer);
		*(buffer+ii-1)='\0';//将末尾的'|'去除
	}catch(...)
	{
		strcpy(buffer,"CSList 类GetBuff 系统异常");
	}
	return ii;
}

int CSList::GetBuff2(char *buffer, int colStart, int colCount, bool trimflag/*=false*/, char mark/*='|'*/)
{
	int ii=0;
	char szMark[2];
	szMark[0]=mark;
	szMark[1]=0;
	
	try
	{
		if(buffer!=NULL) *(buffer)=0;
		else return 0;
		STRINGLIST::iterator plist;
		int col;

		for(plist=m_lst.begin(),col=0; plist!=m_lst.end() && col<colStart; plist++, col++);
		if (col<colStart) return 0;

		for(; plist!=m_lst.end() && col<(colStart+colCount); plist++,col++)
		{
			if(trimflag)
			{
				char buf[4096];
				memset(buf,0,sizeof(buf));
				strcpy(buf,(char*)plist->c_str());
				strcat(buffer,trim(buf));
			}
			else strcat(buffer,plist->c_str());
			strcat(buffer,szMark);
		}
		
		for(; col<(colStart+colCount); col++)
			strcat(buffer, szMark);

	}catch(...)
	{
		strcpy(buffer,"CSList 类GetBuff2 系统异常");
	}
	return ii;
}

bool CSList::FillStrings(const char *mainstring, char mark/*='|'*/,int len /*=0*/)
{
	int i,j,l;
	char strTemp[MAXCOLUMN+1];
	memset(strTemp,0,sizeof(strTemp));
	int strLen=strlen(mainstring);
	j=0;
	Clear();
	
	if(len >0)
	{
		strLen = len;
	}
	
	for(i=0;i<strLen;i++)
	{
		if(*(mainstring+i)==mark)
		{
			l = i-j;
			if (l>MAXCOLUMN) {
				WriteLog(0, "ColumnSize>%d",MAXCOLUMN);
				l=MAXCOLUMN; 
			}
			
			if(l>0)
			{
				strncpy(strTemp, mainstring+j, l);
				strTemp[l]='\0';
			}
			else 
				strcpy(strTemp," ");
			
			j=i+1;
			
			if(!Add(strTemp)) return false;	
		}
	}
	if(i>j)
	{
		l = i-j;
		if (l>MAXCOLUMN) {
			WriteLog(0, "ColumnSize>%d",MAXCOLUMN);
			l=MAXCOLUMN; 
		}
		
		strncpy(strTemp,mainstring+j,l);
		strTemp[l]='\0';
		if(!Add(strTemp)) return false;
	}
	else if(i==j)
	{
		strcpy(strTemp," ");
		if(!Add(strTemp)) return false;
	}
	
	return true;
}
// pen added at 20070831
bool CSList::FillStrings2(const char *mainstring, char cMark)
{
	int i,j,k;
	char strTemp[MAXCOLUMN];
	int strLen=strlen(mainstring);
	
	j=0;Clear();
	for(i=0;i<strLen;i++)
	{
		if(*(mainstring+i)==cMark)
		{
			k=i-j;
			if(k>0)
			{
				if (k>MAXCOLUMN-1) k=MAXCOLUMN-1;
				memset(strTemp,0,sizeof(strTemp));
				memcpy(strTemp,mainstring+j,k);
			}
			else strcpy(strTemp," ");
			
			j=i+1;
			if(!Add(strTemp)) return false;
		}
	}
	
	k=i-j;
	if(k>0)
	{
		if (k>MAXCOLUMN-1) k=MAXCOLUMN-1;
		memset(strTemp,0,sizeof(strTemp));
		memcpy(strTemp,mainstring+j,k);
		if(!Add(strTemp)) return false;
	}
	else if(k==0)
	{
		strcpy(strTemp," ");
		if(!Add(strTemp)) return false;
	}
	
	return true;
}
