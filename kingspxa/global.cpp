/////////////////////////////////////////////////////////////////
//  创建日期：20:43 2002-04-01
//  作者：彭铭
//  版本信息：
//  描述： 本文件为提供给整个子系统编写的全局独立函数实现和
//	全局变量的实例定义所用
/////////////////////////////////////////////////////////////////
#include "StdAfx.h"
#include <stdlib.h>
#include <string.h>

#include "global.h"

CSYSGVARS *g_Vars;
CAlgorism g_Algorism;
FUNCNODE  g_Funclist[5000];

CSYSGVARS::CSYSGVARS()
{
	g_bInInitSection=false;
	
	g_bToExit=false;
	
    g_connected = false;
	g_multied = false;
	g_run = false;
	m_nRequestCount=0; //请求数据包数

	m_nReturnCount=0;  //应答数据包数
	
	memset(szCfgFileName, 0, sizeof(szCfgFileName));
	memset(szCodeFileName, 0, sizeof(szCodeFileName));
	
	memset(g_szIP, 0, sizeof(g_szIP));
	memset(g_byAddr, 0, sizeof(g_byAddr));

	memset(m_wtfs,0,sizeof(m_wtfs));

	m_begintime=0;
	m_endtime=0;
	m_difftime=0;
}

CSYSGVARS::~CSYSGVARS()
{

}

int CSYSGVARS::GetFuncindex(int id)
{
	int index =0;
	for(;index<g_Vars->g_func_count;index++)
	{
		if(id == g_Funclist[index].id)
		{
			break;
		}
	}
	if(index == g_Vars->g_func_count)
	{
		index =-1;
	}
	return index;
}

char *ltrim(char *str)
{ 	
	if (strlen(str)<=0) return str;
	
	char *p=str;
	while (*p==' '||*p=='\t'||*p=='\r'||*p=='\n') p++;
	strcpy(str,p);
	return str;
}

char *rtrim(char *str)
{ 	
	if (strlen(str)<=0) return str;
	
	char *p=&str[strlen(str)-1];
	int i;
	for (i=strlen(str)-1;i>=0;i--)
		if (str[i]!=' ' && str[i]!='\t' && str[i]!='\r' && str[i]!='\n') break;
		str[i+1]=0;
		return str;
}

char *trim(char* in)
{
	return rtrim(ltrim(in));
}

//读取字符型的配置文件数据
//入口参数：lpAppName：项名
//          lpKeyName：键名
//          lpBuf：存放数据的缓冲区
//          filename：配置文件名称
//          defaultValue：缺省值
//返回值：无
//此函数将自动去除数据左右的空格和
//以“//”开始的注释
void CfgReadString(char *lpAppName, char *lpKeyName, 
                   char *lpBuf, char *filename, char *defaultValue)
{
	char *p;
	char buf[1024];
	
	::GetPrivateProfileStringA(lpAppName, lpKeyName, defaultValue, buf, 
		1022, filename);
	trim(buf);
	p = strstr(buf, "//");
	if (p != NULL)
	{
		p[0]='\0';
		trim(buf);
		strcpy(lpBuf,buf);
		/* ****** Updated by CHENYH at 2002-8-22 14:16:09 ****** 
		这种方式将会导致出现： lpBuf的长度不够而使得 
		如其szFirmID[8]而配置定义 FIRMID = 123456      //dsisdafs
		这时候就麻烦了，会
		
		  idx = p - buf;
		  strncpy(lpBuf, buf, idx);
		  lpBuf[idx] = 0;
		  trim(lpBuf);
		*/
	}
	else
		strcpy(lpBuf, buf);
}

//读取布尔型的配置文件数据
//入口参数：lpAppName：项名
//          lpKeyName：键名
//          filename：配置文件名称
//          defaultValue：缺省值
//返回值：若读取的数据为“YES”，则返回 TRUE
//        否则返回 FALSE
BOOL CfgReadBool(char *lpAppName, char *lpKeyName, 
                 char *filename, char *defaultValue)
{
	int idx;
	char *p;
	char buf[1024];
	
	::GetPrivateProfileStringA(lpAppName, lpKeyName, defaultValue, buf, 
		1022, filename);
	trim(buf);
	p = strstr(buf, "//");
	if (p != NULL)
	{
		idx = p - buf;
		buf[idx] = 0;
		trim(buf);
	}
	
	_strupr(buf);
	if (strncmp(buf, "YES", 3) == 0)
		return TRUE;
	else
		return FALSE;
}

//读取长整型的配置文件数据
//入口参数：lpAppName：项名
//          lpKeyName：键名
//          filename：配置文件名称
//          defaultValue：缺省值
//返回值：读取的长整型的数据
long CfgReadLong(char *lpAppName, char *lpKeyName, 
                 char *filename, char *defaultValue)
{
	int idx;
	char *p;
	char buf[1024];
	
	::GetPrivateProfileStringA(lpAppName, lpKeyName, defaultValue, buf,
		1022, filename);
	trim(buf);
	p = strstr(buf, "//");
	if (p != NULL)
	{
		idx = p - buf;
		buf[idx] = 0;
		trim(buf);
	}
	
	return atol(buf);
}

//读取double型的配置文件数据
//入口参数：lpAppName：项名
//          lpKeyName：键名
//          filename：配置文件名称
//          defaultValue：缺省值
//返回值：读取的double型的数据
double CfgReadDouble(char *lpAppName, char *lpKeyName, 
                     char *filename, char *defaultValue)
{
	int idx;
	char *p;
	char buf[1024];
	
	::GetPrivateProfileStringA(lpAppName, lpKeyName, defaultValue, buf,
		1022, filename);
	trim(buf);
	p = strstr(buf, "//");
	if (p != NULL)
	{
		idx = p - buf;
		buf[idx] = 0;
		trim(buf);
	}
	
	return atof(buf);
}

//写入字符型数据到配置文件
//入口参数：lpAppName：项名如果这个区域不存在，则创建这个区域。名不区分大小写
//          lpKeyName：键名若不存在,则创建之
//          lpBuf：要加入的字符串如果参数为NULL，函数将把INI文件中这个关键字删掉
//          filename：配置文件名称
//返回值：true 表示成果 false 表示失败

bool CfgWriteString(char const* lpAppName, char const* lpKeyName,char const* lpBuf, char const* filename)
{
	int ret;		
	ret=::WritePrivateProfileStringA(lpAppName,lpKeyName,lpBuf,filename);
	if(ret<=0) return false;
	else return true;
}

void WriteLog(int DebugLevel, char *FormatStr, ...)
// WriteLog函数用于在RUNNING INFO窗口显示信息，并将显示的信息记录到日志文件中
// WriteLog函数的输出受g_inifile.m_nDebugLevel的控制，对于不必要输出的信息，可以不再显示
// DebugLevel 的值为0(对应于3), 1(对应于2), 2(对应于1)...
{
/*	char	szTmpBuf[LOG_MAXLINE]="";
	short	nTmpLen = 0;
	int	tag;

#ifndef _DEBUG // pen
	if (g_Vars.g_bToExit && !g_Vars.g_bMustLogInExit)
		return;
#endif

	if ((DebugLevel>g_inifile.m_nDebugLevel) && !(g_inifile.m_nDebugLevel==0 && DebugLevel==1))
		return;

	va_list Marker;
	va_start(Marker, FormatStr);
	nTmpLen = vsprintf(szTmpBuf, FormatStr, Marker);
	va_end(Marker);
	
	tag=3-DebugLevel;
	if (tag<1) tag=1;

	if (!(g_inifile.m_nDebugLevel==0 && DebugLevel==1))
		ShowMessageInfo(0, tag, szTmpBuf);
	
	g_ErrorLog.WriteLog(0, szTmpBuf);

	if (g_Vars.g_showflag)
		SaveLogToDisk();*/
}
void WriteError(char *FormatStr, ...)
// WriteError函数用于在ERRORMSG INFO窗口显示信息，并将显示的信息记录到日志文件中
// WriteError的输出同时定向到RUNNING INFO窗口
{
/*	char	szTmpBuf[LOG_MAXLINE]="";
	short	nTmpLen = 0;
	
#ifndef _DEBUG // pen
	if (g_Vars.g_bToExit)
		return;
#endif
	
	va_list Marker;
	va_start(Marker, FormatStr);
	nTmpLen = vsprintf(szTmpBuf, FormatStr, Marker);
	va_end(Marker);
	
	ShowMessageInfo(0, 3, szTmpBuf);
	ShowMessageInfo(1, 3, szTmpBuf);
	g_ErrorLog.WriteLog(0, szTmpBuf);
	g_ErrorLog.WriteLog(97, szTmpBuf);*/
}



//查询委托方式在协议串中的位置
bool GetWTFSFieldIndex(int nFunction,int *nfield)
{
	bool bret=false;
	
	struct st_wtfsField	//	对应表
	{		
		int  nFunction;
		int nwtfsField ; 
	};
	
	struct st_wtfsField dmb[]=
	{
		{1	    , 8}, 
		{2	    , 9},      
		{3	    , 12},   
		{4	    , 9},   
		{5	    , 10},
		{6	    , 9},
		{7	    , 9},
		{8	    , 14},
		{10	    , 16},
		{11	    , 13},
		{13	    , 12},
		{14	    , 15},
		{15	    , 12},
		{16	    , 11},
		{17	    , 11},
		{18	    , 13},
		{19	    , 9},
		{20	    , 14},
		{21	    , 11},
		{22	    , 5},
		{23	    , 8},
		{24	    , 8},
		{25	    , 9}
	};
	
	int i;
	int count=sizeof(dmb)/sizeof(dmb[0]);
	
	for(i=0;i<count;i++)
	{
		if(dmb[i].nFunction == nFunction)
		{
			*nfield = dmb[i].nwtfsField;			
			return true;
		}
	}
	
	return bret;
}
//查询客户号在协议串中的位置
bool GetKHHFieldIndex(int nFunction,char *nfield)
{
	bool bret=false;
	return bret;
}

bool GetMMFieldIndex(int nFunction,char *nfield)
{
	bool bret=false;
	
	struct st_PswField	//	对应表
	{		
		int  nFunction;
		char nPswField[10]; 
	};
	
	struct st_PswField dmb[]=
	{
		{2	    , "8"},      
		{37	    , "8"},   
		{61	    , "8"},   
		{901    , "8"},   
		{961    , "8"},   
		{6061   , "8"}, 		
		{5		, "8,9"}, 
		{29		, "8,9"}, 
		{902	, "8,9"}, 
		{6005	, "8,9"}, 
		{20		, "12,13"}, 
		{71		, "9"}, 
		{72		, "9"}
	};
	
	int i;
	int count=sizeof(dmb)/sizeof(dmb[0]);
	
	for(i=0;i<count;i++)
	{
		if(dmb[i].nFunction == nFunction)
		{
			strncpy(nfield,dmb[i].nPswField,sizeof(nfield)-1);			
			return true;
		}
	}
	
	return bret;
}