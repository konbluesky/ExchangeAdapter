/////////////////////////////////////////////////////////////////
//  创建日期：22:44 2002-03-14
//  作者：彭铭
//  版本信息：
//  描述： 本文件为提供给整个子系统编写的全局独立函数实现和
//	全局变量的实例定义所用
/////////////////////////////////////////////////////////////////
#ifndef __GLOBAL_H__
#define __GLOBAL_H__

//////////////////////////////////////////////////////////////////
// Global variables:
/////////////////////////////////////////////////////////////////
#include "HTTX.h"
#include "Algorism.h"

#define MAX_SERVICE 100;

//配置结构体
typedef  struct {
	int  id      ;   //功能id
	char cust_no[30];             //客户号
	char password[30];              //交易密码，非必填
//    char branch[4];  //modify by lsw 20100507 模拟外围传送超过3位的营业部代码
	char branch[10];
	char cmd[1024];  //功能内容
	int  times;      //执行次数
	int  pausetime;  //延时时间，毫秒
	char desc[256];  //描述
	int  next     ;  //下一个功能id
} FUNCNODE;


class CSYSGVARS 
{
public:
	bool g_bInInitSection;

	bool g_bToExit;

	HANDLE g_hToExit; // 全局控制各线程退出的一个开关事件，有的地方可以用它来控制
	
	HANDLE g_hReqWakeUp; //请求handle

	HANDLE g_hAnsWakeUp; //请求handle
		
	bool g_connected;

	bool g_multied;

	bool g_run;

	int  g_runtimes;

	int  g_func_count;
	
	long m_nRequestCount; //请求数据包数
	
	long m_nReturnCount;  //应答数据包数
	
	// 系统配置文件：
	char szCfgFileName[256];  // 系统配置文件名
	char szCodeFileName[256];  // 系统代码文件名
	
	char g_szIP[20];	// 本机的最大的IP地址
	char    m_wtfs[10];
	int  threadnum;

	BYTE g_byAddr[10]; // 本机的地址，填写在给v6柜台的包头的addr字段（可以是mac地址或者ip地址）

	DWORD m_begintime;
	DWORD m_endtime;
	DWORD m_difftime;

public:
	int GetFuncindex(int id);

	CSYSGVARS();
	~CSYSGVARS();

//	void GetDate(void);
};

extern CSYSGVARS *g_Vars;
extern CAlgorism g_Algorism;
extern FUNCNODE  g_Funclist[5000]; 



// 其他公共函数
char *rtrim(char *buffer);
char *ltrim(char *buffer);
char *trim(char* in);

//读取字符型的配置文件数据
//入口参数：lpAppName：项名
//          lpKeyName：键名
//          lpBuf：存放数据的缓冲区
//          filename：配置文件名称
//          defaultValue：缺省值
//返回值：无
//此函数将自动去除数据左右的空格和
//以“//”开始的注释
void CfgReadString(char *lpAppName, char *lpKeyName, 
				   char *lpBuf, char *filename, char *defaultValue);

//读取布尔型的配置文件数据
//入口参数：lpAppName：项名
//          lpKeyName：键名
//          filename：配置文件名称
//          defaultValue：缺省值
//返回值：若读取的数据为“YES”，则返回 TRUE
//        否则返回 FALSE
BOOL CfgReadBool(char *lpAppName, char *lpKeyName, 
				 char *filename, char *defaultValue);

//读取长整型的配置文件数据
//入口参数：lpAppName：项名
//          lpKeyName：键名
//          filename：配置文件名称
//          defaultValue：缺省值
//返回值：读取的长整型的数据
long CfgReadLong(char *lpAppName, char *lpKeyName, 
				 char *filename, char *defaultValue);

//读取double型的配置文件数据
//入口参数：lpAppName：项名
//          lpKeyName：键名
//          filename：配置文件名称
//          defaultValue：缺省值
//返回值：读取的double型的数据
double CfgReadDouble(char *lpAppName, char *lpKeyName, 
					 char *filename, char *defaultValue);

//写入字符型数据到配置文件
//入口参数：lpAppName：项名如果这个区域不存在，则创建这个区域。名不区分大小写
//          lpKeyName：键名若不存在,则创建之
//          lpBuf：要加入的字符串如果参数为NULL，函数将把INI文件中这个关键字删掉
//          filename：配置文件名称
//返回值：true 表示成果 false 表示失败
bool CfgWriteString(char const* lpAppName, char const* lpKeyName,char const* lpBuf, char const* filename);
////////////////////////////////////////////////////////////////////////

// 以下是系统的标准的日志函数
// 每行日志的最大长度(由于程序的设计，不得大于32767)
void WriteLog(int DebugLevel, char *FormatStr, ...);
void WriteError(char *FormatStr, ...);

//查询委托方式在协议串中的位置
bool GetWTFSFieldIndex(int nFunction,char *nfield);
//查询客户号在协议串中的位置
bool GetKHHFieldIndex(int nFunction,char *nfield);
//查询密码在协议串中的位置
bool GetMMFieldIndex(int nFunction,char *nfield);

#endif // __GLOBAL_H__
