#include "stdafx.h"
#include "../include/kingspxa.h"

#ifdef _DEBUG
	#undef THIS_FILE
	static char THIS_FILE[] = __FILE__;
	#undef DEBUG_NEW
	#define	DEBUG_NEW new(_NORMAL_BLOCK,__FILE__,__LINE__)
	#define new DEBUG_NEW
#endif
/////////////////////////////////////////////////////////////////////////////
#ifdef	WIN32
#pragma	comment(lib,"ws2_32.lib")
//mfcs90ud.lib(dllmodul.obj) : error LNK2005: _DllMain@12 redefined... 遇到这问题，必须去掉_USERDLL宏才可以
BOOL APIENTRY DllMain(HANDLE hModule,DWORD ul_reason,LPVOID lpReserved)
{
	switch(ul_reason)
	{
	case	DLL_PROCESS_ATTACH:
		{
			WSADATA wsa={0};
			WSAStartup(0x0101,&wsa);
		}
		break;
	case	DLL_PROCESS_DETACH:
		WSACleanup();
		break;
	}
	return(TRUE);
}
#endif
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
king_spxa_api::king_spxa_api(void):pKSpxHandle(NULL)
{
	pKSpxHandle = new KSpxAPI;
}

king_spxa_api::~king_spxa_api(void)
{
	if(pKSpxHandle)
		delete pKSpxHandle;
	pKSpxHandle = NULL;
}
//////////////////////////////////////////////////////////////////////////////
void king_spxa_api::reset(void)
{
}

BOOL king_spxa_api::logon_server(char* toptrade_account, pstock_account_info account, int reqid, bool brelogin)
{
	return TRUE;
}

BOOL king_spxa_api::logout_server(int reqid)
{
	return TRUE;
}

BOOL king_spxa_api::query_position(int reqid)
{
	return TRUE;
}

BOOL king_spxa_api::place_order(Order ord,int reqid)
{
	return TRUE;
}

int king_spxa_api::cancel_order(CancelOrderInfo cancel,int reqid)
{
	return 1;
}



