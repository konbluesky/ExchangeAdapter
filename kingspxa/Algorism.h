#ifndef __Algorism_H__
#define __Algorism_H__

#include "scritical.h"


#ifdef __cplusplus
extern "C" {
#endif
	/*初始化KSINIT
	返回：true 成功，false失败
    */
    typedef bool   (_stdcall *KS_init)();
	
	/*释放资源
	返回：true 成功，false失败
    */
    typedef bool   (_stdcall *KS_Free)();
	
	/*获取发送报文
	输入：
	type:
	0  login1      第一次发login
	1  login2      解析第一次login应答包，返回下次通讯密钥，并生成第二次login包
	2  datasend    发送数据包
	3  ping        发送ping包
	4  Pingrsp     返回ping包
	key:  type为1时返回下次通讯密钥,type为2时传入通讯密钥，最大长度20
	inbuf:输入串，type=0、3、4时为空，type=1时为第一次login应答包，type=2时为需要发送的数据包，含业务包头和包体
	inlen:输入串长度
	输出：
	outbuf:处理后的输出串，含通讯包头等
	outlen:输出串长度
	msg:   错误信息，最大长度256
	返回：true 成功，false失败
    */	
    typedef bool   (_stdcall *KS_GetSendPack)(int type,char *inbuf,int inlen,char *outbuf,int *outlen,char *key,char *msg);
	
	/*获取接收报文
	输入：
	key:通讯密钥，最大长度20
	inbuf:输入串,接收报文
	inlen:输入串长度
	输出：
	outbuf:输出串，解析处理后的接收报文(spx竖杆包)
	outlen:输出串长度
	msg:错误信息，最大长度256
	返回：true 成功，false失败
	*/
    typedef bool   (_stdcall *KS_GetRecvPack)(char *inbuf,int inlen,char *outbuf,int *outlen,char *key,char *msg);
	
#ifdef __cplusplus
}
#endif

//#define BASE_ALGORISM_FRAME	100

class CAlgorism
{
private:
	CSCriticalSection m_cs;

	bool m_bLoaded;	
	HMODULE hmodule;
	TCHAR m_szDLLName[MAX_PATH];

	KS_init         pinit;
	KS_Free         pfree;
	KS_GetSendPack  pGetSendPack;
	KS_GetRecvPack  pGetRecvPack;
 

public:
	CAlgorism();
	~CAlgorism();

public:
	bool Load(TCHAR *szDLLName=NULL, TCHAR *szDLLName2=NULL);
	void Unload();

	bool Loaded() { return m_bLoaded; };
	
public:
	bool init ();
	bool GetSendPack(int type,char *inbuf,int inlen,char *outbuf,int *outlen,char *key,char *msg);
	bool GetRecvPack(char *inbuf,int inlen,char *outbuf,int *outlen,char *key,char *msg);
};

#endif //__Algorism_H__
