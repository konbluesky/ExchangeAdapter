
#ifndef __SLIST_H
#define __SLIST_H

#include "windows.h"
#include "stdio.h"
#include "stdlib.h"
#include "string"
#include "iostream"
#include "global.h"

#pragma warning( disable : 4786 )

#include "list"
#define	MAXCOLUMN				4096

using namespace std;
typedef list<string>STRINGLIST;
extern char *trim(char* in);

#define SOH_CHAR	'\x1'

class CSList  //处理以字符分割数据的类
{
public:
	static void GetAtIndex(const char *buffer, int index, char *szFieldValue, int nFieldSize, char mark='|');
	
public:
	// pen added at 20070831
	bool FillStrings2(const char *mainstring, char cMark); //填充类，mainstring包含分割符
	// end pen added at 20070831
	bool FillStrings(const char *mainstring, char mark='|',int len=0); //填充类，mainstring包含分割符
	int GetBuff(char *buffer,bool trimflag=false, char mark='|');  //得到有分割符的整个字符串
	int GetBuff2(char *buffer, int colStart, int colCount, bool trimflag=false, char mark='|');
	void Clear();    //清除
	int GetCount();  //数据个数
	void SetAt(char *buffer,int nIndex);  //修改第nIndex个数据项
	char * GetAt(int nIndex);   //得到某个数据项
	bool Add(char *buffer);     //添加数据项
	CSList();
	virtual ~CSList();

private:
	STRINGLIST m_lst;
	int m_count;
};

#endif 
