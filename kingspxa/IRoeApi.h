#pragma once
#include "../include/KSpx.h"

class IRoeCallbackHandle
{
public:
    /// <summary>
    /// 当接口产生异常时回调
    /// </summary>
    /// <param name="result"></param>
    /// <param name="requestID"></param>        
    virtual void OnError(ReturnInfo *pResult, int requestID){};

    /// <summary>
    /// 资金帐号登陆回调
    /// </summary>
    virtual void OnLogin(ReturnInfo *pResult, LoginInfo *pLoginInfo, int requestId){};

    /// <summary>
    /// 资金帐号登出回调
    /// </summary>
    virtual void OnLogout(ReturnInfo *pResult, LPSTR *pTradingAccount, int requestId){};

    /// <summary>
    /// 委托单状态回调
    /// </summary>
    virtual void OnOrderStatus(ReturnInfo *pResult, OrderStatus *pOrd,int count, int requestId){};

    /// <summary>
    /// 撤单回调
    /// </summary>
    /// <param name="ord"></param>       
    virtual int OnCancelOrder(ReturnInfo *pResult, int requestId)=0;
	
    /// <summary>
    /// 查询持仓/库存回调
    /// </summary>
    virtual void OnQueryPosition(ReturnInfo *pResult, PositionInformation *pPositionInformation, int count, int requestId){};

    /// <summary>
    /// 查询持仓/库存回调
    /// </summary>
    virtual void OnPing(ReturnInfo *pResult, int requestId){};
};


class IRoeApi
{
public:
    /// <summary>
    /// 注册回调
    /// </summary>
    /// <param name="client"></param>
    /// <returns></returns>      
    virtual void RegisterCallback(IRoeCallbackHandle *pIROECallback)=0;
   
    /// <summary>
    /// 卸载回调
    /// </summary>
    /// <param name="tradingAccount"></param>
    /// <param name="iROECallback"></param>
    virtual void UnRegisterCallback(IRoeCallbackHandle *pIROECallback)=0;

    /// <summary>
    /// 连接 
    /// </summary>
    /// <returns></returns>       
    virtual BOOL Connect()=0;

    /// <summary>
    /// 校验资金帐号
    /// </summary>
    /// <param name="account">资金帐号</param>
    /// <returns></returns>       
    virtual int Login(TradingAccount *pAccount, int requestId)=0;

    /// <summary>
    /// 登出资金账号
    /// </summary>
    /// <param name="account"></param>
    /// <param name="requestId"></param>
    virtual int Logout(TradingAccount *pAccount, int requestId)=0;

    /// <summary>
    /// 获取当前会话状态
    /// </summary>
    /// <returns></returns>
    virtual BOOL GetIsConnected()=0;

    /// <summary>
    /// 获取指定资金账号是否已登陆
    /// </summary>
    /// <param name="account"></param>
    /// <returns></returns>
    virtual BOOL GetIsLogined(LPSTR account)=0;

    /// <summary>
    /// 下单
    /// </summary>
    virtual int PlaceOrder(TradingAccount *pAccount, Order *pOrd, int requestId)=0;

    /// <summary>
    /// 撤单
    /// </summary>
    /// <param name="ord"></param>       
    virtual int CancelOrder(TradingAccount *pAccount,  CancelOrderInfo *pOrder, int requestId)=0;

    /// <summary>
    /// 查询持仓/库存
    /// </summary>
    /// <param name="account"></param>       
    virtual int QueryPosition(TradingAccount *pAccount, int requestId)=0;

    /// <summary>
    /// 委托查询
    /// </summary>
    /// <param name="account"></param>       
    virtual int QueryOrderStatus(TradingAccount *pAccount, int requestId)=0;

    /// <summary>
    /// Ping
    /// </summary>
    /// <param name="account"></param>       
    virtual int Ping(TradingAccount *pAccount, int requestId)=0;
};
