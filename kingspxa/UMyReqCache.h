#ifndef	__REQUEST_CACHE__
#define	__REQUEST_CACHE__
/////////////////////////////////////////////////////////////////////////////////////////////////
//这是一个请求队列,之所以不用list模版,一是为了锁定;
//2是为了协议修改(比如行情请求);
//3是需要首尾都可以插入(版面切换协议放队首,不急的信息放队尾),还要删除
class __declspec(novtable) UMyReqCache
{
public:
	typedef struct tagReqItem
	{
		tagReqItem*pNext;
		LPVOID data;
		LPVOID dataEx;
		DWORD	size;
		int		commandID;
		int		reuqestID;
	}REQ_ITEM,*PREQ_ITEM;

	PREQ_ITEM Read(void);	//读取

	void AddEnd(PREQ_ITEM pItem); //末尾追加(默认)
	void AddHead(PREQ_ITEM pItem); //头追加(加急模式)

	void Release(void);	//释放所有资源

	UMyReqCache(void);
	~UMyReqCache(void);

	PREQ_ITEM	__item__,__end__;
	CRITICAL_SECTION	__cs__;
};

#endif
