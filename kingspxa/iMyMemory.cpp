#include "stdafx.h"
#include "iMyMemory.h"


#ifdef _DEBUG
	#undef THIS_FILE
	static char THIS_FILE[] = __FILE__;
	#define new DEBUG_NEW
#endif
/////////////////////////////////////////////////////////////////////////////////////////////////
CRITICAL_SECTION __cs_logfile__;
TCHAR __logdata__[512*4]={0};
TCHAR __logdir__[256]={0};
/////////////////////////////////////////////////////////////////////////////////////////////////
template<class _Tc>class __declspec(novtable) template_malloc
{
#pragma pack(push,1)	//强制本结构1字节对齐
	typedef	struct tagItem
	{
		union
		{
			tagItem*next;
			long size;
		};
		_Tc	data;
	}ITEM,*PITEM;
#pragma pack(pop)
public:
	char*mlloc(void)
	{
		if(!__item__) return(NULL);
		PITEM pitem = __item__;
		__item__ = __item__->next;
		pitem->size = sizeof(_Tc);
		return(pitem->data);
	}
	void release(char*pdata)
	{
		PITEM pitem=(PITEM)pdata;
		memset(pitem,0,sizeof(ITEM));
#ifdef	WIN32
		EnterCriticalSection(__lock__);
#else
		pthread_mutex_lock(__lock__);
#endif
		pitem->next = __item__;
		__item__ = pitem;
#ifdef	WIN32
		LeaveCriticalSection(__lock__);
#else
		pthread_mutex_unlock(__lock__);
#endif
	}
	void clean(void)
	{
		char*lpData=NULL;
		for(PITEM pnext=NULL;__item__;__item__=pnext)
		{
			pnext = __item__->next;
			lpData = (char*)__item__;
			delete[] lpData;
		}
	}
	template_malloc(void):__item__(NULL)
	{
	}
	~template_malloc(void)
	{
		clean();
	}
	PITEM	__item__;
#ifdef	WIN32
	CRITICAL_SECTION*__lock__;
#else
	pthread_mutex_t*__lock__;
#endif
};

#define	__malloc__(T,P)\
	else if(lsize<=T)\
	{\
		lpData=P.mlloc();\
		_size = T;\
	}
class __declspec(novtable) memory_factory
{
public:
	char*malloc(long lsize,long*msize)
	{
		char*lpData=NULL;
		long _size(0);
#ifdef	WIN32
		EnterCriticalSection(&__lock__);
#else
		pthread_mutex_lock(&__lock__);
#endif
		if(lsize<=512)
		{
			lpData = _alloc_512.mlloc();
			_size = 512;
		}
		__malloc__(1024,_alloc_1k)
		__malloc__(1024*2L,_alloc_2k)
		__malloc__(1024*4L,_alloc_4k)
		__malloc__(1024*8L,_alloc_8k)
		__malloc__(1024*16L,_alloc_16k)
		__malloc__(1024*32L,_alloc_32k)
		__malloc__(1024*64L,_alloc_64k)
		__malloc__(1024*128L,_alloc_128k)
		__malloc__(1024*256L,_alloc_256k)
		__malloc__(1024*512L,_alloc_512k)
		__malloc__(1024*1024L,_alloc_1m)
		__malloc__(2*1024*1024L,_alloc_2m)
		__malloc__(3*1024*1024L,_alloc_3m)
		__malloc__(4*1024*1024L,_alloc_4m)
		__malloc__(8*1024*1024L,_alloc_8m)
		__malloc__(16*1024*1024L,_alloc_16m)
		__malloc__(32*1024*1024L,_alloc_32m)
		else
		{
			_size=lsize;
		}
#ifdef	WIN32
		LeaveCriticalSection(&__lock__);
#else
		pthread_mutex_unlock(&__lock__);
#endif
		if(!lpData)
		{
			char*lpMalloc=new char[_size+4];
			if(!lpMalloc) return(NULL);
			*(long*)lpMalloc = _size;
			memset(lpData=lpMalloc+4,0,_size);
		}
		if(msize) *msize = _size;
		return(lpData);
	}
	void release(char*lpData)
	{
		char*lpMalloc=lpData-4;
		switch(*(long*)lpMalloc)
		{
		case	512:		_alloc_512.release(lpMalloc); break;
		case	1024:		_alloc_1k.release(lpMalloc); break;
		case	1024*2L:	_alloc_2k.release(lpMalloc); break;
		case	1024*4L:	_alloc_4k.release(lpMalloc); break;
		case	1024*8L:	_alloc_8k.release(lpMalloc); break;
		case	1024*16L:	_alloc_16k.release(lpMalloc); break;
		case	1024*32L:	_alloc_32k.release(lpMalloc); break;
		case	1024*64L:	_alloc_64k.release(lpMalloc); break;
		case	1024*128L:	_alloc_128k.release(lpMalloc); break;
		case	1024*256L:	_alloc_256k.release(lpMalloc); break;
		case	1024*512L:	_alloc_512k.release(lpMalloc); break;
		case	1024*1024L:	_alloc_1m.release(lpMalloc); break;
		case	2*1024*1024L:	_alloc_2m.release(lpMalloc); break;
		case	3*1024*1024L:	_alloc_3m.release(lpMalloc); break;
		case	4*1024*1024L:	_alloc_4m.release(lpMalloc); break;
		case	8*1024*1024L:	_alloc_8m.release(lpMalloc); break;
		case	16*1024*1024L:	_alloc_16m.release(lpMalloc); break;
		case	32*1024*1024L:	_alloc_32m.release(lpMalloc); break;
		default: delete[] lpMalloc;
		}
	}
	void clean(void)
	{
#ifdef	WIN32
		EnterCriticalSection(&__lock__);
#else
		pthread_mutex_lock(&__lock__);
#endif
		_alloc_512.clean();
		_alloc_1k.clean();
		_alloc_2k.clean();
		_alloc_4k.clean();
		_alloc_8k.clean();
		_alloc_16k.clean();
		_alloc_32k.clean();
		_alloc_64k.clean();
		_alloc_128k.clean();
		_alloc_256k.clean();
		_alloc_512k.clean();
		_alloc_1m.clean();
		_alloc_2m.clean();
		_alloc_3m.clean();
		_alloc_4m.clean();
		_alloc_8m.clean();
		_alloc_16m.clean();
		_alloc_32m.clean();
#ifdef	WIN32
		LeaveCriticalSection(&__lock__);
#else
		pthread_mutex_unlock(&__lock__);
#endif
	}
	memory_factory(void)
	{
#ifdef	WIN32
		InitializeCriticalSection(&__lock__);
#else
		pthread_mutex_init(&__lock__,NULL);
#endif
		_alloc_512.__lock__ = _alloc_1k.__lock__ = _alloc_2k.__lock__ = 
		_alloc_4k.__lock__ = _alloc_8k.__lock__ = _alloc_16k.__lock__ = 
		_alloc_32k.__lock__ = _alloc_64k.__lock__ = _alloc_128k.__lock__ = 
		_alloc_256k.__lock__ = _alloc_512k.__lock__ = _alloc_1m.__lock__ = 
		_alloc_2m.__lock__ = _alloc_3m.__lock__ =  _alloc_4m.__lock__ = 
		_alloc_8m.__lock__ = _alloc_16m.__lock__ = _alloc_32m.__lock__ =
		&__lock__;
		InitializeCriticalSection(&__cs_logfile__);
	}
	~memory_factory(void)
	{
		clean();
#ifdef	WIN32
		DeleteCriticalSection(&__lock__);
#else
		pthread_mutex_destroy(&__lock__);
#endif
		DeleteCriticalSection(&__cs_logfile__);
	}
	template_malloc<char[512]>				_alloc_512;
	template_malloc<char[1024]>				_alloc_1k;
	template_malloc<char[1024*2L]>			_alloc_2k;
	template_malloc<char[1024*4L]>			_alloc_4k;
	template_malloc<char[1024*8L]>			_alloc_8k;
	template_malloc<char[1024*16L]>			_alloc_16k;
	template_malloc<char[1024*32L]>			_alloc_32k;
	template_malloc<char[1024*64L]>			_alloc_64k;
	template_malloc<char[1024*128L]>		_alloc_128k;
	template_malloc<char[1024*256L]>		_alloc_256k;
	template_malloc<char[1024*512L]>		_alloc_512k;
	template_malloc<char[1024*1024L]>		_alloc_1m;
	template_malloc<char[1024*1024L*2]>		_alloc_2m;
	template_malloc<char[1024*1024L*3]>		_alloc_3m;
	template_malloc<char[1024*1024L*4]>		_alloc_4m;
	template_malloc<char[1024*1024L*8]>		_alloc_8m;
	template_malloc<char[1024*1024L*16]>	_alloc_16m;
	template_malloc<char[1024*1024L*32]>	_alloc_32m;
#ifdef	WIN32
	CRITICAL_SECTION __lock__;
#else
	pthread_mutex_t __lock__;
#endif
};
/////////////////////////////////////////////////////////////////////////////////////////////////
static class memory_factory __memory__;
/////////////////////////////////////////////////////////////////////////////////////////////////
char*zwl_memory_malloc(long lsize,long*msize)
{
	return(__memory__.malloc(lsize,msize));
}

void zwl_memory_release(char*&lpData)
{
	if(lpData)
	{
		__memory__.release(lpData);
		lpData = NULL;
	}
}

void zwl_memory_clean(void)
{
	__memory__.clean();
}
/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
zwl_memory::zwl_memory(void):bufData(NULL),sizeData(0),sizeMalloc(0)
{
}

zwl_memory::~zwl_memory(void)
{
	zwl_memory_release(bufData);
}
/////////////////////////////////////////////////////////////////////////////////////////////////
LPSTR zwl_memory::GetBuf(long lsize)
{
	if((sizeData=lsize)>sizeMalloc)
	{
		zwl_memory_release(bufData);
		bufData=zwl_memory_malloc(lsize+16,&sizeMalloc);
	}
	if(bufData&&sizeMalloc) memset(bufData,0,sizeMalloc);
	return(bufData);
}

void zwl_memory::Release(void)
{
	zwl_memory_release(bufData);
	sizeData = sizeMalloc = 0;
}

LPSTR zwl_memory::Copy(LPCVOID lpData,long lsize)
{
	if(!GetBuf(lsize+16)) return(NULL); //避免字符串等不注意的情形，多分配一点
	if(lsize)
	{
		memcpy(bufData,lpData,lsize);
	}
	sizeData = lsize;
	return(bufData);
}

LPSTR zwl_memory::Append(LPCVOID lpData,long lsize)
{
	if(lsize)
	{
		if(!Extern(sizeData+lsize+16)) return(NULL);
		memcpy(bufData+sizeData,lpData,lsize);
		sizeData += lsize;
	}
	return(bufData);
}

LPSTR zwl_memory::Extern(long maxSize)
{
	if(maxSize>sizeMalloc)
	{
		long sizeNew(0);
		LPSTR bufNew=zwl_memory_malloc(maxSize,&sizeNew);
		if(!bufNew) return(NULL);
		if(bufData)
		{
			if(sizeData) memcpy(bufNew,bufData,sizeData);
			zwl_memory_release(bufData);
		}
		bufData = bufNew;
		sizeMalloc = sizeNew;
	}
	return(bufData);
}

zwl_memory::operator LPCSTR(void) const
{
	return(bufData);
}

const zwl_memory&zwl_memory::operator=(LPCSTR lpString)
{
	Copy(lpString,lstrlenA(lpString));
	return(*this);
}

const zwl_memory&zwl_memory::operator+=(LPCSTR lpString)
{
	Append(lpString,lstrlenA(lpString));
	return(*this);
}

void zwl_memory::Attach(zwl_memory*pMemory)
{
	zwl_memory_release(bufData);
	sizeData = pMemory->sizeData;
	sizeMalloc = pMemory->sizeMalloc;
	bufData = pMemory->bufData;
	pMemory->sizeData = pMemory->sizeMalloc = 0;
	pMemory->bufData = NULL;
}
/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
zwl_vector::zwl_vector(void):_items(NULL)
{
}

zwl_vector::~zwl_vector(void)
{
	zwl_vector::ReleaseAll();
}
/////////////////////////////////////////////////////////////////////////////////////////////////
LPSTR zwl_vector::Malloc(DWORD dwSize)
{
	PITEM pItem=new ITEM;
	if(pItem)
	{
		if(pItem->lpData=zwl_memory_malloc(dwSize))
		{
			pItem->pNext = _items;
			_items = pItem;
			return(pItem->lpData);
		}
		delete pItem;
	}
	return(NULL);
}

void zwl_vector::ReleaseAll(void)
{
	for(PITEM pNext=NULL;_items;_items=pNext)
	{
		pNext = _items->pNext;
		zwl_memory_release(_items->lpData);
		delete _items;
	}
}
/////////////////////////////////////////////////////////////////////////////////////////////////
void set_logdir(LPCTSTR lpHome,LPCTSTR strType)
{
	if(strType&&*strType)
	{
		wsprintf(__logdir__,TEXT("%s\\%s"),lpHome,strType);
		CreateDirectory(__logdir__,NULL);

		wsprintf(__logdir__,TEXT("%s\\%s\\logInfo"),lpHome,strType);
		CreateDirectory(__logdir__,NULL);
	}
	else
	{
		wsprintf(__logdir__,TEXT("%s\\logInfo"),lpHome);
		CreateDirectory(__logdir__,NULL);
	}
}

void write_log(LPCTSTR lpFormat,...)
{
	SYSTEMTIME st={0};
	GetLocalTime(&st);	

	HANDLE hFile=INVALID_HANDLE_VALUE;
	va_list arg_ptr;

	DWORD bytes(0);
	SECURITY_ATTRIBUTES sa={0};
	sa.nLength = sizeof(sa);

	EnterCriticalSection(&__cs_logfile__);

	wsprintf(__logdata__,TEXT("%s\\%04d%02d%02d.txt"),__logdir__,st.wYear,st.wMonth,st.wDay);
	hFile=CreateFile(__logdata__,GENERIC_WRITE,
		FILE_SHARE_READ|FILE_SHARE_WRITE,&sa,OPEN_ALWAYS,FILE_ATTRIBUTE_NORMAL,NULL);
	if(hFile!=INVALID_HANDLE_VALUE)
	{
		SetFilePointer(hFile,0,NULL,FILE_END);
		wsprintf(__logdata__,TEXT("%02d:%02d:%02d  "),st.wHour,st.wMinute,st.wSecond);
		
		va_start(arg_ptr,lpFormat);
		_vstprintf_s(__logdata__+9,sizeof(__logdata__) - 9,lpFormat,arg_ptr);
		va_end(arg_ptr);

		lstrcat(__logdata__,TEXT("\r\n"));
		WriteFile(hFile,__logdata__,sizeof(TCHAR)*lstrlen(__logdata__),&bytes,NULL);
		CloseHandle(hFile);
	}
	LeaveCriticalSection(&__cs_logfile__);
}
