#ifndef __SCRITICAL_H__
#define __SCRITICAL_H__

//////////////////////////////////////////////////////////////////////////////
// �ٽ���
//////////////////////////////////////////////////////////////////////////////

#include <afxwin.h>
class CSCriticalSection
{
	CRITICAL_SECTION m_cs;
	BOOL m_bInit;
public:
	CSCriticalSection()
	{
		InitializeCriticalSection( &m_cs );
		m_bInit = TRUE;
	}

	~CSCriticalSection()
	{
		DeleteCriticalSection( &m_cs );
		m_bInit = FALSE;
	}

	void Enter()
	{
		if(m_bInit)
			EnterCriticalSection( &m_cs );
	}

	void Leave()
	{
		if(m_bInit)
			LeaveCriticalSection( &m_cs );
	}
};

#endif //__SCRITICAL_H__

