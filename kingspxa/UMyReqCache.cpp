#include "stdafx.h"
#include "UMyReqCache.h"
#include "iMyMemory.h"

#ifdef _DEBUG
	#undef THIS_FILE
	static char THIS_FILE[] = __FILE__;
	#define new DEBUG_NEW
#endif

/////////////////////////////////////////////////////////////////////////////////////////////////
UMyReqCache::UMyReqCache(void):__item__(NULL),__end__(NULL)
{
	InitializeCriticalSection(&__cs__);
}

UMyReqCache::~UMyReqCache(void)
{
	UMyReqCache::Release();
	DeleteCriticalSection(&__cs__);
}
/////////////////////////////////////////////////////////////////////////////////////////////////
UMyReqCache::PREQ_ITEM UMyReqCache::Read(void)
{
	PREQ_ITEM pItem=NULL;
	EnterCriticalSection(&__cs__);
	if(pItem=__item__)
	{
		if(__item__==__end__) __item__ = __end__ = NULL;
		else __item__ = __item__->pNext;
	}
	LeaveCriticalSection(&__cs__);
	return(pItem);
}

void UMyReqCache::AddEnd(UMyReqCache::PREQ_ITEM pItem) //末尾追加(默认)
{
	pItem->pNext = NULL;
	EnterCriticalSection(&__cs__);
	if(__end__)	__end__->pNext = pItem;
	else __item__ = pItem;
	__end__ = pItem;
	LeaveCriticalSection(&__cs__);
}

void UMyReqCache::AddHead(UMyReqCache::PREQ_ITEM pItem) //头追加(加急模式)
{
	EnterCriticalSection(&__cs__);
	pItem->pNext = __item__;
	__item__ = pItem;
	if(!__end__) __end__ = pItem;
	LeaveCriticalSection(&__cs__);
}

void UMyReqCache::Release(void)
{
	PREQ_ITEM pItem=NULL;
	EnterCriticalSection(&__cs__);
	for(;__item__;__item__=pItem)
	{
		pItem = __item__->pNext;
		zwl_memory_release((LPSTR&)__item__->data);
		zwl_memory_release((LPSTR&)__item__->dataEx);
		zwl_memory_release((LPSTR&)__item__);
	}
	__end__ = NULL;
	LeaveCriticalSection(&__cs__);
}
