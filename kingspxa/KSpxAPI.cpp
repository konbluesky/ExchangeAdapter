#include "StdAfx.h"
#include "UMyReqCache.h"
#include "iMyMemory.h"
#include "global.h"
#include "KSpxAPI.h"
#include "SList.h"

KSpxAPI::KSpxAPI(void)
{
	m_IsLogined = FALSE;
	m_IsConnected = FALSE;
	m_hSocket = NULL;
	m_hExit = NULL;
	m_hCmdThread = NULL;
	m_hEventThread = NULL;
	memset(m_serverIP,0,sizeof(m_serverIP));
	m_serverPort = 0;
	memset(key_in,0,sizeof(key_in));
	Init();
}

KSpxAPI::~KSpxAPI(void)
{
	SetEvent(m_hExit);
	WaitForSingleObject(m_hCmdThread,INFINITE);
	WaitForSingleObject(m_hEventThread,INFINITE);
	_reqCmdCache.Release();
	_reqEventCache.Release();
}

void KSpxAPI::reset(void)
{
}

void KSpxAPI::RegisterCallback(IRoeCallbackHandle *pIROECallback)
{
	if(pIROECallback != NULL)
		m_allCallback[(DWORD)pIROECallback] = pIROECallback;
}

void KSpxAPI::UnRegisterCallback(IRoeCallbackHandle *pIROECallback)
{
	if(pIROECallback != NULL)
	{
		m_allCallback.erase((DWORD)pIROECallback);
	}
}

int KSpxAPI::Ping(TradingAccount *pAccount, int requestId)
{
	if(pAccount)
	{
		int commandID = CommandType_Ping;
		CStringA strCmd;
		return SendCommand(commandID,requestId,strCmd);	
	}

	return ERR;	
}

BOOL KSpxAPI::OnPing(int requestId,LPSCOMM_HEADER pheader,char* lineData,int size)
{
	return TRUE;
}

int KSpxAPI::Login(TradingAccount *pAccount, int requestId)
{
	if(m_IsLogined)
		return OK;

	if(pAccount)
	{
		memcpy(&m_tradingAccount,pAccount,sizeof(TradingAccount));

		int nRet = Login(requestId);
		if(OK == nRet)
		{
			m_bNeedLogin = TRUE;
		}
		else
		{
			m_bNeedLogin = FALSE;
		}

		return nRet;
	}

	return ERR;
}

int KSpxAPI::Login(int requestId)
{
	int commandID = CommandType_Login;
	CStringA strCmd;
	strCmd.FormatMessage("R|%5%|%1!d!|%2!d!|%3%|%4!d!|%5%|%6%|%7%|%8%|%9%|%5%|",
		requestId,
		commandID,
		m_tradingAccount.BranchNO,
		MARKETID,
		STRING_EMPTY,
		m_tradingAccount.UserAccount,
		m_tradingAccount.Password,
		ORDER_METHOD,
		STRING_EMPTY
		);

	return SendCommand(commandID,requestId,strCmd);	
}

BOOL KSpxAPI::OnLogin(int requestId,LPSCOMM_HEADER pheader,char* lineData,int size)
{
	ReturnInfo *pResult = (ReturnInfo *)zwl_memory_malloc(sizeof(ReturnInfo));
	LoginInfo_Kspx *pLoginInfo = (LoginInfo_Kspx *)zwl_memory_malloc(sizeof(LoginInfo_Kspx));
	pLoginInfo->IsLogined = FALSE;
	strcpy(pLoginInfo->UserAccount,m_tradingAccount.UserAccount);
	pResult->IsSuccess = FALSE;
	pResult->ErrorCode = 1;
	pResult->ErrorMessage = NULL;

	BOOL bRet = TRUE;
	int count = 1;
	int i_prev(0),i_this(0),index(0),i(0);
	char tmpData[50]={0};
	do
	{ 
		if(lineData[i_this]=='|')
		{
			memset(tmpData,0x00,sizeof(tmpData));
			memcpy(tmpData,&lineData[i_prev],i_this-i_prev);
			i_prev=i_this+1;
			switch(index)
			{
			case 0:
				if(strcmp(tmpData,"A") == 0)
				{
					index++;
				}
				else
				{
					i_this = size;
				}
				break;
			case 1:
			case 2:
				//忽略
				index++;
				break;
			case 3:
				if(strcmp(tmpData,"Y") == 0)
				{
					index++;
				}
				else
				{
					i_this = size;
				}
				break;
			case 4:
				pLoginInfo->IsLogined = 1;
				pResult->IsSuccess = 1;
				pResult->ErrorCode = 0;
				m_IsLogined = TRUE;
				index++;
				break;
			}
		}
		i_this++;
		if( index>4 || i_this>size) break;
	}while(TRUE);

	bRet = FireEvent(CommandType_Login,requestId,count,(LPVOID *)pLoginInfo,pResult);

	return bRet;
}

int KSpxAPI::Logout(TradingAccount *pAccount, int requestId)
{
	if(pAccount)
	{
		int commandID = CommandType_Logout;

		CStringA strCmd;
		strCmd.FormatMessage("R|%5%|%1!d!|%2!d!|%3%|%4!d!|%5%|%6%|%7!d!|%8%|",
			requestId,
			commandID,
			pAccount->BranchNO,
			MARKETID,
			STRING_EMPTY,
			pAccount->UserAccount,
			0,
			ORDER_METHOD
			);

		return SendCommand(commandID,requestId,strCmd);
	}

	return ERR;
}

BOOL KSpxAPI::OnLogout(int requestId,LPSCOMM_HEADER pheader,char* lineData,int size)
{
	BOOL bRet = TRUE;
	m_bNeedLogin = FALSE;

	ReturnInfo *pResult = (ReturnInfo *)zwl_memory_malloc(sizeof(ReturnInfo));
	char *pTradingAccount = (char *)zwl_memory_malloc(100);
	strcpy(pTradingAccount,m_tradingAccount.UserAccount);
	pResult->IsSuccess = FALSE;
	pResult->ErrorCode = 1;
	pResult->ErrorMessage = NULL;

	int count = 1;
	int i_prev(0),i_this(0),index(0),i(0);
	char tmpData[50]={0};
	do
	{ 
		if(lineData[i_this]=='|')
		{
			memset(tmpData,0x00,sizeof(tmpData));
			memcpy(tmpData,&lineData[i_prev],i_this-i_prev);
			i_prev=i_this+1;
			switch(index)
			{
			case 0:
				if(strcmp(tmpData,"A") == 0)
				{
					index++;
				}
				else
				{
					i_this = size;
				}
				break;
			case 1:
			case 2:
				//忽略
				index++;
				break;
			case 3:
				if(strcmp(tmpData,"Y") == 0)
				{
					pResult->IsSuccess = 1;
					pResult->ErrorCode = 0;
					index++;
				}
				else
				{
					i_this = size;
				}
				break;
			}
		}
		i_this++;
		if( index>3 || i_this>size) break;
	}while(TRUE);

	bRet = FireEvent(CommandType_Login,requestId,count,(LPVOID *)pTradingAccount,pResult);

	return bRet;
}

BOOL KSpxAPI::GetIsConnected()
{
	return m_IsConnected;
}


BOOL KSpxAPI::GetIsLogined(LPSTR account)
{
	return m_IsLogined;
}

int KSpxAPI::PlaceOrder(TradingAccount *pAccount, Order *pOrd, int requestId)
{
	if(pAccount)
	{
		int commandID = CommandType_PlaceOrder;
		char price[20] = {0};
		sprintf(price,"%f",pOrd->Price);

		CStringA strCmd;
		strCmd.FormatMessage("R|%5%|%1!d!|%2!d!|%3%||%5%|%6%|%7%|%8!d!|%9!d!|%10%|%11%||||",
			requestId,
			commandID,
			pAccount->BranchNO,
			MARKETID,
			STRING_EMPTY,
			pOrd->UserAccount,
			pOrd->Code,
			1,
			pOrd->Amount,
			price,
			ORDER_METHOD
			);

		return SendCommand(commandID,requestId,strCmd);
	}

	return ERR;
}

BOOL KSpxAPI::OnPlaceOrder(int requestId,LPSCOMM_HEADER pheader,char* lineData,int size)
{
	BOOL bRet = TRUE;
	m_bNeedLogin = FALSE;

	ReturnInfo *pResult = (ReturnInfo *)zwl_memory_malloc(sizeof(ReturnInfo));
	OrderStatus *pOrdStatus = (OrderStatus *)zwl_memory_malloc(sizeof(OrderStatus));
	memset(pOrdStatus,0,sizeof(OrderStatus));
	pResult->IsSuccess = FALSE;
	pResult->ErrorCode = 1;
	pResult->ErrorMessage = NULL;
	strcpy(pOrdStatus->account,m_tradingAccount.UserAccount);
	int count = 1;
	int i_prev(0),i_this(0),index(0),i(0);
	char tmpData[50]={0};
	do
	{ 
		if(lineData[i_this]=='|')
		{
			memset(tmpData,0x00,sizeof(tmpData));
			memcpy(tmpData,&lineData[i_prev],i_this-i_prev);
			i_prev=i_this+1;
			switch(index)
			{
			case 0:
				if(strcmp(tmpData,"A") == 0)
				{
					index++;
				}
				else
				{
					i_this = size;
				}
				break;
			case 1:
			case 2:
				//忽略
				index++;
				break;
			case 3:
				if(strcmp(tmpData,"Y") == 0)
				{
					pResult->IsSuccess = 1;
					pResult->ErrorCode = 0;
					index++;
				}
				else
				{
					i_this = size;
				}
				break;
			case 4:
				strcpy(pOrdStatus->OrderID,tmpData);
				index++;
				break;
			case 5:
				strcpy(pOrdStatus->OrderRef,tmpData);
				index++;
				break;
			}
		}
		i_this++;
		if( index>5 || i_this>size) break;
	}while(TRUE);

	bRet = FireEvent(CommandType_Login,requestId,count,(LPVOID *)pOrdStatus,pResult);

	return bRet;
}
   
int KSpxAPI::CancelOrder(TradingAccount *pAccount,  CancelOrderInfo *pOrder, int requestId)
{
	if(pAccount)
	{
		int commandID = CommandType_CancelOrder;

		CStringA strCmd;
		strCmd.FormatMessage("R|%5%|%1!d!|%2!d!|%3%|%4!d!|%5%|%6%|%7%|%8%|",
			requestId,
			commandID,
			pAccount->BranchNO,
			MARKETID,
			STRING_EMPTY,
			pAccount->UserAccount,
			pOrder->OrderID,
			ORDER_METHOD
			);

		return SendCommand(commandID,requestId,strCmd);
	}

	return ERR;
}

BOOL KSpxAPI::OnCancelOrder(int requestId,LPSCOMM_HEADER pheader,char* lineData,int size)
{
	BOOL bRet = TRUE;
	m_bNeedLogin = FALSE;

	ReturnInfo *pResult = (ReturnInfo *)zwl_memory_malloc(sizeof(ReturnInfo));
	pResult->IsSuccess = FALSE;
	pResult->ErrorCode = 1;
	pResult->ErrorMessage = NULL;

	int count = 1;
	int i_prev(0),i_this(0),index(0),i(0);
	char tmpData[50]={0};
	do
	{ 
		if(lineData[i_this]=='|')
		{
			memset(tmpData,0x00,sizeof(tmpData));
			memcpy(tmpData,&lineData[i_prev],i_this-i_prev);
			i_prev=i_this+1;
			switch(index)
			{
			case 0:
				if(strcmp(tmpData,"A") == 0)
				{
					index++;
				}
				else
				{
					i_this = size;
				}
				break;
			case 1:
			case 2:
				//忽略
				index++;
				break;
			case 3:
				if(strcmp(tmpData,"Y") == 0)
				{
					pResult->IsSuccess = 1;
					pResult->ErrorCode = 0;
					index++;
				}
				else
				{
					i_this = size;
				}
				break;
			}
		}
		i_this++;
		if( index>3 || i_this>size) break;
	}while(TRUE);

	bRet = FireEvent(CommandType_Login,requestId,count,NULL,pResult);

	return bRet;
}
   
int KSpxAPI::QueryPosition(TradingAccount *pAccount, int requestId)
{
	if(pAccount)
	{
		int commandID = CommandType_QueryPosition;
		int action = 1;
		CStringA strCmd;
		strCmd.FormatMessage("R|%5%|%1!d!|%2!d!|%3%|%4!d!|%5%|%6%|||||||%7!d!|%8%||||",
			requestId,
			commandID,
			pAccount->BranchNO,
			MARKETID,
			STRING_EMPTY,
			pAccount->UserAccount,
			action,//操作功能: 1明细、2汇总
			ORDER_METHOD
			);

		return SendCommand(commandID,requestId,strCmd);
	}

	return ERR;
}

BOOL KSpxAPI::OnQueryPosition(int requestId,LPSCOMM_HEADER pheader,char* lineData,int size)
{
	BOOL bRet = TRUE;
	m_bNeedLogin = FALSE;

	ReturnInfo *pResult = (ReturnInfo *)zwl_memory_malloc(sizeof(ReturnInfo));
	pResult->IsSuccess = FALSE;
	pResult->ErrorCode = 1;
	pResult->ErrorMessage = NULL;
	PositionInformation_Kspx *pPositionInformation = NULL;

	int count = pheader->result_count;
	if(count > 0)
	{
		CSList  anslist;
		anslist.FillStrings(lineData,'\0',size);
		pPositionInformation = (PositionInformation_Kspx *)zwl_memory_malloc(count * sizeof(PositionInformation_Kspx)); 
		for(int i =0;i<count;i++)
		{
			trim(anslist.GetAt(i));
			if(strlen(anslist.GetAt(i))>0)
			{
				GetNextPosition(anslist.GetAt(i),strlen(anslist.GetAt(i)),pPositionInformation+i);
			}
		}
	}

	bRet = FireEvent(CommandType_Login,requestId,count,(LPVOID *)pPositionInformation,pResult);

	return bRet;
}

int KSpxAPI::GetNextPosition(char* lineData,int size,PositionInformation_Kspx *pPositionInformation)
{
	int count = 0;
	int i_prev(0),i_this(0),index(0),i(0);
	char tmpData[50]={0};
	do
	{ 
		if(lineData[i_this]=='|')
		{
			memset(tmpData,0x00,sizeof(tmpData));
			memcpy(tmpData,&lineData[i_prev],i_this-i_prev);
			i_prev=i_this+1;
			switch(index)
			{
			case 4:
				//股东帐号
				strcpy(pPositionInformation->UserAccount,tmpData);
				pPositionInformation->Unit = Unit_Hand;
				index++;
				break;
			case 5:
				//证券代码
				strcpy(pPositionInformation->Symbol,tmpData);
				index++;
				break;
			case 7:
				//余额(加上当日成交)
				pPositionInformation->StockVolumn = (float)atof(tmpData);
				index++;
				break;
			case 8:
				//可用余额
				pPositionInformation->StockCanSoldVolumn = (float)atof(tmpData);
				index++;
				break;
			case 10:
				//浮动盈亏
				pPositionInformation->StockTradingProfitLoss = (float)atof(tmpData);
				index++;
				break;
			case 11:
				//成本价格
				pPositionInformation->StockCost = (float)atof(tmpData);
				index++;
				break;
			case 12:
				//行情最新价
				pPositionInformation->StockNewDealPrise = (float)atof(tmpData);
				index++;
				break;
			case 13:
				//昨日库存数量
				pPositionInformation->YdPosition = atof(tmpData);
				index++;
				break;
			default:
				index++;
				break;
			}
		}
		i_this++;
		if( index>13 || i_this>size) break;
	}while(TRUE);

	return i_this;
}

BOOL KSpxAPI::QueryOrderStatus(TradingAccount *pAccount, int requestId)
{
	if(pAccount)
	{
		int commandID = CommandType_QueryOrderStatus;
		int sortType = 0; //排序方式: 0-市场代码、1-委托时间、2-委托时间倒序
		CStringA strCmd;
		strCmd.FormatMessage("R|%5%|%1!d!|%2!d!|%3%|%4!d!|%5%|%6%||||||||%7%||%8!d!|||||||||||||",
			requestId,
			commandID,
			pAccount->BranchNO,
			MARKETID,
			STRING_EMPTY,
			pAccount->UserAccount,
			ORDER_METHOD,
			sortType
			);

		return SendCommand(commandID,requestId,strCmd);
	}

	return ERR;
}

BOOL KSpxAPI::OnQueryOrderStatus(int requestId,LPSCOMM_HEADER pheader,char* lineData,int size)
{
	BOOL bRet = TRUE;
	m_bNeedLogin = FALSE;

	ReturnInfo *pResult = (ReturnInfo *)zwl_memory_malloc(sizeof(ReturnInfo));
	pResult->IsSuccess = FALSE;
	pResult->ErrorCode = 1;
	pResult->ErrorMessage = NULL;
	OrderStatus *pOrderStatus = NULL;

	int count = pheader->result_count;
	if(count > 0)
	{
		CSList  anslist;
		anslist.FillStrings(lineData,'\0',size);
		pOrderStatus = (OrderStatus *)zwl_memory_malloc(count * sizeof(OrderStatus)); 
		for(int i =0;i<count;i++)
		{
			trim(anslist.GetAt(i));
			if(strlen(anslist.GetAt(i))>0)
			{
				GetNextOrderStatus(anslist.GetAt(i),strlen(anslist.GetAt(i)),pOrderStatus+i);
			}
		}
	}

	bRet = FireEvent(CommandType_Login,requestId,count,(LPVOID *)pOrderStatus,pResult);

	return bRet;
}

OrderState GetOrderStatus(char code)
{
	OrderState state = OrderState_Unknow;
	switch(code)
	{
	case '\0':
		state = OrderState_UnAck;
		break;
	case '0':
		state = OrderState_UnAck;
		break;
	case '1':
		state = OrderState_ToBeReported;
		break;
	case '2':
		state = OrderState_Reported;
		break;
	case '3':
		state = OrderState_ReportedCancel;
		break;
	case '4':
		state = OrderState_PartFillAndCancelling;
		break;
	case '5':
		state = OrderState_PartFilledAndCancelled;
		break;
	case '6':
		state = OrderState_Canceled;
		break;
	case '7':
		state = OrderState_PartFilled;
		break;
	case '8':
		state = OrderState_Filled;
		break;
	case '9':
		state = OrderState_PlaceRejected;
		break;
	case 'A':
		state = OrderState_Canceled;
		break;
	case 'D':
		state = OrderState_ToBeConfirmed;
		break;
	case 'E':
		state = OrderState_PlaceRejected;
		break;
	}

	return state;
}

BOOL KSpxAPI::GetNextOrderStatus(char* lineData,int size,OrderStatus *pOrderStatus)
{
	BOOL bRet = TRUE;
	int count = 0;
	int i_prev(0),i_this(0),index(0),i(0);
	char tmpData[50]={0};
	do
	{ 
		if(lineData[i_this]=='|')
		{
			memset(tmpData,0x00,sizeof(tmpData));
			memcpy(tmpData,&lineData[i_prev],i_this-i_prev);
			i_prev=i_this+1;
			switch(index)
			{
			case 3://股东帐号
				strcpy(pOrderStatus->account,tmpData);
				index++;
				break;
			case 9://合同号
				strcpy(pOrderStatus->OrderID,tmpData);
				index++;
				break;
			case 13://成交状态名称
				strcpy(pOrderStatus->Message,tmpData);
				index++;
				break;			
			case 14://委托时间
				strcpy(pOrderStatus->TimeStamp,tmpData);
				index++;
				break;			
			case 20://成交状态标志
				pOrderStatus->OrderState = GetOrderStatus(tmpData[0]);
				index++;
				break;
			default:
				//忽略
				index++;
				break;
			}
		}
		i_this++;
		if( index>20 || i_this>size) break;
	}while(TRUE);

	return bRet;
}


BOOL KSpxAPI::ReadConfig()
{
	char filename[] = "KingSpxSetting.ini";
	int count=0;

	OFSTRUCT OpenFileStructure;
	OpenFile(filename, &OpenFileStructure, OF_EXIST);
	if(OpenFileStructure.nErrCode == 2) // 2 FILE_NOT_FOUND
	{
		return FALSE;
	}
	
    CfgReadString("server","ip",m_serverIP,filename,"58.246.146.130");
    m_serverPort=CfgReadLong("server","port",filename,"9022");

	return TRUE;
}


BOOL KSpxAPI::Init()
{
	if(!ReadConfig())
		return FALSE;

	if (!g_Algorism.Load()) {
		return FALSE;
	}
	
	if(!g_Algorism.init())
	{
		g_Algorism.Unload();
		return FALSE;
	}
	
	startDataThread();

	return TRUE;
}


int KSpxAPI::FormAppPack(int commandID,int requestID,const char *cmd,char *sendbuffer, int *sendlen)
{
	char msg[256]={0};
	int ComnLen=0;

	LPSCOMM_HEADER pcomheader;
	LPSAPP_HEADER  appheader;
	
    pcomheader=(LPSCOMM_HEADER)sendbuffer;
	//填写上下文关联
    pcomheader->cmd = HTTX_DATACMD;
	pcomheader->userdata=requestID;
	
	
	appheader=(LPSAPP_HEADER)(sendbuffer + sizeof(*pcomheader));
    appheader->func_no= commandID;
//  memcpy(appheader->entrust_type,g_Vars->m_wtfs,sizeof(appheader->entrust_type)-1);
	strcpy(appheader->entrust_type,"ZZWT");
	strcpy(appheader->cust_no,m_tradingAccount.UserAccount);
	strcpy(appheader->password,m_tradingAccount.Password);	
	strcpy(appheader->branch,m_tradingAccount.BranchNO);
	
    //包体
	memcpy(sendbuffer+sizeof(SCOMM_HEADER)+sizeof(SAPP_HEADER),cmd,strlen(cmd));
	ComnLen= sizeof(SAPP_HEADER)+sizeof(SCOMM_HEADER)+strlen(cmd);
	int type = 2;
	if(commandID == CommandType_Ping)
		type = 3;
	if(!g_Algorism.GetSendPack(type,sendbuffer,ComnLen,sendbuffer,sendlen,key_in,msg))
	{
		return -1;
	}

	return 0;
}

BOOL KSpxAPI::RecvAppPack(char *recvbuf,int *buflen)
{
	BOOL bRet = FALSE;
	int  CommLen =0; 
	char msg[256]={0};
	
    LPSCOMM_HEADER pcomheader=(LPSCOMM_HEADER)recvbuf;

	//先收包头
	if(RecvDataEx(recvbuf, sizeof(SCOMM_HEADER)))
	{
		CommLen = pcomheader->len;
		//再收包体
		if(RecvDataEx(recvbuf+sizeof(SCOMM_HEADER), CommLen))
		{
			if(pcomheader->cmd != HTTX_DATARSPCMD && pcomheader->cmd!= HTTX_PINGCMD
				&& pcomheader->cmd!= HTTX_PINGRSPCMD)
			{
			   return FALSE;
			}

			if(!g_Algorism.GetRecvPack(recvbuf,sizeof(SCOMM_HEADER)+CommLen,recvbuf+sizeof(SCOMM_HEADER),buflen,key_in,msg))
			{
				bRet = FALSE;
			}
			else
			{
				bRet = TRUE;
			}
		}
				
	}
	
	return bRet;
}

BOOL KSpxAPI::ToConnect()
{
	char buf[MAXPACKSIZE]={0};        
	int  len=0;
	bool bRet = true;
	int  nRet=-1;
	int  CommLen =0;
	char msg[256]={0};

    LPSCOMM_HEADER pcomheader=(LPSCOMM_HEADER)buf;
    LPCONNECT_REQ  pconnheader=(LPCONNECT_REQ)(buf + sizeof(SCOMM_HEADER));
	LPCONNECT_ANS  pansheader=(LPCONNECT_ANS)(buf + sizeof(SCOMM_HEADER));
	int kk= sizeof(SCOMM_HEADER);

	//第一次登录
    if(!g_Algorism.GetSendPack(0,buf,0,buf,&len,"",msg))
	{
		return false;
	}

	if (!SendData(buf,len))
	{
		return false;
	}

	Sleep(1000);
	nRet = RecvData(buf, MAXPACKSIZE);
	if(nRet<sizeof(SCOMM_HEADER) || pcomheader->error != 0)
	{
		return false;
	}

	//获取下次通讯密钥，并发送第二次登录包
	if(!g_Algorism.GetSendPack(1,buf,nRet,buf,&len,key_in,msg))
	{
		return false;
	}	
				
	if (!SendData(buf,len))
	{
		return false;
	}

	Sleep(1000);
	nRet =  RecvData(buf, MAXPACKSIZE);
	if(nRet<sizeof(SCOMM_HEADER) || pcomheader->error != 0)
	{
		return false;
	}

	return bRet;
}


void KSpxAPI::RunCmdMain()
{
	WORD checkTimes(100);
	UMyReqCache::PREQ_ITEM pItem=NULL;
	int waitTime = 1;
	do
	{
		if(!m_IsConnected)
		{
			waitTime = 1000;
			continue;
		}
		waitTime = 1;

		if(++checkTimes>10)
		{
			if(!check_connect())
			{
				WaitForSingleObject(m_hExit,1000L);
				continue;
			}
			checkTimes = 0;
		}
		
		if(!(pItem=_reqCmdCache.Read())) continue;

		if(SendData(pItem->data,pItem->size))
		{		
			ProcessCmd(pItem->commandID,pItem->reuqestID);
			zwl_memory_release((LPSTR&)pItem->data);
			zwl_memory_release((LPSTR&)pItem);
		}
		else
		{
			checkTimes = 100;
			_reqCmdCache.AddHead(pItem);
			CloseSocket();
		}

	}while(WaitForSingleObject(m_hExit,waitTime)!=WAIT_OBJECT_0);
}

void KSpxAPI::RunEventMain()
{
	WORD checkTimes(100);
	UMyReqCache::PREQ_ITEM pItem=NULL;

	do
	{	
		if(!(pItem=_reqEventCache.Read())) continue;

		std::map<DWORD,IRoeCallbackHandle *>::const_iterator map_iter = m_allCallback.begin();
		switch(pItem->commandID)
		{
		case CommandType_Error:
			for(;map_iter != m_allCallback.end(); map_iter++)
			{
				map_iter->second->OnError((ReturnInfo *)pItem->data,pItem->reuqestID);
			}
			break;
		case CommandType_Login:
			for(;map_iter != m_allCallback.end(); map_iter++)
			{
				map_iter->second->OnLogin((ReturnInfo *)pItem->data,(LoginInfo *)pItem->dataEx,pItem->reuqestID);
			}
			break;
		case CommandType_Logout:
			for(;map_iter != m_allCallback.end(); map_iter++)
			{
				map_iter->second->OnLogout((ReturnInfo *)pItem->data,(LPSTR *)pItem->dataEx,pItem->reuqestID);
			}
			break;
		case CommandType_QueryOrderStatus:
		case CommandType_PlaceOrder:
		case CommandType_CancelOrder:
			for(;map_iter != m_allCallback.end(); map_iter++)
			{
				map_iter->second->OnOrderStatus((ReturnInfo *)pItem->data,(OrderStatus *)pItem->dataEx,pItem->size,pItem->reuqestID);
			}
			break;
		case CommandType_QueryPosition:
			for(;map_iter != m_allCallback.end(); map_iter++)
			{
				map_iter->second->OnQueryPosition((ReturnInfo *)pItem->data,(PositionInformation *)pItem->dataEx,pItem->size,pItem->reuqestID);
			}
			break;

		}

		zwl_memory_release((LPSTR&)pItem->data);
		zwl_memory_release((LPSTR&)pItem->dataEx);
		zwl_memory_release((LPSTR&)pItem);

	}while(WaitForSingleObject(m_hExit,1L)!=WAIT_OBJECT_0);
}

BOOL KSpxAPI::ProcessCmd(int commandID,int reuqestID)
{
	int size = MAXPACKSIZE;
	char* lpData = (char*)zwl_memory_malloc(size);
	int sizeRecv = 0;
	BOOL bRet = FALSE;
	if(RecvAppPack(lpData,&sizeRecv))
	{
		LPSCOMM_HEADER pheader=(LPSCOMM_HEADER)(lpData);
		char *lineData = lpData+sizeof(SCOMM_HEADER);
		switch(commandID)
		{
		case CommandType_Ping:
			bRet = OnPing(reuqestID,pheader,lineData,sizeRecv);
			break;
		case CommandType_Login:
			bRet = OnLogin(reuqestID,pheader,lineData,sizeRecv);
			break;
		case CommandType_Logout:
			bRet = OnLogout(reuqestID,pheader,lineData,sizeRecv);
			break;
		case CommandType_PlaceOrder:
			bRet = OnPlaceOrder(reuqestID,pheader,lineData,sizeRecv);
			break;
		case CommandType_CancelOrder:
			bRet = OnCancelOrder(reuqestID,pheader,lineData,sizeRecv);
			break;
		case CommandType_QueryPosition:
			bRet = OnQueryPosition(reuqestID,pheader,lineData,sizeRecv);
			break;
		case CommandType_QueryOrderStatus:
			bRet = OnQueryOrderStatus(reuqestID,pheader,lineData,sizeRecv);
			break;
		}
	}

	zwl_memory_release(lpData);

	return bRet;
}

BOOL KSpxAPI::SendCommand(int commandID,int requestID,CStringA &strCmd)
{
	UMyReqCache::PREQ_ITEM lpItem = (UMyReqCache::PREQ_ITEM)zwl_memory_malloc(sizeof(UMyReqCache::REQ_ITEM));
	lpItem->commandID = commandID;
	lpItem->reuqestID = requestID;
	lpItem->dataEx = NULL;

	int size = MAXPACKSIZE;
	lpItem->data = zwl_memory_malloc(size);
	FormAppPack(commandID,requestID,strCmd.GetString(),(char*)lpItem->data,&size);
	lpItem->size = size;

	_reqCmdCache.AddEnd(lpItem);
	
	return TRUE;
}

BOOL KSpxAPI::FireEvent(int commandID,int requestID,int dataSize,LPVOID *pData,ReturnInfo *pResult)
{
	UMyReqCache::PREQ_ITEM lpItem = (UMyReqCache::PREQ_ITEM)zwl_memory_malloc(sizeof(UMyReqCache::REQ_ITEM));

	lpItem->commandID = commandID;
	lpItem->reuqestID = requestID;
	lpItem->data = (LPVOID)pResult;
	lpItem->size = dataSize;
	lpItem->dataEx = pData;

	_reqEventCache.AddEnd(lpItem);
	
	return TRUE;
}

BOOL KSpxAPI::InitSocket()
{
	m_hSocket=socket(PF_INET,SOCK_STREAM,0);
	if(m_hSocket==INVALID_SOCKET)
	{
		return(FALSE);
	}

	long noDelay(1);
	setsockopt(m_hSocket,IPPROTO_TCP,TCP_NODELAY,(LPSTR)&noDelay,sizeof(long));

	long tmSend(15*1000L),tmRecv(15*1000L);
	setsockopt(m_hSocket,SOL_SOCKET,SO_SNDTIMEO,(LPSTR)&tmSend,sizeof(long));
	setsockopt(m_hSocket,SOL_SOCKET,SO_RCVTIMEO,(LPSTR)&tmRecv,sizeof(long));

//connect srv
	WORD wPortConn= m_serverPort;
	char *lpServer= m_serverIP;
	sockaddr_in	remote={0};
	remote.sin_family = AF_INET;
	remote.sin_port = htons(wPortConn);

	LPHOSTENT lphost=NULL;
	if((remote.sin_addr.s_addr=inet_addr(lpServer))==INADDR_NONE)
	{
		if(lphost=gethostbyname(lpServer))
			remote.sin_addr.s_addr = ((LPIN_ADDR)lphost->h_addr)->s_addr;
	}
	
	bool ret = true;;  
	int iMode = 1; //0：阻塞
	ioctlsocket(m_hSocket, FIONBIO, (u_long FAR*) &iMode); //设置为非阻塞模式  
	if( connect(m_hSocket, (struct sockaddr *)&remote, sizeof(remote)) == -1)  
	{  
		timeval tm;  
		fd_set set;  
		tm.tv_sec = 3;  
		tm.tv_usec = 0;  
		FD_ZERO(&set);  
		FD_SET(m_hSocket, &set);  
		if( select(m_hSocket+1, NULL, &set, NULL, &tm) < 0)  
		{  
			ret = false;  
		} 
	}  
	iMode = 0;  
	ioctlsocket(m_hSocket, FIONBIO, (u_long FAR*) &iMode); //设置为阻塞模式  

	if(!ret)   
	{
		m_hSocket = INVALID_SOCKET;
		return FALSE;
	}

	return TRUE;
}

BOOL KSpxAPI::Connect()
{
	if(InitSocket() && ToConnect())
		m_IsConnected = TRUE;
	else
		m_IsConnected = FALSE;

	return m_IsConnected;
}

BOOL KSpxAPI::check_connect()
{
	if(m_hSocket!=INVALID_SOCKET)
	{
		fd_set fd={0};
		timeval tmout={0,100};
		FD_ZERO(&fd);
		FD_SET(m_hSocket,&fd);
		int rtCode(select(m_hSocket+1,&fd,NULL,NULL,&tmout));
		if(!rtCode||rtCode==1)
			return(TRUE);
	}
	CloseSocket();
	
	if(InitSocket() && ToConnect())
		m_IsConnected = TRUE;
	else
		m_IsConnected = FALSE;
	
	if(m_bNeedLogin && OK != Login(0))
	{
		m_IsLogined = FALSE;
		m_IsConnected = FALSE;
		CloseSocket();
		//重新连接
	}

	return m_IsConnected;
}

VOID KSpxAPI::CloseSocket()
{
	closesocket(m_hSocket);
	m_hSocket = INVALID_SOCKET;
}

BOOL KSpxAPI::SendData(LPCVOID lpData,long sizeData)
{
	if(m_hSocket == INVALID_SOCKET)
		return FALSE;

	long lBytes(0),l(0);
	for(;sizeData>0;)
	{
		lBytes = send(m_hSocket,(LPCSTR)lpData+l,sizeData,0);
		if(lBytes==SOCKET_ERROR)
		{
			if(WSAGetLastError()==WSAEWOULDBLOCK)
			{
				Sleep(100);
				continue;
			}
			return(FALSE);
		}
		else
		{
			if(lBytes<1)	//连接异常或者被断开
				return(FALSE);
			sizeData -= lBytes;
			l += lBytes;
		}
		Sleep(1);
	}

	return TRUE;
}

int KSpxAPI::RecvData(LPVOID lpData,long sizeData)
{
	Sleep(10);
	int lBytes=recv(m_hSocket,(LPSTR)lpData,sizeData,0);

	if(lBytes==SOCKET_ERROR)
	{
		if(WSAGetLastError()==WSAEWOULDBLOCK)
		{
			Sleep(100);
			return RecvData(lpData,sizeData);
		}
		return(FALSE);
	}
	else
	{
		if(lBytes<1)	//连接异常或者被断开
			return(FALSE);
	}

	return lBytes;
}

BOOL KSpxAPI::RecvDataEx(LPVOID lpData,long sizeData)
{
	long lBytes(0),l(0);
	for(;l<sizeData;)
	{
		lBytes=recv(m_hSocket,(LPSTR)lpData+l,sizeData-l,0);

		if(lBytes==SOCKET_ERROR)
		{
			if(WSAGetLastError()==WSAEWOULDBLOCK)
			{
				Sleep(100);
				continue;
			}
			return(FALSE);
		}
		else
		{
			if(lBytes<1)	//连接异常或者被断开
				return(FALSE);
			l += lBytes;
		}
		Sleep(1);
	}
	return(TRUE);
}


DWORD WINAPI CommandQueue(LPVOID lParam)
{
	if(lParam == NULL)
		return 0;


	KSpxAPI * lpKSpxAPI = (KSpxAPI*)lParam;

	lpKSpxAPI->RunCmdMain();

	return 0;
}

DWORD WINAPI EventQueue(LPVOID lParam)
{
	if(lParam == NULL)
		return 0;


	KSpxAPI * lpKSpxAPI = (KSpxAPI*)lParam;

	lpKSpxAPI->RunEventMain();

	return 0;
}

BOOL KSpxAPI::startDataThread()
{
	m_hExit = CreateEvent(NULL,TRUE,FALSE,NULL);
	if(!m_hExit)
	{
		return(FALSE);
	}
	DWORD id(0);
	if(!(m_hEventThread=CreateThread(NULL,0,EventQueue,this,0,&id)))
	{
		CloseHandle(m_hExit);
		m_hExit = NULL;
		return(FALSE);
	}
	id = 0;
	if(!(m_hCmdThread=CreateThread(NULL,0,CommandQueue,this,0,&id)))
	{
		SetEvent(m_hExit);
		WaitForSingleObject(m_hEventThread,INFINITE);

		CloseHandle(m_hExit);
		m_hExit = NULL;
		return(FALSE);
	}

	return(TRUE);
}


 