#pragma once
///////////////////////////////////////////////////////////////////////////////////////////////////
class __declspec(novtable) zwl_memory
{
public:
	//申请内存,成功返回地址,失败返回NULL
	//如果已分配控件够用,则复用之;否则,重新申请
	//成功返回时,地址空间已经全部清0
	LPSTR	GetBuf(long lsize);

	//扩展现有内存到新长度,成功返回首地址,失败返回NULL
	//成功申请时,会拷贝原地址数据到新空间
	LPSTR	Extern(long maxSize);

	//拷贝数据到本类中(会自动判断内存长度),成功返回首地址,失败返回NULL
	LPSTR	Copy(LPCVOID lpData,long lsize);

	//追加数据到本类中(会自动判断内存长度),成功返回首地址,失败返回NULL
	LPSTR	Append(LPCVOID lpData,long lsize);

	//绑定另一管理类,复用其数据
	void	Attach(zwl_memory*pMemory);

	//释放内存
	void	Release(void);

	//操作符重载:返回内存首地址
	inline operator LPCSTR(void) const;

	//Copy函数操作符重载
	const zwl_memory& operator=(LPCSTR lpString);

	//Append函数操作符重载
	const zwl_memory& operator+=(LPCSTR lpString);

	zwl_memory(void);
	~zwl_memory(void);

	long sizeData;		//已使用数据长度
	long sizeMalloc;	//实际分配数据长度
	char *bufData;		//数据首地址
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//智能指针:
//1.使用内存池管理
//2.预分配空间,不会每次都申请和释放,引起内存抖动
//3.锁定机制,支持多线程
template<class _Tc>
class __declspec(novtable) zwl_safe_array
{
public:
	_Tc*operator[](long i)
	{
		if(i>=__size__)
		{
			long new_size=(i+499)/500*500,msize=0;
			_Tc*new_data=(_Tc*)zwl_memory_malloc(new_size*sizeof(_Tc),&msize);
			if(!new_data) return(NULL);
			if(__size__) memcpy(new_data,__data__,sizeof(_Tc)*__size__);
			zwl_memory_release((LPSTR&)__data__);
			__size__ = msize/sizeof(_Tc);
			__data__ = new_data;
		}
		return(&__data__[i]);
	}
	zwl_safe_array(void):__data__(NULL),__size__(0)
	{
		InitializeCriticalSection(&__cs__);
	}
	~zwl_safe_array(void)
	{
		zwl_memory_release((LPSTR&)__data__);
		DeleteCriticalSection(&__cs__);
	}
	CRITICAL_SECTION __cs__;
	_Tc	*__data__;
	long __size__;
};
/////////////////////////////////////////////////////////////////////////////
//分配管理器:
//1.独立申请,一次释放
//2.析构函数自动释放,无需人工干预
//3.适用于小范围集中内存管理
class __declspec(novtable) zwl_vector
{
	typedef	struct tagItem
	{
		LPSTR	lpData;
		tagItem	*pNext;
	}ITEM,*PITEM;
public:
//申请
	LPSTR	Malloc(DWORD dwSize);
//一次释放所有已分配内存
	void	ReleaseAll(void);

	zwl_vector(void);
	~zwl_vector(void);

	PITEM	_items;
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//分配内存:lsize为需要的长度,msize不为空时存储实际分配的长度(一般都会多分配一些,避免越界)
//成功返回内存地址,失败返回NULL
//注意:分配成功的内存必须由zwl_memory_release释放
char*zwl_memory_malloc(long lsize,long*msize=NULL);

//释放内存(lpData为内存地址)
//释放完毕,自动将地址指针置NULL
//lpData为NULL时,立即返回,不会异常
void zwl_memory_release(char*&lpData);

//释放空闲内存
//可以设定系统每隔一段时间(服务器每小时,客户端每30分钟),释放空闲内存
void zwl_memory_clean(void);
///////////////////////////////////////////////////////////////////////////////////////////////////
void set_logdir(LPCTSTR lpHome,LPCTSTR strType);
void write_log(LPCTSTR lpFormat,...);
