#include "stdafx.h"
#include "Algorism.h"
#include "global.h"

// This file is Algorism.cpp
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CAlgorism::CAlgorism()
{
	m_bLoaded = false;	
	hmodule = NULL;
	memset(m_szDLLName, 0, sizeof(m_szDLLName));
	pinit=NULL;
	pfree = NULL;
	pGetSendPack=NULL;
	pGetRecvPack=NULL;
}

CAlgorism::~CAlgorism()
{
	Unload();
}

bool CAlgorism::Load(TCHAR *szDLLName/*=NULL*/, TCHAR *szDLLName2/*=NULL*/)
{

	if (!m_bLoaded) {
		//加载SpxACode.dll
		if (szDLLName == NULL) {
			_tcscpy(m_szDLLName, _T("SpxACode.dll"));
		}
		else
			_tcscpy(m_szDLLName, szDLLName);

        hmodule=LoadLibrary(m_szDLLName);
		if (hmodule) {			
			pinit = (KS_init)GetProcAddress(hmodule, "KS_init");
			pfree = (KS_Free)GetProcAddress(hmodule,"KS_Free");
			pGetSendPack = (KS_GetSendPack)::GetProcAddress(hmodule, "KS_GetSendPack");
			pGetRecvPack = (KS_GetRecvPack)::GetProcAddress(hmodule, "KS_GetRecvPack");

			if (!pinit || !pfree || !pGetSendPack || !pGetRecvPack) {
				FreeLibrary(hmodule);
				pinit = NULL;
				pfree = NULL;
				pGetSendPack = NULL;
				pGetRecvPack = NULL;
				WriteLog(0, "接口库 %s 格式不对.", m_szDLLName);
				return false;
			}
		}
		else {
			WriteLog(3, "File %s 不存在，装载失败.", m_szDLLName);
			return false;
		}
		m_bLoaded = true;
	}
	
	return m_bLoaded;
}

void CAlgorism::Unload()
{
	if (!m_bLoaded) return;

	m_cs.Enter();
	
	if (m_bLoaded) {
		
		if (hmodule) {
			
			if (pfree) {
				try {
					if (pfree()) {
					}
				}
				catch(...) {
					WriteLog(0, "except in Encode2");
				}
			}
			
			pinit = NULL;
			pfree = NULL ;
			pGetSendPack = NULL;
			pGetRecvPack = NULL;
			memset(m_szDLLName, 0, sizeof(m_szDLLName));
			
			try {
				FreeLibrary(hmodule);
			}
			catch (...) {
				int i=0;
			}

			hmodule = NULL;			
		}			

		m_bLoaded = false;
	}

	m_cs.Leave();		
}

bool CAlgorism::init()
{
	bool bRet = false;
	if (m_bLoaded) {
		m_cs.Enter();
		if (pinit) {
			try {
				if (pinit()) {
					bRet = true;
				}
			}
			catch(...) {
				bRet = false;
				WriteLog(0, "except in Encode2");
			}
		}
		m_cs.Leave();
	}

	return bRet;
}


bool  CAlgorism::GetSendPack(int type,char *inbuf,int inlen,char *outbuf,int *outlen,char *key,char *msg)
{
	bool bRet = false;
	if (m_bLoaded) {
		m_cs.Enter();
		if (pGetSendPack) {
			try {
				if (pGetSendPack(type,inbuf,inlen,outbuf,outlen,key,msg)) {
					bRet = true;
				}
			}
			catch(...) {
				bRet = false;
				WriteLog(0, "except in Compress");
			}
		}
		m_cs.Leave();
	}
	
	return bRet;
}

//解压缩
bool  CAlgorism::GetRecvPack(char *inbuf,int inlen,char *outbuf,int *outlen,char *key,char *msg)
{
	bool bRet = false;
	if (m_bLoaded) {
		m_cs.Enter();
		if (pGetRecvPack) {
			try {
				if (pGetRecvPack(inbuf,inlen,outbuf,outlen,key,msg)) {
					bRet = true;
				}
			}
			catch(...) {
				bRet = false;
				WriteLog(0, "except in Uncompress");
			}
		}
		m_cs.Leave();
	}
	
	return bRet;
}