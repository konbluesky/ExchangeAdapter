#pragma once
#include <map>
#include "IRoeApi.h"
#include "UMyReqCache.h"
#include "HTTX.h"

#define FIELD_SEPARATOR		"|"
#define ORDER_METHOD		"ZZWT"
#define STRING_EMPTY		""

const int RECORD_RETURN_TYPE = 1;
const int MARKETID = 0;

enum CommandType
{
    CommandType_Error = 0,
    CommandType_Ping = 22,
    CommandType_QueryOrderStatus = 14,
    CommandType_Login = 61,
    CommandType_Logout = 65,
    CommandType_PlaceOrder = 3,
    CommandType_CancelOrder = 4,
    CommandType_QueryPosition = 10
};

class UMyReqCache;

class KSpxAPI :	public IRoeApi
{
public:
	KSpxAPI(void);
	virtual ~KSpxAPI(void);

    /// <summary>
    /// 注册回调
    /// </summary>
    /// <param name="client"></param>
    /// <returns></returns>      
    virtual void RegisterCallback(IRoeCallbackHandle *pIROECallback);
   
    /// <summary>
    /// 卸载回调
    /// </summary>
    /// <param name="tradingAccount"></param>
    /// <param name="iROECallback"></param>
    virtual void UnRegisterCallback(IRoeCallbackHandle *pIROECallback);

    /// <summary>
    /// 连接 
    /// </summary>
    /// <returns></returns>       
    virtual BOOL Connect();

    /// <summary>
    /// 校验资金帐号
    /// </summary>
    /// <param name="account">资金帐号</param>
    /// <returns></returns>       
    virtual int Login(TradingAccount *pAccount, int requestId);

    /// <summary>
    /// 登出资金账号
    /// </summary>
    /// <param name="account"></param>
    /// <param name="requestId"></param>
    virtual int Logout(TradingAccount *pAccount, int requestId);

    /// <summary>
    /// 获取当前会话状态
    /// </summary>
    /// <returns></returns>
    virtual BOOL GetIsConnected();

    /// <summary>
    /// 获取指定资金账号是否已登陆
    /// </summary>
    /// <param name="account"></param>
    /// <returns></returns>
    virtual BOOL GetIsLogined(LPSTR account);

    /// <summary>
    /// 下单
    /// </summary>
    virtual int PlaceOrder(TradingAccount *pAccount, Order *pOrd, int requestId);

    /// <summary>
    /// 撤单
    /// </summary>
    /// <param name="ord"></param>       
    virtual int CancelOrder(TradingAccount *pAccount, CancelOrderInfo *pOrder, int requestId);

    /// <summary>
    /// 查询持仓/库存
    /// </summary>
    /// <param name="account"></param>       
    virtual int QueryPosition(TradingAccount *pAccount, int requestId);

    /// <summary>
    /// 委托查询
    /// </summary>
    /// <param name="account"></param>       
    virtual int QueryOrderStatus(TradingAccount *pAccount, int requestId);

    /// <summary>
    /// Ping
    /// </summary>
    /// <param name="account"></param>       
    virtual int Ping(TradingAccount *pAccount, int requestId);


	void reset(void);
	void RunCmdMain();
	void RunEventMain();

private:
	BOOL Init();
	
	BOOL ReadConfig();

	BOOL startDataThread();

	BOOL InitSocket();
	void CloseSocket();
	BOOL check_connect();
	BOOL ToConnect();
    int Login(int requestId);

	BOOL SendCommand(int commandID,int requestID,CStringA &strCmd);
	int RecvData(LPVOID lpData,long sizeData);
	BOOL RecvDataEx(LPVOID lpData,long sizeData);
	BOOL SendData(LPCVOID lpData,long sizeData);

	BOOL ProcessCmd(int commandID,int reuqestID);

	BOOL OnPing(int requestId,LPSCOMM_HEADER pheader,char* lineData,int size);
	BOOL OnLogin(int requestId,LPSCOMM_HEADER pheader,char* lineData,int size);
	BOOL OnLogout(int requestId,LPSCOMM_HEADER pheader,char* lineData,int size);
	BOOL OnPlaceOrder(int requestId,LPSCOMM_HEADER pheader,char* lineData,int size);
	BOOL OnCancelOrder(int requestId,LPSCOMM_HEADER pheader,char* lineData,int size);
	BOOL OnQueryPosition(int requestId,LPSCOMM_HEADER pheader,char* lineData,int size);
	BOOL GetNextPosition(char* lineData,int size,PositionInformation_Kspx *pPositionInformation);
	BOOL OnQueryOrderStatus(int requestId,LPSCOMM_HEADER pheader,char* lineData,int size);
	BOOL GetNextOrderStatus(char* lineData,int size,OrderStatus *pOrderStatus);

	BOOL FireEvent(int commandID,int requestID,int dataSize,LPVOID *pData,ReturnInfo *pResult);
	int FormAppPack(int commandID,int requestID,const char *cmd,char *sendbuffer, int *sendlen);
	int RecvAppPack(char *recvbuf,int *buflen);

	char m_serverIP[50];
	int m_serverPort;
	char key_in[256];

	SOCKET	m_hSocket;
	HANDLE m_hExit;
	HANDLE m_hCmdThread;
	HANDLE m_hEventThread;
	BOOL m_IsLogined;
	BOOL m_bNeedLogin;
	BOOL m_IsConnected;
	TradingAccount m_tradingAccount;

	UMyReqCache _reqCmdCache;
	UMyReqCache _reqEventCache;

	std::map<DWORD,IRoeCallbackHandle *> m_allCallback;
};
