#include <stdio.h>
#include <time.h>
#include <string.h>
#include "FtdcTraderApi.h"

// 会员代码
TShfeFtdcParticipantIDType g_chParticipantID = "";						//需要根据测试环境修改
// 交易用户代码
TShfeFtdcUserIDType g_chUserID = "222001";								//需要根据测试环境修改

class CSimpleHandler : public CShfeFtdcTraderSpi
{
public:
	// 构造函数，需要一个有效的指向CShfeFtdcTraderApi实例的指针
	CSimpleHandler(CShfeFtdcTraderApi *pTraderApi) : m_pTraderApi(pTraderApi) 
	{
	}

	~CSimpleHandler() {}
	
	virtual void OnPackageStart(int nTopicID, int nSequenceNo)
	{
		printf("OnPackageStart:%d %d\n", nTopicID, nSequenceNo);
	}
	
	virtual void OnPackageEnd(int nTopicID, int nSequenceNo)
	{
		printf("OnPackageEnd:%d %d\n", nTopicID, nSequenceNo);
	}

	virtual void OnRspSubscribeTopic(CShfeFtdcDisseminationField *pDissemination, CShfeFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
	{
		printf("OnRspSubscribeTopic:%d %d\n", pDissemination->SequenceSeries,
			pDissemination->SequenceNo);
	};

	virtual void OnRtnInsInstrument(CShfeFtdcInstrumentField *pInstrument)
	{
		printf("OnRtnInsInstrument:%s\n", pInstrument->InstrumentID);
	};
	// 当客户端与交易后台建立起通信连接，客户端需要进行登录
	virtual void OnFrontConnected()
	{
		CShfeFtdcReqUserLoginField reqUserLogin;
		// 发出登陆请求
		strcpy(reqUserLogin.ParticipantID, g_chParticipantID);
		strcpy(reqUserLogin.UserID, g_chUserID);
		strcpy(reqUserLogin.Password, "");									//需要根据测试环境修改								
		m_pTraderApi->ReqUserLogin(&reqUserLogin, 0);
	}
	
	// 当客户端与交易后台通信连接断开时，该方法被调用
	virtual void OnFrontDisconnected(int nReason)
	{
		// 当发生这个情况后，API会自动重新连接，客户端可不做处理
		printf("OnFrontDisconnected.\n");
	}

	// 当客户端发出登录请求之后，该方法会被调用，通知客户端登录是否成功
	virtual void OnRspUserLogin(CShfeFtdcRspUserLoginField *pRspUserLogin, CShfeFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
	{
		printf("OnRspUserLogin:\n");
		printf("ErrorCode=[%d], ErrorMsg=[%s]\n", pRspInfo->ErrorID, pRspInfo->ErrorMsg);
		printf("RequestID=[%d], Chain=[%d]\n", nRequestID, bIsLast);

		if (pRspInfo->ErrorID != 0) {
			// 端登失败，客户端需进行错误处理
			printf("Failed to login, errorcode=%d errormsg=%s requestid=%d chain=%d", pRspInfo->ErrorID, pRspInfo->ErrorMsg, nRequestID, bIsLast);
//			exit(-1);
		}

		
		CShfeFtdcInputOrderField ord;
		
		// 登录成功,发出报单录入请求
		memset(&ord, 0, sizeof(ord));
		
		// 会员代码
		strcpy(ord.ParticipantID, g_chParticipantID);
		// 客户代码
		strcpy(ord.ClientID, "30000011");									//需要根据测试环境修改
		// 交易用户代码
		strcpy(ord.UserID, g_chUserID);
		// 合约代码
		strcpy(ord.InstrumentID, "cu0709");									//需要根据测试环境修改
		// 报单价格条件
		ord.OrderPriceType = SHFE_FTDC_OPT_LimitPrice;
		// 买卖方向
		ord.Direction = SHFE_FTDC_D_Buy;
		// 组合开平标志
		strcpy(ord.CombOffsetFlag, "0");
		// 组合投机套保标志
		strcpy(ord.CombHedgeFlag, "1");
		// 价格
		ord.LimitPrice = 75000;												//需要根据测试环境修改
		// 数量
		ord.VolumeTotalOriginal = 10;
		// 有效期类型
		ord.TimeCondition = SHFE_FTDC_TC_GFD;
		// GTD日期
		strcpy(ord.GTDDate, "");
		// 成交量类型
		ord.VolumeCondition = SHFE_FTDC_VC_AV;
		// 最小成交量
		ord.MinVolume = 0;
		// 触发条件
		ord.ContingentCondition = SHFE_FTDC_CC_Immediately;
		// 止损价
		ord.StopPrice = 0;
		// 强平原因
		ord.ForceCloseReason = SHFE_FTDC_FCC_NotForceClose;
		// 本地报单编号
		strcpy(ord.OrderLocalID, "0000000002");								//需要根据测试环境修改
		// 自动挂起标志
		ord.IsAutoSuspend = 0;
		m_pTraderApi->ReqOrderInsert(&ord, 1);

	}

	// 报单录入应答
	virtual void OnRspOrderInsert(CShfeFtdcInputOrderField *pInputOrder, CShfeFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
	{
		// 输出报单录入结果
		printf("OnRspOrderInsert:ErrorCode=[%d], ErrorMsg=[%s]\n", pRspInfo->ErrorID, pRspInfo->ErrorMsg);
	};
	
	///报单回报
	virtual void OnRtnOrder(CShfeFtdcOrderField *pOrder)
	{
		static int s_nOrderCount = 0;
		s_nOrderCount++;
		if (s_nOrderCount %100 == 0)
		{
			printf("%d\n", time(NULL));
		}
//		printf("OnRtnOrder:\n");
//		printf("OrderSysID=[%s]\n", pOrder->OrderSysID);
	}

	// 针对用户请求的出错通知
	virtual void OnRspError(CShfeFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) {
		printf("OnRspError:\n");
//		printf("ErrorCode=[%d], ErrorMsg=[%s]\n", pRspInfo->ErrorID, pRspInfo->ErrorMsg);
		printf("RequestID=[%d], Chain=[%d]\n", nRequestID, bIsLast);
	}

private:
	// 指向CShfeFtdcMduserApi实例的指针
	CShfeFtdcTraderApi *m_pTraderApi;
};

int main(int argc, char *argv[])
{
	// 产生一个CShfeFtdcTraderApi实例
	CShfeFtdcTraderApi *pTraderApi = CShfeFtdcTraderApi::CreateFtdcTraderApi();

	int nMajorVersion = 0, nMinorVersion = 0;

	const char *pVersionInfo = pTraderApi->GetVersion(nMajorVersion, nMinorVersion);
	printf("version:%s, %d.%d\n", pVersionInfo, nMajorVersion, nMinorVersion);

	// 产生一个事件处理的实例
	CSimpleHandler sh(pTraderApi);
	// 注册一事件处理的实例
	pTraderApi->RegisterSpi(&sh);
	
	// 订阅私有流
	//        TERT_RESTART:从本交易日开始重传
	//        TERT_RESUME:从上次收到的续传
	//        TERT_QUICK:只传送登录后私有流的内容
	pTraderApi->SubscribePrivateTopic(TERT_RESTART);
	
	// 订阅公共流
	//        TERT_RESTART:从本交易日开始重传
	//        TERT_RESUME:从上次收到的续传
	//        TERT_QUICK:只传送登录后公共流的内容
	pTraderApi->SubscribePublicTopic(TERT_RESTART);

	// 设置交易后台服务的地址
	pTraderApi->RegisterFront("tcp://172.16.30.31:49205");
//	pTraderApi->RegisterNameServer("tcp://172.16.1.15:4601");
	// 使客户端开始与后台服务建立连接
	pTraderApi->Init();
	
	pTraderApi->Join();
	
	// 释放API实例
	pTraderApi->Release();

	return 0;
}

