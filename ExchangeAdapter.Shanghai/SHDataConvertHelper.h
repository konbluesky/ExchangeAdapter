 #ifndef __SH_DATACONVERT_H__
#define __SH_DATACONVERT_H__

#include "SHLib.h"
#include "ChinaOrderDefinition.h"
#include "ChinaQuoteDefinition.h"
#include "SHTraderAdapter.h"

namespace exchangeadapter_shanghai
{
	class USHDataConvertHelper
	{
	public:
		static int TranslateQuoteDataToPublicData(
			const CShfeFtdcDepthMarketDataField *,Quote &,DoM &);
	public:
		static int TranslatePublicDataToOrderData(
			const SHTraderProvider::InitParam *pInitParam,
			const Order & clientOrder,CShfeFtdcInputOrderField* pApiOrder);

		static int TranslatePublicDataToCancelOrderData(
			const SHTraderProvider::InitParam *pInitParam,
			const OrderCancelRequest &clientOrder,
			CShfeFtdcOrderActionField* pApiOrder);

		static int TranslatePublicDataToCancelRelaceOrderData(
			const OrderCancelReplaceRequest &clientOrder,
			CShfeFtdcOrderActionField* pApiOrder);

		static int TraslateExecutionReportToPublicData(
			const CShfeFtdcInputOrderField &apiReport,
			ExecutionReport *pClientReport);

		static int TraslateExecutionReportToPublicData(
			const CShfeFtdcInputOrderField &apiReport,
			BusinessReject *pClientReject,CShfeFtdcRspInfoField *pRspInfo,
			int requestID);

		static int TranslateQuoteDataToPublicData(
			const CShfeFtdcOrderActionField &apiReport,
			ExecutionReport* pClientReport);

		static int TraslateExecutionReportToPublicData(
			const CShfeFtdcOrderActionField &apiReport,
			OrderCancelReject *pClientReject, CShfeFtdcRspInfoField *pRspInfo,
			int requestID);

		static int TraslateExecutionReportToPublicData(
			const CShfeFtdcOrderActionField &apiReport,
			OrderCancelReplaceReject* pClientReject,
			CShfeFtdcRspInfoField *pRspInfo, int requestID);

		static int TraslateExecutionReportToPublicData(
			const CShfeFtdcOrderField &apiReport,
			ExecutionReport *pClientReport);

		static int TraslateExecutionReportToPublicData(
			const CShfeFtdcTradeField &apiReport,
			FillReport *pClientReport);

		static int TranslatePublicDataToBulletin(
			const CShfeFtdcBulletinField &apiBulletin,
			Bulletin *pClientBulletin);

	private:
		USHDataConvertHelper(void);
		virtual ~USHDataConvertHelper(void);
	};
}

#endif