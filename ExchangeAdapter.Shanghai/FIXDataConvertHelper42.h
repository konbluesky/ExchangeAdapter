#ifndef __FIX_DATA_CONVERT_HELPER_42_H__
#define __FIX_DATA_CONVERT_HELPER_42_H__


#include "RMMSCommon.h"
#include "ChinaQuoteDefinition.h"
#include "ChinaOrderDefinition.h"

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4100)
#pragma warning(disable:4512)
#pragma warning(disable:4125)
#else
#error "仅支持VC,以待修改"
#endif

#include "quickfix/fix42/Message.h"
#ifdef _MSC_VER
#pragma warning(pop)
#else
#error "仅支持VC,以待修改"
#endif

class FixDataConvertHelper42
{
	/* 报价转换 */
public:
	static void InternalToFix(RMMS_NS::Exchange exch,const Quote &quote,
		FIX42::Message *pmsg);
	static void InternalToFix(RMMS_NS::Exchange exch,const DoM &dom,
		FIX42::Message *pmsg);

	/* 交易转换 */
public:
	static void InternalToFix(RMMS_NS::Exchange exch,
		const ExecutionReport &exereport,FIX42::Message *pmsg);
	static void InternalToFix(RMMS_NS::Exchange exch,
		const FillReport& fillreport,FIX42::Message *pmsg);
	static void InternalToFix(RMMS_NS::Exchange exch,
		const BusinessReject &bizreject,FIX42::Message *pmsg);
	static void InternalToFix(RMMS_NS::Exchange exch,
		const OrderCancelReject &cancelreject,FIX42::Message *pmsg);
	static void InternalToFix(RMMS_NS::Exchange exch,
		const OrderCancelReplaceReject &cancelreplacereject,
		FIX42::Message *pmsg);
	static int32 FixToInternal(RMMS_NS::Exchange exch,
		const FIX42::Message &msg,Order *porder);
	static int32 FixToInternal(RMMS_NS::Exchange exch,
		const FIX42::Message &msg,OrderCancelRequest *pcancel);

private:
	static void InternalToFix_Insert(RMMS_NS::Exchange exch,
		const ExecutionReport &rsporderinsert,FIX42::Message *pmsg);
	static void InternalToFix_Cancel(RMMS_NS::Exchange exch,
		const ExecutionReport &rspordercancel,FIX42::Message *pmsg);
	static void SecurityDescToSymbol(
		const tstring securitydesc, tstring *p_symbol);
};

#endif