#include "SHEnumsConvertHelper.h"
#include "StringUtils.h"
#include "LocalCompile.h"

namespace exchangeadapter_shanghai
{
	

	void USHEnumsConvert::Convert2ExchOpenClose(OpenCloseFlag rmmOpenClose,TShfeFtdcCombOffsetFlagType& exchOpenClose)
	{
		switch(rmmOpenClose)
		{
		case OpenCloseFlag::Open: 
			{
				LIB_NS::UString::Copy(exchOpenClose, "0");
				break;
			}
		case OpenCloseFlag::Close:
			{
				LIB_NS::UString::Copy(exchOpenClose, "1");
				break;
			}
		default:
			{
				LIB_NS::UString::Copy(exchOpenClose, "0");
				break;
			}
		}
	}

	void USHEnumsConvert::Convert2ExchOrderType(OrderTypeFlag rmmOrderType, TShfeFtdcOrderPriceTypeType &exchOrderType)
	{
		switch(rmmOrderType)
		{
		case OrderTypeFlag::Market: 
			exchOrderType = SHFE_FTDC_OPT_AnyPrice;
			break;
		case OrderTypeFlag::Limit: 
			exchOrderType = SHFE_FTDC_OPT_LimitPrice;
			break;
		case OrderTypeFlag::Stop:
		case OrderTypeFlag::Stop_Limit:
		default:exchOrderType = SHFE_FTDC_OPT_AnyPrice;
			break;
		}
	}

	void USHEnumsConvert::Convert2ExchTimeCondition(OrderTypeFlag rmmOrderType,TimeConditionFlag rmmTimeCondition,TShfeFtdcTimeConditionType &exchTimeCondition)
	{
		switch(rmmTimeCondition)
		{
		case TimeConditionFlag::Day: 
			if(rmmOrderType == OrderTypeFlag::Market)
				exchTimeCondition = SHFE_FTDC_TC_IOC;
			else
				exchTimeCondition = SHFE_FTDC_TC_GFD;
			break;
		case TimeConditionFlag::GTC: 
			exchTimeCondition = SHFE_FTDC_TC_GTC;
			break;
		case TimeConditionFlag::IOC:
			exchTimeCondition = SHFE_FTDC_TC_IOC;
			break;
		default:exchTimeCondition = SHFE_FTDC_TC_GFD;
			break;
		}
	}

	void USHEnumsConvert::Convert2ExchSide(TradeSide rmmTradeSide,TShfeFtdcDirectionType& exchTradSide)
	{
		switch(rmmTradeSide)
		{
		case TradeSide::BUY:
			exchTradSide = SHFE_FTDC_D_Buy;
			break;
		case TradeSide::SELL: 
			exchTradSide = SHFE_FTDC_D_Sell;
			break;
		default: 
			exchTradSide = SHFE_FTDC_D_Buy;
			break;
		}
	}

	void Convert2ExchOrderStatus(OrderStatusFlag rmmOrderStatus, TShfeFtdcOrderStatusType& exchOrderStatus)
	{
		switch(rmmOrderStatus)
		{
		case OrderStatusFlag::New: 
			exchOrderStatus = SHFE_FTDC_OST_NoTradeQueueing;
			break;
		case OrderStatusFlag::Filled: 
			exchOrderStatus = SHFE_FTDC_OST_AllTraded;
			break;
		case OrderStatusFlag::Rejected:
			exchOrderStatus = SHFE_FTDC_OST_NoTradeNotQueueing;
			break;
		case OrderStatusFlag::Canceled:
			exchOrderStatus = SHFE_FTDC_OST_Canceled;
			break;
		//case OrderStatusFlag::Held:
		case OrderStatusFlag::PartiallyFilled:
			exchOrderStatus = SHFE_FTDC_OST_PartTradedNotQueueing;
			break;
		//OrderStatusFlag::Replaced = 7,
		//OrderStatusFlag::Pending = 8,
		//OrderStatusFlag::Expired = 9,
		//OrderStatusFlag::CancelPending = 10,
		//OrderStatusFlag::ReplacePending = 11
		default:
			exchOrderStatus = SHFE_FTDC_OST_NoTradeNotQueueing;
			break;
		}
	}

	void USHEnumsConvert::Convert2RmmOpenClose(const TShfeFtdcCombOffsetFlagType &exchOpenClose, OpenCloseFlag &rmmOpenClose)
	{
		if(strcmp(exchOpenClose, "0") == 0)
			rmmOpenClose = OpenCloseFlag::Open;
		else if(strcmp(exchOpenClose, "1") == 0)
			rmmOpenClose = OpenCloseFlag::Close;
		else
			rmmOpenClose = OpenCloseFlag::Open;
	}

	void USHEnumsConvert::Convert2RmmOrderType(TShfeFtdcOrderPriceTypeType exchOrderType, OrderTypeFlag& rmmOrderType)
	{
		switch(exchOrderType)
		{
		case SHFE_FTDC_OPT_AnyPrice: rmmOrderType = OrderTypeFlag::Market;
			break;
		case SHFE_FTDC_OPT_LimitPrice: rmmOrderType = OrderTypeFlag::Limit;
			break;
		default: rmmOrderType = OrderTypeFlag::Market;
			break;
		}
	}

	void USHEnumsConvert::Convert2RmmSide(TShfeFtdcDirectionType exchSide, TradeSide& rmmTradeSide)
	{
		switch(exchSide)
		{
		case SHFE_FTDC_D_Buy: rmmTradeSide = TradeSide::BUY;
			break;
		case SHFE_FTDC_D_Sell: rmmTradeSide = TradeSide::SELL;
			break;
		default: rmmTradeSide = TradeSide::BUY;
			break;
		}
	}

	void USHEnumsConvert::Convert2RmmTimeCondition(TShfeFtdcTimeConditionType exchTimeCondition, TimeConditionFlag& rmmTimeCondition)
	{
		switch(exchTimeCondition)
		{
		case SHFE_FTDC_TC_GFD: rmmTimeCondition = TimeConditionFlag::Day;
			break;
		case SHFE_FTDC_TC_GTC: rmmTimeCondition = TimeConditionFlag::GTC;
			break;
		default: rmmTimeCondition = TimeConditionFlag::Day;
			break;
		}
	}

	void USHEnumsConvert::Convert2RmmOrderStatus(TShfeFtdcOrderStatusType exchOrderStatus, OrderStatusFlag& rmmOrderStatus)
	{
		switch(exchOrderStatus)
		{
		case SHFE_FTDC_OST_AllTraded: rmmOrderStatus = OrderStatusFlag::Filled;
			break;
		case SHFE_FTDC_OST_PartTradedQueueing: rmmOrderStatus = OrderStatusFlag::PartiallyFilled;
			break;
		case SHFE_FTDC_OST_PartTradedNotQueueing: rmmOrderStatus = OrderStatusFlag::PartiallyFilled;
			break;
		case SHFE_FTDC_OST_NoTradeQueueing: rmmOrderStatus = OrderStatusFlag::New;
			break;
		case SHFE_FTDC_OST_NoTradeNotQueueing: rmmOrderStatus = OrderStatusFlag::Rejected;
			break;
		case SHFE_FTDC_OST_Canceled: rmmOrderStatus = OrderStatusFlag::Canceled;
			break;
		default: rmmOrderStatus = OrderStatusFlag::Rejected;
			break;
		}
	}
	void USHEnumsConvert::Convert2RmmExecType(OrderStatusFlag rmmOrderStauts, ExecTypeFlag& rmmExecType)
	{
		switch(rmmOrderStauts)
		{
		case OrderStatusFlag::New: rmmExecType = ExecTypeFlag::ExecType_New; break;
		case OrderStatusFlag::DoneForDay: rmmExecType = ExecTypeFlag::ExecType_DoneForDay; break;
		case OrderStatusFlag::Canceled: rmmExecType = ExecTypeFlag::ExecType_Canceled; break;
		case OrderStatusFlag::Replaced: rmmExecType = ExecTypeFlag::ExecType_Replaced; break;
		case OrderStatusFlag::PendingCancel: rmmExecType = ExecTypeFlag::ExecType_PendingCancel; break;
		case OrderStatusFlag::Stopped: rmmExecType = ExecTypeFlag::ExecType_Stopped; break;
		case OrderStatusFlag::Rejected: rmmExecType = ExecTypeFlag::ExecType_Rejected; break;
		case OrderStatusFlag::Suspended: rmmExecType = ExecTypeFlag::ExecType_Suspended; break;
		case OrderStatusFlag::PendingNew: rmmExecType = ExecTypeFlag::ExecType_PendingNew; break;
		case OrderStatusFlag::Calculated: rmmExecType = ExecTypeFlag::ExecType_Calculated; break;
		case OrderStatusFlag::Expired: rmmExecType = ExecTypeFlag::ExecType_Expired; break;
		case OrderStatusFlag::PendingReplace: rmmExecType = ExecTypeFlag::ExecType_PendingReplace; break;
		//case OrderStatusFlag::AcceptedForBidding: rmmExecType = ExecTypeFlag::ExecType_Restated; break;
		}
	}
}