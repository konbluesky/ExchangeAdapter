#ifndef __SHENUMCONVERT_H__
#define __SHENUMCONVERT_H__

#include "SHLib.h"
#include "ChinaOrderDefinition.h"

namespace exchangeadapter_shanghai
{
	class USHEnumsConvert
	{
	public:
		static void Convert2ExchOpenClose(OpenCloseFlag rmmOpenClose,TShfeFtdcCombOffsetFlagType& exchOpenClose);
		static void Convert2ExchTimeCondition(OrderTypeFlag rmmOrderType,TimeConditionFlag rmmTimeCondition,TShfeFtdcTimeConditionType& exchTimeCondition);
		static void Convert2ExchSide(TradeSide rmmTradeSide, TShfeFtdcDirectionType& exchTradSide);
		static void Convert2ExchOrderType(OrderTypeFlag rmmOrderType, TShfeFtdcOrderPriceTypeType& exchOrderType);
		static void Convert2ExchOrderStatus(OrderStatusFlag rmmOrderStatus, TShfeFtdcOrderStatusType& exchOrderStatus);
		static void Convert2RmmOpenClose(const TShfeFtdcCombOffsetFlagType &exchOpenClose,OpenCloseFlag& rmmOpenClose);
		static void Convert2RmmTimeCondition(TShfeFtdcTimeConditionType exchTimeCondition, TimeConditionFlag& rmmTimeCondition);
		static void Convert2RmmSide(TShfeFtdcDirectionType exchSide, TradeSide& rmmTradeSide);
		static void Convert2RmmOrderType(TShfeFtdcOrderPriceTypeType exchOrderType, OrderTypeFlag& rmmOrderType); 
		static void Convert2RmmOrderStatus(TShfeFtdcOrderStatusType exchOrderStatus, OrderStatusFlag& rmmOrderStatus);
		static void Convert2RmmExecType(OrderStatusFlag rmmOrderStauts, ExecTypeFlag& rmmExecType);

	private:
		USHEnumsConvert();
	};
}
#endif