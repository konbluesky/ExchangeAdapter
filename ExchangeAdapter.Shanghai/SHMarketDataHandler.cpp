#include "SHMarketDataHandler.h"
#include "ChinaQuoteDefinition.h"
#include "SHDataConvertHelper.h"
#include "StringUtils.h"
#include "DebugUtils.h"
#include "SHMarketDataProvider.h"
#include "FixDataConvertHelper.h"
#include "LocalCompile.h"

CSHMarketDataHandler::CSHMarketDataHandler(IFixMarketProvider* marketprovider,
	CShfeFtdcMduserApi *pUserApi,const InitParam& param)
	:m_MarketProvider(marketprovider),m_pUserApi(pUserApi),m_param(param)
{
}

CSHMarketDataHandler::~CSHMarketDataHandler(void)
{
}

void CSHMarketDataHandler::OnFrontConnected()
{
	m_MarketProvider->PriOnNetConnected();
}
	
void CSHMarketDataHandler::OnFrontDisconnected(int nReason)
{
	DBG_TRACE(("<OnFrontDisconnected> be called. Reason=[%d]\n", nReason));
	m_MarketProvider->PriOnNetDisconnected();
}
		
void CSHMarketDataHandler::OnHeartBeatWarning(int nTimeLapse)
{
}
	
void CSHMarketDataHandler::OnRspError(CShfeFtdcRspInfoField *pRspInfo,
	int nRequestID, bool bIsLast)
{
}

void CSHMarketDataHandler::OnRspUserLogin(
	CShfeFtdcRspUserLoginField *pRspUserLogin,CShfeFtdcRspInfoField *pRspInfo,
	int nRequestID, bool bIsLast)
{
	m_MarketProvider->PriOnLogin();
}

void CSHMarketDataHandler::OnRspUserLogout(
	CShfeFtdcRspUserLogoutField *pRspUserLogout,CShfeFtdcRspInfoField *pRspInfo,
	int nRequestID, bool bIsLast)
{
	m_MarketProvider->PriOnLogout();
}

void CSHMarketDataHandler::OnRtnDepthMarketData(
	CShfeFtdcDepthMarketDataField *pDepthMarketData)
{
	Quote quote;
	DoM dom;
	FIX::Message msg;
	exchangeadapter_shanghai::USHDataConvertHelper::TranslateQuoteDataToPublicData(pDepthMarketData,quote,dom);
		
	//m_MarketProvider->OnQuoteRecieve(quote);
	//m_MarketProvider->OnDoMRecieve(dom);
	FixDataConvertHelper::InternalToFix(RMMS_NS::RMMS_CN_SH,dom,&msg);
	exchangeadapter_shanghai::CSHMarketDataProvider* provider = dynamic_cast<
		exchangeadapter_shanghai::CSHMarketDataProvider*>(
		m_MarketProvider);
	provider->OnRspMsg(msg);
}