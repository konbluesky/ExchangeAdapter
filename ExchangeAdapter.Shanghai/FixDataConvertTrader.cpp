#include "ExchangeAdapterConfig.h"
#include "FixDataConvertHelper.h"
#include "FIXDataConvertHelper42.h"
#include "LocalCompile.h"

void FixDataConvertHelper::InternalToFix(RMMS_NS::Exchange exch,
	const ExecutionReport &exereport,FIX::Message *pmsg)
{
	switch(FixHelper::GetCurrentVer())
	{
	case RMMS_NS::RMMS_FIX42:
		FixDataConvertHelper42::InternalToFix(exch,exereport,
			(FIX42::Message*)pmsg);
		break;
	}
}

void FixDataConvertHelper::InternalToFix(RMMS_NS::Exchange exch,
	const FillReport& fillreport,FIX::Message *pmsg)
{
	switch(FixHelper::GetCurrentVer())
	{
	case RMMS_NS::RMMS_FIX42:
		FixDataConvertHelper42::InternalToFix(exch,fillreport,
			(FIX42::Message*)pmsg);
		break;
	}
}

void FixDataConvertHelper::InternalToFix(RMMS_NS::Exchange exch,
	const OrderCancelReject &cancelreject,FIX::Message *pmsg)
{
	switch(FixHelper::GetCurrentVer())
	{
	case RMMS_NS::RMMS_FIX42:
		FixDataConvertHelper42::InternalToFix(exch,cancelreject,
			(FIX42::Message*)pmsg);
		break;
	}
}

void FixDataConvertHelper::InternalToFix(RMMS_NS::Exchange exch,
	const OrderCancelReplaceReject &cancelreplacereject,FIX::Message *pmsg)
{
	switch(FixHelper::GetCurrentVer())
	{
	case RMMS_NS::RMMS_FIX42:
		FixDataConvertHelper42::InternalToFix(exch,cancelreplacereject,
			(FIX42::Message*)pmsg);
		break;
	}
}

void FixDataConvertHelper::InternalToFix(RMMS_NS::Exchange exch,
	const BusinessReject &bizreject,FIX::Message *pmsg)
{
	switch(FixHelper::GetCurrentVer())
	{
	case RMMS_NS::RMMS_FIX42:
		FixDataConvertHelper42::InternalToFix(exch,bizreject,
			(FIX42::Message*)pmsg);
		break;
	}
}

int32 FixDataConvertHelper::FixToInternal(RMMS_NS::Exchange exch,
	const FIX::Message &msg,Order *porder)
{
	int32 ret = 0;
	if(!FixHelper::EqualCurVer(msg))
		return -1;

	switch(FixHelper::GetCurrentVer())
	{
	case RMMS_NS::RMMS_FIX42:
		ret = FixDataConvertHelper42::FixToInternal(exch,(FIX42::Message)msg,
			porder);
		break;
	}

	return ret;
}

int32 FixDataConvertHelper::FixToInternal(RMMS_NS::Exchange exch,
	const FIX::Message &msg,OrderCancelRequest *pcancel)
{
	int32 ret = 0;
	if(!FixHelper::EqualCurVer(msg))
		return -1;

	switch(FixHelper::GetCurrentVer())
	{
	case RMMS_NS::RMMS_FIX42:
		ret = FixDataConvertHelper42::FixToInternal(exch,(FIX42::Message)msg,
			pcancel);
		break;
	}

	return ret;
}