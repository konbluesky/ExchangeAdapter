#include "SHTraderAdapterHandler.h"
#include "ChinaOrderDefinition.h"
#include "SHDataConvertHelper.h"
#include "debugutils.h"
#include "FixDataConvertHelper.h"
#include "LocalCompile.h"
using namespace std;

namespace exchangeadapter_shanghai
{

	static char* OrderSatusString[] =
	{
		"NULSTRING"
		"Working"
		"Completed"
		"Rejected"
		"Cancelled"
		"Held"
		"PartiallyFilled"
		"Replaced"
		"Pending"
		"Expired"
		"CancelPending"
		"ReplacePending"
	};

	SHTraderAdapterHandler::SHTraderAdapterHandler(
		CShfeFtdcTraderApi *pUserApi,IFixTraderProvider *traderProvider) 
		: m_pUserApi(pUserApi), m_TraderProvider(traderProvider)
	{}

	SHTraderAdapterHandler::~SHTraderAdapterHandler(){}
	
	void SHTraderAdapterHandler::OnFrontConnected()
	{
		m_TraderProvider->PriOnNetConnected();
	}

	void SHTraderAdapterHandler::OnFrontDisconnected(int nReason)
	{
		DBG_TRACE(("<OnFrontDisconnected> be called. Reason=[%d]\n", nReason));
		m_TraderProvider->PriOnNetDisconnected();
	}

	void SHTraderAdapterHandler::OnRspUserLogin(
		CShfeFtdcRspUserLoginField *pRspUserLogin,
		CShfeFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
	{
		if (0 != pRspInfo->ErrorID)
			DBG_TRACE(("Failed to login, errorcode=%d errormsg=%s "
				"requestid=%d chain=%d", pRspInfo->ErrorID, pRspInfo->ErrorMsg,
				nRequestID, bIsLast));
		else {
			m_TraderProvider->PriOnLogin();
			DBG_TRACE(("�Ϻ���½�ɹ�"));
		}
	}

	void SHTraderAdapterHandler::OnRspUserLogout(
		CShfeFtdcRspUserLogoutField *pRspUserLogout,
		CShfeFtdcRspInfoField *pRspInfo,int nRequestID,bool bIsLast)
	{
		m_TraderProvider->PriOnLogout();		
	}

	void SHTraderAdapterHandler::OnRspOrderInsert(
		CShfeFtdcInputOrderField *pInputOrder,CShfeFtdcRspInfoField *pRspInfo,
		int nRequestID, bool bIsLast)
	{
		if(0 == pRspInfo->ErrorID){
			ExecutionReport pClientReport;
			USHDataConvertHelper::TraslateExecutionReportToPublicData(
				*pInputOrder, &pClientReport);
			//m_TraderProvider->OnOrderAddResponse(pClientReport);
			FIX::Message msg;
			FixDataConvertHelper::InternalToFix(RMMS_NS::RMMS_CN_SH,
				pClientReport,&msg);
			m_TraderProvider->OnResponseMsg(msg);
		}else{
			BusinessReject pClientReport;
			USHDataConvertHelper::TraslateExecutionReportToPublicData(
				*pInputOrder, &pClientReport, pRspInfo, nRequestID);
			//m_TraderProvider->OnBussinessReject(pClientReport);
			FIX::Message msg;
			FixDataConvertHelper::InternalToFix(RMMS_NS::RMMS_CN_SH,
				pClientReport,&msg);
			m_TraderProvider->OnResponseMsg(msg);
		}
	}

	void SHTraderAdapterHandler::OnRtnOrder(CShfeFtdcOrderField *pOrder)
	{
		ExecutionReport pClientReport;
		std::memset(&pClientReport,0,sizeof(ExecutionReport));
		switch(pOrder->OrderStatus){
		case SHFE_FTDC_OST_AllTraded:
		case SHFE_FTDC_OST_PartTradedQueueing:
		case SHFE_FTDC_OST_PartTradedNotQueueing:
		case SHFE_FTDC_OST_NoTradeNotQueueing:
			break;
		case SHFE_FTDC_OST_Canceled:
			{
				USHDataConvertHelper::TraslateExecutionReportToPublicData(
					*pOrder, &pClientReport);
				//m_TraderProvider->OnOrderAddResponse(pClientReport);
				FIX::Message msg;
				FixDataConvertHelper::InternalToFix(RMMS_NS::RMMS_CN_SH,
					pClientReport,&msg);
				m_TraderProvider->OnResponseMsg(msg);
			}
			
			break;
		case SHFE_FTDC_OST_NoTradeQueueing:
			break;
		}
	}
	
	void SHTraderAdapterHandler::OnRtnTrade(CShfeFtdcTradeField *pTrade)
	{
		FillReport pClientReport;
		USHDataConvertHelper::TraslateExecutionReportToPublicData(
			*pTrade, &pClientReport);
		//m_TraderProvider->OnOrderFillResponse(pClientReport);
		FIX::Message msg;
		FixDataConvertHelper::InternalToFix(RMMS_NS::RMMS_CN_SH,
			pClientReport,&msg);
		m_TraderProvider->OnResponseMsg(msg);
	}

	void SHTraderAdapterHandler::OnRspOrderAction(
		CShfeFtdcOrderActionField *pOrderAction,
		CShfeFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
	{
		if(pRspInfo->ErrorID == 0)
			return;
		else if(SHFE_FTDC_AF_Delete == pOrderAction->ActionFlag){
			OrderCancelReject pClientReject;
			USHDataConvertHelper::TraslateExecutionReportToPublicData(
				*pOrderAction, &pClientReject, pRspInfo, nRequestID);
			//m_TraderProvider->OnOrderCancelReject(pClientReject);
			FIX::Message msg;
			FixDataConvertHelper::InternalToFix(RMMS_NS::RMMS_CN_SH,
				pClientReject,&msg);
			m_TraderProvider->OnResponseMsg(msg);
		}else if(SHFE_FTDC_AF_Modify == pOrderAction->ActionFlag){
			OrderCancelReplaceReject pClientReject;
			USHDataConvertHelper::TraslateExecutionReportToPublicData(
				*pOrderAction, &pClientReject, pRspInfo, nRequestID);
			//m_TraderProvider->OnOrderCancelReplaceReject(pClientReject);
			FIX::Message msg;
			FixDataConvertHelper::InternalToFix(RMMS_NS::RMMS_CN_SH,
				pClientReject,&msg);
			m_TraderProvider->OnResponseMsg(msg);
		}
	}

	void SHTraderAdapterHandler::OnRspError(CShfeFtdcRspInfoField *pRspInfo,
		int nRequestID, bool bIsLast) 
	{
		DBG_ASSERT(false);
	}

	void SHTraderAdapterHandler::OnRtnBulletin(
		CShfeFtdcBulletinField *pBulletin)
	{
		DBG_TRACE(("<OnRtnBulletin>\n"));
		Bulletin clientBulletin;
		USHDataConvertHelper::TranslatePublicDataToBulletin(*pBulletin,
			&clientBulletin);
	}
}