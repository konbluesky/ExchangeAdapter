#ifndef __SH_LIB_H__
#define __SH_LIB_H__

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4100)

#else
#error "不应该出现,以待其他"
#endif

#include "FtdcTraderApi.h"
#include "FtdcUserApiDataType.h"
#include "FtdcUserApiStruct.h"
#include "FtdcMduserApi.h"

#ifdef _MSC_VER
#pragma warning(pop)
#else
#error "仅支持VC,以待修改"
#endif

#endif