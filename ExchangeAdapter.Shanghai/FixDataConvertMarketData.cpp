#include "ExchangeAdapterConfig.h"
#include "FixDataConvertHelper.h"
#include "FIXDataConvertHelper42.h"
#include "LocalCompile.h"

tstring FixHelper::curver = FIX::BeginString_FIX42;
RMMS_NS::RMMSFixVer FixHelper::rmmscurver = RMMS_NS::RMMS_FIX42;

bool FixHelper::SetCurrentVer(RMMS_NS::RMMSFixVer ver)
{
	if(RMMS_NS::RMMS_FIX42 == ver)
		curver = FIX::BeginString_FIX42;
	else
		return false;

	rmmscurver = ver;
	return true;
}

RMMS_NS::RMMSFixVer FixHelper::GetCurrentVer(void)
{
	return rmmscurver;
}

/* Market Data */
void FixDataConvertHelper::InternalToFix(RMMS_NS::Exchange exch,
	const Quote &quote,FIX::Message *pmsg)
{
	switch(FixHelper::GetCurrentVer())
	{
	case RMMS_NS::RMMS_FIX42:
		FixDataConvertHelper42::InternalToFix(exch,quote,(FIX42::Message*)pmsg);
		break;
	}
}

void FixDataConvertHelper::InternalToFix(RMMS_NS::Exchange exch,const DoM &dom,
	FIX::Message *msg)
{
	switch(FixHelper::GetCurrentVer())
	{
	case RMMS_NS::RMMS_FIX42:
		FixDataConvertHelper42::InternalToFix(exch,dom,(FIX42::Message*)msg);
		break;
	}
}