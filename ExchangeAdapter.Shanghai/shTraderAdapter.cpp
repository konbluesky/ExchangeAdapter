#include "ExchangeAdapterConfig.h"
#include "SHTraderAdapter.h"
#include "SHDataConvertHelper.h"
#include "debugutils.h"
#define TIXML_USE_STL
#include "tinyxml.h"
#include "FixDataConvertHelper.h"
using namespace std;

namespace exchangeadapter_shanghai
{
	/* 临时测试 */
	double mystatic[100] = {0.0};
	int staticcount = 0;
	SHTraderProvider::SHTraderProvider(void)
	{
	}

	SHTraderProvider::~SHTraderProvider()
	{
	}

	std::string SHTraderProvider::GetName()
	{
		return "CFFEXTraderAdapter";
	}

	void SHTraderProvider::RequestFix(FIX::Message &msg)
	{
		FIX::MsgType type;
		const FIX::Header &header = msg.getHeader();
		header.getField(type);
		tstring strtype = type.getValue();

		if(FIX::MsgType_NewOrderSingle == strtype){
			Order order;
			if(0 > FixDataConvertHelper::FixToInternal(RMMS_NS::RMMS_CN_SH,msg,&order))
				return;
			ReqeustOrderAdd(order);
		} else if(FIX::MsgType_OrderCancelRequest == strtype) {
			OrderCancelRequest cancel;
			if(0 > FixDataConvertHelper::FixToInternal(RMMS_NS::RMMS_CN_SH,msg,&cancel))
				return;
			RequestOrderCancel(cancel);
		}
	}

	void SHTraderProvider::ReqeustOrderAdd(const Order& order)
	{
		CShfeFtdcInputOrderField pApiOrder;
		USHDataConvertHelper::TranslatePublicDataToOrderData(&pInitParam,order,
			&pApiOrder);

		DBG_ASSERT(NULL != trader);
		/* 测试项 */
		mystatic[staticcount] = DBG_QueryTimeTickImp();
		staticcount++;
		/* 测试项 */

		int result = trader->ReqOrderInsert(&pApiOrder, order.zjpri.IDRequest);
		/* 测试项 */
		if(100 <= staticcount) {
			for(int i = 0;i < 100;i++) {
				std::cout<<"第"<<i<<"次所用时间"<<mystatic[i]<<std::endl;
			}
		}
		/* 测试项 */
		if(result == 0)
			return;
		CShfeFtdcRspInfoField pRspInfo;
		pRspInfo.ErrorID = result;
		std::strcpy(pRspInfo.ErrorMsg,"");
		
		BusinessReject pClientReport;
		USHDataConvertHelper::TraslateExecutionReportToPublicData(pApiOrder,
			&pClientReport, &pRspInfo, order.zjpri.IDRequest);
		//OnBussinessReject(pClientReport);
		FIX::Message msg;
		FixDataConvertHelper::InternalToFix(RMMS_NS::RMMS_CN_SH,pClientReport,
			&msg);
		OnResponseMsg(msg);
	}

	void SHTraderProvider::RequestOrderCancel(const OrderCancelRequest& order)
	{
		CShfeFtdcOrderActionField pApiOrder;
		USHDataConvertHelper::TranslatePublicDataToCancelOrderData(&pInitParam,
			order, &pApiOrder);

		DBG_ASSERT(NULL != trader);
		int result = trader->ReqOrderAction(&pApiOrder, order.zjpri.IDRequest);
		if(0 == result)
			return;

		CShfeFtdcRspInfoField pRspInfo;
		pRspInfo.ErrorID = result;
		strcpy(pRspInfo.ErrorMsg, "");

		OrderCancelReject pClientReport;
		USHDataConvertHelper::TraslateExecutionReportToPublicData(pApiOrder,
			&pClientReport, &pRspInfo, order.zjpri.IDRequest);
		//OnOrderCancelReject(pClientReport);
		FIX::Message msg;
		FixDataConvertHelper::InternalToFix(RMMS_NS::RMMS_CN_SH,pClientReport,
			&msg);
		OnResponseMsg(msg);
	}

	void SHTraderProvider::RequestOrderModify(const OrderCancelReplaceRequest& order)
	{
		CShfeFtdcOrderActionField pApiOrder;

		USHDataConvertHelper::TranslatePublicDataToCancelRelaceOrderData(order, &pApiOrder);

		DBG_ASSERT(NULL != trader);
		int result = trader->ReqOrderAction(&pApiOrder, order.IDRequest);
		if(NULL == trader)
			return;
		CShfeFtdcRspInfoField pRspInfo;
		pRspInfo.ErrorID = result;
		strcpy(pRspInfo.ErrorMsg, "");

		OrderCancelReplaceReject pClientReport;
		USHDataConvertHelper::TraslateExecutionReportToPublicData(pApiOrder,
			&pClientReport, &pRspInfo, order.IDRequest);
		//OnOrderCancelReplaceReject(pClientReport);
		FIX::Message msg;
		FixDataConvertHelper::InternalToFix(RMMS_NS::RMMS_CN_SH,pClientReport,
			&msg);
		OnResponseMsg(msg);
	}

	bool SHTraderProvider::Initialize( void* pParam)
	{
		pInitParam = *((InitParam*)pParam);
		trader = CShfeFtdcTraderApi::CreateFtdcTraderApi(
			pInitParam.sFlowPath.c_str());

		SHTraderAdapterHandler *handler = new SHTraderAdapterHandler(trader,
			this);
		sh = boost::shared_ptr<SHTraderAdapterHandler>(handler);

		std::strcpy(handler->g_chParticipantID,
			pInitParam.sParticipantID.c_str());
		std::strcpy(handler->g_chUserID,
			pInitParam.sUserID.c_str());
		std::strcpy(handler->g_chPassword,
			pInitParam.sPassword.c_str());

		trader->RegisterSpi(sh.get());
		switch(pInitParam.iResumeType){
		case 0: 
			trader->SubscribePrivateTopic(TERT_RESTART);
			trader->SubscribePublicTopic(TERT_RESTART);
			break;
		case 1: 
			trader->SubscribePrivateTopic(TERT_RESUME);
			trader->SubscribePublicTopic(TERT_RESUME);
			break;
		case 2:
			trader->SubscribePrivateTopic(TERT_QUICK);
			trader->SubscribePublicTopic(TERT_QUICK);
			break;
		}

		for(int i = 0;i < (int)pInitParam.sAddressIps.size();i++){
			std::string straddr = pInitParam.sAddressIps[i];
			int iport = pInitParam.iPorts[i];
			char addressinfo[64];
			std::sprintf(addressinfo,"tcp://%s:%d",straddr.c_str(),iport);
			trader->RegisterFront(addressinfo);
		}

		trader->Init();
		
		return true;
	}

	bool SHTraderProvider::InitializeByXML( const char* configfile )
	{
		std::pair<bool,SHTraderProvider::InitParam> param = 
			InitParam::LoadConfig(configfile);
		if (param.first)
			return Initialize(&param.second);
		else
			return false;
	}
		
	std::pair<bool,SHTraderProvider::InitParam> 
		SHTraderProvider::InitParam::LoadConfig(const std::string& fname)
	{
		SHTraderProvider::InitParam param;
		TiXmlDocument doc(fname);
		bool loadOkay = doc.LoadFile();
		if (!loadOkay)
			return std::make_pair(false,param);
		TiXmlNode* pNode = doc.FirstChild("configure");
		TiXmlNode* ele = pNode->FirstChild("sflowpath");
		param.sFlowPath = ele->FirstChild()->Value();
		
		ele = ele->NextSibling();
		TiXmlNode* addressNode;
		while(ele != NULL){
			if(ele->ValueStr() == "saddress"){
				addressNode = ele->FirstChild("saddressip");
				param.sAddressIps.push_back(addressNode->FirstChild()->Value());

				addressNode = ele->FirstChild("iport");
				param.iPorts.push_back(atoi(
					addressNode->FirstChild()->Value()));
			}
			ele = ele->NextSibling();
		}
		
		ele = pNode->FirstChild("sparticipantid");
		param.sParticipantID = ele->FirstChild()->Value();

		ele = pNode->FirstChild("suserid");
		param.sUserID = ele->FirstChild()->Value();

		ele = pNode->FirstChild("spassword");
		param.sPassword = ele->FirstChild()->Value();

		ele = pNode->FirstChild("iresumeType");
		param.iResumeType = atoi(ele->FirstChild()->Value());

		return std::make_pair(true,param);
	}

	void SHTraderProvider::UnInitialize()
	{		
		trader->Release();
		trader = NULL;
	}

	bool SHTraderProvider::Login(void* ppara)
	{
		uint32 seqnum = (uint32)(ppara);
		CShfeFtdcReqUserLoginField reqUserLogin;
		std::strcpy(reqUserLogin.ParticipantID,sh->g_chParticipantID);
		std::strcpy(reqUserLogin.UserID, sh->g_chUserID);
		std::strcpy(reqUserLogin.Password,sh->g_chPassword);
		std::strcpy(reqUserLogin.UserProductInfo, "RMMSoft(v1.0.0)");

		if(0 > trader->ReqUserLogin(&reqUserLogin,seqnum))
			return false;
		return true;
	}

	bool SHTraderProvider::Logout(void* ppara)
	{
		uint32 seqnum = (uint32)(ppara);
		CShfeFtdcReqUserLogoutField reqUserLogout;
		std::strcpy(reqUserLogout.ParticipantID,sh->g_chParticipantID);
		std::strcpy(reqUserLogout.UserID, sh->g_chUserID);

		if(0 > trader->ReqUserLogout(&reqUserLogout,seqnum))
			return false;
		return true;
	}
}