#ifndef SH_TRADER_ADAPTER_H
#define SH_TRADER_ADAPTER_H

#include "SHLib.h"
#include "IFixTrader.h"
#include "ChinaOrderDefinition.h"
#include "SHTraderAdapterHandler.h"
#include "autoptr.h"
#include "ExchangeAdapterBase.h"
#include <vector>

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4251)
#pragma warning(disable:4275)
#else
#error "不应该出现,以待其他"
#endif

struct OrderCancelReplaceRequest;
class CShfeFtdcTraderApi;
class SHTraderAdapterHandler;
namespace exchangeadapter_shanghai
{
	class EXAPI  SHTraderProvider : public IFixTraderProvider
	{
	public:
		struct InitParam
		{
			std::string sFlowPath;
			std::vector<tstring> sAddressIps;
			std::vector<int> iPorts;
			tstring sParticipantID;
			tstring sUserID;
			tstring sPassword;
			int32 iResumeType;

			EXAPI static std::pair<bool,InitParam> LoadConfig(const tstring& fname);	
		};

		SHTraderProvider(void);
		virtual ~SHTraderProvider();
		virtual std::string GetName();
		virtual bool InitializeByXML( const tchar* configfile );
		virtual bool Initialize(void* );
		virtual void UnInitialize();
		virtual bool Login(void*);
		virtual bool Logout(void*);
		virtual void RequestFix(FIX::Message &msg);

		void ReqeustOrderAdd(const Order& order);
		void RequestOrderCancel(const OrderCancelRequest& order);
		void RequestOrderModify(const OrderCancelReplaceRequest& order);

	private:
		CShfeFtdcTraderApi *trader;
		boost::shared_ptr<SHTraderAdapterHandler> sh;
		InitParam pInitParam;
	};
}

#ifdef _MSC_VER
#pragma warning(pop)
#else
#error "仅支持VC,以待修改"
#endif

#endif