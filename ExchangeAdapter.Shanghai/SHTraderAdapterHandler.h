#ifndef __SHTraderAdapterHandler_H
#define __SHTraderAdapterHandler_H

#include "SHLib.h"
#include "Mutex.h"

class IFixTraderProvider;

namespace exchangeadapter_shanghai
{
	class SHTraderAdapterHandler : public CShfeFtdcTraderSpi
	{
	public:
		SHTraderAdapterHandler(CShfeFtdcTraderApi *pUserApi,
			IFixTraderProvider *traderProvider);
		~SHTraderAdapterHandler();

		virtual void OnFrontConnected();
		virtual void OnFrontDisconnected(int nReason);
		virtual void OnRspUserLogin(CShfeFtdcRspUserLoginField *pRspUserLogin,
			CShfeFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast);
		virtual void OnRspUserLogout(
			CShfeFtdcRspUserLogoutField *pRspUserLogout,
			CShfeFtdcRspInfoField *pRspInfo,
			int nRequestID,bool bIsLast);
		virtual void OnRspOrderInsert(CShfeFtdcInputOrderField *pInputOrder,
			CShfeFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast);
		virtual void OnRspOrderAction(CShfeFtdcOrderActionField *pOrderAction,
			CShfeFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast);
		virtual void OnRtnOrder(CShfeFtdcOrderField *pOrder);
		virtual void OnRtnTrade(CShfeFtdcTradeField *pTrade);
		virtual void OnRspError(CShfeFtdcRspInfoField *pRspInfo,int nRequestID,
			bool bIsLast);
		virtual void OnRtnBulletin(CShfeFtdcBulletinField *pBulletin);

	public:
		TShfeFtdcParticipantIDType g_chParticipantID;
		TShfeFtdcUserIDType g_chUserID;
		TShfeFtdcPasswordType g_chPassword;
	private:
		CShfeFtdcTraderApi *m_pUserApi;
		IFixTraderProvider *m_TraderProvider;
	};
}

#endif