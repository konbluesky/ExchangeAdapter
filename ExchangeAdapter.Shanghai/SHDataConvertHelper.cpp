#include "SHDataConvertHelper.h"
#include "SHEnumsConvertHelper.h"
#include "SHPriceHelper.h"
#include <map>
#include <strstream>
#include "StringUtils.h"
#include "debugutils.h"
#include "SHTraderAdapter.h"
#include "LocalCompile.h"

using namespace std;

namespace exchangeadapter_shanghai
{
	struct PreVolume
	{
		int totalVolume;
		int lastVolume;
	};

	int USHDataConvertHelper::TranslatePublicDataToOrderData(
		const SHTraderProvider::InitParam *pInitParam,const Order &clientOrder,
		CShfeFtdcInputOrderField *pApiOrder)
	{
		DBG_ASSERT(NULL != pApiOrder);

		LIB_NS::UString::Copy(pApiOrder->ParticipantID,
			pInitParam->sParticipantID);
		LIB_NS::UString::Copy(pApiOrder->UserID,pInitParam->sUserID);
#if defined(RMMS_TEST_CLIENT)
		LIB_NS::UString::Copy(pApiOrder->ClientID,RMMS_SH_CLIENT);
#else
		LIB_NS::UString::Copy(pApiOrder->ClientID,clientOrder.IDClient);
#endif
		LIB_NS::UString::Copy(pApiOrder->OrderLocalID,
			clientOrder.zjIDLocalOrder);
		LIB_NS::UString::Copy(pApiOrder->OrderSysID,"");
		LIB_NS::UString::Copy(pApiOrder->InstrumentID,
			clientOrder.SecurityDesc);
		pApiOrder->VolumeTotalOriginal = clientOrder.Quantity;
		pApiOrder->LimitPrice = clientOrder.LimitPrice;
		pApiOrder->StopPrice = clientOrder.StopPrice;
		USHEnumsConvert::Convert2ExchOpenClose(clientOrder.OpenClose,
			pApiOrder->CombOffsetFlag);
		USHEnumsConvert::Convert2ExchOrderType(clientOrder.OrderType,
			pApiOrder->OrderPriceType);
		USHEnumsConvert::Convert2ExchSide(clientOrder.Side,
			pApiOrder->Direction);
		USHEnumsConvert::Convert2ExchTimeCondition(clientOrder.OrderType,
			clientOrder.zjpri.TimeCondition, pApiOrder->TimeCondition);

		pApiOrder->VolumeCondition = SHFE_FTDC_VC_AV;
		LIB_NS::UString::Copy(pApiOrder->CombHedgeFlag, "1");
		pApiOrder->ContingentCondition = SHFE_FTDC_CC_Immediately;
		pApiOrder->ForceCloseReason = SHFE_FTDC_FCC_NotForceClose;
		LIB_NS::UString::Copy(pApiOrder->GTDDate, "");
		pApiOrder->IsAutoSuspend = 0;

		return 0;
	}

	int USHDataConvertHelper::TranslateQuoteDataToPublicData(
		const CShfeFtdcDepthMarketDataField *marketData,Quote &quote,DoM &dom)
	{
		DBG_ASSERT(NULL != marketData);
		static std::map<std::string,PreVolume> preVolume;
		quote.ID = marketData->InstrumentID;
		quote.Last_Price = USHPriceHelper::Price(marketData->LastPrice);

		quote.Total_Volume = marketData->Volume;
		if (quote.Total_Volume != preVolume[quote.ID].totalVolume){
			quote.Last_Volume = preVolume[quote.ID].lastVolume = 
				quote.Total_Volume - preVolume[quote.ID].totalVolume ;
			preVolume[quote.ID].totalVolume = quote.Total_Volume;
		}else
			quote.Last_Volume = preVolume[quote.ID].lastVolume;

		quote.BidPrice = USHPriceHelper::Price(marketData->BidPrice1);
		quote.BidQty = marketData->BidVolume1;
		quote.AskPrice = USHPriceHelper::Price(marketData->AskPrice1);
		quote.AskQty = marketData->AskVolume1;
		quote.LowPrice = USHPriceHelper::Price(marketData->LowestPrice);
		quote.HighPrice = USHPriceHelper::Price(marketData->HighestPrice);
		quote.Open_Price = USHPriceHelper::Price(marketData->OpenPrice);
		quote.SettlementPrice = USHPriceHelper::Price(
			marketData->SettlementPrice);

		int32 year = 0,month = 0,day = 0,hour = 0,minute = 0,second = 0;
		std::sscanf(marketData->TradingDay,"%4d%2d%2d",&year,&month,&day);
		std::sscanf(marketData->UpdateTime,"%2d:%2d:%2d",&hour,&minute,&second);
		quote.RecieveTime.SetYMD(year,month,day);
		quote.RecieveTime.SetHMS(hour,minute,second,marketData->UpdateMillisec);
		dom.RecieveTime = quote.RecieveTime;

		dom.CurrentPrice = quote.Last_Price;
		dom.ID = quote.ID;
		std::printf("%s::%f\n",dom.ID.c_str(),dom.CurrentPrice);

		dom.ID = quote.ID;
		dom.LastQuantity = quote.Last_Volume;
		dom.Open_Price = quote.Open_Price;
		dom.LowPrice = quote.LowPrice;
		dom.HighPrice = quote.HighPrice;
		dom.Total_Volume = quote.Total_Volume;
		dom.SettlementPrice = quote.SettlementPrice;

		dom.AskLevel[0] = USHPriceHelper::Price(marketData->AskPrice1);
		dom.AskLevel[1] = USHPriceHelper::Price(marketData->AskPrice2);
		dom.AskLevel[2] = USHPriceHelper::Price(marketData->AskPrice3);
		dom.AskLevel[3] = USHPriceHelper::Price(marketData->AskPrice4);
		dom.AskLevel[4] = USHPriceHelper::Price(marketData->AskPrice5);
		dom.AskLevel[5] = dom.AskLevel[6] = dom.AskLevel[7] = dom.AskLevel[8] = 
			dom.AskLevel[9] = 0.0;
		dom.BidLevel[0] = USHPriceHelper::Price(marketData->BidPrice1);
		dom.BidLevel[1] = USHPriceHelper::Price(marketData->BidPrice2);
		dom.BidLevel[2] = USHPriceHelper::Price(marketData->BidPrice3);
		dom.BidLevel[3] = USHPriceHelper::Price(marketData->BidPrice4);
		dom.BidLevel[4] = USHPriceHelper::Price(marketData->BidPrice5);
		dom.BidLevel[5] = dom.BidLevel[6] = dom.BidLevel[7] = dom.BidLevel[8] = 
			dom.BidLevel[9] = 0.0;

		dom.AskQuantities[0] = marketData->AskVolume1;
		dom.AskQuantities[1] = marketData->AskVolume2;
		dom.AskQuantities[2] = marketData->AskVolume3;
		dom.AskQuantities[3] = marketData->AskVolume4;
		dom.AskQuantities[4] = marketData->AskVolume5;
		dom.AskQuantities[5] = dom.AskQuantities[6] = dom.AskQuantities[7] = 
			dom.AskQuantities[8] = dom.AskQuantities[9] = 0.0;

		dom.BidQuantities[0] = marketData->BidVolume1;
		dom.BidQuantities[1] = marketData->BidVolume2;
		dom.BidQuantities[2] = marketData->BidVolume3;
		dom.BidQuantities[3] = marketData->BidVolume4;
		dom.BidQuantities[4] = marketData->BidVolume5;
		dom.BidQuantities[5] = dom.BidQuantities[6] = dom.BidQuantities[7] = 
			dom.BidQuantities[8] = dom.BidQuantities[9] = 0.0;
		// 。。。。。。结束深度数据转换
		dom.SupportLevel = 5;

		return 0;
	}

	//撤单请求转换
	int USHDataConvertHelper::TranslatePublicDataToCancelOrderData(
		const SHTraderProvider::InitParam *pInitParam,
		const OrderCancelRequest &clientOrder,
		CShfeFtdcOrderActionField* pApiOrder)
	{
		memset(pApiOrder,0,sizeof(CShfeFtdcOrderActionField));

		DBG_ASSERT(NULL != pApiOrder);

		pApiOrder->ActionFlag = SHFE_FTDC_AF_Delete;
		LIB_NS::UString::Copy(pApiOrder->OrderLocalID,
			clientOrder.zjIDLocalOrder);
		LIB_NS::UString::Copy(pApiOrder->ParticipantID,
			pInitParam->sParticipantID);
		LIB_NS::UString::Copy(pApiOrder->UserID,pInitParam->sUserID);
#if defined(RMMS_TEST_CLIENT)
		LIB_NS::UString::Copy(pApiOrder->ClientID, RMMS_SH_CLIENT);
#else
		LIB_NS::UString::Copy(pApiOrder->ClientID, clientOrder.IDClient);
#endif
		LIB_NS::UString::FillChar(pApiOrder->OrderSysID,13,' ');

		LIB_NS::UString::FillEnd(pApiOrder->OrderSysID,
			clientOrder.IDSysOrder.c_str());
		strcpy(pApiOrder->ActionLocalID, "");
		pApiOrder->LimitPrice = 0.0;
		pApiOrder->VolumeChange = 0;
		strcpy(pApiOrder->BusinessUnit, "");

		return 0;
	}

	//修改单请求转换
	int USHDataConvertHelper::TranslatePublicDataToCancelRelaceOrderData(
		const OrderCancelReplaceRequest &clientOrder,
		CShfeFtdcOrderActionField* pApiOrder)
	{
		memset(pApiOrder,0,sizeof(CShfeFtdcOrderActionField));

		DBG_ASSERT(NULL != pApiOrder);

		pApiOrder->ActionFlag = SHFE_FTDC_AF_Modify;
		LIB_NS::UString::Copy(pApiOrder->OrderLocalID,
			clientOrder.zjIDLocalOrder);
		//LIB_NS::UString::Copy(pApiOrder->ParticipantID,
		//	clientOrder.IDParticipant);
		//LIB_NS::UString::Copy(pApiOrder->UserID,
		//	clientOrder.IDUser);
		LIB_NS::UString::Copy(pApiOrder->ClientID,
			clientOrder.IDClient);

		LIB_NS::UString::FillChar(pApiOrder->OrderSysID,13,' ');
		LIB_NS::UString::FillEnd(pApiOrder->OrderSysID,
			clientOrder.IDSysOrder.c_str());
		strcpy(pApiOrder->ActionLocalID, "");
		pApiOrder->LimitPrice = 0.0;
		pApiOrder->VolumeChange = 0;
		strcpy(pApiOrder->BusinessUnit, "");

		return 0;
	}
	
	//下单响应转换
	int USHDataConvertHelper::TraslateExecutionReportToPublicData(
		const CShfeFtdcInputOrderField &apiReport,
		ExecutionReport *pClientReport)
	{
		DBG_ASSERT(NULL != pClientReport);

		/*
#if defined(RMMS_TEST_CLIENT)
		pClientReport->IDClient = RMMS_ZJ_GLOTRADER;
#else
		pClientReport->IDClient = apiReport.ClientID;
#endif
		*/
		sprintf(pClientReport->zjIDLocalOrder,apiReport.OrderLocalID,
			strlen(pClientReport->zjIDLocalOrder));
		pClientReport->IDSysOrder = apiReport.OrderSysID;

		pClientReport->PriceLimit = apiReport.LimitPrice;
		pClientReport->PriceStop = 0;
		pClientReport->QtyOrder = apiReport.VolumeTotalOriginal;

		pClientReport->OrderStatus = OrderStatusFlag::New;
		USHEnumsConvert::Convert2RmmSide(apiReport.Direction,
			pClientReport->Side);
		pClientReport->ExecType = ExecTypeFlag::ExecType_New;

		pClientReport->SecurityDesc = apiReport.InstrumentID;
		pClientReport->datetime = LIB_NS::DateTime::NowUtc();

		return 0;
	}

	//下单拒绝转换
	int USHDataConvertHelper::TraslateExecutionReportToPublicData(
		const CShfeFtdcInputOrderField &apiReport,
		BusinessReject *pClientReject, CShfeFtdcRspInfoField *pRspInfo,
		int requestID)
	{
		DBG_ASSERT(NULL != pClientReject);

		sprintf(pClientReject->zjIDLocalOrder,apiReport.OrderLocalID,
			strlen(pClientReject->zjIDLocalOrder));
		pClientReject->LastMsgSeqNumProcessed = requestID;
		pClientReject->RefMsgType = "D";
		pClientReject->BusinessRejectReason = 
			BusinessRejectReasonFlag::BRR_Other;

		strstream ss;
		ss<<pRspInfo->ErrorID;
		ss>>pClientReject->ErrorCode;
		ss.clear();

		pClientReject->ErrorSource = "SHS";

		return 0;
	}


	int USHDataConvertHelper::TranslateQuoteDataToPublicData(
		const CShfeFtdcOrderActionField &apiReport,
		ExecutionReport* pClientReport)
	{
		return 0;
	}

	//撤单拒绝转换
	int USHDataConvertHelper::TraslateExecutionReportToPublicData(
		const CShfeFtdcOrderActionField &apiReport,
		OrderCancelReject *pClientReject,CShfeFtdcRspInfoField *pRspInfo,
		int requestID)
	{
		DBG_ASSERT(NULL != pClientReject);

		pClientReject->IDSysOrder = apiReport.OrderSysID;
		pClientReject->LastMsgSeqNumProcessed = requestID;

		strstream ss;
		ss<<pRspInfo->ErrorID;
		ss>>pClientReject->ErrorCode;
		ss.clear();

		pClientReject->ErrorSource = "SHS";

		return 0;
	}

	//改单拒绝转换
	int USHDataConvertHelper::TraslateExecutionReportToPublicData(
		const CShfeFtdcOrderActionField &apiReport,
		OrderCancelReplaceReject* pClientReject,CShfeFtdcRspInfoField *pRspInfo,
		int requestID)
	{
		DBG_ASSERT(NULL != pClientReject);

		pClientReject->IDSysOrder = apiReport.OrderSysID;
		pClientReject->LastMsgSeqNumProcessed = requestID;

		strstream ss;
		ss<<pRspInfo->ErrorID;
		ss>>pClientReject->ErrorCode;
		ss.clear();

		pClientReject->ErrorSource = "SHS";

		return 0;
	}

	//报单状态改变转换
	int USHDataConvertHelper::TraslateExecutionReportToPublicData(
		const CShfeFtdcOrderField &apiReport, ExecutionReport *pClientReport)
	{
		DBG_ASSERT(NULL != pClientReport);

		if(apiReport.OrderStatus == SHFE_FTDC_OST_AllTraded ||
			apiReport.OrderStatus == SHFE_FTDC_OST_PartTradedQueueing ||
			apiReport.OrderStatus == SHFE_FTDC_OST_PartTradedNotQueueing ||
			apiReport.OrderStatus == SHFE_FTDC_OST_NoTradeQueueing)
			return 0;

		/*
#if defined(RMMS_TEST_CLIENT)
		pClientReport->IDClient = RMMS_ZJ_GLOTRADER;
#else
		pClientReport->IDClient = apiReport.ClientID;
#endif
		*/
		sprintf(pClientReport->zzIDLocalOrder,apiReport.OrderLocalID,
			std::strlen(pClientReport->zjIDLocalOrder));
		pClientReport->IDSysOrder = apiReport.OrderSysID;
		USHEnumsConvert::Convert2RmmOrderStatus(apiReport.OrderStatus,
			pClientReport->OrderStatus);
		pClientReport->PriceLimit = apiReport.LimitPrice;
		pClientReport->PriceStop = apiReport.StopPrice;
		pClientReport->QtyOrder = apiReport.VolumeTotalOriginal;
		USHEnumsConvert::Convert2RmmSide(apiReport.Direction,
			pClientReport->Side);
		USHEnumsConvert::Convert2RmmExecType(pClientReport->OrderStatus,
			pClientReport->ExecType);
		
		pClientReport->SecurityDesc = apiReport.InstrumentID;

		pClientReport->datetime = LIB_NS::DateTime::NowUtc();
		
		return 0;
	}

	int USHDataConvertHelper::TraslateExecutionReportToPublicData(
		const CShfeFtdcTradeField &apiReport, FillReport *pClientReport)
	{
		DBG_ASSERT(NULL != pClientReport);

#if defined(RMMS_TEST_CLIENT)
		pClientReport->IDClient = RMMS_SH_GLOTRADER;
#else
		pClientReport->IDClient = apiReport.ClientID;
#endif
		sprintf(pClientReport->zjIDLocalOrder,apiReport.OrderLocalID,
			std::strlen(pClientReport->zjIDLocalOrder));
		pClientReport->IDSysOrder = apiReport.OrderSysID;
		pClientReport->IDExec = apiReport.TradeID;
		pClientReport->LastPx = apiReport.Price;
		pClientReport->QtyLastShares = apiReport.Volume;

		USHEnumsConvert::Convert2RmmSide(apiReport.Direction,
			pClientReport->Side);
		pClientReport->SecurityDesc = apiReport.InstrumentID;

		int year = (apiReport.TradingDay[0]-'0') * 1000 + 
			(apiReport.TradingDay[1]-'0') * 100 + 
			(apiReport.TradingDay[2]-'0') * 10 + 
			(apiReport.TradingDay[3]-'0');
		int month = (apiReport.TradingDay[4]-'0') * 10 + 
			(apiReport.TradingDay[5]-'0');
		int day = (apiReport.TradingDay[6]-'0') * 10 + 
			(apiReport.TradingDay[7]-'0');
		int hour = (apiReport.TradeTime[0]-'0') * 10 + 
			(apiReport.TradeTime[1]-'0');
		int minute = (apiReport.TradeTime[3]-'0') * 10 + 
			(apiReport.TradeTime[4]-'0');
		int second = (apiReport.TradeTime[6]-'0') * 10 + 
			(apiReport.TradeTime[7]-'0');

		pClientReport->TradeTime.SetYMD(year,month,day);
		pClientReport->TradeTime.SetHMS(hour,minute,second,0);

		return 0;
	}

	//公告转换
	int USHDataConvertHelper::TranslatePublicDataToBulletin(
		const CShfeFtdcBulletinField &apiBulletin, Bulletin *pClientBulletin)
	{
		pClientBulletin->Headline = apiBulletin.Abstract;
		pClientBulletin->Content = apiBulletin.Content;
		pClientBulletin->URLLink = apiBulletin.URLLink;

		int year = (apiBulletin.TradingDay[0]-'0') * 1000 + 
			(apiBulletin.TradingDay[1]-'0') * 100 + 
			(apiBulletin.TradingDay[2]-'0') * 10 + 
			(apiBulletin.TradingDay[3]-'0');
		int month = (apiBulletin.TradingDay[4]-'0') * 10 + 
			(apiBulletin.TradingDay[5]-'0');
		int day = (apiBulletin.TradingDay[6]-'0') * 10 + 
			(apiBulletin.TradingDay[7]-'0');
		int hour = (apiBulletin.SendTime[0]-'0') * 10 +
			(apiBulletin.SendTime[1]-'0');
		int minute = (apiBulletin.SendTime[3]-'0') * 10 + 
			(apiBulletin.SendTime[4]-'0');
		int second = (apiBulletin.SendTime[6]-'0') * 10 + 
			(apiBulletin.SendTime[7]-'0');

		pClientBulletin->SendTime.SetYMD(year,month,day);
		pClientBulletin->SendTime.SetHMS(hour,minute,second,0);
		return 0;
	}
}