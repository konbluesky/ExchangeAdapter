﻿namespace RS.CmeFastHandler
{
	public class SecurityDefinitionEntity
	{
		#region Properties

		public string CFICode
		{
			get;
			set;
		}

		public string MaturityMonthYear
		{
			get;
			set;
		}

		public string SecurityDesc
		{
			get;
			set;
		}

		public string SecurityExchange
		{
			get;
			set;
		}

		public string SecurityGroup
		{
			get;
			set;
		}

		public string SecurityId
		{
			get;
			set;
		}

		public string StrikePrice
		{
			get;
			set;
		}

		#endregion Properties
	}
}