﻿namespace RS.CmeFastHandler
{
	using System.Threading;

	public class EventWaitFlags
	{


		EventWaitHandle eventDecode = new EventWaitHandle(false, EventResetMode.ManualReset);
		EventWaitHandle eventIncremntData = new EventWaitHandle(false, EventResetMode.ManualReset);
		EventWaitHandle eventScuiryDefinition = new EventWaitHandle(false, EventResetMode.ManualReset);
		EventWaitHandle eventSnapShot = new EventWaitHandle(false, EventResetMode.ManualReset);



		#region Properties

		public EventWaitHandle EventDecode
		{
			get
			{
				return eventDecode;
			}
		}

		public EventWaitHandle EventIncremntData
		{
			get
			{
				return eventIncremntData;
			}
		}

		public EventWaitHandle[] EventList
		{
			get
			{
				return new EventWaitHandle[] { eventScuiryDefinition, eventIncremntData, eventSnapShot, eventDecode };
			}
		}

		//EventWaitHandle[] eventList;
		public EventWaitHandle EventScuiryDefinition
		{
			get
			{
				return eventScuiryDefinition;
			}
		}

		public EventWaitHandle EventSnapShot
		{
			get
			{
				return eventSnapShot;
			}
		}

		#endregion Properties
	}
}