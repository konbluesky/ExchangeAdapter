﻿namespace RS.CmeFastHandler
{
	using System;
	using System.Collections.Generic;
	using System.Text;

	public class SnapShotEntity
	{


		List<SnapShotLevel> bid = new List<SnapShotLevel>();
		string highBid = "";
		string highPrice = "";
		string lowOffer = "";
		string lowPrice = "";
		List<SnapShotLevel> offer = new List<SnapShotLevel>();
		string openingPrice = "";
		string securityDesc;
		string securityId;
		string sendingTime = "";
		string settlementPrice = "";
		string totalVolumeTraded = "";
		string tradePrice = "";
		string tradeSize = "";



		#region Properties

		public List<SnapShotLevel> Bid
		{
			set { this.bid = value; }
			get { return this.bid; }
		}

		public string HighBid
		{
			set { this.highBid = value; }
			get { return this.highBid; }
		}

		public string HighPrice
		{
			set { this.highPrice = value; }
			get { return this.highPrice; }
		}

		public string LowOffer
		{
			set { this.lowOffer = value; }
			get { return this.lowOffer; }
		}

		public string LowPrice
		{
			set { this.lowPrice = value; }
			get { return this.lowPrice; }
		}

		public List<SnapShotLevel> Offer
		{
			set { this.offer = value; }
			get { return this.offer; }
		}

		public string OpeningPrice
		{
			set { this.openingPrice = value; }
			get { return this.openingPrice; }
		}

		public string SecurityDesc
		{
			set { this.securityDesc = value; }
			get { return this.securityDesc; }
		}

		public string SecurityId
		{
			set { this.securityId = value; }
			get { return this.securityId; }
		}

		public string SendingTime
		{
			set { this.sendingTime = value; }
			get { return this.sendingTime; }
		}

		public string SettlementPrice
		{
			set { this.settlementPrice = value; }
			get { return this.settlementPrice; }
		}

		public string TotalVolumeTraded
		{
			set { this.totalVolumeTraded = value; }
			get { return this.totalVolumeTraded; }
		}

		public string TradePrice
		{
			set { this.tradePrice = value; }
			get { return this.tradePrice; }
		}

		public string TradeSize
		{
			set { this.tradeSize = value; }
			get { return this.tradeSize; }
		}

		#endregion Properties



		/// <summary>
		/// Entity String
		/// </summary>
		/// <returns></returns>
		public override String ToString()
		{
			StringBuilder sb = new StringBuilder();
			sb.Append("SecurityId=").Append(SecurityId).
			Append(",SecurityDesc").Append(SecurityDesc).
			Append(",HighPrice").Append(HighPrice).
			Append(",LowPrice").Append(LowPrice).
			Append(",HighBid").Append(HighBid).
			Append(",LowOffer").Append(LowOffer).
			Append(",TradePrice").Append(TradePrice).
			Append(",TradeSize").Append(TradeSize).
			Append(",SendingTime").Append(SendingTime).
			Append(",TotalVolumeTraded").Append(TotalVolumeTraded).Append(Environment.NewLine);
			for (int i = 0; i < 10; i++)
			{
				if (bid.Count > i)
				{
					SnapShotLevel bidItem = bid[i];
					sb.AppendFormat("{0,2}", i + 1).
					AppendFormat("{0,10}", bidItem.Quantity).
					AppendFormat("{0,10}", bidItem.NumberOfOrders).
					AppendFormat("{0,10}", bidItem.Price);
				}
				else
				{
					sb.AppendFormat("{0,2}", i + 1).
					AppendFormat("{0,10}", "0").
					AppendFormat("{0,10}", "0").
					AppendFormat("{0,10}", "0");
				}
				if (offer.Count > i)
				{
					SnapShotLevel offerItem = Offer[i];
					sb.AppendFormat("{0,10}", offerItem.Price).
					AppendFormat("{0,10}", offerItem.Quantity).
					AppendFormat("{0,10}", offerItem.NumberOfOrders);
				}
				else
				{
					sb.AppendFormat("{0,10}", "0").
					AppendFormat("{0,10}", "0").
					AppendFormat("{0,10}", "0");
				}
				sb.Append(Environment.NewLine);
			}
			return sb.ToString();
		}


	}

	public class SnapShotLevel
	{
		#region Properties

		public string NumberOfOrders
		{
			set;
			get;
		}

		public string Price
		{
			set;
			get;
		}

		public string Quantity
		{
			set;
			get;
		}

		#endregion Properties
	}
}