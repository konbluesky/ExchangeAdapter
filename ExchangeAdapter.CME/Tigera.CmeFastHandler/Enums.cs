﻿namespace RS.CmeFastHandler
{
	#region Enumerations

	/// <summary>
	/// This is type of channel server ,Mapping with column of "feed-type" .
	/// </summary>
	public enum CHANNELTYPE
	{
		/// <summary>
		/// Incremental
		/// </summary>
		I,
		/// <summary>
		/// Snapshot
		/// </summary>
		S,
		/// <summary>
		/// InstrumentReplay
		/// </summary>
		N,
		/// <summary>
		/// HistoricalReplay
		/// </summary>
		H
	}

	/// <summary>
	/// Connection type
	/// </summary>
	public enum ConnectionType
	{
		// Summary:
		//     Receive SecurityDefinition.
		SecurityDefinition = 0,
		//
		// Summary:
		//     Receive IncrementData.
		IncrementData = 1,
		//
		// Summary:
		//     Receive Snapshot.
		SnapShot = 2,
		//
		// Summary:
		//     Tcp History.
		TcpHistory = 3,
	}

	// Define combobox control (it's use for test)
	public enum DATATYPE
	{
		UDPA, UDPB, PROC
	}

	/// <summary>
	/// Feed : A server or B server
	/// </summary>
	public enum FEED
	{
		A,
		B,
		A_AND_B
	}

	/// <summary>
	/// Handler state.
	/// </summary>
	public enum HandlerState
	{
		// Summary:
		//     Handler is stopped.
		Stopped = 0,
		//
		// Summary:
		//     Subscribed to Instrument Definition stream.
		SecurityDefinitionsRecoveryStarted = 1,
		//
		// Summary:
		//     All instruments are received.
		SecurityDefinitionsRecoveryFinished = 2,
		//
		// Summary:
		//     The correct books state is lost.
		//
		// Remarks:
		//     Handler switches to this state when it joins the market and the initial books
		//     state is built from MarketDataSnapshotFullRefresh (W) messages or when the
		//     TcpReplay is not used and the message sequence number gap is detected, so
		//     the book state should be re-built from MarketDataSnapshotFullRefresh (W)
		//     messages again. It should be assumed at this point that all books maintained
		//     in the Handler may no longer have the correct, latest state maintained by
		//     CME, so they are cleared.
		BooksResynchronizationStarted = 3,
		//
		// Summary:
		//     The latest books state is restored.
		//
		// Remarks:
		//     Handler switches to this state when the the latest books state is restored
		//     from MarketDataSnapshotFullRefresh (W() messages (all snapshot data is retrieved).
		//      It should be assumed at this point that all books maintained in the Handler
		//     have the correct, latest state maintained by CME.
		BooksResynchronizationFinished = 4,
		//
		// Summary:
		//     TCP Replays is started.
		TcpReplayStarted = 5,
		//
		// Summary:
		//     TCP Replays is finished.
		TcpReplayFinished = 6,
	}

	/// <summary>
	/// Side tag Buy or Sell
	/// </summary>
	public enum Side
	{
		Buy = 0,
		Sell = 1
	}

	#endregion Enumerations
}