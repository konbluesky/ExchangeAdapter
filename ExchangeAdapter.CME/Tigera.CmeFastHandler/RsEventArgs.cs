﻿namespace RS.CmeFastHandler
{
	using System;

	public class RsErrorEventArgs : EventArgs
	{
		#region Properties

		public string ChannelId
		{
			get;
			set;
		}

		public string Message
		{
			get;
			set;
		}

		#endregion Properties



		public override string ToString()
		{
			return "Channel:" + ChannelId + "=" + Message;
		}


	}

	public class RsIncrementEventArgs : EventArgs
	{
		#region Properties

		public string ChannelId
		{
			get;
			set;
		}

		public OpenFAST.Message Message
		{
			get;
			set;
		}

		public string TradePrice
		{
			get;
			set;
		}

		public string TradeSize
		{
			get;
			set;
		}

		#endregion Properties
	}

	public class RsInfoEventArgs : EventArgs
	{
		#region Properties

		public string ChannelId
		{
			get;
			set;
		}

		public string Message
		{
			get;
			set;
		}

		#endregion Properties



		public override string ToString()
		{
			return "Channel:" + ChannelId + "=" + Message;
		}


	}

	public class RsSDEventArgs : EventArgs
	{
		#region Properties

		public string ChannelID
		{
			get;
			set;
		}

		public OpenFAST.Message Message
		{
			get;
			set;
		}

		public string SecurityDesc
		{
			get;
			set;
		}

		public string SecurityId
		{
			get;
			set;
		}

		#endregion Properties
	}

	public class RsSnapshotEventArgs : EventArgs
	{
		#region Properties

		public string ChannelId
		{
			get;
			set;
		}

		public string Key
		{
			get;
			set;
		}

		public OpenFAST.Message Message
		{
			get;
			set;
		}

		#endregion Properties
	}

	public class RsStatusEventArgs : EventArgs
	{
		#region Properties

		public string ChannelId
		{
			get;
			set;
		}

		public string Message
		{
			get;
			set;
		}

		#endregion Properties



		public override string ToString()
		{
			return "Channel:" + ChannelId + "=" + Message;
		}


	}
}