﻿namespace RS.CmeFastHandler
{
	public class ConnectionInfo
	{
		#region Properties

		public string Tcp
		{
			get;
			set;
		}

		public int Tcp_Port
		{
			get;
			set;
		}

		public string UdpA
		{
			get;
			set;
		}

		public int UdpA_Port
		{
			get;
			set;
		}

		public string UdpB
		{
			get;
			set;
		}

		public int UdpB_Port
		{
			get;
			set;
		}

		#endregion Properties
	}
}