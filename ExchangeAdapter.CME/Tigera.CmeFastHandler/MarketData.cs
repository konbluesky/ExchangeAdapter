﻿namespace RS.CmeFastHandler
{
	/// <summary>
	/// CME Fast Data
	/// </summary>
	public class MarketData
	{


		private SecurityDefinitionEntity sdEntity;   //Definition
		private SnapShotEntity ssEntity; //snapshot



		#region Properties

		public SecurityDefinitionEntity SdEntity
		{
			set { sdEntity = value; }
			get { return sdEntity; }
		}

		public SnapShotEntity SsEntity
		{
			set { ssEntity = value; }
			get { return ssEntity; }
		}

		#endregion Properties
	}
}