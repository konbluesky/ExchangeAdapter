/*

The contents of this file are subject to the Mozilla Public License
Version 1.1 (the "License"); you may not use this file except in
compliance with the License. You may obtain a copy of the License at
http://www.mozilla.org/MPL/

Software distributed under the License is distributed on an "AS IS"
basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
License for the specific language governing rights and limitations
under the License.

The Original Code is OpenFAST.

The Initial Developer of the Original Code is The LaSalle Technology
Group, LLC.  Portions created by Shariq Muhammad
are Copyright (C) Shariq Muhammad. All Rights Reserved.

Contributor(s): Shariq Muhammad <shariq.muhammad@gmail.com>

*/
namespace OpenFAST
{
	public interface MessageBlockReader
	{


		void MessageRead(System.IO.Stream in_Renamed, Message message);

		bool ReadBlock(System.IO.Stream in_Renamed);


	}

	public struct MessageBlockReader_Fields
	{


		public static readonly MessageBlockReader NULL;





		static MessageBlockReader_Fields()
		{
			NULL = new NullMessageBlockReader();
		}


	}

	public sealed class NullMessageBlockReader : MessageBlockReader
	{


		public void MessageRead(System.IO.Stream in_Renamed, Message message)
		{
		}

		public bool ReadBlock(System.IO.Stream in_Renamed)
		{
			return true;
		}


	}
}