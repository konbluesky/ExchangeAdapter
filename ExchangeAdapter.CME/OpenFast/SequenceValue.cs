#region Header

/*

The contents of this file are subject to the Mozilla Public License
Version 1.1 (the "License"); you may not use this file except in
compliance with the License. You may obtain a copy of the License at
http://www.mozilla.org/MPL/

Software distributed under the License is distributed on an "AS IS"
basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
License for the specific language governing rights and limitations
under the License.

The Original Code is OpenFAST.

The Initial Developer of the Original Code is The LaSalle Technology
Group, LLC.  Portions created by Shariq Muhammad
are Copyright (C) Shariq Muhammad. All Rights Reserved.

Contributor(s): Shariq Muhammad <shariq.muhammad@gmail.com>

*/

#endregion Header

namespace OpenFAST
{
	using System;
	using System.Collections.Generic;
	using System.Text;

	using Sequence = OpenFAST.Template.Sequence;

	[Serializable]
	public sealed class SequenceValue : FieldValue
	{


		private readonly List<GroupValue> elements = new List<GroupValue>();
		private readonly Sequence sequence;





		public SequenceValue(Sequence sequence)
		{
			if (sequence == null)
			{
				throw new NullReferenceException();
			}

			this.sequence = sequence;
		}



		#region Properties

		public int Length
		{
			get
			{
				return elements.Count;
			}
		}

		public Sequence Sequence
		{
			get
			{
				return sequence;
			}
		}

		public GroupValue[] Values
		{
			get
			{
				return elements.ToArray();
			}
		}

		#endregion Properties

		#region Indexers

		public GroupValue this[int index]
		{
			get
			{
				return elements[index];
			}
		}

		#endregion Indexers



		public void Add(GroupValue value_Renamed)
		{
			elements.Add(value_Renamed);
		}

		public void Add(FieldValue[] values)
		{
			elements.Add(new GroupValue(sequence.Group, values));
		}

		public FieldValue Copy()
		{
			var value_Renamed = new SequenceValue(sequence);
			for (int i = 0; i < elements.Count; i++)
			{
				value_Renamed.Add((GroupValue)elements[i].Copy());
			}
			return value_Renamed;
		}

		public override bool Equals(object other)
		{
			if (other == this)
			{
				return true;
			}

			if ((other == null) || !(other is SequenceValue))
			{
				return false;
			}

			return equals((SequenceValue)other);
		}

		public override int GetHashCode()
		{
			return elements.GetHashCode() * 37 + sequence.GetHashCode();
		}

		public System.Collections.IEnumerator Iterator()
		{
			return elements.GetEnumerator();
		}

		public override string ToString()
		{
			var builder = new StringBuilder();
			System.Collections.IEnumerator iter = elements.GetEnumerator();
			builder.Append("[ ");

			while (iter.MoveNext())
			{
				var value_Renamed = (GroupValue)iter.Current;
				builder.Append('[').Append(value_Renamed).Append("] ");
			}

			builder.Append("]");

			return builder.ToString();
		}

		private bool equals(SequenceValue other)
		{
			if (Length != other.Length)
			{
				return false;
			}

			for (int i = 0; i < Length; i++)
			{
				if (!elements[i].Equals(other.elements[i]))
				{
					return false;
				}
			}

			return true;
		}


	}
}