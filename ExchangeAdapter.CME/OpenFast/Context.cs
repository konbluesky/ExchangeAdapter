#region Header

/*

The contents of this file are subject to the Mozilla Public License
Version 1.1 (the "License"); you may not use this file except in
compliance with the License. You may obtain a copy of the License at
http://www.mozilla.org/MPL/

Software distributed under the License is distributed on an "AS IS"
basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
License for the specific language governing rights and limitations
under the License.

The Original Code is OpenFAST.

The Initial Developer of the Original Code is The LaSalle Technology
Group, LLC.  Portions created by Shariq Muhammad
are Copyright (C) Shariq Muhammad. All Rights Reserved.

Contributor(s): Shariq Muhammad <shariq.muhammad@gmail.com>

*/

#endregion Header

namespace OpenFAST
{
	using BasicTemplateRegistry = OpenFAST.Template.BasicTemplateRegistry;

	using ErrorHandler = OpenFAST.Error.ErrorHandler;

	using Group = OpenFAST.Template.Group;

	using MessageTemplate = OpenFAST.Template.MessageTemplate;

	using TemplateRegistry = OpenFAST.Template.TemplateRegistry;

	public sealed class Context
	{


		private readonly System.Collections.Generic.Dictionary<string, Dictionary> dictionaries = new System.Collections.Generic.Dictionary<string, Dictionary>();

		private QName currentApplicationType;
		private ErrorHandler errorHandler = Error.ErrorHandler_Fields.DEFAULT;
		private TemplateRegistry templateRegistry = new BasicTemplateRegistry();





		public Context()
		{
			dictionaries["global"] = new GlobalDictionary();
			dictionaries["template"] = new TemplateDictionary();
			dictionaries["type"] = new ApplicationTypeDictionary();
		}



		#region Properties

		public QName CurrentApplicationType
		{
			set
			{
				currentApplicationType = value;
			}
		}

		public ErrorHandler ErrorHandler
		{
			set
			{
				errorHandler = value;
			}
		}

		public int LastTemplateId
		{
			get; set;
		}

		public TemplateRegistry TemplateRegistry
		{
			get
			{
				return templateRegistry;
			}

			set
			{
				templateRegistry = value;
			}
		}

		public bool TraceEnabled
		{
			get; set;
		}

		#endregion Properties



		public MessageTemplate GetTemplate(int templateId)
		{
			if (!templateRegistry.IsRegistered(templateId))
			{
				errorHandler.Error(Error.FastConstants.D9_TEMPLATE_NOT_REGISTERED, "The template with id " + templateId + " has not been registered.");
				return null;
			}
			return templateRegistry.get_Renamed(templateId);
		}

		public int GetTemplateId(MessageTemplate template)
		{
			if (!templateRegistry.IsRegistered(template))
			{
				errorHandler.Error(Error.FastConstants.D9_TEMPLATE_NOT_REGISTERED, "The template " + template + " has not been registered.");
				return 0;
			}
			return templateRegistry.GetId(template);
		}

		public ScalarValue Lookup(string dictionary, Group group, QName key)
		{
			if (group.HasTypeReference())
				currentApplicationType = group.TypeReference;
			return GetDictionary(dictionary).Lookup(group, key, currentApplicationType);
		}

		public void NewMessage(MessageTemplate template)
		{
			currentApplicationType = (template.HasTypeReference()) ? template.TypeReference : Error.FastConstants.ANY_TYPE;
		}

		public void RegisterTemplate(int templateId, MessageTemplate template)
		{
			templateRegistry.Register(templateId, template);
		}

		public void Reset()
		{
			foreach (Dictionary dict in dictionaries.Values)
			{
				dict.Reset();
			}
		}

		public void StartTrace()
		{
		}

		public void Store(string dictionary, Group group, QName key, ScalarValue valueToEncode)
		{
			if (group.HasTypeReference())
				currentApplicationType = group.TypeReference;
			GetDictionary(dictionary).Store(group, currentApplicationType, key, valueToEncode);
		}

		private Dictionary GetDictionary(string dictionary)
		{
			if (!dictionaries.ContainsKey(dictionary))
				dictionaries[dictionary] = new GlobalDictionary();
			return dictionaries[dictionary];
		}


	}
}