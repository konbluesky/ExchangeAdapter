#region Header

/*

The contents of this file are subject to the Mozilla Public License
Version 1.1 (the "License"); you may not use this file except in
compliance with the License. You may obtain a copy of the License at
http://www.mozilla.org/MPL/

Software distributed under the License is distributed on an "AS IS"
basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
License for the specific language governing rights and limitations
under the License.

The Original Code is OpenFAST.

The Initial Developer of the Original Code is The LaSalle Technology
Group, LLC.  Portions created by Shariq Muhammad
are Copyright (C) Shariq Muhammad. All Rights Reserved.

Contributor(s): Shariq Muhammad <shariq.muhammad@gmail.com>

*/

#endregion Header

namespace OpenFAST.Template.Operator
{
	using System;

	using FASTType = OpenFAST.Template.Type.FASTType;

	using Key = OpenFAST.util.Key;

	[Serializable]
	public abstract class OperatorCodec
	{


		protected internal static readonly OperatorCodec CONSTANT_ALL;
		protected internal static readonly OperatorCodec COPY_ALL = new CopyOperatorCodec();
		protected internal static readonly OperatorCodec DEFAULT_ALL;
		protected internal static readonly OperatorCodec DELTA_DECIMAL = new DeltaDecimalOperatorCodec();
		protected internal static readonly OperatorCodec DELTA_INTEGER;
		protected internal static readonly OperatorCodec DELTA_STRING = new DeltaStringOperatorCodec();
		protected internal static readonly OperatorCodec INCREMENT_INTEGER;
		protected internal static readonly OperatorCodec NONE_ALL;
		protected internal static readonly OperatorCodec TAIL;

		private static readonly System.Collections.Generic.Dictionary<Key, OperatorCodec> OPERATOR_MAP = new System.Collections.Generic.Dictionary<Key, OperatorCodec>();

		private readonly Operator _Renamed;





		static OperatorCodec()
		{
			NONE_ALL = new NoneOperatorCodec(Operator.NONE, FASTType.ALL_TYPES());
			CONSTANT_ALL = new ConstantOperatorCodec(Operator.CONSTANT, FASTType.ALL_TYPES());
			DEFAULT_ALL = new DefaultOperatorCodec(Operator.DEFAULT, FASTType.ALL_TYPES());
			INCREMENT_INTEGER = new IncrementIntegerOperatorCodec(Operator.INCREMENT, FASTType.INTEGER_TYPES);
			DELTA_INTEGER = new DeltaIntegerOperatorCodec(Operator.DELTA, FASTType.INTEGER_TYPES);
			TAIL = new TailOperatorCodec(Operator.TAIL, new[] { FASTType.ASCII, FASTType.STRING, FASTType.UNICODE, FASTType.BYTE_VECTOR });
		}

		protected internal OperatorCodec(Operator operator_Renamed, FASTType[] types)
		{
			this._Renamed = operator_Renamed;
			for (int i = 0; i < types.Length; i++)
			{
				var key = new Key(_Renamed, types[i]);

				if (!OPERATOR_MAP.ContainsKey(key))
				{
					OPERATOR_MAP[key] = this;
				}
			}
		}



		#region Properties

		public virtual Operator Operator
		{
			get
			{
				return _Renamed;
			}
		}

		#endregion Properties



		public static OperatorCodec GetCodec(Operator operator_Renamed, FASTType type)
		{
			var key = new Key(operator_Renamed, type);

			if (!OPERATOR_MAP.ContainsKey(key))
			{
				Global.HandleError(Error.FastConstants.S2_OPERATOR_TYPE_INCOMP, "The operator \"" + operator_Renamed + "\" is not compatible with type \"" + type + "\"");
				throw new ArgumentException();
			}

			return OPERATOR_MAP[key];
		}

		public virtual bool CanEncode(ScalarValue value_Renamed, Scalar field)
		{
			return true;
		}

		public abstract ScalarValue DecodeEmptyValue(ScalarValue previousValue, Scalar field);

		public abstract ScalarValue DecodeValue(ScalarValue newValue, ScalarValue priorValue, Scalar field);

		//POINTP
		public override bool Equals(object obj)
		{
			return obj != null && obj.GetType() == GetType();
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		public abstract ScalarValue GetValueToEncode(ScalarValue value_Renamed, ScalarValue priorValue, Scalar field);

		public virtual ScalarValue GetValueToEncode(ScalarValue value_Renamed, ScalarValue priorValue, Scalar scalar, BitVectorBuilder presenceMapBuilder)
		{
			var valueToEncode = GetValueToEncode(value_Renamed, priorValue, scalar);
			if (valueToEncode == null)
				presenceMapBuilder.Skip();
			else
				presenceMapBuilder.set_Renamed();
			return valueToEncode;
		}

		public virtual bool IsPresenceMapBitSet(byte[] encoding, FieldValue fieldValue)
		{
			return encoding.Length != 0;
		}

		public virtual bool ShouldDecodeType()
		{
			return true;
		}

		public override string ToString()
		{
			return _Renamed.ToString();
		}

		public virtual bool UsesPresenceMapBit(bool optional)
		{
			return true;
		}


	}
}