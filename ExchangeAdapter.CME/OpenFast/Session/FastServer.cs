#region Header

/*

The contents of this file are subject to the Mozilla Public License
Version 1.1 (the "License"); you may not use this file except in
compliance with the License. You may obtain a copy of the License at
http://www.mozilla.org/MPL/

Software distributed under the License is distributed on an "AS IS"
basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
License for the specific language governing rights and limitations
under the License.

The Original Code is OpenFAST.

The Initial Developer of the Original Code is The LaSalle Technology
Group, LLC.  Portions created by Shariq Muhammad
are Copyright (C) Shariq Muhammad. All Rights Reserved.

Contributor(s): Shariq Muhammad <shariq.muhammad@gmail.com>

*/

#endregion Header

namespace OpenFAST.Session
{
	using ErrorHandler = OpenFAST.Error.ErrorHandler;

	public sealed class FastServer : ConnectionListener
	{


		private readonly Endpoint endpoint;
		private readonly string serverName;
		private readonly SessionProtocol sessionProtocol;

		private ErrorHandler errorHandler = Error.ErrorHandler_Fields.DEFAULT;
		private bool listening;
		private SupportClass.ThreadClass serverThread;
		private SessionHandler sessionHandler = SessionHandler_Fields.NULL;





		public FastServer(string serverName, SessionProtocol sessionProtocol, Endpoint endpoint)
		{
			if (endpoint == null || sessionProtocol == null)
			{
				throw new System.NullReferenceException();
			}
			this.endpoint = endpoint;
			this.sessionProtocol = sessionProtocol;
			this.serverName = serverName;
			endpoint.ConnectionListener = this;
		}



		#region Properties

		public ErrorHandler ErrorHandler
		{
			// ************* OPTIONAL DEPENDENCY SETTERS **************
			set
			{
				if (value == null)
				{
					throw new System.NullReferenceException();
				}

				errorHandler = value;
			}
		}

		public SessionHandler SessionHandler
		{
			set
			{
				sessionHandler = value;
			}
		}

		#endregion Properties



		public void Close()
		{
			listening = false;
			endpoint.Close();
		}

		public void Listen()
		{
			listening = true;
			if (serverThread == null)
			{
				IThreadRunnable runnable = new FastServerThread(this);
				serverThread = new SupportClass.ThreadClass(new System.Threading.ThreadStart(runnable.Run), "FastServer");
			}
			serverThread.Start();
		}

		public override void OnConnect(Connection connection)
		{
			Session session = sessionProtocol.OnNewConnection(serverName, connection);
			sessionHandler.NewSession(session);
		}



		#region Nested Types

		private class FastServerThread : IThreadRunnable
		{


			private FastServer enclosingInstance;





			public FastServerThread(FastServer enclosingInstance)
			{
				InitBlock(enclosingInstance);
			}



			#region Properties

			public FastServer Enclosing_Instance
			{
				get
				{
					return enclosingInstance;
				}
			}

			#endregion Properties



			public virtual void Run()
			{
				while (Enclosing_Instance.listening)
				{
					try
					{
						Enclosing_Instance.endpoint.Accept();
					}
					catch (FastConnectionException e)
					{
						Enclosing_Instance.errorHandler.Error(null, null, e);
					}
					try
					{
						System.Threading.Thread.Sleep(new System.TimeSpan((System.Int64)10000 * 20));
					}
					catch (System.Threading.ThreadInterruptedException)
					{
					}
				}
			}

			private void InitBlock(FastServer internalInstance)
			{
				enclosingInstance = internalInstance;
			}


		}

		#endregion Nested Types
	}
}