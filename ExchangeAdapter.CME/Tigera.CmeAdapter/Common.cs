﻿namespace CmeAdapter
{
    using RS.CmeFastHandler;
    using System;

    /// <summary>
    /// 参数组合，为了在在多线程程序中传递参数
    /// </summary>
    public struct Param
    {


        public string key;
        public OpenFAST.Message msg;


    }

    /// <summary>
    /// Quickfix42 object argument
    /// </summary>
    public class SnapShotEventArgs : EventArgs
    {


        private string channelid;
        private QuickFix42.MarketDataSnapshotFullRefresh msg;





        public SnapShotEventArgs()
        {
        }



        #region Properties

        public string ChannelID
        {
            get
            {
                return this.channelid;
            }
            set
            {
                this.channelid = value;
            }
        }

        public QuickFix42.MarketDataSnapshotFullRefresh Message
        {
            get { return msg; }
            set { msg = value; }
        }

        #endregion Properties
    }

    /// <summary>
    /// SnapshotEntity object argument
    /// </summary>
    public class SnapShotNewEventArgs : EventArgs
    {


        private string channelid;
        private MarketData data;





        public SnapShotNewEventArgs()
        {
        }



        #region Properties

        public string ChannelID
        {
            get
            {
                return this.channelid;
            }
            set
            {
                this.channelid = value;
            }
        }

        public MarketData Data
        {
            get { return data; }
            set { data = value; }
        }

        #endregion Properties
    }
}