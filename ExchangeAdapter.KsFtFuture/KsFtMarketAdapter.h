#ifndef __KSFT_MARKET_ADAPTER_H__
#define __KSFT_MARKET_ADAPTER_H__

#include "KsFtCommon.h"
#include "IFixMarket.h"
#include "RMMSThread.h"
#include "RMMSQueue.h"

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4275)
#else
#error "不应该出现,以待其他"
#endif

KSFT_NS_BEGIN

class KsFtMarketAdapter;

class QuoteWorker : public LIB_NS::RMMSThread {
public:
	virtual int32 Worker();
public:
	KsFtMarketAdapter *m_padapter;
};

class SendWorker : public LIB_NS::RMMSThread {
public:
	virtual int32 Worker();
public:
	KsFtMarketAdapter *m_padapter;
};

class KsFtMarketAdapter : public IFixMarketProvider {
public:
	KsFtMarketAdapter();
	virtual ~KsFtMarketAdapter();

public:
	struct InitParam
	{
		int32 iport;
		int32 itimeout;
		bool blog;
		tstring slogfilename;
		tstring sdatasrc;
		int32 ilines;
		bool bsend;
		bool blastvolume;	/* 该参数国内没有携带,所以暂时这样控制 */
		uint32 uilastvolume;	/* 该参数国内没有携带,所以暂时这样控制 */
		EXAPI static std::pair<bool,InitParam> LoadConfig(
			const tstring& fname);
	};

	tstring GetName();
	bool InitializeByXML(const tchar* cfgpath);
	bool Initialize(void *para);
	void UnInitialize();
	bool Login(void*);
	bool Logout(void*);
	bool SupportDoM();
	void Response(FIX::Message &msg);

	virtual void AgentOnNetConnected();
	virtual void AgenOnNetDisconnected();
	virtual void AgenOnLogin();
	virtual void AgenOnLogout();

public:
	int32 Work();
	int32 Send(const FIX::Message &msg);
	int32 OnSend();
	int32 WorkKing();
	int32 WorkLocal();

private:
	bool TransQuote2Fix(KSFT_QUOTA_PUBDATA_ITEM &item,FIX::Message *pmsg);
	void WriteLogFile(KSFT_QUOTA_PUBDATA_ITEM &item);
	void ReadLogFile();
	int32 GetBuffer(tchar *tmpbuffer,tchar **pindex);
	void GetVar(tchar **pindex,int32 *idata);
	void GetVar(tchar **pindex,double *idata);
	void GetVar(tchar **pindex,tchar *idata);

private:
	InitParam m_para;
	bool m_bworker;
	QuoteWorker m_worker;
	SendWorker m_sender;
	bool m_working;
	bool m_sending;

	LIB_NS::Queue<FIX::Message> m_sendqueue;
};

KSFT_NS_END

#ifdef _MSC_VER
#pragma warning(pop)
#else
#error "仅支持VC,以待修改"
#endif

#endif