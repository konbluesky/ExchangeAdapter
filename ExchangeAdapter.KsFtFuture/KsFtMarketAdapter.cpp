#include "ExchangeAdapterConfig.h"
#include "KsFtMarketAdapter.h"
#define TIXML_USE_STL
#include "tinyxml.h"
#include "DebugUtils.h"
#include <fstream>
#include <cmath>
#include "LocalCompile.h"

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4100)
#pragma warning(disable:4512)
#pragma warning(disable:4125)
#else
#error "仅支持VC,以待修改"
#endif

#include "quickfix/fix42/MarketDataSnapshotFullRefresh.h"

#ifdef _MSC_VER
#pragma warning(pop)
#else
#error "仅支持VC,以待修改"
#endif

#include "LocalCompile.h"

KSFT_NS_BEGIN

int32 QuoteWorker::Worker()
{
	return m_padapter->Work();
}

int32 SendWorker::Worker()
{
	return m_padapter->OnSend();
}

std::pair<bool,KsFtMarketAdapter::InitParam> 
	KsFtMarketAdapter::InitParam::LoadConfig(const tstring& fname)
{
	InitParam param;
	TiXmlDocument doc(fname.c_str());
	bool loadOkay = doc.LoadFile();
	TiXmlNode* pNode = NULL;
	tstring str;

	std::memset(&param,0,sizeof(param));
	if (!loadOkay)
		return std::make_pair(false,param);

	pNode = doc.FirstChild("configure");
	XML_GET_INT(pNode,"iport",param.iport);
	XML_GET_INT(pNode,"itimeout",param.itimeout);
	XML_GET_BOOL(pNode,"blog",param.blog);
	XML_GET_STRING(pNode,"slogfilename",param.slogfilename);
	XML_GET_STRING(pNode,"sdatasrc",param.sdatasrc);
	XML_GET_INT(pNode,"ilines",param.ilines);
	XML_GET_BOOL(pNode,"bsend",param.bsend);
	XML_GET_BOOL(pNode,"blastvolume",param.blastvolume);
	XML_GET_UINT(pNode,"uilastvolume",param.uilastvolume);

	return std::make_pair(true,param);
}

KsFtMarketAdapter::KsFtMarketAdapter():m_bworker(false),m_working(false),
	m_sending(false)
{
	std::memset(&m_para,0,sizeof(m_para));
}

KsFtMarketAdapter::~KsFtMarketAdapter()
{
}

tstring KsFtMarketAdapter::GetName()
{
	return "KSFTMarket";
}

bool KsFtMarketAdapter::InitializeByXML(const tchar* cfgpath)
{
	std::pair<bool,InitParam> it = 
		InitParam::LoadConfig(cfgpath);
	if(false == it.first)
		return false;
	return Initialize((void*)(&(it.second)));
}

bool KsFtMarketAdapter::Initialize(void *para)
{
	m_para = *((InitParam*)para);

	/* 先初始化队列 */
	m_sendqueue.Init();

	m_worker.m_padapter = this;
	m_sender.m_padapter = this;

	m_sending = true;
	m_working = true;

	if(0 > m_sender.Init())
		return false;
	if(0 > m_worker.Init()){
		m_sender.Uninit();
		return false;
	}
	return true;
}

void KsFtMarketAdapter::UnInitialize()
{
	m_working = false;
	m_worker.Uninit();
	m_sending = false;
	m_sender.Uninit();

	/* 释放队列 */
	m_sendqueue.Uninit();
}

bool KsFtMarketAdapter::Login(void*)
{
	uint16 port = (uint16)(m_para.iport & 0x0000ffff);
	tchar errrmsg[256] = {'0'};
	m_bworker = KSFTHQPUB_Start(port,errrmsg);

	if (!m_bworker){
		DBG_TRACE(("%s\n",errrmsg));
		return false;
	}

	if(0 > m_sender.Start())
		KSFTHQPUB_Stop();
	if(0 > m_worker.Start()){
		KSFTHQPUB_Stop();
		m_sender.Stop();
	}

	return true;
}

bool KsFtMarketAdapter::Logout(void*)
{
	m_worker.Stop();
	m_sender.Stop();
	KSFTHQPUB_Stop();	
	return false;
}

bool KsFtMarketAdapter::SupportDoM()
{
	return false;
}

void KsFtMarketAdapter::AgentOnNetConnected(){}

void KsFtMarketAdapter::AgenOnNetDisconnected(){}

void KsFtMarketAdapter::AgenOnLogin(){PriOnLogin();}

void KsFtMarketAdapter::AgenOnLogout(){PriOnLogout();}

int32 KsFtMarketAdapter::Work()
{
	if ("king" == m_para.sdatasrc)
		return WorkKing();
	else if("local" == m_para.sdatasrc) 
		return WorkLocal();
	else
		return WorkKing();
}

int32 KsFtMarketAdapter::OnSend()
{
	FIX::Message msg;
	int32 ret;
	while(m_sending){
		if(!m_sendqueue.Pop(msg)){
			ret = m_sendqueue.Wait(2000);
			if(0 > ret){
				DBG_ASSERT(false);
				LIB_NS::RMMSThread::Sleep(2000);
			}
		}else{
			OnResponseMsg(msg);
			msg.clear();
		}
	}

	int32 isize = m_sendqueue.Size();
	while(0 < isize){
		if(m_sendqueue.Pop(msg))
			OnResponseMsg(msg);
		isize--;
	}

	return 0;
}

int32 KsFtMarketAdapter::Send(const FIX::Message &msg)
{
	try	{
		m_sendqueue.Push(msg);
	}catch (...){
		std::cout<<"Buffer size is too big.Current size is "<<m_sendqueue.Size()
			<<std::endl;
		bool bflag = 1;
		while(bflag)
			LIB_NS::RMMSThread::Sleep(10000);
		return -1;
	}
	return 0;
}

int32 KsFtMarketAdapter::WorkKing()
{
	int32 quotaCount = 0;
	const int32 MAX_QUOTA_ITEM_COUNT = 50;
	KSFT_QUOTA_PUBDATA_ITEM quotaData[MAX_QUOTA_ITEM_COUNT];
	tchar errorMsg[256] = {'0'};
	FIX::Message msg;

	while(m_working){
		quotaCount = KSFTHQPUB_GetQuota((uint8*)quotaData,
			sizeof(KSFT_QUOTA_PUBDATA_ITEM) * MAX_QUOTA_ITEM_COUNT,
			m_para.itimeout,errorMsg);
		if (0 > quotaCount)
			DBG_TRACE(("%s",errorMsg));
		else if(0 == quotaCount)
			DBG_TRACE(("没有行情数据"));
		else{
			while (quotaCount-- > 0){
				msg.clear();
				if (m_para.blog)
					WriteLogFile(quotaData[quotaCount]);
				if(TransQuote2Fix(quotaData[quotaCount],&msg)){
					DBG_TRACE(("收到包了"));
					/* 仅为方便测试,以便了解接收包的速度 */
					if(m_para.bsend)
						Send(msg);
				}
			}
		}
	}

	return 0;
}

int32 KsFtMarketAdapter::WorkLocal()
{
	while(m_working)
		ReadLogFile();
	
	return 0;
}

static void ExchangeMonthToFix(tstring &cmonth,tchar *pmon)
{
	if("01" == cmonth)
		*pmon = 'F';
	else if("02" == cmonth)
		*pmon = 'G';
	else if("03" == cmonth)
		*pmon = 'H';
	else if("04" == cmonth)
		*pmon = 'J';
	else if("05" == cmonth) 
		*pmon = 'K';
	else if("06" == cmonth) 
		*pmon = 'M';
	else if("07" == cmonth)
		*pmon = 'N';
	else if("08" == cmonth)
		*pmon = 'Q';
	else if("09" == cmonth)
		*pmon = 'U';
	else if("10" == cmonth)
		*pmon = 'V';
	else if("11" == cmonth) 
		*pmon = 'X';
	else
		*pmon = 'Z';
}

static void GetSecurityID(const tchar *psymbol,const tchar *pdeliver,
	tstring *psecurityid)
{
	int8 iYear = pdeliver[1];
	int8 iMonth = 0;

	*psecurityid = pdeliver + 2;
	ExchangeMonthToFix(*psecurityid,&iMonth);
	*psecurityid = psymbol;
	*psecurityid += iMonth;
	*psecurityid += iYear;
}

static bool GetSecurityExch(const tchar exchcode,tstring *psecurityexch)
{
	switch(exchcode){
	case 'B':	*psecurityexch = "SHFE";break;
	case 'G':	*psecurityexch = "CFFEX";break;
	case 'C':	*psecurityexch = "ZCE";break;
	case 'D':	*psecurityexch = "DCE";break;
	default:
		return false;
	}
	return true;
}

bool KsFtMarketAdapter::TransQuote2Fix(KSFT_QUOTA_PUBDATA_ITEM &item,
	FIX::Message *pmsg)
{
	tstring tmp;
	
	if('1' == item.cmbtype[0])
		return false;
	if(!GetSecurityExch(item.exchCode[0],&tmp))
		return false;

	FIX::Header &header = pmsg->getHeader();
	header.setField(FIX::MsgType(FIX::MsgType_MarketDataSnapshotFullRefresh));

	/* 交易所 */
	pmsg->setField(FIX::SecurityExchange(tmp));
	/* 产品ID */
	pmsg->setField(FIX::FIELD::Symbol,item.varity_code);
	/* 商品类型 */
	pmsg->setField(FIX::FIELD::SecurityType,"FUT");
	/* 合约ID */
	GetSecurityID(item.varity_code,item.deliv_date,&tmp);
	pmsg->setField(FIX::FIELD::SecurityID,tmp);
	/* 请求ID */
	pmsg->setField(FIX::FIELD::MDReqID,"0");
	/* 成交量 */
	pmsg->setField(FIX::TotalVolumeTraded(std::abs(item.doneVolume)));
	
	/* 报价组 */
	FIX42::MarketDataSnapshotFullRefresh::NoMDEntries group;
	group.clear();
	group.set(FIX::MDEntryType('1'));
	group.set(FIX::MDEntryPx(std::abs(item.askPrice1)));
	group.set(FIX::MDEntrySize(std::abs(item.askVolume1)));
	group.setField(FIX::MDEntryPositionNo(1));
	pmsg->addGroup(group);
	
	group.clear();
	group.set(FIX::MDEntryType('0'));
	group.set(FIX::MDEntryPx(std::abs(item.bidPrice1)));
	group.set(FIX::MDEntrySize(std::abs(item.bidVolume1)));
	group.setField(FIX::MDEntryPositionNo(1));
	pmsg->addGroup(group);
	
	/* 最近的价格 */
	group.clear();
	group.set(FIX::MDEntryType('2'));
	group.set(FIX::MDEntryPx(std::abs(item.lastPrice)));
	/**
	 * !!! 特别注意,该参数国内没有携带,愿意是不带.但是Glotrader则是要求携带的,
	 * 所以,这里暂时填上该参数.并且Ming确定该参数是不会出现0的情况的.所以这里
	 * 填上该参数,能够区分所有的情况了.
	 */
	if(m_para.blastvolume)
		group.set(FIX::MDEntrySize(m_para.uilastvolume));
	pmsg->addGroup(group);
	
	group.clear();
	group.set(FIX::MDEntryType('4'));
	group.set(FIX::MDEntryPx(std::abs(item.openPrice)));
	pmsg->addGroup(group);
	
	group.clear();
	group.set(FIX::MDEntryType('6'));
	group.set(FIX::MDEntryPx(std::abs(item.preSettlePrice)));
	pmsg->addGroup(group);
	
	group.clear();
	group.set(FIX::MDEntryType('7'));
	group.set(FIX::MDEntryPx(std::abs(item.highestPrice)));
	pmsg->addGroup(group);
	
	group.clear();
	group.set(FIX::MDEntryType('8'));
	group.set(FIX::MDEntryPx(std::abs(item.lowestPrice)));
	pmsg->addGroup(group);

	return true;
}

static bool GetSecurityExch(const tchar exchcode)
{
	switch(exchcode){
	case 'B':
	case 'G':
	case 'C':
	case 'D':
		return true;
	default:
		return false;
	}
}

void KsFtMarketAdapter::WriteLogFile(KSFT_QUOTA_PUBDATA_ITEM &item)
{
	if(!GetSecurityExch(item.exchCode[0]))
		return;

	std::ofstream file;
	file.open(m_para.slogfilename,std::ios::app);
	if(file.fail())
		return;
	
	file<<item.contract_id<<","
		<<item.upd_serial<<","
		<<item.upd_date<<","
		<<item.pre_upd_date<<","
		<<item.pre_upd_serial<<","
		<<item.sys_recv_time<<","
		<<item.exchCode<<","
		<<item.varity_code<<","
		<<item.deliv_date<<","
		<<item.chgStatus<<","
		<<item.openPrice<<","
		<<item.lastPrice<<","
		<<item.highestPrice<<","
		<<item.lowestPrice<<","
		<<item.doneVolume<<","
		<<item.chgPrice<<","
		<<item.upperLimitPrice<<","
		<<item.lowerLimitPrice<<","
		<<item.hisHighestPrice<<","
		<<item.hisLowestPrice<<","
		<<item.openInterest<<","
		<<item.preSettlePrice<<","
		<<item.preClosePrice<<","
		<<item.settlePrice<<","
		<<item.turnover<<","
		<<item.preOpenInterest<<","
		<<item.closePrice<<","
		<<item.preDelta<<","
		<<item.currDelta<<","
		<<item.bidPrice1<<","
		<<item.bidVolume1<<","
		<<item.askPrice1<<","
		<<item.askVolume1<<","
		<<item.cmbtype<<","
		<<item.derive_bidlot<<","
		<<item.derive_asklot
		<<std::endl;

	file.close();
}

int32 KsFtMarketAdapter::GetBuffer(tchar *tmpbuffer,tchar **pindex)
{
	tchar *pnewindex = tmpbuffer;
	bool bflag = true;
	do {
		*pnewindex = **pindex;
		if(',' == *pnewindex)
			*pnewindex = '\0';
		if('\0' == *pnewindex)
			break;
		pnewindex++;
		(*pindex)++;
	} while (bflag);
	(*pindex)++;
	return 0;
}

void KsFtMarketAdapter::GetVar(tchar **pindex,int32 *idata)
{
	tchar tmpbuffer[64] = {'\0'};
	GetBuffer(tmpbuffer,pindex);
	std::stringstream ss;
	ss<<tmpbuffer;
	ss>>*idata;
}

void KsFtMarketAdapter::GetVar(tchar **pindex,double *idata)
{
	tchar tmpbuffer[64] = {'\0'};
	GetBuffer(tmpbuffer,pindex);
	std::stringstream ss;
	ss<<tmpbuffer;
	ss>>*idata;
}

void KsFtMarketAdapter::GetVar(tchar **pindex,tchar *idata)
{
	tstring str;
	char tmpbuffer[64] = {'\0'};
	GetBuffer(tmpbuffer,pindex);
	std::stringstream ss;
	ss<<tmpbuffer;
	ss>>str;
	std::strcpy(idata,str.c_str());
}

void KsFtMarketAdapter::ReadLogFile()
{
	std::FILE *file;
	file = std::fopen(m_para.slogfilename.c_str(),"r");
	if (NULL == file)
		DBG_ASSERT(false);
	char *pindex = NULL;
	char buffer[512];
	KSFT_QUOTA_PUBDATA_ITEM item;
	FIX::Message msg;
	
	for (int32 index = 0;index < m_para.ilines && m_working;index++)
	{
		std::fgets(buffer,512,file);
		pindex = buffer;

		GetVar(&pindex,&(item.contract_id));
		GetVar(&pindex,&(item.upd_serial));
		GetVar(&pindex,&(item.upd_date));
		GetVar(&pindex,&(item.pre_upd_date));
		GetVar(&pindex,&(item.pre_upd_serial));
		GetVar(&pindex,item.sys_recv_time);
		GetVar(&pindex,item.exchCode);
		GetVar(&pindex,item.varity_code);
		GetVar(&pindex,item.deliv_date);
		GetVar(&pindex,item.chgStatus);
		GetVar(&pindex,&(item.openPrice));
		GetVar(&pindex,&(item.lastPrice));
		GetVar(&pindex,&(item.highestPrice));
		GetVar(&pindex,&(item.lowestPrice));
		GetVar(&pindex,&(item.doneVolume));
		GetVar(&pindex,&(item.chgPrice));
		GetVar(&pindex,&(item.upperLimitPrice));
		GetVar(&pindex,&(item.lowerLimitPrice));
		GetVar(&pindex,&(item.hisHighestPrice));
		GetVar(&pindex,&(item.hisLowestPrice));
		GetVar(&pindex,&(item.openInterest));
		GetVar(&pindex,&(item.preSettlePrice));
		GetVar(&pindex,&(item.preClosePrice));
		GetVar(&pindex,&(item.settlePrice));
		GetVar(&pindex,&(item.turnover));
		GetVar(&pindex,&(item.preOpenInterest));
		GetVar(&pindex,&(item.closePrice));
		GetVar(&pindex,&(item.preDelta));
		GetVar(&pindex,&(item.currDelta));
		GetVar(&pindex,&(item.bidPrice1));
		GetVar(&pindex,&(item.bidVolume1));
		GetVar(&pindex,&(item.askPrice1));
		GetVar(&pindex,&(item.askVolume1));
		GetVar(&pindex,item.cmbtype);
		GetVar(&pindex,&(item.derive_bidlot));
		GetVar(&pindex,&(item.derive_asklot));
		msg.clear();
		if(TransQuote2Fix(item,&msg))
			OnResponseMsg(msg);

		LIB_NS::RMMSThread::Sleep(m_para.itimeout);
	}
		
	std::fclose(file);
}

KSFT_NS_END