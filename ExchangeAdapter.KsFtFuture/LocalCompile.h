#ifndef __KSFT_LOCAL_COMPILE_H__
#define __KSFT_LOCAL_COMPILE_H__

#if defined(_MSC_VER)
#pragma warning(disable:4996)

/**
 * Release下存在的告警,因为DBG_TRACE不存在
 */
#if defined(NDEBUG)
#pragma warning(disable:4390)
#endif

#else
#error "仅支持VC,待修改"
#endif

#define _XML_GET_VALUE(node,nodename)	\
	node->FirstChild(nodename)->FirstChild()->Value()

#define _STR_TO_LOWER(x)	\
	std::transform(x.begin(),x.end(),x.begin(),std::tolower);

#define XML_GET_STRING(node,nodename,nodevalue)	\
	nodevalue = _XML_GET_VALUE(node,nodename);

#define XML_GET_INT(node,nodename,nodevalue)	\
	std::sscanf(_XML_GET_VALUE(node,nodename),"%d",&nodevalue);

#define XML_GET_UINT(node,nodename,nodevalue)	\
	std::sscanf(_XML_GET_VALUE(node,nodename),"%u",&nodevalue);

#define XML_GET_BOOL(node,nodename,nodevalue)	\
{											\
	tstring str = _XML_GET_VALUE(node,nodename);	\
	_STR_TO_LOWER(str);						\
	nodevalue = "true" == str ? true:false;	\
}

#endif