#ifndef __KSFT_COMMON_H__
#define __KSFT_COMMON_H__

#include "ExchangeAdapterBase.h"

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4275)
#pragma warning(disable:4121)
#else
#error "仅支持VC,以待其他"
#endif

#ifndef WINAPI
#define WINAPI __stdcall
#endif
#include "KsFtQtPub.h"

#ifdef _MSC_VER
#pragma warning(pop)
#else
#error "仅支持VC,以待修改"
#endif


#define KSFT_NS ExchangeAdapter_KsFt
#define KSFT_NS_BEGIN namespace KSFT_NS {
#define KSFT_NS_END	}
#define	USING_KSFT_NS	using namespace NANHUA_NS;

#endif