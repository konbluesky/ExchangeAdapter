#ifndef __I_ORDER_SERVICE_H__
#define __I_ORDER_SERVICE_H__

#include <algorithm>
#include <iostream>
#include <tchar.h>
#include <string>
#include <vector>
#include <list>

#include "ErrorDefinition.h"
#include "TradeDefinition.h"
#include "ExchangeAdapterDef.h"


namespace tradeadapter
{

	class ITradeAdapter;




	//add a Client ID to support multi clients 
	class  IOrderService
	{
	public:
		//这个用单实例的模式，主要方便开发使用，但单实例的生命周期不好控制，所以下面增加类工厂方式
		//这个客户负责释放，基于new方式创建
		static TAIAPI  IOrderService* CreateInstance(const TCHAR* ID);

		//计算头寸总成本
		static  TAIAPI double CalculatePosCostValue(std::list<PositionInfo>* pPoss);
		//计算头寸总市值
		static  TAIAPI double CalcuatePosMarketValue(std::list<PositionInfo>* pPoss);


		virtual bool	Initialize(void* p) = 0;

		virtual bool    InitializeByXML(const TCHAR* pPath) = 0;
		
		virtual bool	IsInitialized() = 0;	
		
		virtual bool  EntrustOrder(const TCHAR* clientID, const  Order* order, OrderID* pOutID) = 0;
		
		virtual bool	CancelOrder(const TCHAR* clinetID, OrderID id) = 0;
		
		virtual int		QueryOrder(const TCHAR* clientID, OrderID id)=0;


		// 查询用户持仓
		virtual bool QueryPostions(std::list<PositionInfo>* pPoss,const TCHAR* pstock = NULL)=0;

		virtual bool GetLastError(ErrorInfo* perr) = 0;
		
		virtual void	UnInitialize() = 0;

		virtual ITradeAdapter* GetAdapter()=0;

		//static ORDERSERVICEAPI  bool SetInstanceConfig(const char* ID, const char* branchid, const char* clientid, const char* password, const char* tempate="default");


	};

	

}
#endif