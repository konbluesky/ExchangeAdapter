#include "stdafx.h"
#include "IOrderService.h"
#include "ITradeAdapter.h"

USING_LIBSPACE
//注：对于其他语言使用的字符集，应该以anscii为首选,这样其他各种语言的调用就相对非常的简单

#define  EXPORT_CAPI extern "C" __declspec(dllexport) 



EXPORT_CAPI bool TradeInitialize(const char* pfname);
EXPORT_CAPI int EntrustOrder(const TCHAR* id,tradeadapter::Order order);
EXPORT_CAPI int QueryOrder(const TCHAR* usrid,tradeadapter::OrderID id);
EXPORT_CAPI bool CancelOrder(const TCHAR* usrid,tradeadapter::OrderID id);