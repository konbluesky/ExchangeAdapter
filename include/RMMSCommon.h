#ifndef __RMMS_COMMON_H__
#define __RMMS_COMMON_H__

/* 通用定义文件 */


/* RMMS域 */
#include "RMMSNamespace.h"

RMMS_NS_BEGIN

enum Exchange{
	RMMS_CN_DL = 0,
	RMMS_CN_SH,
	RMMS_CN_ZJ,
	RMMS_CN_ZZ,
	RMMS_CN_KSFT,
	RMMS_CN_RMMS,
	RMMS_US_CME,
	RMMS_EXCH_SUM,
	RMMS_EXCH_UNKNOWN,
};

enum RMMSFixVer
{
	RMMS_FIX42 = 0,

	RMMS_VER_UNKNOWN,	
};

enum FuncType {
	TRADER = 0,
	MARKET,
};

enum InitType{
	XML = 0,
	PARM
};

RMMS_NS_END

#endif