#ifndef __IRMMS_EXCH_ADM_H__
#define __IRMMS_EXCH_ADM_H__
#include <string>
#include <tchar.h>
class IRMMSExchAdmRsp {
public:
	IRMMSExchAdmRsp(){}
	virtual ~IRMMSExchAdmRsp(){}
	
public:
	virtual void OnNetConnected() = 0;
	virtual void OnNetDisconnected() = 0;
	virtual void OnLogin() = 0;
	virtual void OnLogout() = 0;
};

class IRMMSExchAdmReq{
public:
	IRMMSExchAdmReq(){}
	virtual ~IRMMSExchAdmReq(){}
	
public:
	virtual std::string GetName()=0;
	virtual bool InitializeByXML(const TCHAR* )=0;
	virtual bool Initialize(void* )=0;
	virtual void UnInitialize()=0;
	virtual bool Login(void*) = 0;
	virtual bool Logout(void*) = 0;
		
public:
	virtual void PriOnNetConnected()  = 0;
	virtual void PriOnNetDisconnected() = 0;
	virtual void PriOnLogin() = 0;
	virtual void PriOnLogout() = 0;
};

#endif