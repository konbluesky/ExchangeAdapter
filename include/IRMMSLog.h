#ifndef __IRMMS_LOG_H__
#define __IRMMS_LOG_H__

#include "RMMSCommon.h"

RMMS_NS_BEGIN

typedef enum RMMSLogLevel
{
	RMMS_LOG_NULL = 0,
	RMMS_LOG_LEVEL1 = 0x1UL,
	RMMS_LOG_LEVEL2 = 0x2UL,
}RMMSLogLevel;

class IRMMSLog
{
public:
	virtual bool SetDebugLevel(RMMSLogLevel level) = 0;
	virtual int32 Log(RMMSLogLevel tmplevel,const tchar* buf,...) = 0;
};

RMMS_NS_END

#endif