#ifndef __CHINA_QUOTE_DEFINITION_H__
#define __CHINA_QUOTE_DEFINITION_H__

#include <string>
#include <time.h>
#include <iostream>
#include "DateTime.h"

typedef std::string	QuoteID;

//注：int的
struct Quote
{
	std::string ID;
	/// <summary>
	/// 当前价格。
	/// </summary>
	double Last_Price;
	/// <summary>
	/// 当前成交。
	/// </summary>
	int Last_Volume;
	/// <summary>
	/// 买入价。
	/// </summary>
	 double BidPrice;
	/// <summary>
	/// 买量。
	/// </summary>
	 int BidQty;
	/// <summary>
	/// 卖出价。
	/// </summary>
	 double AskPrice;
	/// <summary>
	/// 卖量。
	/// </summary>
	 int AskQty;
	/// <summary>
	/// 当天最低价格。
	/// </summary>
	 double LowPrice;
	/// <summary>
	/// 当天最高价格
	/// </summary>
	 double HighPrice;
	/// <summary>
	/// 总成交量
	/// </summary>
	 int Total_Volume;
	/// <summary>
	/// 开盘价格
	/// </summary>
	 double Open_Price;

	//TODO,结算价，这个还没有在服务器实现
	double SettlementPrice;

	/// <summary>
	/// 注:这个时间不一定准确，只是我们接收到传过来时的报价时间
	/// </summary>
	 //struct tm RecieveTime;
	LIB_NS::DateTime RecieveTime;
	//DateTime
	//debug使用
	static int _Count;

	
	std::string ToString() const;
};
std::ostream& operator<<( std::ostream& os,const Quote& quote );
/// <summary>
/// 深度市场数据。
/// 例：BidLevel中存放如下数据-1,-2,-3,-5,-6,0,0,0,0,0，则表示共有五档有效买价数据，数值表示离当前价有的ticksize,
/// 如-2表示小于当前价并离当前价格有2个Ticksize。BidQuantities中存储的分别是对应档位的数量。
/// 对Ask的两个数组也是同样意义，不过AskLevel中存放的可能都是正整数，因为AskPrice一般大于当前价。
/// </summary>

struct DoM
{
	tstring ID;	
	double CurrentPrice;
	int LastQuantity;
	int SupportLevel;
	/// <summary>
	/// 买价距离当前价格有几个ticksize。
	/// </summary>
	double BidLevel[10];
	int BidQuantities[10];
	/// <summary>
	/// 卖价距离当前价格有几个Ticksize。
	/// </summary>
	double AskLevel[10];
	int AskQuantities[10];

	 double LowPrice;
	/// <summary>
	/// 当天最高价格
	/// </summary>
	 double HighPrice;
	/// <summary>
	/// 总成交量
	/// </summary>
	 int Total_Volume;
	/// <summary>
	/// 开盘价格
	/// </summary>
	 double Open_Price;

	//TODO,结算价，这个还没有在服务器实现
	double SettlementPrice;

	LIB_NS::DateTime RecieveTime;
public:
	std::string ToString();
};

//注：这个为图省事,以后想想具体如何实现。
inline bool Quote2Dom(const Quote& quote,DoM* pDom)
{
	pDom->ID = quote.ID;
	pDom->CurrentPrice = quote.Last_Price;
	pDom->LastQuantity = quote.Last_Volume;
	pDom->SupportLevel = 1;
	pDom->BidLevel[0] = quote.BidPrice;
	pDom->BidQuantities[0] = quote.BidQty;
	pDom->AskLevel[0] = quote.AskPrice;
	pDom->AskQuantities[0] = quote.AskQty;
	pDom->RecieveTime = quote.RecieveTime;

	//Add:zhanggq;date:2010-09-26;content:
	pDom->Open_Price = quote.Open_Price;
	pDom->LowPrice = quote.LowPrice;
	pDom->HighPrice = quote.HighPrice;
	pDom->Total_Volume = quote.Total_Volume;
	return true;
}
std::ostream& operator<<( std::ostream& os,const DoM& dom );

#endif