#ifndef __I_TRADER_ADAPTER_H__
#define __I_TRADER_ADAPTER_H__
#include "ErrorDefinition.h"
#include "IErrorInfoLog.h"
#include "IConfigSetting.h"
#include "IOrderService.h"

#include <list>
namespace libutils
{
	class  IConfigSetting;
};
namespace tradeadapter
{
	//对于交易接口等的，不要把初始化、结束等的工作放在构造、析构函数里实现
	//因为应用可能不清楚什么时候释放对象，而放在构造函数里，构造失败了就对象都创建不成功
	//则，所有的状态都不清楚
	class ITradeAdapter : public IErrorInfoLog, public ::libutils::IConfigSetting
	{
	public:
//#pragma chMSG(later would change void* param to tdataset)
		virtual bool Initialize(void* param = NULL) = 0;

		///convienent for load config from xml file
		virtual bool InitializeByXML(const TCHAR* fpath) = 0;
		

		virtual bool IsInitialized() = 0;	


		virtual void UnInitialize() = 0;

	public:
		///这里不用传引用，让别的语言方便使用这个接口，如果委托不成功，返回false,同时pOutID = -1
		virtual bool EntrustOrder(Order order, OrderID* pOutID) = 0;

		///查询委托单成交信息，返回成交数量,注：
		virtual bool QueryOrder(OrderID id, int* pCount) = 0;

		///取消委托单，这里的取消并不见得能够取消（注：可能单子在发送取消命令时已经成功成交）， 此函数作用是让这个orderid单子的状态成为确定, 确定返回true，否则返回false（有可能网络出现问题，则长时间无法返回任何状态）
		virtual	bool CancelOrder(OrderID orderid) = 0;
		
		///查询已成交单信息
		virtual bool QueryDealedOrders(std::list<DealedOrder>* pres) = 0;

		///查询委托单信息（包含上面成绩单信息,数据量可能比较大）
		virtual bool QueryEntrustInfos(std::list<EntrustInfo>* pinfo) = 0;

	public:

		///查询帐户基本信息（资金量等）
		virtual bool QueryAccountInfo(BasicAccountInfo* info) = 0;

		///查询帐户当前持仓信息
		virtual bool QueryPostions(std::list<PositionInfo>* pPoss, const TCHAR* pstock = NULL) = 0;

	public:


	public:

		virtual tstring GetName() = 0;
	};
};

#endif