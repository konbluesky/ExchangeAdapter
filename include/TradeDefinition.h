#ifndef __TRADE_DEFINITION_H__
#define __TRADE_DEFINITION_H__




namespace tradeadapter
{


	enum ELanguagePref
	{
		eEnglish = 0,
		eChinese = 1,
	};




	/// <summary>
	/// 价格模式
	/// </summary>
	enum EPriceMode
	{
		/// <summary>
		/// 限价
		/// </summary>
		eLimitPrice,
		/// <summary>
		/// 市价委托--最新价(默认)
		/// </summary>
		eLastPrice,
		/// <summary>
		/// 市价委托
		/// </summary>
		eMarketPrice,
		/// <summary>
		/// 市价委托--本方最优价
		/// </summary>
		eMarketPriceThisSide,
		/// <summary>
		/// 市价委托--对手方最优价
		/// </summary>
		eMarketPriceThatSide,
		/// <summary>
		/// 市价委托--即时成交剩余撤销
		/// </summary>
		eMarketPriceCancelRest,
		/// <summary>
		/// 市价委托--最优五档即时成交剩余转限价
		/// </summary>
		eMarketPriceLimitedRest
	};



	/// <summary>
	/// 开平标志
	/// </summary>
	enum EOpenClose
	{
		/// <summary>
		/// 开仓
		/// </summary>
		eOpen,
		/// <summary>
		/// 平仓
		/// </summary>
		eClose,
		/// <summary>
		/// 平今
		/// </summary>
		eCloseToday,
		/// <summary>
		/// 平昨
		/// </summary>
		eCloseYestoday,
		/// <summary>
		/// 强平
		/// </summary>
		eForceClose
	};



	/// <summary>
	/// 委托状态
	/// </summary>
	enum EEntrustStatus
	{
		e未报 = 0,
		e已报 = 2,
		e已报待撤 = 3,
		e部成待撤 = 4,
		e部撤 = 5,
		e已撤 = 6,
		e部成 = 7,
		e已成 = 8,
		e废单 = 9
	};


	enum EHedgeFlag
	{
		/// <summary>
		/// 投机
		/// </summary>
		eSpeculation = 1,
		/// <summary>
		/// 套保
		/// </summary>
		eHedge = 3
	};




	enum ETradeUnit
	{
		/// <summary>
		/// 手
		/// </summary>
		eHand,
		/// <summary>
		/// 股
		/// </summary>
		eSection,
		/// <summary>
		/// 张
		/// </summary>
		ePiece
	};




	//In Different systems,the definitions are defferent. so don't define values.
	//no zero,which should cause business orror easily.
	enum EBuySell
	{
		eNone = 0,
		eBuy = 1,
		eSell = 2,

	};



	//取消标志
	enum ECancelflag
	{
		eFailed,
		eSuccessed,

	};





	/// <summary>
	/// 持仓方向
	/// </summary>
	enum EPosSide
	{
		/// <summary>
		/// 多
		/// </summary>
		eMore,
		/// <summary>
		/// 空
		/// </summary>
		eShort,
		/// <summary>
		/// 清
		/// </summary>
		eClear
	};



	enum EPositionDateType
	{
		eUseHistory,
		eNoUseHistory
	};



	typedef int OrderID; // 订单号

	const OrderID InvalidOrderId = -1;

	//order的定义,字段大概这些名称，
	struct Order
	{
		tstring clientID;		//From which client(this is not only for account.
		tstring ContractCode;	//注股票里是stock_code，期货里是期货合约id
		EBuySell OpeDir;			//买卖方向
		double	Price;			//note: we only put fixed price order.
		int		Amount;
		int		Day;			//20131010 = 2013.10.10
		int		Time;			//121120 = 12:11:20	

		EPriceMode pricemode;	//comment: default value is fixed price, and havn't used in stock systems.
		EOpenClose openclose;	//only used on ctp/future,
		EHedgeFlag hedge;		//ctp only
		OrderID	orderid;
	public:
		Order()
		{
#pragma warning(disable : 4482)
			OpeDir = EBuySell::eNone;
			Price = 0.0;
			Amount = 0;
			Day = -1;//UTimeUtils::GetNowDate();
			Time = 0;
			pricemode = EPriceMode::eLimitPrice;
			openclose = EOpenClose::eOpen;
			hedge = EHedgeFlag::eHedge;
			orderid = InvalidOrderId;
		}

	};










	//注：金正的orderid和ordersno有点混乱，ordersno是委托号,这里的一个单子可能被多次部分成交，
	//所以这个内容需要修改
	struct EntrustInfo
	{

		ECancelflag  cflag;//取消标志
		double  matchamt;//成交金额
		int matchqty;	//成交数量
		int operdate;	//成交日期
		int opertime;	//成交时间
		//	OrderStatus orderstatus;

		//下面是原来委托时的信息
		EBuySell Ope;	//委托方向
		OrderID orderid;	//委托id
		int orderdate;		//委托日期
		int ordertime;		//委托时间
		tstring stkcode;	//股票代码
		tstring stkname;	//股票名称
		int		orderqty;	//委托数量
		double		orderprice;
		double orderfrzamt;//冻结金额

		EOpenClose futureOC;

	public:
		EntrustInfo()
		{
			memset(this, 0, sizeof(EntrustInfo));
		}

	};



	struct  BasicAccountInfo
	{
	public:

		///资金账户
		tstring FundAccount;

		tstring BranchID;
		//客户ID
		tstring ClientID;		//

		///客户名称
		tstring ClientName;
		tstring MoneyType;
		///当前余额
		double CurrentBalance;  //当前余额
		///可用余额
		double EnableBalance;  //可用余额
		double FetchCash;
		///总资产
		double AssetBalance;	//总资产？?
		tstring SHStockAccount; //sh 股东号
		tstring SZStockAccount; //sz 股东号





		//NEW ADD 所属柜台
		tstring ExProvider;
	public:
		BasicAccountInfo()
		{
			memset(this, 0, sizeof(BasicAccountInfo));
		}

	};








	struct DealedOrder
	{

	public:
		/// <summary>
		/// 定位串 
		/// </summary>

		tstring ClientID;

		/// <summary>
		/// SerialNo = Businessno 成交编号
		/// </summary>
		int serial_no;
		/// <summary>
		/// 日期 
		/// </summary>
		int date;
		/// <summary>
		/// 交易类别 
		/// </summary>
		//tstring exchange_type ;
		/// <summary>
		/// 证券账号 
		/// </summary>
		tstring stock_account;
		/// <summary>
		/// 证券代码 
		/// </summary>
		tstring stock_code;
		/// <summary>
		/// 证券名称 
		/// </summary>
		tstring stock_name;
		/// <summary>
		/// 买卖方向 
		/// </summary>
		EBuySell entrust_bs;
		/// <summary>
		/// 买卖方向名称 
		/// </summary>
		//tstring bs_name ; //注：这个可以不填
		/// <summary>
		/// 成交价格 
		/// </summary>
		double business_price;
		/// <summary>
		/// 成交数量 
		/// </summary>
		double business_amount;
		/// <summary>
		/// 成交时间 
		/// </summary>
		int business_time;
		/// <summary>
		/// 成交状态0：‘成交’，2：‘废单’4：‘确认’ 
		/// </summary>
		//tstring business_status ;
		/// <summary>
		/// 成交状态名称 
		/// </summary>
		//tstring status_name ;
		/// <summary>
		/// 成交类别，0：‘买卖’2：‘撤单’ 
		/// </summary>
		//tstring business_type; 
		/// <summary>
		/// 成交类别名称 
		/// </summary>
		//tstring type_name ;
		/// <summary>
		/// 成交笔数 
		/// </summary>
		//int business_times ;
		/// <summary>
		/// 合同号 
		/// </summary>
		int entrust_no;
		/// <summary>
		/// 申报号 
		/// </summary>
		int report_no; //
		/// <summary>
		/// 成交金额 
		/// </summary>
		double business_balance;

		///
		///期货专用
		EPosSide posside;
	public:
		DealedOrder()
		{
			memset(this, 0, sizeof(DealedOrder));
		}
		static int comparebyDealedID(const DealedOrder& left, const DealedOrder& right)
		{
			bool ret = left.serial_no < right.serial_no;
			return ret;
		}
		static int comparebyDealedTime(const DealedOrder& left, const DealedOrder& right)
		{
			bool ret = left.business_time < right.business_time;
			return ret;
		}

	};






	struct PositionInfo
	{
	public:


		/// <summary>
		/// 交易类别 
		/// 没有用
		/// </summary>
		///tstring exchange_type; 
		/// <summary>
		/// 证券账号 
		/// </summary>
		tstring stock_account;
		/// <summary>
		/// 证券/期货代码 
		/// </summary>
		tstring code;
		/// <summary>
		/// 证券名称 
		/// </summary>
		tstring stock_name;
		/// <summary>
		/// 当前数量 
		/// </summary>
		double current_amount;
		/// <summary>
		/// 可卖股票 
		/// </summary>
		double enable_amount;
		/// <summary>
		/// 最新价 
		/// </summary>
		double total_amount;
		/// <summary>
		/// 总数量（期货专用） 
		/// </summary>
		double total_diaableamount;
		/// <summary>
		/// 已平数量（期货专用） 
		/// </summary>
		double last_price;
		/// <summary>
		/// 成本价 
		/// </summary>
		double cost_price;
		/// <summary>
		/// 买卖盈亏 
		/// </summary>
		double income_balance;
		/// <summary>
		/// 股手标志，'0'--股   '1' -- 手。 
		/// </summary>
		ETradeUnit hand_flag;
		/// <summary>
		/// 证券市值，在明细下为每个股东帐号的市值，而在汇总下则为fund_account的，汇总下无stock_account。 
		/// </summary>
		double market_value;
		EPositionDateType datetype;
		EPosSide posside;
	public:
		PositionInfo()
		{
			memset(this, 0, sizeof(PositionInfo));
		}

	};
};



#endif // !__TRADE_DEFINITION__
