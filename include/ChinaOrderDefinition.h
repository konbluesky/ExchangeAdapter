#ifndef __CHINA_ORDER_DEFINITION_H__
#define __CHINA_ORDER_DEFINITION_H__
#include "CommonDefinition.h"
#include "DateTime.h"

/**
 * 后面数据结构的设计思想,按照参数本质一致,那么放到一起.
 * 如果在转到FIX时,出现交易所之间不一致的情况,一律由
 * 交易所负责实现.
 *
 * 暂时全部用union来控制各个交易所之间的差别.最后合并或者独立
 * 的时候,直接按照交易所来实现即可.
 * 
 * 对于所有的多余参数,在第一期工程中,将全部去掉,这样,找到
 * 必填字段的最小集.
 * 后面字符串全部用tchar数组.
 * 
 * 对于响应没有携带的字段:
 * 必带的和非必带的要区分
 * 数字,多定义一个标志字段.
 * 字符串为空即为不存在
 * 枚举型多定义一个未知型.
 * 尽力按照标准FIX字段进行定义.对于比标准FIX多的,那么不携带.
 * 
 * 对于请求字段,公共字段和私有字段分开,
 * 必带的
 * 有条件必带的
 * 不要求的,对于不要求的字段跟响应字段一致.目前不考虑该字段
 */
typedef int32 OrderID;
typedef tstring ParticipantID;
typedef tstring UserID;
typedef tstring ClientID;
/* 大连是数字,但是可以先转成字符串,并不影响 */
typedef tstring SysOrderID;
/* 成交号 */
typedef tstring ExecID;
typedef int32 AccountID;


enum OpenCloseFlag
{
	Open=0,//开仓
	Close=1,//平仓
	ForceClose = 2,//强平（一般交易所才有权限使用）
	CloseToday = 3,//平今
	CloseYesterday = 4//平昨
};

enum OrderTypeFlag
{
	Market = 1, //市价
	Limit = 2, //限价
	Stop = 3, //止损
	Stop_Limit = 4 //限价止损
};

enum TradeSide
{
	BUY=0,
	SELL=1
};

enum OrderStatusFlag
{
	New = 0,
	PartiallyFilled = 1,
	Filled = 2,
	DoneForDay = 3,
	Canceled = 4,
	Replaced = 5,
	PendingCancel = 6,
	Stopped = 7,
	Rejected = 8,
	Suspended = 9,
	PendingNew = 10,
	Calculated = 11,
	Expired = 12,
	AcceptedForBidding = 13,
	PendingReplace = 14
};

enum ExecTypeFlag
{
	ExecType_New = 0,
	ExecType_PartiallyFilled = 1,
	ExecType_Filled = 2,
	ExecType_DoneForDay = 3,
	ExecType_Canceled = 4,
	ExecType_Replaced = 5,
	ExecType_PendingCancel = 6,
	ExecType_Stopped = 7,
	ExecType_Rejected = 8,
	ExecType_Suspended = 9,
	ExecType_PendingNew = 10,
	ExecType_Calculated = 11,
	ExecType_Expired = 12,
	ExecType_Restated = 13,
	ExecType_PendingReplace = 14
};

enum TimeConditionFlag
{
	Day=0,//Rest of Day
	GTC=1,//Good Till Cancel
	//OPG = 2, //At the Opening
	IOC = 3, //Immediate or Cancel
	//FOK = 4 //Fill or Kill
	//GTD = 6, //Good till date
};

enum BusinessRejectReasonFlag
{
	BRR_Other = 0,
	BRR_UnknownID = 1,
	BRR_UnknownSecurity = 2,
	BRR_UnsupportedMessageType = 3,
	BRR_ApplicationNotAvailable = 4,
	BRR_ConditionallyRequiredFieldMissing = 5,
	BRR_NotAuthorized = 6,
	BRR_DeliverToFirmNotAvailableAtThisTime = 7,
	BRR_InvalidPriceIncrement = 18
};

/**
 * Order 格式
 * 请求数据字段
 */
struct Order {
	Order(){
		std::memset(this,0,sizeof(Order));/* 后面需要删去 */
	}

/**
 * 必带字段
 */
	/* 本地报单编号,都存在,但是字段格式不一致而已 */
	union{
		tchar zzIDLocalOrder[24];
		tchar zjIDLocalOrder[13];
		uint32 dlIDLocalOrder;
	};

	TradeSide Side;
	int32 Quantity;
	OrderTypeFlag OrderType;
	OpenCloseFlag OpenClose;
	tstring SecurityDesc;
	ClientID  IDClient;
	
	union {
		struct {
			/* 请求编号(用Fix消息中的MsgSeqNum) */
			int32 IDRequest;
			TimeConditionFlag TimeCondition;
		}zjpri;
		struct {
			TimeConditionFlag TimeCondition;
		}zzpri;
	};

/**
 * 有条件必带
 */
	double LimitPrice;
	double StopPrice;
/**
 * 可选,暂时不去列出来
 */
};

/**
 * 撤单请求
 */ 
struct OrderCancelRequest
{
/**
 * 必带字段
 */
	ClientID IDClient;
	SysOrderID IDSysOrder;
	union{
		tchar zzIDLocalOrder[24];
		uint32 dlIDLocalOrder;
		tchar zjIDLocalOrder[13];
	};

	tstring IDInstrument;
	union {
		struct {
			/* 请求编号(用Fix消息中的MsgSeqNum) */
			int32 IDRequest;
		}zjpri;
		/* DL */
		struct {
			/* 请求编号(用Fix消息中的MsgSeqNum) */
			int32 IDRequest;
		}dlpri;
		struct {
			/* 订单类型 */
			OrderTypeFlag orderType;
		}zzpri;
	};
};

/**
 * 改单请求
 */
struct OrderCancelReplaceRequest
{
	ClientID IDClient;
	SysOrderID IDSysOrder;
	union{
		tchar zzIDLocalOrder[24];
		uint32 dlIDLocalOrder;;
		tchar zjIDLocalOrder[13];
	};
	int32 IDRequest;
	TradeSide Side;
	double Price;
	int32 QtyOrder;
};

struct OrderStatusRequest {
	union{
		tchar zzIDLocalOrder[24];
		uint32 dlIDLocalOrder;;
		tchar zjIDLocalOrder[13];
	};
	SysOrderID IDSysOrder;
	TradeSide Side;
	tstring SecurityDesc;
};

/**
 * 后面该部分需要增加一个直接标志类型的字段
 */
struct ExecutionReport
{
	/* 按照标准协议,该字段不是必带,基于需要,暂时不要 */
	/* ClientID IDClient; */
	SysOrderID IDSysOrder;
	union{
		tchar zzIDLocalOrder[24];
		tchar zjIDLocalOrder[13];
		uint32 dlIDLocalOrder;
	};

	tstring SecurityDesc;
	TradeSide Side;
	OrderStatusFlag OrderStatus;
	ExecTypeFlag ExecType;
	//原始总申报数量
	int32 QtyOrder;
	double PriceLimit;
	double PriceStop;
	/* 下单成功时时间,取消下单时的时间,结束下单时的时间 */
	LIB_NS::DateTime datetime;
};

/**
 * 下单成功
 */
struct ExeReportNew
{
	ClientID IDClient;
	SysOrderID IDSysOrder;
	union{
		tchar zzIDLocalOrder[24];
		tchar zjIDLocalOrder[13];
		uint32 dlIDLocalOrder;
	};

	tstring SecurityDesc;
	TradeSide Side;
	OrderStatusFlag OrderStatus;
	ExecTypeFlag ExecType;
	//原始总申报数量
	int32 QtyOrder;
	double PriceLimit;
	double PriceStop;
	/* 下单成功时时间 */
	LIB_NS::DateTime datetime;
};

struct ExeReportCancel
{
	ClientID IDClient;
	SysOrderID IDSysOrder;
	union{
		tchar zzIDLocalOrder[24];
		tchar zjIDLocalOrder[13];
		uint32 dlIDLocalOrder;
	};

	tstring SecurityDesc;
	TradeSide Side;
	OrderStatusFlag OrderStatus;
	ExecTypeFlag ExecType;
	//原始总申报数量
	int32 QtyOrder;
	double PriceLimit;
	double PriceStop;
	/* 取消下单时的时间 */
	LIB_NS::DateTime datetime;
};

/**
 * 成交应答
 */
struct FillReport
{
	ClientID IDClient;
	SysOrderID IDSysOrder;
	union{
		tchar zzIDLocalOrder[24];
		tchar zjIDLocalOrder[13];
		uint32 dlIDLocalOrder;
	};
	ExecID IDExec;
	
	tstring SecurityDesc;
	TradeSide Side;
	double LastPx;/* 本单最新成交价格 */
	int32 QtyLastShares;/* 本单成交量 */
	/* 成交时间 UTC时间：yyyyMMdd-HHmmss.fff */
	LIB_NS::DateTime TradeTime;
};

/**
 * 撤单时被拒绝
 */
struct OrderCancelReject
{
	SysOrderID IDSysOrder;
	union{
		tchar zzIDLocalOrder[24];
		uint32 dlIDLocalOrder;
		/* 确认一下 */
		int32 LastMsgSeqNumProcessed;
	};
	tstring ErrorCode;
	tstring ErrorSource;
};

/**
 * 改单时被拒绝
 */
struct OrderCancelReplaceReject
{
	SysOrderID IDSysOrder;
	union{
		tchar zzIDLocalOrder[24];
		uint32 dlIDLocalOrder;
		int32 LastMsgSeqNumProcessed;
	};
	tstring ErrorCode;
	tstring ErrorSource;
};

/**
 * 业务拒绝
 */
struct BusinessReject
{
	union{
		tchar zzIDLocalOrder[24];
		uint32 dlIDLocalOrder;
		tchar zjIDLocalOrder[13];
	};
	int32 LastMsgSeqNumProcessed;
	tstring RefMsgType;
	BusinessRejectReasonFlag BusinessRejectReason;
	tstring ErrorCode;
	tstring ErrorSource;
};

struct Bulletin
{
	//发送时间(UTC时间：yyyyMMdd-HHmmss.fff)
	LIB_NS::DateTime SendTime;
	tstring	Headline;
	tstring	Content;
	tstring	URLLink;
};


#define TEST_TEMP 1

#if TEST_TEMP

/* 改密码 */
typedef struct UserPasswordUpdate
{
	tstring	UserID;			/* 用户名id */
	tstring	ParticipantID;  /* 会员代码 */
	tstring	OldPassword;    /* 旧密码 */
	tstring	NewPassword;    /* 新密码 */
}UserPasswordUpdate;

/* 输入执行宣告 */
typedef struct InputExecOrder
{
	tstring	InstrumentID;          /* 合约编号 */
	tstring	ParticipantID;         /* 会员代码 */
	tstring	ClientID;              /* 客户代码 */
	tstring	UserID;                /* 交易用户代码 */
	tstring	ExecOrderLocalID;      /* 本地执行宣告编号 */
	int32	Volume;                /* 数量 */
    tstring	BusinessUnit;          /* 业务单元 */
}InputExecOrder;

/* 执行宣告操作 */
typedef struct ExecOrderAction
{
	tstring	ExecOrderSysID;        /* 执行宣告编号 */
	tstring	ExecOrderLocalID;      /* 本地执行宣告编号 */
	tchar	ActionFlag;            /* 报单操作标志 */
	tstring	ParticipantID;         /* 会员代码 */
	tstring	ClientID;              /* 客户代码 */
	tstring	UserID;                /* 交易用户代码 */
	tstring	ActionLocalID;         /* 操作本地编号 */
	tstring	BusinessUnit;          /* 业务单元 */
}ExecOrderAction;

/* 交易资金查询 */
typedef struct QryPartAccount
{
	tstring	PartIDStart;   /* 起始会员代码 */
	tstring	PartIDEnd;     /* 结束会员代码 */
	tstring	AccountID;     /* 资金帐号     */
}QryPartAccount;

/* 报单查询 */
typedef struct QryOrder
{
	tstring	PartIDStart;    /* 起始会员代码 */
	tstring	PartIDEnd;      /* 结束会员代码 */
	tstring	OrderSysID;     /* 报单编号 */
	tstring	InstrumentID;   /* 合约代码 */
	tstring	ClientID;       /* 客户代码 */
	tstring	UserID;         /* 交易用户代码 */
}QryOrder;

/* 报价查询 */
typedef struct QryQuote
{
	tstring	PartIDStart;    /* 起始会员代码 */
	tstring	PartIDEnd;      /* 结束会员代码 */
	tstring	QuoteSysID;     /* 报价编号 */
	tstring	ClientID;       /* 客户代码 */
	tstring	InstrumentID;   /* 合约代码 */
	tstring	UserID;         /* 交易用户代码 */
}QryQuote;

/* 成交查询 */
typedef struct QryTrade
{
	tstring	PartIDStart;   /* 起始会员代码 */
	tstring	PartIDEnd;     /* 结束会员代码 */
	tstring	InstIDStart;   /* 起始合约代码 */
	tstring	InstIDEnd;     /* 结束合约代码 */
	tstring	TradeID;       /* 成交编号 */
	tstring	ClientID;      /* 客户代码 */
	tstring	UserID;        /* 交易用户代码 */
}QryTrade;

/* 客户查询 */
typedef struct QryClient
{
	tstring	PartIDStart;   /* 起始会员代码 */
	tstring	PartIDEnd;     /* 结束会员代码 */
	tstring	ClientIDStart; /* 起始客户代码 */
	tstring	ClientIDEnd;   /* 结束客户代码 */
}QryClient;

/* 会员持仓查询 */
typedef struct QryPartPosition
{
	tstring	PartIDStart;   /* 起始会员代码 */
	tstring	PartIDEnd;     /* 结束会员代码 */
	tstring	InstIDStart;   /* 起始合约代码 */
	tstring	InstIDEnd;     /* 结束合约代码 */
}QryPartPosition;

/* 客户持仓查询 */
typedef struct QryClientPosition
{
	tstring	PartIDStart;   /* 起始会员代码 */
	tstring	PartIDEnd;     /* 结束会员代码 */
	tstring	ClientIDStart; /* 起始客户代码 */
	tstring	ClientIDEnd;   /* 结束客户代码 */
	tstring	InstIDStart;   /* 起始合约代码 */
	tstring	InstIDEnd;     /* 结束合约代码 */
	tchar	ClientType;    /* 客户类型 */
}QryClientPosition;

/* 合约查询 */
typedef struct QryInstrument
{
	tstring	SettlementGroupID; /* 结算组代码 */
	tstring	ProductGroupID;    /* 产品组代码 */
	tstring	ProductID;         /* 产品代码 */
	tstring	InstrumentID;      /* 合约代码 */
}QryInstrument;

/* 合约状态查询 */
typedef struct QryInstrumentStatus
{
	tstring	InstIDStart;   /* 起始合约代码 */
	tstring	InstIDEnd;     /* 结束合约代码 */
}QryInstrumentStatus;

/* 行情查询 */
typedef struct QryMarketData
{
	tstring	ProductID;         /* 产品代码 */
	tstring	InstrumentID;      /* 合约代码 */
}QryMarketData;

/* 公告查询 */
typedef struct QryBulletin
{
	tstring	TradingDay;  /*  交易日  */
	tstring	MarketID;    /* 市场代码 */
	tstring	BulletinID;  /* 公告编号 */
	tstring	NewsType;    /* 公告类型 */
	tchar	NewsUrgency; /* 紧急程度 */
}QryBulletin;

/* 合约价位查询 */
typedef struct QryMBLMarketData
{
	tstring	InstIDStart;   /* 起始合约代码 */
	tstring	InstIDEnd;     /* 结束合约代码 */
	tchar	Direction;     /* 买卖方向 */
}QryMBLMarketData;

/* 保值额度查询 */
typedef struct QryHedgeVolume
{
	tstring	PartIDStart;   /* 起始会员代码 */
	tstring	PartIDEnd;     /* 结束会员代码 */
	tstring	ClientIDStart; /* 起始客户代码 */
	tstring	ClientIDEnd;   /* 结束客户代码 */
	tstring	InstIDStart;   /* 起始合约代码 */
	tstring	InstIDEnd;     /* 结束合约代码 */
}QryHedgeVolume;

/* 管理报单输入 */
typedef struct InputAdminOrder
{
	tstring	InstrumentID;       /* 合约代码 */
	tchar	AdminOrderCommand;  /* 管理报单命令 */
	tstring	ClearingPartID;     /* 结算会员编号 */
	tstring	ParticipantID;      /* 交易会员编号 */
	double	Amount;             /* 金额 */
	tstring	SettlementGroupID;  /* 结算组代码 */
}InputAdminOrder;

/* 信用限额查询 */
typedef struct QryCreditLimit
{
	tstring	ParticipantID;      /* 交易会员编号 */
	tstring	ClearingPartID;     /* 结算会员编号 */
}QryCreditLimit;

/* 输入报价 */
typedef struct InputQuote
{
	tstring	QuoteSysID;         /* 报价编号 */
	tstring	ParticipantID;      /* 交易会员编号 */
	tstring	ClientID;           /* 客户代码 */
	tstring	UserID;             /* 交易用户代码 */
	int32	Volume;             /* 数量 */
	tstring	InstrumentID;       /* 合约代码 */
	tstring	QuoteLocalID;       /* 本地报价编号 */
	tstring	BusinessUnit;       /* 业务单元 */
	tstring	BidCombOffsetFlag;  /* 买方组合开平标志 */
	tstring	BidCombHedgeFlag;   /* 买方组合套保标志 */
	double 	BidPrice;           /* 买方价格 */
	tstring	AskCombOffsetFlag;  /* 卖方组合开平标志 */
	tstring	AskCombHedgeFlag;   /* 卖方组合套保标志 */
	double	AskPrice;           /* 卖方价格 */
}InputQuote;

/* 报价操作 */
typedef struct QuoteAction
{
	tstring	QuoteSysID;     /* 报价编号 */
	tstring	QuoteLocalID;   /* 本地报价编号 */
	tchar	ActionFlag;     /* 报单操作标志 */
	tstring	ParticipantID;  /* 会员代码 */
	tstring	ClientID;       /* 客户代码 */
	tstring	UserID;         /* 交易用户代码 */
	tstring	ActionLocalID;  /* 操作本地编号 */
	tstring	BusinessUnit;   /* 业务单元 */
}QuoteAction;

/* 会员资金应答 */
typedef struct RspPartAccount
{
	tstring	TradingDay;         /* 交易日 */
	tstring	SettlementGroupID;  /* 结算组代码 */
	tstring	SettlementID;       /* 结算编号 */
	double	PreBalance;         /* 上次结算准备金 */
	double	CurrMargin;         /* 当前保证金总额 */
	double	CloseProfit;        /* 平仓盈亏 */
	double	Premium;            /* 期权权利金收支 */
	double	Deposit;            /* 入金金额 */
	double	Withdraw;           /* 出金金额 */
	double	Balance;            /* 期货结算准备金 */
	double	Available;          /* 可提资金 */
	tstring	AccountID;          /* 资金帐号 */
	double	FrozenMargin;       /* 冻结的保证金 */
	double	FrozenPremium;      /* 冻结的权利金 */
	double	BaseReserve;        /* 基本准备金 */
}RspPartAccount;

/* 报价 */
typedef struct Quote1
{
	tstring	TradingDay;         /* 交易日 */
	tstring	SettlementGroupID;  /* 结算组代码 */
	tstring	SettlementID;       /* 结算编号 */
	tstring	QuoteSysID;         /* 报价编号 */
	tstring	ParticipantID;      /* 会员代码 */
	tstring	ClientID;           /* 客户代码 */
	tstring	UserID;             /* 交易用户代码 */
	int32	Volume;             /* 数量 */
	tstring	InstrumentID;       /* 合约代码 */
	tstring	QuoteLocalID;       /* 本地报价编号 */
	tstring	BusinessUnit;       /* 业务单元 */
	tstring	BidCombOffsetFlag;  /* 买方组合开平标志 */
	tstring	BidCombHedgeFlag;   /* 买方组合套保标志 */
	double 	BidPrice;           /* 买方价格 */
	tstring	AskCombOffsetFlag;  /* 卖方组合开平标志 */
	tstring	AskCombHedgeFlag;   /* 卖方组合套保标志 */
	double	AskPrice;           /* 卖方价格 */
	tstring	InsertTime;         /* 插入时间 */
	tstring	CancelTime;         /* 撤销时间 */
	tstring	TradeTime;          /* 成交时间 */
	tstring	BidOrderSysID;      /* 买方报单编号 */
	tstring	AskOrderSysID;      /* 卖方报单编号 */
	tstring	ClearingPartID;     /* 结算会员编号 */
}Quote1;

/* 成交 */
typedef struct Trade
{
	tstring	TradingDay;         /* 交易日 */
	tstring	SettlementGroupID;  /* 结算组代码 */
	tstring	SettlementID;       /* 结算编号 */
	tstring	TradeID;            /* 成交编号 */
	tchar	Direction;          /* 买卖方向 */
	tstring	OrderSysID;         /* 报单编号 */
	tstring	ParticipantID;      /* 会员代码 */
	tstring	ClientID;           /* 客户代码 */
	tchar	TradingRole;        /* 交易角色 */
	tstring	AccountID;		    /* 资金帐号 */
	tstring	InstrumentID;       /* 合约代码 */
	tchar	OffsetFlag;         /* 开平标志 */
	tchar	HedgeFlag;          /* 投机套保标志 */
	double	Price;              /* 价格 */
	int32	Volume;             /* 数量 */
	tstring	TradeTime;          /* 成交时间 */
	tchar	TradeType;          /* 成交类型 */
	tchar	PriceSource;        /* 成交价来源 */
	tstring	UserID;             /* 交易用户代码 */
	tstring	OrderLocalID;       /* 本地报单编号 */
	tstring	ClearingPartID;     /* 结算会员编号 */
	tstring	BusinessUnit;       /* 业务单元 */
}Trade;

/* 客户查询应答 */
typedef struct RspClient
{
	tstring	ClientID;              /* 客户代码 */
	tstring	ClientName;            /* 客户名称 */
	tstring	IdentifiedCardType;    /* 证件类型 */
	tstring	UseLess;               /* 原证件号码 */
	tchar	TradingRole;           /* 交易角色 */
	tchar	ClientType;            /* 客户类型 */
	int32	IsActive;              /* 是否活跃 */
	tstring	ParticipantID;         /* 会员号 */
	tstring	IdentifiedCardNo;      /* 证件号码 */
}RspClient;

/* 会员持仓应答 */
typedef struct RspPartPosition
{
	tstring	TradingDay;         /* 交易日 */
	tstring	SettlementGroupID;  /* 结算组代码 */
	tstring	SettlementID;       /* 结算编号 */
	tchar	HedgeFlag;          /* 投机套保标志 */
	tchar	PosiDirection;      /* 持仓多空方向 */
	int32	YdPosition;         /* 上日持仓 */
	int32	Position;           /* 今日持仓 */
	int32	LongFrozen;         /* 多头冻结 */
	int32	ShortFrozen;        /* 空头冻结 */
	int32	YdLongFrozen;       /* 昨日多头冻结 */
	int32	YdShortFrozen;      /* 昨日空头冻结 */
	tstring	InstrumentID;       /* 合约代码 */
	tstring	ParticipantID;      /* 会员代码 */
	tchar	TradingRole;        /* 交易角色 */
}RspPartPosition;

/* 客户持仓应答 */
typedef struct RspClientPosition
{
	tstring	TradingDay;         /* 交易日 */
	tstring	SettlementGroupID;  /* 结算组代码 */
	tstring	SettlementID;       /* 结算编号 */
	tchar	HedgeFlag;          /* 投机套保标志 */
	tchar	PosiDirection;      /* 持仓多空方向 */
	int32	YdPosition;         /* 上日持仓 */
	int32	Position;           /* 今日持仓 */
	int32	LongFrozen;         /* 多头冻结 */
	int32	ShortFrozen;        /* 空头冻结 */
	int32	YdLongFrozen;       /* 昨日多头冻结 */
	int32	YdShortFrozen;      /* 昨日空头冻结 */
	int32	BuyTradeVolume;     /* 当日买成交量 */
	int32	SellTradeVolume;    /* 当日卖成交量 */
	double	PositionCost;       /* 持仓成本 */
	double	YdPositionCost;     /* 昨日持仓成本 */
	double	UseMargin;          /* 占用的保证金 */
	double	FrozenMargin;       /* 冻结的保证金 */
	double	LongFrozenMargin;   /* 多头冻结的保证金 */
	double	ShortFrozenMargin;  /* 空头冻结的保证金 */
	double	FrozenPremium;      /* 冻结的权利金 */
	tstring	InstrumentID;       /* 合约代码 */
	tstring	ParticipantID;      /* 会员代码 */
	tstring	ClientID;           /* 客户代码 */
}RspClientPosition;

/* 合约查询应答 */
typedef struct RspInstrument
{
	tstring	SettlementGroupID;  /* 结算组代码 */
	tstring	ProductID;          /* 产品代码 */
	tstring	ProductGroupID;     /* 产品组代码*/
	tstring	UnderlyingInstrID;  /* 基础商品代码 */
	tchar	ProductClass;       /* 产品类型 */
	tchar	PositionType;       /* 持仓类型 */
	double	StrikePrice;        /* 执行价 */
	tchar	OptionsType;        /* 期权类型 */
	int32	VolumeMultiple;     /* 合约数量乘数 */
	double	UnderlyingMultiple; /* 合约基础商品乘数 */
	tstring	InstrumentID;       /* 合约代码 */
	tstring	InstrumentName;     /* 合约名称 */
	int32	DeliveryYear;       /* 交割年份 */
	int32	DeliveryMonth;      /* 交割月 */
	tstring	AdvanceMonth;       /* 提前月份 */
	int32	IsTrading;          /* 当前是否交易 */
	tstring	CreateDate;         /* 创建日 */
	tstring	OpenDate;           /* 上市日 */
	tstring	ExpireDate;         /* 到期日 */
	tstring	StartDelivDate;     /* 开始交割日 */
	tstring	EndDelivDate;       /* 最后交割日 */
	double	BasisPrice;         /* 挂牌基准价 */
	int32	MaxMarketOrderVolume;   /* 市价单最大下单量 */
	int32	MinMarketOrderVolume;   /* 市价单最小下单量 */
	int32	MaxLimitOrderVolume;    /* 限价单最大下单量 */
	int32	MinLimitOrderVolume;    /* 限价单最小下单量 */
	double	PriceTick;              /* 最小变动价位 */
	int32	AllowDelivPersonOpen;   /* 交割月自然人开仓 */
}RspInstrument;

/* 合约状态 */
typedef struct InstrumentStatus
{
	tstring	SettlementGroupID;  /* 结算组代码 */
	tstring	InstrumentID;       /* 合约代码 */
	tchar	InstrumentStatus1;   /* 合约交易状态 */
	int32	TradingSegmentSN;   /* 交易阶段编号 */
	tstring	EnterTime;          /* 进入本状态时间 */
	tchar	EnterReason;        /* 进入本状态原因 */
}InstrumentStatus;

/* 市场行情 */
typedef struct MarketData
{
	tstring	TradingDay;         /* 交易日 */
	tstring	SettlementGroupID;  /* 结算组代码 */
	tstring	SettlementID;       /* 结算编号 */
	double	LastPrice;          /* 最新价 */
	double	PreSettlementPrice; /* 昨结算 */
	double	PreClosePrice;      /* 昨收盘 */
	double	PreOpenInterest;    /* 昨持仓量 */
	double	OpenPrice;          /* 今开盘 */
	double	HighestPrice;       /* 最高价 */
	double	LowestPrice;        /* 最低价 */
	int32	Volume;             /* 数量 */
	double	Turnover;           /* 成交金额 */
	double	OpenInterest;       /* 持仓量 */
	double	ClosePrice;         /* 今收盘 */
	double	SettlementPrice;    /* 今结算 */
	double	UpperLimitPrice;    /* 涨停板价 */
	double	LowerLimitPrice;    /* 跌停板价 */
	double	PreDelta;           /* 昨虚实度 */
	double	CurrDelta;          /* 今虚实度 */
	tstring	UpdateTime;         /* 最后修改时间 */
	int32	UpdateMillisec;     /* 最后修改毫秒 */
	tstring	InstrumentID;       /* 合约代码 */
}MarketData;

/* 分价表 */
typedef struct MBLMarketData
{
	tstring	InstrumentID;       /* 合约代码 */
	tchar	Direction;          /* 买卖方向 */
	double	Price;              /* 价格 */
	int32	Volume;             /* 数量 */
}MBLMarketData;

/* 保值额度量 */
typedef struct HedgeVolume
{
	tstring	TradingDay;         /* 交易日 */
	tstring	SettlementGroupID;  /* 结算组代码 */
	tstring	SettlementID;       /* 结算编号 */
	tstring	ParticipantID;      /* 会员代码 */
	tstring	ClientID;           /* 客户代码 */
	tstring	InstrumentID;       /* 合约代码 */
	int32	LongVolumeOriginal; /* 多头保值额度最初申请量 */
	int32	ShortVolumeOriginal;/* 空头保值额度最初申请量 */
	int32	LongVolume;         /* 多头保值额度 */
	int32	ShortVolume;        /* 空头保值额度 */
}HedgeVolume;

/* 信用限额 */
typedef struct CreditLimit
{
	tstring	TradingDay;         /* 交易日 */
	tstring	SettlementGroupID;  /* 结算组代码 */
	tstring	SettlementID;       /* 结算编号 */
	double	PreBalance;         /* 上次结算准备金 */
	double	CurrMargin;         /* 当前保证金总额 */
	double	CloseProfit;        /* 平仓盈亏 */
	double	Premium;            /* 期权权利金收支 */
	double	Deposit;            /* 入金金额 */
	double	Withdraw;           /* 出金金额 */
	double	Balance;            /* 期货结算准备金 */
	double	Available;          /* 可提资金 */
	tstring	ParticipantID;      /* 会员代码 */
	tstring	ClearingPartID;     /* 结算会员编号 */
	double	FrozenMargin;       /* 冻结的保证金 */
	double	FrozenPremium;      /* 冻结的权利金 */
}CreditLimit;

/* 用户登出应答 */
typedef struct RspUserLogout
{
	tstring	UserID;            /* 交易用户代码 */
	tstring	ParticipantID;     /* 会员代码 */
}RspUserLogout;

/* 执行宣告 */
typedef struct ExecOrder
{
	tstring	TradingDay;         /* 交易日 */
	tstring	SettlementGroupID;  /* 结算组代码 */
	tstring	SettlementID;       /* 结算编号 */
	tstring	InstrumentID;       /* 合约代码 */
	tstring	ParticipantID;      /* 会员代码 */
	tstring	ClientID;           /* 客户代码 */
	tstring	UserID;             /* 交易用户代码 */
	tstring	ExecOrderLocalID;   /* 本地执行宣告编号 */
	int32	Volume;             /* 数量 */
	tstring	BusinessUnit;       /* 业务单元 */
	tstring	ExecOrderSysID;     /* 执行宣告编号 */
	tstring	InsertDate;         /* 报单日期 */
	tstring	InsertTime;         /* 插入时间 */
	tstring	CancelTime;         /* 撤销时间 */
	tchar	ExecResult;         /* 执行结果 */
	tstring	ClearingPartID;     /* 结算会员编号 */
}ExecOrder;

/* 合约 */
typedef struct Instrument
{
	tstring	SettlementGroupID;  /* 结算组代码 */
	tstring	ProductID;          /* 产品代码 */
	tstring	ProductGroupID;     /* 产品组代码*/
	tstring	UnderlyingInstrID;  /* 基础商品代码 */
	tchar	ProductClass;       /* 产品类型 */
	tchar	PositionType;       /* 持仓类型 */
	double	StrikePrice;        /* 执行价 */
	tchar	OptionsType;        /* 期权类型 */
	int32	VolumeMultiple;     /* 合约数量乘数 */
	double	UnderlyingMultiple; /* 合约基础商品乘数 */
	tstring	InstrumentID;       /* 合约代码 */
	tstring	InstrumentName;     /* 合约名称 */
	int32	DeliveryYear;       /* 交割年份 */
	int32	DeliveryMonth;      /* 交割月 */
	tstring	AdvanceMonth;       /* 提前月份 */
	int32	IsTrading;          /* 当前是否交易 */
}Instrument;

/* 组合交易合约的单腿 */
typedef struct CombinationLeg
{
	tstring	SettlementGroupID;  /* 结算组代码 */
	tstring	CombInstrumentID;   /* 组合合约代码 */
	tstring	LegID;              /* 单腿编号 */
	tstring	LegInstrumentID;    /* 单腿合约代码 */
	tchar	Direction;          /* 买卖方向 */
	int32	LegMultiple;        /* 单腿乘数 */
	int32	ImplyLevel;         /* 推导层数 */
}CombinationLeg;

/* 别名定义 */
typedef struct AliasDefine
{
	int32	StartPos;      /* 起始位置 */
	tstring	Alias;         /* 别名 */
	tstring	OriginalText;  /* 原文 */
}AliasDefine;

/* 输入执行宣告 */
typedef struct InputExecOrder1
{
	tstring	InstrumentID;       /* 合约代码 */
	tstring	ParticipantID;      /* 会员代码 */
	tstring	ClientID;           /* 客户代码 */
	tstring	UserID;             /* 交易用户代码 */
	tstring	ExecOrderLocalID;   /* 本地执行宣告编号 */
	int32	Volume;             /* 数量 */
	tstring	BusinessUnit;       /* 业务单元 */
}InputExecOrder1;




#endif


#endif