#ifndef __IRMMS_DEBUG_H__
#define __IRMMS_DEBUG_H__

#include "RMMSCommon.h"

RMMS_NS_BEGIN

typedef enum RMMSDebugLevel
{
	RMMS_DEBUG_NULL = 0,
	RMMS_DEBUG_LEVEL1,
	RMMS_DEBUG_LEVEL2,
}RMMSDebugLevel;

class IRMMSDebug
{
public:
	virtual void SetDebugLevel(RMMSDebugLevel level) = 0;
	virtual int32 Debug(RMMSDebugLevel crulevel,const tchar* buf,...) = 0;
};

RMMS_NS_END

#endif