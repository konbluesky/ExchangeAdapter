#ifndef __RMMS_NAMESPACE_H__
#define __RMMS_NAMESPACE_H__

#define RMMS_NS			Rmms
#define RMMS_NS_BEGIN	namespace RMMS_NS {
#define RMMS_NS_END		}
#define	USING_RMMS_NS	using namespace RMMS_NS;

#endif