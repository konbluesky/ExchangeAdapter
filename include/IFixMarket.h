#ifndef __IFIX_MARKET_H__
#define __IFIX_MARKET_H__

#include "CommonDefinition.h"
#include "IRMMSExchAdm.h"

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4512)
#pragma warning(disable:4100)
#pragma warning(disable:4125)
#else
#error "仅支持VC,以待修改"
#endif
#include "quickfix/Message.h"
#ifdef _MSC_VER
#pragma warning(pop)
#else
#error "仅支持VC,以待修改"
#endif

class IFixMarketProcessor  : public IRMMSExchAdmRsp{
public:
	IFixMarketProcessor(){}
	virtual ~IFixMarketProcessor(){}
public:
	virtual void BroadcastFix(const FIX::Message &msg) = 0;
};

class IFixMarketProvider  : public  IRMMSExchAdmReq {
public:
	IFixMarketProvider(){}
	virtual ~IFixMarketProvider(){}
	
public:
	virtual bool SupportDoM() = 0;
	
public:
	void SetMarketProcessor(IFixMarketProcessor* processor)
	{
		m_process = processor;	
	}

public:
	void PriOnNetConnected() {m_process->OnNetConnected();}
	void PriOnNetDisconnected() {m_process->OnNetDisconnected();}
	void PriOnLogin(){m_process->OnLogin();}
	void PriOnLogout(){m_process->OnLogout();}

protected:
	void OnResponseMsg(const FIX::Message &msg)
	{
		if(NULL == m_process)
			return;
		m_process->BroadcastFix(msg);
	}

private:
	IFixMarketProcessor* m_process;
};

#endif