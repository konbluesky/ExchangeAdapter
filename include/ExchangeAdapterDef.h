#ifndef __EXCHANGEADAPTER_DEF_H__
#define __EXCHANGEADAPTER_DEF_H__


/////////////
//quote adapter imp api :dll export / import
#ifdef TRADE_ADAPTE_IMP
#define TAIAPI  __declspec(dllexport)
#else
#define TAIAPI  __declspec(dllimport)
#endif

/////////////
//quote data handle imp api :dll export / import
#ifdef TRADE_HANDLE_IMP
#define  THIAPI __declspec(dllexport)
#else
#define THIAPI __declspec(dllimport)
#endif



#include <map>
#include <string>
#include <vector>
#include <iostream>
//typedef std::map<std::string,std::string> EXDataSet;
//typedef std::vector<EXDataSet>  EXDataSetS;


//TAIAPI std::ostream& operator<<(std::ostream& os, const EXDataSet& info);
//TAIAPI std::ostream& operator<<(std::ostream& os, const EXDataSetS& sets);

#if _MSC_VER > 1600
#define PAIR_WRAP(x)   std::move(x)
#else
#define PAIR_WRAP(x)   x
#endif

#endif