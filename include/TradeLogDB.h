#pragma once
#include "IOrderService.h"
#include "ExchangeAdapterDef.h"
#include "loki/Singleton.h"

class TAIAPI CTradeLogDB
{
public:
	CTradeLogDB(void);
	virtual ~CTradeLogDB(void);

	bool InitializeDB(const char* dsn, const char* usr, const char* pwd);

	tradeadapter::BasicAccountInfo GetAccountInfo(std::string clientid, const char* exprovider);



	bool QueryPostions(std::list<tradeadapter::PositionInfo>* pPoss, const std::string&  clientid, const char* exprovider);


	bool InsertEntrustOrderInfo(int Date, int Time, const char* clientID, const char* StockAccount, const char* ContractCode, int buysell, double Price, int Amount, const char* Comment, int orderid, int Status, const char* exprovide);


	bool InsertDealedOrderInfo(int Date, int Time, const char* clientID, const char* StockAccount, const char* ContractCode, int buysell, double Price, int Amount, const char* Comment, int orderid, int Status, int dealid, const char* exprovide);


	bool LoadEntrustOrdersFromDB(std::list<tradeadapter::Order>* porders, int date, const char* exrovide, const char* clientid);


	bool LoadDealedOrderFromDB(std::list<tradeadapter::DealedOrder>* porders, int Ndate, const char* exrovide, const char* clientid);

	bool InsertOrUpdatePositionInfo(const tradeadapter::PositionInfo& info, const char* clientid, const char* exprovider);

	bool UpdateAccountInfo(const tradeadapter::BasicAccountInfo& info);


};

typedef Loki::SingletonHolder<CTradeLogDB>  CTradeLogDBHolder;
