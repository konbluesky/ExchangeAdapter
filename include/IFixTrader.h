#ifndef __IFIX_TRADER_H__
#define __IFIX_TRADER_H__


#include "IRMMSExchAdm.h"

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4512)
#pragma warning(disable:4100)
#pragma warning(disable:4125)
#else
#error "仅支持VC,以待修改"
#endif
#include "quickfix/Message.h"
#ifdef _MSC_VER
#pragma warning(pop)
#else
#error "仅支持VC,以待修改"
#endif

class IFixTraderProcessor  : public IRMMSExchAdmRsp {
public:
	IFixTraderProcessor(){}
	virtual ~IFixTraderProcessor(){}
	
public:
	virtual void ResponseFix(const FIX::Message &msg) = 0;
};

class IFixTraderProvider  : public IRMMSExchAdmReq {
public:
	IFixTraderProvider():m_started(false){}
	virtual ~IFixTraderProvider(){}
	
public:
	virtual void RequestFix(FIX::Message &msg) = 0;

public:
	void PriOnNetConnected() {m_process->OnNetConnected();}
	void PriOnNetDisconnected() {m_process->OnNetDisconnected();}
	void PriOnLogin(){m_process->OnLogin();}
	void PriOnLogout(){m_process->OnLogout();}
	
public:
	bool IsStarted() const {return m_started;}
	void SetTraderProcessor(IFixTraderProcessor* processor)
	{
		m_process = processor;	
	}
	
	void OnResponseMsg(const FIX::Message &msg)
	{
		if(NULL == m_process)
			return;
		m_process->ResponseFix(msg);
	}

protected:
	bool m_started;

private:
	IFixTraderProcessor* m_process;
};

#endif