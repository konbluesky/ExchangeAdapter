#include "StdAfx.h"
#include "ProxyClientExchangeAdapter.h"

CProxyClientExchangeAdapter::CProxyClientExchangeAdapter(void)
{
}

CProxyClientExchangeAdapter::~CProxyClientExchangeAdapter(void)
{
}

bool CProxyClientExchangeAdapter::Initialize( void* param /*= NULL */ )
{
	throw std::logic_error("The method or operation is not implemented.");
}

bool CProxyClientExchangeAdapter::IsInitialized()
{
	throw std::logic_error("The method or operation is not implemented.");
}

bool CProxyClientExchangeAdapter::InitializeByXML( const TCHAR* fpath )
{
	throw std::logic_error("The method or operation is not implemented.");
}

int CProxyClientExchangeAdapter::EntrustOrder( Order order )
{
	throw std::logic_error("The method or operation is not implemented.");
}

bool CProxyClientExchangeAdapter::CancelOrder( OrderID orderid )
{
	throw std::logic_error("The method or operation is not implemented.");
}

int CProxyClientExchangeAdapter::QueryOrder( OrderID id )
{
	throw std::logic_error("The method or operation is not implemented.");
}

bool CProxyClientExchangeAdapter::QueryDealedOrders( std::list<DealedOrder>* pres )
{
	throw std::logic_error("The method or operation is not implemented.");
}

void CProxyClientExchangeAdapter::UnInitialize()
{
	throw std::logic_error("The method or operation is not implemented.");
}

bool CProxyClientExchangeAdapter::QueryAccountInfo( BasicAccountInfo* info )
{
	throw std::logic_error("The method or operation is not implemented.");
}

bool CProxyClientExchangeAdapter::QueryPostions( std::list<PositionInfo>* pPoss,const TCHAR* pstock /*= NULL */ )
{
	throw std::logic_error("The method or operation is not implemented.");
}

bool CProxyClientExchangeAdapter::GetLastError( ErrorInfo* pError )
{
	throw std::logic_error("The method or operation is not implemented.");
}

bool CProxyClientExchangeAdapter::QueryEntrustInfos( std::list<EntrustInfo>* pinfo )
{
	throw std::logic_error("The method or operation is not implemented.");
}
