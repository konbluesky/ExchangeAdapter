#pragma once

class CProxyClientExchangeAdapter : public ITradeAdapter
{
public:
	CProxyClientExchangeAdapter(void);
	virtual ~CProxyClientExchangeAdapter(void);

	virtual bool Initialize( void* param = NULL );

	virtual bool IsInitialized();

	virtual bool InitializeByXML( const TCHAR* fpath );

	virtual int EntrustOrder( Order order );

	virtual bool CancelOrder( OrderID orderid );

	virtual int QueryOrder( OrderID id );

	virtual bool QueryDealedOrders( std::list<DealedOrder>* pres );

	virtual void UnInitialize();

	virtual bool QueryAccountInfo( BasicAccountInfo* info );

	virtual bool QueryPostions( std::list<PositionInfo>* pPoss,const TCHAR* pstock = NULL );

	virtual bool GetLastError( ErrorInfo* pError );

	virtual bool QueryEntrustInfos( std::list<EntrustInfo>* pinfo );

};
