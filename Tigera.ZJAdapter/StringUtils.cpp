#include "Stdafx.h"
#include "StringUtils.h"
using namespace System;
using namespace Runtime::InteropServices;
#pragma managed
namespace ZJAdapter 
{
	std::string StringUtils::Cast_std_string(System::String ^ s)
	{
		const char* chars = (const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
		std::string os = chars;
		Marshal::FreeHGlobal(IntPtr((void*)chars));

		return os;
	}

	std::wstring StringUtils::Cast_std_wstring (System::String ^ s)
	{
		const wchar_t* chars = (const wchar_t*)(Marshal::StringToHGlobalUni(s)).ToPointer();
		std::wstring os = chars;
		Marshal::FreeHGlobal(IntPtr((void*)chars));

		return os;
	}


	System::String^ StringUtils::Cast_String(const char* s)
	{
		System::String^ ret = Marshal::PtrToStringAnsi(IntPtr((void*)s));
		return ret;
	}

	System::String^ StringUtils::Cast_String(const wchar_t* s)
	{
		System::String^ ret = Marshal:: PtrToStringUni(IntPtr((void*)s));
		return ret;
	}

	System::String^ StringUtils::Cast_String(const std::string& s)
	{
		return Cast_String(s.c_str());
	}

	System::String^ StringUtils::Cast_String(const std::wstring& s)
	{
		return Cast_String(s.c_str());
	}

	IntPtr GetGlobMemory()
	{
		static IntPtr ptr = Marshal::AllocHGlobal(1024*1024);
		return ptr;
	}




	//郁闷，VC.NET没试出来用模板方式进行cast的代码(老是编译错误)
	//所以又使用C#来实现。
	class CastHelperx
	{
	public :
		template<typename Target>
		static Target^ Cast(IntPtr source)
		{
			Target^ ret = (Target)Marshal::PtrToStructure(source,typeid(Target));
			return ret;
		}
	};
}