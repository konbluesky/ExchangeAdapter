﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace FuncHelper
{
	//郁闷，VC.NET没试出来用模板方式进行cast的代码(老是编译错误)
	//所以又使用C#来实现。
	public static	class CastHelper
	{
		public static Target Cast<Target>(IntPtr source) where Target :struct
		{
			Target ret = (Target)Marshal.PtrToStructure(source,typeof(Target));
			return ret;
		}
	}
}
