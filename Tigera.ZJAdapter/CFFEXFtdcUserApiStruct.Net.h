#ifndef CFFEXFTDCUSERAPISTRUCT_NET_H
#define CFFEXFTDCUSERAPISTRUCT_NET_H
///注：这个是rmmsoft对应的.net版本定义

/////////////////////////////////////////////////////////////////////////
///@system 新一代交易所系统
///@company 上海期货信息技术有限公司
///@file CFFEXFtdcUserApiStruct.h
///@brief 定义了客户端接口使用的业务数据结构
///@history 
///20060106	赵鸿昊		创建该文件
/////////////////////////////////////////////////////////////////////////



#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

namespace ZJAdapter
{




///信息分发
NET_EXPORT_STRUCT CFfexFtdcDisseminationField
{
	///序列系列号
	TFfexFtdcSequenceSeriesType	SequenceSeries;
	///序列号
	TFfexFtdcSequenceNoType	SequenceNo;
};

///响应信息
NET_EXPORT_STRUCT CFfexFtdcRspInfoField
{
	///错误代码
	TFfexFtdcErrorIDType	ErrorID;
	///错误信息
	TFfexFtdcErrorMsgType	ErrorMsg;
};

///通讯阶段
NET_EXPORT_STRUCT CFfexFtdcCommPhaseField
{
	///交易日
	TFfexFtdcDateType	TradingDay;
	///通讯时段号
	TFfexFtdcCommPhaseNoType	CommPhaseNo;
};

///交易所交易日
NET_EXPORT_STRUCT CFfexFtdcExchangeTradingDayField
{
	///交易日
	TFfexFtdcDateType	TradingDay;
	///交易所代码
	TFfexFtdcExchangeIDType	ExchangeID;
};

///结算会话
NET_EXPORT_STRUCT CFfexFtdcSettlementSessionField
{
	///交易日
	TFfexFtdcDateType	TradingDay;
	///结算组代码
	TFfexFtdcSettlementGroupIDType	SettlementGroupID;
	///结算编号
	TFfexFtdcSettlementIDType	SettlementID;
};

///当前时间
NET_EXPORT_STRUCT CFfexFtdcCurrentTimeField
{
	///当前日期
	TFfexFtdcDateType	CurrDate;
	///当前时间
	TFfexFtdcTimeType	CurrTime;
	///当前时间（毫秒）
	TFfexFtdcMillisecType	CurrMillisec;
};

///用户登录请求
NET_EXPORT_STRUCT CFfexFtdcReqUserLoginField
{
	///交易日
	TFfexFtdcDateType	TradingDay;
	///交易用户代码
	TFfexFtdcUserIDType	UserID;
	///会员代码
	TFfexFtdcParticipantIDType	ParticipantID;
	///密码
	TFfexFtdcPasswordType	Password;
	///用户端产品信息
	TFfexFtdcProductInfoType	UserProductInfo;
	///接口端产品信息
	TFfexFtdcProductInfoType	InterfaceProductInfo;
	///协议信息
	TFfexFtdcProtocolInfoType	ProtocolInfo;
};

///用户登录应答
NET_EXPORT_STRUCT CFfexFtdcRspUserLoginField
{
	///交易日
	TFfexFtdcDateType	TradingDay;
	///登录成功时间
	TFfexFtdcTimeType	LoginTime;
	///最大本地报单号
	TFfexFtdcOrderLocalIDType	MaxOrderLocalID;
	///交易用户代码
	TFfexFtdcUserIDType	UserID;
	///会员代码
	TFfexFtdcParticipantIDType	ParticipantID;
	///交易系统名称
	TFfexFtdcTradingSystemNameType	TradingSystemName;
};

///用户登出请求
NET_EXPORT_STRUCT CFfexFtdcReqUserLogoutField
{
	///交易用户代码
	TFfexFtdcUserIDType	UserID;
	///会员代码
	TFfexFtdcParticipantIDType	ParticipantID;
};

///用户登出应答
NET_EXPORT_STRUCT CFfexFtdcRspUserLogoutField
{
	///交易用户代码
	TFfexFtdcUserIDType	UserID;
	///会员代码
	TFfexFtdcParticipantIDType	ParticipantID;
};

///输入报单
NET_EXPORT_STRUCT CFfexFtdcInputOrderField
{
	///报单编号
	TFfexFtdcOrderSysIDType	OrderSysID;
	///会员代码
	TFfexFtdcParticipantIDType	ParticipantID;
	///客户代码
	TFfexFtdcClientIDType	ClientID;
	///交易用户代码
	TFfexFtdcUserIDType	UserID;
	///合约代码
	TFfexFtdcInstrumentIDType	InstrumentID;
	///报单价格条件
	TFfexFtdcOrderPriceTypeType	OrderPriceType;
	///买卖方向
	TFfexFtdcDirectionType	Direction;
	///组合开平标志
	TFfexFtdcCombOffsetFlagType	CombOffsetFlag;
	///组合投机套保标志
	TFfexFtdcCombHedgeFlagType	CombHedgeFlag;
	///价格
	TFfexFtdcPriceType	LimitPrice;
	///数量
	TFfexFtdcVolumeType	VolumeTotalOriginal;
	///有效期类型
	TFfexFtdcTimeConditionType	TimeCondition;
	///GTD日期
	TFfexFtdcDateType	GTDDate;
	///成交量类型
	TFfexFtdcVolumeConditionType	VolumeCondition;
	///最小成交量
	TFfexFtdcVolumeType	MinVolume;
	///触发条件
	TFfexFtdcContingentConditionType	ContingentCondition;
	///止损价
	TFfexFtdcPriceType	StopPrice;
	///强平原因
	TFfexFtdcForceCloseReasonType	ForceCloseReason;
	///本地报单编号
	TFfexFtdcOrderLocalIDType	OrderLocalID;
	///自动挂起标志
	TFfexFtdcBoolType	IsAutoSuspend;
	///业务单元
	TFfexFtdcBusinessUnitType	BusinessUnit;
};

///报单操作
NET_EXPORT_STRUCT CFfexFtdcOrderActionField
{
	///报单编号
	TFfexFtdcOrderSysIDType	OrderSysID;
	///本地报单编号
	TFfexFtdcOrderLocalIDType	OrderLocalID;
	///报单操作标志
	TFfexFtdcActionFlagType	ActionFlag;
	///会员代码
	TFfexFtdcParticipantIDType	ParticipantID;
	///客户代码
	TFfexFtdcClientIDType	ClientID;
	///交易用户代码
	TFfexFtdcUserIDType	UserID;
	///价格
	TFfexFtdcPriceType	LimitPrice;
	///数量变化
	TFfexFtdcVolumeType	VolumeChange;
	///操作本地编号
	TFfexFtdcOrderLocalIDType	ActionLocalID;
	///业务单元
	TFfexFtdcBusinessUnitType	BusinessUnit;
};

///OTC报单
NET_EXPORT_STRUCT CFfexFtdcOTCOrderField
{
	///交易日
	TFfexFtdcDateType	TradingDay;
	///结算组代码
	TFfexFtdcSettlementGroupIDType	SettlementGroupID;
	///结算编号
	TFfexFtdcSettlementIDType	SettlementID;
	///OTC报单编号
	TFfexFtdcOTCOrderSysIDType	OTCOrderSysID;
	///会员代码
	TFfexFtdcParticipantIDType	ParticipantID;
	///客户代码
	TFfexFtdcClientIDType	ClientID;
	///交易用户代码
	TFfexFtdcUserIDType	UserID;
	///合约代码
	TFfexFtdcInstrumentIDType	InstrumentID;
	///买卖方向
	TFfexFtdcDirectionType	Direction;
	///开平标志
	TFfexFtdcOffsetFlagType	OffsetFlag;
	///投机套保标志
	TFfexFtdcHedgeFlagType	HedgeFlag;
	///价格
	TFfexFtdcPriceType	Price;
	///数量
	TFfexFtdcVolumeType	Volume;
	///对手方会员代码
	TFfexFtdcParticipantIDType	OtherParticipantID;
	///对手方客户代码
	TFfexFtdcClientIDType	OtherClientID;
	///对手方交易用户代码
	TFfexFtdcUserIDType	OtherUserID;
	///对手方开平标志
	TFfexFtdcOffsetFlagType	OtherOffsetFlag;
	///对手方套保标志
	TFfexFtdcHedgeFlagType	OtherHedgeFlag;
	///本地OTC报单编号
	TFfexFtdcOrderLocalIDType	OTCOrderLocalID;
	///OTC报单状态
	TFfexFtdcOTCOrderStatusType	OTCOrderStatus;
	///插入时间
	TFfexFtdcTimeType	InsertTime;
	///撤销时间
	TFfexFtdcTimeType	CancelTime;
	///结算会员编号
	TFfexFtdcParticipantIDType	ClearingPartID;
	///对手方结算会员编号
	TFfexFtdcParticipantIDType	OtherClearingPartID;
};

///输入报价
NET_EXPORT_STRUCT CFfexFtdcInputQuoteField
{
	///报价编号
	TFfexFtdcQuoteSysIDType	QuoteSysID;
	///会员代码
	TFfexFtdcParticipantIDType	ParticipantID;
	///客户代码
	TFfexFtdcClientIDType	ClientID;
	///交易用户代码
	TFfexFtdcUserIDType	UserID;
	///数量
	TFfexFtdcVolumeType	Volume;
	///合约代码
	TFfexFtdcInstrumentIDType	InstrumentID;
	///本地报价编号
	TFfexFtdcOrderLocalIDType	QuoteLocalID;
	///业务单元
	TFfexFtdcBusinessUnitType	BusinessUnit;
	///买方组合开平标志
	TFfexFtdcCombOffsetFlagType	BidCombOffsetFlag;
	///买方组合套保标志
	TFfexFtdcCombHedgeFlagType	BidCombHedgeFlag;
	///买方价格
	TFfexFtdcPriceType	BidPrice;
	///卖方组合开平标志
	TFfexFtdcCombOffsetFlagType	AskCombOffsetFlag;
	///卖方组合套保标志
	TFfexFtdcCombHedgeFlagType	AskCombHedgeFlag;
	///卖方价格
	TFfexFtdcPriceType	AskPrice;
};

///报价操作
NET_EXPORT_STRUCT CFfexFtdcQuoteActionField
{
	///报价编号
	TFfexFtdcQuoteSysIDType	QuoteSysID;
	///本地报价编号
	TFfexFtdcOrderLocalIDType	QuoteLocalID;
	///报单操作标志
	TFfexFtdcActionFlagType	ActionFlag;
	///会员代码
	TFfexFtdcParticipantIDType	ParticipantID;
	///客户代码
	TFfexFtdcClientIDType	ClientID;
	///交易用户代码
	TFfexFtdcUserIDType	UserID;
	///操作本地编号
	TFfexFtdcOrderLocalIDType	ActionLocalID;
	///业务单元
	TFfexFtdcBusinessUnitType	BusinessUnit;
};

///输入执行宣告
NET_EXPORT_STRUCT CFfexFtdcInputExecOrderField
{
	///合约编号
	TFfexFtdcInstrumentIDType	InstrumentID;
	///会员代码
	TFfexFtdcParticipantIDType	ParticipantID;
	///客户代码
	TFfexFtdcClientIDType	ClientID;
	///交易用户代码
	TFfexFtdcUserIDType	UserID;
	///本地执行宣告编号
	TFfexFtdcOrderLocalIDType	ExecOrderLocalID;
	///数量
	TFfexFtdcVolumeType	Volume;
	///业务单元
	TFfexFtdcBusinessUnitType	BusinessUnit;
};

///执行宣告操作
NET_EXPORT_STRUCT CFfexFtdcExecOrderActionField
{
	///执行宣告编号
	TFfexFtdcExecOrderSysIDType	ExecOrderSysID;
	///本地执行宣告编号
	TFfexFtdcOrderLocalIDType	ExecOrderLocalID;
	///报单操作标志
	TFfexFtdcActionFlagType	ActionFlag;
	///会员代码
	TFfexFtdcParticipantIDType	ParticipantID;
	///客户代码
	TFfexFtdcClientIDType	ClientID;
	///交易用户代码
	TFfexFtdcUserIDType	UserID;
	///操作本地编号
	TFfexFtdcOrderLocalIDType	ActionLocalID;
	///业务单元
	TFfexFtdcBusinessUnitType	BusinessUnit;
};

///用户登录退出
NET_EXPORT_STRUCT CFfexFtdcUserLogoutField
{
	///交易用户代码
	TFfexFtdcUserIDType	UserID;
	///会员代码
	TFfexFtdcParticipantIDType	ParticipantID;
};

///用户口令修改
NET_EXPORT_STRUCT CFfexFtdcUserPasswordUpdateField
{
	///交易用户代码
	TFfexFtdcUserIDType	UserID;
	///会员代码
	TFfexFtdcParticipantIDType	ParticipantID;
	///旧密码
	TFfexFtdcPasswordType	OldPassword;
	///新密码
	TFfexFtdcPasswordType	NewPassword;
};

///输入组合报单
NET_EXPORT_STRUCT CFfexFtdcInputCombOrderField
{
	///组合报单编号
	TFfexFtdcOrderSysIDType	CombOrderSysID;
	///会员代码
	TFfexFtdcParticipantIDType	ParticipantID;
	///客户代码
	TFfexFtdcClientIDType	ClientID;
	///交易用户代码
	TFfexFtdcUserIDType	UserID;
	///价格
	TFfexFtdcPriceType	LimitPrice;
	///数量
	TFfexFtdcVolumeType	VolumeTotalOriginal;
	///本地报单编号
	TFfexFtdcOrderLocalIDType	CombOrderLocalID;
	///业务单元
	TFfexFtdcBusinessUnitType	BusinessUnit;
	///合约代码1
	TFfexFtdcInstrumentIDType	InstrumentID1;
	///买卖方向1
	TFfexFtdcDirectionType	Direction1;
	///分腿乘数1
	TFfexFtdcLegMultipleType	LegMultiple1;
	///开平标志1
	TFfexFtdcOffsetFlagType	OffsetFlag1;
	///投机套保标志1
	TFfexFtdcHedgeFlagType	HedgeFlag1;
	///合约代码2
	TFfexFtdcInstrumentIDType	InstrumentID2;
	///买卖方向2
	TFfexFtdcDirectionType	Direction2;
	///分腿乘数2
	TFfexFtdcLegMultipleType	LegMultiple2;
	///开平标志2
	TFfexFtdcOffsetFlagType	OffsetFlag2;
	///投机套保标志2
	TFfexFtdcHedgeFlagType	HedgeFlag2;
	///合约代码3
	TFfexFtdcInstrumentIDType	InstrumentID3;
	///买卖方向3
	TFfexFtdcDirectionType	Direction3;
	///分腿乘数3
	TFfexFtdcLegMultipleType	LegMultiple3;
	///开平标志3
	TFfexFtdcOffsetFlagType	OffsetFlag3;
	///投机套保标志3
	TFfexFtdcHedgeFlagType	HedgeFlag3;
	///合约代码4
	TFfexFtdcInstrumentIDType	InstrumentID4;
	///买卖方向4
	TFfexFtdcDirectionType	Direction4;
	///分腿乘数4
	TFfexFtdcLegMultipleType	LegMultiple4;
	///开平标志4
	TFfexFtdcOffsetFlagType	OffsetFlag4;
	///投机套保标志4
	TFfexFtdcHedgeFlagType	HedgeFlag4;
};

///强制用户退出
NET_EXPORT_STRUCT CFfexFtdcForceUserExitField
{
	///会员代码
	TFfexFtdcParticipantIDType	ParticipantID;
	///交易用户代码
	TFfexFtdcUserIDType	UserID;
};

///会员资金帐户入金
NET_EXPORT_STRUCT CFfexFtdcAccountDepositField
{
	///资金账号
	TFfexFtdcAccountIDType	Account;
	///入金金额
	TFfexFtdcMoneyType	Deposit;
};

///报单查询
NET_EXPORT_STRUCT CFfexFtdcQryOrderField
{
	///起始会员代码
	TFfexFtdcParticipantIDType	PartIDStart;
	///结束会员代码
	TFfexFtdcParticipantIDType	PartIDEnd;
	///报单编号
	TFfexFtdcOrderSysIDType	OrderSysID;
	///合约代码
	TFfexFtdcInstrumentIDType	InstrumentID;
	///客户代码
	TFfexFtdcClientIDType	ClientID;
	///交易用户代码
	TFfexFtdcUserIDType	UserID;
};

///报价查询
NET_EXPORT_STRUCT CFfexFtdcQryQuoteField
{
	///起始会员代码
	TFfexFtdcParticipantIDType	PartIDStart;
	///结束会员代码
	TFfexFtdcParticipantIDType	PartIDEnd;
	///报价编号
	TFfexFtdcQuoteSysIDType	QuoteSysID;
	///客户代码
	TFfexFtdcClientIDType	ClientID;
	///合约代码
	TFfexFtdcInstrumentIDType	InstrumentID;
	///交易用户代码
	TFfexFtdcUserIDType	UserID;
};

///成交查询
NET_EXPORT_STRUCT CFfexFtdcQryTradeField
{
	///起始会员代码
	TFfexFtdcParticipantIDType	PartIDStart;
	///结束会员代码
	TFfexFtdcParticipantIDType	PartIDEnd;
	///起始合约代码
	TFfexFtdcInstrumentIDType	InstIDStart;
	///结束合约代码
	TFfexFtdcInstrumentIDType	InstIDEnd;
	///成交编号
	TFfexFtdcTradeIDType	TradeID;
	///客户代码
	TFfexFtdcClientIDType	ClientID;
	///交易用户代码
	TFfexFtdcUserIDType	UserID;
};

///行情查询
NET_EXPORT_STRUCT CFfexFtdcQryMarketDataField
{
	///产品代码
	TFfexFtdcProductIDType	ProductID;
	///合约代码
	TFfexFtdcInstrumentIDType	InstrumentID;
};

///客户查询
NET_EXPORT_STRUCT CFfexFtdcQryClientField
{
	///起始会员代码
	TFfexFtdcParticipantIDType	PartIDStart;
	///结束会员代码
	TFfexFtdcParticipantIDType	PartIDEnd;
	///起始客户代码
	TFfexFtdcClientIDType	ClientIDStart;
	///结束客户代码
	TFfexFtdcClientIDType	ClientIDEnd;
};

///会员持仓查询
NET_EXPORT_STRUCT CFfexFtdcQryPartPositionField
{
	///起始会员代码
	TFfexFtdcParticipantIDType	PartIDStart;
	///结束会员代码
	TFfexFtdcParticipantIDType	PartIDEnd;
	///起始合约代码
	TFfexFtdcInstrumentIDType	InstIDStart;
	///结束合约代码
	TFfexFtdcInstrumentIDType	InstIDEnd;
};

///客户持仓查询
NET_EXPORT_STRUCT CFfexFtdcQryClientPositionField
{
	///起始会员代码
	TFfexFtdcParticipantIDType	PartIDStart;
	///结束会员代码
	TFfexFtdcParticipantIDType	PartIDEnd;
	///起始客户代码
	TFfexFtdcClientIDType	ClientIDStart;
	///结束客户代码
	TFfexFtdcClientIDType	ClientIDEnd;
	///起始合约代码
	TFfexFtdcInstrumentIDType	InstIDStart;
	///结束合约代码
	TFfexFtdcInstrumentIDType	InstIDEnd;
	///客户类型
	TFfexFtdcClientTypeType	ClientType;
};

///交易资金查询
NET_EXPORT_STRUCT CFfexFtdcQryPartAccountField
{
	///起始会员代码
	TFfexFtdcParticipantIDType	PartIDStart;
	///结束会员代码
	TFfexFtdcParticipantIDType	PartIDEnd;
	///资金帐号
	TFfexFtdcAccountIDType	AccountID;
};

///合约查询
NET_EXPORT_STRUCT CFfexFtdcQryInstrumentField
{
	///结算组代码
	TFfexFtdcSettlementGroupIDType	SettlementGroupID;
	///产品组代码
	TFfexFtdcProductGroupIDType	ProductGroupID;
	///产品代码
	TFfexFtdcProductIDType	ProductID;
	///合约代码
	TFfexFtdcInstrumentIDType	InstrumentID;
};

///合约状态查询
NET_EXPORT_STRUCT CFfexFtdcQryInstrumentStatusField
{
	///起始合约代码
	TFfexFtdcInstrumentIDType	InstIDStart;
	///结束合约代码
	TFfexFtdcInstrumentIDType	InstIDEnd;
};

///结算组状态查询
NET_EXPORT_STRUCT CFfexFtdcQrySGDataSyncStatusField
{
	///结算组代码
	TFfexFtdcSettlementGroupIDType	SettlementGroupID;
};

///用户在线查询
NET_EXPORT_STRUCT CFfexFtdcQryUserSessionField
{
	///起始交易用户代码
	TFfexFtdcUserIDType	UserIDStart;
	///结束交易用户代码
	TFfexFtdcUserIDType	UserIDEnd;
};

///用户查询
NET_EXPORT_STRUCT CFfexFtdcQryUserField
{
	///起始交易用户代码
	TFfexFtdcUserIDType	UserIDStart;
	///结束交易用户代码
	TFfexFtdcUserIDType	UserIDEnd;
};

///公告查询
NET_EXPORT_STRUCT CFfexFtdcQryBulletinField
{
	///交易日
	TFfexFtdcDateType	TradingDay;
	///市场代码
	TFfexFtdcMarketIDType	MarketID;
	///公告编号
	TFfexFtdcBulletinIDType	BulletinID;
	///公告类型
	TFfexFtdcNewsTypeType	NewsType;
	///紧急程度
	TFfexFtdcNewsUrgencyType	NewsUrgency;
};

///会员查询
NET_EXPORT_STRUCT CFfexFtdcQryParticipantField
{
	///起始会员代码
	TFfexFtdcParticipantIDType	PartIDStart;
	///结束会员代码
	TFfexFtdcParticipantIDType	PartIDEnd;
};

///保值额度查询
NET_EXPORT_STRUCT CFfexFtdcQryHedgeVolumeField
{
	///起始会员代码
	TFfexFtdcParticipantIDType	PartIDStart;
	///结束会员代码
	TFfexFtdcParticipantIDType	PartIDEnd;
	///起始客户代码
	TFfexFtdcClientIDType	ClientIDStart;
	///结束客户代码
	TFfexFtdcClientIDType	ClientIDEnd;
	///起始合约代码
	TFfexFtdcInstrumentIDType	InstIDStart;
	///结束合约代码
	TFfexFtdcInstrumentIDType	InstIDEnd;
};

///合约价位查询
NET_EXPORT_STRUCT CFfexFtdcQryMBLMarketDataField
{
	///起始合约代码
	TFfexFtdcInstrumentIDType	InstIDStart;
	///结束合约代码
	TFfexFtdcInstrumentIDType	InstIDEnd;
	///买卖方向
	TFfexFtdcDirectionType	Direction;
};

///信用限额查询
NET_EXPORT_STRUCT CFfexFtdcQryCreditLimitField
{
	///交易会员编号
	TFfexFtdcParticipantIDType	ParticipantID;
	///结算会员编号
	TFfexFtdcParticipantIDType	ClearingPartID;
};

///组合报单查询
NET_EXPORT_STRUCT CFfexFtdcQryCombOrderField
{
	///起始会员代码
	TFfexFtdcParticipantIDType	PartIDStart;
	///结束会员代码
	TFfexFtdcParticipantIDType	PartIDEnd;
	///组合报单编号
	TFfexFtdcOrderSysIDType	CombOrderSysID;
	///客户代码
	TFfexFtdcClientIDType	ClientID;
	///交易用户代码
	TFfexFtdcUserIDType	UserID;
};

///会员资金应答
NET_EXPORT_STRUCT CFfexFtdcRspPartAccountField
{
	///交易日
	TFfexFtdcDateType	TradingDay;
	///结算组代码
	TFfexFtdcSettlementGroupIDType	SettlementGroupID;
	///结算编号
	TFfexFtdcSettlementIDType	SettlementID;
	///上次结算准备金
	TFfexFtdcMoneyType	PreBalance;
	///当前保证金总额
	TFfexFtdcMoneyType	CurrMargin;
	///平仓盈亏
	TFfexFtdcMoneyType	CloseProfit;
	///期权权利金收支
	TFfexFtdcMoneyType	Premium;
	///入金金额
	TFfexFtdcMoneyType	Deposit;
	///出金金额
	TFfexFtdcMoneyType	Withdraw;
	///期货结算准备金
	TFfexFtdcMoneyType	Balance;
	///可提资金
	TFfexFtdcMoneyType	Available;
	///资金帐号
	TFfexFtdcAccountIDType	AccountID;
	///冻结的保证金
	TFfexFtdcMoneyType	FrozenMargin;
	///冻结的权利金
	TFfexFtdcMoneyType	FrozenPremium;
	///基本准备金
	TFfexFtdcMoneyType	BaseReserve;
};

///会员持仓应答
NET_EXPORT_STRUCT CFfexFtdcRspPartPositionField
{
	///交易日
	TFfexFtdcDateType	TradingDay;
	///结算组代码
	TFfexFtdcSettlementGroupIDType	SettlementGroupID;
	///结算编号
	TFfexFtdcSettlementIDType	SettlementID;
	///投机套保标志
	TFfexFtdcHedgeFlagType	HedgeFlag;
	///持仓多空方向
	TFfexFtdcPosiDirectionType	PosiDirection;
	///上日持仓
	TFfexFtdcVolumeType	YdPosition;
	///今日持仓
	TFfexFtdcVolumeType	Position;
	///多头冻结
	TFfexFtdcVolumeType	LongFrozen;
	///空头冻结
	TFfexFtdcVolumeType	ShortFrozen;
	///昨日多头冻结
	TFfexFtdcVolumeType	YdLongFrozen;
	///昨日空头冻结
	TFfexFtdcVolumeType	YdShortFrozen;
	///合约代码
	TFfexFtdcInstrumentIDType	InstrumentID;
	///会员代码
	TFfexFtdcParticipantIDType	ParticipantID;
	///交易角色
	TFfexFtdcTradingRoleType	TradingRole;
};

///客户持仓应答
NET_EXPORT_STRUCT CFfexFtdcRspClientPositionField
{
	///交易日
	TFfexFtdcDateType	TradingDay;
	///结算组代码
	TFfexFtdcSettlementGroupIDType	SettlementGroupID;
	///结算编号
	TFfexFtdcSettlementIDType	SettlementID;
	///投机套保标志
	TFfexFtdcHedgeFlagType	HedgeFlag;
	///持仓多空方向
	TFfexFtdcPosiDirectionType	PosiDirection;
	///上日持仓
	TFfexFtdcVolumeType	YdPosition;
	///今日持仓
	TFfexFtdcVolumeType	Position;
	///多头冻结
	TFfexFtdcVolumeType	LongFrozen;
	///空头冻结
	TFfexFtdcVolumeType	ShortFrozen;
	///昨日多头冻结
	TFfexFtdcVolumeType	YdLongFrozen;
	///昨日空头冻结
	TFfexFtdcVolumeType	YdShortFrozen;
	///当日买成交量
	TFfexFtdcVolumeType	BuyTradeVolume;
	///当日卖成交量
	TFfexFtdcVolumeType	SellTradeVolume;
	///持仓成本
	TFfexFtdcMoneyType	PositionCost;
	///昨日持仓成本
	TFfexFtdcMoneyType	YdPositionCost;
	///占用的保证金
	TFfexFtdcMoneyType	UseMargin;
	///冻结的保证金
	TFfexFtdcMoneyType	FrozenMargin;
	///多头冻结的保证金
	TFfexFtdcMoneyType	LongFrozenMargin;
	///空头冻结的保证金
	TFfexFtdcMoneyType	ShortFrozenMargin;
	///冻结的权利金
	TFfexFtdcMoneyType	FrozenPremium;
	///合约代码
	TFfexFtdcInstrumentIDType	InstrumentID;
	///会员代码
	TFfexFtdcParticipantIDType	ParticipantID;
	///客户代码
	TFfexFtdcClientIDType	ClientID;
};

///合约查询应答
NET_EXPORT_STRUCT CFfexFtdcRspInstrumentField
{
	///结算组代码
	TFfexFtdcSettlementGroupIDType	SettlementGroupID;
	///产品代码
	TFfexFtdcProductIDType	ProductID;
	///产品组代码
	TFfexFtdcProductGroupIDType	ProductGroupID;
	///基础商品代码
	TFfexFtdcInstrumentIDType	UnderlyingInstrID;
	///产品类型
	TFfexFtdcProductClassType	ProductClass;
	///持仓类型
	TFfexFtdcPositionTypeType	PositionType;
	///执行价
	TFfexFtdcPriceType	StrikePrice;
	///期权类型
	TFfexFtdcOptionsTypeType	OptionsType;
	///合约数量乘数
	TFfexFtdcVolumeMultipleType	VolumeMultiple;
	///合约基础商品乘数
	TFfexFtdcUnderlyingMultipleType	UnderlyingMultiple;
	///合约代码
	TFfexFtdcInstrumentIDType	InstrumentID;
	///合约名称
	TFfexFtdcInstrumentNameType	InstrumentName;
	///交割年份
	TFfexFtdcYearType	DeliveryYear;
	///交割月
	TFfexFtdcMonthType	DeliveryMonth;
	///提前月份
	TFfexFtdcAdvanceMonthType	AdvanceMonth;
	///当前是否交易
	TFfexFtdcBoolType	IsTrading;
	///创建日
	TFfexFtdcDateType	CreateDate;
	///上市日
	TFfexFtdcDateType	OpenDate;
	///到期日
	TFfexFtdcDateType	ExpireDate;
	///开始交割日
	TFfexFtdcDateType	StartDelivDate;
	///最后交割日
	TFfexFtdcDateType	EndDelivDate;
	///挂牌基准价
	TFfexFtdcPriceType	BasisPrice;
	///市价单最大下单量
	TFfexFtdcVolumeType	MaxMarketOrderVolume;
	///市价单最小下单量
	TFfexFtdcVolumeType	MinMarketOrderVolume;
	///限价单最大下单量
	TFfexFtdcVolumeType	MaxLimitOrderVolume;
	///限价单最小下单量
	TFfexFtdcVolumeType	MinLimitOrderVolume;
	///最小变动价位
	TFfexFtdcPriceType	PriceTick;
	///交割月自然人开仓
	TFfexFtdcMonthCountType	AllowDelivPersonOpen;
};

///信息查询
NET_EXPORT_STRUCT CFfexFtdcQryInformationField
{
	///起始信息代码
	TFfexFtdcInformationIDType	InformationIDStart;
	///结束信息代码
	TFfexFtdcInformationIDType	InformationIDEnd;
};

///信息查询
NET_EXPORT_STRUCT CFfexFtdcInformationField
{
	///信息编号
	TFfexFtdcInformationIDType	InformationID;
	///序列号
	TFfexFtdcSequenceNoType	SequenceNo;
	///消息正文
	TFfexFtdcContentType	Content;
	///正文长度
	TFfexFtdcContentLengthType	ContentLength;
	///是否完成
	TFfexFtdcBoolType	IsAccomplished;
};

///信用限额
NET_EXPORT_STRUCT CFfexFtdcCreditLimitField
{
	///交易日
	TFfexFtdcDateType	TradingDay;
	///结算组代码
	TFfexFtdcSettlementGroupIDType	SettlementGroupID;
	///结算编号
	TFfexFtdcSettlementIDType	SettlementID;
	///上次结算准备金
	TFfexFtdcMoneyType	PreBalance;
	///当前保证金总额
	TFfexFtdcMoneyType	CurrMargin;
	///平仓盈亏
	TFfexFtdcMoneyType	CloseProfit;
	///期权权利金收支
	TFfexFtdcMoneyType	Premium;
	///入金金额
	TFfexFtdcMoneyType	Deposit;
	///出金金额
	TFfexFtdcMoneyType	Withdraw;
	///期货结算准备金
	TFfexFtdcMoneyType	Balance;
	///可提资金
	TFfexFtdcMoneyType	Available;
	///交易会员编号
	TFfexFtdcParticipantIDType	ParticipantID;
	///结算会员编号
	TFfexFtdcParticipantIDType	ClearingPartID;
	///冻结的保证金
	TFfexFtdcMoneyType	FrozenMargin;
	///冻结的权利金
	TFfexFtdcMoneyType	FrozenPremium;
};

///客户查询应答
NET_EXPORT_STRUCT CFfexFtdcRspClientField
{
	///客户代码
	TFfexFtdcClientIDType	ClientID;
	///客户名称
	TFfexFtdcPartyNameType	ClientName;
	///证件类型
	TFfexFtdcIdCardTypeType	IdentifiedCardType;
	///原证件号码
	TFfexFtdcIdentifiedCardNoV1Type	UseLess;
	///交易角色
	TFfexFtdcTradingRoleType	TradingRole;
	///客户类型
	TFfexFtdcClientTypeType	ClientType;
	///是否活跃
	TFfexFtdcBoolType	IsActive;
	///会员号
	TFfexFtdcParticipantIDType	ParticipantID;
	///证件号码
	TFfexFtdcIdentifiedCardNoType	IdentifiedCardNo;
};

///会员
NET_EXPORT_STRUCT CFfexFtdcParticipantField
{
	///会员代码
	TFfexFtdcParticipantIDType	ParticipantID;
	///会员名称
	TFfexFtdcParticipantNameType	ParticipantName;
	///会员简称
	TFfexFtdcParticipantAbbrType	ParticipantAbbr;
	///会员类型
	TFfexFtdcMemberTypeType	MemberType;
	///是否活跃
	TFfexFtdcBoolType	IsActive;
};

///用户
NET_EXPORT_STRUCT CFfexFtdcUserField
{
	///会员代码
	TFfexFtdcParticipantIDType	ParticipantID;
	///交易用户代码
	TFfexFtdcUserIDType	UserID;
	///交易用户类型
	TFfexFtdcUserTypeType	UserType;
	///密码
	TFfexFtdcPasswordType	Password;
	///交易员权限
	TFfexFtdcUserActiveType	IsActive;
};

///客户
NET_EXPORT_STRUCT CFfexFtdcClientField
{
	///客户代码
	TFfexFtdcClientIDType	ClientID;
	///客户名称
	TFfexFtdcPartyNameType	ClientName;
	///证件类型
	TFfexFtdcIdCardTypeType	IdentifiedCardType;
	///证件号码
	TFfexFtdcIdentifiedCardNoType	IdentifiedCardNo;
	///交易角色
	TFfexFtdcTradingRoleType	TradingRole;
	///客户类型
	TFfexFtdcClientTypeType	ClientType;
	///是否活跃
	TFfexFtdcBoolType	IsActive;
};

///用户会话
NET_EXPORT_STRUCT CFfexFtdcUserSessionField
{
	///前置编号
	TFfexFtdcFrontIDType	FrontID;
	///交易用户代码
	TFfexFtdcUserIDType	UserID;
	///交易用户类型
	TFfexFtdcUserTypeType	UserType;
	///会话编号
	TFfexFtdcSessionIDType	SessionID;
	///登录时间
	TFfexFtdcTimeType	LoginTime;
	///会员代码
	TFfexFtdcParticipantIDType	ParticipantID;
	///IP地址
	TFfexFtdcIPAddressType	IPAddress;
	///用户端产品信息
	TFfexFtdcProductInfoType	UserProductInfo;
	///接口端产品信息
	TFfexFtdcProductInfoType	InterfaceProductInfo;
	///协议信息
	TFfexFtdcProtocolInfoType	ProtocolInfo;
};

///产品组
NET_EXPORT_STRUCT CFfexFtdcProductGroupField
{
	///结算组代码
	TFfexFtdcSettlementGroupIDType	SettlementGroupID;
	///产品组代码
	TFfexFtdcProductGroupIDType	ProductGroupID;
	///产品组名称
	TFfexFtdcProductGroupNameType	ProductGroupName;
	///商品代码
	TFfexFtdcCommodityIDType	CommodityID;
};

///产品
NET_EXPORT_STRUCT CFfexFtdcProductField
{
	///结算组代码
	TFfexFtdcSettlementGroupIDType	SettlementGroupID;
	///产品代码
	TFfexFtdcProductIDType	ProductID;
	///产品组代码
	TFfexFtdcProductGroupIDType	ProductGroupID;
	///产品名称
	TFfexFtdcProductNameType	ProductName;
	///产品类型
	TFfexFtdcProductClassType	ProductClass;
};

///合约
NET_EXPORT_STRUCT CFfexFtdcInstrumentField
{
	///结算组代码
	TFfexFtdcSettlementGroupIDType	SettlementGroupID;
	///产品代码
	TFfexFtdcProductIDType	ProductID;
	///产品组代码
	TFfexFtdcProductGroupIDType	ProductGroupID;
	///基础商品代码
	TFfexFtdcInstrumentIDType	UnderlyingInstrID;
	///产品类型
	TFfexFtdcProductClassType	ProductClass;
	///持仓类型
	TFfexFtdcPositionTypeType	PositionType;
	///执行价
	TFfexFtdcPriceType	StrikePrice;
	///期权类型
	TFfexFtdcOptionsTypeType	OptionsType;
	///合约数量乘数
	TFfexFtdcVolumeMultipleType	VolumeMultiple;
	///合约基础商品乘数
	TFfexFtdcUnderlyingMultipleType	UnderlyingMultiple;
	///合约代码
	TFfexFtdcInstrumentIDType	InstrumentID;
	///合约名称
	TFfexFtdcInstrumentNameType	InstrumentName;
	///交割年份
	TFfexFtdcYearType	DeliveryYear;
	///交割月
	TFfexFtdcMonthType	DeliveryMonth;
	///提前月份
	TFfexFtdcAdvanceMonthType	AdvanceMonth;
	///当前是否交易
	TFfexFtdcBoolType	IsTrading;
};

///组合交易合约的单腿
NET_EXPORT_STRUCT CFfexFtdcCombinationLegField
{
	///结算组代码
	TFfexFtdcSettlementGroupIDType	SettlementGroupID;
	///组合合约代码
	TFfexFtdcInstrumentIDType	CombInstrumentID;
	///单腿编号
	TFfexFtdcLegIDType	LegID;
	///单腿合约代码
	TFfexFtdcInstrumentIDType	LegInstrumentID;
	///买卖方向
	TFfexFtdcDirectionType	Direction;
	///单腿乘数
	TFfexFtdcLegMultipleType	LegMultiple;
	///推导层数
	TFfexFtdcImplyLevelType	ImplyLevel;
};

///账户资金信息
NET_EXPORT_STRUCT CFfexFtdcAccountInfoField
{
	///交易日
	TFfexFtdcDateType	TradingDay;
	///结算组代码
	TFfexFtdcSettlementGroupIDType	SettlementGroupID;
	///结算编号
	TFfexFtdcSettlementIDType	SettlementID;
	///上次结算准备金
	TFfexFtdcMoneyType	PreBalance;
	///当前保证金总额
	TFfexFtdcMoneyType	CurrMargin;
	///平仓盈亏
	TFfexFtdcMoneyType	CloseProfit;
	///期权权利金收支
	TFfexFtdcMoneyType	Premium;
	///入金金额
	TFfexFtdcMoneyType	Deposit;
	///出金金额
	TFfexFtdcMoneyType	Withdraw;
	///期货结算准备金
	TFfexFtdcMoneyType	Balance;
	///可提资金
	TFfexFtdcMoneyType	Available;
	///开户日期
	TFfexFtdcDateType	DateAccountOpen;
	///上次结算的日期
	TFfexFtdcDateType	PreDate;
	///上结算的编号
	TFfexFtdcSettlementIDType	PreSettlementID;
	///上次保证金总额
	TFfexFtdcMoneyType	PreMargin;
	///期货保证金
	TFfexFtdcMoneyType	FuturesMargin;
	///期权保证金
	TFfexFtdcMoneyType	OptionsMargin;
	///持仓盈亏
	TFfexFtdcMoneyType	PositionProfit;
	///当日盈亏
	TFfexFtdcMoneyType	Profit;
	///利息收入
	TFfexFtdcMoneyType	Interest;
	///手续费
	TFfexFtdcMoneyType	Fee;
	///总质押金额
	TFfexFtdcMoneyType	TotalCollateral;
	///用质押抵的保证金金额
	TFfexFtdcMoneyType	CollateralForMargin;
	///上次资金利息积数
	TFfexFtdcMoneyType	PreAccmulateInterest;
	///资金利息积数
	TFfexFtdcMoneyType	AccumulateInterest;
	///质押手续费积数
	TFfexFtdcMoneyType	AccumulateFee;
	///冻结资金
	TFfexFtdcMoneyType	ForzenDeposit;
	///帐户状态
	TFfexFtdcAccountStatusType	AccountStatus;
	///资金帐号
	TFfexFtdcAccountIDType	AccountID;
};

///会员合约持仓
NET_EXPORT_STRUCT CFfexFtdcPartPositionField
{
	///交易日
	TFfexFtdcDateType	TradingDay;
	///结算组代码
	TFfexFtdcSettlementGroupIDType	SettlementGroupID;
	///结算编号
	TFfexFtdcSettlementIDType	SettlementID;
	///投机套保标志
	TFfexFtdcHedgeFlagType	HedgeFlag;
	///持仓多空方向
	TFfexFtdcPosiDirectionType	PosiDirection;
	///上日持仓
	TFfexFtdcVolumeType	YdPosition;
	///今日持仓
	TFfexFtdcVolumeType	Position;
	///多头冻结
	TFfexFtdcVolumeType	LongFrozen;
	///空头冻结
	TFfexFtdcVolumeType	ShortFrozen;
	///昨日多头冻结
	TFfexFtdcVolumeType	YdLongFrozen;
	///昨日空头冻结
	TFfexFtdcVolumeType	YdShortFrozen;
	///合约代码
	TFfexFtdcInstrumentIDType	InstrumentID;
	///会员代码
	TFfexFtdcParticipantIDType	ParticipantID;
	///交易角色
	TFfexFtdcTradingRoleType	TradingRole;
};

///客户合约持仓
NET_EXPORT_STRUCT CFfexFtdcClientPositionField
{
	///交易日
	TFfexFtdcDateType	TradingDay;
	///结算组代码
	TFfexFtdcSettlementGroupIDType	SettlementGroupID;
	///结算编号
	TFfexFtdcSettlementIDType	SettlementID;
	///投机套保标志
	TFfexFtdcHedgeFlagType	HedgeFlag;
	///持仓多空方向
	TFfexFtdcPosiDirectionType	PosiDirection;
	///上日持仓
	TFfexFtdcVolumeType	YdPosition;
	///今日持仓
	TFfexFtdcVolumeType	Position;
	///多头冻结
	TFfexFtdcVolumeType	LongFrozen;
	///空头冻结
	TFfexFtdcVolumeType	ShortFrozen;
	///昨日多头冻结
	TFfexFtdcVolumeType	YdLongFrozen;
	///昨日空头冻结
	TFfexFtdcVolumeType	YdShortFrozen;
	///当日买成交量
	TFfexFtdcVolumeType	BuyTradeVolume;
	///当日卖成交量
	TFfexFtdcVolumeType	SellTradeVolume;
	///持仓成本
	TFfexFtdcMoneyType	PositionCost;
	///昨日持仓成本
	TFfexFtdcMoneyType	YdPositionCost;
	///占用的保证金
	TFfexFtdcMoneyType	UseMargin;
	///冻结的保证金
	TFfexFtdcMoneyType	FrozenMargin;
	///多头冻结的保证金
	TFfexFtdcMoneyType	LongFrozenMargin;
	///空头冻结的保证金
	TFfexFtdcMoneyType	ShortFrozenMargin;
	///冻结的权利金
	TFfexFtdcMoneyType	FrozenPremium;
	///合约代码
	TFfexFtdcInstrumentIDType	InstrumentID;
	///会员代码
	TFfexFtdcParticipantIDType	ParticipantID;
	///客户代码
	TFfexFtdcClientIDType	ClientID;
};

///保值额度量
NET_EXPORT_STRUCT CFfexFtdcHedgeVolumeField
{
	///交易日
	TFfexFtdcDateType	TradingDay;
	///结算组代码
	TFfexFtdcSettlementGroupIDType	SettlementGroupID;
	///结算编号
	TFfexFtdcSettlementIDType	SettlementID;
	///会员代码
	TFfexFtdcParticipantIDType	ParticipantID;
	///客户代码
	TFfexFtdcClientIDType	ClientID;
	///合约代码
	TFfexFtdcInstrumentIDType	InstrumentID;
	///多头保值额度最初申请量
	TFfexFtdcVolumeType	LongVolumeOriginal;
	///空头保值额度最初申请量
	TFfexFtdcVolumeType	ShortVolumeOriginal;
	///多头保值额度
	TFfexFtdcVolumeType	LongVolume;
	///空头保值额度
	TFfexFtdcVolumeType	ShortVolume;
};

///市场行情
NET_EXPORT_STRUCT CFfexFtdcMarketDataField
{
	///交易日
	TFfexFtdcDateType	TradingDay;
	///结算组代码
	TFfexFtdcSettlementGroupIDType	SettlementGroupID;
	///结算编号
	TFfexFtdcSettlementIDType	SettlementID;
	///最新价
	TFfexFtdcPriceType	LastPrice;
	///昨结算
	TFfexFtdcPriceType	PreSettlementPrice;
	///昨收盘
	TFfexFtdcPriceType	PreClosePrice;
	///昨持仓量
	TFfexFtdcLargeVolumeType	PreOpenInterest;
	///今开盘
	TFfexFtdcPriceType	OpenPrice;
	///最高价
	TFfexFtdcPriceType	HighestPrice;
	///最低价
	TFfexFtdcPriceType	LowestPrice;
	///数量
	TFfexFtdcVolumeType	Volume;
	///成交金额
	TFfexFtdcMoneyType	Turnover;
	///持仓量
	TFfexFtdcLargeVolumeType	OpenInterest;
	///今收盘
	TFfexFtdcPriceType	ClosePrice;
	///今结算
	TFfexFtdcPriceType	SettlementPrice;
	///涨停板价
	TFfexFtdcPriceType	UpperLimitPrice;
	///跌停板价
	TFfexFtdcPriceType	LowerLimitPrice;
	///昨虚实度
	TFfexFtdcRatioType	PreDelta;
	///今虚实度
	TFfexFtdcRatioType	CurrDelta;
	///最后修改时间
	TFfexFtdcTimeType	UpdateTime;
	///最后修改毫秒
	TFfexFtdcMillisecType	UpdateMillisec;
	///合约代码
	TFfexFtdcInstrumentIDType	InstrumentID;
};

///深度市场行情
 NET_EXPORT_STRUCT CFfexFtdcDepthMarketDataField
{
	///交易日
	TFfexFtdcDateType	TradingDay;
	///结算组代码
	TFfexFtdcSettlementGroupIDType	SettlementGroupID;
	///结算编号
	TFfexFtdcSettlementIDType	SettlementID;
	///最新价
	TFfexFtdcPriceType	LastPrice;
	///昨结算
	TFfexFtdcPriceType	PreSettlementPrice;
	///昨收盘
	TFfexFtdcPriceType	PreClosePrice;
	///昨持仓量
	TFfexFtdcLargeVolumeType	PreOpenInterest;
	///今开盘
	TFfexFtdcPriceType	OpenPrice;
	///最高价
	TFfexFtdcPriceType	HighestPrice;
	///最低价
	TFfexFtdcPriceType	LowestPrice;
	///数量
	TFfexFtdcVolumeType	Volume;
	///成交金额
	TFfexFtdcMoneyType	Turnover;
	///持仓量
	TFfexFtdcLargeVolumeType	OpenInterest;
	///今收盘
	TFfexFtdcPriceType	ClosePrice;
	///今结算
	TFfexFtdcPriceType	SettlementPrice;
	///涨停板价
	TFfexFtdcPriceType	UpperLimitPrice;
	///跌停板价
	TFfexFtdcPriceType	LowerLimitPrice;
	///昨虚实度
	TFfexFtdcRatioType	PreDelta;
	///今虚实度
	TFfexFtdcRatioType	CurrDelta;
	///最后修改时间
	TFfexFtdcTimeType	UpdateTime;
	///最后修改毫秒
	TFfexFtdcMillisecType	UpdateMillisec;
	///合约代码
	TFfexFtdcInstrumentIDType	InstrumentID;
	///申买价一
	TFfexFtdcPriceType	BidPrice1;
	///申买量一
	TFfexFtdcVolumeType	BidVolume1;
	///申卖价一
	TFfexFtdcPriceType	AskPrice1;
	///申卖量一
	TFfexFtdcVolumeType	AskVolume1;
	///申买价二
	TFfexFtdcPriceType	BidPrice2;
	///申买量二
	TFfexFtdcVolumeType	BidVolume2;
	///申卖价二
	TFfexFtdcPriceType	AskPrice2;
	///申卖量二
	TFfexFtdcVolumeType	AskVolume2;
	///申买价三
	TFfexFtdcPriceType	BidPrice3;
	///申买量三
	TFfexFtdcVolumeType	BidVolume3;
	///申卖价三
	TFfexFtdcPriceType	AskPrice3;
	///申卖量三
	TFfexFtdcVolumeType	AskVolume3;
	///申买价四
	TFfexFtdcPriceType	BidPrice4;
	///申买量四
	TFfexFtdcVolumeType	BidVolume4;
	///申卖价四
	TFfexFtdcPriceType	AskPrice4;
	///申卖量四
	TFfexFtdcVolumeType	AskVolume4;
	///申买价五
	TFfexFtdcPriceType	BidPrice5;
	///申买量五
	TFfexFtdcVolumeType	BidVolume5;
	///申卖价五
	TFfexFtdcPriceType	AskPrice5;
	///申卖量五
	TFfexFtdcVolumeType	AskVolume5;
};

///分价表
NET_EXPORT_STRUCT CFfexFtdcMBLMarketDataField
{
	///合约代码
	TFfexFtdcInstrumentIDType	InstrumentID;
	///买卖方向
	TFfexFtdcDirectionType	Direction;
	///价格
	TFfexFtdcPriceType	Price;
	///数量
	TFfexFtdcVolumeType	Volume;
};

///别名定义
NET_EXPORT_STRUCT CFfexFtdcAliasDefineField
{
	///起始位置
	TFfexFtdcStartPosType	StartPos;
	///别名
	TFfexFtdcAliasType	Alias;
	///原文
	TFfexFtdcOriginalTextType	OriginalText;
};

///行情基础属性
NET_EXPORT_STRUCT CFfexFtdcMarketDataBaseField
{
	///交易日
	TFfexFtdcDateType	TradingDay;
	///结算组代码
	TFfexFtdcSettlementGroupIDType	SettlementGroupID;
	///结算编号
	TFfexFtdcSettlementIDType	SettlementID;
	///昨结算
	TFfexFtdcPriceType	PreSettlementPrice;
	///昨收盘
	TFfexFtdcPriceType	PreClosePrice;
	///昨持仓量
	TFfexFtdcLargeVolumeType	PreOpenInterest;
	///昨虚实度
	TFfexFtdcRatioType	PreDelta;
};

///行情静态属性
NET_EXPORT_STRUCT CFfexFtdcMarketDataStaticField
{
	///今开盘
	TFfexFtdcPriceType	OpenPrice;
	///最高价
	TFfexFtdcPriceType	HighestPrice;
	///最低价
	TFfexFtdcPriceType	LowestPrice;
	///今收盘
	TFfexFtdcPriceType	ClosePrice;
	///涨停板价
	TFfexFtdcPriceType	UpperLimitPrice;
	///跌停板价
	TFfexFtdcPriceType	LowerLimitPrice;
	///今结算
	TFfexFtdcPriceType	SettlementPrice;
	///今虚实度
	TFfexFtdcRatioType	CurrDelta;
};

///行情最新成交属性
NET_EXPORT_STRUCT CFfexFtdcMarketDataLastMatchField
{
	///最新价
	TFfexFtdcPriceType	LastPrice;
	///数量
	TFfexFtdcVolumeType	Volume;
	///成交金额
	TFfexFtdcMoneyType	Turnover;
	///持仓量
	TFfexFtdcLargeVolumeType	OpenInterest;
};

///行情最优价属性
NET_EXPORT_STRUCT CFfexFtdcMarketDataBestPriceField
{
	///申买价一
	TFfexFtdcPriceType	BidPrice1;
	///申买量一
	TFfexFtdcVolumeType	BidVolume1;
	///申卖价一
	TFfexFtdcPriceType	AskPrice1;
	///申卖量一
	TFfexFtdcVolumeType	AskVolume1;
};

///行情申买二、三属性
NET_EXPORT_STRUCT CFfexFtdcMarketDataBid23Field
{
	///申买价二
	TFfexFtdcPriceType	BidPrice2;
	///申买量二
	TFfexFtdcVolumeType	BidVolume2;
	///申买价三
	TFfexFtdcPriceType	BidPrice3;
	///申买量三
	TFfexFtdcVolumeType	BidVolume3;
};

///行情申卖二、三属性
NET_EXPORT_STRUCT CFfexFtdcMarketDataAsk23Field
{
	///申卖价二
	TFfexFtdcPriceType	AskPrice2;
	///申卖量二
	TFfexFtdcVolumeType	AskVolume2;
	///申卖价三
	TFfexFtdcPriceType	AskPrice3;
	///申卖量三
	TFfexFtdcVolumeType	AskVolume3;
};

///行情申买四、五属性
NET_EXPORT_STRUCT CFfexFtdcMarketDataBid45Field
{
	///申买价四
	TFfexFtdcPriceType	BidPrice4;
	///申买量四
	TFfexFtdcVolumeType	BidVolume4;
	///申买价五
	TFfexFtdcPriceType	BidPrice5;
	///申买量五
	TFfexFtdcVolumeType	BidVolume5;
};

///行情申卖四、五属性
NET_EXPORT_STRUCT CFfexFtdcMarketDataAsk45Field
{
	///申卖价四
	TFfexFtdcPriceType	AskPrice4;
	///申卖量四
	TFfexFtdcVolumeType	AskVolume4;
	///申卖价五
	TFfexFtdcPriceType	AskPrice5;
	///申卖量五
	TFfexFtdcVolumeType	AskVolume5;
};

///行情更新时间属性
NET_EXPORT_STRUCT CFfexFtdcMarketDataUpdateTimeField
{
	///合约代码
	TFfexFtdcInstrumentIDType	InstrumentID;
	///最后修改时间
	TFfexFtdcTimeType	UpdateTime;
	///最后修改毫秒
	TFfexFtdcMillisecType	UpdateMillisec;
};

///报价
NET_EXPORT_STRUCT CFfexFtdcQuoteField
{
	///交易日
	TFfexFtdcDateType	TradingDay;
	///结算组代码
	TFfexFtdcSettlementGroupIDType	SettlementGroupID;
	///结算编号
	TFfexFtdcSettlementIDType	SettlementID;
	///报价编号
	TFfexFtdcQuoteSysIDType	QuoteSysID;
	///会员代码
	TFfexFtdcParticipantIDType	ParticipantID;
	///客户代码
	TFfexFtdcClientIDType	ClientID;
	///交易用户代码
	TFfexFtdcUserIDType	UserID;
	///数量
	TFfexFtdcVolumeType	Volume;
	///合约代码
	TFfexFtdcInstrumentIDType	InstrumentID;
	///本地报价编号
	TFfexFtdcOrderLocalIDType	QuoteLocalID;
	///业务单元
	TFfexFtdcBusinessUnitType	BusinessUnit;
	///买方组合开平标志
	TFfexFtdcCombOffsetFlagType	BidCombOffsetFlag;
	///买方组合套保标志
	TFfexFtdcCombHedgeFlagType	BidCombHedgeFlag;
	///买方价格
	TFfexFtdcPriceType	BidPrice;
	///卖方组合开平标志
	TFfexFtdcCombOffsetFlagType	AskCombOffsetFlag;
	///卖方组合套保标志
	TFfexFtdcCombHedgeFlagType	AskCombHedgeFlag;
	///卖方价格
	TFfexFtdcPriceType	AskPrice;
	///插入时间
	TFfexFtdcTimeType	InsertTime;
	///撤销时间
	TFfexFtdcTimeType	CancelTime;
	///成交时间
	TFfexFtdcTimeType	TradeTime;
	///买方报单编号
	TFfexFtdcOrderSysIDType	BidOrderSysID;
	///卖方报单编号
	TFfexFtdcOrderSysIDType	AskOrderSysID;
	///结算会员编号
	TFfexFtdcParticipantIDType	ClearingPartID;
};

///成交
NET_EXPORT_STRUCT CFfexFtdcTradeField
{
	///交易日
	TFfexFtdcDateType	TradingDay;
	///结算组代码
	TFfexFtdcSettlementGroupIDType	SettlementGroupID;
	///结算编号
	TFfexFtdcSettlementIDType	SettlementID;
	///成交编号
	TFfexFtdcTradeIDType	TradeID;
	///买卖方向
	TFfexFtdcDirectionType	Direction;
	///报单编号
	TFfexFtdcOrderSysIDType	OrderSysID;
	///会员代码
	TFfexFtdcParticipantIDType	ParticipantID;
	///客户代码
	TFfexFtdcClientIDType	ClientID;
	///交易角色
	TFfexFtdcTradingRoleType	TradingRole;
	///资金帐号
	TFfexFtdcAccountIDType	AccountID;
	///合约代码
	TFfexFtdcInstrumentIDType	InstrumentID;
	///开平标志
	TFfexFtdcOffsetFlagType	OffsetFlag;
	///投机套保标志
	TFfexFtdcHedgeFlagType	HedgeFlag;
	///价格
	TFfexFtdcPriceType	Price;
	///数量
	TFfexFtdcVolumeType	Volume;
	///成交时间
	TFfexFtdcTimeType	TradeTime;
	///成交类型
	TFfexFtdcTradeTypeType	TradeType;
	///成交价来源
	TFfexFtdcPriceSourceType	PriceSource;
	///交易用户代码
	TFfexFtdcUserIDType	UserID;
	///本地报单编号
	TFfexFtdcOrderLocalIDType	OrderLocalID;
	///结算会员编号
	TFfexFtdcParticipantIDType	ClearingPartID;
	///业务单元
	TFfexFtdcBusinessUnitType	BusinessUnit;
};

///报单
NET_EXPORT_STRUCT CFfexFtdcOrderField
{
	///交易日
	TFfexFtdcDateType	TradingDay;
	///结算组代码
	TFfexFtdcSettlementGroupIDType	SettlementGroupID;
	///结算编号
	TFfexFtdcSettlementIDType	SettlementID;
	///报单编号
	TFfexFtdcOrderSysIDType	OrderSysID;
	///会员代码
	TFfexFtdcParticipantIDType	ParticipantID;
	///客户代码
	TFfexFtdcClientIDType	ClientID;
	///交易用户代码
	TFfexFtdcUserIDType	UserID;
	///合约代码
	TFfexFtdcInstrumentIDType	InstrumentID;
	///报单价格条件
	TFfexFtdcOrderPriceTypeType	OrderPriceType;
	///买卖方向
	TFfexFtdcDirectionType	Direction;
	///组合开平标志
	TFfexFtdcCombOffsetFlagType	CombOffsetFlag;
	///组合投机套保标志
	TFfexFtdcCombHedgeFlagType	CombHedgeFlag;
	///价格
	TFfexFtdcPriceType	LimitPrice;
	///数量
	TFfexFtdcVolumeType	VolumeTotalOriginal;
	///有效期类型
	TFfexFtdcTimeConditionType	TimeCondition;
	///GTD日期
	TFfexFtdcDateType	GTDDate;
	///成交量类型
	TFfexFtdcVolumeConditionType	VolumeCondition;
	///最小成交量
	TFfexFtdcVolumeType	MinVolume;
	///触发条件
	TFfexFtdcContingentConditionType	ContingentCondition;
	///止损价
	TFfexFtdcPriceType	StopPrice;
	///强平原因
	TFfexFtdcForceCloseReasonType	ForceCloseReason;
	///本地报单编号
	TFfexFtdcOrderLocalIDType	OrderLocalID;
	///自动挂起标志
	TFfexFtdcBoolType	IsAutoSuspend;
	///报单来源
	TFfexFtdcOrderSourceType	OrderSource;
	///报单状态
	TFfexFtdcOrderStatusType	OrderStatus;
	///报单类型
	TFfexFtdcOrderTypeType	OrderType;
	///今成交数量
	TFfexFtdcVolumeType	VolumeTraded;
	///剩余数量
	TFfexFtdcVolumeType	VolumeTotal;
	///报单日期
	TFfexFtdcDateType	InsertDate;
	///插入时间
	TFfexFtdcTimeType	InsertTime;
	///激活时间
	TFfexFtdcTimeType	ActiveTime;
	///挂起时间
	TFfexFtdcTimeType	SuspendTime;
	///最后修改时间
	TFfexFtdcTimeType	UpdateTime;
	///撤销时间
	TFfexFtdcTimeType	CancelTime;
	///最后修改交易用户代码
	TFfexFtdcUserIDType	ActiveUserID;
	///优先权
	TFfexFtdcPriorityType	Priority;
	///按时间排队的序号
	TFfexFtdcTimeSortIDType	TimeSortID;
	///结算会员编号
	TFfexFtdcParticipantIDType	ClearingPartID;
	///业务单元
	TFfexFtdcBusinessUnitType	BusinessUnit;
};

///执行宣告
NET_EXPORT_STRUCT CFfexFtdcExecOrderField
{
	///交易日
	TFfexFtdcDateType	TradingDay;
	///结算组代码
	TFfexFtdcSettlementGroupIDType	SettlementGroupID;
	///结算编号
	TFfexFtdcSettlementIDType	SettlementID;
	///合约编号
	TFfexFtdcInstrumentIDType	InstrumentID;
	///会员代码
	TFfexFtdcParticipantIDType	ParticipantID;
	///客户代码
	TFfexFtdcClientIDType	ClientID;
	///交易用户代码
	TFfexFtdcUserIDType	UserID;
	///本地执行宣告编号
	TFfexFtdcOrderLocalIDType	ExecOrderLocalID;
	///数量
	TFfexFtdcVolumeType	Volume;
	///业务单元
	TFfexFtdcBusinessUnitType	BusinessUnit;
	///执行宣告编号
	TFfexFtdcExecOrderSysIDType	ExecOrderSysID;
	///报单日期
	TFfexFtdcDateType	InsertDate;
	///插入时间
	TFfexFtdcTimeType	InsertTime;
	///撤销时间
	TFfexFtdcTimeType	CancelTime;
	///执行结果
	TFfexFtdcExecResultType	ExecResult;
	///结算会员编号
	TFfexFtdcParticipantIDType	ClearingPartID;
};

///组合报单
NET_EXPORT_STRUCT CFfexFtdcCombOrderField
{
	///交易日
	TFfexFtdcDateType	TradingDay;
	///结算组代码
	TFfexFtdcSettlementGroupIDType	SettlementGroupID;
	///结算编号
	TFfexFtdcSettlementIDType	SettlementID;
	///组合报单编号
	TFfexFtdcOrderSysIDType	CombOrderSysID;
	///会员代码
	TFfexFtdcParticipantIDType	ParticipantID;
	///客户代码
	TFfexFtdcClientIDType	ClientID;
	///交易用户代码
	TFfexFtdcUserIDType	UserID;
	///价格
	TFfexFtdcPriceType	LimitPrice;
	///数量
	TFfexFtdcVolumeType	VolumeTotalOriginal;
	///本地报单编号
	TFfexFtdcOrderLocalIDType	CombOrderLocalID;
	///业务单元
	TFfexFtdcBusinessUnitType	BusinessUnit;
	///合约代码1
	TFfexFtdcInstrumentIDType	InstrumentID1;
	///买卖方向1
	TFfexFtdcDirectionType	Direction1;
	///分腿乘数1
	TFfexFtdcLegMultipleType	LegMultiple1;
	///开平标志1
	TFfexFtdcOffsetFlagType	OffsetFlag1;
	///投机套保标志1
	TFfexFtdcHedgeFlagType	HedgeFlag1;
	///合约代码2
	TFfexFtdcInstrumentIDType	InstrumentID2;
	///买卖方向2
	TFfexFtdcDirectionType	Direction2;
	///分腿乘数2
	TFfexFtdcLegMultipleType	LegMultiple2;
	///开平标志2
	TFfexFtdcOffsetFlagType	OffsetFlag2;
	///投机套保标志2
	TFfexFtdcHedgeFlagType	HedgeFlag2;
	///合约代码3
	TFfexFtdcInstrumentIDType	InstrumentID3;
	///买卖方向3
	TFfexFtdcDirectionType	Direction3;
	///分腿乘数3
	TFfexFtdcLegMultipleType	LegMultiple3;
	///开平标志3
	TFfexFtdcOffsetFlagType	OffsetFlag3;
	///投机套保标志3
	TFfexFtdcHedgeFlagType	HedgeFlag3;
	///合约代码4
	TFfexFtdcInstrumentIDType	InstrumentID4;
	///买卖方向4
	TFfexFtdcDirectionType	Direction4;
	///分腿乘数4
	TFfexFtdcLegMultipleType	LegMultiple4;
	///开平标志4
	TFfexFtdcOffsetFlagType	OffsetFlag4;
	///投机套保标志4
	TFfexFtdcHedgeFlagType	HedgeFlag4;
	///报单来源
	TFfexFtdcOrderSourceType	OrderSource;
	///今成交数量
	TFfexFtdcVolumeType	VolumeTraded;
	///剩余数量
	TFfexFtdcVolumeType	VolumeTotal;
	///报单日期
	TFfexFtdcDateType	InsertDate;
	///插入时间
	TFfexFtdcTimeType	InsertTime;
	///结算会员编号
	TFfexFtdcParticipantIDType	ClearingPartID;
};

///管理报单
NET_EXPORT_STRUCT CFfexFtdcAdminOrderField
{
	///合约代码
	TFfexFtdcInstrumentIDType	InstrumentID;
	///管理报单命令
	TFfexFtdcAdminOrderCommandFlagType	AdminOrderCommand;
	///结算会员编号
	TFfexFtdcParticipantIDType	ClearingPartID;
	///交易会员编号
	TFfexFtdcParticipantIDType	ParticipantID;
	///金额
	TFfexFtdcMoneyType	Amount;
	///结算组代码
	TFfexFtdcSettlementGroupIDType	SettlementGroupID;
};

///管理报单输入
NET_EXPORT_STRUCT CFfexFtdcInputAdminOrderField
{
	///合约代码
	TFfexFtdcInstrumentIDType	InstrumentID;
	///管理报单命令
	TFfexFtdcAdminOrderCommandFlagType	AdminOrderCommand;
	///结算会员编号
	TFfexFtdcParticipantIDType	ClearingPartID;
	///交易会员编号
	TFfexFtdcParticipantIDType	ParticipantID;
	///金额
	TFfexFtdcMoneyType	Amount;
	///结算组代码
	TFfexFtdcSettlementGroupIDType	SettlementGroupID;
};

///公告
NET_EXPORT_STRUCT CFfexFtdcBulletinField
{
	///交易日
	TFfexFtdcDateType	TradingDay;
	///公告编号
	TFfexFtdcBulletinIDType	BulletinID;
	///序列号
	TFfexFtdcSequenceNoType	SequenceNo;
	///公告类型
	TFfexFtdcNewsTypeType	NewsType;
	///紧急程度
	TFfexFtdcNewsUrgencyType	NewsUrgency;
	///发送时间
	TFfexFtdcTimeType	SendTime;
	///消息摘要
	TFfexFtdcAbstractType	Abstract;
	///消息来源
	TFfexFtdcComeFromType	ComeFrom;
	///消息正文
	TFfexFtdcContentType	Content;
	///WEB地址
	TFfexFtdcURLLinkType	URLLink;
	///市场代码
	TFfexFtdcMarketIDType	MarketID;
};

///交易所数据同步状态
NET_EXPORT_STRUCT CFfexFtdcExchangeDataSyncStatusField
{
	///交易日
	TFfexFtdcDateType	TradingDay;
	///交易所代码
	TFfexFtdcExchangeIDType	ExchangeID;
	///交易所数据同步状态
	TFfexFtdcExchangeDataSyncStatusType	ExchangeDataSyncStatus;
};

///结算组数据同步状态
NET_EXPORT_STRUCT CFfexFtdcSGDataSyncStatusField
{
	///结算组代码
	TFfexFtdcSettlementGroupIDType	SettlementGroupID;
	///交易日
	TFfexFtdcDateType	TradingDay;
	///结算编号
	TFfexFtdcSettlementIDType	SettlementID;
	///结算组数据同步状态
	TFfexFtdcSGDataSyncStatusType	SGDataSyncStatus;
};

///合约状态
NET_EXPORT_STRUCT CFfexFtdcInstrumentStatusField
{
	///结算组代码
	TFfexFtdcSettlementGroupIDType	SettlementGroupID;
	///合约代码
	TFfexFtdcInstrumentIDType	InstrumentID;
	///合约交易状态
	TFfexFtdcInstrumentStatusType	InstrumentStatus;
	///交易阶段编号
	TFfexFtdcTradingSegmentSNType	TradingSegmentSN;
	///进入本状态时间
	TFfexFtdcTimeType	EnterTime;
	///进入本状态原因
	TFfexFtdcInstStatusEnterReasonType	EnterReason;
};

///客户持仓查询
NET_EXPORT_STRUCT CFfexFtdcQryClientPositionV1Field
{
	///起始会员代码
	TFfexFtdcParticipantIDType	PartIDStart;
	///结束会员代码
	TFfexFtdcParticipantIDType	PartIDEnd;
	///起始客户代码
	TFfexFtdcClientIDType	ClientIDStart;
	///结束客户代码
	TFfexFtdcClientIDType	ClientIDEnd;
	///起始合约代码
	TFfexFtdcInstrumentIDType	InstIDStart;
	///结束合约代码
	TFfexFtdcInstrumentIDType	InstIDEnd;
};

};

#endif