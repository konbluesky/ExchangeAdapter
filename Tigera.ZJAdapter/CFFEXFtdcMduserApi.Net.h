#ifndef CFFEXFTDCMDUSERAPI_NET_H
#define CFFEXFTDCMDUSERAPI_NET_H
//注：这个是rmmsoft对应的.net版本定义
//我们的开发人员需要实现ZJMDAdapter里OnXXXX函数的具体内容

/////////////////////////////////////////////////////////////////////////
///@system 新一代交易所系统
///@company 上海期货信息技术有限公司
///@file CFFEXFtdcMduserApi.h
///@brief 定义了客户端接口
///@history 
///20060106	赵鸿昊		创建该文件
/////////////////////////////////////////////////////////////////////////

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "ZJAdapter.h"
#include "CFFEXFtdcUserApiStruct.Net.h"
#include "StringUtils.h"
#include "gcroot.h"

namespace ZJAdapter
{


class Interop_Derived_CFfexFtdcMduserSpi;

//深度市场数据获取接口
public ref class ZJMDAdapter
{
public:
	void WARNINGMSG(System::String^ info) 
	{
		System::Console::WriteLine("if the actual function not be called,check whether override key word set at {0}",info);
	}
	///当客户端与交易后台建立起通信连接时（还未登录前），该方法被调用。
	virtual void OnFrontConnected()
	{
		WARNINGMSG("OnFrontConnected");
	}
	///当客户端与交易后台通信连接断开时，该方法被调用。当发生这个情况后，API会自动重新连接，客户端可不做处理。
	///@param nReason 错误原因
	///        0x1001 网络读失败
	///        0x1002 网络写失败
	///        0x2001 接收心跳超时
	///        0x2002 发送心跳失败
	///        0x2003 收到错误报文
	virtual void OnFrontDisconnected(int nReason)
	{
		WARNINGMSG("OnFrontDisconnected");
	}		
	///心跳超时警告。当长时间未收到报文时，该方法被调用。
	///@param nTimeLapse 距离上次接收报文的时间
	virtual void OnHeartBeatWarning(int nTimeLapse)
	{
		WARNINGMSG("OnHeartBeatWarning");
	}
	///错误应答
	virtual void OnRspError(ZJAdapter::CFfexFtdcRspInfoField pRspInfo, int nRequestID, bool bIsLast) 
	{
		WARNINGMSG("OnRspError");
	}
	///用户登录应答
	virtual void OnRspUserLogin(ZJAdapter::CFfexFtdcRspUserLoginField pRspUserLogin,ZJAdapter:: CFfexFtdcRspInfoField pRspInfo, int nRequestID, bool bIsLast) 
	{
		WARNINGMSG("OnRspUserLogin");
	}
	///用户退出应答
	virtual void OnRspUserLogout(ZJAdapter::CFfexFtdcRspUserLogoutField pRspUserLogout,ZJAdapter:: CFfexFtdcRspInfoField pRspInfo, int nRequestID, bool bIsLast) 
	{
		WARNINGMSG("OnRspUserLogout");
	}
	///深度行情通知
	virtual void OnRtnDepthMarketData(ZJAdapter::CFfexFtdcDepthMarketDataField pDepthMarketData) 
	{
		WARNINGMSG("OnRtnDepthMarketData");
	}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




public:
	///创建MduserApi
	///@param pszFlowPath 存贮订阅信息文件的目录，默认为当前目录
	///@return 创建出的UserApi
	//static CFfexFtdcMduserApi *CreateFtdcMduserApi(const char *pszFlowPath = "");
	ZJMDAdapter(System::String^ pszFlowPath);

public:
	///获取系统版本号
	///@param nMajorVersion 主版本号
	///@param nMinorVersion 子版本号
	///@return 系统标识字符串
	//static cosnt char* GetVersion(int &nMajorVersion, int &nMinorVersion)
	static System::String^ GetVersion(int% nMajorVersion, int% nMinorVersion)
	{
		int nmaj,nmin;
		static const char* ret = CFfexFtdcMduserApi::GetVersion(nmaj, nmin);
		nMajorVersion = nmaj;
		nMinorVersion = nmin;
		return StringUtils::Cast_String(ret);
	}
	
	///删除接口对象本身
	///@remark 不再使用本接口对象时,调用该函数删除接口对象
	virtual void Release()
	{
		return m_pCFfexFtdcMduserApi->Release();
	}
	
	///初始化
	///@remark 初始化运行环境,只有调用后,接口才开始工作
	virtual void Init() 
	{
		return m_pCFfexFtdcMduserApi->Init();
	}
	
	///等待接口线程结束运行
	///@return 线程退出代码
	virtual int Join() 
	{
		return m_pCFfexFtdcMduserApi->Join();
	}
	
	///获取当前交易日
	///@retrun 获取到的交易日
	///@remark 只有登录成功后,才能得到正确的交易日
	//virtual const char *GetTradingDay() 
	virtual System::String^ GetTradingDay()
	{
		const char* ret = m_pCFfexFtdcMduserApi->GetTradingDay();
		return StringUtils::Cast_String(ret);
	}
	
	///注册前置机网络地址
	///@param pszFrontAddress：前置机网络地址。
	///@remark 网络地址的格式为：“protocol://ipaddress:port”，如：”tcp://127.0.0.1:17001”。 
	///@remark “tcp”代表传输协议，“127.0.0.1”代表服务器地址。”17001”代表服务器端口号。
	//virtual void RegisterFront(char *pszFrontAddress)
	virtual void RegisterFront(System::String^ pszFrontAddress)
	{
		std::string ret =  StringUtils::Cast_std_string(pszFrontAddress);
		char* p_input = const_cast<char*>(ret.c_str());
		return m_pCFfexFtdcMduserApi->RegisterFront(p_input) ;
	}
	
	///注册回调接口
	///@param pSpi 派生自回调接口类的实例
	//	virtual void RegisterSpi(CFfexFtdcMduserSpi *pSpi) = 0;
	
	///订阅市场行情。
	///@param nTopicID 市场行情主题  
	///@param nResumeType 市场行情重传方式  
	///        TERT_RESTART:从本交易日开始重传
	///        TERT_RESUME:从上次收到的续传
	///        TERT_QUICK:先传送当前行情快照,再传送登录后市场行情的内容
	///@remark 该方法要在Init方法前调用。若不调用则不会收到私有流的数据。
	virtual void SubscribeMarketDataTopic(int nTopicID, TE_RESUME_TYPE nResumeType)
	{
		return m_pCFfexFtdcMduserApi->SubscribeMarketDataTopic(nTopicID,  (::TE_RESUME_TYPE)(int)nResumeType);
	}

	///用户登录请求
	virtual int ReqUserLogin(ZJAdapter::CFfexFtdcReqUserLoginField% pReqUserLoginField, int nRequestID)
	{
		::CFfexFtdcReqUserLoginField ret = CastToNative<::CFfexFtdcReqUserLoginField>(pReqUserLoginField);
		return m_pCFfexFtdcMduserApi->ReqUserLogin(&ret,  nRequestID);
	}

	///用户退出请求
	virtual int ReqUserLogout(ZJAdapter::CFfexFtdcReqUserLogoutField% pReqUserLogout, int nRequestID)
	{
		::CFfexFtdcReqUserLogoutField input = CastToNative<::CFfexFtdcReqUserLogoutField>(pReqUserLogout);
		return m_pCFfexFtdcMduserApi->ReqUserLogout(&input,  nRequestID);
	}
private:
	::CFfexFtdcMduserApi* m_pCFfexFtdcMduserApi;
	Interop_Derived_CFfexFtdcMduserSpi* m_psh;
protected:
	~ZJMDAdapter()
	{
		if(m_psh)
			delete m_psh;
	}
};


//这个类的作用就是把native返回来的结果直接传递到.net代码中
private class Interop_Derived_CFfexFtdcMduserSpi : public ::CFfexFtdcMduserSpi
{
public:
	Interop_Derived_CFfexFtdcMduserSpi(ZJMDAdapter^ instance )
	{
		m_instance = new gcroot<ZJMDAdapter^>(instance);
	}
	///当客户端与交易后台建立起通信连接时（还未登录前），该方法被调用。
	virtual void OnFrontConnected()
	{
		((ZJMDAdapter^)*m_instance)->OnFrontConnected();
	}
	
	///当客户端与交易后台通信连接断开时，该方法被调用。当发生这个情况后，API会自动重新连接，客户端可不做处理。
	///@param nReason 错误原因
	///        0x1001 网络读失败
	///        0x1002 网络写失败
	///        0x2001 接收心跳超时
	///        0x2002 发送心跳失败
	///        0x2003 收到错误报文
	virtual void OnFrontDisconnected(int nReason)
	{
		((ZJMDAdapter^)*m_instance)->OnFrontDisconnected(nReason);
	}
		
	///心跳超时警告。当长时间未收到报文时，该方法被调用。
	///@param nTimeLapse 距离上次接收报文的时间
	virtual void OnHeartBeatWarning(int nTimeLapse)
	{
		((ZJMDAdapter^)*m_instance)->OnHeartBeatWarning(nTimeLapse);
	}
	///错误应答
	virtual void OnRspError(::CFfexFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
	{
		
		ZJAdapter::CFfexFtdcRspInfoField ret = CastToNet<ZJAdapter::CFfexFtdcRspInfoField>(pRspInfo);
		((ZJMDAdapter^)*m_instance)->OnRspError(ret,nRequestID,  bIsLast) ;
	}

	///用户登录应答
	virtual void OnRspUserLogin(::CFfexFtdcRspUserLoginField *pRspUserLogin,:: CFfexFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
	{
		ZJAdapter::CFfexFtdcRspUserLoginField ret1 = CastToNet<ZJAdapter::CFfexFtdcRspUserLoginField>(pRspUserLogin);
		ZJAdapter::CFfexFtdcRspInfoField ret2 = CastToNet<ZJAdapter::CFfexFtdcRspInfoField>(pRspInfo);
		((ZJMDAdapter^)*m_instance)->OnRspUserLogin(ret1,ret2,  nRequestID,  bIsLast) ;
	}

	///用户退出应答
	virtual void OnRspUserLogout(::CFfexFtdcRspUserLogoutField *pRspUserLogout,:: CFfexFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
	{
		ZJAdapter::CFfexFtdcRspUserLogoutField ret1 = CastToNet<ZJAdapter::CFfexFtdcRspUserLogoutField>(pRspUserLogout);
		ZJAdapter::CFfexFtdcRspInfoField ret2 = CastToNet<ZJAdapter::CFfexFtdcRspInfoField>(pRspInfo);
		((ZJMDAdapter^)*m_instance)->OnRspUserLogout(ret1,ret2,  nRequestID,  bIsLast) ;
	}

	///深度行情通知
	virtual void OnRtnDepthMarketData(::CFfexFtdcDepthMarketDataField *pDepthMarketData)
	{
		ZJAdapter::CFfexFtdcDepthMarketDataField ret1 = CastToNet<ZJAdapter::CFfexFtdcDepthMarketDataField>(pDepthMarketData);
		((ZJMDAdapter^)*m_instance)->OnRtnDepthMarketData(ret1);
	}
private:
	gcroot<ZJMDAdapter^>* m_instance;
};


//
ZJMDAdapter::ZJMDAdapter(System::String^  pszFlowPath)
{
	std::string ret =  StringUtils::Cast_std_string(pszFlowPath);
	const char* p_input = ret.c_str();
	m_pCFfexFtdcMduserApi = CFfexFtdcMduserApi::CreateFtdcMduserApi(p_input);

	m_psh = new Interop_Derived_CFfexFtdcMduserSpi(this);
	// 注册一事件处理的实例
	m_pCFfexFtdcMduserApi->RegisterSpi(m_psh);
}
}
#endif