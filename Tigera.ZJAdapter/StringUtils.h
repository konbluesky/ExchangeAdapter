using namespace System;
#include <string>
namespace ZJAdapter 
{
	private class StringUtils
	{
	public:
		static std::string Cast_std_string (System::String^ s);
		static std::wstring Cast_std_wstring (System::String ^ s);

		static System::String^ Cast_String(const char* s);
		static System::String^ Cast_String(const wchar_t* s);

		static System::String^ Cast_String(const std::string& s);
		static System::String^ Cast_String(const std::wstring& s);
	};

	//注：这个类是.net能够使用的，但其方便性却是C#无法比及的。
	template<typename T> ref class AutoPtr
	{
	private:
		T* _t;
	public:
		AutoPtr():_t(new T){}
		AutoPtr(T* t):_t(t){}

		T* operator->() {return _t;}

	protected:
		~AutoPtr() //implement as dispose
		{
			delete _t; _t =0;
		}
		!AutoPtr()	//implement as finalize
		{
			delete _t; _t=0;
		}
	};

	 IntPtr GetGlobMemory();
	//从.net的结构到native结构之间的转换:类似marshal_as功能
	template<typename TargetNative,typename SourceNet> 
	TargetNative CastToNative(SourceNet source)
	{
		IntPtr ptr = GetGlobMemory();//Marshal::AllocHGlobal(sizeof(TargetNative));
		Marshal::StructureToPtr(source,ptr,false);

		TargetNative ret = *((TargetNative*)(void*)ptr);
		return ret;
	}


	//注：source对象是取其指针的。
	template<typename TargetNet,typename SourceNative> 
	TargetNet CastToNet(SourceNative source)
	{


		try
		{
			TargetNet ret = (TargetNet) FuncHelper::CastHelper::Cast<TargetNet>((IntPtr)source);
			return ret;
		}
		catch(...)
		{
			TargetNet ret;
			return ret;
		}

		//TEST ret = (TEST) Marshal::PtrToStructure(&source,TEST::typeid);
		//return ret;
		/*
		pin_ptr<TargetNet> dst = &ret;

		memcpy((void*)dst, (void*)&source,
		System::Runtime::InteropServices::Marshal::SizeOf(T::typeid)
		sizeof(SourceNative));
		*/
	}


}