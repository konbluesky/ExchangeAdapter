#ifndef CFFEXFTDCTRADERAPI_NET_H
#define CFFEXFTDCTRADERAPI_NET_H
///注：这个是rmmsoft对应的.net版本定义
//我们的开发人员需要实现ZJTraderAdapter里OnXXXX函数的具体内容

/////////////////////////////////////////////////////////////////////////
///@system 新一代交易所系统
///@company 上海期货信息技术有限公司
///@file CFFEXFtdcTraderApi.h
///@brief 定义了客户端接口
///@history 
///20060106	赵鸿昊		创建该文件
/////////////////////////////////////////////////////////////////////////
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

namespace ZJAdapter
{



class Interop_Derived_CFfexFtdcTraderSpi;


public ref class ZJException : System::Exception
{
public:	
	
	ZJException(int id,System::String^ info):System::Exception(info)
	{
		mid = id;
	}
	int GetExceptionID()
	{
		return mid;
	}
private:
	int mid;
};

public ref class ZJTraderAdapter
{
public:
	void WARNINGMSG(System::String^ info) 
	{
		System::Console::WriteLine("if the actual function not be called,check whether override key word set at {0}",info);
	}
	///当客户端与交易后台建立起通信连接时（还未登录前），该方法被调用。
	virtual void OnFrontConnected()
	{
		WARNINGMSG("OnFrontConnected");
	}
	
	///当客户端与交易后台通信连接断开时，该方法被调用。当发生这个情况后，API会自动重新连接，客户端可不做处理。
	///@param nReason 错误原因
	///        0x1001 网络读失败
	///        0x1002 网络写失败
	///        0x2001 接收心跳超时
	///        0x2002 发送心跳失败
	///        0x2003 收到错误报文
	virtual void OnFrontDisconnected(int nReason)
	{
		WARNINGMSG("OnFrontDisconnected");
	}
		
	///心跳超时警告。当长时间未收到报文时，该方法被调用。
	///@param nTimeLapse 距离上次接收报文的时间
	virtual void OnHeartBeatWarning(int nTimeLapse)
	{
		WARNINGMSG("OnHeartBeatWarning");
	}

	///错误应答
	virtual void OnRspError(CFfexFtdcRspInfoField pRspInfo, int nRequestID, bool bIsLast) 
	{
		WARNINGMSG("OnRspError");
	}

	///用户登录应答
	virtual void OnRspUserLogin(CFfexFtdcRspUserLoginField pRspUserLogin, CFfexFtdcRspInfoField pRspInfo, int nRequestID, bool bIsLast) 
	{
		WARNINGMSG("OnRspUserLogin");
	}

	///用户退出应答
	virtual void OnRspUserLogout(CFfexFtdcRspUserLogoutField pRspUserLogout, CFfexFtdcRspInfoField pRspInfo, int nRequestID, bool bIsLast) 
	{
		WARNINGMSG("OnRspUserLogout");
	}

	///报单录入应答
	virtual void OnRspOrderInsert(CFfexFtdcInputOrderField pInputOrder, CFfexFtdcRspInfoField pRspInfo, int nRequestID, bool bIsLast) 
	{
		WARNINGMSG("OnRspOrderInsert");
	}

	///报单操作应答
	virtual void OnRspOrderAction(CFfexFtdcOrderActionField pOrderAction, CFfexFtdcRspInfoField pRspInfo, int nRequestID, bool bIsLast) 
	{
		WARNINGMSG("OnRspOrderAction");
	}

	///报价录入应答
	virtual void OnRspQuoteInsert(CFfexFtdcInputQuoteField pInputQuote, CFfexFtdcRspInfoField pRspInfo, int nRequestID, bool bIsLast) 
	{
		WARNINGMSG("OnRspQuoteInsert");
	}

	///报价操作应答
	virtual void OnRspQuoteAction(CFfexFtdcQuoteActionField pQuoteAction, CFfexFtdcRspInfoField pRspInfo, int nRequestID, bool bIsLast) 
	{
		WARNINGMSG("OnRspQuoteAction");
	}

	///用户密码修改应答
	virtual void OnRspUserPasswordUpdate(CFfexFtdcUserPasswordUpdateField pUserPasswordUpdate, CFfexFtdcRspInfoField pRspInfo, int nRequestID, bool bIsLast) 
	{
		WARNINGMSG("OnRspUserPasswordUpdate");
	}

	///执行宣告录入应答
	virtual void OnRspExecOrderInsert(CFfexFtdcInputExecOrderField pInputExecOrder, CFfexFtdcRspInfoField pRspInfo, int nRequestID, bool bIsLast) 
	{
		WARNINGMSG("OnRspExecOrderInsert");
	}

	///执行宣告操作应答
	virtual void OnRspExecOrderAction(CFfexFtdcExecOrderActionField pExecOrderAction, CFfexFtdcRspInfoField pRspInfo, int nRequestID, bool bIsLast) 
	{
		WARNINGMSG("OnRspExecOrderAction");
	}

	///管理报单录入应答
	virtual void OnRspAdminOrderInsert(CFfexFtdcInputAdminOrderField pInputAdminOrder, CFfexFtdcRspInfoField pRspInfo, int nRequestID, bool bIsLast) 
	{
		WARNINGMSG("OnRspAdminOrderInsert");
	}

	///会员资金查询应答
	virtual void OnRspQryPartAccount(CFfexFtdcRspPartAccountField pRspPartAccount, CFfexFtdcRspInfoField pRspInfo, int nRequestID, bool bIsLast) 
	{
		WARNINGMSG("OnRspQryPartAccount");
	}

	///报单查询应答
	virtual void OnRspQryOrder(CFfexFtdcOrderField pOrder, CFfexFtdcRspInfoField pRspInfo, int nRequestID, bool bIsLast) 
	{
		WARNINGMSG("OnRspQryOrder");
	}

	///报价查询应答
	virtual void OnRspQryQuote(CFfexFtdcQuoteField pQuote, CFfexFtdcRspInfoField pRspInfo, int nRequestID, bool bIsLast) 
	{
		WARNINGMSG("OnRspQryQuote");
	}

	///成交单查询应答
	virtual void OnRspQryTrade(CFfexFtdcTradeField pTrade, CFfexFtdcRspInfoField pRspInfo, int nRequestID, bool bIsLast) 
	{
		WARNINGMSG("OnRspQryTrade");
	}

	///会员客户查询应答
	virtual void OnRspQryClient(CFfexFtdcRspClientField pRspClient, CFfexFtdcRspInfoField pRspInfo, int nRequestID, bool bIsLast) 
	{
		WARNINGMSG("OnRspQryClient");
	}

	///会员持仓查询应答
	virtual void OnRspQryPartPosition(CFfexFtdcRspPartPositionField pRspPartPosition, CFfexFtdcRspInfoField pRspInfo, int nRequestID, bool bIsLast) 
	{
		WARNINGMSG("OnRspQryPartPosition");
	}

	///客户持仓查询应答
	virtual void OnRspQryClientPosition(CFfexFtdcRspClientPositionField pRspClientPosition, CFfexFtdcRspInfoField pRspInfo, int nRequestID, bool bIsLast) 
	{
		WARNINGMSG("OnRspQryClientPosition");
	}

	///合约查询应答
	virtual void OnRspQryInstrument(CFfexFtdcRspInstrumentField pRspInstrument, CFfexFtdcRspInfoField pRspInfo, int nRequestID, bool bIsLast) 
	{
		WARNINGMSG("OnRspQryInstrument");
	}

	///合约交易状态查询应答
	virtual void OnRspQryInstrumentStatus(CFfexFtdcInstrumentStatusField pInstrumentStatus, CFfexFtdcRspInfoField pRspInfo, int nRequestID, bool bIsLast) 
	{
		WARNINGMSG("OnRspQryInstrumentStatus");
	}

	///保值额度应答
	virtual void OnRspQryHedgeVolume(CFfexFtdcHedgeVolumeField pHedgeVolume, CFfexFtdcRspInfoField pRspInfo, int nRequestID, bool bIsLast) 
	{
		WARNINGMSG("OnRspQryHedgeVolume");
	}

	///信用限额查询应答
	virtual void OnRspQryCreditLimit(CFfexFtdcCreditLimitField pCreditLimit, CFfexFtdcRspInfoField pRspInfo, int nRequestID, bool bIsLast) 
	{
		WARNINGMSG("OnRspQryCreditLimit");
	}

	///普通行情查询应答
	virtual void OnRspQryMarketData(CFfexFtdcMarketDataField pMarketData, CFfexFtdcRspInfoField pRspInfo, int nRequestID, bool bIsLast) 
	{
		WARNINGMSG("OnRspQryMarketData");
	}

	///交易所公告查询请求应答
	virtual void OnRspQryBulletin(CFfexFtdcBulletinField pBulletin, CFfexFtdcRspInfoField pRspInfo, int nRequestID, bool bIsLast) 
	{
		WARNINGMSG("OnRspQryBulletin");
	}

	///合约价位查询应答
	virtual void OnRspQryMBLMarketData(CFfexFtdcMBLMarketDataField pMBLMarketData, CFfexFtdcRspInfoField pRspInfo, int nRequestID, bool bIsLast) 
	{
		WARNINGMSG("OnRspQryMBLMarketData");
	}

	///成交回报
	virtual void OnRtnTrade(CFfexFtdcTradeField pTrade) 
	{
		WARNINGMSG("OnRtnTrade");
	}

	///报单回报
	virtual void OnRtnOrder(CFfexFtdcOrderField pOrder) 
	{
		WARNINGMSG("OnRtnOrder");
	}

	///执行宣告回报
	virtual void OnRtnExecOrder(CFfexFtdcExecOrderField pExecOrder) 
	{
		WARNINGMSG("OnRtnExecOrder");
	}

	///报价回报
	virtual void OnRtnQuote(CFfexFtdcQuoteField pQuote) 
	{
		WARNINGMSG("OnRtnQuote");
	}

	///合约交易状态通知
	virtual void OnRtnInstrumentStatus(CFfexFtdcInstrumentStatusField pInstrumentStatus) 
	{
		WARNINGMSG("OnRtnInstrumentStatus");
	}

	///增加合约通知
	virtual void OnRtnInsInstrument(CFfexFtdcInstrumentField pInstrument) 
	{
		WARNINGMSG("OnRtnInsInstrument");
	}

	///删除合约通知
	virtual void OnRtnDelInstrument(CFfexFtdcInstrumentField pInstrument) 
	{
		WARNINGMSG("OnRtnDelInstrument");
	}

	///增加合约单腿通知
	virtual void OnRtnInsCombinationLeg(CFfexFtdcCombinationLegField pCombinationLeg) 
	{
		WARNINGMSG("OnRtnInsCombinationLeg");
	}

	///删除合约单腿通知
	virtual void OnRtnDelCombinationLeg(CFfexFtdcCombinationLegField pCombinationLeg) 
	{
		WARNINGMSG("OnRtnDelCombinationLeg");
	}

	///别名定义通知
	virtual void OnRtnAliasDefine(CFfexFtdcAliasDefineField pAliasDefine) 
	{
		WARNINGMSG("OnRtnAliasDefine");
	}

	///公告通知
	virtual void OnRtnBulletin(CFfexFtdcBulletinField pBulletin) 
	{
		WARNINGMSG("OnRtnBulletin");
	}

	///报单录入错误回报
	virtual void OnErrRtnOrderInsert(CFfexFtdcInputOrderField pInputOrder, CFfexFtdcRspInfoField pRspInfo) 
	{
		WARNINGMSG("OnErrRtnOrderInsert");
	}

	///报单操作错误回报
	virtual void OnErrRtnOrderAction(CFfexFtdcOrderActionField pOrderAction, CFfexFtdcRspInfoField pRspInfo) 
	{
		WARNINGMSG("OnErrRtnOrderAction");
	}

	///报价录入错误回报
	virtual void OnErrRtnQuoteInsert(CFfexFtdcInputQuoteField pInputQuote, CFfexFtdcRspInfoField pRspInfo) 
	{
		WARNINGMSG("OnErrRtnQuoteInsert");
	}

	///报价操作错误回报
	virtual void OnErrRtnQuoteAction(CFfexFtdcQuoteActionField pQuoteAction, CFfexFtdcRspInfoField pRspInfo) 
	{
		WARNINGMSG("OnErrRtnQuoteAction");
	}

	///执行宣告录入错误回报
	virtual void OnErrRtnExecOrderInsert(CFfexFtdcInputExecOrderField pInputExecOrder, CFfexFtdcRspInfoField pRspInfo) 
	{
		WARNINGMSG("OnErrRtnExecOrderInsert");
	}

	///执行宣告操作错误回报
	virtual void OnErrRtnExecOrderAction(CFfexFtdcExecOrderActionField pExecOrderAction, CFfexFtdcRspInfoField pRspInfo) 
	{
		WARNINGMSG("OnErrRtnExecOrderAction");
	}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

public:
	///创建TraderApi
	///@param pszFlowPath 存贮订阅信息文件的目录，默认为当前目录
	///@return 创建出的UserApi
	//static CFfexFtdcTraderApi *CreateFtdcTraderApi(const char *pszFlowPath = "");

	ZJTraderAdapter(System::String^ pszFlowPath );
	///获取系统版本号
	///@param nMajorVersion 主版本号
	///@param nMinorVersion 子版本号
	///@return 系统标识字符串
	//static const char *GetVersion(int &nMajorVersion, int &nMinorVersion);
	static System::String^ GetVersion(int %nMajorVersion, int %nMinorVersion)
	{
		int nMaj,nMin;
		const char* ret = CFfexFtdcTraderApi::GetVersion(nMaj, nMin);
		nMajorVersion = nMaj;
		nMinorVersion = nMin;
		return StringUtils::Cast_String(ret);
	}

	///删除接口对象本身
	///@remark 不再使用本接口对象时,调用该函数删除接口对象
	virtual void Release() 
	{
		return m_pCFfexFtdcTraderApi->Release();
	}

	///初始化
	///@remark 初始化运行环境,只有调用后,接口才开始工作
	virtual void Init() 
	{
		return m_pCFfexFtdcTraderApi->Init();
	}

	///等待接口线程结束运行
	///@return 线程退出代码
	virtual int Join() 
	{
		return m_pCFfexFtdcTraderApi->Join();
	}

	///获取当前交易日
	///@retrun 获取到的交易日
	///@remark 只有登录成功后,才能得到正确的交易日
	//virtual const char *GetTradingDay() = 0;
	virtual System::String^ GetTradingDay()
	{
		const char* ret = m_pCFfexFtdcTraderApi->GetTradingDay();
		return StringUtils::Cast_String(ret);
	}

	///注册前置机网络地址
	///@param pszFrontAddress：前置机网络地址。
	///@remark 网络地址的格式为：“protocol://ipaddress:port”，如：”tcp://127.0.0.1:17001”。 
	///@remark “tcp”代表传输协议，“127.0.0.1”代表服务器地址。”17001”代表服务器端口号。
	//virtual void RegisterFront(char *pszFrontAddress) = 0;
	virtual void RegisterFront(System::String^ pszFrontAddress) 
	{
		std::string ret =  StringUtils::Cast_std_string(pszFrontAddress);
		char* p_input = const_cast<char*>(ret.c_str());
		
		return m_pCFfexFtdcTraderApi->RegisterFront(p_input);
	}
	
	///注册回调接口
	///@param pSpi 派生自回调接口类的实例
	//virtual void RegisterSpi(CFfexFtdcTraderSpi *pSpi) 
	//{
	//	return m_pCFfexFtdcTraderApi->RegisterSpi(pSpi);
	//}
	
	///加载证书
	///@param pszCertFileName 用户证书文件名
	///@param pszKeyFileName 用户私钥文件名
	///@param pszCaFileName 可信任CA证书文件名
	///@param pszKeyFilePassword 用户私钥文件密码
	///@return 0 操作成功
	///@return -1 可信任CA证书载入失败
	///@return -2 用户证书载入失败
	///@return -3 用户私钥载入失败	
	///@return -4 用户证书校验失败
	//virtual int RegisterCertificateFile(const char *pszCertFileName, const char *pszKeyFileName, 
	//	const char *pszCaFileName, const char *pszKeyFilePassword) 
	virtual int RegisterCertificateFile(System::String^ pszCertFileName, System::String^ pszKeyFileName, 
		System::String^ pszCaFileName, System::String^ pszKeyFilePassword) 
	{
		std::string ret1 = StringUtils::Cast_std_string(pszCertFileName);
		std::string ret2 = StringUtils::Cast_std_string(pszKeyFileName);
		std::string ret3 = StringUtils::Cast_std_string(pszCaFileName);
		std::string ret4 = StringUtils::Cast_std_string(pszKeyFilePassword);

		return m_pCFfexFtdcTraderApi->RegisterCertificateFile(
			ret1.c_str(),
			ret2.c_str(),
			ret3.c_str(),
			ret4.c_str());			
	}

	///订阅私有流。
	///@param nResumeType 私有流重传方式  
	///        TERT_RESTART:从本交易日开始重传
	///        TERT_RESUME:从上次收到的续传
	///        TERT_QUICK:只传送登录后私有流的内容
	///@remark 该方法要在Init方法前调用。若不调用则不会收到私有流的数据。
	virtual void SubscribePrivateTopic(ZJAdapter::TE_RESUME_TYPE nResumeType) 
	{
		return m_pCFfexFtdcTraderApi->SubscribePrivateTopic((::TE_RESUME_TYPE)(int)nResumeType);
	}
	
	///订阅公共流。
	///@param nResumeType 公共流重传方式  
	///        TERT_RESTART:从本交易日开始重传
	///        TERT_RESUME:从上次收到的续传
	///        TERT_QUICK:只传送登录后公共流的内容
	///@remark 该方法要在Init方法前调用。若不调用则不会收到公共流的数据。
	virtual void SubscribePublicTopic(ZJAdapter::TE_RESUME_TYPE nResumeType) 
	{
		return m_pCFfexFtdcTraderApi->SubscribePublicTopic((::TE_RESUME_TYPE)(int)nResumeType);
	}

	///用户登录请求
	virtual int ReqUserLogin(ZJAdapter::CFfexFtdcReqUserLoginField% pReqUserLoginField, int nRequestID) 
	{
		::CFfexFtdcReqUserLoginField input = CastToNative<::CFfexFtdcReqUserLoginField >(pReqUserLoginField);
		return m_pCFfexFtdcTraderApi->ReqUserLogin(&input,nRequestID);
	}

	///用户退出请求
	virtual int ReqUserLogout(CFfexFtdcReqUserLogoutField %pReqUserLogout, int nRequestID) 
	{
		::CFfexFtdcReqUserLogoutField input = CastToNative<::CFfexFtdcReqUserLogoutField >(pReqUserLogout);
		return m_pCFfexFtdcTraderApi->ReqUserLogout(&input,nRequestID);
	}

	///报单录入请求
	virtual int ReqOrderInsert(CFfexFtdcInputOrderField% pInputOrder, int nRequestID) 
	{
		::CFfexFtdcInputOrderField input = CastToNative<::CFfexFtdcInputOrderField >(pInputOrder);
		return m_pCFfexFtdcTraderApi->ReqOrderInsert(&input,  nRequestID) ;
	}

	///报单操作请求
	virtual int ReqOrderAction(CFfexFtdcOrderActionField %pOrderAction, int nRequestID) 
	{
		::CFfexFtdcOrderActionField input = CastToNative<::CFfexFtdcOrderActionField >(pOrderAction);
		return m_pCFfexFtdcTraderApi->ReqOrderAction(&input,  nRequestID) ;
	}

	///报价录入请求
	virtual int ReqQuoteInsert(ZJAdapter::CFfexFtdcInputQuoteField %pInputQuote, int nRequestID) 
	{
		::CFfexFtdcInputQuoteField input = CastToNative<::CFfexFtdcInputQuoteField >(pInputQuote);
		return m_pCFfexFtdcTraderApi->ReqQuoteInsert(&input,  nRequestID);
	}

	///报价操作请求
	virtual int ReqQuoteAction(ZJAdapter::CFfexFtdcQuoteActionField %pQuoteAction, int nRequestID) 
	{
		::CFfexFtdcQuoteActionField input = CastToNative<::CFfexFtdcQuoteActionField >(pQuoteAction);
		return m_pCFfexFtdcTraderApi->ReqQuoteAction(&input,nRequestID);
	}

	///用户密码修改请求
	virtual int ReqUserPasswordUpdate(ZJAdapter::CFfexFtdcUserPasswordUpdateField %pUserPasswordUpdate, int nRequestID)
	{
		::CFfexFtdcUserPasswordUpdateField input = CastToNative<::CFfexFtdcUserPasswordUpdateField >(pUserPasswordUpdate);
		return m_pCFfexFtdcTraderApi->ReqUserPasswordUpdate(&input,nRequestID);
	}

	///执行宣告录入请求
	virtual int ReqExecOrderInsert(ZJAdapter::CFfexFtdcInputExecOrderField %pInputExecOrder, int nRequestID) 
	{
		::CFfexFtdcInputExecOrderField input = CastToNative<::CFfexFtdcInputExecOrderField >(pInputExecOrder);
		return m_pCFfexFtdcTraderApi->ReqExecOrderInsert(&input,nRequestID);
	}

	///执行宣告操作请求
	virtual int ReqExecOrderAction(ZJAdapter::CFfexFtdcExecOrderActionField %pExecOrderAction, int nRequestID) 
	{
		::CFfexFtdcExecOrderActionField input = CastToNative<::CFfexFtdcExecOrderActionField >(pExecOrderAction);
		return m_pCFfexFtdcTraderApi->ReqExecOrderAction(&input,nRequestID);
	}

	///管理报单录入请求
	virtual int ReqAdminOrderInsert(ZJAdapter::CFfexFtdcInputAdminOrderField %pInputAdminOrder, int nRequestID) 
	{
		::CFfexFtdcInputAdminOrderField input = CastToNative<::CFfexFtdcInputAdminOrderField >(pInputAdminOrder);
		return m_pCFfexFtdcTraderApi->ReqAdminOrderInsert(&input,nRequestID);
	}

	///会员资金查询请求
	virtual int ReqQryPartAccount(ZJAdapter::CFfexFtdcQryPartAccountField %pQryPartAccount, int nRequestID)		
	{
		::CFfexFtdcQryPartAccountField input = CastToNative<::CFfexFtdcQryPartAccountField >(pQryPartAccount);
		return m_pCFfexFtdcTraderApi->ReqQryPartAccount(&input,nRequestID);
	}

	///报单查询请求
	virtual int ReqQryOrder(CFfexFtdcQryOrderField %pQryOrder, int nRequestID) 
	{
		::CFfexFtdcQryOrderField input = CastToNative<::CFfexFtdcQryOrderField >(pQryOrder);
		return m_pCFfexFtdcTraderApi->ReqQryOrder(&input,  nRequestID) ;
	}

	///报价查询请求
	virtual int ReqQryQuote(CFfexFtdcQryQuoteField %pQryQuote, int nRequestID) 
	{
		::CFfexFtdcQryQuoteField input = CastToNative<::CFfexFtdcQryQuoteField >(pQryQuote);
		return  m_pCFfexFtdcTraderApi->ReqQryQuote(&input,  nRequestID) ;
	}

	///成交单查询请求
	virtual int ReqQryTrade(CFfexFtdcQryTradeField %pQryTrade, int nRequestID) 
	{
		::CFfexFtdcQryTradeField input = CastToNative<::CFfexFtdcQryTradeField >(pQryTrade);
		return  m_pCFfexFtdcTraderApi->ReqQryTrade(&input,  nRequestID) ;
	}

	///会员客户查询请求
	virtual int ReqQryClient(CFfexFtdcQryClientField %pQryClient, int nRequestID) 
	{
		::CFfexFtdcQryClientField input = CastToNative<::CFfexFtdcQryClientField >(pQryClient);
		return  m_pCFfexFtdcTraderApi->ReqQryClient(&input,  nRequestID) ;
	}

	///会员持仓查询请求
	virtual int ReqQryPartPosition(CFfexFtdcQryPartPositionField %pQryPartPosition, int nRequestID) 
	{
		::CFfexFtdcQryPartPositionField input = CastToNative<::CFfexFtdcQryPartPositionField >(pQryPartPosition);
		return  m_pCFfexFtdcTraderApi->ReqQryPartPosition(&input,  nRequestID) ;
	}

	///客户持仓查询请求
	virtual int ReqQryClientPosition(CFfexFtdcQryClientPositionField %pQryClientPosition, int nRequestID) 
	{
		::CFfexFtdcQryClientPositionField input = CastToNative<::CFfexFtdcQryClientPositionField >(pQryClientPosition);
		return  m_pCFfexFtdcTraderApi->ReqQryClientPosition(&input,  nRequestID) ;
	}

	///合约查询请求
	virtual int ReqQryInstrument(CFfexFtdcQryInstrumentField %pQryInstrument, int nRequestID)
	{
		::CFfexFtdcQryInstrumentField input = CastToNative<::CFfexFtdcQryInstrumentField >(pQryInstrument);
		return m_pCFfexFtdcTraderApi->ReqQryInstrument(&input,nRequestID);
	}

	///合约交易状态查询请求
	virtual int ReqQryInstrumentStatus(CFfexFtdcQryInstrumentStatusField %pQryInstrumentStatus, int nRequestID)
	{
		::CFfexFtdcQryInstrumentStatusField input = CastToNative<::CFfexFtdcQryInstrumentStatusField >(pQryInstrumentStatus);
		return m_pCFfexFtdcTraderApi->ReqQryInstrumentStatus(&input,nRequestID);
	}

	///保值额度查询
	virtual int ReqQryHedgeVolume(CFfexFtdcQryHedgeVolumeField %pQryHedgeVolume, int nRequestID) 
	{
		::CFfexFtdcQryHedgeVolumeField input = CastToNative<::CFfexFtdcQryHedgeVolumeField >(pQryHedgeVolume);
		return m_pCFfexFtdcTraderApi->ReqQryHedgeVolume(&input,nRequestID);
	}

	///信用限额查询请求
	virtual int ReqQryCreditLimit(CFfexFtdcQryCreditLimitField %pQryCreditLimit, int nRequestID) 
	{
		::CFfexFtdcQryCreditLimitField input = CastToNative<::CFfexFtdcQryCreditLimitField >(pQryCreditLimit);
		return m_pCFfexFtdcTraderApi->ReqQryCreditLimit(&input,nRequestID);
	}

	///普通行情查询请求
	virtual int ReqQryMarketData(CFfexFtdcQryMarketDataField %pQryMarketData, int nRequestID)
	{
		::CFfexFtdcQryMarketDataField input = CastToNative<::CFfexFtdcQryMarketDataField >(pQryMarketData);
		return m_pCFfexFtdcTraderApi->ReqQryMarketData(&input,nRequestID);
	}

	///交易所公告查询请求
	virtual int ReqQryBulletin(CFfexFtdcQryBulletinField %pQryBulletin, int nRequestID) 
	{
		::CFfexFtdcQryBulletinField input = CastToNative<::CFfexFtdcQryBulletinField >(pQryBulletin);
		return m_pCFfexFtdcTraderApi->ReqQryBulletin(&input,nRequestID);
	}

	///合约价位查询
	virtual int ReqQryMBLMarketData(CFfexFtdcQryMBLMarketDataField %pQryMBLMarketData, int nRequestID)
	{
		::CFfexFtdcQryMBLMarketDataField input = CastToNative<::CFfexFtdcQryMBLMarketDataField >(pQryMBLMarketData);
		return m_pCFfexFtdcTraderApi->ReqQryMBLMarketData(&input,nRequestID);
	}
protected:
	~ZJTraderAdapter()
	{
		if(m_sh)
			delete m_sh;
	};
private: 
	::CFfexFtdcTraderApi* m_pCFfexFtdcTraderApi;
	Interop_Derived_CFfexFtdcTraderSpi* m_sh;
};




private class Interop_Derived_CFfexFtdcTraderSpi :public CFfexFtdcTraderSpi
{

public:
	Interop_Derived_CFfexFtdcTraderSpi(ZJTraderAdapter^ instance)
	{
		m_instance = new gcroot<ZJTraderAdapter^>(instance);
	}
	
	///当客户端与交易后台建立起通信连接时（还未登录前），该方法被调用。
	virtual void OnFrontConnected()
	{
		return ((ZJTraderAdapter^)*m_instance)->OnFrontConnected();
	}

	///当客户端与交易后台通信连接断开时，该方法被调用。当发生这个情况后，API会自动重新连接，客户端可不做处理。
	///@param nReason 错误原因
	///        0x1001 网络读失败
	///        0x1002 网络写失败
	///        0x2001 接收心跳超时
	///        0x2002 发送心跳失败
	///        0x2003 收到错误报文
	virtual void OnFrontDisconnected(int nReason)
	{
		return ((ZJTraderAdapter^)*m_instance)->OnFrontDisconnected(nReason);
	}

	///心跳超时警告。当长时间未收到报文时，该方法被调用。
	///@param nTimeLapse 距离上次接收报文的时间
	virtual void OnHeartBeatWarning(int nTimeLapse)
	{
		return ((ZJTraderAdapter^)*m_instance)->OnHeartBeatWarning(nTimeLapse);
	}


	///错误应答
	virtual void OnRspError(::CFfexFtdcRspInfoField* pRspInfo, int nRequestID, bool bIsLast) 
	{
		ZJAdapter::CFfexFtdcRspInfoField ret1= CastToNet<ZJAdapter::CFfexFtdcRspInfoField>(pRspInfo);
		return ((ZJTraderAdapter^)*m_instance)->OnRspError(ret1,nRequestID,  bIsLast);
	}

	///用户登录应答
	virtual void OnRspUserLogin(::CFfexFtdcRspUserLoginField *pRspUserLogin, ::CFfexFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
	{
		ZJAdapter::CFfexFtdcRspUserLoginField ret1 = CastToNet<ZJAdapter::CFfexFtdcRspUserLoginField>(pRspUserLogin);
		ZJAdapter::CFfexFtdcRspInfoField ret2 = CastToNet<ZJAdapter::CFfexFtdcRspInfoField>(pRspInfo);
		return ((ZJTraderAdapter^)*m_instance)->OnRspUserLogin(ret1,ret2,nRequestID,bIsLast);
	}

	
	///用户退出应答
	virtual void OnRspUserLogout(::CFfexFtdcRspUserLogoutField *pRspUserLogout, ::CFfexFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
	{
		ZJAdapter::CFfexFtdcRspUserLogoutField ret1 = CastToNet<ZJAdapter::CFfexFtdcRspUserLogoutField>(pRspUserLogout);
		ZJAdapter::CFfexFtdcRspInfoField ret2 = CastToNet<ZJAdapter::CFfexFtdcRspInfoField>(pRspInfo);
		return ((ZJTraderAdapter^)*m_instance)->OnRspUserLogout(ret1,ret2,nRequestID,bIsLast);
	}

	///报单录入应答
	virtual void OnRspOrderInsert(::CFfexFtdcInputOrderField *pInputOrder, ::CFfexFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
	{
		ZJAdapter::CFfexFtdcInputOrderField ret1 =CastToNet<ZJAdapter::CFfexFtdcInputOrderField>(pInputOrder);
		ZJAdapter::CFfexFtdcRspInfoField ret2 = CastToNet<ZJAdapter::CFfexFtdcRspInfoField>(pRspInfo);
		return ((ZJTraderAdapter^)*m_instance)->OnRspOrderInsert(ret1,ret2,nRequestID,  bIsLast); 
	}

	///报单操作应答totot
	virtual void OnRspOrderAction(::CFfexFtdcOrderActionField *pOrderAction, ::CFfexFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
	{
		ZJAdapter::CFfexFtdcOrderActionField ret1 =CastToNet<ZJAdapter::CFfexFtdcOrderActionField>(pOrderAction);
		ZJAdapter::CFfexFtdcRspInfoField ret2 = CastToNet<ZJAdapter::CFfexFtdcRspInfoField>(pRspInfo);
		return ((ZJTraderAdapter^)*m_instance)->OnRspOrderAction(ret1,ret2,nRequestID,  bIsLast) ;
	}

	///报价录入应答
	virtual void OnRspQuoteInsert(::CFfexFtdcInputQuoteField *pInputQuote, ::CFfexFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
	{
		ZJAdapter::CFfexFtdcInputQuoteField ret1 =CastToNet<ZJAdapter::CFfexFtdcInputQuoteField>(pInputQuote);
		ZJAdapter::CFfexFtdcRspInfoField ret2 = CastToNet<ZJAdapter::CFfexFtdcRspInfoField>(pRspInfo);
		return ((ZJTraderAdapter^)*m_instance)->OnRspQuoteInsert(ret1,ret2,  nRequestID,  bIsLast) ;
	}

	///报价操作应答
	virtual void OnRspQuoteAction(::CFfexFtdcQuoteActionField *pQuoteAction, ::CFfexFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
	{
		ZJAdapter::CFfexFtdcQuoteActionField ret1 =CastToNet<ZJAdapter::CFfexFtdcQuoteActionField>(pQuoteAction);
		ZJAdapter::CFfexFtdcRspInfoField ret2 = CastToNet<ZJAdapter::CFfexFtdcRspInfoField>(pRspInfo);
		return ((ZJTraderAdapter^)*m_instance)->OnRspQuoteAction(ret1,ret2,  nRequestID,  bIsLast) ;
	}

	///用户密码修改应答
	virtual void OnRspUserPasswordUpdate(::CFfexFtdcUserPasswordUpdateField *pUserPasswordUpdate, ::CFfexFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
	{
		ZJAdapter::CFfexFtdcUserPasswordUpdateField ret1 =CastToNet<ZJAdapter::CFfexFtdcUserPasswordUpdateField>(pUserPasswordUpdate);
		ZJAdapter::CFfexFtdcRspInfoField ret2 = CastToNet<ZJAdapter::CFfexFtdcRspInfoField>(pRspInfo);
		return ((ZJTraderAdapter^)*m_instance)->OnRspUserPasswordUpdate(ret1,ret2, nRequestID,  bIsLast) ;
	}

	///执行宣告录入应答
	virtual void OnRspExecOrderInsert(::CFfexFtdcInputExecOrderField *pInputExecOrder, ::CFfexFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
	{
		ZJAdapter::CFfexFtdcInputExecOrderField ret1 =CastToNet<ZJAdapter::CFfexFtdcInputExecOrderField>(pInputExecOrder);
		ZJAdapter::CFfexFtdcRspInfoField ret2 = CastToNet<ZJAdapter::CFfexFtdcRspInfoField>(pRspInfo);
		return ((ZJTraderAdapter^)*m_instance)->OnRspExecOrderInsert(ret1,ret2, nRequestID, bIsLast) ;
	}

	///执行宣告操作应答
	virtual void OnRspExecOrderAction(::CFfexFtdcExecOrderActionField *pExecOrderAction, ::CFfexFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
	{
		ZJAdapter::CFfexFtdcExecOrderActionField ret1 =CastToNet<ZJAdapter::CFfexFtdcExecOrderActionField>(pExecOrderAction);
		ZJAdapter::CFfexFtdcRspInfoField ret2 = CastToNet<ZJAdapter::CFfexFtdcRspInfoField>(pRspInfo);
		return ((ZJTraderAdapter^)*m_instance)->OnRspExecOrderAction(ret1,ret2, nRequestID, bIsLast) ;
	}

	///管理报单录入应答
	virtual void OnRspAdminOrderInsert(::CFfexFtdcInputAdminOrderField *pInputAdminOrder, ::CFfexFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
	{
		ZJAdapter::CFfexFtdcInputAdminOrderField ret1 =CastToNet<ZJAdapter::CFfexFtdcInputAdminOrderField>(pInputAdminOrder);
		ZJAdapter::CFfexFtdcRspInfoField ret2 = CastToNet<ZJAdapter::CFfexFtdcRspInfoField>(pRspInfo);
		return ((ZJTraderAdapter^)*m_instance)->OnRspAdminOrderInsert(ret1,ret2,  nRequestID,  bIsLast) ;
	}

	///会员资金查询应答
	virtual void OnRspQryPartAccount(::CFfexFtdcRspPartAccountField *pRspPartAccount, ::CFfexFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
	{
		ZJAdapter::CFfexFtdcRspPartAccountField ret1 =CastToNet<ZJAdapter::CFfexFtdcRspPartAccountField>(pRspPartAccount);
		ZJAdapter::CFfexFtdcRspInfoField ret2 = CastToNet<ZJAdapter::CFfexFtdcRspInfoField>(pRspInfo);
		return ((ZJTraderAdapter^)*m_instance)->OnRspQryPartAccount(ret1,ret2, nRequestID,  bIsLast) ;
	}

	///报单查询应答
	virtual void OnRspQryOrder(::CFfexFtdcOrderField *pOrder,:: CFfexFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
	{
		ZJAdapter::CFfexFtdcOrderField ret1 =CastToNet<ZJAdapter::CFfexFtdcOrderField>(pOrder);
		ZJAdapter::CFfexFtdcRspInfoField ret2 = CastToNet<ZJAdapter::CFfexFtdcRspInfoField>(pRspInfo);
		return ((ZJTraderAdapter^)*m_instance)->OnRspQryOrder(ret1,ret2, nRequestID,  bIsLast) ;
	}

	///报价查询应答
	virtual void OnRspQryQuote(::CFfexFtdcQuoteField *pQuote, ::CFfexFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
	{
		ZJAdapter::CFfexFtdcQuoteField ret1 =CastToNet<ZJAdapter::CFfexFtdcQuoteField>(pQuote);
		ZJAdapter::CFfexFtdcRspInfoField ret2 = CastToNet<ZJAdapter::CFfexFtdcRspInfoField>(pRspInfo);
		return ((ZJTraderAdapter^)*m_instance)->OnRspQryQuote(ret1,ret2,  nRequestID,  bIsLast) ;
	}

	///成交单查询应答
	virtual void OnRspQryTrade(::CFfexFtdcTradeField *pTrade, ::CFfexFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
	{
		ZJAdapter::CFfexFtdcTradeField ret1 =CastToNet<ZJAdapter::CFfexFtdcTradeField>(pTrade);
		ZJAdapter::CFfexFtdcRspInfoField ret2 = CastToNet<ZJAdapter::CFfexFtdcRspInfoField>(pRspInfo);
		return ((ZJTraderAdapter^)*m_instance)->OnRspQryTrade(ret1,ret2,  nRequestID,  bIsLast) ;
	}

	///会员客户查询应答-------------------
	virtual void OnRspQryClient(::CFfexFtdcRspClientField *pRspClient, ::CFfexFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
	{
		if(pRspClient)
		{				
			ZJAdapter::CFfexFtdcRspClientField ret1 =CastToNet<ZJAdapter::CFfexFtdcRspClientField>(pRspClient);
			ZJAdapter::CFfexFtdcRspInfoField ret2 = CastToNet<ZJAdapter::CFfexFtdcRspInfoField>(pRspInfo);
			return ((ZJTraderAdapter^)*m_instance)->OnRspQryClient(ret1,ret2,  nRequestID,  bIsLast) ;
		}
		else
		{
			throw gcnew ZJException(pRspInfo->ErrorID, StringUtils::Cast_String(pRspInfo->ErrorMsg));
		}
	}

	

	///会员持仓查询应答
	virtual void OnRspQryPartPosition(::CFfexFtdcRspPartPositionField *pRspPartPosition, ::CFfexFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
	{
		ZJAdapter::CFfexFtdcRspPartPositionField ret1 =CastToNet<ZJAdapter::CFfexFtdcRspPartPositionField>(pRspPartPosition);
		ZJAdapter::CFfexFtdcRspInfoField ret2 = CastToNet<ZJAdapter::CFfexFtdcRspInfoField>(pRspInfo);
		return ((ZJTraderAdapter^)*m_instance)->OnRspQryPartPosition(ret1,ret2,  nRequestID,  bIsLast) ;
	}

	///客户持仓查询应答
	virtual void OnRspQryClientPosition(::CFfexFtdcRspClientPositionField *pRspClientPosition,:: CFfexFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
	{
		ZJAdapter::CFfexFtdcRspClientPositionField ret1 =CastToNet<ZJAdapter::CFfexFtdcRspClientPositionField>(pRspClientPosition);
		ZJAdapter::CFfexFtdcRspInfoField ret2 = CastToNet<ZJAdapter::CFfexFtdcRspInfoField>(pRspInfo);
		return ((ZJTraderAdapter^)*m_instance)->OnRspQryClientPosition(ret1,ret2,  nRequestID,  bIsLast) ;
	}

	///合约查询应答
	virtual void OnRspQryInstrument(::CFfexFtdcRspInstrumentField *pRspInstrument, ::CFfexFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
	{
		ZJAdapter::CFfexFtdcRspInstrumentField ret1 =CastToNet<ZJAdapter::CFfexFtdcRspInstrumentField>(pRspInstrument);
		ZJAdapter::CFfexFtdcRspInfoField ret2 = CastToNet<ZJAdapter::CFfexFtdcRspInfoField>(pRspInfo);
		return ((ZJTraderAdapter^)*m_instance)->OnRspQryInstrument(ret1,ret2,  nRequestID,  bIsLast); 
	}

	///合约交易状态查询应答
	virtual void OnRspQryInstrumentStatus(::CFfexFtdcInstrumentStatusField *pInstrumentStatus, ::CFfexFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
	{
		ZJAdapter::CFfexFtdcInstrumentStatusField ret1 =CastToNet<ZJAdapter::CFfexFtdcInstrumentStatusField>(pInstrumentStatus);
		ZJAdapter::CFfexFtdcRspInfoField ret2 = CastToNet<ZJAdapter::CFfexFtdcRspInfoField>(pRspInfo);
		return ((ZJTraderAdapter^)*m_instance)->OnRspQryInstrumentStatus(ret1,ret2,  nRequestID,  bIsLast);
	}

	///保值额度应答
	virtual void OnRspQryHedgeVolume(::CFfexFtdcHedgeVolumeField *pHedgeVolume, ::CFfexFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
	{
		ZJAdapter::CFfexFtdcHedgeVolumeField ret1 =CastToNet<ZJAdapter::CFfexFtdcHedgeVolumeField>(pHedgeVolume);
		ZJAdapter::CFfexFtdcRspInfoField ret2 = CastToNet<ZJAdapter::CFfexFtdcRspInfoField>(pRspInfo);
		return ((ZJTraderAdapter^)*m_instance)->OnRspQryHedgeVolume(ret1,ret2,  nRequestID,  bIsLast) ;
	}

	///信用限额查询应答
	virtual void OnRspQryCreditLimit(::CFfexFtdcCreditLimitField *pCreditLimit, ::CFfexFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
	{
		ZJAdapter::CFfexFtdcCreditLimitField ret1 =CastToNet<ZJAdapter::CFfexFtdcCreditLimitField>(pCreditLimit);
		ZJAdapter::CFfexFtdcRspInfoField ret2 = CastToNet<ZJAdapter::CFfexFtdcRspInfoField>(pRspInfo);
		return ((ZJTraderAdapter^)*m_instance)->OnRspQryCreditLimit(ret1,ret2,  nRequestID,  bIsLast);
	}

	///普通行情查询应答
	virtual void OnRspQryMarketData(::CFfexFtdcMarketDataField *pMarketData,:: CFfexFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
	{
		ZJAdapter::CFfexFtdcMarketDataField ret1 =CastToNet<ZJAdapter::CFfexFtdcMarketDataField>(pMarketData);
		ZJAdapter::CFfexFtdcRspInfoField ret2 = CastToNet<ZJAdapter::CFfexFtdcRspInfoField>(pRspInfo);
		return ((ZJTraderAdapter^)*m_instance)->OnRspQryMarketData(ret1,ret2,  nRequestID,  bIsLast);
	}

	///交易所公告查询请求应答
	virtual void OnRspQryBulletin(::CFfexFtdcBulletinField *pBulletin,:: CFfexFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
	{
		ZJAdapter::CFfexFtdcBulletinField ret1 =CastToNet<ZJAdapter::CFfexFtdcBulletinField>(pBulletin);
		ZJAdapter::CFfexFtdcRspInfoField ret2 = CastToNet<ZJAdapter::CFfexFtdcRspInfoField>(pRspInfo);
		return ((ZJTraderAdapter^)*m_instance)->OnRspQryBulletin(ret1,ret2,  nRequestID,  bIsLast) ;
	}

	///合约价位查询应答
	virtual void OnRspQryMBLMarketData(::CFfexFtdcMBLMarketDataField *pMBLMarketData,:: CFfexFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
	{
		ZJAdapter::CFfexFtdcMBLMarketDataField ret1 =CastToNet<ZJAdapter::CFfexFtdcMBLMarketDataField>(pMBLMarketData);
		ZJAdapter::CFfexFtdcRspInfoField ret2 = CastToNet<ZJAdapter::CFfexFtdcRspInfoField>(pRspInfo);
		return ((ZJTraderAdapter^)*m_instance)->OnRspQryMBLMarketData(ret1,ret2,  nRequestID,  bIsLast) ;
	}

	///成交回报
	virtual void OnRtnTrade(::CFfexFtdcTradeField *pTrade) 
	{
		ZJAdapter::CFfexFtdcTradeField ret1 =CastToNet<ZJAdapter::CFfexFtdcTradeField>(pTrade);
		return ((ZJTraderAdapter^)*m_instance)->OnRtnTrade(ret1) ;
	}

	///报单回报
	virtual void OnRtnOrder(::CFfexFtdcOrderField *pOrder)
	{
		ZJAdapter::CFfexFtdcOrderField ret1 =CastToNet<ZJAdapter::CFfexFtdcOrderField>(pOrder);		
		return ((ZJTraderAdapter^)*m_instance)->OnRtnOrder(ret1);
	}

	///执行宣告回报
	virtual void OnRtnExecOrder(::CFfexFtdcExecOrderField *pExecOrder)
	{
		ZJAdapter::CFfexFtdcExecOrderField ret1 =CastToNet<ZJAdapter::CFfexFtdcExecOrderField>(pExecOrder);		
		return ((ZJTraderAdapter^)*m_instance)->OnRtnExecOrder(ret1);
	}

	///报价回报
	virtual void OnRtnQuote(::CFfexFtdcQuoteField *pQuote)
	{
		ZJAdapter::CFfexFtdcQuoteField ret1 =CastToNet<ZJAdapter::CFfexFtdcQuoteField>(pQuote);	
		return ((ZJTraderAdapter^)*m_instance)->OnRtnQuote(ret1);
	}

	///合约交易状态通知
	virtual void OnRtnInstrumentStatus(::CFfexFtdcInstrumentStatusField *pInstrumentStatus)
	{
		ZJAdapter::CFfexFtdcInstrumentStatusField ret1 =CastToNet<ZJAdapter::CFfexFtdcInstrumentStatusField>(pInstrumentStatus);		
		return ((ZJTraderAdapter^)*m_instance)->OnRtnInstrumentStatus(ret1);
	}

	///增加合约通知
	virtual void OnRtnInsInstrument(::CFfexFtdcInstrumentField *pInstrument) 
	{
		ZJAdapter::CFfexFtdcInstrumentField ret1 =CastToNet<ZJAdapter::CFfexFtdcInstrumentField>(pInstrument);	
		return ((ZJTraderAdapter^)*m_instance)->OnRtnInsInstrument(ret1);
	}

	///删除合约通知
	virtual void OnRtnDelInstrument(::CFfexFtdcInstrumentField *pInstrument)
	{
		ZJAdapter::CFfexFtdcInstrumentField ret1 = CastToNet<ZJAdapter::CFfexFtdcInstrumentField>(pInstrument);
		return ((ZJTraderAdapter^)*m_instance)->OnRtnDelInstrument(ret1);
	}

	///增加合约单腿通知
	virtual void OnRtnInsCombinationLeg(::CFfexFtdcCombinationLegField *pCombinationLeg)
	{
		ZJAdapter::CFfexFtdcCombinationLegField ret1 =CastToNet<ZJAdapter::CFfexFtdcCombinationLegField>(pCombinationLeg);	
		return ((ZJTraderAdapter^)*m_instance)->OnRtnInsCombinationLeg(ret1);
	}

	///删除合约单腿通知
	virtual void OnRtnDelCombinationLeg(::CFfexFtdcCombinationLegField *pCombinationLeg)
	{
		ZJAdapter::CFfexFtdcCombinationLegField ret1 =CastToNet<ZJAdapter::CFfexFtdcCombinationLegField>(pCombinationLeg);	
		return ((ZJTraderAdapter^)*m_instance)->OnRtnDelCombinationLeg(ret1);
	}

	///别名定义通知
	virtual void OnRtnAliasDefine(::CFfexFtdcAliasDefineField *pAliasDefine) 
	{
		ZJAdapter::CFfexFtdcAliasDefineField ret1 =CastToNet<ZJAdapter::CFfexFtdcAliasDefineField>(pAliasDefine);	
		return ((ZJTraderAdapter^)*m_instance)->OnRtnAliasDefine(ret1);
	}

	///公告通知
	virtual void OnRtnBulletin(::CFfexFtdcBulletinField *pBulletin) 
	{
		ZJAdapter::CFfexFtdcBulletinField ret1 =CastToNet<ZJAdapter::CFfexFtdcBulletinField>(pBulletin);	
		return ((ZJTraderAdapter^)*m_instance)->OnRtnBulletin(ret1);
	}

	///报单录入错误回报
	virtual void OnErrRtnOrderInsert(::CFfexFtdcInputOrderField *pInputOrder, ::CFfexFtdcRspInfoField *pRspInfo)
	{
		ZJAdapter::CFfexFtdcInputOrderField ret1 =CastToNet<ZJAdapter::CFfexFtdcInputOrderField>(pInputOrder);
		ZJAdapter::CFfexFtdcRspInfoField ret2 = CastToNet<ZJAdapter::CFfexFtdcRspInfoField>(pRspInfo);
		return ((ZJTraderAdapter^)*m_instance)->OnErrRtnOrderInsert(ret1,ret2);
	}

	///报单操作错误回报
	virtual void OnErrRtnOrderAction(::CFfexFtdcOrderActionField *pOrderAction, ::CFfexFtdcRspInfoField *pRspInfo) 
	{
		ZJAdapter::CFfexFtdcOrderActionField ret1 =CastToNet<ZJAdapter::CFfexFtdcOrderActionField>(pOrderAction);
		ZJAdapter::CFfexFtdcRspInfoField ret2 = CastToNet<ZJAdapter::CFfexFtdcRspInfoField>(pRspInfo);
		return ((ZJTraderAdapter^)*m_instance)->OnErrRtnOrderAction(ret1,ret2);
	}

	///报价录入错误回报
	virtual void OnErrRtnQuoteInsert(::CFfexFtdcInputQuoteField *pInputQuote, ::CFfexFtdcRspInfoField *pRspInfo) 
	{
		ZJAdapter::CFfexFtdcInputQuoteField ret1 =CastToNet<ZJAdapter::CFfexFtdcInputQuoteField>(pInputQuote);
		ZJAdapter::CFfexFtdcRspInfoField ret2 = CastToNet<ZJAdapter::CFfexFtdcRspInfoField>(pRspInfo);
		return ((ZJTraderAdapter^)*m_instance)->OnErrRtnQuoteInsert(ret1,ret2);
	}

	///报价操作错误回报
	virtual void OnErrRtnQuoteAction(::CFfexFtdcQuoteActionField *pQuoteAction, ::CFfexFtdcRspInfoField *pRspInfo) 
	{
		ZJAdapter::CFfexFtdcQuoteActionField ret1 =CastToNet<ZJAdapter::CFfexFtdcQuoteActionField>(pQuoteAction);
		ZJAdapter::CFfexFtdcRspInfoField ret2 = CastToNet<ZJAdapter::CFfexFtdcRspInfoField>(pRspInfo);
		return ((ZJTraderAdapter^)*m_instance)->OnErrRtnQuoteAction(ret1,ret2);
	}

	///执行宣告录入错误回报
	virtual void OnErrRtnExecOrderInsert(::CFfexFtdcInputExecOrderField *pInputExecOrder, ::CFfexFtdcRspInfoField *pRspInfo) 
	{
		ZJAdapter::CFfexFtdcInputExecOrderField ret1 =CastToNet<ZJAdapter::CFfexFtdcInputExecOrderField>(pInputExecOrder);
		ZJAdapter::CFfexFtdcRspInfoField ret2 = CastToNet<ZJAdapter::CFfexFtdcRspInfoField>(pRspInfo);
		return ((ZJTraderAdapter^)*m_instance)->OnErrRtnExecOrderInsert(ret1,ret2);
	}

	///执行宣告操作错误回报
	virtual void OnErrRtnExecOrderAction(::CFfexFtdcExecOrderActionField *pExecOrderAction, ::CFfexFtdcRspInfoField *pRspInfo) 
	{
		ZJAdapter::CFfexFtdcExecOrderActionField ret1 =CastToNet<ZJAdapter::CFfexFtdcExecOrderActionField>(pExecOrderAction);
		ZJAdapter::CFfexFtdcRspInfoField ret2 = CastToNet<ZJAdapter::CFfexFtdcRspInfoField>(pRspInfo);
		return ((ZJTraderAdapter^)*m_instance)->OnErrRtnExecOrderAction(ret1,ret2);
	}

private:
	gcroot<ZJTraderAdapter^>* m_instance;
};



ZJTraderAdapter::ZJTraderAdapter(System::String^ pszFlowPath )
{
	std::string ret = StringUtils::Cast_std_string(pszFlowPath);
	const char* pinput = ret.c_str();
	m_pCFfexFtdcTraderApi = ::CFfexFtdcTraderApi::CreateFtdcTraderApi(pinput);
	m_sh = new Interop_Derived_CFfexFtdcTraderSpi(this);
	m_pCFfexFtdcTraderApi->RegisterSpi(m_sh);
}

};
#endif
