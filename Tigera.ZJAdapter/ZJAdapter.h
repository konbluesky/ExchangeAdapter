#ifndef ZJ_ADAPTER_H
#define ZJ_ADAPTER_H

//.net的结构和native的结构是同构的，因此可以直接放进函数调用进行替换
//（注：在最终编译的模块中，这两种定义都存在于编译生成模块，因此文件不选择条件编译）。




#include "CFFEXFtdcUserApiDataType.h"	//基本数据类型定义
#include "CFFEXFtdcUserApiStruct.h"		//基本结构定义
#include "CFFEXFtdcMduserApi.h"			//市场数据获取调用接口
#include "CFFEXFtdcTraderApi.h"			//交易流程调用接口

#pragma managed
//namespace ZJAdapter
//{
	using namespace System::Runtime::InteropServices;
#ifndef NET_EXPORT_STRUCT
	#undef NET_EXPORT_STRUCT
	#define NET_EXPORT_STRUCT [StructLayoutAttribute(LayoutKind:: Sequential, CharSet=CharSet::Ansi)] public value struct
#endif
	
	#include "CFFEXFtdcUserApiDataType.Net.h"	//内容类上1，但是.net版本，文件主体格式未变
	#include "CFFEXFtdcUserApiStruct.Net.h"		//内容类上2，但是.net版本，文件主体格式未变
	#include "CFFEXFtdcMduserApi.Net.h"			//内容类上3，但是.net版本，由于.net开发不习惯使用回调函数和指针，因此做了较大的变动
	#include "CFFEXFtdcTraderApi.Net.h"			//内容类上4，但是.net版本，由于.net开发不习惯使用回调函数和指针，因此做了较大的变动


//};
#pragma unmanaged
#endif