infname = "CFFEXFtdcUserApiDataType.h"
inlines=open(infname).readlines()
outlines=[]
for line in inlines:
    if line.startswith("typedef char") and line.find('[') !=-1:

        startindex = line.find('[')+1
        endindex =line.find(']')

        keyname=line[13:startindex-1]
        print startindex
        keylen = line[startindex:endindex]

        newline="#define " + keyname +" "+ "[MarshalAsAttribute(UnmanagedType::ByValTStr, SizeConst = " + keylen +")]  System::String^"
        outlines.append(newline)
    else:
        outlines.append(line)

outfname= "CFFEXFtdcUserApiDataType.Net.h"
open(outfname,"w").writelines(outlines)

