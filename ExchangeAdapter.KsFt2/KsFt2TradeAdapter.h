#pragma once
#include "stdafx.h"
#include <string>
#include "ITradeAdapter.h"
#include "KsFt2OrderSession.h"
namespace tradeadapter
{
	namespace ksft2
	{



		struct KsFt2InitParam
		{
			tstring AdapterName; //可选
			tstring AdapterVer; //可选
			bool AutoReconnect;
			tstring Addr;
			int			Port;
			tstring KsFt2User;
			tstring KsFt2Pwd;
			int branchno;
			int Timeout;   // 设6*1000

			tstring ClientID;
			tstring Password;


			tstring YYB;


			tstring gydm;
			tstring wtfs;

			tstring fbdm;
			tstring mbyyb;


			tstring dsn;
			tstring dsnusr;
			tstring dsnpwd;

			KsFt2InitParam()
			{
				AdapterName = _T("TigEra");
				AdapterVer = _T("1.01");
				AutoReconnect = true;
				branchno = 0;
			}
		};


		struct KsFt2InitParamA
		{
			std::string AdapterName; //可选
			std::string AdapterVer; //可选
			bool AutoReconnect;
			std::string Addr;
			int			Port;
			std::string KsFt2User;
			std::string KsFt2Pwd;
			int branchno;
			int Timeout;   // 设6*1000

			std::string YYB;
			std::string ClientID;
			std::string Password;

			std::string Gdh_SH; //不需要填 初始化过程会查
			std::string Gdh_SZ; //不需要填，初始化过程会查




			std::string gydm;
			std::string wtfs;

			std::string fbdm;
			std::string mbyyb;

			std::string dsn;
			std::string dsnusr;
			std::string dsnpwd;

			KsFt2InitParamA()
			{
				memset(this, 0, sizeof(KsFt2InitParamA));
				AdapterName = "TigEra";
				AdapterVer = "1.01";
				AutoReconnect = true;
			}

			static KsFt2InitParamA FromKsFt2InitParam(const KsFt2InitParam& input);;
		};


		class  CKsFt2TradeAdapter : ITradeAdapter
		{
		public:
			CKsFt2TradeAdapter(void);
			virtual ~CKsFt2TradeAdapter(void);


			virtual bool Initialize( void* param = NULL );

			virtual void UnInitialize();

			
			virtual bool IsInitialized();

			virtual bool InitializeByXML( const TCHAR* fpath );

			

			virtual bool EntrustOrder(Order order, OrderID* pOutID);

			virtual bool CancelOrder( OrderID orderid );

			

			virtual bool QueryOrder(OrderID id, int* pCount);

			virtual bool QueryDealedOrders( std::list<DealedOrder>* pres );

			virtual bool QueryAccountInfo( BasicAccountInfo* info );

			virtual bool QueryPostions( std::list<PositionInfo>* pPoss,const TCHAR* pstock = NULL );


			
			virtual bool QueryEntrustInfos(std::list<EntrustInfo>* pinfo);


			struct stockinfo
			{
				double zdjg;
			};
			stockinfo GetStockInfo(const TCHAR* code);

			/////////////////
			//
			void GetDealedOrders();

			virtual tstring GetName();



		private:

			//
#ifndef HANDLE_CONN
#define HANDLE_CONN     long
#endif
#pragma warning(disable :4251)
			HANDLE_CONN m_Conn;
			KsFt2InitParamA m_param;
			bool m_Initialzed;

			CKsFt2OrderSession m_OrderSession; 
		};
	}
}