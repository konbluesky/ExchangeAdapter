#pragma once
#pragma pack(push,1)
#include "stdafx.h"
#include <winsock2.h>

#pragma  comment(lib,"winmm.lib")
#pragma  comment(lib, "ws2_32.lib")//winsock2��

namespace tradeadapter
{
	namespace ksft2
	{
		class  KSPXSocket
		{
		public:
			KSPXSocket(void);
			~KSPXSocket(void);
			bool	Connect(LPTSTR lpError);
			bool check_connect(BOOL&new_connect);
			bool send_data(LPCVOID lpData,long sizeData);
			bool recv_data(LPVOID lpData,long sizeData);
			void Close(BOOL bDestroy);	
		public:
			SOCKET	_hSocket;

			char	_srvurl[32];
			TCHAR	_srvurlTCHAR[32];
			WORD	_srvport;

		};
	}
}
#pragma pack(pop)