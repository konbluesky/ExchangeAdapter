#include "StdAfx.h"
#include "UKsFt2Helper.h"
#include "texceptionex.h"

USING_LIBSPACE 

std::string UKsFt2Helper::GetBuySell( EBuySell ope )
{
	if(ope == BuySell::Buy)
		return "1";
	else if(ope == BuySell::Sell)
		return "2";
	else
		throw TException("未定义的买卖类型");
}

std::string UKsFt2Helper::GetStockExchange( tstring code )
{
	if(code[0] == _T('6') )
		return "SH";
	else if(code[0] == _T('0'))
		return "SZ";
	else
		throw TException("未知的股票所属市场，请查字典，并在代码增加");
}
