#pragma once
#include "stdafx.h"
#include "IOrderService.h"
#include <map>
#include <memory>

class COTLDB;

namespace tradeadapter
{
	namespace ksft2
	{
		class CriticalSectionLocker
		{
		public:
			CriticalSectionLocker(LPCRITICAL_SECTION lpCS):m_lpCS(lpCS)
			{
				EnterCriticalSection( m_lpCS );
			}
			~CriticalSectionLocker()
			{
				LeaveCriticalSection( m_lpCS );
			}
		private:
			LPCRITICAL_SECTION m_lpCS;
		};

		class   UKsFt2Helper
		{
		public:
			enum EntrustStatusChar
			{
				Weichengjiao = 0,			//空字符，未成交
				Wcj_Weibao = '0',			//未成交-未报
				Wcj_Daibao = '1',			//未成交-待报
				Wcj_Yibao = '2',			//未成交-已报
				Wcj_Daiche = '3',			//未成交-已报待撤
				PartFilledAndCancelled= '4',	//未成交-部成待撤
				BufenChedan = '5',			//部分撤单
				ChangwaiChedan = '6',		//场外撤单
				PartFilled = '7',		//部分成交
				Filled = '8',			//已成交
				Feidan = '9',				//废单
				ChangneiChedan = 'A',		//场内撤单
			};
			static Order GetTodayOrderInfoByID(int orderid);
			
			static std::string SelectGDHFromProduct(tstring code , std::string sh,std::string sz);
			static std::string GetJYSByContractCode(tstring code);			
			static void SaveOrderInfo(int wth,Order order, std::string jys,std::string gdh);
			static void InsertEntrustOrderInfo( int Date, int Time, const char* clientID, const char* StockAccount, const char* ContractCode, int buysell, double Price, int Amount, const char* Comment, int orderid, int Status );
			static void InsertErrorLog(int Date,int Time, const char* ExProvider,int errcode,const char* errmsg);
			static long GetJYLBFromBuySell(EBuySell ope);
			static EBuySell ToBuySell(int JYLB );
			static EBuySell ToBuySell(const char* name);

			

			static bool InitializeDB(const char* dsn, const char* usr,const char* pwd);
			static bool LoadTodayEntrustOrdersFromDB();
		private:
			
#pragma warning(disable :4251)
			//注：下面两个变量是相关联的，有可能服务器隔夜未重启，因此每次取GetTodayEntrustOrdersFromDB()都要判断时间
			static std::map<OrderID, Order>  m_todayOrder;
			static int m_today;


			static std::shared_ptr<COTLDB> m_pdb;

			
		};		
	}
}