#include "StdAfx.h"
#include "ustringutils.h"
#include "ucodehelper.h"
#include "tinyxml.h"
#include "tconfigsetting.h"
#include "umaths.h"
#include "KsFt2Helper.h"
#include "ucomhelper.h"
#include "KsFt2TradeAdapter.h"
#include "stdio.h"
#include "LogHelper.h"



#pragma comment(lib,"comsuppw.lib")
#pragma warning(disable : 4482)
USING_LIBSPACE
namespace tradeadapter
{
	namespace ksft2
	{
		CKsFt2TradeAdapter::CKsFt2TradeAdapter(void): m_Conn(NULL)
		{
		}

		CKsFt2TradeAdapter::~CKsFt2TradeAdapter(void)
		{
		}

		bool CKsFt2TradeAdapter::Initialize( void* param /*= NULL */ )
		{
			KsFt2InitParam tparam = *(KsFt2InitParam*)param;
			m_param = KsFt2InitParamA::FromKsFt2InitParam(tparam);
			bool bRet = false;

			//只能调用一次
			//连接柜台及登陆柜台
			bRet = m_OrderSession.Connect(m_param.Addr.c_str(),m_param.Port,m_param.KsFt2User.c_str(),m_param.KsFt2Pwd.c_str(),m_param.branchno,m_param.Timeout);
			if(bRet)
			{
				bRet = m_OrderSession.logon_server();
				if(!bRet)
				{
					WriteDetailLog(_T("error"),LTALL,(_T("登陆柜台接口失败")));				
					return bRet;
				}
			}
			else
			{
				UnInitialize();				
				return bRet;
			}		
			
			WriteLog(_T("log"),LTALL,_T("初始化Ok"));
			m_Initialzed = true;

			bRet = UKsFt2Helper::InitializeDB(m_param.dsn.c_str(),m_param.dsnusr.c_str(),m_param.dsnpwd.c_str());
			bRet = UKsFt2Helper::LoadTodayEntrustOrdersFromDB();

			//得到股东账号信息
			m_param.Gdh_SH =m_OrderSession._my_exch_sh_account;
			m_param.Gdh_SZ = m_OrderSession._my_exch_sz_account;
			return true;

		}
		void CKsFt2TradeAdapter::UnInitialize()
		{			
			BOOL bRet = m_OrderSession.logout_server();
			if(bRet)
			{
				WriteLog(_T("log"),LTALL,_T("结束Ok"));
			}
			else
			{
				WriteDetailLog(_T("error"),LTALL,(_T("结束接口失败")));
			}
		}
		bool CKsFt2TradeAdapter::IsInitialized()
		{
			return m_Initialzed;
		}

		bool CKsFt2TradeAdapter::InitializeByXML( const TCHAR* fpath )
		{
			TConfigSetting cfg;
			cfg.LoadConfig(fpath);

			KsFt2InitParam param;

			param.AdapterName = U82TS(cfg["configuration"]["adaptername"].EleVal().c_str());
			param.AdapterVer = U82TS(cfg["configuration"]["adapterver"].EleVal().c_str());
			param.AutoReconnect = cfg["configuration"]["autoreconnect"] .EleVal()== "true";
			param.Addr = U82TS(cfg["configuration"]["addr"].EleVal().c_str());
			param.Port = UMaths::ToInt32( cfg["configuration"]["port"].EleVal()  );
			param.KsFt2User = U82TS(cfg["configuration"]["ksft2user"].EleVal().c_str());
			param.KsFt2Pwd = U82TS(cfg["configuration"]["ksft2pwd"].EleVal().c_str());
			param.branchno = UMaths::ToInt32( cfg["configuration"]["branchno"].EleVal()  );
			param.Timeout = UMaths::ToInt32( cfg["configuration"]["timeout"] .EleVal() );
			param.ClientID = U82TS(cfg["configuration"]["clientid"].EleVal().c_str());
			param.Password = U82TS(cfg["configuration"]["password"].EleVal().c_str());
			param.YYB = U82TS(cfg["configuration"]["yyb"].EleVal().c_str());
			param.gydm = U82TS(cfg["configuration"]["gydm"].EleVal().c_str());
			param.wtfs = U82TS(cfg["configuration"]["wtfs"].EleVal().c_str());
			param.fbdm = U82TS(cfg["configuration"]["fbdm"].EleVal().c_str());
			param.mbyyb = U82TS(cfg["configuration"]["mbyyb"].EleVal().c_str());
			param.dsn = U82TS(cfg["configuration"]["dsn"].EleVal().c_str());
			param.dsnusr = U82TS(cfg["configuration"]["dsnusr"].EleVal().c_str());
			param.dsnpwd = U82TS(cfg["configuration"]["dsnpwd"].EleVal().c_str());

			bool ret = this->Initialize(&param);
			return ret;
		}

		bool CKsFt2TradeAdapter::EntrustOrder(Order order, OrderID* pOutID)
		{
			*pOutID = tradeadapter::InvalidOrderId;
			if( !m_OrderSession.TestHeart() )
				m_OrderSession.ReConnect();

			int nWTH = 0;			
			char errormsg[200]={0};
			if(m_OrderSession.place_order(order,nWTH,errormsg))
			{				
				long wth = nWTH; //委托号
				std::cout<<"wth:" <<wth<<std::endl;				
				if(wth !=0 )
				{
				}
				else
				{
					wth  = -1;
				}

				std::string gdh = UKsFt2Helper::SelectGDHFromProduct(order.ContractCode,m_param.Gdh_SH,m_param.Gdh_SZ);
				std::string jys = UKsFt2Helper::GetJYSByContractCode(order.ContractCode);
				UKsFt2Helper::SaveOrderInfo(wth,order,jys,gdh);
				*pOutID =  (int)wth;
				return true;
			}
			else
			{	//errormsg			
				return false;
			}
		}

		

		//#ifndef FID_ZXSZ 
#pragma message("this id is not defined in ksft2 current version")
#define FID_ZXSZ 760
		//#endif

		bool CKsFt2TradeAdapter::QueryPostions( std::list<PositionInfo>* pPoss,const TCHAR* pstock  )
		{
			if( !m_OrderSession.TestHeart() )
				m_OrderSession.ReConnect();

			pPoss->clear();
			if(!m_OrderSession.query_position(pPoss))
			{
				pPoss->clear();
				return false;
			}
			return true;
		}
		
		bool CKsFt2TradeAdapter::CancelOrder(OrderID orderid)
		{
			if( !m_OrderSession.TestHeart() )
				m_OrderSession.ReConnect();

			int nRet = 0;
			nRet = m_OrderSession.cancel_order(orderid);
			if(nRet)
			{
				//int code = sess.GetResultLong(FID_CODE);
				//std::string msg= sess.GetResultString(FID_MESSAGE);				
				//std::cout<<code<<"   "<<msg<<std::endl;
				if(nRet ==2)//已经全部成交了
				{
					return false;
				}
				return true;
			}
			else
			{
				return false;
			}
		}
		bool CKsFt2TradeAdapter::QueryOrder(OrderID id, int* pCount)
		{
			*pCount = 0;
			if( !m_OrderSession.TestHeart() )
				m_OrderSession.ReConnect();

			std::list<DealedOrder> dealedorders;
			if(this->QueryDealedOrders(&dealedorders))
			{
				for(std::list<DealedOrder>::iterator it = dealedorders.begin(); it != dealedorders.end(); ++it)
				{
					if( it->entrust_no == id)
					{
						*pCount =  (int)it->business_amount;
						return true;
					}
				}
			}
			return false;
		}

		

		bool CKsFt2TradeAdapter::QueryDealedOrders( std::list<DealedOrder>* pres )
		{
			if( !m_OrderSession.TestHeart() )
				m_OrderSession.ReConnect();

			pres->clear();
			if(!m_OrderSession.query_dealedorders(pres))
			{
				pres->clear();
				return false;
			}
			return true;
		}

		bool CKsFt2TradeAdapter::QueryAccountInfo( BasicAccountInfo* info )
		{
			if( !m_OrderSession.TestHeart() )
				m_OrderSession.ReConnect();

			m_OrderSession.query_accountname(info);
			m_OrderSession.query_accountsummary( info );
			return true;	
		}

		

		void CKsFt2TradeAdapter::GetDealedOrders()
		{

		}


		/*
		CKsFt2TradeAdapter::stockinfo CKsFt2TradeAdapter::GetStockInfo( const TCHAR* code )
		{
			CKsFt2TradeAdapter::stockinfo info;
			CFixSession sess(m_Conn);
			sess.BeginJob(m_param,B_FUN_QUOTE_GETHQ);

			sess.AddField(FID_ZQDM,UCodeHelper::ToStr(code).c_str());
			std::string jys = UKsFt2Helper::GetJYSByContractCode(code);
			sess.AddField(FID_JYS,jys.c_str());
			if( sess.CommitJob() )
			{
				double zdjg = sess.GetResultDouble(FID_ZDBJ);
				info.zdjg = zdjg;
			}
			return info;
		}
		*/		


		bool CKsFt2TradeAdapter::QueryEntrustInfos(std::list<EntrustInfo>* pinfo)
		{
			if( !m_OrderSession.TestHeart() )
				m_OrderSession.ReConnect();

			//查询委托
			bool bRet = m_OrderSession.query_entrustinfo( pinfo );

			//查询成交，将每笔委托对应的成交时间，填入上面查出的委托列表中
			std::list<DealedOrder> dealtOrders;
			m_OrderSession.query_dealedorders( &dealtOrders );

			for ( std::list<EntrustInfo>::iterator it = pinfo->begin(); it != pinfo->end(); ++it )
			{
				for ( std::list<DealedOrder>::iterator itDealt = dealtOrders.begin(); itDealt != dealtOrders.end(); ++itDealt )
				{
					if( it->orderid == itDealt->entrust_no )
					{
						it->operdate = itDealt->date;
						it->opertime = itDealt->business_time;
					}
				}
			}

			return bRet;
		}

		tstring CKsFt2TradeAdapter::GetName()
		{
			throw std::logic_error("The method or operation is not implemented.");
		}


		tradeadapter::ksft2::KsFt2InitParamA KsFt2InitParamA::FromKsFt2InitParam(const KsFt2InitParam& input)
		{
			KsFt2InitParamA ret;


			ret.AdapterName = TS2GB(input.AdapterName);
			ret.AdapterVer = TS2GB(input.AdapterVer);
			ret.AutoReconnect = input.AutoReconnect;
			ret.Addr = TS2GB(input.Addr);
			ret.Port = input.Port;
			ret.KsFt2User = TS2GB(input.KsFt2User);
			ret.KsFt2Pwd = TS2GB(input.KsFt2Pwd);
			ret.branchno = input.branchno;
			ret.Timeout = input.Timeout;
			ret.ClientID = TS2GB(input.ClientID);
			ret.Password = TS2GB(input.Password);
			ret.YYB = TS2GB(input.YYB);


			ret.gydm = TS2GB(input.gydm);
			ret.wtfs = TS2GB(input.wtfs);

			ret.fbdm = TS2GB(input.fbdm);
			ret.mbyyb = TS2GB(input.mbyyb);

			ret.dsn = TS2GB(input.dsn);
			ret.dsnusr = TS2GB(input.dsnusr);
			ret.dsnpwd = TS2GB(input.dsnpwd);



			return ret;
		}

	}
}
