#include "stdafx.h"
#include "ucodehelper.h"
#include "ustringutils.h"
#include "unetutils.h"
#include "IOrderService.h"
#include "KsFt2OrderSession.h"
#include "KsFt2Helper.h"


#include "tinyxml.h"
#include "tconfigsetting.h"


#include "umaths.h"

#include "ucomhelper.h"

#include "stdio.h"
#include "LogHelper.h"
#include "stdarg.h"

USING_LIBSPACE
tradeadapter::ksft2::CKsFt2OrderSession::CKsFt2OrderSession(bool bAsync)
{
	m_bAsync = bAsync;
	if( m_bAsync )
	{
		m_lnCommHead	= 19;
		m_lnBizHead		= 58;
	}
	else
	{
		m_lnCommHead = m_lnBizHead	= 0;
	}

	if(_sendBuffer=new char[512L])
		memset(_sendBuffer,0,512L);
	if(_recvBuffer=new char[4*1024L])
		memset(_recvBuffer,0,4*1024L);	
	_recvSize=4*1024L;
	
	memset(_srvAccount,0,sizeof(_srvAccount));	
	memset(_srvPwd,0,sizeof(_srvPwd));	
	m_reqID = 0;

	memset(_my_exch_sh_account,0x00,20);
	memset(_my_exch_sz_account,0x00,20);	
	memset(_recvArray,0x00,200*500);

	m_bLogin = false;
	m_bConnect = false;
	_branchno = 0;

	InitializeCriticalSection(&m_critiSec_Counter);
	InitializeCriticalSection(&m_critiSec_ReqID);

	m_hExitEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_hHeartThread = NULL;
}

tradeadapter::ksft2::CKsFt2OrderSession::~CKsFt2OrderSession()
{

	if(_sendBuffer) delete[] _sendBuffer;
	if(_recvBuffer) delete[] _recvBuffer;

	SetEvent(m_hExitEvent);

	if( m_hHeartThread )
	{
		WaitForSingleObject( m_hHeartThread, INFINITE );
		CloseHandle(m_hHeartThread);
		m_hHeartThread = NULL;
	}

	CloseHandle(m_hExitEvent);
	m_hExitEvent = NULL;

	DeleteCriticalSection(&m_critiSec_Counter);
	DeleteCriticalSection(&m_critiSec_ReqID);
	WSACleanup();
}

/*
功能:
	准备通信包头和CRC包尾
	只有在连接异步网关时才需要调用改函数，SpxSwitch标准同步网关不需要包头和包尾
	如果有包体，需在调用该函数之前，填好包体内容( _sendBuffer+m_headLength )
	如果包体过长，该函数会压缩包体
入参:
	dataType	:	包类型
	dataLength	:	包体长度
返回值:
	整个通信数据包的长度（包括通信包头，包体，CRC尾）
*/

int tradeadapter::ksft2::CKsFt2OrderSession::FillCommHeadAndCRC( DataType dataType, int dataLength )
{
	//标准同步网关，不需要通信包头和CRC包尾
	if( !m_bAsync )
		return dataLength;

	//异步网关，准备包头和CRC包尾


	//填充包头
	_sendBuffer[0] = dataType;
	if( dataType == DTPing 
		|| dataType != DTPintRet )
		return m_lnCommHead;

	//TO DO	标准同步网关，不需要通信包头和CRC包尾，该函数暂时不需要实现

	//如果包体超过限制，压缩并分段发送

	//填充CRC包尾

	return m_lnCommHead + dataLength + 8;
}

int tradeadapter::ksft2::CKsFt2OrderSession::FillAsyncLogonData( char* pstrLogon )
{
	pstrLogon[ 0 ] = 0;
	pstrLogon[ 1 ] = 0;
	strcpy( pstrLogon + 2, "ExchangeAdapter.KsFt2" );//char[30]
	strcpy( pstrLogon + 32, "1.0.0" );						//char[30]
	strcpy( pstrLogon + 62, "Ex....KsFt2.dll" );			//char[20]
	strcpy( pstrLogon + 82, "KsFt2" );						//char[8]
	strcpy( pstrLogon + 90, TS2GB(UNetUtils::GetMacAddress()).c_str() );	//char[22]
	tstrings ipList = UNetUtils::GetIpAddress();
	if( ipList.size() > 0 )
		strcpy( pstrLogon + 112, TS2GB( ipList.at(0) ).c_str() );	//char[20]
	else
		strcpy( pstrLogon + 112, "127.0.0.1" );

	strcpy( pstrLogon + 132, _srvAccount );		//char[20]
	strcpy( pstrLogon + 152, _srvPwd );			//char[20]

	return 428;
}


/*
功能:
	准备业务包头
	只有在连接异步网关时才需要调用改函数，SpxSwitch标准同步网关不需要包头和包尾
*/
int tradeadapter::ksft2::CKsFt2OrderSession::FillBizHead()
{
	//TO DO 	标准同步网关，不需要业务包头，该函数暂时不需要实现
	return 0;
}

/*
功能:
	填充业务数据包字符串
参数:
	buffer			用来存储业务数据包的缓冲区
	maxFieldCount	数据包中数据域域的个数
	...				int, const char*, int, const char*, int, const char*, int .....
					后续参数依次为 域编号，域数据，域编号，与数据....，最后以一个int参数传递-1，代表参数传递结束
					准备再增加一个指定buffer缓冲区大小的传入参数...
返回值:
	生成的业务数据包字符串的长度
示例:
	int length = FillBizData( buffer, 5,
								0, "R",
								1, "127.0.0.1",
								3, "12345"
								-1 );
	上述示例输出结果:
	buffer：		R|127.0.0.1||12345||
	return：		20
*/

int tradeadapter::ksft2::CKsFt2OrderSession::FillBizData( char* buffer, int maxFieldCount, ...)
{
	va_list arg_ptr;
	va_start(arg_ptr,maxFieldCount);   //以最后一个固定参数的地址为起点确定变参的内存起始地址

	buffer[0] = 0x00;
	for( int i = 0; i < maxFieldCount; i++ )
	{
		int index = va_arg(arg_ptr,int);					//域编号
		if( index < 0 )										//编号-1代表后面不再有参数
			break;

		const char* val = va_arg( arg_ptr, const char* );	//域内容

		while( i < index )	//index前面没有指定的域，填上空值
		{
			strcat( buffer, "|" );
			i++;
		}

		strcat( buffer, val );
		strcat( buffer, "|" );
	}

	return strlen( buffer );
}

/*
FillBizData的安全版本，强参数类型，可以在编译时发现不小心输入的错误参数类型
*/
int tradeadapter::ksft2::CKsFt2OrderSession::FillBizData2( char* buffer, int maxFieldCount, 
				 int index1, const char* data1,int index2, const char* data2, int index3, const char* data3, int index4, const char* data4, int index5, const char* data5,
				 int index6, const char* data6,int index7, const char* data7, int index8, const char* data8, int index9, const char* data9, int index10, const char* data10,
				 int index11, const char* data11,int index12, const char* data12, int index13, const char* data13, int index14, const char* data14, int index15, const char* data15,
				 int index16, const char* data16,int index17, const char* data17, int index18, const char* data18, int index19, const char* data19, int index20, const char* data20,
				 int index21, const char* data21,int index22, const char* data22, int index23, const char* data23, int index24, const char* data24, int index25, const char* data25,
				 int index26, const char* data26,int index27, const char* data27, int index28, const char* data28, int index29, const char* data29, int index30, const char* data30)
{
	return FillBizData( buffer, maxFieldCount,
		index1, data1, index2, data2, index3, data3, index4, data4, index5, data5, index6, data6, index7, data7, index8, data8, index9, data9, index10, data10,
		index11, data11, index12, data12, index13, data13, index14, data14, index15, data15, index16, data16, index17, data17, index18, data18, index19, data19, index20, data20,
		index21, data21, index22, data22, index23, data23, index24, data24, index25, data25, index26, data26, index27, data27, index28, data28, index29, data29, index30, data30 );

}

int tradeadapter::ksft2::CKsFt2OrderSession::RecvArray()
{
	memset(_recvBuffer,0x00,_recvSize);

	if(!m_socket.recv_data(_recvBuffer, _recvSize)) 
		return 0;
	else
		return transferRecvToArray();
}

int tradeadapter::ksft2::CKsFt2OrderSession::transferRecvToArray(void)
{
	//memcpy(_recvBuffer_backup,_recvBuffer,4*1024L);
	int index(0),i(0);
	char* lpBuffer;
	lpBuffer=strtok(_recvBuffer,"|");
	while(lpBuffer!=NULL)
	{
		i=0;
		do{}while(lpBuffer[i++]==' ');
		sprintf(_recvArray[index++],"%s",&lpBuffer[i-1]);
		lpBuffer=strtok(NULL,"|");
	}
	return index;
}
DWORD WINAPI tradeadapter::ksft2::CKsFt2OrderSession::HeartThread(void *pParam)
{
	return 0;	//TO DO 暂时有bug:线程互锁

	bool bNeedReconn = false;

	CKsFt2OrderSession * pOrderSessi = (CKsFt2OrderSession*)pParam;
	while(WaitForSingleObject(pOrderSessi->m_hExitEvent, 0) == WAIT_TIMEOUT)
	{
		if( pOrderSessi->m_bConnect && pOrderSessi->m_bLogin )
		{
			if( ! pOrderSessi->TestHeart() )	//心跳检测，异常重连
			{
				bNeedReconn = true;
				continue;
			}
		}
		else if( ( pOrderSessi->_my_exch_sh_account[0] || pOrderSessi->_my_exch_sz_account[0] )	//表示是否已成功登陆过柜台
			&& bNeedReconn )	
		{
			bool bSuc = pOrderSessi->ReConnect();
			if( bSuc )
				bNeedReconn = false;
		}

		Sleep(1000*30);
	}
	return 0;
}
int tradeadapter::ksft2::CKsFt2OrderSession::GetReqID()
{
	CriticalSectionLocker csLocker(&m_critiSec_ReqID);
	m_reqID++;
	return m_reqID;
}
bool tradeadapter::ksft2::CKsFt2OrderSession::Connect( const char* addr,int port,const char* usr,const char* pwd,int branchno,int timeout/*=1000 */ )
{
	bool bRet = false;
	m_bConnect = false;
	strcpy(m_socket._srvurl,addr);
	m_socket._srvport = port;
	
	strcpy(_srvAccount,usr);
	strcpy(_srvPwd,pwd);
	_branchno = branchno;
	bRet = m_socket.Connect(strError);
	if(!bRet)
	{
		WriteDetailLog(_T("error"),LTALL,(_T("连接服务器%s:%d  usr: %s ,pwd: %s失败"),addr,port,usr,pwd));
		return bRet;
	}
	m_bConnect = bRet;

	//启动心跳线程
	if( !m_hHeartThread )
		m_hHeartThread = CreateThread(NULL, 0, HeartThread, this, 0, NULL);

	return bRet;
}
bool tradeadapter::ksft2::CKsFt2OrderSession::logon_server()
{
	CriticalSectionLocker csLocker(&m_critiSec_Counter);
	m_bLogin = false;

	//准备数据包
	memset(_sendBuffer,0,sizeof(_sendBuffer));
	if( m_bAsync )	//异步网关登录包
	{
		int nLogonSize = FillAsyncLogonData( _sendBuffer + m_lnCommHead );
		_sendSize = FillCommHeadAndCRC( DTLogin, nLogonSize );
	}
	else			//标准同步网关，使用61号功能验证用户名密码
	{
		_sendSize = FillBizData2( _sendBuffer, 12,
									0,	"R",
									1,	TS2GB(UNetUtils::GetMacAddress()).c_str(),
									2,	UStrConvUtils::Format( "%d",  GetReqID() ).c_str(),
									3,	"61",
									4,	UStrConvUtils::Format( "%d", _branchno ).c_str(),
									5,	"0",
									7,	_srvAccount,
									8,	_srvPwd,
									9,	"RJWT",
									10,	UStrConvUtils::Format( "%d", _branchno ).c_str(),
									11,	"1"
								);
	}

	//发送
	if(!m_socket.send_data(_sendBuffer, _sendSize)) 
	{
		return false;
	}

	//接受返回的字符串数组
	int nArray = RecvArray();

	if( nArray < 4 || _recvArray[3][0]!='Y')
	{
		return false;
	}

	//解析股东账户信息
	int exch_count=atoi(_recvArray[9]);		
	if( exch_count > 0 )
		ReadAccountFromA61();

	m_bLogin = true;
	return true;
}

bool tradeadapter::ksft2::CKsFt2OrderSession::logout_server()
{	
	m_socket.Close(TRUE);	
	m_bLogin = false;
	m_bConnect = false;

	return true;
}
bool tradeadapter::ksft2::CKsFt2OrderSession::place_order(Order ord, int& nWTH,char *errMsg)
{
	CriticalSectionLocker csLocker(&m_critiSec_Counter);

	//买卖类型（数据包第9域）
	char buy_sell_type[3]={0};
	buy_sell_type[0] = ord.OpeDir == tradeadapter::EBuySell::eBuy? '1' : '2';
	buy_sell_type[1] = ord.pricemode == tradeadapter::EPriceMode::eLimitPrice ? '0' : '5';
	
	//股东账号 和 市场名称
	std::string gdh = UKsFt2Helper::SelectGDHFromProduct(ord.ContractCode,_my_exch_sh_account,_my_exch_sz_account);
	std::string market = UKsFt2Helper::GetJYSByContractCode( ord.ContractCode );

	//填充报文数据包
	_sendSize = FillBizData2( _sendBuffer, 24,
								0, "R",
								1, TS2GB(UNetUtils::GetMacAddress()).c_str(),
								2, UStrConvUtils::Format( "%d", GetReqID() ).c_str(),
								3, "3",
								4, LIB_NS::UStrConvUtils::Format( "%d", _branchno ).c_str(),
								5, market == "SH" ? "1" : "2",
								7, gdh.c_str(),
								8, TS2GB( ord.ContractCode ).c_str(),
								9, buy_sell_type, 
								10, UStrConvUtils::Format( "%d", ord.Amount ).c_str(),
								11, UStrConvUtils::Format( "%0.3f", ord.Price ).c_str(),
								12, "RJWT",
								17, "1"
							);

	//发送
	if(!m_socket.send_data(_sendBuffer, _sendSize)) 
	{
		return false;
	}

	//接收
	int rtn = RecvArray();

	//返回委托号或者错误信息
	if(_recvArray[3][0]!='Y')
	{
		nWTH = -1;
		memcpy(errMsg,_recvArray[4],strlen(_recvArray[4]));
		return false;
	}

	nWTH = atoi(_recvArray[5]);		//TO DO TBD 返回 4委托序号 还是 5委托批号,目前来看，两个字段内容相同??

	return true;
}
bool tradeadapter::ksft2::CKsFt2OrderSession::query_position(std::list<PositionInfo>* pPoss)
{
	CriticalSectionLocker csLocker(&m_critiSec_Counter);

	//填充数据包
	_sendSize = FillBizData2( _sendBuffer, 20,
								0, "R",
								1, TS2GB(UNetUtils::GetMacAddress()).c_str(),
								2, UStrConvUtils::Format( "%d", GetReqID() ).c_str(),
								3, "10",
								4, LIB_NS::UStrConvUtils::Format( "%d", _branchno ).c_str(),
								7, _srvAccount,
								14, "100",
								15, "1",
								16, "RJWT",
								18, "0"
							);

	//发送
	if(!m_socket.send_data(_sendBuffer, _sendSize)) 
	{
		return FALSE;
	}

	//接收首包
	int rtn = RecvArray();
	if( rtn < 5 || _recvArray[3][0] != 'Y' )
	{
		return FALSE;
	}

	//接收后续包
	int position_count = atoi( _recvArray[4] );
	for( int index=0; index < position_count; index++ )
	{
		//填充0号功能
		_sendSize = FillBizData2( _sendBuffer, 6,
									0, "R",
									1, TS2GB(UNetUtils::GetMacAddress()).c_str(),
									2, LIB_NS::UStrConvUtils::Format( "%d", GetReqID() ).c_str(),
									3, "0",
									4, LIB_NS::UStrConvUtils::Format( "%d", _branchno ).c_str(),
									5, "RJWT"
								);

		//发送
		if(!m_socket.send_data(_sendBuffer, _sendSize)) 
		{
			break;
		}

		//接收返回
		int rtn = RecvArray();

		int stock_can_sold_volumn = atoi(_recvArray[8]);
		//if( stock_can_sold_volumn == 0 )				//TO DO TBD
		//	continue;


		PositionInfo info;
		info.stock_account	=	GB2TS( _recvArray[4] );
		info.code			=	GB2TS(_recvArray[5]);
		info.stock_name		=	GB2TS( _recvArray[6] );
		info.current_amount =	atoi( _recvArray[7] );
		info.enable_amount	=	stock_can_sold_volumn;
		info.cost_price		=	atof(_recvArray[11]);
		info.last_price		=	atof(_recvArray[12]);
		info.income_balance	=	atof( _recvArray[10] );
		info.market_value	=	atof( _recvArray[9] );

		pPoss->push_back(info);
	}

	return true;
}
int tradeadapter::ksft2::CKsFt2OrderSession::cancel_order(OrderID orderid)
{
	CriticalSectionLocker csLocker(&m_critiSec_Counter);

	int iRtn(0);

	//填充数据包
	tstring stockCode;
	Order orderinfo = UKsFt2Helper::GetTodayOrderInfoByID(orderid);
	if( orderinfo.ContractCode.size() > 0 )
		stockCode = orderinfo.ContractCode;
	else
	{
		std::list<EntrustInfo> eiList;
		if( query_entrustinfo( &eiList, orderid ) )
		{
			if( eiList.size() == 1 )
			{
				stockCode = eiList.front().stkcode;
			}
		}
	}

	std::string market = UKsFt2Helper::GetJYSByContractCode( stockCode );

	_sendSize = FillBizData2( _sendBuffer, 24,
								0, "R",
								1, TS2GB(UNetUtils::GetMacAddress()).c_str(),
								2, UStrConvUtils::Format( "%d", GetReqID() ).c_str(),
								3, "4",
								4, LIB_NS::UStrConvUtils::Format( "%d", _branchno ).c_str(),
								5, market == "SH" ? "1" : "2",
								7, _srvAccount,
								8, LIB_NS::UStrConvUtils::Format( "%d", orderid ).c_str(),
								9, "RJWT"
							);

	//发送
	if(!m_socket.send_data(_sendBuffer, _sendSize)) 
	{
		return iRtn;
	}

	//接收
	int rtn = RecvArray();
	if( rtn < 4 )
		return iRtn;

	if(_recvArray[3][0]!='Y')
	{
		if( !strcmp( _recvArray[4], "该委托已经撤过单!" ) )
		{
			iRtn=2;
		}

		return iRtn;
	}

	iRtn=1;
	return iRtn;
}

/*
	查委托信息
*/
bool tradeadapter::ksft2::CKsFt2OrderSession::query_entrustinfo(std::list<EntrustInfo>* pinfo, int orderID)
{
	CriticalSectionLocker csLocker(&m_critiSec_Counter);

	//对于按委托时间或委托时间倒序的查询，不支持带索引的查询
	//这个功能有bug,使用索引合同号或者开始日期结束日期,无效,所以只能按照合同号单个查询

	//填充数据包
	_sendSize = FillBizData2( _sendBuffer, 24,
								0,	"R",
								1,	TS2GB(UNetUtils::GetMacAddress()).c_str(),
								2,	UStrConvUtils::Format( "%d", GetReqID() ).c_str(),
								3,	"14",
								4,	UStrConvUtils::Format( "%d", _branchno ).c_str(),
								7,	_srvAccount,
								8,	UStrConvUtils::Format( "%d", UTimeUtils::GetNowDate() ).c_str(),
								9,	UStrConvUtils::Format( "%d", UTimeUtils::GetNowDate() ).c_str(),
								10, orderID > 0 ? UStrConvUtils::Format( "%d", orderID ).c_str() : "",
								11, "",
								12, "100",
								15, "RJWT",
								17, "2"
							);

	//发送
	if(!m_socket.send_data(_sendBuffer, _sendSize)) 
	{
		return FALSE;
	}

	//接收首包
	int rtn = RecvArray();
	if( rtn < 5 || _recvArray[3][0]!='Y' )
	{
		return false;
	}

	//请求并接收后续包
	int wt_count=atoi(_recvArray[4]);
	for (int i = 0; i < wt_count; i++)		
	{
		//填充0号功能
		_sendSize = FillBizData2( _sendBuffer, 6,
									0, "R",
									1, TS2GB(UNetUtils::GetMacAddress()).c_str(),
									2, LIB_NS::UStrConvUtils::Format( "%d", GetReqID() ).c_str(),
									3, "0",
									4, LIB_NS::UStrConvUtils::Format( "%d", _branchno ).c_str(),
									5, "RJWT"
								);
		//发送
		if(!m_socket.send_data(_sendBuffer, _sendSize)) 
		{				
			return FALSE;
		}

		//接收	
		int rtn = RecvArray();
		if( rtn < 16 )
			return false;

		if( atol(_recvArray[15]) < UTimeUtils::GetNowDate() )
			break;

		EntrustInfo info;

		int orderState		= _recvArray[17][0];
		info.cflag			= orderState==UKsFt2Helper::Filled ? eFailed : eSuccessed;	//取消标志		//TO DO TBD
		info.matchamt		= atof( _recvArray[25] );	//成交金额
		info.matchqty		= atoi( _recvArray[10] );	//成交数量
		info.Ope			= UKsFt2Helper::ToBuySell( atoi( _recvArray[19] ) );		//委托方向
		info.orderid		= atoi( _recvArray[9] );						//委托id
		info.orderdate		= atoi( _recvArray[15] );						//委托日期
		info.ordertime		= UTimeUtils::FormTimeString( GB2TS(_recvArray[14]));	//委托时间
		info.stkcode		= GB2TS( _recvArray[5] );			//股票代码
		info.stkname		= GB2TS( _recvArray[6] );			//股票名称
		info.orderqty		= atoi(_recvArray[7]);							//委托数量
		info.orderprice		= atof(_recvArray[8]);							//委托价格
		info.orderfrzamt	= info.orderqty * info.orderprice;	//冻结金额
		info.futureOC		= ( info.Ope == eBuy ) ? eOpen : eClose;

		//只返回委托日期和时间，没有成交日期和时间.调用当前函数之后，再次调用成交查询，获取成交时间并填入字段
		info.operdate		= info.orderdate;	//成交日期
		info.opertime		= info.ordertime;	//成交时间

		pinfo->push_back( info );
	}

	return true;
}

/*
	查委托单成交状态
*/
bool tradeadapter::ksft2::CKsFt2OrderSession::query_dealedorders(std::list<DealedOrder>* pres)
{
	CriticalSectionLocker csLocker(&m_critiSec_Counter);

	//填充数据包
	_sendSize = FillBizData2( _sendBuffer, 24,
								0, "R",
								1, TS2GB(UNetUtils::GetMacAddress()).c_str(),
								2, LIB_NS::UStrConvUtils::Format( "%d", GetReqID() ).c_str(),
								3, "13",
								4, LIB_NS::UStrConvUtils::Format( "%d", _branchno ).c_str(),
								7, _srvAccount,
								8, "100",
								12, "RJWT"
							);

	//发送
	if(!m_socket.send_data(_sendBuffer, _sendSize)) 
	{
		return FALSE;
	}

	//接收首包
	int rtn = RecvArray();
	if( rtn < 5 || _recvArray[3][0]!='Y' )
	{
		return false;
	}

	//请求并接收后续包
	int wt_count=atoi(_recvArray[4]);
	for (int i = 0; i < wt_count; i++)		
	{
		//填充0号功能
		_sendSize = FillBizData2( _sendBuffer, 6,
									0, "R",
									1, TS2GB(UNetUtils::GetMacAddress()).c_str(),
									2, LIB_NS::UStrConvUtils::Format( "%d", GetReqID() ).c_str(),
									3, "0",
									4, LIB_NS::UStrConvUtils::Format( "%d", _branchno ).c_str(),
									5, "RJWT"
								);
		//发送
		if(!m_socket.send_data(_sendBuffer, _sendSize)) 
		{				
			return FALSE;
		}

		//接收	
		int rtn = RecvArray();
		if( rtn < 16 )
			return false;
		
		DealedOrder order;

		order.ClientID			= GB2TS( _srvAccount );
		order.serial_no			= atoi( _recvArray[10] );

		order.date				= UTimeUtils::GetNowDate();
		order.stock_account		= GB2TS( _recvArray[3] );
		order.stock_code		= GB2TS( _recvArray[5] );
		order.stock_name		= GB2TS( _recvArray[6] );
		order.entrust_bs		= UKsFt2Helper::ToBuySell( atoi( _recvArray[13] ) );

		order.business_time		= UTimeUtils::FormTimeString( GB2TS(_recvArray[9]));
		order.business_price	= atof( _recvArray[8] );
		order.business_amount	= atoi( _recvArray[7] );
		order.business_balance	= atoi( _recvArray[16] );

		order.entrust_no		= atoi( _recvArray[11] );
		order.report_no			= atoi( _recvArray[14] );

		pres->push_back( order );
	}

	return true;
}

bool tradeadapter::ksft2::CKsFt2OrderSession::query_accountname(BasicAccountInfo* info)
{
	info->FundAccount	 = GB2TS( _srvAccount );
	info->BranchID		 = GB2TS( UStrConvUtils::Format( "%d", _branchno ) );
	info->ClientID		 = GB2TS( _srvAccount );
	info->SHStockAccount = GB2TS( _my_exch_sh_account );
	info->SZStockAccount = GB2TS( _my_exch_sz_account );


	//填充数据包
	_sendSize = FillBizData2( _sendBuffer, 10,
								0, "R",
								1, TS2GB(UNetUtils::GetMacAddress()).c_str(),
								2, UStrConvUtils::Format( "%d", GetReqID() ).c_str(),
								3, "63",
								4, UStrConvUtils::Format( "%d", _branchno ).c_str(),
								5, "1",
								7, _srvAccount,
								8, "RJWT",
								9, "0"
							);

	//发送
	if(!m_socket.send_data(_sendBuffer, _sendSize)) 
	{
		return false;
	}

	//接收
	int rtn = RecvArray();
	if( rtn > 5 && _recvArray[3][0] == 'Y' )
	{
		if( strlen( _recvArray[5] ) > 0 && strcmp( _recvArray[5], _srvAccount ) )
			info->ClientName = GB2TS( _recvArray[5] );
		else if( strlen( _recvArray[6] ) > 0 && strcmp( _recvArray[6], _srvAccount ) )
			info->ClientName = GB2TS( _recvArray[6] );
		else
			info->ClientName = GB2TS( _srvAccount );
	}


	return true;
}

bool tradeadapter::ksft2::CKsFt2OrderSession::query_accountsummary(BasicAccountInfo* info)
{
	CriticalSectionLocker csLocker(&m_critiSec_Counter);

	//填充数据包
	_sendSize = FillBizData2( _sendBuffer, 10,
								0, "R",
								1, TS2GB(UNetUtils::GetMacAddress()).c_str(),
								2, UStrConvUtils::Format( "%d", GetReqID() ).c_str(),
								3, "36",
								4, UStrConvUtils::Format( "%d", _branchno ).c_str(),
								5, "1",
								7, _srvAccount,
								8, "RJWT"
							);



	//发送
	if(!m_socket.send_data(_sendBuffer, _sendSize)) 
	{
		return false;
	}

	//接收
	int rtn = RecvArray();
	if( rtn < 12 || _recvArray[3][0]!='Y' )
	{
		return false;
	}
	info->MoneyType			= GB2TS( _recvArray[14] );
	info->CurrentBalance	= atof( _recvArray[4] );
	info->EnableBalance		= atof( _recvArray[5] );
	info->FetchCash			= atof( _recvArray[29] );
	info->AssetBalance		= atof( _recvArray[7] );

	return true;
}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          

bool tradeadapter::ksft2::CKsFt2OrderSession::TestHeart()
{
	CriticalSectionLocker csLocker(&m_critiSec_Counter);

	//填充心跳包
	memset(_sendBuffer,0,sizeof(_sendBuffer));	
	if( m_bAsync )
	{
		_sendSize = FillCommHeadAndCRC( DTPing, 0 );
	}
	else
	{
		_sendSize = FillBizData2( _sendBuffer, 6,
									0, "R",
									1, TS2GB(UNetUtils::GetMacAddress()).c_str(),
									2, UStrConvUtils::Format( "%d", GetReqID() ).c_str(),
									3, "22",
									4, UStrConvUtils::Format( "%D", _branchno ).c_str(),
									5, "RJWT"
								);
	}

	//发送
	if(!m_socket.send_data(_sendBuffer, _sendSize)) 
	{
		return false;
	}

	//心跳包返回
	memset(_recvBuffer,0x00,_recvSize);
	if(!m_socket.recv_data(_recvBuffer, _recvSize)) 
	{
		return false;
	}

	return true;
}


bool tradeadapter::ksft2::CKsFt2OrderSession::ReConnect()
{
	CriticalSectionLocker csLocker(&m_critiSec_Counter);

	m_bConnect = false;
	m_bLogin = false;

	BOOL bRet = m_socket.Connect(strError);
	if(!bRet)
	{
		WriteDetailLog( _T("error"),LTALL,(_T("重连连接服务器%s:%d  usr: %s ,pwd: %s失败"),
							m_socket._srvurl, m_socket._srvport, _srvAccount, _srvPwd) );
		return false;
	}

	m_bConnect = true;

	bRet = logon_server();
	if(!bRet)
	{
		return false;
	}

	return true;
}


/*
从 61 号功能的返回数据包中读取股东账户信息
*/
void tradeadapter::ksft2::CKsFt2OrderSession::ReadAccountFromA61()
{
	int index(0);
	char exch_split[500]={0};
	char exch_data[20][100]={0};
	char* lpBuffer=strtok(&_recvArray[16][1],";");	//格式："1,沪A,A139807969,1,人民币;2,深A,0027985790,1,人民币"
	while(lpBuffer!=NULL)
	{
		memset(exch_split,0x00,500);
		sprintf(exch_split,"%s",lpBuffer);
		lpBuffer=strtok(NULL,";");
		//

		index=0;
		memset(exch_data,0x00,2000);
		char* lpBuffer2=strtok(exch_split,",");
		while(lpBuffer2!=NULL)
		{
			sprintf(exch_data[index++],"%s",lpBuffer2);
			lpBuffer2=strtok(NULL,",");
		}
		if(exch_data[0][0]=='1')
		{
			//上海股东账号
			memcpy(_my_exch_sh_account,exch_data[2],strlen(exch_data[2]));
		}
		else if(exch_data[0][0]=='2')
		{
			//深圳股东账号
			memcpy(_my_exch_sz_account,exch_data[2],strlen(exch_data[2]));
		}
	}
}