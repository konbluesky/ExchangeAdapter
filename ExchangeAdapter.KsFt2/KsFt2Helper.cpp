
#include "texceptionex.h"
#include "ustringutils.h"
#include "KsFt2Helper.h"
#include "utimeutils.h"
#include "LogHelper.h"
#include <memory>
#include "otldb.h"

USING_LIBSPACE

#pragma warning(disable : 4244)

namespace tradeadapter
{
	namespace ksft2
	{


		std::map<OrderID, Order> UKsFt2Helper:: m_todayOrder;
		int UKsFt2Helper::m_today;
		/*
			JYS	交易所	
			SH		上海
			SZ		深圳
			HB		沪B
			SB		深B
			TA		三板A股
			TU		三板B股
			*/
		std::string UKsFt2Helper::GetJYSByContractCode( tstring code )
		{
			if(code[0] == _T('6'))
				return "SH";
			else if(code[0] == _T('0'))
				return "SZ";
			else if(code[0] == _T('1'))
				return "SZ";
			else if(code[0] == _T('5'))
				return "SH";
			else if(code[0] == _T('3'))
				return "SZ";
			else
			{
				WriteDetailLog( _T("error"),LTALL,(_T("未定义股票代码%s所属的市场"),code) );
				return "";
			}
		}

		std::string UKsFt2Helper::SelectGDHFromProduct( tstring code , std::string sh,std::string sz )
		{
			std::string market = GetJYSByContractCode(code);
			if(market == "SH")
				return sh;
			else if(market == "SZ")
				return sz;
			else
			{
				WriteDetailLog( _T("error"),LTALL,(_T("没有市场%s对应的股东账号"),market.c_str()) );
				return "";
			}
		}

		long UKsFt2Helper::GetJYLBFromBuySell( EBuySell ope )
		{
#pragma warning(disable : 4482)
			if(ope == EBuySell::eBuy)
				return 1;
			else if(ope == EBuySell::eSell)
				return 2;
			else
			{
				WriteDetailLog( _T("error"),LTALL,(_T("未定义交易类别")) );
				return -1;
			}
		}



		Order UKsFt2Helper::GetTodayOrderInfoByID( int orderid )
		{
			LoadTodayEntrustOrdersFromDB();

			std::map<OrderID, Order>::iterator it =  m_todayOrder.find(orderid) ;
			if(it != m_todayOrder.end())
			{
				return it->second;
			}
			else
			{
				WriteDetailLog( _T("error"),LTALL, ( _T("错误的orderid:%d，使在数据库里无法查找到对应的order信息"),orderid) );
				return Order();	
			}
		}


		void UKsFt2Helper::InsertEntrustOrderInfo( int Date, int Time, const char* clientID, const char* StockAccount, const char* ContractCode, int buysell, double Price, int Amount, const char* Comment, int orderid, int Status )
		{
			char sqlcmd[1024] = {0};

			sprintf(sqlcmd,
				"INSERT INTO  [ENTRUSTORDER] ( \
				[EXPROVIDER],[DATE],[TIME],[CLIENTID],[STOCKACCOUNT],[CONTRACTCODE],[BUYSELL],[PRICE],[AMOUNT],	[COMMENT],[ORDERID],[STATUS]\
				) VALUES  \
				('%s',   %d,     %d,    '%s',        '%s',         '%s',      '%d',    %f,   %d,    '%s',  %d,    '%d')",
				"ksft2",  Date,   Time , clientID,  StockAccount,ContractCode,buysell,Price,Amount,Comment,orderid,Status);
			try
			{
				m_pdb->ExcuteSql(sqlcmd);
			}
			catch(...)
			{
			}
		}


		void UKsFt2Helper::SaveOrderInfo(int wth,Order order, std::string jys,std::string gdh)
		{
			const char* clientID ="";
			int Date = UTimeUtils::GetNowDate();
			int Time = UTimeUtils::GetNowTime();
			const char* StockAccount = gdh.c_str();
			std::string ContractCode = TS2GB(order.ContractCode).c_str();
			int buysell = (int)order.OpeDir;
			double Price = order.Price;
			int Amount = order.Amount;
			const char* Comment = "";
			int orderid = wth;
			int Status = 0;
			try
			{
				InsertEntrustOrderInfo(Date, Time, clientID, StockAccount, ContractCode.c_str(), buysell, Price, Amount, Comment, orderid, Status);	
			}
			catch(...)
			{
			}	

		}

		bool UKsFt2Helper::LoadTodayEntrustOrdersFromDB()
		{
			
			if(m_today == UTimeUtils::GetNowDate())
				return true;

			std::cout<<"注：金仕达的柜台服务器的日期必须是正确的，否则业务会出现错误"<<std::endl;

			m_todayOrder.clear();

			std::string  date = TS2GB( UStrConvUtils::ToString( UTimeUtils::GetNowDate() ) );
			std::string  sqlcmd = std::string("SELECT [EXPROVIDER],[DATE],[TIME],[CLIENTID],[STOCKACCOUNT],[CONTRACTCODE],[BUYSELL],[PRICE],[AMOUNT],	[COMMENT],[ORDERID],[STATUS] \
								 FROM [ENTRUSTORDER] WHERE [DATE] = " ) + date;

			otl_stream result;
			try{

			if( m_pdb->Select(sqlcmd,&result) )
			{
				while(!result.eof())
				{
					//Name	Declared Type	Type	Size	Precision	Not Null	Not Null On Conflict	Default Value	Collate
					char EXPROVIDER[255] = {0};//	CHAR(255)	CHAR	255	0	False		NULL	
					int DATE =0;//	INT	INT	0	0	False		NULL	
					int TIME = 0 ;//	INT	INT	0	0	False		NULL	
					char CLIENTID[64] ={0} ;//	CHAR(64)	CHAR	64	0	False		NULL	
					char STOCKACCOUNT[16] ={0};//	CHAR(10)	CHAR	10	0	False		NULL	
					char CONTRACTCODE[12] = {0};//	CHAR(12)	CHAR	12	0	False		NULL	
					int BUYSELL	=0;//	INT		0	False		NULL	
					double PRICE = 0;//	DOUBLE	DOUBLE	0	0	False		NULL	
					int AMOUNT = 0;//	INT	INT	0	0	False		NULL	
					char COMMENT[128] ={0};//	CHAR(128)	CHAR	128	0	False		NULL	
					int ORDERID = 0;//	INT	INT	0	0	False		NULL	
					int STATUS =0;//	CHAR(255)	CHAR	255	0	False		NULL	

					result >>EXPROVIDER;
					result >>DATE;
					result >>TIME;
					result >>CLIENTID;
					result >>STOCKACCOUNT;
					result >>CONTRACTCODE;
					result >>BUYSELL;
					result >>PRICE;
					result >>AMOUNT;
					result >>COMMENT;
					result >>ORDERID;
					result >>STATUS;

					Order order;
					order.Amount = AMOUNT;
					order.clientID = GB2TS(CLIENTID);
					order.ContractCode = GB2TS(CONTRACTCODE);
					order.Day = DATE;
					order.OpeDir = (EBuySell )BUYSELL;
					order.Price = PRICE;
					order.Time = TIME;
					order.orderid = ORDERID;

					m_todayOrder[ORDERID] = order;
				}
				m_today = UTimeUtils::GetNowDate();
				return true;
			}
			else
			{
				WriteDetailLog( _T("error"),LTALL,(_T("读取金仕达业务数据库表[ENTRUSTORDER]时，出错了")) );
				return false;
			}
			
			}
			catch(...)
			{
				return false;
			}
		}
		std::shared_ptr<COTLDB> UKsFt2Helper::m_pdb;
		bool UKsFt2Helper::InitializeDB(const char* dsn, const char* usr,const char* pwd)
		{

			m_pdb = std::shared_ptr<COTLDB>(new COTLDB);
			bool ret = false;
			try
			{
				ret = m_pdb->Connect(dsn,usr,pwd);
			}
			catch(...)
			{
			}
			return ret;
		}

		tradeadapter::EBuySell UKsFt2Helper::ToBuySell( int JYLB )
		{
			if(1 == JYLB )
				return EBuySell::eBuy;
			else if(2 == JYLB)
				return EBuySell::eSell;
			else
			{
				WriteDetailLog( _T("error"),LTALL,(_T("未定义买卖方向代码%d"),JYLB) );

				return EBuySell::eNone;
			}
		}
		
		tradeadapter::EBuySell UKsFt2Helper::ToBuySell(const char* name )
		{
			if( 0 == strcmp( name, "买" ) )
				return EBuySell::eBuy;
			else if( 0 == strcmp( name, "卖" ) )
				return EBuySell::eSell;
			else
			{
				WriteDetailLog( _T("error"),LTALL,(_T("未定义买卖方向名称%s"),name) );
				return EBuySell::eNone;
			}
		}
	}
}