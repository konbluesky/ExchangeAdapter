#include "KSPXSocket.h"
namespace tradeadapter
{
	namespace ksft2
	{

		class CKsFt2OrderSession
		{
		public:

			CKsFt2OrderSession(bool bAsync = false);
			virtual ~CKsFt2OrderSession();	

		public:

			CRITICAL_SECTION  m_critiSec_Counter;
			CRITICAL_SECTION  m_critiSec_ReqID;
			HANDLE m_hExitEvent;
			HANDLE m_hHeartThread;
			static DWORD WINAPI HeartThread(void *pParam);//心跳线程
			bool TestHeart();
			bool ReConnect();
		public:

			bool Connect( const char* addr,int port,const char* usr="",const char* pwd="",int branchno=0,int timeout=1000 );
			bool logon_server();
			bool logout_server();		
			bool query_position(std::list<PositionInfo>* pPoss);
			bool place_order(Order ord, int& nWTH,char *errMsg);
			int cancel_order(OrderID orderid);
			bool query_dealedorders(std::list<DealedOrder>* pres);
			bool query_accountname(BasicAccountInfo* info);
			bool query_accountsummary(BasicAccountInfo* info);
			bool query_entrustinfo(std::list<EntrustInfo>* pinfo, int orderID = 0);
			int GetReqID();
		protected:
			int RecvArray();
			void ReadAccountFromA61();
		protected:
			enum DataType{
				DTLogin	= 0x11,		//登录包
				DTLoginRet	= 0x91,		//登录返回包
				DTPing		= 0x12,		//ping包
				DTPintRet	= 0x92,		//ping返回包
				DTData		= 0x13,		//业务数据包
				DTDataRet	= 0x93		//业务数据返回包
			};
			int FillCommHeadAndCRC( DataType dataType, int dataLength );
			int FillAsyncLogonData( char* pstrLogon );
			int FillBizHead();
			int FillBizData( char* buffer, int fieldMaxCount, ...);
			int FillBizData2( char* buffer, int fieldMaxCount, 
				int index1, const char* data1, int index2 = -1, const char* data2 = 0, int index3 = -1, const char* data3 = 0, int index4 = -1, const char* data4 = 0, int index5 = -1, const char* data5 = 0,
				int index6 = -1, const char* data6 = 0, int index7 = -1, const char* data7 = 0, int index8 = -1, const char* data8 = 0, int index9 = -1, const char* data9 = 0, int index10 = -1, const char* data10 = 0,
				int index11 = -1, const char* data11 = 0, int index12 = -1, const char* data12 = 0, int index13 = -1, const char* data13 = 0, int index14 = -1, const char* data14 = 0, int index15 = -1, const char* data15 = 0,
				int index16 = -1, const char* data16 = 0, int index17 = -1, const char* data17 = 0, int index18 = -1, const char* data18 = 0, int index19 = -1, const char* data19 = 0, int index20 = -1, const char* data20 = 0,
				int index21 = -1, const char* data21 = 0, int index22 = -1, const char* data22 = 0, int index23 = -1, const char* data23 = 0, int index24 = -1, const char* data24 = 0, int index25 = -1, const char* data25 = 0,
				int index26 = -1, const char* data26 = 0, int index27 = -1, const char* data27 = 0, int index28 = -1, const char* data28 = 0, int index29 = -1, const char* data29 = 0, int index30 = -1, const char* data30 = 0);
			int transferRecvToArray(void);
		private:
			KSPXSocket m_socket;
			/*
			金仕达SPX周边接入网关,分为两种: 标准同步网关SpxSwitch 和 异步网关SpxASwitch
			标准同步网关，只有业务数据包，通信头和业务头长度都为0
			*/
			bool m_bAsync;
			int m_lnCommHead;
			int m_lnBizHead;

			LPSTR _sendBuffer;
			long  _sendSize;
			LPSTR	_recvBuffer;	//接收缓冲区
			long	_recvSize;		//已接受数据大小

			TCHAR strError[100];

			int 	_branchno;

			char	_srvAccount[32];
			char	_srvPwd[32];
		public:
			int m_reqID;


			char  _recvArray[200][500];
			BOOL m_bLogin;
			BOOL m_bConnect;
		public:
			char _my_exch_sh_account[20];
			char _my_exch_sz_account[20];

			

			
		};
	}
}