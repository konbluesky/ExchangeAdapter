#include "KSPXSocket.h"
#include "ustringutils.h"

tradeadapter::ksft2::KSPXSocket::KSPXSocket(void)
{
	WSADATA wsa={0};
	int ret = WSAStartup(0x0101,&wsa);
	if( ret != 0 )
	{
		printf( "!!!WSAStartup failed:%d", ret );
	}

	memset(_srvurl,0,sizeof(_srvurl));	
	lstrcpyA(_srvurl,"");
	_srvport = 9000;	
}

tradeadapter::ksft2::KSPXSocket::~KSPXSocket(void)
{	
	WSACleanup();
}
bool tradeadapter::ksft2::KSPXSocket::Connect(LPTSTR lpError)
{
	Close(FALSE);
	_hSocket=socket(PF_INET,SOCK_STREAM,0);
	if(_hSocket==INVALID_SOCKET)
	{
		lstrcpy(lpError,_T("INVALID_SOCKET"));
		return false;
	}
	long noDelay(1),tmSend(150*1000L),tmRecv(150*1000L);
	long reset_op(1024*1024L);

	setsockopt(_hSocket,IPPROTO_TCP,TCP_NODELAY,(LPSTR)&noDelay,sizeof(long));
	setsockopt(_hSocket,SOL_SOCKET,SO_SNDTIMEO,(LPSTR)&tmSend,sizeof(long));
	setsockopt(_hSocket,SOL_SOCKET,SO_RCVTIMEO,(LPSTR)&tmRecv,sizeof(long));
	setsockopt(_hSocket,SOL_SOCKET,SO_RCVBUF,(LPSTR)&reset_op,sizeof(long));

	sockaddr_in	remote={0};
	remote.sin_family = AF_INET;
	remote.sin_port = htons(_srvport);
	LPHOSTENT lphost=NULL;
	if((remote.sin_addr.s_addr=inet_addr(_srvurl))==INADDR_NONE)
	{
		if(lphost=gethostbyname(_srvurl))
			remote.sin_addr.s_addr = ((LPIN_ADDR)lphost->h_addr)->s_addr;
	}
	int ret = connect(_hSocket,(sockaddr*)&remote,sizeof(remote));
	if( SOCKET_ERROR == ret  )
	{
		int nErr = WSAGetLastError();
		if( WSAEWOULDBLOCK == nErr )
		{
			lstrcpy(lpError,_T("connect error-ksft2counter!"));		
			closesocket(_hSocket);
			_hSocket = INVALID_SOCKET;
		}
		return false; 
	}
	return true;
	
}
bool tradeadapter::ksft2::KSPXSocket::check_connect(BOOL&new_connect)
{
	new_connect = FALSE;
	if(_hSocket!=INVALID_SOCKET)
	{
		fd_set fd={0};
		timeval tmout={0,100};
		FD_ZERO(&fd);
		FD_SET(_hSocket,&fd);
		int rtCode(select(_hSocket+1,&fd,NULL,NULL,&tmout));
		if(!rtCode||rtCode==1) //保险一点:只要有异常,就认为需要重联
			return true;
	}
	new_connect = TRUE;
	TCHAR szError[256]={0};
	return(Connect(szError));
}
bool tradeadapter::ksft2::KSPXSocket::send_data(LPCVOID lpData,long sizeData)
{
	long lBytes(0),l(0);
	for(;sizeData>0;)
	{
		lBytes = send(_hSocket,(LPCSTR)lpData+l,sizeData,0);
		if(lBytes==SOCKET_ERROR)
		{
			if(WSAGetLastError()==WSAEWOULDBLOCK)
			{
				Sleep(100);
				continue;
			}
			return false;
		}
		else
		{
			if(lBytes<1)	//连接异常或者被断开
				return(FALSE);
			sizeData -= lBytes;
			l += lBytes;
		}
		Sleep(1);
	}
	return true;
}

bool tradeadapter::ksft2::KSPXSocket::recv_data(LPVOID lpData,long sizeData)
{
	long lBytes(0),l(0);
	//for(;l<sizeData;)
	{
		lBytes=recv(_hSocket,(LPSTR)lpData+l,sizeData-l,0);
		if(lBytes==SOCKET_ERROR)
		{
			int nErr = WSAGetLastError();
			if( WSAEWOULDBLOCK == nErr )
			{
				Sleep(100);
				//continue;
			}
			return false;
		}
		else
		{
			if(lBytes<1)	//连接异常或者被断开
				return false;
			l += lBytes;
		}
		Sleep(1);
	}
	return true;
}



void tradeadapter::ksft2::KSPXSocket::Close(BOOL bDestroy)
{	
	if(_hSocket!=INVALID_SOCKET)
	{
		closesocket(_hSocket);
		_hSocket = INVALID_SOCKET;
	}
	else
	{
		printf( "");
	}
}

