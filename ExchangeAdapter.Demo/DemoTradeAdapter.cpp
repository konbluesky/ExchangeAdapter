#include "StdAfx.h"
#include <iostream>
#include "ucodehelper.h"
#include "ustringutils.h"
#include "URandom.h"
#include "umaths.h"
#include "tinyxml.h"
#include "tconfigsetting.h"
#include "ucomhelper.h"
#include "utimeutils.h"
#include "loki/Singleton.h"
#include "otldb.h"
#include "TradeLogDB.h"
#include "DemoTradeAdapter.h"


//typedef Loki::SingletonHolder<COTLDB>   COTLDBHolder;
#pragma warning(disable : 4482)
USING_LIBSPACE


namespace tradeadapter
{
	namespace demo
	{
		CDemoTradeAdapter::CDemoTradeAdapter(void)
		{
			m_orderidIndex = 1;
		}

		CDemoTradeAdapter::~CDemoTradeAdapter(void)
		{
		}

		bool CDemoTradeAdapter::Initialize(void* param /*= NULL */)
		{
			m_param = *((DemoParam*)param);
			bool ret = CTradeLogDBHolder::Instance().InitializeDB(m_param.dbname.c_str(), m_param.dbusr.c_str(), m_param.dbpwd.c_str());
			if (!ret)
			{
				SetLastError(MAKE_ERRORINFO(EErrSystem::EConfigFile ,-1, _T("链接数据库失败")));
				return false;
			}


			ret = InitAccountInfo();

			ret = QueryPostions(&m_pos);

			ret = QueryDealedOrders(&m_dealedorders);


			SetOrderIDIndex();



			return ret;
		}



		bool CDemoTradeAdapter::InitializeByXML(const TCHAR* fpath)
		{
			TConfigSetting setting;
			bool ret = setting.LoadConfig(fpath);
			if (!ret)
			{
				this->SetLastError(MAKE_ERRORINFO(EErrSystem::EConfigFile, -1, _T("配置文件无法找到")));
				return ret;
			}

			DemoParam param;
			param.DealedAll = setting["configuration"]["dealedall"].EleVal() == "true";
			param.Poundage = UMaths::ToDouble(setting["configuration"]["poundage"].EleVal().c_str());

			param.clientid = setting["configuration"]["clientid"].EleVal();


			param.dbname = setting["configuration"]["database"]["dsn"].EleVal();
			param.dbusr = setting["configuration"]["database"]["dbusr"].EleVal();
			param.dbpwd = setting["configuration"]["database"]["dbpwd"].EleVal();
			param.usedb = setting["configuration"]["database"]["usedb"].EleVal() == "true";


			ret = Initialize(&param);



			return ret;
		}

		bool CDemoTradeAdapter::EntrustOrder(Order order, OrderID* pOutID)
		{
			*pOutID = tradeadapter::InvalidOrderId;
			order.Day = UTimeUtils::GetNowDate();

			int curpos = 0;
			if (order.OpeDir == EBuySell::eSell)
			{
				bool finded = false;
				for (std::list<PositionInfo>::iterator it = m_pos.begin(); it != m_pos.end(); ++it)
				{
					if (it->code == order.ContractCode)
					{
						finded = true;
						curpos = (int)it->current_amount;
						break;
					}
				}
				if (finded == false)
				{
					SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter, -1, _T("没有可卖产品的股票")));
					return false;
				}



			}
			DealedOrder dealed = CreateRandomStockDealedOrder(order, curpos);
			if (dealed.business_amount != 0)
			{


				UpdatePosition(dealed);
				bool ret = CTradeLogDBHolder::Instance().InsertDealedOrderInfo(dealed.date, dealed.business_time, m_param.clientid.c_str(), "demofund",
					TS2U8(dealed.stock_code.c_str()).c_str()
					, dealed.entrust_bs, dealed.business_price, (int)dealed.business_amount, "demo test", dealed.entrust_no, 1, dealed.serial_no, "demo");
				m_dealedorders.push_back(dealed);
				*pOutID = dealed.entrust_no;
				return true;
			}
			else
			{
				SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter, -1, _T("随机成交的数量为0，不生成有效的委托单号")));
				return false;
			}
		}

		bool CDemoTradeAdapter::CancelOrder(OrderID orderid)
		{
			SetLastError(MAKE_ERRORINFO(EErrSystem::ETradeAdapter,-1, _T("模拟系统，无法撤单")));
			return false;
		}

		bool CDemoTradeAdapter::QueryOrder(OrderID id, int* pCount)
		{
			*pCount = 0;
			for (std::list<DealedOrder>::iterator it = m_dealedorders.begin(); it != m_dealedorders.end(); ++it)
			{
				if (it->entrust_no == id)
				{
					*pCount = (int)it->business_amount;
					return true;
				}
			}
			return true;
		}

		void CDemoTradeAdapter::UnInitialize()
		{
			this->m_dealedorders.clear();
		}

		bool CDemoTradeAdapter::QueryDealedOrders(std::list<DealedOrder>* pres)
		{
			pres->clear();
			CTradeLogDBHolder::Instance().LoadDealedOrderFromDB(pres, UTimeUtils::GetNowDate(), "demo",
				m_param.clientid.c_str());

			m_dealedorders = *pres;
			if (pres != NULL && pres->size() > 0)
			{

			}

			return true;
		}

		bool CDemoTradeAdapter::QueryAccountInfo(BasicAccountInfo* info)
		{
			*info = m_info;
			return true;
		}

		bool CDemoTradeAdapter::QueryPostions(std::list<PositionInfo>* pPoss, const TCHAR* pstock /*= NULL */)
		{
			bool ret = false;

			if (this->m_pos.size() > 0)
			{
				*pPoss = m_pos;
				ret = true;
			}
			else
			{
				ret = CTradeLogDBHolder::Instance().QueryPostions(pPoss, m_param.clientid, "demo");
			}

			if (pstock == NULL)
			{
				return ret;
			}
			else
			{
				for (auto it = pPoss->begin(); it != pPoss->end(); ++it)
				{
					if (it->code == pstock)
					{
						PositionInfo info = *it;
						pPoss->clear();
						pPoss->push_back(info);
						return ret;
					}
				}

			}
			return ret;
		}

		DealedOrder CDemoTradeAdapter::CreateRandomStockDealedOrder(Order order, int curpos)
		{

			DealedOrder dealed;

			dealed.business_amount = URandom::NextRange(0, (int)(order.Amount));
			if (order.OpeDir == EBuySell::eSell && curpos < 100)
			{
				dealed.business_amount = curpos;

			}
			dealed.business_price = order.Price;
			dealed.business_balance = order.Price * dealed.business_amount;
			dealed.business_time = UTimeUtils::GetNowTime();
			dealed.date = UTimeUtils::GetNowDate();
			dealed.entrust_bs = order.OpeDir;

			m_orderidIndex++;
			dealed.entrust_no = m_orderidIndex;
			dealed.serial_no = m_orderidIndex;
			dealed.stock_code = order.ContractCode;

			return dealed;
		}




		bool CDemoTradeAdapter::IsInitialized()
		{
			return true;
		}

		bool CDemoTradeAdapter::InitAccountInfo()
		{
			m_info = CTradeLogDBHolder::Instance().GetAccountInfo(m_param.clientid, "demo");
			m_info.ExProvider = _T("demo");
			return true;
		}

		bool CDemoTradeAdapter::QueryEntrustInfos(std::list<EntrustInfo>* pinfo)
		{
			return false;
		}

		void CDemoTradeAdapter::SetOrderIDIndex()
		{
			if (m_dealedorders.size() > 0)
			{
				std::list<DealedOrder>::iterator it = m_dealedorders.end();
				--it;
				m_orderidIndex = it->entrust_no;
			}
			else
			{
				m_orderidIndex = 0;
			}

		}

		bool CDemoTradeAdapter::UpdatePosition(DealedOrder order)
		{
			for (std::list<PositionInfo>::iterator it = m_pos.begin(); it != m_pos.end(); ++it)
			{
				if (it->code == order.stock_code)
				{
					double money = it->cost_price * it->total_amount;
					double newmoney = order.business_price* order.business_amount;
					if (order.entrust_bs == EBuySell::eBuy)
					{

						it->total_amount += order.business_amount;
						it->cost_price = (money + newmoney) / it->total_amount;
					}
					else
					{
						it->total_amount -= order.business_amount;
						it->cost_price = (money - newmoney) / it->total_amount;

					}
					it->current_amount = it->enable_amount = it->total_amount;
					return true;
				}
			}
			return true;

		}

		tstring CDemoTradeAdapter::GetName()
		{
			return _T("Demo Trade Adapter");
		}

	}
}