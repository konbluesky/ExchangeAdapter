#pragma once
#include <map>
#include <memory>
#include "ITradeAdapter.h"
class COTLDB;
namespace tradeadapter
{
	namespace demo
	{
		struct DemoParam
		{
			bool DealedAll;
			double Poundage;
			//
			bool usedb;
			std::string dbname;
			std::string dbusr;
			std::string dbpwd;
			std::string clientid;

			static bool LoadFromXML(DemoParam* pOut)
			{
				return true;
			}

		};



		class CDemoTradeAdapter : public ITradeAdapter
		{
		public:
			CDemoTradeAdapter(void);

			bool InitAccountInfo();

			virtual ~CDemoTradeAdapter(void);

			virtual bool Initialize(void* param = NULL);

			virtual bool InitializeByXML(const TCHAR* fpath);

			virtual bool EntrustOrder(Order order, OrderID* pOutID);

			virtual bool CancelOrder(OrderID orderid);

			virtual bool QueryOrder(OrderID id, int* pCount);

			virtual void UnInitialize();

			virtual bool QueryDealedOrders(std::list<DealedOrder>* pres);

			virtual bool QueryAccountInfo(BasicAccountInfo* info);

			virtual bool QueryPostions(std::list<PositionInfo>* pPoss, const TCHAR* pstock = NULL);



			

			virtual bool UpdatePosition(DealedOrder order);


			void SetOrderIDIndex();

		private:
			DealedOrder CreateRandomStockDealedOrder(Order order, int curpos);
			DealedOrder CreateRandomFutureDealedOrder(Order order, int curpos);

			virtual bool IsInitialized();

			virtual bool QueryEntrustInfos(std::list<EntrustInfo>* pinfo);

			virtual tstring GetName();

			typedef std::map<OrderID, Order>::iterator ITERATOR;


			BasicAccountInfo m_info;

			std::list<PositionInfo> m_pos;

			std::list<DealedOrder> m_dealedorders;



			

			DemoParam m_param;

			int m_orderidIndex;


			std::shared_ptr<COTLDB> m_pdb;

		};

	}
}