#pragma once
#include "IOrderService.h"
#include <vector>
#include <map>
#include "threadlock.h"
#include "ExchangeAdapterDef.h"

namespace tradeadapter
{

#pragma warning(disable : 4275)

	//
	//
	//注：这里
	class TAIAPI COrderServiceImp : public IOrderService
	{
	public:
		COrderServiceImp(void);
		virtual ~COrderServiceImp(void);


		virtual bool Initialize(void* p = NULL);
		virtual void UnInitialize();


		virtual ITradeAdapter* GetAdapter();
		//************************************
		// Method:    EntrustOrder
		// FullName:  COrderServie::EntrustOrder
		// Access:    public 
		// Returns:   如果返回-1，则系统错误，所有业务停止，调用GetLastError
		// Qualifier:
		// Parameter: const TCHAR * clientID
		// Parameter: Order
		//************************************
		virtual bool  EntrustOrder(const TCHAR* clientID, const  Order* order, OrderID* pOutID);
		//************************************
		// Method:    CancelOrder
		// FullName:  COrderServie::CancelOrder
		// Access:    public 
		// Returns:   撤单是否成功，在自动交易的情况下，应该都是true。
		// Qualifier:
		// Parameter: const TCHAR * clinetID
		// Parameter: int OrderID
		//************************************
		virtual  bool CancelOrder(const TCHAR* clinetID, OrderID id);


		//************************************
		// Method:    QueryOrder
		// FullName:  COrderServie::QueryOrder
		// Access:    public 
		// Returns:   返回成交量，注：这个值如果order是无效单，则为0，如果order部分成，则不一定被100整除，
		// Qualifier:
		// Parameter: const TCHAR * clientID
		// Parameter: OrderID id
		//************************************
		virtual int QueryOrder(const TCHAR* clientID, OrderID id);



		std::vector<int> QueryOrders(const TCHAR* clientID);



		virtual bool IsInitialized();

		virtual bool QueryPostions(std::list<PositionInfo>* pPoss, const TCHAR* pstock = NULL);

		//virtual bool InitializeByXML( const TCHAR* fpane );


		bool InitializeTradelogDB();

		virtual bool GetLastError(ErrorInfo* perr);

		virtual bool InitializeByXML(const TCHAR* pPath);


		std::string m_id;

	private:
		ITradeAdapter* m_adapter;
		threadlock::multi_threaded_local m_lock;

		//std::vector<OrderDetail> m_orderdetails;

	};




};