设计原理：
ExchangeAdapter.xxx主要做的是：上面标准的数据格式和函数调用，映射到具体柜台的数据格式和函数调用，并做反向操作


而ExchangeAdapterService做的内容：

1. 根据配置创建/管理ExchangeAdapter.xxx   （类工厂）
2. 多账户/分账户，多交易柜台的映射          （map）
3. 并做此层面的日志记录（注：这里是一个统一的日志记录，不用每个adapter都做记录）   (log)





==============