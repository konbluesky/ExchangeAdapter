#include "stdafx.h"
#include "ustringutils.h"
#include "IOrderService.h"
#include "ITradeAdapter.h"
#include "ustringutils.h"
#include "TradeCExport.h"
#include "ucodehelper.h"
USING_LIBSPACE
//注：对于其他语言使用的字符集，应该以anscii为首选,这样其他各种语言的调用就相对非常的简单


static tradeadapter::IOrderService* pinstance=NULL;


EXPORT_CAPI bool TradeInitialize(const char* pfname)
{
	tstring tfname = UCodeHelper::ToTS(std::string(pfname));
	pinstance = tradeadapter::IOrderService::CreateInstance(_T("default"));
	bool ret = pinstance->GetAdapter()->InitializeByXML(tfname.c_str());
	return ret;
}
EXPORT_CAPI int EntrustOrder(const TCHAR* id,tradeadapter::Order order)
{
	if(pinstance == NULL)
	{
		return -1;
	}
	else
	{
		int amount = pinstance->EntrustOrder(id,order);
		return amount;
	}
}
EXPORT_CAPI int QueryOrder(const TCHAR* usrid,tradeadapter::OrderID id)
{
	if(pinstance == NULL)
	{
		return -1;
	}
	else
	{
		int ret = pinstance->QueryOrder(usrid,id);
		return ret;
	}

}
EXPORT_CAPI bool CancelOrder(const TCHAR* usrid,tradeadapter::OrderID id)
{
	if(pinstance == NULL)
	{
		return false;
	}
	else
	{
		bool ret = pinstance->CancelOrder(usrid,id);
		return ret;
	}

}
