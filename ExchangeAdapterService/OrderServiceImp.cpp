#include "StdAfx.h"
#include <iostream>
#include <tchar.h>
#include "ucodehelper.h"
#include "utimeutils.h"
#include "ustringutils.h"
#include "OrderServiceImp.h"
#include "ITradeAdapter.h"
#include "tconfigsetting.h"
#include "ustringutils.h"
#include "tinyxml.h"
#include "udll.h"
#include "texceptionex.h"
#include "ErrorDefinition.h"
#include "ExchangeAdapterDef.h"

USING_LIBSPACE
#pragma warning(disable : 4273)

#define auto_lock() threadlock::autoguard<threadlock::multi_threaded_local>  __xx__(&m_lock)
namespace tradeadapter
{


	void* _CreateTraderProviderFromDll(const TCHAR* hsdllname)
	{

		UDll::HLIB hlib = UDll::OpenDll(hsdllname);
		if (hlib == NULL)
		{
			tstring err = UStrConvUtils::Format(_T("无法加载交易接口动态库%s"), hsdllname);
			throw TException(err);
		}

		typedef  void* CTFunc();
		CTFunc* pfunc = (CTFunc*)UDll::GetDllFunc(hlib, _T("CreateTraderProvider"));

		if (pfunc == NULL)
		{
			throw TException(_T("动态库没有对应的导出函数CreateTraderProvider"));
		}

		void* pProvide = pfunc();
		return pProvide;
	}
	bool COrderServiceImp::Initialize(void* p)
	{
		auto_lock();
		TConfigSetting cfg;
		bool ret = cfg.LoadConfig(_T("ExchangeAdapterService.xml"));
		if (!ret)
		{
			std::cout << "无法加载配置文件" << std::endl;
			return false;
		}


		tstring dll = U82TS(cfg["configuration"][m_id.c_str()]["dll"].EleVal().c_str());

		m_adapter = (ITradeAdapter*)_CreateTraderProviderFromDll(dll.c_str());
		if (m_adapter == NULL)
		{
			return false;
		}

		if (p == NULL)
		{
			tstring cfgfile = U82TS(cfg["configuration"][m_id.c_str()]["cfg"].EleVal().c_str());

			ret = m_adapter->InitializeByXML(cfgfile.c_str());
		}
		else
		{
			ret = m_adapter->Initialize(p);
		}
		return ret;
	}


	bool COrderServiceImp::InitializeTradelogDB()
	{
		return false;

	}



	bool COrderServiceImp::EntrustOrder(const TCHAR* clientID, const Order* order, OrderID* pOutID)
{
		auto_lock();
		bool ret = false;
		if (m_adapter != NULL)
		{
			/*if (order->Day == -1)
			{
			order->Day = UTimeUtils::GetNowDate();
			}*/
			ret = m_adapter->EntrustOrder(*order, pOutID);			
		}
		return ret;
	}

	bool COrderServiceImp::CancelOrder(const TCHAR* clinetID, OrderID id)
	{
		auto_lock();
		if (m_adapter != NULL)
		{
			bool ret = m_adapter->CancelOrder(id);
			return ret;
		}
		return false;
	}

	int COrderServiceImp::QueryOrder(const TCHAR* clientID, OrderID id)
	{
		auto_lock();
		if (m_adapter != NULL)
		{
			int ret;
			m_adapter->QueryOrder(id, &ret);
			return ret;
		}
		return 0;
	}

	std::vector<int> COrderServiceImp::QueryOrders(const TCHAR* clientID)
	{
		auto_lock();
		std::vector<int> orderids;
		return orderids;
	}


	COrderServiceImp::COrderServiceImp(void) :m_adapter(NULL)
	{
	}

	COrderServiceImp::~COrderServiceImp(void)
	{
		UnInitialize();
	}

	void COrderServiceImp::UnInitialize()
	{
		auto_lock();
		if (m_adapter != NULL)
		{
			m_adapter->UnInitialize();
			m_adapter = NULL;
		}
	}

	ITradeAdapter* COrderServiceImp::GetAdapter()
	{
		return m_adapter;
	}

	bool COrderServiceImp::IsInitialized()
	{
		auto_lock();

		return m_adapter != NULL && m_adapter->IsInitialized();
	}

	bool COrderServiceImp::QueryPostions(std::list<PositionInfo>* pPoss, const TCHAR* pstock /*= NULL */)
	{

		auto_lock();

		bool ret = m_adapter->QueryPostions(pPoss, pstock);
		if (ret && pstock != NULL && pPoss->size() > 1)
		{
			for (std::list<PositionInfo>::iterator it = pPoss->begin(); it != pPoss->end(); ++it)
			{
				if (it->code == pstock)
				{
					PositionInfo info = *it;
					pPoss->clear();
					pPoss->push_back(info);
					return ret;
				}
			}
		}
		return ret;
	}

	bool COrderServiceImp::GetLastError(ErrorInfo* perr)
	{
		auto_lock();

		if (m_adapter != NULL)
		{

			m_adapter->GetLastError(perr);
			return true;
		}
		else
		{
			return false;
		}
	}

	bool COrderServiceImp::InitializeByXML(const TCHAR* pPath)
	{
		throw std::logic_error("The method or operation is not implemented.");
	}



	TAIAPI  IOrderService* IOrderService::CreateInstance(const TCHAR* id)
	{
		
		std::string strid = TS2U8(id);

		strid = strid != "default" ? strid : "default";
		std::cout << "tradeadapter: " << strid << std::endl;
		COrderServiceImp* pinstance = new COrderServiceImp;

		pinstance->m_id = "default";


		return pinstance;
	}




	//计算成本价
	TAIAPI double IOrderService::CalculatePosCostValue(std::list<PositionInfo>* pPoss)
	{
		if (pPoss != NULL)
		{
			double sum = 0;
			for (std::list<PositionInfo>::iterator it = pPoss->begin(); it != pPoss->end(); ++it)
			{
				sum += it->cost_price * it->total_amount;
			}
			return sum;
		}
		return 0;
	}
	TAIAPI double IOrderService::CalcuatePosMarketValue(std::list<PositionInfo>* pPoss)
	{
		if (pPoss != NULL)
		{
			double sum = 0;
			for (std::list<PositionInfo>::iterator it = pPoss->begin(); it != pPoss->end(); ++it)
			{
				sum += it->market_value;
			}
			return sum;
		}
		return 0;
	}

}
